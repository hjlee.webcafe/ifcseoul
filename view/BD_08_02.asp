<div id="content">
	<div class="contWrap">
		<div class="tabWrap type3">
			<ul>
				<li class=""><a href="BD_08_01.asp"><%=gnb_text(31)%></a></li>
				<li class="other on"><a href="BD_08_02.asp"><%=gnb_text(32)%></a></li>
				<li><a href="BD_08_03.asp"><%=gnb_text(33)%></a></li>
			</ul>
		</div>
		<div class="titleWrap bgIs0802 pSh" id="contTitle">
			<h2><a href="#none">Leasing</a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(12)%></dt>
				<dd><a href="BD_01_00.asp"><%=gnb_text(17)%></a></dd>
				<dd><a href="BD_02_00.asp"><%=gnb_text(18)%></a></dd>
				<dd><a href="BD_03_00.asp"><%=gnb_text(19)%></a></dd>
				<dd><a href="BD_04_00.asp"><%=gnb_text(20)%></a></dd>
				<dd><a href="BD_05_00.asp"><%=gnb_text(21)%></a></dd>
				<!--dd><a href="BD_06_00.asp"><%=gnb_text(22)%></a></dd-->
				<dd><a href="BD_07_01.asp"><%=gnb_text(23)%></a></dd>
				<dd class="on"><a href="BD_08_01.asp"><%=gnb_text(30)%></a></dd>
			</dl>
			<h3><a href="#none"><%=lang_text(0)%></a></h3>
			<dl class="h3Siblings">
				<dt>Leasing</dt>
				<dd><a href="BD_08_01.asp"><%=gnb_text(31)%></a></dd>
				<dd class="on"><a href="BD_08_02.asp"><%=gnb_text(32)%></a></dd>
				<dd><a href="BD_08_03.asp"><%=gnb_text(33)%></a></dd>
			</dl>
		</div>
		<!-- <p class="subTitle cB"><%=lang_text(1)%></p> -->		
		<div class="contBody contactCont">
			<h4><%=lang_text(2)%></h4>
			<ul>
				<li>
					<dl>
						<dt class="titleBar"><%=lang_text(3)%></dt>
						<dd>TEL : (02) 6333-5740</dd>
						<dd>E-mail : james.tyrrell@aig.com</dd>
						<!-- <dd class="gender man"><span class="blind">man</span></dd> -->
					</dl>
				</li>
				<li>
					<dl>
						<dt class="titleBar"><%=lang_text(4)%></dt>
						<dd>TEL : (02) 6333-5745</dd>
						<dd>Mobile :  010-3774-2119</dd>
						<dd>E-mail : sw.lee@aig.com</dd>
						<!-- <dd class="gender man"><span class="blind">man</span></dd> -->
					</dl>
				</li>
			</ul>
			<ul>
				<li>
					<dl>
						<dt class="titleBar"><%=lang_text(5)%></span></dt>
						<dd>TEL : (02) 6333-5747</dd>
						<dd>Mobile :  010-7266-2847</dd>
						<dd>E-mail : hs.shin@aig.com</dd>
						<!-- <dd class="gender man"><span class="blind">man</span></dd> -->
					</dl>
				</li>
				<li>
					<dl>
						<dt class="titleBar"><%=lang_text(6)%></dt>
						<dd>TEL : (02) 6333-5749</dd>
						<dd>Mobile :  010-9751-5558</dd>
						<dd>E-mail : my.park@aig.com</dd>
						<!-- <dd class="gender man"><span class="blind">man</span></dd> -->
					</dl>
				</li>
			</ul>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>