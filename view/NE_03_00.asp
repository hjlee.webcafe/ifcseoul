<%
Dim countQuery,selectQuery,num_count,page_result, t_page, query_where,objRs,sarch_query, select_name
Dim state_array(),state,ny,nm,nd,n_alldate,y,m,d,alldate,ey,em,ed,e_alldate,objCo,group_array(2)
group_array(0)="Seoul"
group_array(1)="Mall"
group_array(2)="Conrad"

ny = FormatDateTime(now(),2)
ny = left(ny,4)
nm = FormatDateTime(now(),2)
nm = mid(nm,6,2)
nd = FormatDateTime(now(),2)
nd = mid(nd,9,2)

n_alldate = ny&nm&nd

sarch_query=""

if(current_type = "ko") then
query_where = "and e_group='0'"
else
query_where = "and e_group='1'"
end if

e_state = Request.QueryString("type")

If e_state = "" or e_state = "all" Then
	e_state = "all"
	select_name = "ALL"
ElseIf e_state = 0 Then
	select_name = "IFC SEOUL"
ElseIf e_state = 1 Then
	select_name = "CONRAD SEOUL"
ElseIf e_state = 2 Then
	select_name = "IFC MALL"
End If
	
If e_state = "all" Then
	query_where = query_where
Else
	query_where = query_where & " and e_state = '"& e_state &"'"
End If

' counting list
countQuery = "select count(*) as count from dbo.tblEvent where 1 = 1 " &sarch_query & query_where

set objConn = OpenDBConnection()
set objRs = SendQuery(objConn,countQuery)
total_count = objRs("count")
num_count = total_count - (page - 1) * page_limit
set objRs = Nothing

query_where = query_where & "order by e_num desc"

' get list
selectQuery = "select Top " & page_limit & " * from dbo.tblEvent where e_num Not In (select Top " & current_count & " e_num from tblEvent where 1 = 1 "&sarch_query & query_where &") "&sarch_query & query_where
set objRs = SendQuery(objConn,selectQuery)

' get Y-m-s list
selectList = "select Top " & page_limit & " * from dbo.tblEvent where e_num Not In (select Top " & current_count & " e_num from tblEvent where 1 = 1 "&sarch_query & query_where &") "&sarch_query & query_where
set objCo = SendQuery(objConn,selectList)

if objCo.RecordCount >=0 then
	
ReDim state_array(objCo.RecordCount - 1)
	
	For k = 0 to objCo.RecordCount - 1 
	
	y=objCo("e_st_date")
	y=left(y,4)
	m=objCo("e_st_date")
	m=mid(m,6,2)
	d=objCo("e_st_date")
	d=mid(d,9,2)
	
	alldate=y&m&d
	
	ey=objCo("e_ed_date")
	ey=left(ey,4)
	em=objCo("e_ed_date")
	em=mid(em,6,2)
	ed=objCo("e_ed_date")
	ed=mid(ed,9,2)
	
	e_alldate=ey&em&ed
	
	'n_alldate : 현재날짜, alldate : 시작일 , e_alldate : 종료일
	
		if(n_alldate < alldate) then
			state_array(k) = "예정"
		end if
		
		if(n_alldate > alldate and n_alldate < e_alldate) then
			state_array(k) = "진행중"		
		end if 
		
		if(n_alldate > e_alldate) then
			state_array(k) = "종료일"
		end if
	
	objCo.MoveNext
 	Next
 End If

t_page = page
page_result = page_create(total_count, t_page, page_limit, page_length)
%>

<div id="content">
	<div class="contWrap">
		<div class="titleWrap titleNe0300 pSh" id="contTitle">
			<h2><a href="#none"><%=lang_text(0)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(36)%></dt>
				<dd><a href="NE_01_00.asp"><%=gnb_text(37)%></a></dd>
				<dd><a href="NE_02_00.asp"><%=gnb_text(38)%></a></dd>
				<dd class="on"><a href="NE_03_00.asp"><%=gnb_text(39)%></a></dd>
			</dl>
		</div>
		<!-- <p class="subTitle cB"><%=lang_text(1)%></p> -->
		<div class="contBody noticeTbl">
			
			<div class="selectList">
			<p><a href="#none"><%=select_name%></a></p>
				<ul class="hide">
					<li><a href="?type=all">ALL</a></li>
					<li><a href="?type=0">IFC SEOUL</a></li>
					<li><a href="?type=1">IFC MALL</a></li>
					<li><a href="?type=2">CONRAD SEOUL</a></li>
				</ul>
			</div>

			<% if(current_type = "ko") then %>
			<div class="noticeList3">
				<ul>
					<% If objRs.RecordCount = 0 Then %>
					<li style="height: 100px; margin-bottom: 20px; line-height: 100px; text-align: center; font-size: 1.5em;">
						<%=lang_text(2)%>
					</li>
					<% End If %>
					<% For k = 0 to objRs.RecordCount - 1 %>
					<li>
						<div class="thumImg"><a href="#none" class="show"><img src="<%=objRs("e_img")%>" width="235" height="121" alt="" /></a></div>
						<div class="eventInfo">
							
							<% if state_array(k)="예정" then %>
							<div class="state soon">SOON</div>
							
							<%elseif state_array(k)="진행중" then%>
							<div class="state ing">ONGOING</div>
							
							<%elseif state_array(k)="종료일" then%>
							<div class="state end">END</div>
							<%end if%>
							
							<strong class="title"><a href="#none" class="show"><%=objRs("e_title")%></a></strong>
							<span class="spon type1"><%=group_array(objRs("e_state"))%></span>
							<span class="date">Period: <%=objRs("e_st_date")%> ~ <%=objRs("e_ed_date")%></span>
						</div>
						<div class="detailBtn"><a href="#none" class="btn show">Detail<span></span></a></div>
						<div class="eventVeiw">
							<p class="txtC"><%=objRs("e_text")%></p>
							<!-- div class="btnArea mt20"><a href="#" class="btn">이벤트 참여하기</a></div -->
							<div class="closeBtn"><a href="#" class="btn show">닫기<span></span></a></div>
						</div>
						
					</li>
					<% objRs.MoveNext %>
					<% Next %>
				</ul>
				
				<div class="paginate">
					<!-- #include file = "main_page_template.asp" -->
				</div>
			</div>

			<% else %>
			<table border="1" summary="<%=lang_text(6)%>" class="tblType noticeList1">
				<caption><%=lang_text(7)%></caption>
				<colgroup>
					<col />
					<col />
					<col style="width:21%" />
				</colgroup>
				<thead>
				<tr>
					<th scope="col" class="number"><%=lang_text(3)%></th>
					<th scope="col"><%=lang_text(4)%></th>
					<th scope="col"><%=lang_text(5)%></th>
				</tr>
				</thead>
				<tbody>
				<% if objRs.EOF Then %>
				<tr>
					<td colspan="3" align="center">
						<%=lang_text(2)%>
					</td>
				</tr>
				<% Else %>
				<% For k = 0 to objRs.RecordCount - 1 %>
				<tr>
					<td class="number"><%=num_count-k%></td>
					<td class="txtL"><a href="NE_03_01.asp?num=<%=objRs("e_num")%>&list_num=<%=objRs.RecordCount-k%>"><%=objRs("e_title")%></a></td>
					<td><%=objRs("e_st_date") %> ~ <%=objRs("e_ed_date") %></td>
				</tr>
				<% objRs.MoveNext %>
				<% Next %>
				<% End If %>
				</tbody>
			</table>

			<div class="paginate">
				<!-- #include file = "main_page_template.asp" -->
			</div>
			<% end if %>

			<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
		</div>
	</div>
</div>

