<script type="text/javascript" src="https://openapi.map.naver.com/openapi/v3/maps.js?ncpClientId=97encl097n"></script>

<div id="content">
	<div class="contWrap">
		<div class="titleWrap pSh" id="contTitle">
			<h2><span><%=lang_text(0)%></span></h2>
		</div>
		<!-- <p class="subTitle cB"><%=lang_text(1)%></p> -->
		<div class="contBody mapsCont">
			<ul class="tabs type1 type2">
				<li class="tc-tab tc-selected"><h3><%=lang_text(43)%></h3></li>
				<li class="tc-tab bgc6"><h3><%=lang_text(18)%></h3></li>
				<li class="tc-tab last"><h3><%=lang_text(21)%></h3></li>
			</ul>

			<div class="panels">
				<div class="tc-panel tc-selected">
					<h3 class="titleBar"><%=lang_text(43)%></h3>
					<!-- <div class="mapsList mt25"><img src="../img/com/img_location_map07_<%=current_type%>.png" <%=lang_text(41)%> /></div> -->
                    <div id="ifcmall_naver_map" style="width:100%; height:510px;">
                        <input id="ifcmall_address" type="hidden" value="<%=lang_text(42)%>"/>
                    </div>
					<!--  <p class="mt25"><%=lang_text(42)%></p> -->
				</div>

				<div class="tc-panel">
					<h3 class="titleBar"><%=lang_text(18)%></h3>
					<ul class="mapsList">
						<li class="method1">
							<div class="imgArea"><img src="../img/com/img_location_map01_<%=current_type%>.jpg" <%=lang_text(32)%> /></div>
							<h4><%=lang_text(19)%></h4>
							<ol>
								<li class="first"><span><span class="blind">1</span></span><%=lang_text(2)%></li>
								<li class="second"><span><span class="blind">2</span></span><%=lang_text(3)%></li>
								<li class="third"><span><span class="blind">3</span></span><%=lang_text(4)%></li>
							</ol>
						</li>
						<li class="method1">
							<div class="imgArea"><img src="../img/com/img_location_map02_<%=current_type%>.jpg" <%=lang_text(33)%> /></div>
							<h4><%=lang_text(20)%></h4>
							<ol>
								<li class="first"><span><span class="blind">1</span></span><%=lang_text(5)%></li>
								<li class="second"><span><span class="blind">2</span></span><%=lang_text(6)%></li>
								<li class="third"><span><span class="blind">3</span></span><%=lang_text(7)%></li>
							</ol>
						</li>
					</ul>
				</div>

				<div class="tc-panel">
					<h3 class="titleBar"><%=lang_text(21)%></h3>
					<ul class="mapsList">
						<li class="method2">
							<div class="imgArea"><img src="../img/com/img_location_map03_<%=current_type%>.jpg" <%=lang_text(34)%> /></div>
							<h4><%=lang_text(22)%></h4>
							<ol>
								<li class="first"><span><span class="blind">1</span></span><%=lang_text(8)%></li>
								<li class="second"><span><span class="blind">2</span></span><%=lang_text(9)%></li>
								<li class="third"><span><span class="blind">3</span></span><%=lang_text(10)%></li>
							</ol>
						</li>
						<li class="method3">
							<div class="imgArea"><img src="../img/com/img_location_map04_<%=current_type%>.jpg" <%=lang_text(35)%> /></div>
							<h4><%=lang_text(23)%></h4>
							<ol>
								<li class="first"><span><span class="blind">1</span></span><%=lang_text(11)%></li>
								<li class="second"><span><span class="blind">2</span></span><%=lang_text(12)%></li>
							</ol>
						</li>
					</ul>
					<h3 class="titleBar"><%=lang_text(13)%></h3>
					<ul class="busInfo">
						<li><%=lang_text(14)%></li>
						<li><%=lang_text(15)%></li>
						<li><%=lang_text(16)%></li>
						<li><%=lang_text(17)%></li>
					</ul>
					<div class="overflow_h">
						<div class="airportTbl fl">
							<h3 class="titleBar"><%=lang_text(24)%></h3>
							<table summary="<%=lang_text(38)%>" class="tblType2">
								<caption><%=lang_text(39)%></caption>
								<colgroup>
									<col style="width:20%;" />
									<col style="width:auto;" />
									<col style="width:20%;" />
									<col style="width:auto;" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="hour"><%=lang_text(25)%></th>
										<th scope="col"><%=lang_text(26)%></th>
										<th scope="col" class="hour"><%=lang_text(25)%></th>
										<th scope="col"><%=lang_text(26)%></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>06</td>
										<td>00, 40</td>
										<td class="borderL">07</td>
										<td>05, 30, 57</td>
									</tr>
									<tr>
										<td>08</td>
										<td>25, 50</td>
										<td class="borderL">09</td>
										<td>17, 40</td>
									</tr>
									<tr>
										<td>10</td>
										<td>00, 27, 52</td>
										<td class="borderL">11</td>
										<td>20, 43</td>
									</tr>
									<tr>
										<td>12</td>
										<td>08, 32</td>
										<td class="borderL">13</td>
										<td>00, 27, 53</td>
									</tr>
									<tr>
										<td>14</td>
										<td>17, 40</td>
										<td class="borderL">15</td>
										<td>04, 30, 53</td>
									</tr>
									<tr>
										<td>16</td>
										<td>20, 50</td>
										<td class="borderL">17</td>
										<td>20, 44</td>
									</tr>
									<tr>
										<td>18</td>
										<td>12, 38</td>
										<td class="borderL">19</td>
										<td>00, 27, 53</td>
									</tr>
									<tr>
										<td>20</td>
										<td>17, 42</td>
										<td class="borderL">21</td>
										<td>10, 35</td>
									</tr>
									<tr>
										<td>22</td>
										<td>05, 35</td>
										<td class="borderL">23</td>
										<td>00</td>
									</tr>
								</tbody>
							</table>
							<p><%=lang_text(27)%></p>
						</div>

						<div class="airportTbl fr">
							<h3 class="titleBar"><%=lang_text(28)%></h3>
							<table summary="<%=lang_text(39)%>" class="tblType2">
								<caption><%=lang_text(40)%></caption>
								<colgroup>
									<col style="width:20%;" />
									<col style="width:auto;" />
									<col style="width:20%;" />
									<col style="width:auto;" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="hour"><%=lang_text(25)%></th>
										<th scope="col"><%=lang_text(26)%></th>
										<th scope="col" class="hour"><%=lang_text(25)%></th>
										<th scope="col"><%=lang_text(26)%></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>05</td>
										<td>20, 40</td>
										<td class="borderL">06</td>
										<td>00, 20, 40</td>
									</tr>
									<tr>
										<td>07</td>
										<td>00, 20, 40</td>
										<td class="borderL">08</td>
										<td>05, 30, 55</td>
									</tr>
									<tr>
										<td>09</td>
										<td>20, 45</td>
										<td class="borderL">10</td>
										<td>10, 35</td>
									</tr>
									<tr>
										<td>11</td>
										<td>00, 25, 50</td>
										<td class="borderL">12</td>
										<td>15, 40</td>
									</tr>
									<tr>
										<td>13</td>
										<td>05, 30, 55</td>
										<td class="borderL">14</td>
										<td>20, 45</td>
									</tr>
									<tr>
										<td>15</td>
										<td>10, 35</td>
										<td class="borderL">16</td>
										<td>00, 25, 50</td>
									</tr>
									<tr>
										<td>17</td>
										<td>15, 40</td>
										<td class="borderL">18</td>
										<td>05, 30</td>
									</tr>
									<tr>
										<td>19</td>
										<td>00, 30</td>
										<td class="borderL">20</td>
										<td>00, 25, 50</td>
									</tr>
									<tr>
										<td>21</td>
										<td>20</td>
										<td class="borderL" colspan="2">&nbsp;</td>
									</tr>
								</tbody>
							</table>
							<p><%=lang_text(29)%></p>
						</div>
					</div>

					<h3 class="titleBar"><%=lang_text(30)%></h3>
					<ul class="mapsList">
						<li>
							<div class="imgArea"><img src="../img/com/img_location_map05_<%=current_type%>.jpg" alt="<%=lang_text(36)%>" /></div>
						</li>
					</ul>
					<h3 class="titleBar"><%=lang_text(31)%></h3>
					<ul class="mapsList">
						<li>
							<div class="imgArea"><img src="../img/com/img_location_map06.jpg" alt="<%=lang_text(37)%>" /></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>
