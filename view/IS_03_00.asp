<div id="content">
	<div class="contWrap">
		<div class="titleWrap bgIs0300 pSh" id="contTitle">
			<h2><a href="#none"><%=lang_text(0)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(0)%></dt>
				<dd><a href="IS_01_00.asp"><%=gnb_text(2)%></a></dd>
				<dd><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<!--<dd class="on"><a href="IS_03_00.asp"><%=gnb_text(8)%></a></dd>-->
				<dd><a href="IS_04_00.asp"><%=gnb_text(9)%></a></dd>
				<dd><a href="IS_05_00.asp"><%=gnb_text(10)%></a></dd>
				<dd><a href="IS_06_00.asp"><%=gnb_text(11)%></a></dd>
				<dd><a href="IS_07_01.asp"><%=gnb_text(12)%></a></dd>
			</dl>
		</div>
		<p class="subTitle cB"><%=lang_text(1)%></p>
		<div class="contBody">
			<ul class="listType9">
				<li>
					<h3><%=lang_text(2)%></h3>
					<div class="imgArea">
						<img src="../img/com/img_developer1.jpg" alt="<%=lang_text(13)%>" />
						<span class="bgBox9">&nbsp;</span>
					</div>
					<div class="txtCont">
						<p><%=lang_text(3)%></p>
						<!-- <p><%=lang_text(4)%></p> -->
						<!-- <p><%=lang_text(5)%></p>
						<p><%=lang_text(6)%></p>
						<p><%=lang_text(7)%></p>
						<p><%=lang_text(8)%></p>
						<p><%=lang_text(14)%></p> -->
					</div>
					<div class="linkBtn link3">
						<!-- <a href="/download/aig_logo.zip" target="_blank" class="btn white down">AIG LOGO DOWNLOAD</a>  --><a href="http://www.aig.com" class="btn white" target="_blank" title="Open New Window">GO TO AIG WEB SITE</a>
					</div>
				</li>
				<li>
					<h3 class="mt25"><%=lang_text(9)%></h3>
					<div class="imgArea">
						<img src="../img/com/img_developer2.jpg" alt="" />
						<span class="bgBox9">&nbsp;</span>
					</div>
					<div class="txtCont">
						<p><%=lang_text(10)%></p>
						<p><%=lang_text(11)%></p>
						<!-- <p><%=lang_text(12)%></p> -->
					</div>
					<div class="linkBtn link1">
						<a href="http://www.aig.com/real-estate_3171_523524.html" class="btn white" target="_blank" title="Open New Window">GO TO AIG GRE WEB SITE</a>
					</div>
				</li>
				</li>
			</ul>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>