

<div id="content">
	<div class="contWrap">
		<div class="titleWrap bgIs0500 pSh" id="contTitle">
			<h2><a href="#none"><%=lang_text(0)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(0)%></dt>
				<dd><a href="IS_01_00.asp"><%=gnb_text(2)%></a></dd>
				<dd><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<!--<dd><a href="IS_03_00.asp"><%=gnb_text(8)%></a></dd>-->
				<dd><a href="IS_04_00.asp"><%=gnb_text(9)%></a></dd>
				<dd class="on"><a href="IS_05_00.asp"><%=gnb_text(10)%></a></dd>
				<dd><a href="IS_06_00.asp"><%=gnb_text(11)%></a></dd>
				<dd><a href="IS_07_01.asp"><%=gnb_text(12)%></a></dd>
			</dl>
		</div>
		<p class="subTitle cB"><%=lang_text(1)%></p>
		<div class="contBody">
			<div class="historySlides">
                <div class="historyTabsWrap">
                    <div class="historyTabs">
                        <div id="bx-pager">
                            <a data-slide-index="0" class="n1" href="#none"> ~1950</a>
                            <a data-slide-index="1" class="n2" href="#none">1960~1970</a>
                            <a data-slide-index="2" class="n3" href="#none">1980~1990</a>
                            <a data-slide-index="3" class="n4" href="#none">2000~ </a>
                        </div>
                    </div>
                </div>

				<ul class="historyCont locationHistory sliderhistory">
					<li class="slideCont" id="historyArea1">
						<h3><%=lang_text(2)%></h3>
						<p><%=lang_text(3)%></p>
						<p class="mt10"><%=lang_text(4)%></p>
						<p class="mt10"><%=lang_text(5)%></p>
					</li>
					<li class="slideCont" id="historyArea2">
						<h3><%=lang_text(6)%></h3>
						<p class="other"><%=lang_text(7)%></p>
						<p class="mt10"><%=lang_text(8)%></p>
						<p class="mt10"><%=lang_text(9)%></p>
						<p class="mt10"><%=lang_text(10)%></p>
						<!-- <p class="mt10"><%=lang_text(11)%></p>
						<p class="mt10"><%=lang_text(12)%></p>
						<p class="mt10"><%=lang_text(13)%></p> -->
					</li>
					<li class="slideCont" id="historyArea3">
						<h3><%=lang_text(14)%></h3>
						<p><%=lang_text(15)%></p>
						<p class="mt10"><%=lang_text(16)%></p>
						<p class="mt10"><%=lang_text(17)%></p>
						<p class="mt10"><%=lang_text(18)%></p>
					</li>
					<li class="slideCont" id="historyArea4">
						<h3><%=lang_text(19)%></h3>
						<p><%=lang_text(20)%></p>
						<dl>
							<dt class="mt20"><strong><%=lang_text(21)%></strong></dt>
							<dd class="mt10"><%=lang_text(22)%></dd>
							<dt class="mt20"><strong><%=lang_text(23)%></strong></dt>
							<dd class="mt10"><%=lang_text(24)%></dd>
							<dt class="mt20"><strong><%=lang_text(25)%></strong></dt>
							<dd class="mt10"><%=lang_text(26)%></dd>
							<dt class="mt20"><strong><%=lang_text(27)%></strong></dt>
							<dd class="mt10"><%=lang_text(28)%></dd>
							<dt class="mt20"><strong><%=lang_text(29)%></strong></dt>
							<dd class="mt10"><%=lang_text(30)%></dd>
						</dl>
					</li>
				</ul>

			</div>
            <!--<div class="dotControl jsBtnWrap">
                <ul>
                    <li class="on"><a href="#"><span class="blind">first</span></a></li>
                    <li><a href="#"><span class="blind">second</span></a></li>
                    <li><a href="#"><span class="blind">third</span></a></li>
                    <li><a href="#"><span class="blind">fourth</span></a></li>
                </ul>
            </div>

			<div class="slideControl">
				<a href="#" class="prev jsBtnPrev"><img src="../../img/com/btn_arrow_prev2.png" alt="prev" /></a>
				<a href="#" class="next jsBtnNext"><img src="../../img/com/btn_arrow_next2.png" alt="next" /></a>
			</div>-->
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function () {
	$('.sliderhistory').bxSlider({
		infiniteLoop: false,
		hideControlOnEnd: true,
		touchEnabled: false,
		pagerCustom: '#bx-pager'
	});
});
</script>