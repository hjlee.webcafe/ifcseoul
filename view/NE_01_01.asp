<%
Dim num,list_num,selectQuery,imgRs,objRs,objConn,prevRs,nextRs
num = Request.QueryString("num")
list_num = Request.QueryString("list_num")

set objConn = OpenDBConnection()

if(current_type = "ko") then
query_where = "and n_group='0'"
else
query_where = "and n_group='1'"
end if

selectQuery="select * from dbo.tblNotice where n_num='"& num &"'"&query_where
set objRs = SendQuery(objConn,selectQuery)

// get image list
selectQuery = "select * from dbo.tblNotice_img where n_num = '"& num &"'"
set imgRs = SendQuery(objConn,selectQuery)

//Prev
selectQuery = "select TOP 1 * from dbo.tblNotice where n_num < '"& num &"'" &query_where& "order by n_num desc"
set prevRs = SendQuery(objConn,selectQuery)

//Next
selectQuery = "select TOP 1 * from dbo.tblNotice where n_num > '"& num &"'" &query_where& " order by n_num asc"
set nextRs = SendQuery(objConn,selectQuery)

%>
<div id="content">
	<div class="contWrap">
		<div class="titleWrap titleNe0101 pSh" id="contTitle">
			<h2><a href="#none"><%=lang_text(0)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(36)%></dt>
				<dd class="on"><a href="NE_01_00.asp"><%=gnb_text(37)%></a></dd>
				<dd><a href="NE_02_00.asp"><%=gnb_text(38)%></a></dd>
				<dd><a href="NE_03_00.asp"><%=gnb_text(39)%></a></dd>
			</dl>
		</div>
		<!-- <p class="subTitle cB"><%=lang_text(1)%></p> -->
		<div class="contBody noticeTbl">
			<table summary="<%=lang_text(5)%>" class="tblType noticeView1">
				<caption><%=lang_text(6)%></caption>
				<colgroup>
					<col />
					<col />
					<col style="width:21%" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col" class="number"><%=lang_text(2)%></th>
						<th scope="col"><%=lang_text(3)%></th>
						<th scope="col"><%=lang_text(4)%></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="number"><%=list_num%></td>
						<td class="txtL"><%=objRs("n_title")%></td>
						<td><%=Left(objRs("n_reg_date"), 10)%></td>
					</tr>
					<tr class="veiw">
						<td colspan="3" class="pl20">
							<p class="mt20"><%=objRs("n_text")%></p>
						</td>
					</tr>
				</tbody>
			</table>
			<ul class="veiwLoop">
				<%if nextRs("n_num")="" then %>
				<%else%>
				<li class="next"><span>Next</span><a href="NE_01_01.asp?num=<%=nextRs("n_num")%>&list_num=<%=list_num+1%>" class="title"><%=nextRs("n_title")%></a></li>
				<%end if%>
				<%if prevRs("n_num")="" then %>
				<%else%>
				<li class="prev"><span>Prev</span><a href="NE_01_01.asp?num=<%=prevRs("n_num")%>&list_num=<%=list_num-1%>" class="title"><%=prevRs("n_title")%></a></li>
				<%end if%>
			</ul>
			<div class="btnArea"><a href="NE_01_00.asp" class="btn">View the list</a></div>
		</div>
	</div>
</div>