<!-- paging section -->	
<%
'(ispara, $prev_para, $prev_page, intval($start_page), intval($last_page), $next_page, $next_para, page_para, $total_page);
'(p_prev_page, p_start_page, p_last_page, p_next_page, p_total_page)
'$search_link for search link
%>
<div class="page_template">
	<div style="margin: 0 auto">
		<ul class="paginate">
			<% If Cint(page) <> 1 Then %>
			<li>
				<a class="preEnd" href="?page=1<%=search_link%>">&laquo;</a>
			</li>
			<% End If %>
			<% If Not page_result(0) = "" Then %>
			<li>
				<a class="pre" href="?page=<%=page_result(0)%><%=search_link%>"></a>
			</li>
			<% End If %>
			<% For i = page_result(1) To page_result(2) %>
			<li>
				<% If Cint(i) = Cint(page) Then %>
				<a href="#"><strong><%=i%></strong></a>
				<% Else  %>
				<a href="?page=<%=i%><%=search_link%>"><%=i%></a>
				<% End If %>
			</li>
			<% Next %>
			<% If Not page_result(3) = "" Then %>
			<li>
				<a class="next" href="?page=<%=page_result(3)%><%=search_link%>"></a>
			</li>
			<% End If %>
			<%if Cint(page) <> Cint(page_result(4)) Then%>
			<li class="nextEnd">
				<a href="?page=<%=page_result(4)%><%=search_link%>">&raquo;</a>
			</li>
			<% End If %>
		</ul>
	</div>
</div>
<!--  paging section end -->