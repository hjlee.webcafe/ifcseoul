<div id="content">
	<div class="contWrap">
		<div class="titleWrap contBg bgIs0100" id="contTitle">
			<h2><a href="#none"><%=lang_text(0)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(0)%></dt>
				<dd class="on"><a href="IS_01_00.asp"><%=gnb_text(2)%></a></dd>
				<dd><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<!--<dd><a href="IS_03_00.asp"><%=gnb_text(8)%></a></dd>-->
				<dd><a href="IS_04_00.asp"><%=gnb_text(9)%></a></dd>
				<dd><a href="IS_05_00.asp"><%=gnb_text(10)%></a></dd>
				<dd><a href="IS_06_00.asp"><%=gnb_text(11)%></a></dd>
				<dd><a href="IS_07_01.asp"><%=gnb_text(12)%></a></dd>
			</dl>
		</div>
		<!-- <p class="subTitle"><%=lang_text(1)%></p> -->
		<div class="contVisual bgIs0100">
			<div class="introTitle">
				<p><%=lang_text(2)%></p>
				<p class="introTxt"><%=lang_text(3)%></p>
			</div>
		</div>
		<div class="contBody">
			<div class="txtCont">
				<p><%=lang_text(4)%></p>
				<p><%=lang_text(5)%></p>
				<p><%=lang_text(6)%></p>
			</div>
			<!-- <div class="areaCont">
				<ul>
					<li class="repeat"><strong>Area of land</strong>33,058 m&sup2; (10,000 pyeong)<span class="icon repeat"><span class="blind">repeat</span></span></li>
					<li class="crop"><strong>Gross area</strong>307,402 m&sup2; above ground /<br /> 197,834 m&sup2; underground<br />(Gross area: 505,236 m&sup2;)<span class="icon crop"><span class="blind">crop</span></span></li>
				</ul>
			</div>
			<div class="buildingsCont">
				<h4>Buildings</h4>
				<div class="buildingsImg"><img src="../img/com/img_building_map.jpg" alt="IFC Seoul Building Plan Image" /></div>
				<div class="buildingsTbl">
					<table summary="One IFC, Two IFC, Three IFC" class="tblType3">
						<caption></caption>
						<colgroup>
							<col style="width:25%;" />
							<col style="width:25%;" />
							<col style="width:25%;" />
							<col style="width:25%;" />
						</colgroup>
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th scope="col">One IFC<br />(32 floors)</th>
								<th scope="col">Two IFC<br />(29 floors)</th>
								<th scope="col">Three IFC<br />(55 floors)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">Gross Area (m&sup2;)</th>
								<td>70,140</td>
								<td>63,253</td>
								<td>130,944</td>
							</tr>
							<tr>
								<th scope="row">Height(m)</th>
								<td>184.5</td>
								<td>175.5</td>
								<td>284.0</td>
							</tr>
							<tr>
								<th scope="row">Completion</th>
								<td>Oct 2011</td>
								<td>Q4, 2012</td>
								<td>Q4, 2012</td>
							</tr>
						</tbody>
					</table>
					<table summary="Conrad Seoul, IFC Mall, Underground" class="tblType3 mtm1">
						<caption></caption>
						<colgroup>
							<col style="width:25%;" />
							<col style="width:25%;" />
							<col style="width:25%;" />
							<col style="width:25%;" />
						</colgroup>
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th scope="col">Conrad Seoul<br />Hotel (38 floors)</th>
								<th scope="col">IFC Mall<br />(3 underground<br />floors)</th>
								<th scope="col">Underground<br />parking lot<br />(4~7 floors)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">Gross Area (m&sup2;)</th>
								<td>77,878</td>
								<td>76,021</td>
								<td>87,000</td>
							</tr>
							<tr>
								<th scope="row">Height(m)</th>
								<td>199.4</td>
								<td>-</td>
								<td>2,046 lots</td>
							</tr>
							<tr>
								<th scope="row">Completion</th>
								<td>Q4, 2012</td>
								<td>Aug 2012</td>
								<td>Oct 2011</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="buildingsTbl">
					<table summary="Conrad Seoul, IFC Mall, Underground" class="tblType3">
						<caption></caption>
						<colgroup>
							<col style="width:100%;" />
						</colgroup>
						<thead>
							<tr>
								<th scope="col" class="bgc4">Underground walkway</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									Area (m&sup2;) :5,725 / Length (m) : 363 / Max Width (m) : 16
									<p>Underground walkway will revert back to the ownership of The SMG after completion</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div> -->
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>