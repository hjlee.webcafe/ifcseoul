
<div id="content">
	<div class="contWrap">
		<div class="titleWrap pSh" id="contTitle">
			<h2><span><%=gnb_text(48)%></span></h2>
		</div>
		<!-- <p class="subTitle cB"><%=lang_text(0)%></p> -->
		<div class="contBody siteMapCont">
			<div class="siteMap1">
				<h3>IFC Seoul</h3>
				<ul>
					<li><a href="IS_01_00.asp">Introduction</a></li>
					<li><a href="IS_02_01.asp">IFC Seoul Project
						<ul class="depth2">
							<li><a href="IS_02_01.asp">Overview</a></li>
							<li><a href="IS_02_02.asp">Significances &amp; Vision</a></li>
							<li><a href="IS_02_03.asp">Features</a></li>
							<li><a href="IS_02_04.asp">History</a></li>
						</ul>
					</a></li>
					<!-- <li><a href="IS_03_00.asp">The Developer</a></li> -->
					<li><a href="IS_04_00.asp">The Project Team</a></li>
					<li><a href="IS_05_00.asp">Location History</a></li>
					<li><a href="IS_06_00.asp">Award</a></li>
					<li><a href="IS_07_01.asp">Gallery</a>
						<ul class="depth2">
							<li><a href="IS_07_01.asp">IFC Seoul Gallery</a></li>
							<li><a href="IS_07_02.asp">Digital Brochure</a></li>
							<li><a href="IS_07_03.asp">IFC Seoul Film</a></li>
							<li><a href="IS_07_04.asp">Flying IFC Seoul</a></li>
							<li><a href="IS_07_05.asp">Panorama View</a></li>
						</ul>
					</li>
				</ul>
				<div class="symbolImg"><span class="blind">IFC Seoul</span></div>
			</div>
			<div class="siteMap2">
				<h3>Buildings</h3>
				<ul>
					<li><a href="BD_01_00.asp">Overview</a></li>
					<li><a href="BD_02_00.asp">Concept</a></li>
					<li><a href="BD_03_00.asp">Features</a></li>
					<li><a href="BD_04_00.asp">Floor Plans</a></li>
					<li><a href="BD_05_00.asp">Specifications</a></li>
					<!--li><a href="BD_06_00.asp">Tenants Testimonial</a></li-->
					<li><a href="BD_07_01.asp">Amenities</a>
						<ul class="depth2">
							<li><a href="BD_07_01.asp">CONRAD Seoul</a></li>
							<li><a href="BD_07_02.asp">IFC Mall</a></li>
							<li><a href="BD_07_03.asp">Artworks</a></li>
							<li><a href="BD_07_04.asp">Service</a></li>
							<li><a href="BD_07_05.asp">Parking</a></li>
							<li><a href="BD_07_07.asp">Gallery</a></li>
						</ul>
					</li>
					<li><a href="BD_08_01.asp">For Lease</a>
						<ul class="depth2">
							<li><a href="BD_08_01.asp">The New Opportunity</a></li>
							<li><a href="BD_08_02.asp">Office Leasing</a></li>
							<li><a href="BD_08_03.asp">Retail Leasing</a></li>
						</ul>
					</li>
				</ul>
				<div class="symbolImg"><span class="blind">Buildings</span></div>
			</div>
			<div class="siteMap3">
				<h3>Neighborhood Maps</h3>
				<ul>
					<li><a href="NM_01_00.asp">Neighborhood Maps</a></li>
				</ul>
				<div class="symbolImg"><span class="blind">Neighborhood Maps</span></div>
			</div>
			<div class="siteMap4">
				<h3>News &amp; Events</h3>
				<ul>
					<li><a href="NE_01_00.asp">Notice</a></li>
					<li><a href="NE_02_00.asp">Press Releases</a></li>
					<li><a href="NE_03_00.asp">Events</a></li>
				</ul>
				<div class="symbolImg"><span class="blind">News &amp; Events</span></div>
			</div>
			<div class="siteMap5">
				<h3>Etc.</h3>
				<ul>
					<li><a href="HF_01_00.asp">Location  &amp; Maps</a></li>
					<li><a href="HF_02_00.asp">Contact</a></li>
					<li><a href="HF_06_00.asp">Real Estate Notice</a></li>
				</ul>
				<div class="symbolImg"><span class="blind">Etc.</span></div>
			</div>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>

