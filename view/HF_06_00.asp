<div id="content">
	<div class="contWrap">
		<div class="titleWrap pSh" id="contTitle">
			<h2><span><%=lang_text(0)%></span></h2>
		</div>
		<p class="subTitle cB"><%=lang_text(1)%></p>
		<div class="contBody legalNotice bg_none">
			<dl class="lealEstateNotice">
				<dt><%=lang_text(2)%></dt>
				<dd><%=lang_text(3)%></dd>
				<dt><%=lang_text(4)%></dt>
				<dd><%=lang_text(5)%></dd>
				<dt><%=lang_text(6)%></dt>
				<dd><%=lang_text(7)%></dd>
				<dt><%=lang_text(8)%></dt>
				<dd><%=lang_text(9)%></dd>
				<dt><%=lang_text(10)%></dt>
				<dd><%=lang_text(11)%></dd>
				<dt class="last"><%=lang_text(12)%></dt>
				<dd class="last"><%=lang_text(13)%></dd>
			</dl>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>