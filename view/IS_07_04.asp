<script type="text/javascript" src="/js/video.js"></script>
<link rel="stylesheet" type="text/css" href="/css/video-js.css">
<div id="content">
	<div class="contWrap">
		<div class="tabWrap type4">
			<ul>
				<li><a href="IS_07_01.asp"><%=gnb_text(13)%></a></li>
				<li class="other"><a href="IS_07_06.asp"><%=gnb_text(53)%></a></li>
				<li><a href="IS_07_02.asp"><%=gnb_text(14)%></a></li>
				<li class="other"><a href="IS_07_03.asp"><%=gnb_text(15)%></a></li>
				<li class="on"><a href="IS_07_04.asp"><%=gnb_text(51)%></a></li>
				<li class="other"><a href="IS_07_05.asp"><%=gnb_text(52)%></a></li>
			</ul>
		</div>
		<div class="titleWrap titleIs0703 pSh" id="contTitle">
			<h2><a href="#none">Gallery</a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(0)%></dt>
				<dd><a href="IS_01_00.asp"><%=gnb_text(2)%></a></dd>
				<dd><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<!--<dd><a href="IS_03_00.asp"><%=gnb_text(8)%></a></dd>-->
				<dd><a href="IS_04_00.asp"><%=gnb_text(9)%></a></dd>
				<dd><a href="IS_05_00.asp"><%=gnb_text(10)%></a></dd>
				<dd><a href="IS_06_00.asp"><%=gnb_text(11)%></a></dd>
				<dd class="on"><a href="IS_07_01.asp"><%=gnb_text(12)%></a></dd>
			</dl>
			<h3><a href="#none"><%=lang_text(0)%></a></h3>
			<dl class="h3Siblings">
				<dt>Gallery</dt>
				<dd><a href="IS_07_01.asp"><%=gnb_text(13)%></a></dd>
				<dd><a href="IS_07_02.asp"><%=gnb_text(14)%></a></dd>
				<dd><a href="IS_07_03.asp"><%=gnb_text(15)%></a></dd>
				<dd class="on"><a href="IS_07_04.asp"><%=gnb_text(51)%></a></dd>
				<dd><a href="IS_07_05.asp"><%=gnb_text(52)%></a></dd>
				<dd><a href="IS_07_06.asp"><%=gnb_text(53)%></a></dd>
			</dl>
		</div>
		<p class="subTitle cB"><%=lang_text(1)%></p>
		<div class="contBody videoCont">
			<div class="videoWrap">
				<video id="ad_movie" class="video-js vjs-default-skin"
					controls preload="auto" width="100%" height="720"
					poster="" data-setup='{"techOrder" : ["html5","flash","other supported tech"]}'>
					<source src="/movie/IFCMall_Master_003.mp4" type='video/mp4' />
				</video>
			</div>
			<!-- <ul class="downBtn">
				<li><a href="#" class="btn white"><%=lang_text(2)%> #01</a> <a href="#" class="btn download"><span class="blind">Download</span></a></li>
				<li><a href="#" class="btn white"><%=lang_text(2)%> #02</a> <a href="#" class="btn download"><span class="blind">Download</span></a></li>
			</ul> -->
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>
