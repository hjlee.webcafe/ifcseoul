<div id="content">
	<div class="contWrap">
		<div class="tabWrap">
			<ul>
				<li class="on"><a href="IS_02_01.asp"><%=gnb_text(4)%></a></li>
				<li class="other"><a href="IS_02_02.asp"><%=gnb_text(5)%></a></li>
				<li><a href="IS_02_03.asp"><%=gnb_text(6)%></a></li>
				<li class="other"><a href="IS_02_04.asp"><%=gnb_text(7)%></a></li>
			</ul>
		</div>
		<div class="titleWrap titleIs0201 " id="contTitle">
			<h2><a href="#none"><%=gnb_text(4)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(0)%></dt>
				<dd><a href="IS_01_00.asp"><%=gnb_text(2)%></a></dd>
				<dd class="on"><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<!-- <dd><a href="IS_03_00.asp"><%=gnb_text(8)%></a></dd> -->
				<dd><a href="IS_04_00.asp"><%=gnb_text(9)%></a></dd>
				<dd><a href="IS_05_00.asp"><%=gnb_text(10)%></a></dd>
				<dd><a href="IS_06_00.asp"><%=gnb_text(11)%></a></dd>
				<dd><a href="IS_07_01.asp"><%=gnb_text(12)%></a></dd>
			</dl>
			<h3><a href="#none"><%=lang_text(0)%></a></h3>
			<dl class="h3Siblings">
				<dt>IFC Seoul Project</dt>
				<dd class="on"><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<dd><a href="IS_02_02.asp"><%=gnb_text(5)%></a></dd>
				<dd><a href="IS_02_03.asp"><%=gnb_text(6)%></a></dd>
				<dd><a href="IS_02_04.asp"><%=gnb_text(7)%></a></dd>
			</dl>
		</div>
		<!-- <p class="subTitle"><%=lang_text(1)%></p> -->
		<div class="contVisual bgIs0201">
			<div class="introTitle">
				<p><%=lang_text(2)%></p>
				<p class="introTxt"><%=lang_text(3)%></p>
			</div>
		</div>
		<div class="contBody">
			<div class="txtCont">
				<p><%=lang_text(4)%></p>
				<!-- <p><%=lang_text(5)%></p>
				<p><%=lang_text(6)%></p> -->
			</div>
			<!-- <div class="areaCont">
				<ul>
					<li class="repeat"><%=lang_text(7)%><span class="icon repeat"><span class="blind">repeat</span></span></li>
					<li class="crop"><%=lang_text(8)%><span class="icon crop"><span class="blind">crop</span></span></li>
				</ul>
			</div> -->
			<div class="buildingsCont">
				<h4><%=lang_text(9)%></h4>
				<div class="buildingsImg"><img src="../../img/com/img_building_map.jpg" alt="IFC Seoul Building Plan Image" /></div>
				<div class="buildingsTbl">
					<table summary="<%=lang_text(30)%>" class="tblType3">
						<caption></caption>
						<colgroup>
							<col style="width:25%;" />
							<col style="width:25%;" />
							<col style="width:25%;" />
							<col style="width:25%;" />
						</colgroup>
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th scope="col"><%=lang_text(10)%></th>
								<th scope="col"><%=lang_text(11)%></th>
								<th scope="col"><%=lang_text(12)%></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row"><%=lang_text(13)%></th>
								<td>90,064</td>
								<td>78,095</td>
								<td>161,429</td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(14)%></th>
								<td>184.5</td>
								<td>175.5</td>
								<td>284.0</td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(15)%></th>
								<td><%=lang_text(16)%></td>
								<td><%=lang_text(17)%></td>
								<td><%=lang_text(18)%></td>
							</tr>
						</tbody>
					</table>
					<table summary="Conrad Seoul, IFC Mall, Underground" class="tblType3 mtm1">
						<caption></caption>
						<colgroup>
							<col style="width:25%;" />
							<col style="width:25%;" />
							<col style="width:25%;" />
							<col style="width:25%;" />
						</colgroup>
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th scope="col"><%=lang_text(19)%></th>
								<th scope="col"><%=lang_text(20)%></th>
								<th scope="col"><%=lang_text(21)%></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row"><%=lang_text(22)%></th>
								<td>91,253</td>
								<td>85,366</td>
								<td>85,194</td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(23)%></th>
								<td>199.4</td>
								<td>-</td>
								<td>2,085 lots</td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(24)%></th>
								<td><%=lang_text(25)%></td>
								<td><%=lang_text(26)%></td>
								<td><%=lang_text(27)%></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- <div class="buildingsTbl">
					<table summary="Conrad Seoul, IFC Mall, Underground" class="tblType3">
						<caption></caption>
						<colgroup>
							<col style="width:100%;" />
						</colgroup>
						<thead>
							<tr>
								<th scope="col" class="bgc4"><%=lang_text(28)%></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<%=lang_text(29)%>
								</td>
							</tr>
						</tbody>
					</table>
				</div> -->
			</div>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>
