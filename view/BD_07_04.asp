<div id="content">
	<div class="contWrap">
		<div class="tabWrap type4">
			<ul>
				<li><a href="BD_07_01.asp"><%=gnb_text(24)%></a></li>
				<li class="other"><a href="BD_07_02.asp"><%=gnb_text(25)%></a></li>
				<li><a href="BD_07_03.asp"><%=gnb_text(26)%></a></li>
				<li class="other on"><a href="BD_07_04.asp"><%=gnb_text(27)%></a></li>
				<li class=""><a href="BD_07_05.asp"><%=gnb_text(28)%></a></li>
				<!-- li><a href="BD_07_06.asp"><%=gnb_text()%></a></li-->
				<li class="other"><a href="BD_07_07.asp"><%=gnb_text(29)%></a></li>
			</ul>
		</div>
		<div class="titleWrap titleBd0704 pSh" id="contTitle">
			<h2><a href="#none">Amenities</a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(12)%></dt>
				<dd><a href="BD_01_00.asp"><%=gnb_text(17)%></a></dd>
				<dd><a href="BD_02_00.asp"><%=gnb_text(18)%></a></dd>
				<dd><a href="BD_03_00.asp"><%=gnb_text(19)%></a></dd>
				<dd><a href="BD_04_00.asp"><%=gnb_text(20)%></a></dd>
				<dd><a href="BD_05_00.asp"><%=gnb_text(21)%></a></dd>
				<!--dd><a href="BD_06_00.asp"><%=gnb_text(22)%></a></dd-->
				<dd class="on"><a href="BD_07_01.asp"><%=gnb_text(23)%></a></dd>
				<!--<dd><a href="BD_08_01.asp"><%=gnb_text(30)%></a></dd>-->
			</dl>
			<h3><a href="#none"><%=lang_text(0)%></a></h3>
			<dl class="h3Siblings">
				<dt>Amenities</dt>
				<dd><a href="BD_07_01.asp"><%=gnb_text(24)%></a></dd>
				<dd><a href="BD_07_02.asp"><%=gnb_text(25)%></a></dd>
				<dd><a href="BD_07_03.asp"><%=gnb_text(26)%></a></dd>
				<dd class="on"><a href="BD_07_04.asp"><%=gnb_text(27)%></a></dd>
				<dd><a href="BD_07_05.asp"><%=gnb_text(28)%></a></dd>
				<!-- dd><a href="BD_07_06.asp"><%=gnb_text()%></a></dd-->
				<dd><a href="BD_07_07.asp"><%=gnb_text(29)%></a></dd>
			</dl>
		</div>
		<p class="subTitle cB"><%=lang_text(1)%></p>
		<div class="contBody">
			<ul class="serviceList">
				<li class="parking">
					<span class="simbol">&nbsp;</span>
					<dl>
						<dt class="titleBar"><%=lang_text(2)%></dt>
						<dd>
							<p><%=lang_text(3)%></p>
						</dd>
					</dl>
				</li>
				<li class="quickDesk">
					<span class="simbol">&nbsp;</span>
					<dl>
						<dt class="titleBar"><%=lang_text(8)%></dt>
						<dd>
							<p><%=lang_text(4)%></p>
						</dd>
					</dl>
				</li>
				<li class="bicycle">
					<span class="simbol">&nbsp;</span>
					<dl>
						<dt class="titleBar"><%=lang_text(5)%></dt>
						<dd>
							<p><%=lang_text(6)%></p>
						</dd>
					</dl>
				</li>
				<!-- <li class="cityCar">
					<span class="simbol">&nbsp;</span>
					<dl>
						<dt class="titleBar">City Car (Public use of Electric Motor Cars)</dt>
						<dd>
							<p><%=lang_text(7)%></p>
						</dd>
					</dl>
				</li> -->
			</ul>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>
