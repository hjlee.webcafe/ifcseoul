<div id="content">
	<div class="contWrap">
		<div class="titleWrap bgIs0400 pSh" id="contTitle">
			<h2><a href="#none"><%=lang_text(0)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(0)%></dt>
				<dd><a href="IS_01_00.asp"><%=gnb_text(2)%></a></dd>
				<dd><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<!--<dd><a href="IS_03_00.asp"><%=gnb_text(8)%></a></dd>-->
				<dd class="on"><a href="IS_04_00.asp"><%=gnb_text(9)%></a></dd>
				<dd><a href="IS_05_00.asp"><%=gnb_text(10)%></a></dd>
				<dd><a href="IS_06_00.asp"><%=gnb_text(11)%></a></dd>
				<dd><a href="IS_07_01.asp"><%=gnb_text(12)%></a></dd>
			</dl>
		</div>
		<p class="subTitle cB"><%=lang_text(1)%></p>
		<div class="contBody">
			<div class="partInfo">
				<div class="partArea1">
					<h3><%=lang_text(2)%></h3>
					<ul>
						<li><span>SIFC Tower One Development YH</span></li>
						<li><span>SIFC Tower Two Development YH</span></li>
						<li><span>SIFC Tower Three Development YH</span></li>
						<li><span>SIFC Hotel Development YH</span></li>
						<li><span>SIFC Retail Mall Development YH</span></li>
					</ul>
					<span class="gender ifc"><span class="blind">ifc</span></span>
				</div>
				<div class="partArea2">
					<h3><%=lang_text(3)%></h3>
					<p><%=lang_text(4)%></p>
				</div>
				<!-- <div class="partArea3">
					<h3><%=lang_text(5)%></h3>
					<p><%=lang_text(6)%></p>
				</div>
				<div class="partArea4">
					<h3><%=lang_text(7)%></h3>
					<p>Benoy</p>
				</div> -->
				<div class="partArea5">
					<ul>
						<li>
							<strong><%=lang_text(30)%></strong>
							<p class="txtC"><span><%=lang_text(31)%></span></p>
							<p><%=lang_text(32)%></p>
						</li>
						<li>
							<strong><%=lang_text(33)%></strong>
							<p class="txtC"><span><%=lang_text(34)%></span></p>
							<p><%=lang_text(35)%></p>
						</li>
						<li>
							<strong><%=lang_text(36)%></strong>
							<p class="txtC"><span><%=lang_text(37)%></span></p>
							<p><%=lang_text(38)%></p>
						</li>
					</ul>
				</div>
			</div>
			<div class="partInfoList">
				<ul>
					<li class="other">
						<strong><%=lang_text(39)%></strong>
						<p><%=lang_text(40)%></p>
					</li>
					<li>
						<strong><%=lang_text(8)%></strong>
						<p><%=lang_text(9)%></p>
					</li>
					<li>
						<strong><%=lang_text(10)%></strong>
						<p>Thornton Tomasetti Group</p>
					</li>
					<li>
						<strong><%=lang_text(11)%></strong>
						<p><%=lang_text(12)%></p>
					</li>
					<li>
						<strong><%=lang_text(13)%></strong>
						<p><%=lang_text(14)%></p>
					</li>
					<li>
						<strong><%=lang_text(15)%></strong>
						<p>C.G.E&amp;C CO.Ltd.</p>
					</li>
					<li>
						<strong><%=lang_text(16)%></strong>
						<p><%=lang_text(17)%></p>
					</li>
					<li>
						<strong>MEP Engineering</strong>
						<p>Woowon M&amp;E</p>
					</li>
					<li>
						<strong><%=lang_text(18)%></strong>
						<p><%=lang_text(19)%></p>
					</li>
					<li>
						<strong><%=lang_text(20)%></strong>
						<p><%=lang_text(21)%></p>
					</li>
					<li>
						<strong><%=lang_text(22)%></strong>
						<p><%=lang_text(23)%></p>
					</li>
					<li>
						<strong><%=lang_text(24)%></strong>
						<p>ERM Korea Limited</p>
					</li>
					<li>
						<strong><%=lang_text(25)%></strong>
						<p>Davis Langdon &amp; Seah Korea Co. Ltd.</p>
					</li>
					<li>
						<strong><%=lang_text(26)%></strong>
						<p>IBS Industry</p>
					</li>
					<li>
						<strong><%=lang_text(27)%></strong>
						<p>JAT Advisory</p>
					</li>
					<li>
						<strong><%=lang_text(28)%></strong>
						<p>SIFC Property Korea</p>
					</li>
					<li>
						<strong><%=lang_text(29)%></strong>
						<p>Hilton Worldwide(Conrad)</p>
					</li>
				</ul>
			</div>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>