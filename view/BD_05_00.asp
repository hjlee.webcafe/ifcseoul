<div id="content">
	<div class="contWrap">
		<div class="titleWrap titleBd0500" id="contTitle">
			<h2><a href="#none"><%=lang_text(0)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(12)%></dt>
				<dd><a href="BD_01_00.asp"><%=gnb_text(17)%></a></dd>
				<dd><a href="BD_02_00.asp"><%=gnb_text(18)%></a></dd>
				<dd><a href="BD_03_00.asp"><%=gnb_text(19)%></a></dd>
				<dd><a href="BD_04_00.asp"><%=gnb_text(20)%></a></dd>
				<dd class="on"><a href="BD_05_00.asp"><%=gnb_text(21)%></a></dd>
				<!--dd><a href="BD_06_00.asp"><%=gnb_text(22)%></a></dd-->
				<dd><a href="BD_07_01.asp"><%=gnb_text(23)%></a></dd>
				<!--<dd><a href="BD_08_01.asp"><%=gnb_text(30)%></a></dd>-->
			</dl>
		</div>
		<!-- <p class="subTitle"><%=lang_text(1)%></p> -->
		<div class="contVisual bgBd0500">
			<div class="introTitle">
				<p><%=lang_text(2)%></p>
				<p class="introTxt"><%=lang_text(3)%></p>
			</div>
		</div>
		<div class="contBody">
			<ul class="specificationsList">
				<li>10 ft floor to ceiling</li>
				<li>Access control &amp; alarm monitoring</li>
				<li class="other">15 ft slab to slab</li>
				<li class="other">CCTV &amp; intercom</li>
				<li>6 inch full access raised floor</li>
				<li>Low, mid &amp; high rise passenger lifts</li>
				<li class="other">Column free floor plate design</li>
				<li class="other">2 car park lift &amp; 3 service lifts</li>
				<li>Large floor plate in Seoul</li>
				<li>Custom floor layouts</li>
				<li class="other">Tallest Floor to ceiling windows</li>
				<li class="other">350kg/sm (70lb/ft) regular floor loading</li>
				<li>Variable Air Volume (VAV) system</li>
				<li>Dual source main power supply</li>
			</ul>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>