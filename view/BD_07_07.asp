<script type="text/javascript" src="/js/lightbox.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/lightbox.css">

<% 
Dim countQuery,selectQuery,num_count,page_result, t_page, query_where,objRs , selet_query

' for custom page_limit and page_length
page_limit = 12
current_count = (page - 1) * page_limit 'set count number of current page
page_length = 5

query_where = "order by fg_num desc"

// counting list
countQuery = "select count(*) as count from dbo.tblFacGal"

set objConn = OpenDBConnection()
set objRs = SendQuery(objConn,countQuery)
total_count = objRs("count")
num_count = total_count - (page - 1) * page_limit
set objRs = Nothing

// get list
selectQuery = "select Top " & page_limit & " * from dbo.tblFacGal where fg_num Not In (select Top " & current_count & " fg_num from tblFacGal "& query_where &") "& query_where

set objRs = SendQuery(objConn,selectQuery)

t_page = page
page_result = page_create(total_count, t_page, page_limit, page_length)
%>
<div id="content">
	<div class="contWrap">
		<div class="tabWrap type4">
			<ul>
				<li class=""><a href="BD_07_01.asp"><%=gnb_text(24)%></a></li>
				<li class="other"><a href="BD_07_02.asp"><%=gnb_text(25)%></a></li>
				<li><a href="BD_07_03.asp"><%=gnb_text(26)%></a></li>
				<li class="other"><a href="BD_07_04.asp"><%=gnb_text(27)%></a></li>
				<li class=""><a href="BD_07_05.asp"><%=gnb_text(28)%></a></li>
				<!-- li><a href="BD_07_06.asp"><%=gnb_text()%></a></li-->
				<li class="other on"><a href="BD_07_07.asp"><%=gnb_text(29)%></a></li>
			</ul>
		</div>
		<div class="titleWrap titleBd0707 pSh" id="contTitle">
			<h2><a href="#none"><%=gnb_text(23)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(12)%></dt>
				<dd><a href="BD_01_00.asp"><%=gnb_text(17)%></a></dd>
				<dd><a href="BD_02_00.asp"><%=gnb_text(18)%></a></dd>
				<dd><a href="BD_03_00.asp"><%=gnb_text(19)%></a></dd>
				<dd><a href="BD_04_00.asp"><%=gnb_text(20)%></a></dd>
				<dd><a href="BD_05_00.asp"><%=gnb_text(21)%></a></dd>
				<!--dd><a href="BD_06_00.asp"><%=gnb_text(22)%></a></dd-->
				<dd class="on"><a href="BD_07_01.asp"><%=gnb_text(23)%></a></dd>
				<!--<dd><a href="BD_08_01.asp"><%=gnb_text(30)%></a></dd>-->
			</dl>
			<h3><a href="#none"><%=lang_text(0)%></a></h3>
			<dl class="h3Siblings">
				<dt><%=gnb_text(23)%></dt>
				<dd><a href="BD_07_01.asp"><%=gnb_text(24)%></a></dd>
				<dd><a href="BD_07_02.asp"><%=gnb_text(25)%></a></dd>
				<dd><a href="BD_07_03.asp"><%=gnb_text(26)%></a></dd>
				<dd><a href="BD_07_04.asp"><%=gnb_text(27)%></a></dd>
				<dd><a href="BD_07_05.asp"><%=gnb_text(28)%></a></dd>
				<!-- dd><a href="BD_07_06.asp"><%=gnb_text()%></a></dd-->
				<dd class="on"><a href="BD_07_07.asp"><%=gnb_text(29)%></a></dd>
			</dl>
		</div>
		<p class="subTitle cB"><%=lang_text(1)%></p>
		<div class="contBody galleryCont">
			<div class="carouselWrap" id="carousel">
				<ul class="carouselArea">
					<!-- li>
						<div class="zoomBtn"><a href="#?w=963" class="btn zoom jsbtnLyp" rel="layerPop01"><span class="blind">ZOOM</span></a></div>
						<img src="<%=objRs("fg_img")%>" alt="" height="521px" class="previewImg" />
					</li-->
					<li>
						<div class="zoomBtn"><a href="<%=objRs("fg_img")%>" class="btn zoom" data-lightbox="gallery" title="<%=objRs("fg_title_" & current_type)%>"><span class="blind">ZOOM</span></a></div>
						<img src="<%=objRs("fg_img")%>" alt="" class="previewImg" />
					</li>
				</ul>
			</div>
			<div class="tempWrap">
				<ul class="tempArea">
					<% if objRs.EOF Then %>
					<li>
						
					</li>
					<% Else %>
						<% For k = 0 to objRs.RecordCount - 1 %>
					<%
						if k = 0 then
							previewImg = objRs("fg_img")
						end if
					%>
					<li>
						<a href="#" data-url="<%=objRs("fg_img")%>"><img src="<%=objRs("fg_img")%>" width="82px" height="82px" alt="<%=objRs("fg_title_" & current_type)%>" /><span class="text"><span class="bgBar"><%=objRs("fg_list_title")%><input type="hidden" class="img_title" value="<%=objRs("fg_title_" & current_type)%>" /></span></span></a>
					</li>
							<% objRs.MoveNext %>
						<% Next %>
					<% End If %>
					<% objRs.MoveFirst %>
				</ul>
				<div class="paginate">
					<!-- #include file = "main_page_template.asp" -->
				</div>
			</div>


		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>

        <!-- 이미지 미리보기 -->
        <div id="layerPop01" class="lyPopWrap" style="width:963px">
            <div class="lyPopBody">
                <div class="lyPopContWrap">
                    <div class="previewImgWrap"><img src="<%=previewImg %>" alt="" class="previewImg" /></div>
                </div>
            </div>
            <a href="#none" class="popCls jsPopClose">취소</a>
        </div>
	</div>
</div>

<!-- image lightbox list -->
<div class="hide_row lightbox_list">
<% For k = 0 to objRs.RecordCount - 1 %>
<a href="<%=objRs("fg_img")%>" data-lightbox="gallery" title="<%=objRs("fg_title_" & current_type)%>"></a>
	<% objRs.MoveNext %>
<% Next %>
</div>
<!-- image lightbox list end -->