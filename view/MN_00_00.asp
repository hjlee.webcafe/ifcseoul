<script type="text/javascript" src="/js/video.js"></script>
<link rel="stylesheet" type="text/css" href="/css/video-js.css">


<%
Dim countQuery,selectQuery,num_count,page_result, t_page, query_where,objRs,objRs2, objRs3, objRs4

query_where = "order by n_num desc"

If (current_type = "ko") Then
	query_where = "and n_group='0' "&query_where
Else
	query_where = "and n_group='1' "&query_where
End If

// get list
selectQuery = "select Top 1 * from dbo.tblEvent where 1=1 order by e_num desc"

set objConn = OpenDBConnection()
set objRs = SendQuery(objConn,selectQuery)

// get imgList
selectQuery = "select Top 5 * from dbo.tblGall where g_type = '0' and g_show = '1' order by g_num desc"
set objRs2 = SendQuery(objConn,selectQuery)

selectQuery = "select Top 5 * from dbo.tblGall where g_type = '2' and g_show = '1' order by g_num desc"
set objRs3 = SendQuery(objConn,selectQuery)

selectQuery = "select Top 5 * from dbo.tblGall where g_type = '1' and g_show = '1' order by g_num desc"
set objRs4 = SendQuery(objConn,selectQuery)

%>

<script type="text/javascript">
$(document).ready(function () {
	$('.col1Slider').bxSlider($.extend({
		auto: true,
		autoControls: true,
		pause: 4000,
		slideMargin: 0,
		touchEnabled: false
	}, typeof ltIE9 !== 'undefined' && ltIE9 ? {mode:'fade'} : {}));
	$('.col2Slider').bxSlider({
		mode: 'fade',
		auto: true,
		autoControls: true,
		pause: 4000
	});
	$('.col3Slider').bxSlider({
		mode: 'fade',
		auto: true,
		autoControls: true,
		pause: 4000
	});
	$('.col4Slider').bxSlider({
		minSlides: 3,
		maxSlides: 3,
		slideWidth: 360,
		slideMargin: 10,
		infiniteLoop: false,
		hideControlOnEnd: true
	});
});



$(function () {
	$('.gallNavi li a').each(function (index) {
		$('.gallNavi li a').eq(index).click(function (e) {
			e.preventDefault();
			$('.viewGall').removeClass('viewOn');
			$('.viewGall').eq(index).addClass('viewOn');
		});
	});
});

videojs.options.flash.swf = "/swf/video-js.swf";
</script>

<!-- main -->
<div id="main" class="main">
	<div class="box col1">
		<div class="mainSectionCont main_img" style="top:0;">
				<!-- <p><a href="IS_01_00.asp">Newly rising with perfect freshness</a></p> -->
            <div class="mainSectionContTitle">
				<h2><a href="IS_01_00.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': 'Be The Best, With The Best IFC SEOUL'});">Be The Best, With The Best <br/><strong>IFC SEOUL</strong></a></h2>
            </div>
		</div>
		<div class="bdWrap">
			<ul class="col1Slider">
				<li class="sliderBg1"><span class="blind">CONRAD Seoul External Image1</span></li>
                <li class="sliderBg2"><span class="blind">CONRAD Seoul External Image2</span></li>
				<li class="sliderBg3"><span class="blind">CONRAD Seoul External Image3</span></li>
                <li class="sliderBg4"><span class="blind">CONRAD Seoul External Image3</span></li>
			</ul>
		</div>
	</div>
	<div class="box col2">
		<div class="mainSectionCont main_img" style="top:0;">
            <div class="mainSectionContTitle">
    			<a href="BD_07_01.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': 'CONRAD SEOUL'});">
    				<p>Never Just Stay, Stay Inspired</p>
    				<h2><strong>CONRAD SEOUL</strong></h2>
    			</a>
            </div>
		</div>

		<div class="bdWrap">
			<ul class="col2Slider">
				<li><img src="../img/main/col2_1.jpg" alt="CONRAD Seoul External Image" /></li>
				<li><img src="../img/main/col2_2.jpg" alt="CONRAD Seoul External Image" /></li>
				<li><img src="../img/main/col2_3.jpg" alt="CONRAD Seoul External Image" /></li>
                <li><img src="../img/main/col2_4.jpg" alt="CONRAD Seoul External Image" /></li>
                <li><img src="../img/main/col2_5.jpg" alt="CONRAD Seoul External Image" /></li>
			</ul>
		</div>
	</div>
	<div class="box col3">
		<div class="mainSectionCont main_img" style="top:0;">
            <div class="mainSectionContTitle">
    			<a href="BD_07_02.asp" onclick="gtag('event', '버튼클릭', {'event_category':'메인배너', 'event_label':'IFC MALL'});">
    				<p>The place you can spend<br />the most enjoyable time,</p>
    				<h2><strong>IFC MALL</strong></h2>
    			</a>
            </div>
		</div>
		<div class="bdWrap">
			<ul class="col3Slider">
				<li><img src="../img/main/col3_1.jpg" alt="IFC Mall External Image" /></li>
				<li><img src="../img/main/col3_2.jpg" alt="IFC Mall External Image" /></li>
				<li><img src="../img/main/col3_3.jpg" alt="IFC Mall External Image" /></li>
                <li><img src="../img/main/col3_4.jpg" alt="IFC Mall External Image" /></li>
                <li><img src="../img/main/col3_5.jpg" alt="IFC Mall External Image" /></li>
			</ul>
		</div>
	</div>
	<div class="box col4">
		<div class="bdWrap">
			<div class="gall_top">
				<div class="mainSectionCont">
					<h2><a href="IS_07_01.asp" onclick="gtag('event', '버튼클릭', {'event_category':'메인배너', 'event_label':'IFC SEOUL GALLERY'});">IFC SEOUL GALLERY</a></h2>
				</div>
			</div>
			<div class="viewZone">
				<ul>
					<% For k = 0 to objRs2.RecordCount + objRs3.RecordCount + objRs4.RecordCount - 1 %>
						<% IF k mod 3 = 0 Then %>
							<li class="viewGall viewOn"><img src="<%=objRs2("g_img")%>" alt="<%=objRs2("g_title_" & current_type)%>" /></li>
							<% objRs2.MoveNext %>
						<% ElseIf (k - 1) mod 3 = 0 Then %>
							<li class="viewGall"><img src="<%=objRs3("g_img")%>" alt="<%=objRs3("g_title_" & current_type)%>" /></li>
							<% objRs3.MoveNext %>
						<% Else %>
							<li class="viewGall"><img src="<%=objRs4("g_img")%>" alt="<%=objRs4("g_title_" & current_type)%>" /></li>
						<% objRs4.MoveNext %>
						<% End If %>
					<% Next %>
					<% objRs2.MoveFirst %>
					<% objRs3.MoveFirst %>
					<% objRs4.MoveFirst %>
				</ul>
			</div>
			<div class="bkBox">&nbsp;</div>
			<div class="listShadow">&nbsp;</div>
			<div class="gallNavi">
				<ul class="imgList col4Slider">
					<% For k = 0 to objRs2.RecordCount + objRs3.RecordCount + objRs4.RecordCount - 1 %>
						<% IF k mod 3 = 0 Then %>
							<li><a href="#none"><span class="text">IFC Seoul</span><img src="<%=objRs2("g_img")%>" alt="<%=objRs2("g_title_" & current_type)%>" /></a></li>
							<% objRs2.MoveNext %>
						<% ElseIf (k - 1) mod 3 = 0 Then %>
							<li><a href="#none"><span class="text">IFC Mall</span><img src="<%=objRs3("g_img")%>" alt="<%=objRs3("g_title_" & current_type)%>" /></a></li>
							<% objRs3.MoveNext %>
						<% Else %>
							<li><a href="#none"><span class="text">CONRAD Seoul</span><img src="<%=objRs4("g_img")%>" alt="<%=objRs4("g_title_" & current_type)%>" /></a></li>
							<% objRs4.MoveNext %>
						<% End If %>
					<% Next %>
				</ul>
				<!--<a href="#" class="prev"><img src="../img/main/prev.png" alt="prev" /></a>
				<a href="#" class="next"><img src="../img/main/next.png" alt="next" /></a>-->
			</div>
		</div>
	</div>
	<div class="boxGroup">
		<div class="boxInner">
			<div class="box col5">
				<div class="bdWrap">
					<div class="mainSectionCont">
						<h2>IFC SEOUL<br /><span>EVENT</span></h2>
						<p class="mt15"><a href="NE_03_00.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': 'IFCSEOUL EVENT title'});"><%=objRs("e_title")%></a></p>
						<span class="moreBtn"><a href="NE_03_00.asp" class="goLink" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': 'IFCSEOUL EVENT Readmore'});">Read more</a></span>
					</div>
				</div>
			</div>
			<div class="box col6">
				<div class="bdWrap">
					<img src="../img/main/m_4.png" alt="IFC Seoul Building External Image" />
					<div class="mainSectionCont">
						<a href="IS_07_03.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': 'IFC SEOUL FILM'});"><img src="../img/main/txt5.png" alt="IFC SEOUL FILM" /></a>
					</div>
				</div>
			</div>
			<div class="box col7">
				<div class="bdWrap">
					<div class="mainSectionCont">
						<h2>SPECIFICATIONS</h2>
						<p class="mt10"><%=lang_text(0)%></p>
						<span class="moreBtn"><a href="BD_05_00.asp" class="goLink" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': 'SPECIFICATIONS Readmore'});">Read more</a></span>
					</div>
				</div>
			</div>
			<div class="box col8">
				<div class="bdWrap">
					<div class="mainSectionCont">
						<h2><span>AMENITIES</span></h2>
						<p class="mt30 ml30"><a href="BD_07_03.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': 'AMENITIES'});"><img src="../img/main/icon_ifcseoul.png" alt="AMENITIES" /></a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<!-- layer popup -->
<% If Request.Cookies("noticePop_211110")  <> "true" Then %>
<!--<div id="noticePop_211110" class="layer_popup noticePopWarp" style="position: absolute; z-index: 99999; line-height: 0;">
	<div class="video_wrap">
		<img src="/img/popup/20211110_popup.png" usemap="#popMap" alt="공지사항" style="width:320px;" />
	</div>
	<div class="btn_area clearfix" style="background-color: black; color: white; margin-top: 0px; height: 20px; line-height: 20px;">
		<label><input type="checkbox" class="vertical_m" /> <%=lang_text(1)%> </label> <a href="#none" class="js_PopClose fr type2" style="color: white;">Close</a>
	</div>
</div>-->
<% End If %>
 <!-- //layer popup -->

