<div id="content">
	<div class="contWrap">
		<div class="tabWrap">
			<ul>
				<li><a href="IS_02_01.asp"><%=gnb_text(4)%></a></li>
				<li class="other"><a href="IS_02_02.asp"><%=gnb_text(5)%></a></li>
				<li><a href="IS_02_03.asp"><%=gnb_text(6)%></a></li>
				<li class="other on"><a href="IS_02_04.asp"><%=gnb_text(7)%></a></li>
			</ul>
		</div>
		<div class="titleWrap titleIs0204 pSh" id="contTitle">
			<h2><a href="#none"><%=gnb_text(4)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(0)%></dt>
				<dd><a href="IS_01_00.asp"><%=gnb_text(2)%></a></dd>
				<dd class="on"><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<!--<dd><a href="IS_03_00.asp"><%=gnb_text(8)%></a></dd>-->
				<dd><a href="IS_04_00.asp"><%=gnb_text(9)%></a></dd>
				<dd><a href="IS_05_00.asp"><%=gnb_text(10)%></a></dd>
				<dd><a href="IS_06_00.asp"><%=gnb_text(11)%></a></dd>
				<dd><a href="IS_07_01.asp"><%=gnb_text(12)%></a></dd>
			</dl>
			<h3><a href="#none"><%=lang_text(0)%></a></h3>
			<dl class="h3Siblings">
				<dt>IFC Seoul Project</dt>
				<dd><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<dd><a href="IS_02_02.asp"><%=gnb_text(5)%></a></dd>
				<dd><a href="IS_02_03.asp"><%=gnb_text(6)%></a></dd>
				<dd class="on"><a href="IS_02_04.asp"><%=gnb_text(7)%></a></dd>
			</dl>
		</div>
		<p class="subTitle cB"><%=lang_text(1)%></p>
		<div class="contBody historyBox">
			<ol class="tabArea">
				<li class="on"><a href="#"><span class="txtL">Now</span><span class="txtR">- 2009</span></a></li>
				<li><a href="#"><span class="txtL">2007</span><span class="txtR">- 2005</span></a></li>
				<li><a href="#"><span class="txtL">2004</span><span class="txtR">- 2002</span></a></li>
			</ol>

            <ol class="tabList">
                <li class="on"><a href="#none">Now - 2009</a></li>
                <li class="bgc6"><a href="#none">2007- 2005</a></li>
                <li><a href="#none">2004- 2002</a></li>
            </ol>
			<div class="historyWrap">
				<div class="historyArea">
					<div class="historyImg"><img src="../img/com/img_history.jpg" alt="IFC Seoul Building Image" /></div>
					<ol class="historyList">
						<li class="y1"><strong>2012.<span>11</span></strong><p><%=lang_text(2)%></p></li>
						<li class="y2"><strong>2012.<span>08</span></strong><p><%=lang_text(3)%></p></li>
						<li class="y3"><strong>2011.<span>10</span></strong><p><%=lang_text(4)%></p></li>
						<li class="y4"><strong>2009.<span>11</span></strong><p><%=lang_text(5)%></p></li>
						<li class="y5"><strong>2009.<span>02</span></strong><p><%=lang_text(6)%></p></li>
						<li class="y6"><strong>2007.<span>11</span></strong><p><%=lang_text(7)%></p></li>
						<li class="y7"><strong>2007.<span>01</span></strong><p><%=lang_text(8)%></p></li>
						<li class="y8"><strong>2006.<span>12</span></strong><p><%=lang_text(9)%></p></li>
						<li class="y9"><strong>2006.<span>06</span></strong><p><%=lang_text(10)%></p></li>
						<li class="y10"><strong>2005.<span>02</span></strong><p><%=lang_text(11)%></p></li>
						<li class="y11"><strong>2004.<span>06</span></strong><p><%=lang_text(12)%></p></li>
						<li class="y12"><strong>2003.<span>06</span></strong><p><%=lang_text(13)%></p></li>
						<li class="y13"><strong>2003.<span>03</span></strong><p><%=lang_text(14)%></p></li>
						<li class="y14"><strong>2002-<span>2003</span></strong><p><%=lang_text(15)%></p></li>
					</ol>
				</div>
			</div>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>