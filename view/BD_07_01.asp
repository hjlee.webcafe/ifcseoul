<div id="content">
	<div class="contWrap">
		<div class="tabWrap type4">
			<ul>
				<li class="on"><a href="BD_07_01.asp"><%=gnb_text(24)%></a></li>
				<li class="other"><a href="BD_07_02.asp"><%=gnb_text(25)%></a></li>
				<li><a href="BD_07_03.asp"><%=gnb_text(26)%></a></li>
				<li class="other"><a href="BD_07_04.asp"><%=gnb_text(27)%></a></li>
				<li><a href="BD_07_05.asp"><%=gnb_text(28)%></a></li>
				<!-- li><a href="BD_07_06.asp"><%=gnb_text()%></a></li-->
				<li class="other"><a href="BD_07_07.asp"><%=gnb_text(29)%></a></li>
			</ul>
		</div>
		<div class="titleWrap contBg bgBd0701" id="contTitle">
			<h2><a href="#none">Amenities</a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(12)%></dt>
				<dd><a href="BD_01_00.asp"><%=gnb_text(17)%></a></dd>
				<dd><a href="BD_02_00.asp"><%=gnb_text(18)%></a></dd>
				<dd><a href="BD_03_00.asp"><%=gnb_text(19)%></a></dd>
				<dd><a href="BD_04_00.asp"><%=gnb_text(20)%></a></dd>
				<dd><a href="BD_05_00.asp"><%=gnb_text(21)%></a></dd>
				<!--dd><a href="BD_06_00.asp"><%=gnb_text(22)%></a></dd-->
				<dd class="on"><a href="BD_07_01.asp"><%=gnb_text(23)%></a></dd>
				<!--<dd><a href="BD_08_01.asp"><%=gnb_text(30)%></a></dd>-->
			</dl>
			<h3><a href="#none"><%=lang_text(0)%></a></h3>
			<dl class="h3Siblings">
				<dt>Amenities</dt>
				<dd class="on"><a href="BD_07_01.asp"><%=gnb_text(24)%></a></dd>
				<dd><a href="BD_07_02.asp"><%=gnb_text(25)%></a></dd>
				<dd><a href="BD_07_03.asp"><%=gnb_text(26)%></a></dd>
				<dd><a href="BD_07_04.asp"><%=gnb_text(27)%></a></dd>
				<dd><a href="BD_07_05.asp"><%=gnb_text(28)%></a></dd>
				<!-- dd><a href="BD_07_06.asp"><%=gnb_text()%></a></dd-->
				<dd><a href="BD_07_07.asp"><%=gnb_text(29)%></a></dd>
			</dl>
		</div>
		<!-- <p class="subTitle"><%=lang_text(1)%></p> -->
		<div class="contVisual bgBd0701">
			<div class="introTitle">
				<p><%=lang_text(2)%></p>
				<p class="introTxt"><%=lang_text(3)%></p>
			</div>
		</div>
		<div class="contBody">
			<div class="txtCont">
				<p><%=lang_text(4)%></p>
			</div>
		</div>
		<div class="linkBtn link2 mb15"><a href="http://www.conradseoul.co.kr"class="btn white" target="_blank" title="새창 열림" onclick="gtag('event', '버튼클릭', {'event_category':'빌딩 시설', 'event_label':'GO TO CONRAD SEOUL WEB SITE'});">GO TO CONRAD SEOUL WEB SITE</a></div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>
