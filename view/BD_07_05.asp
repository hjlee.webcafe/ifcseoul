<div id="content">
	<div class="contWrap">
		<div class="tabWrap type4">
			<ul>
				<li class=""><a href="BD_07_01.asp"><%=gnb_text(24)%></a></li>
				<li class="other"><a href="BD_07_02.asp"><%=gnb_text(25)%></a></li>
				<li><a href="BD_07_03.asp"><%=gnb_text(26)%></a></li>
				<li class="other"><a href="BD_07_04.asp"><%=gnb_text(27)%></a></li>
				<li class="on"><a href="BD_07_05.asp"><%=gnb_text(28)%></a></li>
				<!-- li><a href="BD_07_06.asp"><%=gnb_text()%></a></li-->
				<li class="other"><a href="BD_07_07.asp"><%=gnb_text(29)%></a></li>
			</ul>
		</div>
		<div class="titleWrap titleBd0705" id="contTitle">
			<h2><a href="#none">Amenities</a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(12)%></dt>
				<dd><a href="BD_01_00.asp"><%=gnb_text(17)%></a></dd>
				<dd><a href="BD_02_00.asp"><%=gnb_text(18)%></a></dd>
				<dd><a href="BD_03_00.asp"><%=gnb_text(19)%></a></dd>
				<dd><a href="BD_04_00.asp"><%=gnb_text(20)%></a></dd>
				<dd><a href="BD_05_00.asp"><%=gnb_text(21)%></a></dd>
				<!--dd><a href="BD_06_00.asp"><%=gnb_text(22)%></a></dd-->
				<dd class="on"><a href="BD_07_01.asp"><%=gnb_text(23)%></a></dd>
				<!--<dd><a href="BD_08_01.asp"><%=gnb_text(30)%></a></dd>-->
			</dl>
			<h3><a href="#none"><%=lang_text(0)%></a></h3>
			<dl class="h3Siblings">
				<dt>Amenities</dt>
				<dd><a href="BD_07_01.asp"><%=gnb_text(24)%></a></dd>
				<dd><a href="BD_07_02.asp"><%=gnb_text(25)%></a></dd>
				<dd><a href="BD_07_03.asp"><%=gnb_text(26)%></a></dd>
				<dd><a href="BD_07_04.asp"><%=gnb_text(27)%></a></dd>
				<dd class="on"><a href="BD_07_05.asp"><%=gnb_text(28)%></a></dd>
				<!-- dd><a href="BD_07_06.asp"><%=gnb_text()%></a></dd-->
				<dd><a href="BD_07_07.asp"><%=gnb_text(29)%></a></dd>
			</dl>
		</div>
		<!-- <p class="subTitle"><%=lang_text(1)%></p> -->
		<div class="contVisual bgBd0705">
			<div class="introTitle">
				<!-- <p class="introTxt"><%=lang_text(2)%></p> -->
			</div>
		</div>
		<div class="contBody">
			<p><%=lang_text(3)%></p>
			<h4 class="titleBar mt30"><%=lang_text(4)%></h4>
			<ul class="mt0">
				<li><%=lang_text(5)%></li>
				<li><%=lang_text(6)%></li>
			</ul>
			<h4 class="titleBar mt30">Building’s Internal Routes</h4>
			<div class="parkingMap"><img src="../img/com/img_parking_map_<%=current_type%>.jpg" alt="Building's Internal Routes Map Image" /></div>
			<table summary="<%=lang_text(12)%>" class="tblType2">
				<caption><%=lang_text(13)%></caption>
				<colgroup>
					<col style="width:22%;" />
					<col style="auto" />
				</colgroup>
				<thead>
					<tr>
						<th colspan="2" scope="col"><%=lang_text(7)%></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row">One IFC</th>
						<td><%=lang_text(8)%></td>
					</tr>
					<tr>
						<th scope="row">Two IFC</th>
						<td><%=lang_text(9)%></td>
					</tr>
					<tr>
						<th scope="row">Three IFC</th>
						<td><%=lang_text(10)%></td>
					</tr>
					<tr>
						<th scope="row">Conrad Seoul</th>
						<td><%=lang_text(11)%></td>
					</tr>
				</tbody>
			</table>

			<!-- <h3 class="titleBar mt30">Parking</h3>
			<table summary="<%=lang_text(14)%>" class="tblType2 parkingTbl">
				<caption><%=lang_text(15)%></caption>
				<colgroup>
					<col style="width:25%;" />
					<col style="width:25%;" />
					<col style="width:25%;" />
					<col style="width:25%;" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col" colspan="4">Parking space pillar number</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row">One IFC</th>
						<td>No. 7 (Orange)</td>
						<th scope="row">IFC Mall A</th>
						<td>No.16 (Light green)</td>
					</tr>
					<tr>
						<th scope="row">Two IFC</th>
						<td>IFC Mall B</td>
						<th scope="row">One IFC</th>
						<td>No.36 (Blue)</td>
					</tr>
					<tr>
						<th scope="row">Three IFC</th>
						<td>No. 49 (Blue)</td>
						<th scope="row">IFC Mall C</th>
						<td>No. 6 (Blue)</td>
					</tr>
					<tr>
						<th scope="row">Conrad hotel</th>
						<td colspan="3">No. 68 (Purple) </td>
					</tr>
				</tbody>
			</table> -->
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>
