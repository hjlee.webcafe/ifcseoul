<div id="content">
	<div class="contWrap">
		<div class="tabWrap type3">
			<ul>
				<li class="on"><a href="BD_08_01.asp"><%=gnb_text(31)%></a></li>
				<li class="other"><a href="BD_08_02.asp"><%=gnb_text(32)%></a></li>
				<li><a href="BD_08_03.asp"><%=gnb_text(33)%></a></li>
			</ul>
		</div>
		<div class="titleWrap titleBd0801" id="contTitle">
			<h2><a href="#none">Leasing</a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(12)%></dt>
				<dd><a href="BD_01_00.asp"><%=gnb_text(17)%></a></dd>
				<dd><a href="BD_02_00.asp"><%=gnb_text(18)%></a></dd>
				<dd><a href="BD_03_00.asp"><%=gnb_text(19)%></a></dd>
				<dd><a href="BD_04_00.asp"><%=gnb_text(20)%></a></dd>
				<dd><a href="BD_05_00.asp"><%=gnb_text(21)%></a></dd>
				<!--dd><a href="BD_06_00.asp"><%=gnb_text(22)%></a></dd-->
				<dd><a href="BD_07_01.asp"><%=gnb_text(23)%></a></dd>
				<dd class="on"><a href="BD_08_01.asp"><%=gnb_text(30)%></a></dd>
			</dl>
			<h3><a href="#none"><%=lang_text(0)%></a></h3>
			<dl class="h3Siblings">
				<dt>Leasing</dt>
				<dd class="on"><a href="BD_08_01.asp"><%=gnb_text(31)%></a></dd>
				<dd><a href="BD_08_02.asp"><%=gnb_text(32)%></a></dd>
				<dd><a href="BD_08_03.asp"><%=gnb_text(33)%></a></dd>
			</dl>
		</div>
		<!-- <p class="subTitle"><%=lang_text(1)%></p> -->
		<div class="contVisual bgBd0801">
			<div class="introTitle">
				<p><%=lang_text(2)%></p>
				<p class="introTxt"><%=lang_text(3)%></p>
			</div>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>