<div id="content">
	<div class="contWrap">
		<div class="titleWrap" id="contTitle">
			<h2><span>Terms of Use</span></h2>
			<span class="subTitle">Sub Copy Areas</span>
		</div>
		<div class="contBody legalWrap">
			<div class="inventoryList">
				<ul>
					<li><strong>제 1 조</strong>목적</li>
					<li><strong>제 2 조</strong>용어의 정의</li>
					<li><strong>제 3 조</strong>IFC 서울 서비스의 개요</li>
					<li><strong>제 4 조</strong>약관의 효력 및 개정</li>
					<li><strong>제 5 조</strong>개인정보 수집</li>
					<li><strong>제 6 조</strong>개인정보 보유 및 이용기간</li>
					<li><strong>제 7 조</strong> IFC 서울 서비스의 개요</li>
					<li><strong>제 8 조</strong>약관의 효력 및 개정</li>
					<li><strong>제 9 조</strong>개인정보 수집</li>
					<li><strong>제 10 조</strong>개인정보 보유 및 이용기간</li>
					<li><strong>제 11 조</strong>목적</li>
					<li><strong>제 12 조</strong>용어의 정의</li>
					<li><strong>제 13 조</strong>IFC 서울 서비스의 개요</li>
					<li><strong>제 14 조</strong>약관의 효력 및 개정</li>
					<li><strong>제 15 조</strong>개인정보 수집</li>
					<li><strong>제 16 조</strong>개인정보 보유 및 이용기간</li>
					<li><strong>제 17 조</strong> IFC 서울 서비스의 개요</li>
					<li><strong>제 18 조</strong>약관의 효력 및 개정</li>
					<li><strong>제 19 조</strong>개인정보 수집</li>
					<li><strong>제 20 조</strong>개인정보 보유 및 이용기간</li>
				</ul>
			</div>
			<div class="legalCont">
				<h3>제1장 총칙</h3>
				<h4>제1조 (목적)</h4>
				<p>본 약관은 &quot;IFC Seoul 멤버십 회원&quot;이 &quot;에스아이에프씨 리테일 몰 디벨로프먼트 유한회사&quot;(이하 &quot;당사&quot;)가 제공하는 &quot;IFC Mall 멤버십 회원서비스&quot;를  이용함에 있어 &quot;IFC Mall 멤버십 회원&quot;과 당사와의 제반 관리, 의무, 관련 절차 등을 규정하는데 그 목적이 있습니다.</p>
				<h4>제2조 (용어의 정의)</h4>
				<ul>
					<li>1. &quot;IFC Mall 멤버십 회원&quot;(이하 &quot;회원&quot;)이란 본 약관에 동의하고 회원가입 신청서와 &quot;개인정보의 수집, 제공 및 활용 동의서&quot;를 영업점 또는 안내데스크에 제출하거나, 홈페이지를 통하여 본 약관 제5조에 정해진 가입 절차에 따라 가입하여 정상적으로 서비스를 이용할 수 있는 권한을 부여 받은 개인 및 법인을 말합니다.</li>
					<li>2. &quot;IFC Mall 멤버십 카드&quot;(이하 &quot;카드&quot;)란 회원이 IFC Mall 멤버십 서비스를 정상적으로 이용할 수 있도록 당사가 승인한 카드로서 당사가 발급한 카드(모바일카드 포함)를 말합니다.</li>
					<li>3. &quot;IFC Mall 멤버십 서비스’란 회원을 위해 당사가 제공하는 서비스로서 그 내용은 본 약관 제 3조에 정한 바와 같습니다.</li>
					<li>4. &quot;IFC Mall 멤버십 마일리지&quot;(이하 &quot;마일리지&quot;)란 회원이 본 약관에 따라 IFC Mall 멤버십 서비스를 이용하기 위하여 취득하는 것으로서 그 적립 및 사용 등에 관한 구체적인 사항은 본 약관 제7조 및 제8조에 정한 바와 같습니다.<br />가맹점&quot;이란 회원이 구입한 재화 또는 용역의 가액에 따라 마일리지를 적립할 수 있는 매장으로서 회사와 가맹계약을 체결하고 IFC Mall 내에서 영업을 하는 매장을 말합니다.</li>
				</ul>
			</div>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>

