<div id="content">
	<div class="contWrap">
		<div class="titleWrap titleBd0600 pSh" id="contTitle">
			<h2><a href="#none"><%=lang_text(0)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(12)%></dt>
				<dd><a href="BD_01_00.asp"><%=gnb_text(17)%></a></dd>
				<dd><a href="BD_02_00.asp"><%=gnb_text(18)%></a></dd>
				<dd><a href="BD_03_00.asp"><%=gnb_text(19)%></a></dd>
				<dd><a href="BD_04_00.asp"><%=gnb_text(20)%></a></dd>
				<dd><a href="BD_05_00.asp"><%=gnb_text(21)%></a></dd>
				<!--dd class="on"><a href="BD_06_00.asp"><%=gnb_text(22)%></a></dd-->
				<dd><a href="BD_07_01.asp"><%=gnb_text(23)%></a></dd>
				<!--<dd><a href="BD_08_01.asp"><%=gnb_text(30)%></a></dd>-->
			</dl>
		</div>
		<p class="subTitle cB"><%=lang_text(1)%></p>
		<!-- <div class="contVisual bgBd0600">
			<div class="introTitle">
				<p><%=lang_text(2)%></p>
				<p class="introTxt"><%=lang_text(3)%></p>
			</div>
		</div> -->
		<div class="contBody tenantsCont">
			<ul class="listType7">
				<li>
					<div class="gender man"><span class="blind">man</span></div>
					<dl>
						<dt><%=lang_text(4)%></dt>
						<dd>
							<%=lang_text(5)%>
						</dd>
					</dl>
				</li>
				<li>
					<div class="gender man"><span class="blind">man</span></div>
					<dl>
						<dt><%=lang_text(6)%></dt>
						<dd>
							<%=lang_text(7)%>
						</dd>
					</dl>
				</li>
				<li>
					<div class="gender man"><span class="blind">man</span></div>
					<dl>
						<dt><%=lang_text(8)%></dt>
						<dd>
							<%=lang_text(9)%>
						</dd>
					</dl>
				</li>
				<li>
					<div class="gender woman"><span class="blind">man</span></div>
					<dl>
						<dt><%=lang_text(10)%></dt>
						<dd>
							<%=lang_text(11)%>
						</dd>
					</dl>
				</li>
			</ul>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>