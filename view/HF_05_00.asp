<div id="content">
	<div class="contWrap">
		<div class="titleWrap" id="contTitle">
			<h2><span>Anti –Spam Policy</span></h2>
			<span class="subTitle">Sub Copy Areas</span>
		</div>
		<div class="contBody legalNotice">
			<p>IFC 서울 웹사이트는 무차별적으로 보내지는 스팸(또는 타사) 메일을 차단하기 위해 본 웹사이트에 게시된 이메일 주소가 전자우편 수집 프로그램이나 그 밖의 기술적 장치를 포함한 어떠한 수단으로도 수집되는 것을 거부하며, 이를 위반시 정보통신망법에 의해 형사처벌 됨을 유념하시기 바랍니다.</p>
			<p class="mt25">게시일 : 2014년 3월 12일 </p>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>