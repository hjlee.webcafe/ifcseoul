<!-- <script type="text/javascript" src="http://openapi.map.naver.com/openapi/naverMap.naver?ver=2.0&key=d73332282f011d31b436e2f38e6bbcd6"></script> -->
<!-- <script type="text/javascript" src="https://openapi.map.naver.com/openapi/v3/maps.js?clientId=HtQZKjVGGP0bUkN_BLjY&submodules=geocoder"></script> -->
<script type="text/javascript" src="https://openapi.map.naver.com/openapi/v3/maps.js?ncpClientId=97encl097n"></script>
<script type="text/javascript" src="/js/jquery.ajax-cross-origin.min.js"></script>

<div id="content">
	<div class="contWrap">
		<div class="titleWrap titleNm0100 pSh" id="contTitle">
			<h2><span><%=lang_text(0)%></span></h2>
		</div>
		<p class="subTitle cB"><%=lang_text(1)%></p>
		<div class="contBody fringeInfo">
			<ul class="tabs type1">
                <li class="tc-tab tc-selected" id="traffic_tab"><h3>교통상황</h3></li>
				<li class="tc-tab" id="publicoffice_tab"><h3><%=lang_text(2)%></h3></li>
				<li class="tc-tab" id="hospital_tab"><h3><%=lang_text(3)%></h3></li>
			</ul>
			<div class="panels">
                <!-- 교통상황 -->
                <span class="blind">교통상황</span>
                <div class="tc-panel tc-selected">
                    <div class="publicOffice">
                        <div class="trafficmap placeImg" id="map_wrap0" style="margin-right:500px;">
                            <div id="trafficMap">
                                <div class="buttons" style="position:absolute; z-index:1000; ">
                                    <input id="btnTraffic" type="button" name="교통상황" value="교통상황" class="control-btn control-on">
                                </div>
                            </div>
                        </div>
                        <div id="trafficDiv" class="placePosWrap">
                            <ul id="trafficInfo" style="width:90%; margin:0 auto;"></ul>
                            <ul class="paginate"></ul>
                        </div>
                    </div>
                </div>
                <!-- //교통상황 -->
				<!-- 관공서 -->
				<span class="blind"><%=lang_text(4)%></span>
				<div class="tc-panel">
					<div class="publicOffice">
						<div class="placeImg" id="map_wrap1">
							<% If current_type = "ko" Then %>
							<div id="testMap" style="width:592px; height:510px;"></div>
							<% Else %>
							<div id="google_map" style="width:592px; height:510px;"></div>
							<% End If %>
						</div>
						<ul class="placePosWrap">
							<li class="on"><a href="#?w=300" class="jsbtnLyp" rel="layerPop01"><%=lang_text(5)%></a></li>
							<li class="other"><a href="#?w=300" class="jsbtnLyp" rel="layerPop02"><%=lang_text(6)%></a></li>
							<li class="other"><a href="#?w=300" class="jsbtnLyp" rel="layerPop03"><%=lang_text(7)%></a></li>
							<li><a href="#?w=300" class="jsbtnLyp" rel="layerPop04"><%=lang_text(8)%></a></li>
						</ul>

						<div id="layerPop01" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(14)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02)788-2114</li>
										<li><%=lang_text(10)%> : <a href="http://www.assembly.go.kr" class="goLink" target="_blank" title="Open New Window">http://www.assembly.go.kr</a></li>
										<li><%=lang_text(11)%> : 09:00 – 18:00<%=lang_text(15)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=12&pinType=site&pinId=11583151&x=126.9141499&y=37.5320453&enc=b64" class="btn" target="_blank"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop02" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(16)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02)788-4211</li>
										<li><%=lang_text(10)%> : <a href="http://www.nanet.go.kr" class="goLink" target="_blank" title="Open New Window">http://www.nanet.go.kr</a></li>
										<li><%=lang_text(11)%> : 09:00 – 16:00<%=lang_text(17)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=12&pinType=site&pinId=11591187&x=126.9171550&y=37.5312711&enc=b64" class="btn" target="_blank"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop03" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(18)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02)2670-1475</li>
										<li><%=lang_text(10)%> : <a href="http://www.ydp.go.kr" class="goLink" target="_blank" title="Open New Window">http://www.ydp.go.kr</a></li>
										<li><%=lang_text(11)%> : 09:00 – 18:00</li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=11&pinType=site&pinId=11627898&x=126.8959997&y=37.5264526&enc=b64" class="btn" target="_blank"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop04" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(19)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02)783-0014</li>
										<li><%=lang_text(10)%> : <a href="http://www.koreapost.go.kr/se/150" class="goLink" target="_blank" title="Open New Window">http://www.koreapost.go.kr/se/150</a></li>
										<li><%=lang_text(11)%> : 09:00 – 16:30<%=lang_text(20)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=10&pinType=site&pinId=13286276&x=126.9263277&y=37.5224735&enc=b64" class="btn" target="_blank"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

					</div>
				</div>
				<!-- //관공서 -->
				<!-- 병원 -->
				<span class="blind"><%=lang_text(21)%></span>
				<div class="tc-panel">
					<div class="hospital">
						<div class="placeImg" id="map_wrap2">
							<% If current_type = "ko" Then %>
							<div id="hospital_map" style="width:592px; height:510px;"></div>
							<% Else %>
							<div id="google_map2" style="width:592px; height:510px;"></div>
							<% End If %>
						</div>
						<ul class="placePosWrap hospital_list" id="hospital_1">
							<li class="on"><a href="#?w=300" class="jsbtnLyp" rel="layerPop05"><%=lang_text(22)%></a></li>
							<li class="other"><a href="#?w=300" class="jsbtnLyp" rel="layerPop06"><%=lang_text(23)%></a></li>
							<li class="other"><a href="#?w=300" class="jsbtnLyp" rel="layerPop07"><%=lang_text(24)%></a></li>
							<li><a href="#?w=300" class="jsbtnLyp" rel="layerPop08"><%=lang_text(25)%></a></li>
							<li><a href="#?w=300" class="jsbtnLyp" rel="layerPop09"><%=lang_text(26)%></a></li>
							<li class="other"><a href="#?w=300" class="jsbtnLyp" rel="layerPop10"><%=lang_text(27)%></a></li>
							<li class="other"><a href="#?w=300" class="jsbtnLyp" rel="layerPop11"><%=lang_text(28)%></a></li>
							<li><a href="#?w=300" class="jsbtnLyp" rel="layerPop12"><%=lang_text(29)%></a></li>
						</ul>
						<ul class="placePosWrap hide hospital_list" id="hospital_2">
							<li><a href="#?w=300" class="jsbtnLyp" rel="layerPop13"><%=lang_text(30)%></a></li>
							<li class="other"><a href="#?w=300" class="jsbtnLyp" rel="layerPop14"><%=lang_text(31)%></a></li>
							<li class="other"><a href="#?w=300" class="jsbtnLyp" rel="layerPop15"><%=lang_text(32)%></a></li>
							<li><a href="#?w=300" class="jsbtnLyp" rel="layerPop16"><%=lang_text(33)%></a></li>
							<li><a href="#?w=300" class="jsbtnLyp" rel="layerPop17"><%=lang_text(34)%></a></li>
							<li class="other"><a href="#?w=300" class="jsbtnLyp" rel="layerPop18"><%=lang_text(35)%></a></li>
						</ul>

						<div id="layerPop05" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(36)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02) 783-5635</li>
										<li><%=lang_text(13)%> : <%=lang_text(37)%></li>
										<li><%=lang_text(10)%> : <a href="http://www.kimjoonho.kr" class="goLink" target="_blank" title="Open New Window">http://www.kimjoonho.kr</a></li>
										<li><%=lang_text(11)%> : <%=lang_text(38)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=12&pinType=site&pinId=12987632&x=126.9239888&y=37.5204147&enc=b64" target="_blank" class="btn"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop06" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(39)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02) 780-7582</li>
										<li><%=lang_text(13)%> : <%=lang_text(40)%></li>
										<li><%=lang_text(10)%> : <a href="http://doctor-kim.co.kr" class="goLink" target="_blank" title="Open New Window">http://doctor-kim.co.kr</a></li>
										<li><%=lang_text(11)%> : <%=lang_text(41)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=12&pinType=site&pinId=12030078&x=126.9248750&y=37.5216583&enc=b64" target="_blank" class="btn"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop07" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(42)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02) 780-7585</li>
										<li><%=lang_text(13)%> : <%=lang_text(43)%></li>
										<li><%=lang_text(10)%> : <a href="https://blog.naver.com/irondoor11" class="goLink" target="_blank" title="Open New Window">https://blog.naver.com/irondoor11</a></li>
										<li><%=lang_text(11)%> : <%=lang_text(44)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=12&lat=37.5214658&lng=126.9250847&query=7ISc7Jq47Yq567OE7IucIOyYgeuTse2PrOq1rCDsl6zsnZjrj4Trj5kgMzUtNQ%3D%3D&type=ADDRESS&tab=1&enc=b64" target="_blank" class="btn"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop08" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(45)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02) 761-5082</li>
										<li><%=lang_text(13)%> : <%=lang_text(46)%></li>
										<li><%=lang_text(10)%> : <a href="http://www.newyonseient.co.kr" class="goLink" target="_blank" title="Open New Window">http://www.newyonseient.co.kr</a></li>
										<li><%=lang_text(11)%> : <%=lang_text(47)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=9&pinType=site&pinId=19982136&x=126.9239910&y=37.5227260&enc=b64" target="_blank" class="btn"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop09" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(48)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02) 761-1188</li>
										<li><%=lang_text(13)%> : <%=lang_text(49)%></li>
										<li><%=lang_text(10)%> : <a href="http://www.damiskin.com" class="goLink" target="_blank" title="Open New Window">http://www.damiskin.com</a></li>
										<li><%=lang_text(11)%> : <%=lang_text(50)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=12&pinType=site&pinId=31982099&x=126.9307527&y=37.5189394&enc=b64" target="_blank" class="btn"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop10" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(51)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02) 786-5123</li>
										<li><%=lang_text(13)%> : <%=lang_text(52)%></li>
										<li><%=lang_text(10)%> : <a href="http://www.cnpskin.com" class="goLink" target="_blank" title="Open New Window">http://www.cnpskin.com</a></li>
										<li><%=lang_text(11)%> : <%=lang_text(53)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=12&pinType=site&pinId=11877512&x=126.9249565&y=37.5226445&enc=b64" target="_blank" class="btn"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop11" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(54)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02) 785-5575</li>
										<li><%=lang_text(13)%> : <%=lang_text(55)%></li>
										<!-- li><%=lang_text(10)%> : <a href="http://www.cnpskin.com" class="goLink" target="_blank" title="Open New Window">http://www.cnpskin.com</a></li -->
										<li><%=lang_text(11)%> : <%=lang_text(56)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=12&lat=37.5242623&lng=126.9248899&query=7ISc7Jq47Yq567OE7IucIOyYgeuTse2PrOq1rCDsl6zsnZjrj4Trj5kgMjMtMTA%3D&type=ADDRESS&tab=1&enc=b64" target="_blank" class="btn"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop12" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(57)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02) 3770-2875</li>
										<li><%=lang_text(13)%> : <%=lang_text(58)%></li>
										<li><%=lang_text(11)%> : <%=lang_text(59)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=12&pinType=site&pinId=19000922&x=126.9241529&y=37.5238233&enc=b64" target="_blank" class="btn"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop13" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(60)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02) 785-5575</li>
										<li><%=lang_text(13)%> : <%=lang_text(61)%></li>
										<li><%=lang_text(10)%> : <a href="http://yelimdent.com" class="goLink" target="_blank" title="Open New Window">http://yelimdent.com</a></li>
										<li><%=lang_text(11)%> : <%=lang_text(62)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=12&pinType=site&pinId=11589494&x=126.9246490&y=37.5225150&enc=b64" target="_blank" class="btn"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop14" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(63)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02) 780-6288</li>
										<li><%=lang_text(13)%> : <%=lang_text(64)%></li>
										<!-- li><%=lang_text(10)%> : <a href="http://yelimdent.com" class="goLink" target="_blank" title="Open New Window">http://yelimdent.com</a></li -->
										<li><%=lang_text(11)%> : <%=lang_text(65)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=12&lat=37.5204924&lng=126.9239892&query=7ISc7Jq47Yq567OE7IucIOyYgeuTse2PrOq1rCDsl6zsnZjrj4Trj5kgMzc%3D&type=ADDRESS&tab=1&enc=b64" target="_blank" class="btn"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop15" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(66)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02) 780-1766</li>
										<li><%=lang_text(13)%> : <%=lang_text(67)%></li>
										<li><%=lang_text(11)%> : <%=lang_text(68)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=12&lat=37.5204924&lng=126.9239892&query=7ISc7Jq47Yq567OE7IucIOyYgeuTse2PrOq1rCDsl6zsnZjrj4Trj5kgMzc%3D&type=ADDRESS&tab=1&enc=b64" target="_blank" class="btn"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop16" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(69)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02) 785-1068</li>
										<li><%=lang_text(13)%> : <%=lang_text(70)%></li>
										<li><%=lang_text(10)%> : <a href="http://www.jceye.co.kr" class="goLink" target="_blank" title="Open New Window">http://www.jceye.co.kr</a></li>
										<li><%=lang_text(11)%> : <%=lang_text(71)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=12&pinType=site&pinId=11559082&x=126.9227668&y=37.5214695&enc=b64" target="_blank" class="btn"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop17" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(72)%></strong>
									<ul>
										<li><%=lang_text(9)%> : 02) 6334-1000</li>
										<li><%=lang_text(13)%> : <%=lang_text(73)%></li>
										<!-- li><%=lang_text(10)%> : <a href="#" class="goLink" target="_blank" title="Open New Window">http://www.jceye.co.kr</a></li-->
										<li><%=lang_text(11)%> : <%=lang_text(74)%></li>
									</ul>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=12&pinType=site&pinId=18999854&x=126.9254903&y=37.5236234&enc=b64" target="_blank" class="btn"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div id="layerPop18" class="lyPopWrap bw">
							<div class="lyPopBody">
								<div class="lyPopContWrap">
									<strong><%=lang_text(75)%></strong>
									<ul>
										<li><%=lang_text(9)%> : <%=lang_text(76)%></li>
										<li><%=lang_text(13)%> : <%=lang_text(77)%></li>
										<li><%=lang_text(10)%> : <a href="http://www.cmcsungmo.or.kr" class="goLink" target="_blank" title="Open New Window">http://www.cmcsungmo.or.kr</a></li>
										<li><%=lang_text(11)%> : <%=lang_text(78)%></li>
									</ul>
									<p><%=lang_text(79)%></p>
									<span class="placeBtn"><a href="http://map.naver.com/?dlevel=12&pinType=site&pinId=11686905&x=126.9367517&y=37.5183253&enc=b64" target="_blank" class="btn"><%=lang_text(12)%></a></span>
								</div>
							</div>
							<a href="#none" class="popCls jsPopClose">Close</a>
						</div>

						<div class="paginate">
							<br />
							<!-- a href="#" class="pre"><span>prev</span></a-->
							<a href="#hospital_1" class="list_p"><strong>1</strong></a>
							<a href="#hospital_2" class="list_p">2</a>
							<!-- a href="#" class="next"><span>next</span></a-->
						</div>

						<!-- div class="m_paging jsCarouselPaging">
							<ul>
								<li class="on"><a href="#"><span class="blind">prev</span></a></li>
								<li><a href="#"><span class="blind">next</span></a></li>
							</ul>
						</div -->
					</div>
				</div>
				<!-- //병원 -->
			</div>

            <!-- 주변지역안내 -->
            <div id="surrounding_info">
    			<h4 class="mt20"><%=lang_text(80)%></h4>
    			<table summary="<%=lang_text(102)%>" class="tblType2">
    				<caption><%=lang_text(103)%></caption>
    				<colgroup>
    					<col style="width:33%;" />
    					<col />
    					<col style="width:30%;" />
    				</colgroup>
    				<thead>
    					<tr>
    						<th>&nbsp;</th>
    						<th scope="col"><%=lang_text(81)%></th>
    						<th scope="col"><%=lang_text(82)%></th>
    					</tr>
    				</thead>
    				<tbody>
    					<tr>
    						<th scope="row" rowspan="5"><%=lang_text(83)%></th>
    						<td class="name"><%=lang_text(84)%></td>
    						<td>02-2670-3114</td>
    					</tr>
    					<tr>
    						<td class="name"><%=lang_text(85)%></td>
    						<td>02-3153-8114</td>
    					</tr>
    					<tr>
    						<td class="name"><%=lang_text(86)%></td>
    						<td>02-3456-3114</td>
    					</tr>
    					<tr>
    						<td class="name"><%=lang_text(87)%></td>
    						<td>02-843-2891</td>
    					</tr>
    					<tr>
    						<td class="name"><%=lang_text(88)%></td>
    						<td>121</td>
    					</tr>
    					<tr>
    						<th scope="row" rowspan="4"><%=lang_text(89)%></th>
    						<td class="name"><%=lang_text(90)%></td>
    						<td>112</td>
    					</tr>
    					<tr>
    						<td class="name"><%=lang_text(91)%></td>
    						<td>02-2633-0112</td>
    					</tr>
    					<tr>
    						<td class="name"><%=lang_text(92)%></td>
    						<td>02-784-0112</td>
    					</tr>
    					<tr>
    						<td class="name"><%=lang_text(93)%></td>
    						<td>02-2273-1113</td>
    					</tr>
    					<tr>
    						<th scope="row" rowspan="3"><%=lang_text(94)%></th>
    						<td class="name"><%=lang_text(95)%></td>
    						<td>119</td>
    					</tr>
    					<tr>
    						<td class="name"><%=lang_text(96)%></td>
    						<td>02-783-0119</td>
    					</tr>
    					<tr>
    						<td class="name"><%=lang_text(97)%></td>
    						<td>02-2675-0119</td>
    					</tr>
    					<tr>
    						<th scope="row" rowspan="5"><%=lang_text(98)%></th>
    						<td class="name"><%=lang_text(99)%></td>
    						<td>119</td>
    					</tr>
    					<tr>
    						<td class="name"><%=lang_text(100)%></td>
    						<td>02-1544-2992</td>
    					</tr>
    					<tr>
    						<td class="name"><%=lang_text(101)%></td>
    						<td>02-1661-7575</td>
    					</tr>
    					<tr>
    						<td class="name"><%=lang_text(102)%></td>
    						<td>02-337-7582</td>
    					</tr>
    					<tr>
    						<td class="name"><%=lang_text(103)%></td>
    						<td>02-2632-0013</td>
    					</tr>
    				</tbody>
    			</table>
            </div>
		</div>
	</div>
</div>
