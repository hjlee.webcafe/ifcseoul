<div id="content">
	<div class="contWrap">
		<div class="tabWrap type4">
			<ul>
				<li><a href="BD_07_01.asp"><%=gnb_text(24)%></a></li>
				<li class="other"><a href="BD_07_02.asp"><%=gnb_text(25)%></a></li>
				<li class="on"><a href="BD_07_03.asp"><%=gnb_text(26)%></a></li>
				<li class="other"><a href="BD_07_04.asp"><%=gnb_text(27)%></a></li>
				<li class=""><a href="BD_07_05.asp"><%=gnb_text(28)%></a></li>
				<!-- li><a href="BD_07_06.asp"><%=gnb_text()%></a></li-->
				<li class="other"><a href="BD_07_07.asp"><%=gnb_text(29)%></a></li>
			</ul>
		</div>
		<div class="titleWrap titleIs0703 pSh" id="contTitle">
			<h2><a href="#none">Amenities</a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(12)%></dt>
				<dd><a href="BD_01_00.asp"><%=gnb_text(17)%></a></dd>
				<dd><a href="BD_02_00.asp"><%=gnb_text(18)%></a></dd>
				<dd><a href="BD_03_00.asp"><%=gnb_text(19)%></a></dd>
				<dd><a href="BD_04_00.asp"><%=gnb_text(20)%></a></dd>
				<dd><a href="BD_05_00.asp"><%=gnb_text(21)%></a></dd>
				<!--dd><a href="BD_06_00.asp"><%=gnb_text(22)%></a></dd-->
				<dd class="on"><a href="BD_07_01.asp"><%=gnb_text(23)%></a></dd>
				<!--<dd><a href="BD_08_01.asp"><%=gnb_text(30)%></a></dd>-->
			</dl>
			<h3><a href="#none"><%=lang_text(0)%></a></h3>
			<dl class="h3Siblings">
				<dt>Amenities</dt>
				<dd><a href="BD_07_01.asp"><%=gnb_text(24)%></a></dd>
				<dd><a href="BD_07_02.asp"><%=gnb_text(25)%></a></dd>
				<dd class="on"><a href="BD_07_03.asp"><%=gnb_text(26)%></a></dd>
				<dd><a href="BD_07_04.asp"><%=gnb_text(27)%></a></dd>
				<dd><a href="BD_07_05.asp"><%=gnb_text(28)%></a></dd>
				<!-- dd><a href="BD_07_06.asp"><%=gnb_text()%></a></dd-->
				<dd><a href="BD_07_07.asp"><%=gnb_text(29)%></a></dd>
			</dl>
		</div>
		<!-- <p class="subTitle cB"><%=lang_text(1)%></p> -->
		<div class="contBody">
			<div class="restPlacesCont">
				<div class="allRestPlaces">
					<div class="imgBox">
						<span class="pcVer"><img src="../img/com/img_rest_places5.jpg" alt="Sculpture Installation location Map Image" /></span>
						<span class="mVer"><img src="../img/com/img_rest_places5_m.jpg" alt="Sculpture Installation location Button Map Image" /></span>
					</div>
					<div class="restBtn place1"><a href="#" onclick="toggleLayer('restLayer1');"><span><%=lang_text(2)%></span></a></div>
					<div class="restLayer basicLayer" id="restLayer1">
						<div class="layerInner">
							<strong><%=lang_text(3)%></strong>
							<img src="../img/com/img_rest_places6.jpg" alt="Constellation" />
							<div class="closedBtn"><a href="#none" onclick="toggleLayer('restLayer1');"><span class="blind">close</span></a></div>
						</div>
					</div>
					<div class="restBtn place2"><a href="#" onclick="toggleLayer('restLayer2');" style="<% If current_type = "ko" Then Response.Write "padding-top: 10px;" End If%>"><span><%=lang_text(4)%></span></a></div>
					<div class="restLayer basicLayer" id="restLayer2">
						<div class="layerInner">
							<strong><%=lang_text(5)%></strong>
							<img src="../img/com/img_rest_places7.jpg" alt="Silent Propagation" />
							<div class="closedBtn"><a href="#none" onclick="toggleLayer('restLayer2');"><span class="blind">close</span></a></div>
						</div>
					</div>
					<div class="restBtn place4"><a href="#" onclick="toggleLayer('restLayer4');"><span><%=lang_text(6)%></span></a></div>
					<div class="restLayer basicLayer" id="restLayer4">
						<div class="layerInner">
							<strong><%=lang_text(7)%></strong>
							<img src="../img/com/img_rest_places9.jpg" alt="Whites" />
							<div class="closedBtn"><a href="#none" onclick="toggleLayer('restLayer4');"><span class="blind">close</span></a></div>
						</div>
					</div>
					<div class="restBtn place3"><a href="#" onclick="toggleLayer('restLayer3');"><span><%=lang_text(8)%></span></a></div>
					<div class="restLayer basicLayer" id="restLayer3">
						<div class="layerInner">
							<strong><%=lang_text(9)%></strong>
							<img src="../img/com/img_rest_places13.jpg" alt="Metallica" />
							<div class="closedBtn"><a href="#none" onclick="toggleLayer('restLayer3');"><span class="blind">close</span></a></div>
						</div>
					</div>
					<div class="restBtn place5"><a href="#" onclick="toggleLayer('restLayer5');"><span><%=lang_text(10)%></span></a></div>
					<div class="restLayer basicLayer" id="restLayer5">
						<div class="layerInner">
							<strong><%=lang_text(11)%></strong>
							<img src="../img/com/img_rest_places10.jpg" alt="Looking Beyond" />
							<div class="closedBtn"><a href="#none" onclick="toggleLayer('restLayer5');"><span class="blind">close</span></a></div>
						</div>
					</div>
					<div class="restBtn place6"><a href="#" onclick="toggleLayer('restLayer6');" style="<% If current_type = "ko" Then Response.Write "padding-top: 10px;" End If%>"><span><%=lang_text(12)%></span></a></div>
					<div class="restLayer basicLayer" id="restLayer6">
						<div class="layerInner">
							<strong><%=lang_text(13)%></strong>
							<img src="../img/com/img_rest_places11.jpg" alt="Adventure Ophelia" />
							<div class="closedBtn"><a href="#none" onclick="toggleLayer('restLayer6');"><span class="blind">close</span></a></div>
						</div>
					</div>
					<div class="restBtn place7"><a href="#" onclick="toggleLayer('restLayer7');"><span><%=lang_text(14)%></span></a></div>
					<div class="restLayer basicLayer" id="restLayer7">
						<div class="layerInner">
							<strong><%=lang_text(15)%></strong>
							<img src="../img/com/img_rest_places12.jpg" alt="Dream" />
							<div class="closedBtn"><a href="#none" onclick="toggleLayer('restLayer7');"><span class="blind">close</span></a></div>
						</div>
					</div>					
				</div>
				<h4 class="mt25"><%=lang_text(19)%></h4>
				<div class="imgArea"><img src="../img/com/img_rest_places1.jpg" alt="Constellation Sculpture and Location Map Image" /></div>
				<div class="infoTbl">
					<table summary="<%=lang_text(39)%>" class="tblType2">
						<caption><%=lang_text(40)%></caption>
						<colgroup>
							<col style="width:20%;" />
							<col style="auto;" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><%=lang_text(16)%></th>
								<td><%=lang_text(20)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(49)%></th>
								<td><%=lang_text(50)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(17)%></th>
								<td><%=lang_text(21)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(18)%></th>
								<td><%=lang_text(22)%></td>
							</tr>
						</tbody>
					</table>
				</div>
				<h4><%=lang_text(23)%></h4>
				<div class="imgArea"><img src="../img/com/img_rest_places2.jpg" alt="Silent Propagation Sculpture and Location Map Image" /></div>
				<div class="infoTbl">
					<table summary="<%=lang_text(41)%>" class="tblType2">
						<caption><%=lang_text(42)%></caption>
						<colgroup>
							<col style="width:20%;" />
							<col style="auto;" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><%=lang_text(16)%></th>
								<td><%=lang_text(24)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(49)%></th>
								<td><%=lang_text(51)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(17)%></th>
								<td><%=lang_text(25)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(18)%></th>
								<td><%=lang_text(26)%></td>
							</tr>
						</tbody>
					</table>
				</div>
				<h4><%=lang_text(27)%></h4>
				<div class="imgArea"><img src="../img/com/img_rest_places3.jpg" alt="Dream Sculpture and Location Map Image" /></div>
				<div class="infoTbl">
					<table summary="<%=lang_text(43)%>" class="tblType2">
						<caption><%=lang_text(44)%></caption>
						<colgroup>
							<col style="width:20%;" />
							<col style="auto;" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><%=lang_text(16)%></th>
								<td><%=lang_text(28)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(49)%></th>
								<td><%=lang_text(52)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(17)%></th>
								<td><%=lang_text(29)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(18)%></th>
								<td><%=lang_text(30)%></td>
							</tr>
						</tbody>
					</table>
				</div>
				<h4><%=lang_text(31)%></h4>
				<div class="imgArea"><img src="../img/com/img_rest_metallica.jpg" alt="Metallica Map Image" /></div>
				<div class="infoTbl">
					<table summary="<%=lang_text(45)%>" class="tblType2">
						<caption><%=lang_text(46)%></caption>
						<colgroup>
							<col style="width:20%;" />
							<col style="auto;" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><%=lang_text(16)%></th>
								<td><%=lang_text(32)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(49)%></th>
								<td><%=lang_text(53)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(17)%></th>
								<td><%=lang_text(33)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(18)%></th>
								<td><%=lang_text(34)%></td>
							</tr>
						</tbody>
					</table>
				</div>
				<h4><%=lang_text(35)%></h4>
				<div class="imgArea"><img src="../img/com/img_rest_white.jpg" alt="WhiteSculpture Image" /></div>
				<div class="infoTbl">
					<table summary="<%=lang_text(47)%>" class="tblType2">
						<caption><%=lang_text(48)%></caption>
						<colgroup>
							<col style="width:20%;" />
							<col style="auto;" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><%=lang_text(16)%></th>
								<td><%=lang_text(36)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(49)%></th>
								<td><%=lang_text(54)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(17)%></th>
								<td><%=lang_text(37)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(18)%></th>
								<td><%=lang_text(38)%></td>
							</tr>
						</tbody>
					</table>
				</div>


				<h4><%=lang_text(55)%></h4>
				<div class="imgArea"><img src="../img/com/img_rest_looking_beyond.jpg" alt="Looking Beyoun Sculpture Image" /></div>
				<div class="infoTbl">
					<table summary="<%=lang_text(56)%>" class="tblType2">
						<caption><%=lang_text(57)%></caption>
						<colgroup>
							<col style="width:20%;" />
							<col style="auto;" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><%=lang_text(16)%></th>
								<td><%=lang_text(58)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(49)%></th>
								<td><%=lang_text(59)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(17)%></th>
								<td><%=lang_text(60)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(18)%></th>
								<td><%=lang_text(61)%></td>
							</tr>
						</tbody>
					</table>
				</div>

				<h4><%=lang_text(62)%></h4>
				<div class="imgArea"><img src="../img/com/img_rest_adventure_of_ophelia.jpg" alt="Adventure of Ophelia Sculpture Image" /></div>
				<div class="infoTbl">
					<table summary="<%=lang_text(63)%>" class="tblType2">
						<caption><%=lang_text(64)%></caption>
						<colgroup>
							<col style="width:20%;" />
							<col style="auto;" />
						</colgroup>
						<tbody>
							<tr>
								<th scope="row"><%=lang_text(16)%></th>
								<td><%=lang_text(65)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(49)%></th>
								<td><%=lang_text(66)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(17)%></th>
								<td><%=lang_text(67)%></td>
							</tr>
							<tr>
								<th scope="row"><%=lang_text(18)%></th>
								<td><%=lang_text(68)%></td>
							</tr>
						</tbody>
					</table>
				</div>


			</div>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>