<div id="content">
	<div class="contWrap">
		<div class="titleWrap pSh" id="contTitle">
			<h2><span>Contact</span></h2>
		</div>
		<!-- <p class="subTitle cB"><%=lang_text(0)%></p> -->
		<div class="contBody contactCont">
			<h4><%=lang_text(1)%></h4>
			<ul>
                <li>
					<dl class="bgc5">
						<dt class="titleBar"><%=lang_text(2)%></dt>
						<dd>TEL : (02) 6333-5851</dd>
						<dd>E-mail : Eunji.kim@ifcseoul.com</dd>
						<!-- <dd class="gender mans"><span class="blind">남</span></dd> -->
					</dl>
                </li>
                <li>
                    <dl class="bgc5">
                        <dt class="titleBar"><%=lang_text(3)%></dt>
                        <dd>TEL : (02) 6333-5784</dd>
                        <dd>E-mail : Hyunju.chon@ifcseoul.com</dd>
                        <!-- <dd class="gender womans"><span class="blind">여</span></dd> -->
                    </dl>
                </li>
			</ul>
            <h4 class="mtm1">
                <%=lang_text(4)%>
            </h4>
            <ul>
                <li>
                    <dl class="bgc5">
                        <dt class="titleBar">
                            <%=lang_text(5)%>
                        </dt>
                        <dd>TEL : (02) 6333-5740</dd>
                        <dd>E-mail : james.tyrrell@ifcseoul.com</dd>
                        <!-- <dd class="gender man"><span class="blind">man</span></dd> -->
                    </dl>
                </li>
                <li>
                    <dl class="bgc5">
                        <dt class="titleBar">
                            <%=lang_text(6)%>
                        </dt>
                        <dd>TEL : (02) 6333-5745</dd>
                        <dd>Mobile :  010-3774-2199</dd>
                        <dd>E-mail : sw.lee@ifcseoul.com</dd>
                        <!-- <dd class="gender man"><span class="blind">man</span></dd> -->
                    </dl>
                </li>
            </ul>
            <ul>
                <li>
                    <dl class="bgc5">
                        <dt class="titleBar">
                            <%=lang_text(7)%>
                        </dt>
												<dd>TEL : (02) 6333-5747</dd>
                        <dd>Mobile :  010-9355-4136</dd>
                        <dd>E-mail : dennis.chung@ifcseoul.com</dd>
                        <!-- <dd class="gender man"><span class="blind">man</span></dd> -->
                    </dl>
                </li>
                <li>
                    <dl class="bgc5">
                        <dt class="titleBar">
                            <%=lang_text(8)%>
                        </dt>
                                                <dd>TEL : (02) 6333-5749</dd>
                        <dd>Mobile :  010-9751-5558</dd>
                        <dd>E-mail : my.park@ifcseoul.com</dd>
                        <!-- <dd class="gender man"><span class="blind">man</span></dd> -->
                    </dl>
                </li>
            </ul>
            <!--
            <h4 class="mtm1">
                <%=lang_text(9)%>
            </h4>
            <ul>
                <li>
                    <dl class="bgc5">
                        <dt class="titleBar">
                            <%=lang_text(10)%>
                        </dt>
                        <dd>TEL : (02) 6333-5700</dd>
                        <dd>E-mail : vivian.ahn@ifcseoul.com</dd>
                        <dd class="gender woman"><span class="blind">woman</span></dd>
                    </dl>
                </li>
                <li>
                    <dl class="bgc5">
                        <dt class="titleBar">
                            <%=lang_text(11)%>
                        </dt>
                        <dd>TEL : (02) 6137-5013</dd>
                        <dd>E-mail : dahn@taubman.com</dd>
                        <dd class="gender man"><span class="blind">man</span></dd>
                    </dl>
                </li>
            </ul>
            -->

            <h4 class="mtm1"><%=lang_text(12)%></h4>
             <dl class="bgc5">
                <dt class="titleBar"><%=lang_text(13)%></dt>
                <dd>TEL : (02) 6333-5872</dd>
                <dd>E-mail : Busung.choi@ifcseoul.com</dd>
                <!-- <dd class="gender malls"><span class="blind"><%=lang_text(14)%></span></dd> -->
            </dl>

            <h4 class="mtm1"><%=lang_text(14)%></h4>
            <dl class="bgc5">
                <dt class="titleBar"><%=lang_text(15)%></dt>
                <dd>TEL : (02) 6333-5742</dd>
                <dd>E-mail : Minhee.Woo@ifcseoul.com</dd>
            </dl>

			<h4 class="mtm1"><%=lang_text(16)%></h4>
			<dl class="bgc5">
				<dt class="titleBar"><%=lang_text(17)%></dt>
				<dd>TEL : (02) 6137-7000</dd>
				<dd>E-mail : Conrad_Seoul@conradhotels.com</dd>
				<!-- <dd class="gender conrads"><span class="blind"><%=lang_text(18)%></span></dd> -->
			</dl>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>
