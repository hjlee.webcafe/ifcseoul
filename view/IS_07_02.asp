<div id="content">
	<div class="contWrap">
		<div class="tabWrap type4">
			<ul>
				<li><a href="IS_07_01.asp"><%=gnb_text(13)%></a></li>
				<li class="other"><a href="IS_07_06.asp"><%=gnb_text(53)%></a></li>
				<li class="on"><a href="IS_07_02.asp"><%=gnb_text(14)%></a></li>
				<li class="other"><a href="IS_07_03.asp"><%=gnb_text(15)%></a></li>
				<li class=""><a href="IS_07_04.asp"><%=gnb_text(51)%></a></li>
				<li class="other"><a href="IS_07_05.asp"><%=gnb_text(52)%></a></li>
			</ul>
		</div>
		<div class="titleWrap titleIs0702 pSh" id="contTitle">
			<h2><a href="#none">Gallery</a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(0)%></dt>
				<dd><a href="IS_01_00.asp"><%=gnb_text(2)%></a></dd>
				<dd><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<!--<dd><a href="IS_03_00.asp"><%=gnb_text(8)%></a></dd>-->
				<dd><a href="IS_04_00.asp"><%=gnb_text(9)%></a></dd>
				<dd><a href="IS_05_00.asp"><%=gnb_text(10)%></a></dd>
				<dd><a href="IS_06_00.asp"><%=gnb_text(11)%></a></dd>
				<dd class="on"><a href="IS_07_01.asp"><%=gnb_text(12)%></a></dd>
			</dl>
			<h3><a href="#none"><%=lang_text(0)%></a></h3>
			<dl class="h3Siblings">
				<dt>Gallery</dt>
				<dd><a href="IS_07_01.asp"><%=gnb_text(13)%></a></dd>
				<dd class="on"><a href="IS_07_02.asp"><%=gnb_text(14)%></a></dd>
				<dd><a href="IS_07_03.asp"><%=gnb_text(15)%></a></dd>
				<dd><a href="IS_07_04.asp"><%=gnb_text(51)%></a></dd>
				<dd><a href="IS_07_05.asp"><%=gnb_text(52)%></a></dd>
				<dd><a href="IS_07_06.asp"><%=gnb_text(53)%></a></dd>
			</dl>
		</div>
		<p class="subTitle cB"><%=lang_text(1)%></p>
		<div class="contBody brochureCont">
			<div class="brochureArea"><span class="cover"></span></div>
			<div class="brochureBtn">
				<% If current_type = "ko" Then %>
				<a href="/ko/autoalbum/page/IFC_Seoul_Brochure(Korean)/view.html?FdirectPageNum[]=1" class="btn white brochure1" target="_blank">View Brochure</a>
				<a href="/download/IFC%20Seoul_Brochure(KOR)_final.zip" target="_blank" class="btn white brochure2">Brochure Download</a>
				<% Else %>
				<a href="/en/autoalbum/page/IFC_Seoul_Brochure(English)/view.html?FdirectPageNum[]=1" class="btn white brochure1" target="_blank">View Brochure</a>
				<a href="/download/IFC%20Seoul_Brochure(ENG)_final.zip" target="_blank" class="btn white brochure2">Brochure Download</a>
				<% End If %>
			</div>
			<div class="brochureCopy"><%=lang_text(2)%></div>
		</div>

		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>