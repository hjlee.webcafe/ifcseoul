<div id="content">
	<div class="contWrap">
		<div class="tabWrap">
			<ul>
				<li><a href="IS_02_01.asp"><%=gnb_text(4)%></a></li>
				<li class="other"><a href="IS_02_02.asp"><%=gnb_text(5)%></a></li>
				<li class="on"><a href="IS_02_03.asp"><%=gnb_text(6)%></a></li>
				<li class="other"><a href="IS_02_04.asp"><%=gnb_text(7)%></a></li>
			</ul>
		</div>
		<div class="titleWrap titleIs0203" id="contTitle">
			<h2><a href="#none"><%=gnb_text(4)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(0)%></dt>
				<dd><a href="IS_01_00.asp"><%=gnb_text(2)%></a></dd>
				<dd class="on"><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<!--<dd><a href="IS_03_00.asp"><%=gnb_text(8)%></a></dd>-->
				<dd><a href="IS_04_00.asp"><%=gnb_text(9)%></a></dd>
				<dd><a href="IS_05_00.asp"><%=gnb_text(10)%></a></dd>
				<dd><a href="IS_06_00.asp"><%=gnb_text(11)%></a></dd>
				<dd><a href="IS_07_01.asp"><%=gnb_text(12)%></a></dd>
			</dl>
			<h3><a href="#none"><%=lang_text(0)%></a></h3>
			<dl class="h3Siblings">
				<dt>IFC Seoul Project</dt>
				<dd><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<dd><a href="IS_02_02.asp"><%=gnb_text(5)%></a></dd>
				<dd class="on"><a href="IS_02_03.asp"><%=gnb_text(6)%></a></dd>
				<dd><a href="IS_02_04.asp"><%=gnb_text(7)%></a></dd>
			</dl>
		</div>
		<!-- <p class="subTitle"><%=lang_text(1)%></p> -->
		<div class="contVisual bgIs0203">
			<div class="introTitle">
				<p><%=lang_text(2)%></p>
				<p class="introTxt"><%=lang_text(3)%></p>
			</div>
		</div>
		<div class="contBody">
			<div class="listType4">
				<div class="imgArea">
					<img src="../img/com/img_cont3.jpg" alt="IFC Seoul Building External Image" />
				</div>
				<dl>
					<dt><%=lang_text(4)%></dt>
					<dd><%=lang_text(5)%></dd>
				</dl>
			</div>
			<div class="listType4 mt30">
				<div class="imgArea">
					<img src="../img/com/img_cont4.jpg" alt="IFC Seoul Building External Image" />
				</div>
				<dl>
					<dt><%=lang_text(6)%></dt>
					<dd><%=lang_text(7)%></dd>
				</dl>
			</div>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>