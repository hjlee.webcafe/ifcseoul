<script type="text/javascript" src="/js/lightbox.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/lightbox.css">

<% 
Dim countQuery,selectQuery,num_count,page_result, t_page, query_where,objRs , selet_query, g_type, select_name

' for custom page_limit and page_length
page_limit = 12
current_count = (page - 1) * page_limit 'set count number of current page
page_length = 5

g_type = Request.QueryString("type")

If g_type = "" or g_type = "all" Then
	g_type = "all"
	select_name = "ALL"
ElseIf g_type = 0 Then
	select_name = "Deloitte"
ElseIf g_type = 1 Then
	select_name = "BNY Mellon"
ElseIf g_type = 2 Then
	select_name = "Sony"
ElseIf g_type = 3 Then
	select_name = "OTIS"
End If
	
If g_type = "all" Then
	query_where = query_where
Else
	query_where = query_where & " and g_type = '"& g_type &"'"
End If

// counting list
countQuery = "select count(*) as count from dbo.tblTenantGall where 1 = 1 " & query_where

query_where = query_where & " order by g_num desc"

set objConn = OpenDBConnection()
set objRs = SendQuery(objConn,countQuery)
total_count = objRs("count")
num_count = total_count - (page - 1) * page_limit
set objRs = Nothing

// get list
selectQuery = "select Top " & page_limit & " * from dbo.tblTenantGall where g_num Not In (select Top " & current_count & " g_num from tblTenantGall where 1=1 "& query_where &") "& query_where
//selectQuery="select * from dbo.tblNotice"
set objRs = SendQuery(objConn,selectQuery)

t_page = page
page_result = page_create(total_count, t_page, page_limit, page_length)
%>
<div id="content">
	<div class="contWrap">
		<div class="tabWrap type4">
			<ul>
				<li><a href="IS_07_01.asp"><%=gnb_text(13)%></a></li>
				<li class="other on"><a href="IS_07_06.asp"><%=gnb_text(53)%></a></li>
				<li><a href="IS_07_02.asp"><%=gnb_text(14)%></a></li>
				<li class="other"><a href="IS_07_03.asp"><%=gnb_text(15)%></a></li>
				<li><a href="IS_07_04.asp"><%=gnb_text(51)%></a></li>
				<li class="other"><a href="IS_07_05.asp"><%=gnb_text(52)%></a></li>
			</ul>
		</div>
		<div class="titleWrap titleIs0701 pSh" id="contTitle">
			<h2><a href="#none">Gallery</a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(0)%></dt>
				<dd><a href="IS_01_00.asp"><%=gnb_text(2)%></a></dd>
				<dd><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<!--<dd><a href="IS_03_00.asp"><%=gnb_text(8)%></a></dd>-->
				<dd><a href="IS_04_00.asp"><%=gnb_text(9)%></a></dd>
				<dd><a href="IS_05_00.asp"><%=gnb_text(10)%></a></dd>
				<dd><a href="IS_06_00.asp"><%=gnb_text(11)%></a></dd>
				<dd class="on"><a href="IS_07_01.asp"><%=gnb_text(12)%></a></dd>
			</dl>
			<h3><a href="#none"><%=lang_text(0)%></a></h3>
			<dl class="h3Siblings">
				<dt>Gallery</dt>
				<dd><a href="IS_07_01.asp"><%=gnb_text(13)%></a></dd>
				<dd><a href="IS_07_02.asp"><%=gnb_text(14)%></a></dd>
				<dd><a href="IS_07_03.asp"><%=gnb_text(15)%></a></dd>
				<dd><a href="IS_07_04.asp"><%=gnb_text(51)%></a></dd>
				<dd><a href="IS_07_05.asp"><%=gnb_text(52)%></a></dd>
				<dd class="on"><a href="IS_07_06.asp"><%=gnb_text(53)%></a></dd>
			</dl>
		</div>
		<p class="subTitle cB"><%=lang_text(1)%></p>
		<div class="contBody galleryCont">
			<div class="carouselWrap" id="carousel">
				<ul class="carouselArea">
					<!-- li>
						<div class="zoomBtn"><a href="#?w=963" class="btn zoom jsbtnLyp" rel="layerPop01"><span class="blind">ZOOM</span></a></div>
						<img src="<%=objRs("fg_img")%>" alt="" height="521px" class="previewImg" />
					</li-->
					<li>
						<div class="zoomBtn"><a href="<%=objRs("g_img")%>" class="btn zoom" data-lightbox="gallery" title="<%=objRs("g_title_" & current_type)%>"><span class="blind">ZOOM</span></a></div>
						<img src="<%=objRs("g_img")%>" alt="" class="previewImg" />
					</li>
				</ul>
			</div>
			<!--div class="selectList mt10">
				<p><a href="#none"><%=select_name%></a></p>
				<ul class="hide">
					<li><a href="?type=all">ALL</a></li>
					<li><a href="?type=0">Deloitte</a></li>
					<li><a href="?type=1">BNY Mellon</a></li>
					<li><a href="?type=2">Sony</a></li>
					<li><a href="?type=3">OTIS</a></li>
				</ul>
			</div-->
			<div class="tempWrap" style="top: -40px">
				<ul class="tempArea mt20" style="width: 100%;">
					<% if objRs.EOF Then %>
					<li>
						
					</li>
					<% Else %>
						<% For k = 0 to objRs.RecordCount - 1 %>
						<%
						if k = 0 then
							previewImg = objRs("g_img")
						end if
						%>
					<li>
						<a href="#" data-url="<%=objRs("g_img")%>"><img src="<%=objRs("g_img")%>" width="82px" height="82px" alt="<%=objRs("g_title_" & current_type)%>" /><span class="text"><span class="bgBar"><%=objRs("g_list_title")%><input type="hidden" class="img_title" value="<%=objRs("g_title_" & current_type)%>" /></span></span></a>
					</li>
							<% objRs.MoveNext %>
						<% Next %>
					<% End If %>
					<% objRs.MoveFirst %>
				</ul>
				<div class="paginate">
					<!-- #include file = "main_page_template.asp" -->
				</div>
			</div>


		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>

        <!-- 이미지 미리보기 -->
        <div id="layerPop01" class="lyPopWrap" style="width:963px">
            <div class="lyPopBody">
                <div class="lyPopContWrap">
                    <div class="previewImgWrap"><img src="<%=previewImg %>" alt="" class="previewImg" /></div>
                </div>
            </div>
            <a href="#none" class="popCls jsPopClose">close</a>
        </div>
	</div>
</div>

<!-- image lightbox list -->
<div class="hide_row lightbox_list">
<% For k = 0 to objRs.RecordCount - 1 %>
<a href="<%=objRs("g_img")%>" data-lightbox="gallery" title="<%=objRs("g_title_" & current_type)%>"></a>
	<% objRs.MoveNext %>
<% Next %>
</div>
<!-- image lightbox list end -->
