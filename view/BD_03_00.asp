<div id="content">
	<div class="contWrap">
		<div class="titleWrap contBg bgBd0300" id="contTitle">
			<h2><a href="#none"><%=lang_text(0)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(12)%></dt>
				<dd><a href="BD_01_00.asp"><%=gnb_text(17)%></a></dd>
				<dd><a href="BD_02_00.asp"><%=gnb_text(18)%></a></dd>
				<dd class="on"><a href="BD_03_00.asp"><%=gnb_text(19)%></a></dd>
				<dd><a href="BD_04_00.asp"><%=gnb_text(20)%></a></dd>
				<dd><a href="BD_05_00.asp"><%=gnb_text(21)%></a></dd>
				<!--dd><a href="BD_06_00.asp"><%=gnb_text(22)%></a></dd-->
				<dd><a href="BD_07_01.asp"><%=gnb_text(23)%></a></dd>
				<!--<dd><a href="BD_08_01.asp"><%=gnb_text(30)%></a></dd>-->
			</dl>
		</div>
		
		<!-- <p class="subTitle"><%=lang_text(1)%></p> -->
		<div class="contVisual bgBd0300">
			
			<div class="introTitle">
				<p><%=lang_text(2)%></p>
				<p class="introTxt"><%=lang_text(3)%></p>
			</div>
		</div>

		<div class="contBody featuresCont">
			<ul class="listType3">
				<li>
					<div class="img"><img src="../img/com/img_features_01.jpg" alt="Rainwater Harvesting Related Images" /></div>
					<dl>
						<dt><%=lang_text(4)%></dt>
						<dd><%=lang_text(5)%></dd>
					</dl>
				</li>
				<li>
					<div class="img"><img src="../img/com/img_features_02.jpg" alt="Solar Energy Related Images" /></div>
					<dl>
						<dt><%=lang_text(6)%></dt>
						<dd><%=lang_text(7)%></dd>
					</dl>
				</li>
				<li>
					<div class="img"><img src="../img/com/img_features_03.jpg" alt="Air Quality Related Images" /></div>
					<dl>
						<dt><%=lang_text(8)%></dt>
						<dd><%=lang_text(9)%></dd>
					</dl>
				</li>
				<li>
					<div class="img"><img src="../img/com/img_features_04.jpg" alt="Security System Related Images" /></div>
					<dl>
						<dt><%=lang_text(10)%></dt>
						<dd><%=lang_text(11)%></dd>
					</dl>
				</li>
			</ul>
			<p class="mb20 fs18"><%=lang_text(12)%></p>
			<ul class="listType3 oneImg">
				<li>
					<div class="img"><img src="../img/com/img_features_05.jpg" alt="IFC Mall Ceiling Light Image" /></div>
					<p><%=lang_text(13)%></p>
				</li>
			</ul>
			<p class="mb20 fs18"><%=lang_text(14)%></p>
			<ul class="listType3">
				<li>
					<div class="img"><img src="../img/com/img_features_06.jpg" alt="Flexible Floor Space Image" /></div>
					<dl>
						<dt><%=lang_text(15)%></dt>
						<dd><%=lang_text(16)%></dd>
					</dl>
				</li>
				<li>
					<div class="img"><img src="../img/com/img_features_07.jpg" alt="Solid Support Related Images" /></div>
					<dl>
						<dt><%=lang_text(17)%></dt>
						<dd><%=lang_text(18)%></dd>
					</dl>
				</li>
			</ul>
			<p class="mb20 fs18"><%=lang_text(19)%></p>
			<ul class="listType3 oneImg">
				<li>
					<div class="img"><img src="../img/com/img_features_08.jpg" alt="3meter Ceilings Related Images" /></div>
					<dl>
						<dt><%=lang_text(20)%></dt>
						<dd><%=lang_text(21)%></dd>
						<dt class="pt20"><%=lang_text(22)%></dt>
						<dd><%=lang_text(23)%></dd>
					</dl>
				</li>
				<!-- <li>
					<div class="img"><img src="../img/com/img_features_09.jpg" alt="Raised Floors to Accommodate Wires and Cables Related Images" /></div>
					<dl>
						
					</dl>
				</li> -->
				<li>
					<div class="img"><img src="../img/com/img_features_10.jpg" alt="Vision Glass for Spectacular Views Related Images" /></div>
					<dl>
						<dt><%=lang_text(24)%></dt>
						<dd><%=lang_text(25)%></dd>
					</dl>
				</li>
			</ul>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>