<div id="content">
	<div class="contWrap">
		<div class="tabWrap">
			<ul>
				<li class=""><a href="IS_02_01.asp"><%=gnb_text(4)%></a></li>
				<li class="other on"><a href="IS_02_02.asp"><%=gnb_text(5)%></a></li>
				<li><a href="IS_02_03.asp"><%=gnb_text(6)%></a></li>
				<li class="other"><a href="IS_02_04.asp"><%=gnb_text(7)%></a></li>
			</ul>
		</div>
		<div class="titleWrap titleIs0202">
			<h2><a href="#none"><%=gnb_text(4)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(0)%></dt>
				<dd><a href="IS_01_00.asp"><%=gnb_text(2)%></a></dd>
				<dd class="on"><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<!--<dd><a href="IS_03_00.asp"><%=gnb_text(8)%></a></dd>-->
				<dd><a href="IS_04_00.asp"><%=gnb_text(9)%></a></dd>
				<dd><a href="IS_05_00.asp"><%=gnb_text(10)%></a></dd>
				<dd><a href="IS_06_00.asp"><%=gnb_text(11)%></a></dd>
				<dd><a href="IS_07_01.asp"><%=gnb_text(12)%></a></dd>
			</dl>
			<h3><a href="#none"><%=lang_text(0)%></a></h3>
			<dl class="h3Siblings">
				<dt>IFC Seoul Project</dt>
				<dd><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<dd class="on"><a href="IS_02_02.asp"><%=gnb_text(5)%></a></dd>
				<dd><a href="IS_02_03.asp"><%=gnb_text(6)%></a></dd>
				<dd><a href="IS_02_04.asp"><%=gnb_text(7)%></a></dd>
			</dl>
		</div>
		<!-- <p class="subTitle"><%=lang_text(1)%></p> -->
		<div class="contVisual bgIs0202">
			<div class="introTitle">
				<p><%=lang_text(2)%></p>
				<p class="introTxt"><%=lang_text(3)%></p>
			</div>
		</div>
		<div class="contBody">
			<div class="listType4">
				<div class="imgArea">
					<img src="../img/com/img_cont1.jpg" alt="IFC Seoul Building External Image" />
					<span class="bgBox8">&nbsp;</span>
				</div>
				<dl>
					<dt><%=lang_text(4)%></dt>
					<dd><%=lang_text(5)%></dd>
					<dd><%=lang_text(6)%></dd>
					<dd><%=lang_text(7)%></dd>
				</dl>
			</div>

			<div class="contVisual bgIs0202_1" id="contTitle">
				<div class="introTitle">
					<p><%=lang_text(8)%></p>
					<p class="introTxt"><%=lang_text(9)%></p>
				</div>
			</div>
			<div class="listType4">
				<div class="imgArea right">
					<img src="../img/com/img_cont2.jpg" alt="IFC Seoul Building Night View Image" />
					<span class="bgBox8">&nbsp;</span>
				</div>
				<dl>
					<dt><%=lang_text(10)%></dt>
					<dd><%=lang_text(11)%></dd>
					<dd><%=lang_text(12)%></dd>
					<dd><%=lang_text(13)%></dd>
					<!-- <dd><%=lang_text(14)%></dd> -->
					<dt><%=lang_text(14)%></dt>
					<dd><%=lang_text(15)%></dd>
					<dt><%=lang_text(16)%></dt>
					<dd><%=lang_text(17)%></dd>
				</dl>
			</div>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>