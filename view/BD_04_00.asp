<script type="text/javascript" src="/js/pano2vr_player.js"></script>
<script type="text/javascript" src="/js/skin.js"></script>
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	$('.sliderRooms, .sliderMeetingEvent').bxSlider({
		infiniteLoop: false,
		hideControlOnEnd: true
	});
	$('.slider3dLayout').bxSlider({
		mode: 'fade',
		pagerCustom: '#bx-pager'
	});
});
</script>

<div id="content">
	<div class="contWrap">
		<div class="titleWrap titleBd0400 pSh" id="contTitle">
			<h2><a href="#none"><%=lang_text(0)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(12)%></dt>
				<dd><a href="BD_01_00.asp"><%=gnb_text(17)%></a></dd>
				<dd><a href="BD_02_00.asp"><%=gnb_text(18)%></a></dd>
				<dd><a href="BD_03_00.asp"><%=gnb_text(19)%></a></dd>
				<dd class="on"><a href="BD_04_00.asp"><%=gnb_text(20)%></a></dd>
				<dd><a href="BD_05_00.asp"><%=gnb_text(21)%></a></dd>
				<!--dd><a href="BD_06_00.asp"><%=gnb_text(22)%></a></dd-->
				<dd><a href="BD_07_01.asp"><%=gnb_text(23)%></a></dd>
				<!--<dd><a href="BD_08_01.asp"><%=gnb_text(30)%></a></dd>-->
			</dl>
		</div>
		<p class="subTitle cB"><%=lang_text(1)%></p>
		<div class="contBody">
			<div class="floorPlansWrap">
				<div class="floorPlansArea">
					<div class="floorPlansMap">
						<div class="pR">
							<img src="../img/com/img_floor_plans.jpg" alt="IFC Seoul Building Plan Image" />
							<ul>
								<li class="plansMap1 on"><a href="#"><span class="blind">TWO IFC</span></a></li>
								<li class="plansMap2 on"><a href="#"><span class="blind">ONE IFC</span></a></li>
								<li class="plansMap3 on"><a href="#"><span class="blind">THREE IFC</span></a></li>
								<li class="plansMap4"><a href="#"><span class="blind">IFC MALL</span></a></li>
								<li class="plansMap5"><a href="#"><span class="blind">CONRED SEOUL</span></a></li>
							</ul>
						</div>
					</div>
					<div class="floorPlansInfo itemWrap01">
						<div class="PlansMoreImg">
							<span class="floorInfo side1"><span class="blind">Lobby</span></span>
							<span class="floorInfo side2"><span class="blind">Office Layout</span></span>
							<span class="floorInfo side3"><span class="blind">Panorama View</span></span>
							<span class="floorInfo side4"><span class="blind">Office Floor Perspective Layout</span></span>
						</div>
						<div class="floorPlansBtn">
							<ul>
								<li class="on"><a href="#THREEIFC_Lobby"><%=lang_text(25)%></a></li>
								<li><a href="#THREEIFC_OfficeLayout"><%=lang_text(26)%></a></li>
								<li><a href="#THREEIFC_PanoramaView"><%=lang_text(27)%></a></li>
								<li><a href="#THREEIFC_3DLayout"><%=lang_text(28)%></a></li>
							</ul>
						</div>
					</div>
                    <div class="floorPlansInfo itemWrap02">
						<div class="PlansMoreImg">
							<span class="floorInfo side5"><span class="blind">L1</span></span>
							<span class="floorInfo side6"><span class="blind">L2</span></span>
							<span class="floorInfo side7"><span class="blind">L3</span></span>
						</div>
						<div class="floorPlansBtn">
							<ul>
								<li class="on"><a href="#L1">L1</a></li>
								<li><a href="#L2">L2</a></li>
								<li><a href="#L3">L3</a></li>
							</ul>
						</div>
					</div>
                    <div class="floorPlansInfo itemWrap03">
						<div class="PlansMoreImg">
							<span class="floorInfo side8"><span class="blind">Rooms</span></span>
							<span class="floorInfo side9"><span class="blind">Restaurant &amp; bar</span></span>
							<span class="floorInfo side10"><span class="blind">Meeting &amp; Event</span></span>
							<span class="floorInfo side11"><span class="blind">Fitness club &amp; SPA</span></span>
						</div>
						<div class="floorPlansBtn">
							<ul>
								<li class="on"><a href="#Room"><%=lang_text(29)%></a></li>
								<li><a href="#Restaurantbar"><%=lang_text(30)%></a></li>
								<li><a href="#MeetingEvent"><%=lang_text(31)%></a></li>
								<li><a href="#FitnessclubSPA"><%=lang_text(32)%></a></li>
							</ul>
						</div>
					</div>
				</div>

				<!--<div class="floorPlansControl jsCarouselPaging">
					<ul>
						<li class="on"><a href="#"><span class="blind">first</span></a></li>
						<li><a href="#"><span class="blind">second</span></a></li>
						<li><a href="#"><span class="blind">third</span></a></li>
					</ul>
				</div>-->
			</div>
            
            
			<div class="IfcOfficeTowers">
				<h3 class="floorPlansTitle"><%=lang_text(2)%></h3>
				<!-- 로비 -->
				<div class="ifcSection type1" id="THREEIFC_Lobby">
					<div class="sectionCont">
						<h4><%=lang_text(3)%><br /><!-- <strong>IFC L01</strong> --></h4>
						<p><%=lang_text(4)%></p>
					</div>
					<div class="imgWra">
						<img src="../img/com/img_robby1.jpg" alt="" />
					</div>
					<div class="bgBox">&nbsp;</div>
				</div>
				<!-- //로비 -->
				<div class="topBtn"><a href="#" class="btn top">top</a></div>
				<!-- 오피스 레이아웃 -->
				<div class="ifcSection type3" id="THREEIFC_OfficeLayout">
					<div class="sectionCont">
						<h4><%=lang_text(5)%><br /><!-- <strong>IFC L03 ~ L54</strong> --></h4>
						<dl>
							<dt><%=lang_text(6)%></dt>
							<dd><%=lang_text(7)%></dd>
						</dl>
					</div>
					<div class="imgWrap">
						<div class="imgArea"><img src="../img/com/img_office_layout1.jpg" alt="Office Layout Image" /> <img src="../img/com/img_office_layout2.jpg" alt="Office Layout 3D Structure Image" /></div>
						<div class="viewFullBtn"><a href="#?w=595" class="jsbtnLyp" rel="layerPop01"><span>View Full Size</span></a></div>	
					</div>
				</div>
				<div id="layerPop01" class="lyPopWrap">
					<div class="lyPopBody">
						<div class="lyPopContWrap">
							<img src="../img/com/img_office_layout3.jpg" alt="Office Layout 3D Structure Image" />
						</div>
					</div>
					<a href="#none" class="popCls jsPopClose">Close</a>
				</div>
				<!-- //오피스 레이아웃 -->
				<div class="topBtn"><a href="#" class="btn top">top</a></div>
				<!-- 파노라마 뷰 -->
				<div class="ifcSection type4" id="THREEIFC_PanoramaView">
					<div class="sectionCont">
						<h4><%=lang_text(18)%> <!-- <strong>IFC L03 ~L54</strong> --></h4>
					</div>
					<div class="imgWrap">
						<div id="pano" style="width:100%;height:254px;">
						This content requires HTML5/CSS3, WebGL, or Adobe Flash Player Version 9 or higher.
						</div>
						<div id="flashContent">
							<p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
						</div>
						<noscript>
							<p><b>Please enable Javascript!</b></p>
						</noscript>
					</div>
				</div>
				<!-- //파노라마 뷰 -->
				<div class="topBtn"><a href="#" class="btn top">top</a></div>
				<!-- 3D 레이아웃 -->
				<div class="ifcSection type6" id="THREEIFC_3DLayout">
					<div class="sectionCont">
						<h4><%=lang_text(19)%></h4>
					</div>
					<div class="imgWrap">
						<ul class="slider3dLayout">
							<!-- <li><img src="../img/com/img_panorama_view1.jpg" alt="3D layout Image" /></li> -->
							<li><img src="../img/com/img_panorama_view2.jpg" alt="3D layout Image" /></li>
							<li><img src="../img/com/img_panorama_view3.jpg" alt="3D layout Image" /></li>
						</ul>
						<div id="bx-pager">
							<!-- <a data-slide-index="0" href="#none"><img src="../img/com/img_panorama_view1.jpg" alt="" /></a> -->
							<a data-slide-index="0" href="#none"><img src="../img/com/img_panorama_view2.jpg" alt="" /></a>
							<a data-slide-index="1" href="#none"><img src="../img/com/img_panorama_view3.jpg" alt="" /></a>
						</div>
						<div class="tempAreabg"></div>
					</div>
				</div>
				<!-- //3D 레이아웃 -->
				<div class="topBtn"><a href="#" class="btn top">top</a></div>
			</div>
			<div class="IfcMall hide">
				<h3 class="floorPlansTitle"><%=lang_text(8)%></h3>
				<!-- Rooms -->
				<div class="ifcSection type3" id="L1">
					<div class="sectionCont">
						<h4><strong>L1</strong> Fashion</h4>
					</div>
					<div class="imgWrap">
						<ul class="">
							<li><img src="../img/com/img_ifcmall1_<%=current_type%>.jpg" alt="<%=lang_text(22)%>" /></li>
						</ul>
					</div>
					
					<div class="bgBox">&nbsp;</div>
				</div>
				<!-- //Rooms -->
				<div class="topBtn"><a href="#" class="btn top">top</a></div>
				<!-- Fashion / YP Books -->
				<div class="ifcSection type3" id="L2">
					<div class="sectionCont">
						<h4><strong>L2</strong> Fashion / YP Books</h4>
					</div>
					<div class="imgWrap">
						<ul class="">
							<li><img src="../img/com/img_ifcmall2_<%=current_type%>.jpg" alt="<%=lang_text(23)%>" /></li>
						</ul>
					</div>
					
					<div class="bgBox">&nbsp;</div>
				</div>
				<!-- //Fashion / YP Books -->
				<div class="topBtn"><a href="#" class="btn top">top</a></div>
				<!-- F&B / CGV -->
				<div class="ifcSection type3" id="L3">
					<div class="sectionCont">
						<h4><strong>L3</strong> F&amp;B / CGV</h4>
					</div>
					<div class="imgWrap">
						<ul class="">
							<li><img src="../img/com/img_ifcmall3_<%=current_type%>.jpg" alt="<%=lang_text(24)%>" /></li>
						</ul>
					</div>
					<div class="bgBox">&nbsp;</div>
				</div>
				<!-- //F&B / CGV -->
				<div class="topBtn"><a href="#" class="btn top">top</a></div>
			</div>
			<div class="ConradSeoulHotel hide">
				<h3 class="floorPlansTitle"><%=lang_text(9)%></h3>
				<!-- Rooms -->
				<div class="ifcSection type1" id="Room">
					<div class="sectionCont">
						<h4><%=lang_text(10)%></h4>
						<p><%=lang_text(11)%></p>
					</div>
					<div class="imgWrap">
						<ul class="sliderRooms">
							<li><img src="../img/com/img_conrad1.jpg" alt="" /></li>
							<li><img src="../img/com/img_conrad2.jpg" alt="" /></li>
							<li><img src="../img/com/img_conrad3.jpg" alt="" /></li>
						</ul>
						<div class="btn_wrap_bg"></div>
					</div>
				</div>
				<!-- //Rooms -->
				<div class="topBtn"><a href="#" class="btn top">top</a></div>
				<!-- Restaurant & bar -->
				<div class="ifcSection type1" id="Restaurantbar">
					<div class="sectionCont">
						<h4><%=lang_text(12)%></h4>
						<p><%=lang_text(13)%></p>
					</div>
					<div class="imgWrap">
						<ul class="">
							<li><img src="../img/com/img_conrad4.jpg" alt="" /></li>
						</ul>
					</div>
					<div class="bgBox">&nbsp;</div>
				</div>
				<!-- //Restaurant & bar -->
				<div class="topBtn"><a href="#" class="btn top">top</a></div>
				<!-- Meeting & Event  -->
				<div class="ifcSection type1" id="MeetingEvent">
					<div class="sectionCont">
						<h4><%=lang_text(14)%></h4>
						<p><%=lang_text(15)%></p>
					</div>
                    <div class="imgWrap">
						<ul class="sliderMeetingEvent">
							<li><img src="../img/com/img_conrad5.jpg" alt="" /></li>
							<li><img src="../img/com/img_conrad6.jpg" alt="" /></li>
						</ul>
					</div>
				</div>
				<!-- //Meeting & Event  -->
				<div class="topBtn"><a href="#" class="btn top">top</a></div>
				<!-- Fitness club & SPA -->
				<div class="ifcSection type1" id="FitnessclubSPA">
					<div class="sectionCont">
						<h4><%=lang_text(16)%></h4>
						<p><%=lang_text(17)%></p>
					</div>
					<div class="imgWrap">
						<ul class="">
							<li><img src="../img/com/img_conrad7.jpg" alt="" /></li>
						</ul>
					</div>	
					<div class="bgBox">&nbsp;</div>
				</div>
				<!-- //Fitness club & SPA -->
				<div class="topBtn"><a href="#" class="btn top">top</a></div>
			</div>
		</div>
	</div>
</div>