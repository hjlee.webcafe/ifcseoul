<%
Dim countQuery,selectQuery,num_count,page_result, t_page, query_where,objRs , selet_query

query_where = ""

if(current_type = "ko") then
query_where = "and p_group='0' "&query_where
else
query_where = "and p_group='1' "&query_where
end if

// counting list
countQuery = "select count(*) as count from dbo.tblPress where 1 = 1 "& query_where

set objConn = OpenDBConnection()
set objRs = SendQuery(objConn,countQuery)
total_count = objRs("count")
num_count = total_count - (page - 1) * page_limit
set objRs = Nothing

// get list
query_where = query_where & " order by p_num desc"
selectQuery = "select Top " & page_limit & " * from dbo.tblPress where p_num Not In (select Top " & current_count & " p_num from tblPress where 1 = 1 "& query_where &") "& query_where
set objRs = SendQuery(objConn,selectQuery)

//ex
select_query = "select * from dbo.tblPress"
set objRs1 = SendQuery(objConn,select_query)

t_page = page
page_result = page_create(total_count, t_page, page_limit, page_length)
%>

<div id="content">
	<div class="contWrap">
		<div class="titleWrap titleNe0200 pSh" id="contTitle">
			<h2><a href="#none"><%=lang_text(0)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(36)%></dt>
				<dd><a href="NE_01_00.asp"><%=gnb_text(37)%></a></dd>
				<dd class="on"><a href="NE_02_00.asp"><%=gnb_text(38)%></a></dd>
				<dd><a href="NE_03_00.asp"><%=gnb_text(39)%></a></dd>
			</dl>
		</div>
		<!-- <p class="subTitle cB"><%=lang_text(1)%></p> -->
		<div class="contBody noticeTbl">
			<table border="1" summary="<%=lang_text(6)%>" class="tblType noticeList1">
				<caption><%=lang_text(7)%></caption>
				<colgroup>
					<col />
					<col />
					<col style="width:21%" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col" class="number"><%=lang_text(2)%></th>
						<th scope="col"><%=lang_text(3)%></th>
						<th scope="col"><%=lang_text(4)%></th>
					</tr>
				</thead>
				<tbody>
					<% if objRs.EOF Then %>
						<tr>
							<td colspan="3" align="center">
								<%=lang_text(5)%>
							</td>
						</tr>
					<% Else %>
						<% For k = 0 to objRs.RecordCount - 1 %>
					<tr>
						<td class="number"><%=num_count-k%></td>
						<td class="txtL"><a href="NE_02_01.asp?num=<%=objRs("p_num")%>&list_num=<%=objRs.RecordCount-k%>"><%=objRs("p_title")%></a></td>
						<td><%=Left(objRs("p_reg_date"), 10)%></td>
					</tr>
							<% objRs.MoveNext %>
						<% Next %>
					<% End If %>
				</tbody>
			</table>

			<div class="paginate">
				<!-- #include file = "main_page_template.asp" -->
			</div>
		</div>
	</div>
</div>