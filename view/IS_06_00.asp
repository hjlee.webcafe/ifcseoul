<div id="content">
	<div class="contWrap">
		<div class="titleWrap titleIs0600" id="contTitle">
			<h2><a href="#none"><%=lang_text(0)%></a></h2>
			<dl class="h2Siblings">
				<dt><%=gnb_text(0)%></dt>
				<dd><a href="IS_01_00.asp"><%=gnb_text(2)%></a></dd>
				<dd><a href="IS_02_01.asp"><%=gnb_text(4)%></a></dd>
				<!--<dd><a href="IS_03_00.asp"><%=gnb_text(8)%></a></dd>-->
				<dd><a href="IS_04_00.asp"><%=gnb_text(9)%></a></dd>
				<dd><a href="IS_05_00.asp"><%=gnb_text(10)%></a></dd>
				<dd class="on"><a href="IS_06_00.asp"><%=gnb_text(11)%></a></dd>
				<dd><a href="IS_07_01.asp"><%=gnb_text(12)%></a></dd>
			</dl>
		</div>
		<!-- <p class="subTitle"><%=lang_text(1)%></p> -->
		<div class="contVisual bgIs0600">
			<div class="imgAward"><img src="../img/com/img_award.png" alt="LEED Gold Certification for<br />Leadership in Energy and Environmental design Image" /></div>
			<div class="introTitle">
				<%=lang_text(2)%>
			</div>
		</div>
		<div class="contBody awardCont">
			<dl class="other">
				<dt>
					<strong><%=lang_text(8)%></strong>
					November 2013 by U.S. Green Building Council
				</dt>
				<dd><img src="../img/com/img_aword07.jpg"alt="Leed gold certification for Leadership in energy and environmental design Image" /></dd>
			</dl>
			<dl class="other">
				<dt>
					<strong><%=lang_text(3)%></strong>
					November 21, 2012 by Youngdeungpo-gu
				</dt>
				<dd><img src="../img/com/img_aword01.jpg"alt="Grand prize at the 3rd Youngdeungpo Design Awards for architecture and design Image" /></dd>
			</dl>
			<dl>
				<dt>
					<strong><%=lang_text(4)%></strong>
					October 15, 2013 by Korea Institute of Registered Architects
				</dt>
				<dd><img src="../img/com/img_aword02.jpg"alt="Achievement Award at 2013 Korean Architecture Award Image" /></dd>
			</dl>
			<dl>
				<dt>
					<strong><%=lang_text(5)%></strong>
					October 21, 2013 by Seoul Metro Government
				</dt>
				<dd><img src="../img/com/img_aword03.jpg"alt="Excellence award at the 31th Seoul architecture prize Image" /></dd>
			</dl>
			<!-- <dl>
				<dt>
					<strong><%=lang_text(6)%></strong>
					October 15, 2013 by Korea Institute of Registered Architects
				</dt>
				<dd><img src="../img/com/img_aword04.jpg"alt="Achievement Award at 2013 Korean Architecture Award Image" /></dd>
			</dl> -->
			<dl>
				<dt>
					<strong><%=lang_text(7)%></strong>
					October 9, 2012 by Korea’s Green Building Council
				</dt>
				<dd><img src="../img/com/img_aword05.jpg"alt="Korea’s Green Building certification Image" /></dd>
			</dl>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>