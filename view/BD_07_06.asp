<div id="content">
	<div class="contWrap">
		<div class="tabWrap type4">
			<ul>
				<li><a href="BD_07_01.asp"><span>CONRAD Seoul</span></a></li>
				<li class="other"><a href="BD_07_02.asp"><span>IFC Mall</span></a></li>
				<li><a href="BD_07_03.asp"><span>Rest Places</span></a></li>
				<li class="other"><a href="BD_07_04.asp"><span>Services</span></a></li>
				<li class=""><a href="BD_07_05.asp"><span>Parking</span></a></li>
				<li class="on other"><a href="BD_07_06.asp"><span>Security &amp; Safety</span></a></li>
				<li class=""><a href="BD_07_07.asp"><span>Gallery</span></a></li>
			</ul>
		</div>
		<div class="titleWrap titleBd0706" id="contTitle">
			<h2><a href="#none">Amenities</a></h2>
			<dl class="h2Siblings">
				<dt>Buildings</dt>
				<dd><a href="BD_01_00.asp">Overview</a></dd>
				<dd><a href="BD_02_00.asp">Concept</a></dd>
				<dd><a href="BD_03_00.asp">Features</a></dd>
				<dd><a href="BD_04_00.asp">Floor Plans</a></dd>
				<dd><a href="BD_05_00.asp">Specifications</a></dd>
				<!--dd><a href="BD_06_00.asp"><%=gnb_text(22)%></a></dd-->
				<dd class="on"><a href="BD_07_01.asp">Amenities</a></dd>
				<!--<dd><a href="BD_08_01.asp">Leasing</a></dd>-->
			</dl>
			<h3><a href="#none">Security &amp; Safety</a></h3>
			<dl class="h3Siblings">
				<dt>Amenities</dt>
				<dd><a href="BD_07_01.asp">CONRAD Seoul</a></dd>
				<dd><a href="BD_07_02.asp">IFC Mall</a></dd>
				<dd><a href="BD_07_03.asp">Rest Places</a></dd>
				<dd><a href="BD_07_04.asp">Services</a></dd>
				<dd><a href="BD_07_05.asp">Parking</a></dd>
				<dd class="on"><a href="BD_07_06.asp">Security &amp; Safety</a></dd>
				<dd><a href="BD_07_07.asp">Gallery</a></dd>
			</dl>
		</div>
		<div class="contVisual bgBd0706">
			<span class="subTitle">IFC Seoul, New Landmark of Yeouido</span>
			<div class="introTitle">
				<p><span>세계가 주목하는 <strong>완벽한 새로움</strong></span><br />The NEW, <strong>IFC Seoul</strong></p>
				<p class="introTxt">여의도에 자리한 최초의 복합쇼핑공간,IFC몰은 24시간도 부족한 비즈니스 리더들에게 더할 나위 없이 만족스러운 장소입니다. 멀리가지 않아도 쇼핑을 즐길 수 있고 영화와 도서, 공연까지 충분히 누릴 수 있습니다. 업무의 스트레스를 잠시 잊게 해주는 행복한 공간입니다.</p>
			</div>
		</div>
		<div class="contBody">
			<h4>AIG Global Real Estate</h4>
			<div class="txtCont">
				<p>Strengthen the geographic position of Yeouido as an international finance hub by building IFC Seoul and actively attracting leading foreign finance corporations. Provide finance corporations and investors with new opportunities brought by the remarkable business environment of Yeouido and the potential in the South Korean finance market</p>
			</div>
			<ul class="listType2">
				<li>
					<div class="img"><img src="../img/temp/@img_320x320.jpg" alt="" /></div>
					<dl>
						<dt class="titleBar">AIG Global</dt>
						<dd>Construct the comprehensive support system related to finance such as the global business Services support center and etc.</dd>
					</dl>
				</li>
				<li>
					<div class="img"><img src="../img/temp/@img_320x320.jpg" alt="" /></div>
					<dl>
						<dt class="titleBar">AIG Global</dt>
						<dd>Strengthen the geographic position of Yeouido as an international finance hub by building IFC Seoul and actively attracting leading foreign finance corporations.</dd>
					</dl>
				</li>
				<li>
					<div class="img"><img src="../img/temp/@img_320x320.jpg" alt="" /></div>
					<dl>
						<dt class="titleBar">AIG Global</dt>
						<dd>IFC Seoul features Conrad Seoul Hotel, which is the finest five-star luxury business hotel among the affiliates of Hilton Worldwide and it is the first for South Korea.</dd>
					</dl>
				</li>
				<li>
					<div class="img"><img src="../img/temp/@img_320x320.jpg" alt="" /></div>
					<dl>
						<dt class="titleBar">AIG Global</dt>
						<dd>Expand the variety of infrastructures necessary for financial firm’s move-in to strengthen its position as international financial cluster.</dd>
					</dl>
				</li>
				<li>
					<div class="img"><img src="../img/temp/@img_320x320.jpg" alt="" /></div>
					<dl>
						<dt class="titleBar">AIG Global</dt>
						<dd>Construct the comprehensive support system related to finance such as the global business Services support center and etc.</dd>
					</dl>
				</li>
				<li>
					<div class="img"><img src="../img/temp/@img_320x320.jpg" alt="" /></div>
					<dl>
						<dt class="titleBar">AIG Global</dt>
						<dd>Strengthen the geographic position of Yeouido as an international finance hub by building IFC Seoul and actively attracting leading foreign finance corporations.</dd>
					</dl>
				</li>
				<li>
					<div class="img"><img src="../img/temp/@img_320x320.jpg" alt="" /></div>
					<dl>
						<dt>AIG Global</dt>
						<dd>IFC Seoul features Conrad Seoul Hotel, which is the finest five-star luxury business hotel among the affiliates of Hilton Worldwide and it is the first for South Korea.</dd>
					</dl>
				</li>
				<li>
					<div class="img"><img src="../img/temp/@img_320x320.jpg" alt="" /></div>
					<dl>
						<dt>AIG Global</dt>
						<dd>Expand the variety of infrastructures necessary for financial firm’s move-in to strengthen its position as international financial cluster.</dd>
					</dl>
				</li>
				<li>
					<div class="img"><img src="../img/temp/@img_320x320.jpg" alt="" /></div>
					<dl>
						<dt>AIG Global</dt>
						<dd>Construct the comprehensive support system related to finance such as the global business Services support center and etc.</dd>
					</dl>
				</li>
				<li>
					<div class="img"><img src="../img/temp/@img_320x320.jpg" alt="" /></div>
					<dl>
						<dt>AIG Global</dt>
						<dd>Strengthen the geographic position of Yeouido as an international finance hub by building IFC Seoul and actively attracting leading foreign finance corporations.</dd>
					</dl>
				</li>
				<li>
					<div class="img"><img src="../img/temp/@img_320x320.jpg" alt="" /></div>
					<dl>
						<dt>AIG Global</dt>
						<dd>IFC Seoul features Conrad Seoul Hotel, which is the finest five-star luxury business hotel among the affiliates of Hilton Worldwide and it is the first for South Korea.</dd>
					</dl>
				</li>
				<li>
					<div class="img"><img src="../img/temp/@img_320x320.jpg" alt="" /></div>
					<dl>
						<dt>AIG Global</dt>
						<dd>Expand the variety of infrastructures necessary for financial firm’s move-in to strengthen its position as international financial cluster.</dd>
					</dl>
				</li>
			</ul>
		</div>
		<div class="mobile topBtn"><a href="#" class="btn top">top</a></div>
	</div>
</div>