<%

filename = Server.MapPath("/movie/IFC_2m30s_2012_Kor_0526.movie")

Response.Expires = 0
Response.Buffer = True
Response.Clear

Set fs = Server.CreateObject("Scripting.FileSystemObject")

If fs.FileExists(filename) Then
    '파일이 있을경우 파일을 스트림 형태로 열어 보낸다.
    Response.ContentType = "application/mp4"
    Response.CacheControl = "public"
    Response.AddHeader "Content-Disposition","attachment;filename=movie.mp4"

    Set Stream=Server.CreateObject("ADODB.Stream")
    Stream.Open
    Stream.Type=1
    Stream.LoadFromFile filepath & filename
    Response.BinaryWrite Stream.Read
    Stream.close
    Set Stream = nothing
Else 
    '파일이 없을 경우...
    Response.Write "해당 파일을 찾을 수 없습니다."
End If

Set fs = Nothing

%>