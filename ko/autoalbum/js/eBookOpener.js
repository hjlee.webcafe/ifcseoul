function eBookOpener() {
    this.maxWin	= null;
	this.winWidth        = null;
    this.winHeight       = 0;
	this.winLimitWidth   = 1024;
    this.winLimitHeight  = 768;
	this.winStyle        = null;
	this.winStyleName    = null;
    this.albumID; 
	this.dirPage = 1;
    this.eBookUrl;
	this.ViewerStyleList = new Array("eBookFull", "eBookCustomer", "eBookDefault", "FullScreen");
	
    this.GetWinStyle     = GetWinStyle;
    this.OpenBook        = OpenBook;
	
	this.reMax="N";
	this.User = null;
	this.bUrl = null;
	this.reBook = null;
	
	this.SearchKey="";
	this.SearchType=null;
	this.Search_year1 = null;
	this.Search_month1 = null;
	this.Search_day1 = null;
	this.Search_year2 = null;
	this.Search_month2 = null;
	this.Search_day2 = null;
	this.ClassInit = null;
	this.HTMLFILE = 0;
	this.Group = null;
	this.subGroup = null;
	this.BroswerCheck = "IE";
	this.IEver = 5;
	this.openChk = 1;
}
function browserVersion(){    

	this.BroswerCheck = (function x(){})[-5]=='x'?'FF3':(function x(){})[-6]=='x'?'FF2':/a/[-1]=='a'?'FF':'\v'=='v'?'IE':/a/.__proto__=='//'?'Saf':/s/.test(/a/.toString)?'Chr':/^function \(/.test([].sort)?'Op':'Unknown' ;
	if(this.BroswerCheck == "IE")
		this.IEversionCheck();
}

function IEversionCheck()
{
	var ver = 5;
	if( navigator.appName.indexOf("Microsoft") > -1 ) // IE?
	{
		if( navigator.appVersion.indexOf("MSIE 6") > -1) // IE6?
			ver= 6;
		else if(navigator.appVersion.indexOf( "MSIE 7") > -1) // IE7?IE7? 
			ver = 7;
		else if(navigator.appVersion.indexOf("MSIE 8") > -1) // IE8?IE8? 
			ver = 8;
	}

	this.IEver = ver;
}
function GetWinStyle() { 
	if(this.maxWin == "Y") {
		var tmpFullScreen = ""
		this.winStyle = 0;
	}
	else var tmpFullScreen = ""  ;   

    if (this.winStyle == null ) { this.winStyle = 2; }
	if(( screen.width <= this.winLimitWidth || screen.height <= this.winLimitHeight) ) {  this.winStyle = 0 ; }
    
    switch ( this.winStyle ) {              
      case 0  : //  Default window 
                this.winStyleName  = this.ViewerStyleList[0]; 
                this.winTop     = 0 ;                                   
                this.winLeft    = 0 ;
                this.winWidth   =  screen.width  ;
                this.winHeight  =  screen.height ;
               
				if ( screen.width  != screen.availWidth  ) { this.winWidth  = screen.availWidth } // TaskBar Width
                if ( screen.height != screen.availHeight ) { this.winHeight = screen.availHeight } // TaskBar Heigh

				if(this.BroswerCheck=="Saf") 
					this.winHeight -= 25;

				this.maxWin = "Y";
				
				break;
      
      case 1  : //  customer window 
                this.winStyleName  = this.ViewerStyleList[1]; 
                if(this.winWidth < this.winLimitWidth)    {  this.winWidth  = this.winLimitWidth; }
                if(this.winHeight < this.winLimitHeight)  {  this.winHeight  = this.winLimitHeight; }
                this.winLeft    = (screen.width  - this.winWidth) / 2;
                this.winTop     = (screen.height - this.winHeight) / 2;
                break;
    
      case 2 : // user customer window environemnt
                this.winStyleName  = this.ViewerStyleList[2]; 
                this.winWidth      = this.winLimitWidth;  
                this.winHeight     = this.winLimitHeight;  
                this.winLeft       = (screen.width  - this.winWidth) / 2;
                this.winTop        = (screen.height - this.winHeight) / 2;
				
                break;
            
      case 3 :  // Full Screen  
                this.winStyleName  = this.ViewerStyleList[3]; 
                this.winWidth      = screen.width;  
                this.winHeight     = screen.height;  
                this.winLeft       = 0;
                this.winTop        = 0;
                break; 
      default : ;                            
    }
    this.winStyle    = "top=" + this.winTop + ", left=" + this.winLeft + ", width=" + this.winWidth + ", height=" + this.winHeight + ", directories=no, location=no, menubar=no, resizable=yes, scrollbars=no, status=no, toolbar=no, copyhistory=no" + tmpFullScreen ;
  }
 


function OpenBook() {
    // NOTE : process v1.6 , v1.8 param type for naver
	var bro_status_chk=null;
	this.GetWinStyle();
	var eBookClient = null;
	try
	{
		var u = this.eBookUrl;
		if(u.indexOf('view.html') > 0)
			var formPage = u.replace('view.html', 'connectBook.html');
		else if(u.indexOf('bbb.html') > 0)
			var formPage = u.replace('bbb.html', 'connectBook.html');
		
		var sQuery2 = formPage;
		eBookClient = window.open(this.eBookUrl, this.winStyleName +"_"+ Math.round(Math.random() * 100), this.winStyle);	
		this.openChk = eBookClient.closed;
	
	}
	catch (e)
	{
		//alert("팝업창이 차단되어 책자를 열지 못하였습니다.\n\n아래 \"전자북 보기\" 버튼 또는 책자 표지를 클릭 하거나\n\n위 노란 표시줄의 팝업 차단을 해제 하여 주시면 책자를 열람 하실수 있습니다.");
		bro_status_chk = "block";
	}	
	
	if(this.reMax == "Y") {
		self.opener = self;
		self.close(); 
	}

	if(this.ClassInit==1) {
		OpenEBook = new eBookOpener();
	}

	return bro_status_chk;
}  

OpenEBook = new eBookOpener();