/*
 ______________________________________________________
/???????????????????????????\
|           Folder Navigation 2.0 by EAE               |
|                                                      |
|   Based on a cross browser outline sample found      |
|   at 'www.webreference.com/dhtml'                    |
|                                                      |
|   Feel free to copy, use and change this script as   |
|   long as this part remains unchanged.               |
|                                                      |
|   If you have any questions and or comments please   |
|   E-mail me 'eae@eae.net'. If you're looking for     |
|   more JavaScripts etc, please check out my webpage  |
|                 'www.eae.net/weebfx'                 |
|                                                      |
|              Last Updated: 17 July 1998              |
\______________________________________________________/
 ???????????????????????????
*/
/*
document.onmouseover = mOver ;
document.onmouseout = mOut ;
*/
function mOver() {
	var eSrc = window.event.srcElement ;
	if (eSrc.className == "item") {
		window.event.srcElement.className = "highlight";
	}
}

function mOut() {
	var eSrc = window.event.srcElement ;
	if (eSrc.className == "highlight") {
		window.event.srcElement.className = "item";
	}
}


var bV=parseInt(navigator.appVersion);
NS4=(document.layers) ? true : false;
IE4=((document.all)&&(bV>=4))?true:false;
IE4 = true;
ver4 = (NS4 || IE4) ? true : false;

isExpanded = false;

function getIndex(EX1) {
	ind = null;
	for (i=0; i<document.layers.length; i++) {
		whichEl = document.layers[i];
		if (whichEl.id == EX1) {
			ind = i;
			break;
		}
	}
	return ind;
}

function arrange() {
	nextY = document.layers[firstInd].pageY + document.layers[firstInd].document.height;
	for (i=firstInd+1; i<document.layers.length; i++) {
		whichEl = document.layers[i];
		if (whichEl.visibility != "hide") {
			whichEl.pageY = nextY;
			nextY += whichEl.document.height;
		}
	}
}

function FolderInit(){
	if (NS4) 
	{
		firstEl = "mParent";
		firstInd = getIndex(firstEl);
		showAll();
		for (i=0; i<document.layers.length; i++) {
			whichEl = document.layers[i];
			if (whichEl.id.indexOf("Child") != -1) whichEl.visibility = "hide";
		}
		arrange();
	}
	else {
		tempColl = document.getElementsByTagName("DIV");  
		for (i=0; i<tempColl.length; i++) 
		{
			if (tempColl[i].className == "child") tempColl[i].style.display = "none";
		}
	}
}

function FolderExpand(EX1,EX2) {

	if (!ver4) return;
	if (IE4) { ExpandIE(EX1,EX2) } 
	else { ExpandNS(EX1,EX2) }
}

function ExpandIE(EX1,EX2) {

	Expanda = document.getElementById(EX1+ "a");
	try{Expanda.blur();}catch(e){}
    ExpandChild = document.getElementById(EX1 + "Child");

	if (EX2 != "top") 
	{ 
		ExpandTree = document.getElementById(EX1 + "Tree");
		ExpandFolder = document.getElementById(EX1 + "Folder");
	}
	
	if (ExpandChild.style.display == "none") 
	{
		ExpandChild.style.display = "block";
        if (EX2 != "top") 
		{ 
			if (EX2 == "last") { ExpandTree.src = "../../base//category/Lminus.gif"; }
			else { ExpandTree.src = "../../base//category/Tminus.gif"; }
			ExpandFolder.src = "../../base//category/openfoldericon.gif";	
		}
		else { document.getElementById('mTree').src = "../../base//dot_select.gif"; }
	}
	else 
	{
		ExpandChild.style.display = "none";
        if (EX2 != "top") 
		{ 
			if (EX2 == "last") { ExpandTree.src = "../../base//category/Lplus.gif"; }
			else { ExpandTree.src = "../../base//category/Tplus.gif"; }
			ExpandFolder.src = "../../base//category/foldericon.gif";
		}
		else { document.getElementById('mTree').src = "../../base//dot_select.gif"; }
	}
}

function ExpandNS(EX1,EX2) {
	ExpandChild = eval("document." + EX1 + "Child")
        if (EX2 != "top") { 
		ExpandTree = eval("document." + EX1 + "Parent.document." + EX1 + "Tree")
		ExpandFolder = eval("document." + EX1 + "Parent.document." + EX1 + "Folder")
	}	
	if (ExpandChild.visibility == "hide") {
		ExpandChild.visibility = "show";
                if (EX2 != "top") { 
               		if (EX2 == "last") { ExpandTree.src = "../../base//category/Lminus.gif"; }
			else { ExpandTree.src = "../../base//category/Tminus.gif"; }
			ExpandFolder.src = "../../base//category/openfoldericon.gif";	
		}
		else { mTree.src = "../../base//category/top.gif"; }
	}
	else {
		ExpandChild.visibility = "hide";
                if (EX2 != "top") { 
               		if (EX2 == "last") { ExpandTree.src = "../../base//category/Lplus.gif"; }
			else { ExpandTree.src = "../../base//category/Tplus.gif"; }
			ExpandFolder.src = "../../base//category/foldericon.gif";	
		}
		else { mTree.src = "../../base//dot_select.gif"; }
		//else { mTree.src = "../../base//category/top.gif"; }
	}
	arrange();
}

function showAll() {
	for (i=firstInd; i<document.layers.length; i++) {
		whichEl = document.layers[i];
		whichEl.visibility = "show";
	}
}


with (document) {
	write("<STYLE TYPE='text/css'>");
	if (NS4) {
		write(".parent { color: black; font-size:9pt; line-height:0pt; color:black; text-decoration:none; margin-top: 0px; margin-bottom: 0px; position:absolute; visibility:hidden }");
		write(".child { text-decoration:none; font-size:9pt; line-height:15pt; position:absolute }");
	        write(".item { color: black; text-decoration:none }");
	        write(".highlight { color: blue; text-decoration:none }");
	}
	else {
		write(".parent { font-family:����; font-size:9pt; text-decoration: none; color: black }");
		write(".child { font-family:����; font-size:9pt; display:none }");
	        write(".item { color: black; text-decoration:none; cursor: hand }");
	        write(".highlight { color: blue; text-decoration:none }");
	        write(".icon { margin-right: 5 }")
	}
	write("</STYLE>");
}

function getSelectLayerOver1(obj,img)
{
	obj.style.border = '1 solid #225588';
	img.style.filter = '';
}

//Ÿ��Ʋ���̾� MouseOut
function getSelectLayerOut1(obj,img)
{
	obj.style.border = '1 solid #C0C0C0';
	img.style.filter = 'gray()';
}



//onload=FolderInit;

