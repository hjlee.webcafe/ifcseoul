
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

 function block_go(ls_code,ls_page,ls_key_index,ls_key){
	//alert('실행');
    document.block_form.tCode.value     = ls_code;
    document.block_form.page_no.value   = ls_page;
    document.block_form.key.value       = ls_key_index;
    document.block_form.key_index.value = ls_key;
    document.block_form.submit();
  }

  <!-- 페이지 이동함수 -->
  function page_go(ls_code,ls_mode,ls_id,ls_page,ls_key_index,ls_key){
    document.page_form.tCode.value      = ls_code;
	  //alert(ls_code);
	document.page_form.mode.value       = ls_mode;
	  //alert(ls_mode);
	document.page_form.uid.value        = ls_id;
      //alert(ls_id);
	document.page_form.page_no.value    = ls_page;
      //alert(ls_page);
	document.page_form.key.value        = ls_key_index;
      //alert(is_key_index);
	document.page_form.key_index.value  = ls_key;
      //alert(ls_key);
	document.page_form.submit();
  }

   var old='';
   function menu(name){
        	
		submenu=eval("board_view"+name+".style");

        if(old!=submenu)
        {
            if(old!='')
            {
                old.display='none';
            }
            submenu.display='block';
            old=submenu;
        }
        else
        {
            submenu.display='none';
            old='';
        }
    }

	function key_chk(arg){
		if(!f_empty_chk(arg.key,'검색어')) { return false; }
    }

	function img_open(file_path){
           window.open("view_image.html?name="+file_path, "img_pop", 'width=450, height=450, resizable=1, scrollbars=1');
         }

 function cmt_check(arg){
	
	if(!f_empty_chk(arg.cmt_writer,'성명')){ return false; }
	if(!f_empty_chk(arg.cmt_passwd,'비밀번호')){ return false; }
	if(!f_empty_chk(arg.cmt,'메모내용')){ return false; }          
    
}
function cmt_len_check(arg) {
	cmt_len = arg.cmt.value.length;
	if(cmt_len > 32768) {
		alert('메모에 입력할수 있는 최대 글자수를 초과 하였습니다');
		return;
	}
}

function delete_cmt(url,num){
	arg = eval('document.cmt_l.wpasswd'+num);
	if(!f_empty_chk(arg,'메모삭제 패스워드')) return;
	location.href=url+"&wpasswd="+arg.value;
}


function f_chk(arg){
	if(!f_empty_chk(arg.passwd,'비밀번호'))  {return false;}
}

function open_album(file_path,num,code) {
	window.open("g_open.php?name="+file_path+"&num="+num+"&tCode="+code,'','width=600, height=280, resizable=0, scrollbars=yes, top=150, left=150');
}
function open_gallery(file_path,num,code) {
	window.open("g_open1.php?name="+file_path+"&num="+num+"&tCode="+code,'','width=600, height=280, resizable=0, scrollbars=yes, top=150, left=150');
}


//SELECT -> LAYER변환
function getSelectToLayer(obj,lwidth,href)
{

	obj.style.display = 'none';
	var newsb = obj.id + "_sbj";
	var newid = obj.id + "_tmp";
	var newim = obj.id + "_img";
	var LayerTag = "";
	var tmpID = obj.id+"_top";

	LayerTag += "<DIV ID ='"+tmpID+"' style='position:relative; z-index:50; top:0;left:0;width:100%;height:100%;'>";
	LayerTag += "<TABLE WIDTH='"+lwidth+"' CELLSPACING=0 CELLPADDING=0  STYLE='position:absolute;cursor:default;z-index:100;'>";
	LayerTag += "<TR HEIGHT=1><TD></TD></TR><TR><TD>";
	LayerTag += "<TABLE BGCOLOR='#FFFFFF' HEIGHT=15 WIDTH=100% CELLSPACING=1 CELLPADDING=0 STYLE='border:1 solid #C0C0C0;line-height:117%;' onmouseover='getSelectLayerOver(this,document.all."+newim+",document.all."+newid+");' onmouseout='getSelectLayerOut(this,document.all."+newim+",document.all."+newid+");'>";
	
	LayerTag += "<TR WIDTH=100% onclick='getSelectSubLayer(document.all."+newid+",document.all."+newsb+");'>";
	LayerTag += "<TD ID='"+newsb+"' WIDTH=95% onblur='getSelectLayerBlur(this);'>&nbsp;" + obj.options[obj.selectedIndex].text + "</TD>";
	LayerTag += "<TD ALIGN=RIGHT><IMG ID='"+newim+"' SRC='board/image/dot_select.gif'ALIGN=absmiddle STYLE='filter:gray();'></TD></TR>";
	
	LayerTag += "</TABLE>";

	LayerTag += "<TABLE ID='"+newid+"' WIDTH=100% CELLSPACING=0 CELLPADDING=0 STYLE='display:none;border:1 solid #C0C0C0;z-index:100;' BGCOLOR='#FFFFFF'>";
	for (var i = 0 ; i < obj.length; i++)
	{
		LayerTag += "<TR onmouseover='getSelectMoveLayer(this);' onmouseout='getSelectMoveLayer1(this);' onclick=\"getSelectChangeLayer(document.all."+obj.id+",document.all."+newid+",document.all."+newsb+",'"+obj.options[i].text+"','"+obj.options[i].value+"','"+href+"');\" ";

		if (obj.value == obj.options[i].value)
		{
			LayerTag += "STYLE='background:#225588;color:#FFFFFF;'><TD>&nbsp;";
		}
		else {
			LayerTag += "STYLE='background:#FFFFFF;color:#000000;'><TD>&nbsp;";
		}

		LayerTag += obj.options[i].text;
		LayerTag += "</TD></TR>";
	}
	LayerTag += "</TABLE>";
	LayerTag += "</TD></TR></TABLE><IMG SRC='' WIDTH='"+lwidth+"' HEIGHT=0>";
	LayerTag += "</DIV>";
	document.write(LayerTag);
	
	tms = eval("document.all."+tmpID);

	tms.style.pixelTop  = tms.style.pixelTop;
	tms.style.pixelLeft  = tms.style.pixelLeft - 4;
}

//서브레이어보이기
function getSelectSubLayer(obj,sobj)
{
	if(obj.style.display == 'none')
	{
		sobj.style.background = '#FFFFFF';
		sobj.style.color = '#000000';	
		obj.style.display = 'block';
		obj.focus();
		setTimeout("getSelectSubLayer1(document.all."+obj.id+");" , 5000);
	}
	else {
		sobj.style.background = '#225588';
		sobj.style.color = '#FFFFFF';
		obj.style.display = 'none';
	}
}

//서브레이어체크
function getSelectSubLayer1(obj)
{
	if(obj.style.display != 'none')
	{
		obj.style.display = 'none';
	}
}

//타이틀레이어 MouseOver
function getSelectLayerOver(obj,img,nobj)
{
	if (nobj.style.display == 'none')
	{
		obj.style.border = '1 solid #225588';
		img.style.filter = '';
	}
}

//타이틀레이어 MouseOut
function getSelectLayerOut(obj,img)
{
	obj.style.border = '1 solid #C0C0C0';
	img.style.filter = 'gray()';
}


//서브레이어 MouseOver
function getSelectMoveLayer(obj)
{
	if (obj.style.color == '#000000')
	{
		obj.style.background = '#225588';
		obj.style.color = '#FFFFFF';
	}
}
//서브레이어 MouseOut
function getSelectMoveLayer1(obj)
{
	obj.style.background = '#FFFFFF';
	obj.style.color = '#000000';
}

//새레이어선택
function getSelectChangeLayer(obj,fobj,sobj,text,value,href)
{
	if (href)
	{
		//alert(href+value);
		//location.href = href + value;
		location.href = href;
		//return false;
	}
	
	fobj.style.display = 'none';
	sobj.innerHTML = '&nbsp;' + text;
	sobj.style.background = '#225588';
	sobj.style.color = '#FFFFFF';
	sobj.focus();
	obj.value = value;
}

//타이틀레이어 blur
function getSelectLayerBlur(obj)
{
	obj.style.background = '#FFFFFF';
	obj.style.color = '#000000';
}

//검색이동
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  var url = targ+"?mode=";
  var cat1=document.searchTop.category1.options[document.searchTop.category1.selectedIndex].value;
  var cat2=document.searchTop.category2.options[document.searchTop.category2.selectedIndex].value;
  if(cat1 !="all" || cat2 != "all") url+= "&key_index=subject";
  if(cat1 != "all") var key = "&key1=@1"+cat1;
  else var key = "";
  if(cat2 != "all") var key1 = "&key2=@2"+cat2;
  else key1 ="";
  var url = url+key+key1;
  location.href=url;
  
}
//카테고리 검색
function searchCategory(targ,arg){ //v3.0
  var url = targ+"?mode=";
  var cat1 = document.searchTop.bbs_category.options[document.searchTop.bbs_category.selectedIndex].value;
  var cat2=document.searchTop.category2.options[document.searchTop.category2.selectedIndex].value;
  if(cat2 != "all" || cat1 !="all") url+= "&key_index=subject";

  if(cat1 != "all") var key1 = "&cateBoardName_bbs="+cat1;
  else var key1 =""; 
  if(cat2 != "all") var key2 = "&key2=@2"+cat2;
  else var key2 ="";
 
  var url = url+key1+key2;
  location.href=url;
}
//전체검색 검색
function MM_allserach(targ,arg){ //v3.0
  var url = targ+"?mode=";
  var cat1 = document.searchTop.category1.options[document.searchTop.category1.selectedIndex].value;
  var cat2=document.searchTop.category2.options[document.searchTop.category2.selectedIndex].value;
  var cat3 = document.searchTop.bbs_category.options[document.searchTop.bbs_category.selectedIndex].value;
  if(cat2 != "all" || cat1 !="all" || cat3 != "all") url+= "&key_index=subject";

  if(cat3 != "all") var key1 = "&cateBoardName_bbs="+cat3;
  else var key1 =""; 
  if(cat2 != "all") var key2 = "&key2=@2"+cat2;
  else var key2 ="";
  if(cat1 != 'all') var key3 = "&key1=@1"+cat1;
  else var key3 ="";
  var url = url+key1+key2+key3;
  location.href=url;
}

//셀렉트 박스 선택 
function selectBox(arg,val) {
	for(i=0; i < arg.length; i++) {
		//alert(val +'/'+ arg.options[i].value);
		if(val == arg.options[i].value) {	
			arg.options[i].selected = true;
			break;
		}
	}
}

