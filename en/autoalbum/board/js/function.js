/****************************************************************************/
/* 주민번호 체크                                                            */
/* 2000.8.6 commany                                                         */
/****************************************************************************/
function f_jumin_chk(ai_jumin1,ai_jumin2)
{
  /* 앞번호6자리, 뒷번호 7자리 모두 입력했는지 체크 */
  if (!(f_maxlen_chk(ai_jumin1,6,'주민등록번호앞'))) return false;
  if (!(f_maxlen_chk(ai_jumin2,7,'주민등록번호뒤'))) return false;

  /* 숫자만 입력되었는지를 체크 */
  var ls_f_jumin = ai_jumin1.value;
  var ls_r_jumin = ai_jumin2.value;

  /* 앞 6자리 숫자 체크 */
  for (i=0 ; i<ls_f_jumin.length ; i++ )
  {
    var ls_sub_jumin = ls_f_jumin.substring(i,i+1);
    if ((ls_sub_jumin < '0') || (ls_sub_jumin > '9'))
    {
      alert('주민등록번호는 숫자만 입력할 수 있습니다.');
      ai_jumin1.select();
      ai_jumin1.focus();
      return false;
    }
  }
  /* 뒤 7자리 숫자 체크 */
  for (i=0 ; i<ls_r_jumin.length ; i++ )
  {
    var ls_sub_jumin = ls_r_jumin.substring(i,i+1);
    if ((ls_sub_jumin < '0') || (ls_sub_jumin > '9'))
    {
      alert('주민등록번호는 숫자만 입력할 수 있습니다.');
      ai_jumin1.select();
      ai_jumin1.focus();
      return false;
    }
  }

  /* 월 입력 체크 */
  var ls_sub_jumin = ls_f_jumin.substring(2,4);
  if ((ls_sub_jumin < '01') || (ls_sub_jumin > '12'))
  {
    alert('01월부터 12월까지만 입력할 수 있습니다.');
    ai_jumin1.select();
    ai_jumin1.focus();
    return false;
  }

  /* 일 입력 체크 */
  var ls_sub_jumin = ls_f_jumin.substring(4,6);
  if ((ls_sub_jumin < '01') || (ls_sub_jumin > '31'))
  {
    alert('01일부터 31일까지만 입력할 수 있습니다.');
    ai_jumin1.select();
    ai_jumin1.focus();
    return false;
  }

  /* 남녀 입력 체크 */
  var ls_sub_jumin = ls_r_jumin.substring(0,1);
  if ((ls_sub_jumin < '1') || (ls_sub_jumin > '4'))
  {
    alert('남녀 입력은 1부터 4까지 입니다.');
    ai_jumin2.select();
    ai_jumin2.focus();
    return false;
  }

  /*  주민등록번호 유효성 검사 */
  ar_f_jumin = new Array(6);
  ar_r_jumin = new Array(7);
  var check_digit = 0;
  for (i=0 ; i < 6 ; i++ )
  {
    ar_f_jumin[i] = ls_f_jumin.substring(i,i+1);
    check_digit = check_digit + (ar_f_jumin[i] * (i+2));
  }

  for (i=0 ; i < 7 ; i++ )
  {
    ar_r_jumin[i] = ls_r_jumin.substring(i,i+1);
  }
  check_digit = check_digit + (ar_r_jumin[0]*8 + ar_r_jumin[1]*9 + ar_r_jumin[2]*2 +
                               ar_r_jumin[3]*3 + ar_r_jumin[4]*4 + ar_r_jumin[5]*5);

  check_digit = check_digit % 11;
  check_digit = 11 - check_digit;
  check_digit = check_digit % 10;

  if (check_digit != ar_r_jumin[6])
  {
    alert('잘못된 주민등록번호입니다.\n다시 확인하시고 입력해 주세요');
    ai_jumin1.value = '';
    ai_jumin2.value = '';
    ai_jumin1.focus();
    return false;
  }
  if(ai_jumin1.value == "701010" && ai_jumin2.value == "1111111")
  {
    alert('잘못된 주민등록번호입니다.\n다시 확인하시고 입력해 주세요');
    ai_jumin1.value = '';
    ai_jumin2.value = '';
    ai_jumin1.focus();
    return false;
  }
  return true;
}

/****************************************************************************/
/* 이메일 형식에 맞는지 체크                                                */
/* 2001.1.8 Sandeep V. Tamhankar                                            */
/****************************************************************************/
function f_email_chk(as_fin)
{
  var emailStr = as_fin.value;
  var emailPat=/^(.+)@(.+)$/
  var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
  var validChars="\[^\\s" + specialChars + "\]"
  var quotedUser="(\"[^\"]*\")"
  var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
  var atom=validChars + '+'
  var word="(" + atom + "|" + quotedUser + ")"
  var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
  var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
  var matchArray=emailStr.match(emailPat)

  if (matchArray==null)
  {
    alert("이메일 형식에 맞지 않습니다.");
    as_fin.focus();
    as_fin.select();
    return false;
  }

  var user=matchArray[1]
  var domain=matchArray[2]

  // See if "user" is valid
  if (user.match(userPat)==null)
  {
    alert("이메일 형식에 맞지 않습니다.");
    as_fin.focus();
    as_fin.select();
    return false;
  }

  /* if the e-mail address is at an IP address (as opposed to a symbolic
     host name) make sure the IP address is valid. */
  var IPArray=domain.match(ipDomainPat)
  if (IPArray!=null)
  {
    // this is an IP address
    for (var i=1;i<=4;i++)
    {
      if (IPArray[i]>255)
      {
        alert("이메일 형식에 맞지 않습니다.");
        as_fin.focus();
        as_fin.select();
        return false;
      }
    }
    return true
  }

  // Domain is symbolic name
  var domainArray=domain.match(domainPat)
  if (domainArray==null)
  {
    alert("이메일 형식에 맞지 않습니다.");
    as_fin.focus();
    as_fin.select();
    return false;
  }

  var atomPat=new RegExp(atom,"g")
  var domArr=domain.match(atomPat)
  var len=domArr.length
  if (domArr[domArr.length-1].length<2 || domArr[domArr.length-1].length>3)
  {
    alert("이메일 형식에 맞지 않습니다.");
    as_fin.focus();
    as_fin.select();
    return false;
  }

  // Make sure there's a host name preceding the domain.
  if (len<2)
  {
    alert("이메일 형식에 맞지 않습니다.");
    as_fin.focus();
    as_fin.select();
    return false;
  }

  // If we've gotten this far, everything's valid!
  return true;
}

/****************************************************************************/
/* 빈항목과 첫칸이 스페이스 인지를 체크하는 함수                            */
/* 2000.11.17 commany                                                       */
/* [회원등록에서 항목 유효성 검사시 사용]                                   */
/****************************************************************************/
function f_empty_chk(as_fin,as_fin_nm)
{
  // as_fin의 항목의 값을 ls_value에 저장한다.
  var ls_value = as_fin.value;

  var ls_msg = '';

  // ls_value 변수 값이 널이면
  // 1. 에러 메세지를 출력해 준다.
  // 2. 에러가 난 항목에 포커스를 준다.
  // 3. FALSE 값을 리턴한다.
  if (ls_value == '')
  {
    var ls_msg = '[' + as_fin_nm + '] 항목이 비어 있습니다.\n'
                                 + '다시 입력하여 주시기 바랍니다.';
    alert(ls_msg);
    as_fin.focus();
    return false;
  }
  // 항목의 제일 첫번째 위치가 널인지를 비교
  // 공백이 존재하면
  // 1. 에러 메세지를 출력한다.
  // 2. 해당 항목을 초기화 한다.
  // 3. 해당 항목에 포커스를 준다.
  // 4. FALSE 값을 리턴한다.
  else if (ls_value.substring(0,1) == ' ')
  {
    var ls_msg = '[' + as_fin_nm + '] 항목의 앞부분이 공백으로 띄워져 있습니다.\n'
                                 + '다시 입력하여 주시기 바랍니다.';
    alert(ls_msg);
    as_fin.select();
    as_fin.focus();
    return false;
  }
  return true;
}

/****************************************************************************/
/* 숫자만 입력 받을 수 있도록 하는 함수 (문자이면 에러 체크)                */
/* 2000.11.22 commany                                                       */
/****************************************************************************/
/* as_fin는 현재 포커스가 있는 항목을 말한다.                               */
function f_integer_chk(as_fin,as_alias)
{
  // ls_value은 비교 대상이 되는 항목의 값이다.
  var ls_value = as_fin.value;

  // ls_value의 문자수를 알아낸다.
  var li_length = ls_value.length;

  // 1. 항목 값이 1-9 사이의 값이면
  // 2. 입력된 숫자 앞에 '0'를 붙여놓는다.
  // for (vas i=0 ; i <= ls_value.length ; i++)
  for (var i=0 ; i < li_length ; i++)
  {
    // li_value 값의 한자리 마다의 값
    // ▶ substring(인덱스1,인덱스2) 에 대해서
    //    - [인덱스1]에서 [인텍스2] 사이의 문자열을 문장 중에서 추출
    var ls_sub_value = ls_value.substring(i,i+1);

    if ((ls_sub_value < '0') || (ls_sub_value > '9'))
    {
      var ls_msg = '[' + as_alias + ']은 숫자만 입력 할 수 있습니다.\n' + '다시 입력해 주시기 바랍니다.';
      alert(ls_msg);
      as_fin.value = '';
      as_fin.focus();
      return false;
    }
  }
  return true;
}

/****************************************************************************/
/* 일정 자리수 만큼 입력했는지 체크하는 함수                                */
/* 2000.11.27 commany                                                       */
/****************************************************************************/
/* as_fin는 현재 포커스가 있는 항목을 말한다.                               */
/* as_next_fin는 현재 포커스가 있는 항목의 다음 항목을 말한다.              */
/* ai_maxlen_int는 입력할 수 있는 최고 자리수를 말한다.                     */
function f_maxlen_chk(as_fin,ai_maxlen_int,as_fin_nm)
{
  // 최고 자릿수 만큼 입력을 안했을 때
  if (as_fin.value.length < ai_maxlen_int)
  {
    var ls_msg = '[' + as_fin_nm + '] 항목은 최소 ' + ai_maxlen_int + '자 이상적으셔야 됩니다.\n'
                                 + '다시 입력하여 주시기 바랍니다.';
    alert(ls_msg);
    as_fin.focus();
    as_fin.select();
    return false;
  }
  if (as_fin.nm == '패스워드' || as_fin.nm == '패스워드확인')
  {
    var ls_msg = '[' + as_fin_nm + '] 항목은 꼭 ' + ai_maxlen_int + '자에서 10자까지 적으셔야 됩니다.\n'
                                 + '다시 입력하여 주시기 바랍니다.';
    alert(ls_msg);
    as_fin.focus();
    as_fin.select();
    return false;
  }
  return true;
}

/****************************************************************************/
/* 두 항목의 텍스트 비교 함수                                               */
/* 2000.11.21 commany                                                       */
/* [회원등록에서 비밀번호와 재입력비밀번호의 텍스트를 비교]                 */
/****************************************************************************/
/* as_pre_fin는 이전 항목을 말한다.                                         */
/* as_fin는 현재 포커스가 있는 항목을 말한다.                               */
/* as_next는 다음 함옥을 말한다.                                            */
function f_compare_text(as_pre_fin,as_fin,as_next_fin)
{
 if (as_pre_fin.value != as_fin.value)
 {
   alert('입력된 비밀번호가 일치하지 않습니다. \n다시 입력해 주시기 바랍니다.');
   as_fin.value = '';
   as_fin.focus();
   return false;
 }
 else
 {
   as_next_fin.focus();
   return true;
 }
}

<!-- popup -->
function OpenWindow(url,ls_width,ls_height,ls_scroll){
  winobject = window.open(url,"demo","toolbar=no,status=no,menubar=no,scrollbars="+ls_scroll+",resizable=no,width="+ls_width+",height="+ls_height);
}

<!-- blink tag -->
function doBlink() {
  var blink = document.all.tags("blink")
  for (var i=0; i < blink.length; i++)
    blink[i].style.visibility = blink[i].style.visibility == "" ? "hidden" : ""
}
function startBlink() {
  if (document.all) setInterval("doBlink()",600)
}
window.onload = startBlink;


<!-- 	로그인 메세지 -->
function logMsg(){
	alert('회원전용서비스입니다. '
	    + '로그인하십시요. ');
}

/****************************************************************************/
/* 이미지변환(롤오버) 및 레이어뷰 함수                                      */
/****************************************************************************/
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}

MM_reloadPage(true);

function key_chk(arg){
  if(!f_empty_chk(arg.key,'검색어')) { return false; }
}
//-->
