var _cookie = new Array();

function setCookie(k, v, expire, path)
{
	var tmp, pattern, todaydate, unixtime, expiretext, e;
	path = (path == null || path == "") ? "/" : path;
	todaydate = new Date();
	unixtime = todaydate.getTime();
	expiretext = "";

	if (v == null) {
		expire = 0;
	}
	if (expire == null || expire == 0) {
		expiretext = "";
	} else {
		todaydate.setTime(unixtime + (expire * 1000));
		expiretext = " expires=" + todaydate.toUTCString() +";";
	}
	document.cookie = k + "=" + escape(v) + "; path=" + path + ";" + expiretext;

	pattern = /^(.*)\[(\d*)\]$/;

	if(pattern.test(k)) {
		if(RegExp.$2 == "") {
			try {
				_cookie[RegExp.$1].push(v);
			} catch(e) {
				_cookie[RegExp.$1] = new Array();
				_cookie[RegExp.$1][0] = v;
			}
		} else {
			try {
				_cookie[RegExp.$1][RegExp.$2 * 1] = v;
			} catch(e) {
				_cookie[RegExp.$1] = new Array();
				_cookie[RegExp.$1][RegExp.$2 * 1] = v;
			}
		}
	} else {
		_cookie[k] = v;
	}
}

function parseCookie()
{
	var tmp, pattern1, pattern2, i, keyval = new Array(), e;

	if(document.cookie == "") {
		return true;
	}

	tmp = document.cookie.replace(/;[ \t]+/g, ";");
	
	tmp = tmp.split(";");

	pattern1 = /^([^=]+)=?(.*)$/;
	pattern2 = /^(.*)\[(\d*)\]$/;

	for(i = 0; i < tmp.length; i ++) {
		if(!pattern1.test(tmp[i])) {
			window.alert("error");
		}
		keyval[0] = RegExp.$1;
		keyval[1] = RegExp.$2;
		if(pattern2.test(keyval[0])) {
			if(RegExp.$2 == "") {
				try {
					_cookie[RegExp.$1].push(keyval[1]);
				} catch(e) {
					_cookie[RegExp.$1] = new Array();
					_cookie[RegExp.$1][0] = keyval[1];
				}
			} else {
				try {
					_cookie[RegExp.$1][RegExp.$2] = keyval[1];
				} catch(e) {
					_cookie[RegExp.$1] = new Array();
					_cookie[RegExp.$1][RegExp.$2] = keyval[1];
				}
			}
		} else {
			_cookie[keyval[0]] = keyval[1];
		}
	}

	return true;
}

parseCookie();