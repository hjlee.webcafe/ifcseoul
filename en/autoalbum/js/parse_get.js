var _get = new Array();

function parseGet()
{
	var tmp, pattern1, pattern2, i, keyval = new Array(), e;

	pattern1 = /\?/;
	if(!pattern1.test(location.href)) {
		return true;
	}

	tmp = location.href.replace(/^.*\?/, "");
	
	if(tmp == "") {
		return true;
	}

	tmp = tmp.split("&");

	pattern1 = /^([^=]+)=?(.*)$/;
	pattern2 = /^(.*)\[(\d*)\]$/;
	for(i = 0; i < tmp.length; i ++) {
		if(!pattern1.test(tmp[i])) {
			window.alert("error");
		}
		keyval[0] = RegExp.$1;
		keyval[1] = RegExp.$2;
		if(pattern2.test(keyval[0])) {
			if(RegExp.$2 == "") {
				try {
					_get[RegExp.$1].push(keyval[1]);
				} catch(e) {
					_get[RegExp.$1] = new Array();
					_get[RegExp.$1][0] = keyval[1];
				}
			} else {
				try {
					_get[RegExp.$1][RegExp.$2] = keyval[1];
				} catch(e) {
					_get[RegExp.$1] = new Array();
					_get[RegExp.$1][RegExp.$2] = keyval[1];
				}
			}
		} else {
			_get[keyval[0]] = keyval[1];
		}
	}

	return true;
}

parseGet();