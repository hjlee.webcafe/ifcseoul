function getSelectToLayer(obj,lwidth,href)
{

	obj.style.display = 'none';
	var newsb = obj.id + "_sbj";
	var newid = obj.id + "_tmp";
	var newim = obj.id + "_img";
	var LayerTag = "";
	var tmpID = obj.id+"_top";
	
	LayerTag += "<DIV ID ='"+tmpID+"' style='position:relative; z-index:50; top:0;left:0;width:100%;height:100%;'>";
	LayerTag += "<TABLE WIDTH='"+lwidth+"' HEIGHT='100%' CELLSPACING=0 CELLPADDING=0  STYLE='position:absolute;cursor:default;z-index:100;'>";
	LayerTag += "<TR HEIGHT=1><TD></TD></TR><TR><TD>";
	LayerTag += "<TABLE BGCOLOR='#EAF4F8' HEIGHT=15 WIDTH=100% CELLSPACING=1 CELLPADDING=0 STYLE='border:1 solid #C0C0C0;line-height:117%;' onmouseover='getSelectLayerOver(this,document.all."+newim+",document.all."+newid+");' onmouseout='getSelectLayerOut(this,document.all."+newim+",document.all."+newid+");'>";
	
	LayerTag += "<TR WIDTH=100% onclick='getSelectSubLayer(document.all."+newid+",document.all."+newsb+");'>";
	LayerTag += "<TD ID='"+newsb+"' WIDTH=95% onblur='getSelectLayerBlur(this);'>&nbsp;" + obj.options[obj.selectedIndex].text + "</TD>";
	LayerTag += "<TD ALIGN=RIGHT><IMG ID='"+newim+"' SRC='base/dot_select.gif' ALIGN=absmiddle STYLE='filter:gray();'></TD></TR>";
	
	LayerTag += "</TABLE>";

	LayerTag += "<TABLE ID='"+newid+"' WIDTH=100% CELLSPACING=0 CELLPADDING=0 STYLE='display:none;border:1 solid #C0C0C0;z-index:100;' BGCOLOR='#EAF4F8'>";
	for (var i = 0 ; i < obj.length; i++)
	{
		var val = obj.options[i].value;
		var AlignStr = val.substring(0,5);
		if(AlignStr == "{cen}") {
			var AlignCon = " align=\"center\" "; 
			val = val.replace(/{cen}/gi, "");
			obj.options[i].value = val;
			var backColor = "bgcolor=\"#EAEAEA\" ";
			var Bolds = "<strong>";
		}
		else {
			var AlignCon ="";
			var backColor="";
			var Bolds ="";
		}

		LayerTag += "<TR onmouseover='getSelectMoveLayer(this);' onmouseout='getSelectMoveLayer1(this);' onclick=\"getSelectChangeLayer(document.all."+obj.id+",document.all."+newid+",document.all."+newsb+",'"+obj.options[i].text+"','"+obj.options[i].value+"','"+href+"');\" ";
		
		if (obj.value == obj.options[i].value)
		{
			LayerTag += "STYLE='background:#225588;color:#EAF4F8;'><TD "+AlignCon+" "+backColor+">&nbsp;"+Bolds;
		}
		else {
			LayerTag += "STYLE='background:#EAF4F8;color:#000000;'><TD "+AlignCon+" "+backColor+">&nbsp;"+Bolds;
		}

		LayerTag += obj.options[i].text;
		LayerTag += "</TD></TR>";
	}
	LayerTag += "</TABLE>";
	LayerTag += "</TD></TR></TABLE><IMG SRC='' WIDTH='"+lwidth+"' HEIGHT=0>";
	LayerTag += "</DIV>";
	document.write(LayerTag);
	
	tms = eval("document.all."+tmpID);

	tms.style.pixelTop  = tms.style.pixelTop;
	tms.style.pixelLeft  = tms.style.pixelLeft - 4;
}

//서브레이어보이기
function getSelectSubLayer(obj,sobj)
{
	if(obj.style.display == 'none')
	{
		sobj.style.background = '#EAF4F8';
		sobj.style.color = '#000000';	
		obj.style.display = 'block';
		obj.focus();
		setTimeout("getSelectSubLayer1(document.all."+obj.id+");" , 5000);
	}
	else {
		sobj.style.background = '#225588';
		sobj.style.color = '#EAF4F8';
		obj.style.display = 'none';
	}
}

//서브레이어체크
function getSelectSubLayer1(obj)
{
	if(obj.style.display != 'none')
	{
		obj.style.display = 'none';
	}
}

//타이틀레이어 MouseOver
function getSelectLayerOver(obj,img,nobj)
{
	if (nobj.style.display == 'none')
	{
		obj.style.border = '1 solid #225588';
		img.style.filter = '';
	}
}

//타이틀레이어 MouseOut
function getSelectLayerOut(obj,img)
{
	obj.style.border = '1 solid #C0C0C0';
	img.style.filter = 'gray()';
}


//서브레이어 MouseOver
function getSelectMoveLayer(obj)
{
	if (obj.style.color == '#000000')
	{
		obj.style.background = '#225588';
		obj.style.color = '#EAF4F8';
	}
}
//서브레이어 MouseOut
function getSelectMoveLayer1(obj)
{
	obj.style.background = '#EAF4F8';
	obj.style.color = '#000000';
}

//새레이어선택
function getSelectChangeLayer(obj,fobj,sobj,text,value,href)
{
	if (href)
	{
		//alert(href+value);
		//location.href = href + value;
		location.href = href;
		//return false;
	}
	
	fobj.style.display = 'none';
	sobj.innerHTML = '&nbsp;' + text;
	sobj.style.background = '#225588';
	sobj.style.color = '#EAF4F8';
	sobj.focus();
	obj.value = value;
}

//타이틀레이어 blur
function getSelectLayerBlur(obj)
{
	obj.style.background = '#EAF4F8';
	obj.style.color = '#000000';
}