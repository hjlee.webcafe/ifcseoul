	function eBookOpener() {
		this.maxWin	= null;
		this.winWidth        = null;
		this.winHeight       = 0;
		this.winLimitWidth   = 1024;
		this.winLimitHeight  = 768;
		this.winStyle        = null;
		this.winStyleName    = null;
		this.albumID; 
		this.dirPage = 1;
		this.eBookUrl;
		this.Group;
		this.ViewerStyleList = new Array("eBookFull", "eBookCustomer", "eBookDefault", "FullScreen");
		
		this.SetLaunchHTML   = SetLaunchHTML;
		this.GetWinStyle     = GetWinStyle;
		this.GetBorderSize   = GetBorderSize;
		this.IsWinXP         = IsWinXP;
		this.browserVersion = browserVersion;
		this.SelfOpen        = SelfOpen;
		this.OpenBook        = OpenBook;

		this.User=null;
		this.bUrl = null;
	}
	function SelfOpen() {
		this.GetWinStyle();
		var aBorderInfo = this.GetBorderSize();
		this.winWidth   += 2 * aBorderInfo[0];
		this.winHeight  += aBorderInfo[0] + aBorderInfo[1];

		var iRemovePosLeft  = (screen.width - this.winWidth) / 2;
		var iRemovePosTop   = (screen.height - this.winHeight) / 2;
		if(iRemovePosLeft < 0)  {  iRemovePosLeft = 0;  }
		if(iRemovePosTop < 0)   {  iRemovePosTop = 0;   }
		moveTo(iRemovePosLeft, iRemovePosTop);

		window.name = this.winStyleName;
		//alert(this.winWidth +"/"+this.winHeight+'/'+iRemovePosLeft+'/'+iRemovePosTop);
		resizeTo(this.winWidth, this.winHeight);
	}
	function GetWinStyle() { 
		if(this.maxWin == "Y") {
			var tmpFullScreen = " , fullscreen"
			this.winStyle = 3;
		}
		else var tmpFullScreen = ""  ;   
		
		if ( this.winStyle == null ) { this.winStyle = 2 ; }
		if( ( screen.width <= this.winLimitWidth || screen.height <= this.winLimitHeight) ) {  this.winStyle = 0 ; }
		 
		
	   
		switch ( this.winStyle ) {              
		  case 0  : //  Default window 
					this.winStyleName  = this.ViewerStyleList[0]; 
					this.winTop     = 0 ;                                   
					this.winLeft    = 0 ;
					this.winWidth   =  screen.width  ;
					this.winHeight  =  screen.height ;
					if ( screen.width  != screen.availWidth  ) { this.winWidth  = screen.width  - 57 } // TaskBar Width
					if ( screen.height != screen.availHeight ) { this.winHeight = screen.height - 27 } // TaskBar Height
					if(screen.availWidth == 1024) {if (browserVersion()) { tmpFullScreen  = ", fullscreen=yes";}}
					break;
		  
		  case 1  : // corporation customer window 
					this.winStyleName  = this.ViewerStyleList[1]; 
					if(this.winWidth < this.winLimitWidth)    {  this.winWidth  = this.winLimitWidth; }
					if(this.winHeight < this.winLimitHeight)  {  this.winHeight  = this.winLimitHeight; }
					this.winLeft    = (screen.width  - this.winWidth) / 2;
					this.winTop     = (screen.height - this.winHeight) / 2;
					break;
		
		  case 2 : // user customer window environemnt
					this.winStyleName  = this.ViewerStyleList[2]; 
					this.winWidth      = this.winLimitWidth;  
					this.winHeight     = this.winLimitHeight;  
					this.winLeft       = (screen.width  - this.winWidth) / 2;
					this.winTop        = (screen.height - this.winHeight) / 2;
					if(screen.availWidth == 1024) {if (browserVersion()) { tmpFullScreen  = ", fullscreen=yes";}}
					break;
				
		  case 3 :  // Full Screen  
					this.winStyleName  = this.ViewerStyleList[3]; 
					this.winWidth      = screen.width;  
					this.winHeight     = screen.height;  
					this.winLeft       = 0;
					this.winTop        = 0;

					// Windows 2003 
					if( !(IsWin2k3()) ){ tmpFullScreen  = ", fullscreen=yes";    }
					break; 
		  default : ;                            
		}
		this.winStyle    = "top=" + this.winTop + ", left=" + this.winLeft + ", width=" + this.winWidth + ", height=" + this.winHeight + ", directories=no, location=no, menubar=no, resizable=no, scrollbars=no, status=no, toolbar=no" + tmpFullScreen ;
	  }
	 
	function GetBorderSize () {
		var aBorderInfo = new Array();
		if (this.IsWinXP() ) {
		  aBorderInfo[0]  = 6 ;
		  aBorderInfo[1]  = 32 ;
		} else {
		  aBorderInfo[0]  = 5 ;
		  aBorderInfo[1]  = 24 ;
		}
		return aBorderInfo;
	}  
	
	function browserVersion(){    
		tmp_MSIE = window.navigator.userAgent.indexOf("MSIE");
		if(tmp_MSIE && window.navigator.userAgent.indexOf("SV1") > tmp_MSIE){   
			//This browser is Internet Explorer with SP2.   
			return true; 
		} else {
			//This browser is not Internet Explorer with SP2.
			return false;
		}
	}
	 
	function IsWinXP() {
		if ( navigator.userAgent.indexOf("Windows NT 5.1") > 1 ) { return true ; }
		return false ; // WinMe, Win9x, Win2k, Win2k  
	}
	function IsWin2k3() {
		if ( navigator.userAgent.indexOf("Windows NT 5.2") > 1 ) { return true ; }
		return false // WinMe, Win9x, Win2k  
	}      

	function SetLaunchHTML(arg_URL) {  //  open window in HTML
		argStr = arg_URL;
		oalbum = argStr.substring(argStr.indexOf('album=')+6, argStr.indexOf('&'));
		argStr = argStr.substring(argStr.indexOf('&') + 1);
		omaxWin = argStr.substring(argStr.indexOf('maxWin=')+7, argStr.indexOf('&'));
		argStr = argStr.substring(argStr.indexOf('&') + 1);
		odpage = argStr.substring(argStr.indexOf('directPageNum=')+14, argStr.indexOf('&'));
		argStr = argStr.substring(argStr.indexOf('&') + 1);
		obhi = argStr.substring(argStr.indexOf('bhi=')+4,argStr.indexOf('&'));
		argStr = argStr.substring(argStr.indexOf('&') + 1);
		User = argStr.substring(argStr.indexOf('User=')+5, argStr.indexOf('&'));
		argStr = argStr.substring(argStr.indexOf('&') + 1);
		fspGroup = argStr.substring(argStr.indexOf('fspGroup=')+9);
		var OpenHTML  = "<html>" + "\n" 
					  + "<body onload=\"oLaunchForm.submit()\">" + "\n" 
					  + "<form name=\"oLaunchForm\" action=\""+this.eBookUrl +"\" method=\"post\">\n"
					  + "<input type=hidden name=Falbum  value=\""+oalbum+"\">\n"
					  + "<input type=hidden name=FmaxWin  value=\""+omaxWin+"\">\n"
					  + "<input type=hidden name=FdirectPageNum  value=\""+odpage+"\">\n"
					  + "<input type=hidden name=Fbhi  value=\""+obhi+"\">\n"
					  + "<input type=hidden name=UserDumy  value=\""+User+"\">\n"
					  + "<input type=hidden name=fspGroup  value=\""+fspGroup+"\">\n"
					  + "<input type=hidden name=bUrl  value=\""+this.bUrl+"\">\n"
					  + "</body>" + "\n"  
					  + "</html>" ;
		return OpenHTML;
	}

	function OpenBook() {
		// NOTE : process v1.6 , v1.8 param type for naver
		this.GetWinStyle();
		sQuery = "?album="+this.albumID+"&maxWin="+this.maxWin+"&directPageNum="+this.dirPage+"&bhi="+this.winHeight+"&User="+this.User+"&fspGroup="+this.Group;
	   
		var eBookClient = window.open("about:blank", this.winStyleName +"_"+ Math.round(Math.random() * 100), this.winStyle);
		eBookClient.document.open();
		eBookClient.document.write(this.SetLaunchHTML(sQuery));
		eBookClient.document.close();
		//eBookClient.document.location.reload();
	}  

	OpenEBook = new eBookOpener();

	function mbookzView(album,max) {
		OpenEBook.albumID = album;
		if(max) OpenEBook.maxWin =max;
		OpenEBook.eBookUrl= "/autoalbum/page/"+album+"/bbb.html";
		OpenEBook.OpenBook();
	}