/*==============================
<script src="class.Progress.js"></script>
<script>
function Start() {
	
	Progress = new Progress();
	Progress.Init("DIVS", "제목", "내용");
}
</script>
<a href=javascript:Start()>시작</a>
<div id=DIVS></div>
==============================*/
var prgobj;
var prgsubject;
var prgcontents;
var prgcolor;
var tops = -1;
var lefts = -1;

function Progress2() {
	
	this.Init = _PrgInit;		
}
   
function _PrgInit(obj,subject,contents,top,left,color) {
		
	prgobj = obj;
	prgsubject = subject;
	prgcontents = contents;
	if(color) prgcolor = color;
	else prgcolor = "navy";
	if(top) tops = top;
	if(left) lefts = left;
	
	_SetDIV();      		
	_PrgPrc(5);	
      	setInterval("_PrgCenter()",10);      	      	
}

function _SetDIV() {		    
   	
	if(lefts > -1) sizewidth = lefts;
	else sizewidth = (document.body.clientWidth/2) - (350/2) + 50;   
   	
	if(tops > -1) sizeheight = tops; 
	else sizeheight = (document.body.clientHeight/2) - (250/2) + document.body.scrollTop;	
	
	if(prgsubject && prgcontents) {
		
		html  = "<div align=center id=ProgressDIV style='position:absolute;z-index:1;width:300;height:200;top:"+sizeheight+";left:"+sizewidth+";'>";	
		html += "<table width=300 cellspacing=0 cellpadding=3 height=200 border=0 style='border:1x solid black' bgcolor=#d6d6d6>";
		html += "<tr>";
		html += "<td colspan=2 bgcolor=navy style='font-size:9pt'>&nbsp;<font color=white>"+prgsubject+"</font></td>";
		html += "</tr>";
		html += "<tr>";
		html += "<td width=85% height=175 style='font-size:9pt'>"+prgcontents+"</td>";
		html += "<td width=15% height=175 align=center>";
		html += "<div id=ProgressPrcDIV></div>";
		html += "</td>";
		html += "</tr>";
		html += "</table>";	
		html += "</div>";
		
	} else {
		
		html  = "<div id=ProgressDIV>";					
		html += "<table cellspacing=0 cellpadding=3 width=30 height=130 border=0 bgcolor=#d6d6d6>";
		html += "<tr><td align=center>";
		html += "<div id=ProgressPrcDIV></div>";		
		html += "</td></tr>";
		html += "</table>";
		html += "</div>";
	}
		
	eval(prgobj+".innerHTML = html");	
}

function _PrgPrc(mode) {
	
	if(mode == 1) {		// 1부터 시작을 안하고 랜덤하게 시작한다.
	
		while(1) {
		
			ran = Math.round(Math.random()*10);
      	            	if(ran >= 1 && ran <= 4) {
      	            		mode = ran;
      	            		break;
      	            	}
      	         }                        
	}      
	      	
      	if(mode=="1") _PrgPrcInner("#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf",prgcolor);
      	else if(mode=="2") _PrgPrcInner("#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf",prgcolor,prgcolor);
      	else if(mode=="3") _PrgPrcInner("#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf",prgcolor,prgcolor,prgcolor);
      	else if(mode=="4") _PrgPrcInner("#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf",prgcolor,prgcolor,prgcolor,prgcolor);
      	else if(mode=="5") _PrgPrcInner("#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf",prgcolor,prgcolor,prgcolor,prgcolor,prgcolor);
      	else if(mode=="6") _PrgPrcInner("#bfbfbf","#bfbfbf","#bfbfbf","#bfbfbf",prgcolor,prgcolor,prgcolor,prgcolor,prgcolor,prgcolor);
      	else if(mode=="7") _PrgPrcInner("#bfbfbf","#bfbfbf","#bfbfbf",prgcolor,prgcolor,prgcolor,prgcolor,prgcolor,prgcolor,prgcolor);
      	else if(mode=="8") _PrgPrcInner("#bfbfbf","#bfbfbf",prgcolor,prgcolor,prgcolor,prgcolor,prgcolor,prgcolor,prgcolor,prgcolor);
      	else if(mode=="9") _PrgPrcInner("#bfbfbf",prgcolor,prgcolor,prgcolor,prgcolor,prgcolor,prgcolor,prgcolor,prgcolor,prgcolor);      	
      	else if(mode=="10") {
      		
      		_PrgPrcInner(prgcolor,prgcolor,prgcolor,prgcolor,prgcolor,prgcolor,prgcolor,prgcolor,prgcolor,prgcolor);
      		mode = 0;
      	
      	}
      	
	++mode;      	      	
      	
	setTimeout("_PrgPrc("+mode+")",100);
}

function _PrgPrcInner(mode1,mode2,mode3,mode4,mode5,mode6,mode7,mode8,mode9,mode10) {

	table = "<table height=120 border=1 bordercolordark=white bordercolorlight=#7F7F7F bordercolor=black cellpadding=0 cellspacing=0 width=20 bgcolor=#bfbfbf>";
	for(i=1; i<=10; i++) {
	
		color = eval("mode"+i);
		table += "<tr><td bgcolor="+color+" width=100% height=10></td></tr>";
	}               
	for(i=1; i<=2; i++) table += "<tr><td bgcolor="+prgcolor+" width=100% height=10></td></tr>";               
	table += "</table>";
		
	ProgressPrcDIV.innerHTML = table; 
}

function _PrgCenter() {      
      
      	if(tops > -1) sizeheight = tops +document.body.scrollTop;
	else sizeheight = (document.body.clientHeight/2) - (250/2) + document.body.scrollTop;	
	      
      	ProgressDIV.style.pixelTop = sizeheight;      
}
