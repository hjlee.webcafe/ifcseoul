<!-- #include file = "../inc/_config.asp" -->
<%
' remove cache
Response.Expires = -1
Response.ExpiresAbsolute = now() - 1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"


Dim mode, result
Set result = jsObject()
result("result") = 0
result("error_msg") = ""

Dim objConn, objRs, arrRs, strQuery
set objConn = OpenDBConnection()

if Request.Form("mode") = "" Then
	result("result") = ""
	result("error_msg") = "The mode is not selected"
	result.Flush
	Response.End
End If

mode = Request.Form("mode")

Dim as_num, as_name, as_id, as_pw, as_tel, as_email, error_flag, selected, select_array, num, pw_where
Err.clear
On Error Resume Next

If mode = "uid_info" Then
	If Request.Form("UID") = "" Then
		result("result") = ""
		result("error_msg") = "the uid is empty"
		result.Flush
		Response.End
	End If

	uid = Request.Form("UID")

	// check id
	strQuery = "select * from TBL_USER_INFO where UID = '"& uid &"' "

	set objRs = SendQuery(objConn,strQuery)

	If objRs.EOF Then
		result("result") = ""
	Else
		result("result") = objRs("USER_NAME") & "|" & objRs("AUTHORITY") & "|" & objRs("TELNUM") & "|" & objRs("MOBILENUM") & "|" & objRs("FAXNUM") & "|" & objRs("EMAIL")
	End If

	result.Flush
	Response.End
'공휴일 체크
ElseIf mode ="is_offday" Then

    If Request.Form("NOW") = "" Then
        result("result") = ""
        result("error_msg") = "the now is empty"
        result.Flush
        Response.End
    End If

    R_NOW = Request.Form("NOW")
    strQuery = "SELECT COUNT(*) as Cnt FROM TBL_OFFDAY WHERE O_YEAR = YEAR('" & R_NOW & "') AND O_MONTH = RIGHT('00' + CONVERT(NVARCHAR, MONTH('"&  R_NOW &"')), 2) AND O_DAY = RIGHT('00' + CONVERT(NVARCHAR, DAY('"& R_NOW &"')), 2) AND OFFDAY_NAME != '코로나'"

    set objRs = SendQuery(objConn,strQuery)

    '공휴일일경우 "T"
    If objRs("Cnt") = 0 Then
        result("result") = "F"
    Else
        result("result") = "T"
    End If

    result.Flush
    Response.End
ElseIf mode = "get_day" Then
	If Request.Form("YEAR") = "" Then
		result("result") = ""
		result("error_msg") = "the year is empty"
		result.Flush
		Response.End
	End If
	If Request.Form("MONTH") = "" Then
		result("result") = ""
		result("error_msg") = "the month is empty"
		result.Flush
		Response.End
	End If
	If Request.Form("NOW") = "" Then
		result("result") = ""
		result("error_msg") = "the now is empty"
		result.Flush
		Response.End
	End If
	If Request.Form("UID") = "" Then
		result("result") = ""
		result("error_msg") = "the uid is empty"
		result.Flush
		Response.End
	End If
	If Request.Form("LIMIT_TIME") = "" Then
		result("result") = ""
		result("error_msg") = "the limit_time is empty"
		result.Flush
		Response.End
	End If

	R_YEAR = Request.Form("YEAR")
	R_MONTH = Request.Form("MONTH")
	R_NOW = Request.Form("NOW")
	R_UID = Request.Form("UID")
	R_LIMIT_TIME = Request.Form("LIMIT_TIME")

	'월예약 가능시간 확인
	strQuery = "SELECT ISNULL(SUM(T_QTY*R_QTY),0) AS USE_LIMIT_TIME FROM TBL_RESERVATION_INFO WHERE UID = '"& R_UID &"' AND RESERVE_STATUS <> 'C' AND RESERVE_DT BETWEEN '"& R_YEAR &"-"& Right("0" & R_MONTH,2) &"-01' AND DATEADD(MONTH, 1, '"& R_YEAR &"-"& Right("0" & R_MONTH,2) &"-01')"

	set objRsU = SendQuery(objConn,strQuery)

	strUseLimitTime = R_LIMIT_TIME
	If objRsU.EOF Then
		result("result") = ""
	Else
		strUseLimitTime = strUseLimitTime - objRsU("USE_LIMIT_TIME")
	End If

	'월 잔여무상시간 확인
	strQuery = "SELECT REMAINING_FREE_TIME FROM TBL_FREE_TIME WHERE FT_UID = '" & R_UID & "' AND FT_YEAR = '" & R_YEAR & "' AND FT_MONTH = '" & Right("0" & R_MONTH,2) & "'"
    set objRsU = SendQuery(objConn,strQuery)
    If objRsU.EOF Then
		strUseFreeTime = "0"
	Else
		strUseFreeTime =  objRsU("REMAINING_FREE_TIME")
	End If

    '날짜데이터 가져오기
	strQuery = "exec RESERVE_GET_DAY '"& R_YEAR &"', '"& R_MONTH &"', '"& R_NOW &"' "
	set objRs = SendQuery(objConn,strQuery)

	If objRs.EOF Then
		result("result") = ""
	Else
		raList = objRs.GetRows()
		firstDW = 0
		CalHeight = 1
		htmlUl = ""

		If IsArray(raList) Then
			firstDW = raList(2,0)
			For i=0 To Ubound(raList, 2)
				If raList(2,i) = 1 Then
					CalHeight = CalHeight + 1
				End IF
			Next
		End If
		If firstDW = 1 Then
			CalHeight = CalHeight - 1
		End If

		If IsArray(raList) Then
			For i=0 To 7 * CalHeight
				If i+1 < firstDW Then ' 전월
					htmlUl = htmlUl & "<li></li>"
				ElseIf i > Ubound(raList, 2) + firstDW Then ' 다음월
					htmlUl = htmlUl & "<li></li>"
				Else
					strClassTodayBefore = ""
					strTodayBefore = "N"
					strClassToday = ""
					strSpanToday = ""
					If raList(4,i-firstDW)&"" = "Y" Then						'오늘이전 날짜인경우
						strClassTodayBefore = " class='disabled'"
						strTodayBefore = "Y"
					Else
						If raList(2,i-firstDW)&"" = "1" Or raList(2,i-firstDW)&"" = "7" Or raList(5,i-firstDW)&"" = "Y" Or raList(5,i-firstDW)&"" = "C" Or raList(8,i-firstDW)&"" = "Y" Then      '주말 공휴일 익일
							strClassTodayBefore = " class='week'"
						End If
					End If
					If raList(3,i-firstDW)&"" = "Y" Then						'오늘날짜 인경우
						strClassToday = " class='today'"
						strSpanToday = "<span class='txt'>Today</span>"
					End If
					If raList(7,i-firstDW)&"" = "N" Then						'120일이후 날짜인경우
						strClassTodayBefore = " class='disabled'"
					End If
					htmlUl = htmlUl & "<li"& strClassTodayBefore & strClassToday &" onClick='fnDayClick("""& strTodayBefore &""","""& raList(1,i-firstDW) &""","""& raList(2,i-firstDW) &""","""& raList(5,i-firstDW) &""","""& raList(3,i-firstDW) &""","""& raList(8,i-firstDW) &""","""& raList(7,i-firstDW) &""", this);' style='cursor:pointer'>" & raList(1,i-firstDW) & strSpanToday &"</li>"
				End If
			Next
		End If

		result("result") = strUseLimitTime & "|" & strUseFreeTime & "|" & htmlUl 'firstDW & "|" & Ubound(raList, 2) & "|" & htmlUl
	End If

	result.Flush
	Response.End
ElseIf mode = "get_detail" Then
	If Request.Form("YEAR") = "" Then
		result("result") = ""
		result("error_msg") = "the year is empty"
		result.Flush
		Response.End
	End If
	If Request.Form("MONTH") = "" Then
		result("result") = ""
		result("error_msg") = "the month is empty"
		result.Flush
		Response.End
	End If
	If Request.Form("DAY") = "" Then
		result("result") = ""
		result("error_msg") = "the day is empty"
		result.Flush
		Response.End
	End If

	R_YEAR = Request.Form("YEAR")
	R_MONTH = Right("0" & Request.Form("MONTH"),2)
	R_DAY = Right("0" & Request.Form("DAY"),2)

	strQuery = "exec RESERVE_GET_TIME '"& R_YEAR & R_MONTH & R_DAY &"' "

	set objRs = SendQuery(objConn,strQuery)

	B0001YN = "N"
	R0001YN = "N"
	R0002YN = "N"
	R0003YN = "N"
	R0004YN = "N"
	htmlB0001 = ""
	htmlR0001 = ""
	htmlR0002 = ""
	htmlR0003 = ""
	htmlR0004 = ""

	If objRs.EOF Then
		B0001YN = "Y"
		R0001YN = "Y"
		R0002YN = "Y"
		R0003YN = "Y"
		R0004YN = "Y"
		For i = 1 To 9
			htmlB0001 = htmlB0001 & "<td onClick='fnTimeClick(""B0001"","""&i+8&""", this);' onMouseOver='fnTimeMOver(""B0001"","""&i+8&""", this);' onMouseOut='fnTimeMOut();' id='td_time_B0001_"&i+8&"'></td>"
		Next
		For i = 1 To 9
			htmlR0001 = htmlR0001 & "<td onClick='fnTimeClick(""R0001"","""&i+8&""", this);' onMouseOver='fnTimeMOver(""R0001"","""&i+8&""", this);' onMouseOut='fnTimeMOut();' id='td_time_R0001_"&i+8&"'></td>"
		Next
		For i = 1 To 9
			htmlR0002 = htmlR0002 & "<td onClick='fnTimeClick(""R0002"","""&i+8&""", this);' onMouseOver='fnTimeMOver(""R0002"","""&i+8&""", this);' onMouseOut='fnTimeMOut();' id='td_time_R0002_"&i+8&"'></td>"
		Next
		For i = 1 To 9
			htmlR0003 = htmlR0003 & "<td onClick='fnTimeClick(""R0003"","""&i+8&""", this);' onMouseOver='fnTimeMOver(""R0003"","""&i+8&""", this);' onMouseOut='fnTimeMOut();' id='td_time_R0003_"&i+8&"'></td>"
		Next
		For i = 1 To 9
			htmlR0004 = htmlR0004 & "<td onClick='fnTimeClick(""R0004"","""&i+8&""", this);' onMouseOver='fnTimeMOver(""R0004"","""&i+8&""", this);' onMouseOut='fnTimeMOut();' id='td_time_R0004_"&i+8&"'></td>"
		Next

		result("result") = B0001YN & "|" & R0001YN & "|" & R0002YN & "|" & R0003YN & "|" & R0004YN & "|" & htmlB0001 & "|" & htmlR0001 & "|" & htmlR0002 & "|" & htmlR0003 & "|" & htmlR0004
	Else
		raList = objRs.GetRows()

		If IsArray(raList) Then
			For i=0 To Ubound(raList, 2)
				If raList(0, i)&"" = "B0001" Then
					For j = 1 To 9
						If raList(j, i)&"" = "0" Then
							B0001YN = "Y"
							Exit For
						End If
					Next
					For k = 1 To 9
						nPreIdx = k -1
						nNextIdx = k +1
						If nNextIdx > 9 Then
							nNextIdx = 0
						End If
						strClass = ""
						If raList(k, i)&"" = "1" Then
							strClass = "class='disable_v01'"
						ElseIf raList(k, i)&"" = "0" And raList(nNextIdx, i)&"" = "1" Then
							strClass = "class='disable_v02'"
						ElseIf raList(k, i)&"" = "0" And raList(nPreIdx, i)&"" = "1" Then
							strClass = "class='disable_v02'"
						End If
						htmlB0001 = htmlB0001 & "<td "&strClass&" onClick='fnTimeClick(""B0001"","""&k+8&""", this);' onMouseOver='fnTimeMOver(""B0001"","""&k+8&""", this);' onMouseOut='fnTimeMOut();' id='td_time_B0001_"&k+8&"'></td>"
					Next
				End If
				If raList(0, i)&"" = "R0001" Then
					For j = 1 To 9
						If raList(j, i)&"" = "0" Then
							R0001YN = "Y"
							Exit For
						End If
					Next
					For k = 1 To 9
						nPreIdx = k -1
						nNextIdx = k +1
						If nNextIdx > 9 Then
							nNextIdx = 0
						End If
						strClass = ""
						If raList(k, i)&"" = "1" Then
							strClass = "class='disable_v01'"
						ElseIf raList(k, i)&"" = "0" And raList(nNextIdx, i)&"" = "1" Then
							strClass = "class='disable_v02'"
						ElseIf raList(k, i)&"" = "0" And raList(nPreIdx, i)&"" = "1" Then
							strClass = "class='disable_v02'"
						End If
						htmlR0001 = htmlR0001 & "<td "&strClass&" onClick='fnTimeClick(""R0001"","""&k+8&""", this);' onMouseOver='fnTimeMOver(""R0001"","""&k+8&""", this);' onMouseOut='fnTimeMOut();' id='td_time_R0001_"&k+8&"'></td>"
					Next
				End If
				If raList(0, i)&"" = "R0002" Then
					For j = 1 To 9
						If raList(j, i)&"" = "0" Then
							R0002YN = "Y"
							Exit For
						End If
					Next
					For k = 1 To 9
						nPreIdx = k -1
						nNextIdx = k +1
						If nNextIdx > 9 Then
							nNextIdx = 0
						End If
						strClass = ""
						If raList(k, i)&"" = "1" Then
							strClass = "class='disable_v01'"
						ElseIf raList(k, i)&"" = "0" And raList(nNextIdx, i)&"" = "1" Then
							strClass = "class='disable_v02'"
						ElseIf raList(k, i)&"" = "0" And raList(nPreIdx, i)&"" = "1" Then
							strClass = "class='disable_v02'"
						End If
						htmlR0002 = htmlR0002 & "<td "&strClass&" onClick='fnTimeClick(""R0002"","""&k+8&""", this);' onMouseOver='fnTimeMOver(""R0002"","""&k+8&""", this);' onMouseOut='fnTimeMOut();' id='td_time_R0002_"&k+8&"'></td>"
					Next
				End If
				If raList(0, i)&"" = "R0003" Then
					For j = 1 To 9
						If raList(j, i)&"" = "0" Then
							R0003YN = "Y"
							Exit For
						End If
					Next
					For k = 1 To 9
						nPreIdx = k -1
						nNextIdx = k +1
						If nNextIdx > 9 Then
							nNextIdx = 0
						End If
						strClass = ""
						If raList(k, i)&"" = "1" Then
							strClass = "class='disable_v01'"
						ElseIf raList(k, i)&"" = "0" And raList(nNextIdx, i)&"" = "1" Then
							strClass = "class='disable_v02'"
						ElseIf raList(k, i)&"" = "0" And raList(nPreIdx, i)&"" = "1" Then
							strClass = "class='disable_v02'"
						End If
						htmlR0003 = htmlR0003 & "<td "&strClass&" onClick='fnTimeClick(""R0003"","""&k+8&""", this);' onMouseOver='fnTimeMOver(""R0003"","""&k+8&""", this);' onMouseOut='fnTimeMOut();' id='td_time_R0003_"&k+8&"'></td>"
					Next
				End If
				If raList(0, i)&"" = "R0004" Then
					For j = 1 To 9
						If raList(j, i)&"" = "0" Then
							R0004YN = "Y"
							Exit For
						End If
					Next
					For k = 1 To 9
						nPreIdx = k -1
						nNextIdx = k +1
						If nNextIdx > 9 Then
							nNextIdx = 0
						End If
						strClass = ""
						If raList(k, i)&"" = "1" Then
							strClass = "class='disable_v01'"
						ElseIf raList(k, i)&"" = "0" And raList(nNextIdx, i)&"" = "1" Then
							strClass = "class='disable_v02'"
						ElseIf raList(k, i)&"" = "0" And raList(nPreIdx, i)&"" = "1" Then
							strClass = "class='disable_v02'"
						End If
						htmlR0004 = htmlR0004 & "<td "&strClass&" onClick='fnTimeClick(""R0004"","""&k+8&""", this);' onMouseOver='fnTimeMOver(""R0004"","""&k+8&""", this);' onMouseOut='fnTimeMOut();' id='td_time_R0004_"&k+8&"'></td>"
					Next
				End If
			Next
		End If
		result("result") = B0001YN & "|" & R0001YN & "|" & R0002YN & "|" & R0003YN & "|" & R0004YN & "|" & htmlB0001 & "|" & htmlR0001 & "|" & htmlR0002 & "|" & htmlR0003 & "|" & htmlR0004
	End If

	result.Flush
	Response.End
ElseIf mode = "reservation" Then
	error_flag = False

	If Request.Form("UID") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the UID is empty | "
		error_flag = true
	End If
	If Request.Form("SUBJECT") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the SUBJECT is empty | "
		error_flag = true
	End If
	If Request.Form("P_COUNT") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the P_COUNT is empty | "
		error_flag = true
	End If
	If Request.Form("EMAIL") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the EMAIL is empty"
		error_flag = true
	End If
	If Request.Form("YEAR") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the YEAR is empty"
		error_flag = true
	End If
	If Request.Form("MONTH") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the MONTH is empty"
		error_flag = true
	End If
	If Request.Form("DAY") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the DAY is empty"
		error_flag = true
	End If
	If Request.Form("R_QTY") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the R_QTY is empty"
		error_flag = true
	End If
	If Request.Form("T_QTY") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the T_QTY is empty"
		error_flag = true
	End If
	If Request.Form("ORI_AMOUNT") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the ORI_AMOUNT is empty"
		error_flag = true
	End If
	If Request.Form("DISCOUNT") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the DISCOUNT is empty"
		error_flag = true
	End If
	If Request.Form("ETC_AMOUNT") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the ETC_AMOUNT is empty"
		error_flag = true
	End If
	If Request.Form("FREE_TIME") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the FREE_TIME is empty"
		error_flag = true
	End If
	If Request.Form("TOTAL_AMOUNT") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the TOTAL_AMOUNT is empty"
		error_flag = true
	End If
	If Request.Form("ROOM_NOS") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the ROOM_NOS is empty"
		error_flag = true
	End If
	If Request.Form("ROOM_NAMES") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the ROOM_NAMES is empty"
		error_flag = true
	End If
	If Request.Form("RESERVE_TIMES") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the RESERVE_TIMES is empty"
		error_flag = true
	End If
	If Request.Form("REG_UID") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the REG_UID is empty"
		error_flag = true
	End If

	If error_flag Then
		result.Flush
		Response.End
	End If

	UID = GetTag2Text(Request.Form("UID"))
	USER_NAME = GetTag2Text(Request.Form("USER_NAME"))
	SUBJECT = GetTag2Text(Request.Form("SUBJECT"))
	RESERVE_NAME = GetTag2Text(Request.Form("RESERVE_NAME"))
	P_COUNT = GetTag2Text(Request.Form("P_COUNT"))
	TELNUM = GetTag2Text(Request.Form("TELNUM"))
	FAXNUM = GetTag2Text(Request.Form("FAXNUM"))
    FILMING_EQUIPMENT = GetTag2Text(Request.Form("FILMING_EQUIPMENT"))
	MOBILENUM = GetTag2Text(Request.Form("MOBILENUM"))
	EMAIL = GetTag2Text(Request.Form("EMAIL"))
	REQUIREMENT = GetTag2Text(Request.Form("REQUIREMENT"))
	R_YEAR = GetTag2Text(Request.Form("YEAR"))
	R_MONTH = GetTag2Text(Right("0" & Request.Form("MONTH"),2))
	R_DAY = GetTag2Text(Right("0" & Request.Form("DAY"),2))
	R_QTY = GetTag2Text(Request.Form("R_QTY"))
	T_QTY = GetTag2Text(Request.Form("T_QTY"))
	POLYCOM = GetTag2Text(Request.Form("POLYCOM"))
	ETC = GetTag2Text(Request.Form("ETC"))
	ORI_AMOUNT = GetTag2Text(Request.Form("ORI_AMOUNT"))
	DISCOUNT = GetTag2Text(Request.Form("DISCOUNT"))
	ETC_AMOUNT = GetTag2Text(Request.Form("ETC_AMOUNT"))
	FREE_TIME = GetTag2Text(Request.Form("FREE_TIME"))
	TOTAL_AMOUNT = GetTag2Text(Request.Form("TOTAL_AMOUNT"))
	ROOM_NOS = GetTag2Text(Request.Form("ROOM_NOS"))
	ROOM_NAMES = GetTag2Text(Request.Form("ROOM_NAMES"))
	RESERVE_TIMES = GetTag2Text(Request.Form("RESERVE_TIMES"))
	REG_UID = GetTag2Text(Request.Form("REG_UID"))

	// insert
	strQuery = "exec RESERVE_INSERT '"& UID &"', '"& R_YEAR &"', '"& R_MONTH &"', '"& R_DAY &"', "& P_COUNT &", "& R_QTY &", "& T_QTY &", '"& SUBJECT &"', '"& RESERVE_NAME &"', '"& USER_NAME &"', '"& TELNUM &"', '"& MOBILENUM &"', '"& FAXNUM &"', '"& FILMING_EQUIPMENT &"', '"& EMAIL &"', '"& REQUIREMENT &"', '', '"& POLYCOM &"', '"& ETC &"', "& ORI_AMOUNT &", "& DISCOUNT &", "& ETC_AMOUNT &", "& FREE_TIME &", "& TOTAL_AMOUNT &", '"& REG_UID &"', '"& ROOM_NOS &"', '"& ROOM_NAMES &"', '"& RESERVE_TIMES &"' "

	set objRs = SendQuery(objConn,strQuery)

	If Err.Number <> 0 Then
		result("result") = ""
		result("error_msg") = "Insert Error : " & Err.Description
		result.Flush
		Response.End
	ElseIf objRs.EOF Then
		result("result") = ""
		result("error_msg") = "Insert Error : Insert Fail "
		result.Flush
		Response.End
	Else
		If objRs("RESERVE_NO")&"" = "" Then
			result("result") = ""
			result("error_msg") = objRs("ERR_MSG")
			result.Flush
			Response.End
		Else
			result("result") = objRs("RESERVE_NO")
		End If
	End If

	result.Flush
	Response.End
ElseIf mode = "edit" Then
	error_flag = false

	If Request.Form("RESERVE_NO") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the RESERVE_NO is empty | "
		error_flag = true
	End If
	If Request.Form("SUBJECT") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the SUBJECT is empty | "
		error_flag = true
	End If
	If Request.Form("RESERVE_NAME") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the RESERVE_NAME is empty | "
		error_flag = true
	End If
	If Request.Form("TELNUM") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the TELNUM is empty"
		error_flag = true
	End If
	If Request.Form("MOBILENUM") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the MOBILENUM is empty"
		error_flag = true
	End If
	If Request.Form("EMAIL") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the EMAIL is empty"
		error_flag = true
	End If
	If Request.Form("UPD_UID") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the UPD_UID is empty"
		error_flag = true
	End If

	If error_flag Then
		result.Flush
		Response.End
	End If

	RESERVE_NO = GetTag2Text(Request.Form("RESERVE_NO"))
	SUBJECT = GetTag2Text(Request.Form("SUBJECT"))
	RESERVE_NAME = GetTag2Text(Request.Form("RESERVE_NAME"))
	TELNUM = Request.Form("TELNUM")
	MOBILENUM = GetTag2Text(Request.Form("MOBILENUM"))
	FAXNUM = GetTag2Text(Request.Form("FAXNUM"))
	EMAIL = GetTag2Text(Request.Form("EMAIL"))
	DESCRIPTION = GetTag2Text(Request.Form("DESCRIPTION"))
	UPD_UID = GetTag2Text(Request.Form("UPD_UID"))

	// update
	strQuery = "UPDATE TBL_RESERVATION_INFO SET SUBJECT = '"& SUBJECT &"', RESERVE_NAME = '"& RESERVE_NAME &"', TELNUM = '"& TELNUM &"', MOBILENUM = '"& MOBILENUM &"', FAXNUM = '"& FAXNUM &"', EMAIL = '"& EMAIL &"', DESCRIPTION = '"& DESCRIPTION &"' WHERE RESERVE_NO = '"& RESERVE_NO &"'"
	objConn.Execute(strQuery)

	If Err.Number <> 0 Then
		result("result") = ""
		result("error_msg") = "Update Error : " & Err.Description & " "
		result.Flush
		Response.End
	Else
		result("result") = "Y"
	End If

	result.Flush
	Response.End
ElseIf mode = "cancel" Then
	error_flag = false

	If Request.Form("RESERVE_NO") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the RESERVE_NO is empty | "
		error_flag = true
	End If
	If Request.Form("CAN_UID") = "" Then
		result("result") = ""
		result("error_msg") = result("error_msg") & "the CAN_UID is empty | "
		error_flag = true
	End If

	If error_flag Then
		result.Flush
		Response.End
	End If

	RESERVE_NO = GetTag2Text(Request.Form("RESERVE_NO"))
	CAN_UID = GetTag2Text(Request.Form("CAN_UID"))

	strQuery = "SELECT (SELECT MIN(CONVERT(INT,RESERVE_TIME)) FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MIN_TIME, * FROM TBL_RESERVATION_INFO RI WHERE RESERVE_NO = '"& RESERVE_NO &"'"

	set objRs = SendQuery(objConn,strQuery)

	If objRs.EOF Then
		result("result") = ""
		result("error_msg") = "Cancel Error : " & Err.Description
		result.Flush
		Response.End
	Else
		strReserveDt = ""
		If Len(objRs("RESERVE_DT")) = 8 Then
			strReserveDt = Mid(objRs("RESERVE_DT"),1,4) & "-" & Mid(objRs("RESERVE_DT"),5,2) & "-" & Mid(objRs("RESERVE_DT"),7,2)
		End If
	End If

	'취소위약금 계산
	CAN_AMOUNT = 0
	If DateDiff("d", NOW, CDate(strReserveDt)) <= 14 Then				'14일~이용당일 위약금 100%
		CAN_AMOUNT = objRs("TOTAL_AMOUNT")
	ElseIf DateDiff("d", NOW, CDate(strReserveDt)) < 30 Then			'15일~29일전까지 위약금 50%
		CAN_AMOUNT = objRs("TOTAL_AMOUNT") / 2
	End If

	// insert
	strQuery = "exec RESERVE_CANCEL '"& RESERVE_NO &"', "& CAN_AMOUNT &", '"& CAN_UID &"' "

	set objCRs = SendQuery(objConn,strQuery)

	If Err.Number <> 0 Then
		result("result") = ""
		result("error_msg") = "Cancel Error : " & Err.Description
		result.Flush
		Response.End
	ElseIf objCRs.EOF Then
		result("result") = ""
		result("error_msg") = "Cancel Error : Cancel Fail "
		result.Flush
		Response.End
	Else
		If objCRs("SUC_YN")&"" = "N" Then
			result("result") = ""
			result("error_msg") = objCRs("ERR_MSG")
			result.Flush
			Response.End
		Else
			result("result") = RESERVE_NO
		End If
	End If

	result.Flush
	Response.End
Else
	result("result") = ""
	result("error_msg") = "The mode is not selected"
	result.Flush
	Response.End
End If

%>
