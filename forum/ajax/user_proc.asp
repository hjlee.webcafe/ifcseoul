<!-- #include file = "../inc/_config.asp" -->
<%
' remove cache
Response.Expires = -1
Response.ExpiresAbsolute = now() - 1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"


Dim mode, result
Set result = jsObject()
result("result") = 0
result("error_msg") = ""

Dim objConn, objRs, arrRs, strQuery
set objConn = OpenDBConnection()

if Request.Form("mode") = "" Then
	result("result") = 2
	result("error_msg") = "The mode is not selected"
	result.Flush
	Response.End
End If

mode = Request.Form("mode")

Dim as_num, as_name, as_id, as_pw, as_tel, as_email, error_flag, selected, select_array, num, pw_where
Err.clear
On Error Resume Next

If mode = "pwd_edit" Then
	error_flag = false

	If Request.Form("UID") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the uid is empty | "
		error_flag = true
	End If
	If Request.Form("ORI_PASSWORD") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the ori_password is empty | "
		error_flag = true
	End If
	If Request.Form("NEW_PASSWORD") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the new_password is empty"
		error_flag = true
	End If
	If Request.Form("UPD_UID") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the upd_uid is empty"
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	UID = GetTag2Text(Request.Form("UID"))
	ORI_PASSWORD = Request.Form("ORI_PASSWORD")
	NEW_PASSWORD = Request.Form("NEW_PASSWORD")
	UPD_UID = GetTag2Text(Request.Form("UPD_UID"))

	strQuery = "select * from TBL_USER_INFO where UID = '"& UID &"' "

	set objRs = SendQuery(objConn,strQuery)
	
	If objRs.EOF Then
		result("result") = 2
		result("error_msg") = "Update Error : " & Err.Description
		result.Flush
		Response.End
	Else
		If ORI_PASSWORD <> objRs("PASSWORD") Then
			result("result") = -1
			result("error_msg") = "wrong password"
			result.Flush
			Response.End
		End If
	End If

	// update 
	strQuery = "UPDATE TBL_USER_INFO SET PASSWORD = '"& NEW_PASSWORD &"', UPD_DT = GETDATE(), UPD_UID = '"& UPD_UID &"' WHERE UID = '"& UID &"'"
	objConn.Execute(strQuery)
	
	If Err.Number <> 0 Then
		result("result") = 2
		result("error_msg") = "Update Error : " & Err.Description
		result.Flush
		Response.End
	End If
	
	result.Flush
	Response.End

ElseIf mode = "del" Then

	error_flag = false

	If Request.Form("UID") = "" Then
		result("result") = 2
		result("error_msg") = "the uid is empty"
		error_flag = true
	End If
	If Request.Form("DEL_UID") = "" Then
		result("result") = 2
		result("error_msg") = "the del_uid is empty"
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	UID = Request.Form("UID")
	DEL_UID = Request.Form("DEL_UID")
	'select_array = Split(uid, "|")
	'
	'For i = 0 To UBound(select_array) - 1
	'	num = select_array(i)
	'	// delete 
		strQuery = "UPDATE TBL_USER_INFO SET USE_FLAG = 'N', DEL_DT = GETDATE(), DEL_UID = '"& DEL_UID &"' WHERE UID = '" & UID & "'"
		objConn.Execute(strQuery)
		
		If Err.Number <> 0 Then
			result("result") = 2
			result("error_msg") = "Delete Error : " & Err.Description
			result.Flush
			Response.End
		End If
	'Next
	
	result.Flush
	Response.End

End If

%>