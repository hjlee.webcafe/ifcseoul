<!--#include file = "default_control.asp" -->
<%
re_url = "reservation_list"
%>
<!--#include file = "session_check.asp" -->
<!--#include file = "index_header.asp" -->
<%
Dim selectQuery, reserve_no
reserve_no = Request.QueryString("reserve_no")

selectQuery = "select (SELECT MIN(CONVERT(INT,RESERVE_TIME)) FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MIN_TIME, (SELECT MAX(CONVERT(INT,RESERVE_TIME))+1 FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MAX_TIME, (SELECT BUSINESS_RE_NO FROM TBL_USER_INFO WHERE UID=RI.UID) AS BUSINESS_RE_NO, * from TBL_RESERVATION_INFO RI where RESERVE_NO = '"& reserve_no &"'"

set objRs = SendQuery(objConn,selectQuery)

If objRs.EOF Then
%>
	<script>
		alert("비정상적인 접속입니다.");
		location.href="reservation_list.asp";
	</script>
<%
Else
	strBUSINESS_RE_NO = objRs("BUSINESS_RE_NO")
	If Len(strBUSINESS_RE_NO) > 7 Then
		strBUSINESS_RE_NO = Left(strBUSINESS_RE_NO, 7) & "*****"
	End If

	strReserveDt = objRs("RESERVE_DT")
	If Len(strReserveDt) = 8 Then
		strReserveDt = Mid(strReserveDt,1,4) & "년 " & Mid(strReserveDt,5,2) & "월 " & Mid(strReserveDt,7,2) & "일"
	End If

    '예약시간표시
    arrReserveTime = Split(objRs("RESERVE_TIMES"), ",")
    For i=0 To UBound(arrReserveTime)
        strLastTime = CInt(arrReserveTime(i)) + 1
    Next

	strRequirement = objRs("REQUIREMENT")
	If strRequirement&"" <> "" Then
		strRequirement = Replace(Replace(strRequirement, "&lt", "<"),"<br><br>","<br>")
	End If

	strPolycom = ""
	If objRs("POLYCOM")&"" = "N" Then
		strPolycom = "사용안함"
	ElseIf objRs("POLYCOM")&"" = "L" Then
		strPolycom = "국내전화"
	ElseIf objRs("POLYCOM")&"" = "G" Then
		strPolycom = "국외전화"
	End If

	strEtc = ""
	If objRs("ETC")&"" = "N" Then
		strEtc = "반입안함"
	ElseIf objRs("ETC")&"" = "Y" Then
		strEtc = "반입함"
	End If

    strFilEquipment = ""
    If objRs("FILMING_EQUIPMENT")&"" = "N" Then
        strFilEquipment = "반입안함"
    ElseIf objRs("FILMING_EQUIPMENT")&"" = "Y" Then
        strFilEquipment = "반입함"
    End If

	strOriAmount = FnMoneySet(objRs("ORI_AMOUNT")) & " 원"
	strTotalAmount = FnMoneySet(objRs("TOTAL_AMOUNT"))
End If
%>
<script type="text/javascript">
	function fnGoList() {
		get_link = "reservation_list.asp";
		location.href = get_link;
	}
</script>
<div class="contBody">
	<div class="pTitle"><h2>예약 완료</h2></div>
	<div class="reservedName">
		<p><%=objRs("SUBJECT")%></p>
	</div>

	<div class="pTitle"><h2>예약 정보</h2></div>
	<div class="tblStyle1">
		<table>
			<caption>예약 정보</caption>
			<colgroup>
				<col style="width:17%"/>
				<col style="width:37%"/>
				<col style="width:15%"/>
				<col style="width:*%"/>
			</colgroup>
			<tbody>
				<tr>
					<th scope="row"><span>예약날짜</span></th>
					<td><%=strReserveDt%></td>
					<th scope="row"><span>예약시간</span></th>
					<td><%=objRs("MIN_TIME")%>:00~<%=strLastTime%>:00</td>
				</tr>
				<tr>
					<th scope="row"><span>회의실</span></th>
					<td><%=objRs("ROOM_NAMES")%></td>
					<th scope="row"><span>참석인원</span></th>
					<td><%=objRs("P_COUNT")%>명</td>
				</tr>
				<tr>
					<th scope="row"><span>요청사항</span></th>
					<td colspan="3"><%=strRequirement%></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="pTitle"><h2>예약자 정보</h2></div>
	<div class="tblStyle1">
		<table>
			<caption>예약자 정보</caption>
			<colgroup>
				<col style="width:17%"/>
				<col style="width:37%"/>
				<col style="width:15%"/>
				<col style="width:*%"/>
			</colgroup>
			<tbody>
				<tr>
					<th scope="row"><span>회사명</span></th>
					<td><%=objRs("USER_NAME")%></td>
					<th scope="row"><span>FAX</span></th>
					<td><%=FnSetNum(objRs("FAXNUM"))%></td>
				</tr>
				<tr>
					<th scope="row"><span>사업자등록번호</span></th>
					<td><%=strBUSINESS_RE_NO%></td>
					<th scope="row"><span>휴대폰번호</span></th>
					<td><%=FnSetNum(objRs("MOBILENUM"))%></td>
				</tr>
				<tr>
					<th scope="row"><span>예약자명</span></th>
					<td><%=objRs("RESERVE_NAME")%></td>
					<th scope="row"><span>이메일</span></th>
					<td><%=objRs("EMAIL")%></td>
				</tr>
				<tr>
					<th scope="row"><span>전화번호</span></th>
					<td colspan="3"><%=FnSetNum(objRs("TELNUM"))%></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="pTitle"><h2>기자재 및 요금정보</h2></div>
	<div class="tblStyle1 last">
		<table>
			<caption>기자재 및 요금정보</caption>
			<colgroup>
				<col style="width:17%"/>
				<col style="width:37%"/>
				<col style="width:15%"/>
				<col style="width:*%"/>
			</colgroup>
			<tbody>
				<tr>
					<th scope="row"><span>폴리콤(3자통화)</span></th>
					<td><%=strPolycom%></td>
					<th scope="row"><span>외부기자재반입</span></th>
					<td><%=strEtc%></td>
				</tr>
                <tr>
                    <th scope="row"><span>촬영장비반입</span></th>
                    <td colspan="3"><%=strFilEquipment%></td>
                </tr>
				<tr>
					<th scope="row"><span>기본요금</span></th>
					<td><%=strOriAmount%> (할인율 <%=objRs("DISCOUNT")%>% 적용)</td>
					<th scope="row"><span>무료 이용시간</span></th>
					<td><%=objRs("FREE_TIME")%>시간</td>
				</tr>
				<tr>
					<th scope="row"><span>최종금액</span></th>
					<td colspan="3" class="bold"><em class="poTxt01"><%=strTotalAmount%></em>원 (VAT 별도)</td>
				</tr>
			</tbody>
		</table>
	</div>
	<ul class="listDot">
		<li>외부기자재 반입하는 경우 관리자에게 별도 문의 바랍니다.</li>
		<li>폴리콤(3자 통화) 사용 시 별도 비용이 청구됩니다.</li>
		<li>안내데스크 : 02-6137-2100</li>
	</ul>

	<div class="btnWrap">
		<button class="nbtnG" onClick="fnGoList();">확인</button>
	</div>
</div>
<!--#include file = "index_footer.asp" -->
