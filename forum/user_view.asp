<!--#include file = "default_control.asp" -->
<%
re_url = "user_view"
%>
<!--#include file = "session_check.asp" -->
<!--#include file = "index_header.asp" -->
<%
Dim selectQuery, uid, nowMonth, nextMonth
Dim arrRemainingFreeTime(11), iYearRemainingFreeTime        '무상시간관련
uid = strUserId

nowMonth = Year(NOW) & "-" & Right("0" & MONTH(NOW),2) & "-01"
nextMonth = DateAdd("m", 1, nowMonth)

selectQuery = "SELECT *, (SELECT ISNULL(SUM(T_QTY*R_QTY),0) FROM TBL_RESERVATION_INFO WHERE UID=UI.UID AND RESERVE_STATUS <> 'C' AND RESERVE_DT BETWEEN '" & nowMonth & "' AND '" & nextMonth & "') AS USE_TIME from TBL_USER_INFO UI where UID = '"& uid &"'"
set objRs = SendQuery(objConn,selectQuery)

'무상시간 확인 쿼리
selectQuery = "SELECT FT_YEAR, FT_MONTH, REMAINING_FREE_TIME FROM TBL_FREE_TIME WHERE FT_UID = '"& uid &"' AND FT_YEAR = YEAR(GETDATE())"
set objRsT = SendQuery(objConn,selectQuery)

iYearRemainingFreeTime = 0
For i = 0 to objRsT.RecordCount - 1
	arrRemainingFreeTime(CInt(objRsT("FT_MONTH"))-1) = objRsT("REMAINING_FREE_TIME")
	iYearRemainingFreeTime = iYearRemainingFreeTime + objRsT("REMAINING_FREE_TIME")
	objRsT.MoveNext
Next

If objRs.EOF Then
%>
	<script>
		alert("비정상적인 접속입니다.");
		location.href="../index.asp";
	</script>
<%
Else
	strBUSINESS_RE_NO = objRs("BUSINESS_RE_NO")
	If Len(strBUSINESS_RE_NO) > 7 Then
		strBUSINESS_RE_NO = Left(strBUSINESS_RE_NO, 7) & "*****"
	End IF
End If
%>

<script type="text/javascript">
	function fnGoBack() {
		location.href = "forum_info.asp";
	}

	function fnPwdChg(objVal) {
		location.href = "user_edit.asp?uid=" + objVal;
	}
</script>

<div class="contBody">
	<div class="pTitle"><h2>My page</h2></div>
	<div class="tblStyle2">
		<table>
			<caption>나의 상세정보</caption>
			<colgroup>
				<col style="width:19%"/><col style="width:35%"/><col style="width:17%"/><col style="width:*%"/>
			</colgroup>
			<tbody>
				<tr>
					<th scope="row">아이디</th>
					<td><%=objRs("UID")%></td>
					<th scope="row">비밀번호</th>
					<td><button class="nbtnG03" id="btn_pw_chg" onClick="fnPwdChg('<%=objRs("UID")%>');">변경하기</button></td>
				</tr>
				<tr>
					<th scope="row">사업자등록번호</th>
					<td colspan="3"><%=strBUSINESS_RE_NO%></td>
				</tr>
				<tr>
					<th scope="row">입주사명</th>
					<td><%=objRs("USER_NAME")%></td>
					<th scope="row">회사 전화번호</th>
					<td><%=FnSetNum(objRs("TELNUM"))%></td>
				</tr>
				<tr>
					<th scope="row">FAX</th>
					<td><%=FnSetNum(objRs("FAXNUM"))%></td>
					<th scope="row">대표 이메일</th>
					<td><%=objRs("EMAIL")%></td>
				</tr>
				<tr>
					<th scope="row">월간 예약 가능시간</th>
					<td colspan="3"><%=objRs("LIMIT_TIME")%>시간 중 <%=objRs("USE_TIME")%>시간 사용</td>
				</tr>
				<tr>
					<th scope="row">월별 무상 제공시간</th>
 					<td colspan="3"><%=objRs("MONTHLY_FREE_TIME")%>시간</td>
				</tr>
				<tr>
					<th scope="row">해당년도 무상시간</th>
 					<td colspan="3"><%=iYearRemainingFreeTime%> 시간</td>
				</tr>
			</tbody>
		</table>
	</div>

	<br/><h2><%=Year(NOW)%>년 월별 잔여 무상시간</h2>
	<div class="tblStyle2" style="margin-top:0">
		<table>
			<caption>나의 상세정보</caption>
			<tbody>
				<tr>
					<% For i=0 to 11 %>
					    <th scope="row"><%=(i+1)%> 월</th>
					<% Next %>
				</tr>
				<tr>
				    <%
				    For i=0 to 11
				    	If IsEmpty(arrRemainingFreeTime(i)) Then
				    	    Response.Write "<td scope='row'>0</td>"
				    	Else
				    	    Response.Write "<td scope='row'>" & arrRemainingFreeTime(i) & "</td>"
					    End If
					Next
					%>
				</tr>
			</tbody>
		</table>
	</div>

	<ul class="listDot">
		<li>비밀번호 변경만 가능합니다.</li>
		<li>정보 변경 및 문의사항은 02-6137-2100로 연락주시기 바랍니다.</li>
	</ul>


	<div class="btnWrap">
		<button class="nbtnG" id="btn_goBack" onClick="fnGoBack();">확인</button>
	</div>
</div>
<!--#include file = "index_footer.asp" -->
