<!--#include file = "default_control.asp" -->
<%
re_url = "reservation"
%>
<!--#include file = "session_check.asp" -->
<!--#include file = "index_header.asp" -->
<%
R_YEAR = Request.Form("YEAR")
R_MONTH = Request.Form("MONTH")
R_DAY = Request.Form("DAY")
P_COUNT = Request.Form("P_COUNT")
R_QTY = Request.Form("R_QTY")
T_QTY = Request.Form("T_QTY")
POLYCOM = Request.Form("POLYCOM")
ETC = Request.Form("ETC")
ORI_AMOUNT = Request.Form("ORI_AMOUNT")
DISCOUNT = Request.Form("DISCOUNT")
FREE_TIME = Request.Form("FREE_TIME")
TOTAL_AMOUNT = Request.Form("TOTAL_AMOUNT")
ROOM_NOS = Request.Form("ROOM_NOS")
ROOM_NAMES = Request.Form("ROOM_NAMES")
RESERVE_TIMES = Request.Form("RESERVE_TIMES")

m3Day = Replace(Left(DateAdd("d", -3, CDate(R_YEAR & "-" & R_MONTH & "-" & R_DAY)), 10), "-", "")
m2Day = Replace(Left(DateAdd("d", -2, CDate(R_YEAR & "-" & R_MONTH & "-" & R_DAY)), 10), "-", "")
m1Day = Replace(Left(DateAdd("d", -1, CDate(R_YEAR & "-" & R_MONTH & "-" & R_DAY)), 10), "-", "")
p1Day = Replace(Left(DateAdd("d", +1, CDate(R_YEAR & "-" & R_MONTH & "-" & R_DAY)), 10), "-", "")
p2Day = Replace(Left(DateAdd("d", +2, CDate(R_YEAR & "-" & R_MONTH & "-" & R_DAY)), 10), "-", "")
p3Day = Replace(Left(DateAdd("d", +3, CDate(R_YEAR & "-" & R_MONTH & "-" & R_DAY)), 10), "-", "")

selectQuery = "SELECT (SELECT COUNT(*) FROM TBL_RESERVATION_INFO WHERE UID = '"&strUserId&"' AND RESERVE_STATUS <> 'C' AND RESERVE_DT IN ('" & m3Day & "','" & m2Day & "','" & m1Day& "') AND ROOM_NAMES = '"&ROOM_NAMES&"' AND RESERVE_TIMES = '"&RESERVE_TIMES&"') AS CNT1, (SELECT COUNT(*) FROM TBL_RESERVATION_INFO WHERE UID = '"&strUserId&"' AND RESERVE_STATUS <> 'C' AND RESERVE_DT IN ('" & m2Day & "','" & m1Day & "','" & p1Day& "') AND ROOM_NAMES = '"&ROOM_NAMES&"' AND RESERVE_TIMES = '"&RESERVE_TIMES&"') AS CNT2, (SELECT COUNT(*) FROM TBL_RESERVATION_INFO WHERE UID = '"&strUserId&"' AND RESERVE_STATUS <> 'C' AND RESERVE_DT IN ('" & m1Day & "','" & p1Day & "','" & p2Day& "') AND ROOM_NAMES = '"&ROOM_NAMES&"' AND RESERVE_TIMES = '"&RESERVE_TIMES&"') AS CNT3, (SELECT COUNT(*) FROM TBL_RESERVATION_INFO WHERE UID = '"&strUserId&"' AND RESERVE_STATUS <> 'C' AND RESERVE_DT IN ('" & p1Day & "','" & p2Day & "','" & p3Day& "') AND ROOM_NAMES = '"&ROOM_NAMES&"' AND RESERVE_TIMES = '"&RESERVE_TIMES&"') AS CNT4"

set objCRs = SendQuery(objConn,selectQuery)
If objCRs.EOF Then
%>
	<script>
		alert("비정상적인 접속입니다.");
		location.href="reservation.asp";
	</script>
<%
Else
	If (CInt(objCRs("CNT1")) >= 3 OR CInt(objCRs("CNT2")) >= 3 OR CInt(objCRs("CNT3")) >= 3 OR CInt(objCRs("CNT4")) >= 3) Then
%>
		<script>
			alert("선택한 내용으로 예약이 불가능합니다.\n\n(동일한 조건 4일 이상 예약불가)");
			location.href="reservation.asp";
		</script>
<%
	End If
End If


arrRESERVE_TIMES = Split(RESERVE_TIMES,",")
If IsArray(arrRESERVE_TIMES) Then
	firstTime = ""
	lastTime = ""
	For i=0 To UBound(arrRESERVE_TIMES)
		If i = 0 Then
			firstTime = arrRESERVE_TIMES(i)
		End If
		If i = UBound(arrRESERVE_TIMES) Then
			lastTime = CInt(arrRESERVE_TIMES(i)) + 1
		End If
	Next
End If


selectQuery = "select * from TBL_USER_INFO where UID = '" & strUserId & "' ORDER BY USER_NAME ASC"

set objRs = SendQuery(objConn,selectQuery)

If objRs.EOF Then
%>
	<script>
		alert("비정상적인 접속입니다.");
		location.href="reservation.asp";
	</script>
<%
Else
	strBUSINESS_RE_NO = objRs("BUSINESS_RE_NO")
	If Len(strBUSINESS_RE_NO) > 7 Then
		strBUSINESS_RE_NO = Left(strBUSINESS_RE_NO, 7) & "*****"
	End If
End If
%>
<script type="text/javascript">

    $(document).ready(function() {
        fnFilEquipment('N');
    });

    // 촬영장비반입 선택
    function fnFilEquipment(objVal) {
        $("#FILMING_EQUIPMENT").val(objVal);
    }

	// 예약하기
	function fnReservation() {

		if ($("#SUBJECT").val() == "") {
			alert("회의명을 입력해주세요.");
			$("#SUBJECT").focus();
			return true;
		}

		if ($("#RESERVE_NAME").val() == "") {
			alert("예약자명을 입력해주세요.");
			$("#RESERVE_NAME").focus();
			return true;
		}

		if ($("#TELNUM").val() == "") {
			alert("전화번호를 입력해주세요.");
			$("#TELNUM").focus();
			return true;
		}

		if ($("#MOBILENUM").val() == "") {
			alert("휴대폰번호를 입력해주세요.");
			$("#MOBILENUM").focus();
			return true;
		}

		if ($("#P_COUNT").val() == "") {
			alert("참석인원을 입력해주세요.");
			$("#P_COUNT").focus();
			return true;
		}

		if ($("#EMAIL").val() == "") {
			alert("이메일을 입력해주세요.");
			$("#EMAIL").focus();
			return true;
		}
		else {
			var emailReg = new RegExp(/^([0-9a-zA-Z_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,3}$/);
			if (!emailReg.test($("#EMAIL").val())) {
				alert("이메일 주소가 잘못되었습니다.");
				$("#EMAIL").focus();
				return true;
			}
		}

		// 이용약관 규정 동의
		if ($("#chk_agree").is(":checked") == false) {
			alert("이용약관에 동의해 주시기 바랍니다.");
			$("#chk_agree").focus();
			return true;
		}

		// 위약금관련 팝업 후 예약 프로시저 실행
		showCheckLayerPopup();
	}

	// 위약금 규정동의 팝업
	function showCheckLayerPopup() {

		var $popObj = $('#layerPopChk');

		$popObj.show().css({ 'width': Number(890) }); //Number(width)
		var popMargTop  = ($popObj.height()) / 2;
		var popMargLeft = ($popObj.width()) / 2;

		$popObj.css({
		  'margin-top': -popMargTop,
		  'margin-left': -popMargLeft
		});

		var $wrap = $('body');
		if ($wrap.find('#popFade').length < 1) {
		      $wrap.append('<div id="popFade"></div>')
		}
		$('#popFade').show();
		$('html').addClass('hidden');

		$('#layerPopChk button').on('click', function() {
			if ($("#chk_agree_3").is(":checked") == false) {
			    alert("위약금 규정에 동의해 주시기 바랍니다.");
			    $("#chk_agree_3").focus();
			    return false;
			} else {
				$(this).parents('.lyPopWrap').trigger('closePopup');
				ExecReservationProc();
			}
		});
	}

	// 예약
	function ExecReservationProc() {

		$("#index_loading_overlay").show();

		// DB
		var reserve_no = "";
		$.ajax ({
			type : "POST",
			url : "./ajax/reservation_proc.asp",
			dataType : "json",
			data : ({	"UID" : $("#REG_UID").val(),
						"USER_NAME" : $("#USER_NAME").val(),
						"SUBJECT" : $("#SUBJECT").val(),
						"RESERVE_NAME" : $("#RESERVE_NAME").val(),
						"P_COUNT" : $("#P_COUNT").val(),
						"TELNUM" : $("#TELNUM").val(),
						"FAXNUM" : $("#FAXNUM").val(),
                        "FILMING_EQUIPMENT" : $("#FILMING_EQUIPMENT").val(),
						"MOBILENUM" : $("#MOBILENUM").val(),
						"EMAIL" : $("#EMAIL").val(),
						"REQUIREMENT" : $("#REQUIREMENT").val(),
						"YEAR" : $("#YEAR").val(),
						"MONTH" : $("#MONTH").val(),
						"DAY" : $("#DAY").val(),
						"R_QTY" : $("#R_QTY").val(),
						"T_QTY" : $("#T_QTY").val(),
						"POLYCOM" : $("#POLYCOM").val(),
						"ETC" : $("#ETC").val(),
						"ORI_AMOUNT" : $("#ORI_AMOUNT").val(),
						"DISCOUNT" : $("#DISCOUNT").val(),
						"ETC_AMOUNT" : $("#ETC_AMOUNT").val(),
						"FREE_TIME" : $("#FREE_TIME").val(),
						"TOTAL_AMOUNT" : $("#TOTAL_AMOUNT").val(),
						"ROOM_NOS" : $("#ROOM_NOS").val(),
						"ROOM_NAMES" : $("#ROOM_NAMES").val(),
						"RESERVE_TIMES" : $("#RESERVE_TIMES").val(),
						"REG_UID" : $("#REG_UID").val(),
						"mode" : "reservation"
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == "") {
						alert(error_msg);
					}
					else {
						alert("예약이 완료되었습니다.\n\n예약번호 : " + data.result);
						reserve_no = data.result;
						location.href = "reservation_result.asp?reserve_no=" + reserve_no;
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}

	function fnGoBack() {
		location.href = "reservation.asp";
	}
</script>

<input type="hidden" id="YEAR" name="YEAR" value="<%=R_YEAR%>" />
<input type="hidden" id="MONTH" name="MONTH" value="<%=R_MONTH%>" />
<input type="hidden" id="DAY" name="DAY" value="<%=R_DAY%>" />
<input type="hidden" id="R_QTY" name="R_QTY" value="<%=R_QTY%>" />
<input type="hidden" id="T_QTY" name="T_QTY" value="<%=T_QTY%>" />
<input type="hidden" id="POLYCOM" name="POLYCOM" value="<%=POLYCOM%>" />
<input type="hidden" id="ETC" name="ETC" value="<%=ETC%>" />
<input type="hidden" id="FILMING_EQUIPMENT" name="FILMING_EQUIPMENT" value="<%=FILMING_EQUIPMENT%>" />
<input type="hidden" id="ORI_AMOUNT" name="ORI_AMOUNT" value="<%=ORI_AMOUNT%>" />
<input type="hidden" id="DISCOUNT" name="DISCOUNT" value="<%=DISCOUNT%>" />
<input type="hidden" id="ETC_AMOUNT" name="ETC_AMOUNT" value="0" />
<input type="hidden" id="FREE_TIME" name="FREE_TIME" value="<%=FREE_TIME%>" />
<input type="hidden" id="TOTAL_AMOUNT" name="TOTAL_AMOUNT" value="<%=TOTAL_AMOUNT%>" />
<input type="hidden" id="ROOM_NOS" name="ROOM_NOS" value="<%=ROOM_NOS%>" />
<input type="hidden" id="ROOM_NAMES" name="ROOM_NAMES" value="<%=ROOM_NAMES%>" />
<input type="hidden" id="RESERVE_TIMES" name="RESERVE_TIMES" value="<%=RESERVE_TIMES%>" />
<input type="hidden" id="USER_NAME" name="USER_NAME" value="<%=objRs("USER_NAME")%>" />
<input type="hidden" id="REG_UID" name="REG_UID" value="<%=strUserId%>" />

<div class="contBody">
	<div class="pTitle"><h2>예약정보 입력</h2></div>
	<div class="tblStyle2">
		<table>
			<caption>예약정보</caption>
			<colgroup>
				<col style="width:17%"/>
				<col style="width:35%"/>
				<col style="width:17%"/>
				<col style="width:*%"/>
			</colgroup>
			<tbody>
				<tr>
					<th scope="row">회사명</th>
					<td><%=objRs("USER_NAME")%></td>
					<th scope="row">사업자등록번호</th>
					<td><%=strBUSINESS_RE_NO%></td>
				</tr>
				<tr>
					<th scope="row">회의실</th>
					<td><%=ROOM_NAMES%></td>
					<th scope="row">이용시간</th>
					<td><%=R_YEAR%>년 <%=R_MONTH%>월 <%=R_DAY%>일 <%=firstTime%>:00 ~ <%=lastTime%>:00</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="tblStyle3">
		<table>
			<caption>예약정보 입력</caption>
			<colgroup>
				<col style="width:14%"/>
				<col style="width:*%"/>
			</colgroup>
			<tbody>
				<tr>
					<th scope="row"><label for="SUBJECT">회의명 <span class="asterisk">*<em>필수입력</em></span></label></th>
					<td>
						<input type="text" id="SUBJECT" name="SUBJECT" placeholder="30자 이내로 입력해 주세요." style="width:100%;" maxlength="30" />
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="RESERVE_NAME">예약자명 <span class="asterisk">*<em>필수입력</em></span></label></th>
					<td>
						<input type="text" id="RESERVE_NAME" name="RESERVE_NAME" placeholder="20자 이내로 입력해 주세요." style="width:580px;" maxlength="20" />
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="TELNUM">전화번호 <span class="asterisk">*<em>필수입력</em></span></label></th>
					<td>
						<input type="text" id="TELNUM" name="TELNUM" placeholder="-제외하고 숫자만 입력해 주세요." style="width:580px;" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" />
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="MOBILENUM">휴대폰번호 <span class="asterisk">*<em>필수입력</em></span></label></th>
					<td>
						<input type="text" id="MOBILENUM" name="MOBILENUM" placeholder="-제외하고 숫자만 입력해 주세요." style="width:580px;" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" />
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="P_COUNT">참석인원 <span class="asterisk">*<em>필수입력</em></span></label></th>
					<td>
						<!-- <input id="P_COUNT" name="P_COUNT" type="text" placeholder="30명 이상 숫자만 입력해 주세요." style="width:580px;" maxlength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /> -->
                        <input id="P_COUNT" name="P_COUNT" type="text" style="width:580px;" maxlength="3" value="<%=P_COUNT%>" disabled />
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="EMAIL">이메일 <span class="asterisk">*<em>필수입력</em></span></label></th>
					<td>
						<input type="text" id="EMAIL" name="EMAIL" placeholder="이메일 형식에 맞게 입력해 주세요." style="width:580px;" maxlength="100" style="ime-mode:disabled" />
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="FAXNUM">FAX</label></th>
					<td>
						<input type="text" id="FAXNUM" name="FAXNUM" placeholder="-제외하고 숫자만 입력해 주세요." style="width:580px;" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" />
					</td>
				</tr>
                <tr>
                    <th scope="row"><label for="FILMING_EQUIPMENT">촬영장비반입</label></th>
                    <td style="line-height:50px;">
                        <label><input type="radio" name="FILMING_EQUIPMENT" onClick="fnFilEquipment('Y');"> 반입</label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <label><input type="radio" name="FILMING_EQUIPMENT" onClick="fnFilEquipment('N');" checked> 미반입</label>
                    </td>
                </tr>
				<tr>
					<th scope="row"><label for="REQUIREMENT">요청사항</label></th>
					<td>
						<textarea id="REQUIREMENT" name="REQUIREMENT" placeholder="300자 이내로 입력해 주세요." style="height:190px;" maxlength="300"></textarea>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="notiBoxTerms">
		<h3 class="tit">이용약관 동의</h3>
		<div class="gBox">
			<div class="bx">
				<ul>
					<li><strong>1. 예약 방법 규정</strong>
						<ul>
							<li>1.1	 컨퍼런스룸 대관은 IFC Seoul 홈페이지내 관리자 페이지에서만 가능합니다.(유선, 방문예약 불가합니다.)</li>
							<li>1.2  이용 가능 시간은 주중 오전 9시부터 오후 6시까지, 빌딩운영 시간과 동일합니다.
							<li>1.3  주말 및 공휴일은 이용이 불가합니다.</li>
							<li>1.4  예약은 최소 2시간예약을 기본으로, 이후 1시간 단위로 추가할 수 있습니다.</li>
							<li>1.5  IFC홀 전체 사용시 최소 예약 시간은 4시간 부터입니다.</li>
							<li>1.6  이용 예약 가능기간은 예약일로부터 120일 이내 예약이 가능합니다.</li>
							<li>1.7  24시간이내의 예약은 불가합니다. (당일예약은 불가합니다.)</li>
							<li>1.8  회의실 독점을 방지하기위해 임차인별로 동일조건으로 4일이상 연속이용이 불가합니다. </li>
							<li>1.9  입주사는 매월 64시간이상의 사용은 불가합니다.</li>
							<li>1.10 기존 회의 예약 회의 전/후 1시간은 예약이 불가합니다. (가구배치/원복시간입니다.)</li>
							<li>1.11 이용 회의 주최자께서는 회의룸 가구배치와 원복을 직접 진행하셔야합니다.</li>
						</ul>
					</li><br/>
					<li><strong>2. 결제 규정</strong>
						<ul>
							<li>2.1	결제는 카드결제, 사전입금이 가능합니다.(세금계산서 발행 가능합니다.) </li>
							<li>2.2	예약 후 1주일(주말 및 공휴일 포함 7일이내)이내 대관료 결제를 완료해야 예약이 확정됩니다. 일주일 이내 결제되지않은 예약은 자동 취소됩니다.</li>
							<li>2.3	일주일 이내 예약의 경우에는 24시간 이내 결제를 완료해야합니다. </li>
							<li>2.4	카드결제 후 취소 및 재결제 요청은 불가합니다.</li>
						</ul>
					</li><br/>
					<li><strong>3. 취소및 환불 규정</strong>
						<ul>
							<li>3.1	대관료 결제는 사전 결제를 원칙으로 합니다. </li>
							<li>3.2	예약 48시간 이내는 예약 취소가 불가합니다. </li>
							<li>3.3	24시간 이전의 예약은 예약후 취소가 불가합니다. 신중한 예약을 부탁드립니다.</li>
							<li>3.4	예약취소로 인한 위약금은 무상시간을 우선 차감합니다</li>
							<li>3.5	이용시간 단축 조기퇴실이 발생시에도 예약시간 기준으로 요금이 부과됩니다. .</li>
							<li>3.6	취소위약금은 세금계산서 발행이 되지않습니다.
								<ul>
									<li>위약금 규졍 (계산방법)</li>
									<li>-	30일전까지 : 100% 환급</li>
									<li>-	29일 ~ 15일 : 총 결제금액의 50%</li>
									<li>-	14일 ~ 이용당일  : 총결제금액의 100% </li>
								</ul>
							</li>
							<li>3.7	위약금은 결제금에 반환함을 원칙으로 합니다. 현금 입금의 경우 최대 30일이 소요될 수 있습니다.</li>
							<li>3.8	카드결제 승인 취소 및 반환의 경우, 결재시 카드영수증을 제출하여 주셔야 하며, 카드 취소는 카드사의 사정에 따라 10일이 소요될 수 있습니다.</li>
							<li>3.9	임대인은 불가항력 또는 국가시책 변경에 의해 계약이 해제되는 경우에는 임차인에게 임대료 반환 이외의 손해배상금은 지급되지않습니다.</li>
						</ul>
					</li><br/>
					<li><strong>4. 운영규정 </strong>
						<ul>
							<li>4.1	운영시간은 주중 오전 9시부터 오후 6시까지, 빌딩운영 시간과 동일합니다.</li>
							<li>4.2	예약시간 초과시 초과요금이 발생합니다. 초과요금은 1시간 단위입니다. </li>
							<li>4.3	기자재(빔 프로젝터, 추가 마이크, 노트북 등) 사용은 대관료와 별도로 개별 요금이 부과 됩니다. </li>
							<li>4.4	폴리콤(3자통화) 이용시 통화료가 별도로 부과되며, 이용후 안내되어지는 요금에 대해서 추가로 결제하셔야합니다.</li>
							<li>4.5	컨퍼런스룸 내에는 커피와 쿠키 외 식사류의 반입과 섭취가 불가합니다.</li>
							<li>4.6	캐이터링 등 기타 외부 서비스가 진행되는경우 반입되는 반드시 회의전 안내데스크로 사전 등록하여 주시기 바랍니다. </li>
							<li>4.7	사무용품은 지급되지않으니 미리 준비하여 주시기 바랍니다.</li>
							<li>4.8	회의실 내 비품, 집기, 기자재등 파손시 변상 또는 원상복구를 해야합니다.</li>
							<li>4.9	빔 프로젝터, 마이크, 전용회선, 정밀기기는 회의 사용 중 오류가 발생할 수 있사오니, 회의주최자께서는 회의룸 이용 방법을 사전에 습득하시기 바랍니다.</li>
							<li>4.10	시설 이용객에게는 별도의 주차할인이 제공되지 않습니다.</li>
						</ul>
					</li><br/>
					<li><strong>5. 이용불가조건 </strong>
						<ul>
							<li>5.1	정치성의 행사 (당원교육, 선거관련집회, 단식투쟁, 혈서작성 성격의 집회, 기타 등)</li>
							<li>5.2	회의실 파손, 타 이용자의 불편, 주변 분위기 문란의 우려가 있는 집회</li>
							<li>5.3	고성 또는 기물 파손 등의 우려가 있는 집회, 악기를 동원한 집회</li>
							<li>5.4	알코올이 함유된 음료(샴페인, 맥주 포함)가 동반되는 회의 등</li>
							<li>5.5	외부인을 상대로 미풍양속을 해치는 영업을 겸한 집회</li>
							<li>5.6	회의실 사용(임대)계약을 만성적으로 위반하는 입주사</li>
							<li>5.7	기타 회의실을 사용하기에 부적합하다고 판단되는 행사의 경우 이용이 불가할 수 있습니다.</li>
						</ul>
					</li><br/>
					<li><strong>6. 불가항력 </strong>
						<ul>
							<li>6.1. 천재지변, 재앙, 국가시책변경 및 기타 불가항력적 원인에 의해 사용자의 재산상 손해가 발생하였을 때 임대인은 그 손해에 대하여 책임을 지지 아니한다</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div class="designChk">
			<input type="checkbox" id="chk_agree" />
			<label for="chk_agree">약관에 동의합니다.</label>
		</div>
	</div>

	<div class="btnWrap">
		<button class="nbtnW" id="btn_before" onClick="fnGoBack();">이전</button>
		<button class="nbtnG" id="btn_reservation" onClick="fnReservation();">예약 하기</button>
	</div>
</div>

<div id="layerPopChk" class="lyPopWrap bw">
	<div class="lyPopBody">
		<strong class="pTit">위약금 규정 동의</strong>
		<div class="lyPopContWrap scrollH">

			<div style="font-size:14px; background:#f7f7f6; margin:20px 0 30px 0; padding:10px; ">
				<ul>
					<li>-	30일전까지 : 100% 환급</li>
					<li>-	29일 ~ 15일 : 총 결제금액의 50%</li>
					<li>-	14일 ~ 이용당일  : 총결제금액의 100% </li>
				</ul>
				1.1  위약금은 결제금에 반환함을 원칙으로 합니다. 현금 입금의 경우 최대 30일이 소요될 수 있습니다.<br/>
				1.2  카드결제 승인 취소 및 반환의 경우, 결재시 카드영수증을 제출하여 주셔야 하며, 카드 취소는 카드사의 사정에 따라 10일이 소요될 수 있습니다.<br/><br/>
			</div>
		</div>

		<div class="designChk">
			<input type="checkbox" id="chk_agree_3" />
			<label for="chk_agree_3">상기 위약금 규정에 동의합니다.</label>
		</div>

		<div class="btnWrap">
			<button class="nbtnG04">예약완료</button>
		</div>
	</div>
	<a href="#none" class="popCls jsPopClose">Close</a>
</div>

<!--#include file = "index_footer.asp" -->
