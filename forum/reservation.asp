<!--#include file = "default_control.asp" -->
<%
re_url = "reservation"
%>
<!--#include file = "session_check.asp" -->
<!--#include file = "index_header.asp" -->
<%
strNOW = Left(NOW, 10)

If Request("TEST_DATE")&"" <> "" Then
	strNOW = Request("TEST_DATE")
End If

selectQuery = "select * from TBL_USER_INFO where UID = '" & strUserId & "' ORDER BY USER_NAME ASC"

set objRs = SendQuery(objConn,selectQuery)
%>
<script type="text/javascript">
	$(document).ready(function() {
		fnMonthChg("<%=Year(strNow)%>", "<%=Month(strNow)%>");
        //fnMonthChg("<%=Year(strNow)%>", "02");
		fnPolyClick("N",1);
		fnEtcClick("N",1);
		fnIFCHallChg(1);

	});

	$(window).load(function() {

		// 팝업위치 설정
		if ($(".video_wrap img").length > 0) {
			var player = $(".video_wrap img");
			var popup = $("#noticePop_211110");

			var temp = new Image();
			temp.src = player.attr('src');
			var w = temp.width;
			var h = temp.height;
			var left = 0;
			var top = 0;

			if ($(window).width() > w) {
				left = ($(window).width() - w) / 2;
				top = 170;
			}
			else {
				left =0;
				top =0;
				player.width($(window).width());
			}

			popup.css("top", top + "px").css("left", left + "px");
		}

		// 팝업창 닫기 
		$(".js_PopClose").click(function(event) {
			event.preventDefault();
			event.stopPropagation();
			$(this).parent().parent().hide();
			if ($(this).parent().find(".vertical_m").prop("checked")) {
				var id = $(this).closest(".layer_popup").attr("id");
				setCookie(id, "true", 1);
			}
	  });

		//팝업제거
		// alert("거리두기 시행기간인 2주동안 (10/18~10/31) The Forum at IFC 예약이 중지됩니다. 정부시책에 따라 예약상황이 변경될 수 있사오니 담당자에게 문의바랍니다.\n\n02) 6137-2100");

		fnTimeCheck();
	});

	function setCookie(cName, cValue, cDay){
		var expire = new Date();
		expire.setDate(expire.getDate() + cDay);
		cookies = cName + '=' + escape(cValue) + '; path=/ ';
		if(typeof cDay != 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
		document.cookie = cookies;
	}

	// 달 선택
	function fnMonthChg(objYear, objMonth) {

		if (objYear == '2021' && objMonth == '01') {
				alert("신종코로나바이러스 감염증으로 인해 1월은 The Forum at IFC의 사용이 제한됩니다. 예약 또는 사용안내는 관리자에게 문의 부탁드립니다.  02)6137-2100\n");
				return false;
		}

		$("#index_loading_overlay").show();

		$("li[id*=li_cal_month]").removeClass("on");
		$("#li_cal_month" + objMonth).addClass("on");

		$("#em_cal_YM").html(objYear + "." + objMonth);

		$("#YEAR").val(objYear);
		$("#MONTH").val(objMonth);

		$.ajax ({
			type : "POST",
			url : "./ajax/reservation_proc.asp",
			dataType : "json",
			data : ({"YEAR" : objYear, "MONTH" : objMonth, "NOW" : "<%=strNOW%>", "UID" : '<%=strUserId%>', "LIMIT_TIME" : '<%=objRs("LIMIT_TIME")%>', "mode" : "get_day"}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == "") {
						alert(error_msg);
					}
					else {
						var dateData = data.result.split('|');

						// 월무상시간 표시
						$("#em_cal_free_time").html(dateData[1]);
						selFreeTimeHtml = '<select id="sel_free_time" name="sel_free_time" onChange="fnFreeTimeChg(this.value);">';
						for (i=0; i<=dateData[1]; i++) {
							selFreeTimeHtml += '<option value="' + i + '"> ' + i + '시간 </option>'
						}
						selFreeTimeHtml += '</select><span class="gearNoti"> 현재 ' + dateData[1] + '시간 까지 이용 가능</span>'
						$("#td_free_time").html(selFreeTimeHtml);

						// 예약가능시간 초과시 처리
						if (Number(dateData[0]) < 1) {
							alert("선택한 달의 예약가능시간을 모두 사용하여\n회의실 예약이 불가능합니다.");
							$("#em_cal_limit_time").html("0");
							$("#ul_cal_day").html("");
						}
						else {
							$("#em_cal_limit_time").html(dateData[0]);
							$("#ul_cal_day").html(dateData[2]);
						}
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});

		fnMonSelReset();
	}

	// 일 선택
	function fnDayClick(beforeTodayYN ,day, dw, offYn, todayYn, tomorrowYn, uday90, obj) {
		if (beforeTodayYN == "Y") {
			alert("오늘 이전의 날짜는 선택할 수 없습니다.");
			return true;
		}
		if (todayYn == "Y") {
			alert("당일예약은 불가능합니다.");
			return true;
		}
        if (tomorrowYn == "Y") {
            alert("익일예약은 불가능합니다.");
            return true;
        }
		if (uday90 != "Y") {
			alert("회의실 예약은 120일 이전의 날짜만 예약이 가능합니다.");
			return true;
		}
		if (dw == "7") {
			alert("토요일예약은 불가능합니다.");
			return true;
		}
		if (dw == "1") {
			alert("일요일예약은 불가능합니다.");
			return true;
		}
		if (offYn == "Y") {
			alert("공휴일예약은 불가능합니다.");
			return true;
		}
        if (offYn == "C") {
            alert("사회적 거리두기로 인하여 예약이 제한됩니다. ");
            return true;
        }
		$("#ul_cal_day").children().each(function(idx,item) {
			$(item).removeClass("on");
		});
		$(obj).addClass("on");

		$("#DAY").val(day);
		$("#td_info_reserve_dt").html($("#YEAR").val() + "년 " + $("#MONTH").val() + "월 " + $("#DAY").val() + "일");
		$("#ul_pCount").addClass("active");

		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/reservation_proc.asp",
			dataType : "json",
			data : ({"YEAR" : $("#YEAR").val(), "MONTH" : $("#MONTH").val(), "DAY" : day, "mode" : "get_detail"}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == "") {
						alert(error_msg);
					}
					else {
						var dateData = data.result.split('|');
						$("#tr_time_B0001").html("<td>브룩필드홀</td>" + dateData[5]);
						$("#tr_time_R0001").html("<td>IFC Hall 301</td>" + dateData[6]);
						$("#tr_time_R0002").html("<td>IFC Hall 302</td>" + dateData[7]);
						$("#tr_time_R0003").html("<td>IFC Hall 303</td>" + dateData[8]);
						$("#tr_time_R0004").html("<td>IFC Hall 304</td>" + dateData[9]);

						if (fnRoomSelYN("B0001") == "N") {
							$("#div_room_B0001").addClass("disabled_v01");
							$("#chk_room_B0001").attr("disabled",true);
						}
						if (fnRoomSelYN("R0001") == "N") {
							$("#div_room_R0001").addClass("disabled_v01");
							$("#chk_room_R0001").attr("disabled",true);
						}
						if (fnRoomSelYN("R0002") == "N") {
							$("#div_room_R0002").addClass("disabled_v01");
							$("#chk_room_R0002").attr("disabled",true);
						}
						if (fnRoomSelYN("R0003") == "N") {
							$("#div_room_R0003").addClass("disabled_v01");
							$("#chk_room_R0003").attr("disabled",true);
						}
						if (fnRoomSelYN("R0004") == "N") {
							$("#div_room_R0004").addClass("disabled_v01");
							$("#chk_room_R0004").attr("disabled",true);
						}
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});

		fnDaySelReset();
	}

	// 인원선택
	function fnPCountChk(cnt) {

      // if (cnt == 75 || cnt == 100) {
      //     alert("50명 이상의 예약은 관리자에게 문의해주시면 감사하겠습니다.\n(문의 : 02-6137-2100) ");
      //     return true;
      // }

		if ($("#ul_pCount").hasClass('active')) {
			$("li[id*=li_pCount]").removeClass("on");
			$("#li_pCount" + cnt).addClass("on");
			$("#td_info_p_count").html(cnt + " 명");
			$("#P_COUNT").val(cnt);

			$("#div_room").addClass("active");
			$("div[id*=div_room_]").removeClass("disabled_v02");

			// 예약불가 아니면 활성화
			if ($("#div_room_B0001").hasClass('disabled_v01') == false) {
				$("#chk_room_B0001").attr("disabled",false);
			}
			if ($("#div_room_R0001").hasClass('disabled_v01') == false) {
				$("#chk_room_R0001").attr("disabled",false);
			}
			if ($("#div_room_R0002").hasClass('disabled_v01') == false) {
				$("#chk_room_R0002").attr("disabled",false);
			}
			if ($("#div_room_R0003").hasClass('disabled_v01') == false) {
				$("#chk_room_R0003").attr("disabled",false);
			}
			if ($("#div_room_R0004").hasClass('disabled_v01') == false) {
				$("#chk_room_R0004").attr("disabled",false);
			}

			// 인원수별 회의실 disabled_v02
			if (cnt == 25) {
				if ($("#div_room_R0002").hasClass('disabled_v01') == false) {
					$("#div_room_R0002").addClass("disabled_v02");
					$("#chk_room_R0002").attr("disabled",true);
				}
				if ($("#div_room_R0004").hasClass('disabled_v01') == false) {
					$("#div_room_R0004").addClass("disabled_v02");
					$("#chk_room_R0004").attr("disabled",true);
				}
			}
			else if (cnt == 50) {
				if ($("#div_room_B0001").hasClass('disabled_v01') == false) {
					$("#div_room_B0001").addClass("disabled_v02");
					$("#chk_room_B0001").attr("disabled",true);
				}

				if ($("#div_room_R0001").hasClass('disabled_v01') && $("#div_room_R0002").hasClass('disabled_v01')) {
					if ($("#div_room_R0003").hasClass('disabled_v01') == false) {
						$("#div_room_R0003").addClass("disabled_v02");
						$("#chk_room_R0003").attr("disabled",true);
					}
					if ($("#div_room_R0004").hasClass('disabled_v01') == false) {
						$("#div_room_R0004").addClass("disabled_v02");
						$("#chk_room_R0004").attr("disabled",true);
					}
				}
				if ($("#div_room_R0003").hasClass('disabled_v01')) {
					if ($("#div_room_R0001").hasClass('disabled_v01') == false) {
						$("#div_room_R0001").addClass("disabled_v02");
						$("#chk_room_R0001").attr("disabled",true);
					}
					if ($("#div_room_R0002").hasClass('disabled_v01') == false) {
						$("#div_room_R0002").addClass("disabled_v02");
						$("#chk_room_R0002").attr("disabled",true);
					}
				}

                // 50명 선택시 301+302 예약제한 (20200814 요청)
                $("#div_room_R0001").addClass("disabled_v02");
                $("#div_room_R0002").addClass("disabled_v02");
                $("#chk_room_R0001").attr("disabled",true);
                $("#chk_room_R0002").attr("disabled",true);
			}
			else if (cnt == 75 || cnt == 100) {
				/*if ($("#div_room_B0001").hasClass('disabled_v01') == false) {
					$("#div_room_B0001").addClass("disabled_v02");
					$("#chk_room_B0001").attr("disabled",true);
				}
				if ($("#div_room_R0001").hasClass('disabled_v01') || $("#div_room_R0003").hasClass('disabled_v01')) {
					if ($("#div_room_R0001").hasClass('disabled_v01') == false) {
						$("#div_room_R0001").addClass("disabled_v02");
						$("#chk_room_R0001").attr("disabled",true);
					}
					if ($("#div_room_R0002").hasClass('disabled_v01') == false) {
						$("#div_room_R0002").addClass("disabled_v02");
						$("#chk_room_R0002").attr("disabled",true);
					}
					if ($("#div_room_R0003").hasClass('disabled_v01') == false) {
						$("#div_room_R0003").addClass("disabled_v02");
						$("#chk_room_R0003").attr("disabled",true);
					}
					if ($("#div_room_R0004").hasClass('disabled_v01') == false) {
						$("#div_room_R0004").addClass("disabled_v02");
						$("#chk_room_R0004").attr("disabled",true);
					}
				}*/
			}

			fnPCountSelReset();
		}
	}

	// 룸 선택
	function fnRoomChk(objId) {
		if ($("#div_room").hasClass("active") && $("#div_room_" + objId).hasClass("disabled_v01") == false && $("#div_room_" + objId).hasClass("disabled_v02") == false) {
			var pCount = 0;
			$("li[id*=li_pCount]").each(function(idx,item) {
				if ($(item).hasClass("on")) {
					pCount = $(item).val();
				}
			});
			if (pCount == 25) {
				fnRoomSelect(objId);
				var roomCnt = 0;
				$("input:checkbox[id*=chk_room_]:checked").each(function() {
					roomCnt++;
				});
				if (roomCnt > 1) {
					alert("25명일 경우 하나의 홀만 예약가능합니다.");
					fnPCountChk("25");
					return true;
				}
			}
			else if (pCount == 50) {
				if (objId == "R0001" || objId == "R0002") {
					if ($("#div_room_R0001").hasClass("disabled_v01") == false && $("#div_room_R0001").hasClass("disabled_v02") == false && $("#div_room_R0002").hasClass("disabled_v01") == false && $("#div_room_R0002").hasClass("disabled_v02") == false && $("#div_room_R0003").hasClass("check") == false) {
						fnRoomSelect("R0001");
						fnRoomSelect("R0002");
					}
				}
				else if (objId == "R0003" || objId == "R0004") {
					if ($("#div_room_R0003").hasClass("disabled_v01") == false && $("#div_room_R0003").hasClass("disabled_v02") == false && $("#div_room_R0004").hasClass("disabled_v01") == false && $("#div_room_R0004").hasClass("disabled_v02") == false && $("#div_room_R0001").hasClass("check") == false) {
						fnRoomSelect("R0003");
						fnRoomSelect("R0004");
					}
				}
			}
			else if (pCount == 75) {
				if (objId == "R0001" || objId == "R0002") {
					if ($("#div_room_R0001").hasClass("disabled_v01") == false && $("#div_room_R0001").hasClass("disabled_v02") == false && $("#div_room_R0002").hasClass("disabled_v01") == false && $("#div_room_R0002").hasClass("disabled_v02") == false && $("#div_room_R0003").hasClass("disabled_v01") == false && $("#div_room_R0003").hasClass("disabled_v02") == false && $("#div_room_R0004").hasClass("check") == false) {
						fnRoomSelect("R0001");
						fnRoomSelect("R0002");
						fnRoomSelect("R0003");
					}
				}
				else if (objId == "R0003" || objId == "R0004") {
					if ($("#div_room_R0002").hasClass("disabled_v01") == false && $("#div_room_R0002").hasClass("disabled_v02") == false && $("#div_room_R0003").hasClass("disabled_v01") == false && $("#div_room_R0003").hasClass("disabled_v02") == false && $("#div_room_R0004").hasClass("disabled_v01") == false && $("#div_room_R0004").hasClass("disabled_v02") == false && $("#div_room_R0001").hasClass("check") == false) {
						fnRoomSelect("R0002");
						fnRoomSelect("R0003");
						fnRoomSelect("R0004");
					}
				}
			}
			else if (pCount == 100) {
				if ($("#div_room_R0001").hasClass("disabled_v01") == false && $("#div_room_R0001").hasClass("disabled_v02") == false && $("#div_room_R0002").hasClass("disabled_v01") == false && $("#div_room_R0002").hasClass("disabled_v02") == false && $("#div_room_R0003").hasClass("disabled_v01") == false && $("#div_room_R0003").hasClass("disabled_v02") == false && $("#div_room_R0004").hasClass("disabled_v01") == false && $("#div_room_R0004").hasClass("disabled_v02") == false) {
					fnRoomSelect("R0001");
					fnRoomSelect("R0002");
					fnRoomSelect("R0003");
					fnRoomSelect("R0004");
				}
			}

			/* SSSSS 룸명 구하기 SSSSS */
			var RoomName = ""
			$("input:checkbox[id*=chk_room_]").each(function(idx,item) {
				if ($(item).is(":checked")) {
					if (idx == 0)
						RoomName = RoomName + ",브룩필드홀";
					if (idx == 1)
						RoomName = RoomName + ",IFC Hall 301";
					if (idx == 2)
						RoomName = RoomName + ",IFC Hall 302";
					if (idx == 3)
						RoomName = RoomName + ",IFC Hall 303";
					if (idx == 4)
						RoomName = RoomName + ",IFC Hall 304";
				}
			});
			if (RoomName.substring(0,1) == ",") {
				RoomName = RoomName.substring(1, RoomName.length);
			}
			$("#td_info_room").html(RoomName);
			/* EEEEE 룸명 구하기 EEEEE */

			fnInfoReset();
		}
	}

	// 선택된 룸의 시간 인포 처리
	function fnRoomSelect(objId) {
		// 체크박스 체크
		if ($("#chk_room_" + objId).is(":checked")) {
			$("#chk_room_" + objId).prop("checked",false);
		}
		else {
			$("#chk_room_" + objId).prop("checked",true);
		}

		if ($("#chk_room_" + objId).is(":checked")) {
			$("#div_room_" + objId).addClass("check");
			$("#tr_time_" + objId).removeClass("disabled");
			$("#div_info_" + objId).addClass("check");
		}
		else {
			$("#div_room_" + objId).removeClass("check");
			$("#tr_time_" + objId).addClass("disabled");
			$("#div_info_" + objId).removeClass("check");
		}
		$("td[id*=td_time_]").each(function(idx,item) {
			if ($(item).hasClass("selected")) {
				$(item).removeClass("selected");
			}
		});
	}

	// 룸 마우스 오버
	function fnRoomMOver(objId) {
		var pCount = 0;
		if ($("li[id*=li_pCount]").hasClass("on")) {
			$("li[id*=li_pCount]").each(function(idx,item) {
				if ($(item).hasClass("on")) {
					pCount = $(item).val();
				}
			});
			if (pCount == 25) {
				$("#div_room_" + objId).addClass("overview");
			}
			else if (pCount == 50) {
				// if (objId == "R0001" || objId == "R0002") {
				// 	$("#div_room_R0001").addClass("overview");
				// 	$("#div_room_R0002").addClass("overview");
				// }
				// else
                if (objId == "R0003" || objId == "R0004") {
					$("#div_room_R0003").addClass("overview");
					$("#div_room_R0004").addClass("overview");
				}
			}
			else if (pCount == 75) {
				if (objId == "R0001" || objId == "R0002") {
					$("#div_room_R0001").addClass("overview");
					$("#div_room_R0002").addClass("overview");
					$("#div_room_R0003").addClass("overview");
				}
				else if (objId == "R0003" || objId == "R0004") {
					$("#div_room_R0002").addClass("overview");
					$("#div_room_R0003").addClass("overview");
					$("#div_room_R0004").addClass("overview");
				}
			}
			else if (pCount == 100) {
				$("#div_room_R0001").addClass("overview");
				$("#div_room_R0002").addClass("overview");
				$("#div_room_R0003").addClass("overview");
				$("#div_room_R0004").addClass("overview");
			}
		}
	}

	// 룸 마우스 아웃
	function fnRoomMOut() {
		$("div[id*=div_room_]").removeClass("overview");
	}

	// 시간 선택
	function fnTimeClick(room, time, obj) {

        //24시간 이전타임 선택 불가하도록 처리.
        var date = new Date();
        // var date = new Date('2020-10-07 15:20');             //test
        var selTime = new Date($("#YEAR").val() + '/' +  $("#MONTH").val() + '/' + $("#DAY").val() + ' ' + time + ':00');

        if (selTime < date.setHours(date.getHours()+24)) {
            alert("24시간전에는 예약이 불가합니다. ");
            return false;
        }

		if ($("#tr_time_" + room).hasClass("disabled") == false) {
			if ($(obj).hasClass("disable_v01")) {
				alert("이미 예약되었습니다.");
				return false;
			}
			else {
				var selB0001YN = "N";
				var selR0001YN = "N";
				var selR0002YN = "N";
				var selR0003YN = "N";
				var selR0004YN = "N";
				if ($("#tr_time_B0001").hasClass("disabled") == false) { // 룸 클릭되어 시간선택이 가능하고
					if ($("#td_time_B0001_" + time).hasClass("disable_v01")) {
						alert("브룩필드홀의 " + time + "시가 예약되어있습니다.");
						return true;
					}
					else if ($("#td_time_B0001_" + time).hasClass("disable_v02")) {
						alert("브룩필드홀의 " + time + "시는 클리닝 타임입니다.");
						return true;
					}
					else {
						selB0001YN = "Y";
					}
				}
				if ($("#tr_time_R0001").hasClass("disabled") == false) {
					if ($("#td_time_R0001_" + time).hasClass("disable_v01")) {
						alert("IFC Hall 301의 " + time + "시가 예약되어있습니다.");
						return true;
					}
					else if ($("#td_time_R0001_" + time).hasClass("disable_v02")) {
						alert("IFC Hall 301의 " + time + "시는 클리닝 타임입니다.");
						return true;
					}
					else {
						selR0001YN = "Y";
					}
				}
				if ($("#tr_time_R0002").hasClass("disabled") == false) {
					if ($("#td_time_R0002_" + time).hasClass("disable_v01")) {
						alert("IFC Hall 302의 " + time + "시가 예약되어있습니다.");
						return true;
					}
					else if ($("#td_time_R0002_" + time).hasClass("disable_v02")) {
						alert("IFC Hall 302의 " + time + "시는 클리닝 타임입니다.");
						return true;
					}
					else {
						selR0002YN = "Y";
					}
				}
				if ($("#tr_time_R0003").hasClass("disabled") == false) {
					if ($("#td_time_R0003_" + time).hasClass("disable_v01")) {
						alert("IFC Hall 303의 " + time + "시가 예약되어있습니다.");
						return true;
					}
					else if ($("#td_time_R0003_" + time).hasClass("disable_v02")) {
						alert("IFC Hall 303의 " + time + "시는 클리닝 타임입니다.");
						return true;
					}
					else {
						selR0003YN = "Y";
					}
				}
				if ($("#tr_time_R0004").hasClass("disabled") == false) {
					if ($("#td_time_R0004_" + time).hasClass("disable_v01")) {
						alert("IFC Hall 304의 " + time + "시가 예약되어있습니다.");
						return true;
					}
					else if ($("#td_time_R0004_" + time).hasClass("disable_v02")) {
						alert("IFC Hall 304의 " + time + "시는 클리닝 타임입니다.");
						return true;
					}
					else {
						selR0004YN = "Y";
					}
				}

				// 인원별 시간제한
				var pCount = 0;
				var timeChk = true;
				$("li[id*=li_pCount]").each(function(idx,item) {
					if ($(item).hasClass("on")) {
						pCount = $(item).val();
					}
				});

				$("input:checkbox[id*=chk_room_R000]:checked").each(function(idx,item) {
					if (pCount == 25) {
						if ($("#td_time_R0002_" + time).hasClass("disable_v01")) {
							alert("시간대를 변경해주세요. 동일 시간대에 예약된 회의실은 예약이 불가능합니다.");
							timeChk = false;
						}
					}
					else if (pCount == 50) {
						if ($(item).val() == "R0002") {
							if ($("#td_time_R0003_" + time).hasClass("disable_v01")) {
								alert("시간대를 변경해주세요. 동일 시간대에 예약된 회의실은 예약이 불가능합니다.");
								timeChk = false;
							}
						}
						if ($(item).val() == "R0003") {
							if ($("#td_time_R0002_" + time).hasClass("disable_v01")) {
								alert("시간대를 변경해주세요. 동일 시간대에 예약된 회의실은 예약이 불가능합니다.");
								timeChk = false;
							}
						}
					}
					else if (pCount == 75) {
						if ($(item).val() == "R0002") {
							if ($("#td_time_R0001_" + time).hasClass("disable_v01")) {
								alert("시간대를 변경해주세요. 동일 시간대에 예약된 회의실은 예약이 불가능합니다.");
								timeChk = false;
							}
						}
					}
				});

				if (timeChk) {
					var firstSelected = "Y";
					var timeChkCnt = 0;
					$("td[id*=td_time_]").each(function(idx,item) { // 최초클릭 판단
						if ($(item).hasClass("selected")) {
							firstSelected = "N";
						}
					});

					if (selB0001YN == "Y")
						$("#td_time_B0001_" + time).toggleClass("selected");
					if (selR0001YN == "Y")
						$("#td_time_R0001_" + time).toggleClass("selected");
					if (selR0002YN == "Y")
						$("#td_time_R0002_" + time).toggleClass("selected");
					if (selR0003YN == "Y")
						$("#td_time_R0003_" + time).toggleClass("selected");
					if (selR0004YN == "Y")
						$("#td_time_R0004_" + time).toggleClass("selected");

					// 연속된 시간 체크
					$("td[id*=td_time_]").each(function(idx,item) {
						if ($(item).hasClass("selected")) {
							timeChkCnt = Number(timeChkCnt) +1;
						}
					});
					if (timeChkCnt < 1)
						firstSelected = "Y";
					var preTime = Number(time) -1;
					var nextTime = Number(time) +1;
					if (firstSelected == "N") {
						if ($("#td_time_B0001_" + preTime).hasClass("selected") == false && $("#td_time_B0001_" + nextTime).hasClass("selected") == false && $("#td_time_R0001_" + preTime).hasClass("selected") == false && $("#td_time_R0001_" + nextTime).hasClass("selected") == false && $("#td_time_R0002_" + preTime).hasClass("selected") == false && $("#td_time_R0002_" + nextTime).hasClass("selected") == false && $("#td_time_R0003_" + preTime).hasClass("selected") == false && $("#td_time_R0003_" + nextTime).hasClass("selected") == false && $("#td_time_R0004_" + preTime).hasClass("selected") == false && $("#td_time_R0004_" + nextTime).hasClass("selected") == false) { // selected 시 전타임과 다음타임 체크하여 연속여부 판단.
							if (selB0001YN == "Y")
								$("#td_time_B0001_" + time).toggleClass("selected");
							if (selR0001YN == "Y")
								$("#td_time_R0001_" + time).toggleClass("selected");
							if (selR0002YN == "Y")
								$("#td_time_R0002_" + time).toggleClass("selected");
							if (selR0003YN == "Y")
								$("#td_time_R0003_" + time).toggleClass("selected");
							if (selR0004YN == "Y")
								$("#td_time_R0004_" + time).toggleClass("selected");
							alert("연속된 시간을 선택해 주시기 바랍니다.");
							return true;
						}
						else if ((selB0001YN == "Y" && $("#td_time_B0001_" + time).hasClass("selected") == false) || (selR0001YN == "Y" && $("#td_time_R0001_" + time).hasClass("selected") == false) || (selR0002YN == "Y" && $("#td_time_R0002_" + time).hasClass("selected") == false) || (selR0003YN == "Y" && $("#td_time_R0003_" + time).hasClass("selected") == false) || (selR0004YN == "Y" && $("#td_time_R0004_" + time).hasClass("selected") == false)) { // selected 해제 시
							if (($("#td_time_B0001_" + preTime).hasClass("selected") && $("#td_time_B0001_" + nextTime).hasClass("selected")) || ($("#td_time_R0001_" + preTime).hasClass("selected") && $("#td_time_R0001_" + nextTime).hasClass("selected")) || ($("#td_time_R0002_" + preTime).hasClass("selected") && $("#td_time_R0002_" + nextTime).hasClass("selected")) || ($("#td_time_R0003_" + preTime).hasClass("selected") && $("#td_time_R0003_" + nextTime).hasClass("selected")) || ($("#td_time_R0004_" + preTime).hasClass("selected") && $("#td_time_R0004_" + nextTime).hasClass("selected"))) { // 전타임과 다음타임 체크하여 연속여부 판단.
								if (selB0001YN == "Y")
									$("#td_time_B0001_" + time).toggleClass("selected");
								if (selR0001YN == "Y")
									$("#td_time_R0001_" + time).toggleClass("selected");
								if (selR0002YN == "Y")
									$("#td_time_R0002_" + time).toggleClass("selected");
								if (selR0003YN == "Y")
									$("#td_time_R0003_" + time).toggleClass("selected");
								if (selR0004YN == "Y")
									$("#td_time_R0004_" + time).toggleClass("selected");
								alert("연속된 시간을 선택해 주시기 바랍니다.");
								return true;
							}
						}
					}
				}
			}
		}

        /* SSSSS 시간 구하기 SSSSS */
        var roomId = "";
        var roomCnt = 0;
        $("input:checkbox[id*=chk_room_]:checked").each(function(idx,item) {
            roomId = $(item).val();
            roomCnt = roomCnt +1;
        });
        var arrTime = new Array();
        var startTime = "";
        var endTime = "";
        $("td[id*=td_time_" + roomId + "]").each(function(idx,item) {
            if ($(item).hasClass("selected")) {
                arrTime.push(idx+9);
            }
        });
        if (arrTime.length > 0) {
            startTime = arrTime[0];
            endTime = arrTime[arrTime.length-1];
        }

        // 예약정보에 예약시간 표시
        if (startTime != "" && endTime != "") {
            endTime = Number(endTime) +1;
            $("#td_info_time").html(startTime + ":00 ~ " + endTime + ":00");
        }
        else {
        	$("#td_info_time").html("");
        }
        /* EEEEE 시간 구하기 EEEEE */

        /* SSSSS 금액 구하기 SSSSS */
        var timeCnt = 0;
        $("td[id*=td_time_" + roomId + "]").each(function(idx,item) {
        	if ($(item).hasClass("selected")) {
        		timeCnt = timeCnt +1;
        	}
        });

        // 총 이용시간
        var total_unit = Number(roomCnt) * Number(timeCnt);
        var total_unit_html = "총 이용시간 " + total_unit + "시간, ";

        var iAmountPerHour = new Array();               // 시간당 사용금액 정의
        iAmountPerHour['B'] = 240000;                   // 브룩필드홀
        iAmountPerHour['R'] = 120000;                   // IFC홀

        var arrDiscountRate = Array();                  // 사용시간별 할인률 정의
        arrDiscountRate = [
            [0,  0, 20, 20, 30, 30, 35, 40, 40],        //브룩필드홀
            [0,  0, 20, 20, 25, 25, 30, 30, 35],        //IFC홀 1Unit
            [0,  5, 25, 25, 30, 30, 35, 35, 40],        //IFC홀 2Unit
            [0, 10, 30, 30, 35, 35, 40, 40, 45],        //IFC홀 3Unit
            [0, 15, 35, 35, 40, 40, 45, 45, 50]         //IFC홀 4Unit
        ];

        var iDiscountCnt = 0;                           //arrDiscountRate 키값
        var iDiscountRate = 0;                          //계산된 할인율
        var iOriAmount = 0;                             //시간당 사용금액

        // 계산된 값 셋팅
        iDiscountCnt = (room == "B0001") ? 0 : roomCnt;
        iOriAmount = iAmountPerHour[room.substring(0,1)] * timeCnt * roomCnt;

        if ("<%=objRs("AUTHORITY")%>" == "N") {
            iDiscountRate = 0;
        } else {
            iDiscountRate = arrDiscountRate[iDiscountCnt][timeCnt-1];
        }

        $("#spn_ori_amount").html(fnMoneyNum(iOriAmount) + "<em>원</em>");                       //기본요금
        $("#ORI_AMOUNT").val(iOriAmount);
        $("#spn_discount").html("("+total_unit_html+"할인율 " + iDiscountRate + "% 적용)");      //할인율
        $("#DISCOUNT").val(iDiscountRate);

        fnTotalAmountCal();
        /* EEEEE 금액 구하기 EEEEE */
}

	// 시간 마우스 오버
	function fnTimeMOver(room, time, obj) {
		if ($("#tr_time_" + room).hasClass("disabled") == false) {
			if ($(obj).hasClass("disable_v01") == false) {
				var selB0001YN = "N";
				var selR0001YN = "N";
				var selR0002YN = "N";
				var selR0003YN = "N";
				var selR0004YN = "N";
				if ($("#tr_time_B0001").hasClass("disabled") == false) {
					if ($("#td_time_B0001_" + time).hasClass("disable_v01") == false && $("#td_time_B0001_" + time).hasClass("disable_v02") == false) {
						selB0001YN = "Y";
					}
				}
				if ($("#tr_time_R0001").hasClass("disabled") == false) {
					if ($("#td_time_R0001_" + time).hasClass("disable_v01") == false && $("#td_time_R0001_" + time).hasClass("disable_v02") == false) {
						selR0001YN = "Y";
					}
				}
				if ($("#tr_time_R0002").hasClass("disabled") == false) {
					if ($("#td_time_R0002_" + time).hasClass("disable_v01") == false &&  $("#td_time_R0002_" + time).hasClass("disable_v02") == false) {
						selR0002YN = "Y";
					}
				}
				if ($("#tr_time_R0003").hasClass("disabled") == false) {
					if ($("#td_time_R0003_" + time).hasClass("disable_v01") == false &&  $("#td_time_R0003_" + time).hasClass("disable_v02") == false) {
						selR0003YN = "Y";
					}
				}
				if ($("#tr_time_R0004").hasClass("disabled") == false) {
					if ($("#td_time_R0004_" + time).hasClass("disable_v01") == false &&  $("#td_time_R0004_" + time).hasClass("disable_v02") == false) {
						selR0004YN = "Y";
					}
				}

				if (selB0001YN == "Y")
					$("#td_time_B0001_" + time).toggleClass("overview");
				if (selR0001YN == "Y")
					$("#td_time_R0001_" + time).toggleClass("overview");
				if (selR0002YN == "Y")
					$("#td_time_R0002_" + time).toggleClass("overview");
				if (selR0003YN == "Y")
					$("#td_time_R0003_" + time).toggleClass("overview");
				if (selR0004YN == "Y")
					$("#td_time_R0004_" + time).toggleClass("overview");
			}
		}
	}

	// 시간 마우스 아웃
	function fnTimeMOut() {
		$("td[id*=td_time_]").removeClass("overview");
	}

	// 폴리콤선택
	function fnPolyClick(objVal, idx) {
		$("button[id*=btn_poly]").removeClass("on");
		$("#btn_poly" + idx).addClass("on");
		$("#POLYCOM").val(objVal);
	}

	// 외부기자재 반입
	function fnEtcClick(objVal, idx) {
		$("button[id*=btn_etc]").removeClass("on");
		$("#btn_etc" + idx).addClass("on");
		$("#ETC").val(objVal);
	}

    // 숫자 앞에 0 붙여주는 함수
    function pad(n, width) {
        n = n.toString();
        return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
    }

	// 무료이용시간 변경
	function fnFreeTimeChg(objVal) {
		var roomCnt = fnGetRoomCnt();
		var timeCnt = fnGetTimeCnt(roomCnt);
		var total_unit = Number(roomCnt) * Number(timeCnt);

		if (total_unit == 0) {
			alert("룸과 시간을 선택해주세요.");
			$("#sel_free_time").val("0");
			return true;
		}
		if ($("#chk_room_B0001").is(":checked")) {
			alert("브룩필드홀은 무료시간을 사용할 수 없습니다.");
			$("#sel_free_time").val("0");
			fnTotalAmountCal();
			return true;
		}
		if (objVal > total_unit) {
			alert("총 유닛보다 무료이용시간을 더 사용할 수 없습니다.");
			$("#sel_free_time").val("0");
			fnTotalAmountCal();
			return true;
		}

		if ($("#YEAR").val() != '<%=Year(strNOW)%>') {
			alert("무료시간은 접속일 기준 해당년도만 사용가능합니다.");
			$("#sel_free_time").val("0");
			return true;
		}

		fnTotalAmountCal();
	}

	// 예약불가 여부 판단
	function fnRoomSelYN(objId) {
		var reservePosCnt = 0;
		var bRoomSel = "Y";
		$("td[id*=td_time_" + objId + "_]").each(function(idx,item) {
			if ($(item).hasClass("disable_v01") == false && $(item).hasClass("disable_v02") == false) {
				reservePosCnt = reservePosCnt +1;
			}
		});

		if (reservePosCnt < 2)
			bRoomSel = "N";

		return bRoomSel;
	}

	// 총 금액 계산
	function fnTotalAmountCal() {
		var roomCnt = fnGetRoomCnt();                         //룸선택수
		var timeCnt = fnGetTimeCnt(roomCnt);                  //시간선택수
		var total_unit = Number(roomCnt) * Number(timeCnt);   //룸갯수 * 시간수
		var ori_amount = 0;                                   //기본요금 (요금단가 * total_unit )
		var discount = 0;                                     //할인율
		var etc_amount = 0;                                   //
		var free_time = $("#sel_free_time").val();            //선택된 무상시간
		var pre_amount = 0;                                   //
		var total_amount = 0;                                 //최종금액

		if ($("#ORI_AMOUNT").val() != "")
			ori_amount = $("#ORI_AMOUNT").val();
		if ($("#DISCOUNT").val() != "")
			discount = $("#DISCOUNT").val();
		$("#ETC_AMOUNT").val(etc_amount);

		if (free_time >= total_unit) {
			pre_amount = Number(ori_amount) + Number(etc_amount);
			total_amount = Number(etc_amount);
			total_amount = Number(fnMoneyCut(total_amount));
		}
		else {
			total_amount = (Number(ori_amount) * (100 - Number(discount)) / 100) * (total_unit - free_time) / total_unit;
			total_amount = Number(fnMoneyCut(total_amount));
		}
		$("#spn_total_amount").html(fnMoneyNum(total_amount) + "<em>원 (VAT 별도) </em>");
		$("#TOTAL_AMOUNT").val(total_amount);
	}

	// 룸 선택 개수 구하기
	function fnGetRoomCnt() {
		var roomCnt = 0;

		$("input:checkbox[id*=chk_room_]:checked").each(function() {
			roomCnt = roomCnt + 1;
		});

		return roomCnt;
	}

	// 시간 선택 개수 구하기
	function fnGetTimeCnt(roomCnt) {
		var timeCnt = 0;
		var returnTime = 0;

		$("td[id*=td_time_]").each(function(idx,item) {
			if ($(item).hasClass("selected")) {
				timeCnt = timeCnt + 1;
			}
		});

		returnTime = timeCnt / roomCnt;
		// if (returnTime > 8) returnTime = 8;

		return returnTime;
	}

	// 금액 , 표시
	function fnMoneyNum(num) {
		var money = String(num)
		if (money.length == 5)
			money = money.substring(0,2) + "," + money.substring(2,5)
		else if (money.length == 6)
			money = money.substring(0,3) + "," + money.substring(3,6)
		else if (money.length == 7)
			money = money.substring(0,1) + "," + money.substring(1,4) + "," + money.substring(4,7)

		return money;
	}

	// 100단위 절삭
	function fnMoneyCut(num) {
		var cutMoney = Number(num) % 1000;
		return Number(num) - Number(cutMoney);
	}

	// 리셋
	function fnReset() {
		fnMonthChg("<%=Month(strNow)%>");
		var val = $("#div_top").offset();
		$("body, html").animate({scrollTop:val.top},1000);
	}

	// 달 선택 리셋
	function fnMonSelReset() {
		$("#DAY").val("");
		$("#td_info_reserve_dt").html("");
		$("#sel_free_time").val("0");
		$("#ul_pCount").removeClass("active");
		$("div[id*=div_room_]").each(function(idx,item) {
			$(item).removeClass("disabled_v01");
		});
		$("#tr_time_B0001").html("<td>브룩필드홀</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>");
		$("#tr_time_R0001").html("<td>IFC Hall 301</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>");
		$("#tr_time_R0002").html("<td>IFC Hall 302</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>");
		$("#tr_time_R0003").html("<td>IFC Hall 303</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>");
		$("#tr_time_R0004").html("<td>IFC Hall 304</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>");

		fnDaySelReset();
	}

	// 날 선택 리셋
	function fnDaySelReset() {
		$("li[id*=li_pCount]").removeClass("on");
		$("#P_COUNT").val("");
		$("#td_info_p_count").html("");
		$("#div_room").removeClass("active");
		$("div[id*=div_room_]").each(function(idx,item) {
			$(item).removeClass("disabled_v01");
			$(item).removeClass("disabled_v02");
		});
		$("input:checkbox[id*=chk_room_]").each(function(idx,item) {
			$(item).attr("disabled",true);
		});

		fnPCountSelReset();
	}

	// 인원 선택 리셋
	function fnPCountSelReset() {
		$("input:checkbox[id*=chk_room_]:checked").each(function(idx,item) {
			$(item).attr("checked",false);
		});
		$("div[id*=div_room_]").each(function(idx,item) {
			$(item).removeClass("check");
		});
		$("tr[id*=tr_time_]").each(function(idx,item) {
			$(item).addClass("disabled");
		});
		$("#td_info_room").html("");
		$("div[id*=div_info_]").each(function(idx,item) {
			$(item).removeClass("check");
		});
		$("td[id*=td_time_]").each(function(idx,item) {
			if ($(item).hasClass("selected")) {
				$(item).removeClass("selected");
			}
		});

		fnInfoReset();
	}

	// 정보 리셋
	function fnInfoReset() {
		$("#td_info_time").html("");
		$("#ORI_AMOUNT,#DISCOUNT,#FREE_TIME,#TOTAL_AMOUNT").val("");
		$("#spn_ori_amount,#spn_total_amount").html("0<em>원</em>");
		$("#sel_free_time").val("0");
		fnPolyClick("N",1);
		fnEtcClick("N",1);
	}

    // 예약가능시간 체크
    function fnTimeCheck() {
        $.ajax ({
            type : "POST",
            url : "./ajax/reservation_proc.asp",
            dataType : "json",
            //data : ({"NOW" : "2019-12-25 15:01", "mode" : "is_offday"}),      //test
            data : ({"NOW" : "<%=strNOW%>", "mode" : "is_offday"}),
            success : function (data) {

                if (data.result == "") {
                    alert("err_msg");
                } else {

                    var now = new Date();
                    //var now = new Date('2020-01-21T09:41');         // test

                   // 공휴일 체크
                    if (data.result == 'T' || now.getDay() == 0 || now.getDay() == 6) {
                        $("#OFFDAY").val("T");
                    }

                    // 예약가능시간체크
                    var today = now.getFullYear() + '-' + pad(now.getMonth()+1,2) + '-' + pad(now.getDate(),2);
                    if (now < new Date(today+'T09:00') || new Date(today+'T17:00') < now) {
                        $("#OFFTIME").val("T");
                    }

                    if ($("#OFFDAY").val() == "T") {
                      if (now.getFullYear() == "2022" &&  now.getMonth()+1 != 4) {
                        alert(now.getFullYear() + "년 " + (now.getMonth()+1) + "월 " + now.getDate() + "일은 공휴일입니다.\n공휴일은 예약이 불가능합니다. ");
                      }
                    } else if ($("#OFFTIME").val() == "T") {
                        alert("현재 시간은 " + now.getHours() + "시 " + now.getMinutes() + "분 입니다. \n9시에서 17시 사이에 예약 가능합니다 .");
                    }

                }
            },
            error : function (data) {
                $("#index_loading_overlay").hide();
                alert("Error : Ajax Error : " + data);
            }
        });
    }

	// 예약하기
	function fnReservation() {

        var now = new Date();

        // 예약가능한 시간이 아닌경우 예약불가.
        if ($("#OFFDAY").val() == "T") {
          if (now.getFullYear() == "2022" &&  now.getMonth()+1 != 4) {
            alert(now.getFullYear() + "년 " + (now.getMonth()+1) + "월 " + now.getDate() + "일은 공휴일입니다.\n공휴일은 예약이 불가능합니다. ");
            return true;
          }
        } else if ($("#OFFTIME").val() == "T") {
            alert("현재 시간은 " + now.getHours() + "시 " + now.getMinutes() + "분 입니다. \n9시에서 17시 사이에 예약 가능합니다 .");
            return true;
        }

		$("#R_QTY").val(fnGetRoomCnt());
		$("#T_QTY").val(fnGetTimeCnt($("#R_QTY").val()));

		if ($("#DAY").val() == "") {
			alert("예약 날짜를 선택해주시기 바랍니다.");
			var val = $("#div_top").offset();
			$("body, html").animate({scrollTop:val.top},1000);
			return true;
		}
		else if ($("#R_QTY").val() == 0) {
			alert("회의실을 선택해주시기 바랍니다.");
			var val = $("#ul_pCount").offset();
			$("body, html").animate({scrollTop:val.top},1000);
			return true;
		}
		else if ($("#T_QTY").val() == 0) {
			alert("예약시간을 선택해주시기 바랍니다.");
			var val = $("#div_timeScroll").offset();
			$("body, html").animate({scrollTop:val.top},1000);
			return true;
		}
		else if ($("#T_QTY").val() < 2) {
			alert("최소 2시간 이상 예약 가능합니다.");
			var val = $("#div_timeScroll").offset();
			$("body, html").animate({scrollTop:val.top},1000);
			return true;
		}
		else if ($("#R_QTY").val() == 4 && $("#T_QTY").val() < 4) {
			alert("IFC홀 전체 사용시 최소 4시간 이상 예약 가능합니다.");
			var val = $("#div_timeScroll").offset();
			$("body, html").animate({scrollTop:val.top},1000);
			return true;
		}
		else {
			if ($("#sel_free_time").val() > Number($("#R_QTY").val()) * Number($("#T_QTY").val())) {
				alert("총 유닛보다 무료이용시간을 더 사용할 수 없습니다.");
				$("#sel_free_time").val("0");
				return true;
			}
			else
				$("#FREE_TIME").val($("#sel_free_time").val());

			var roomNos = "";
			var roomNames = "";
			var roomPick = "";
			var reserveTimes = "";
			$("input:checkbox[id*=chk_room_]").each(function(idx,item) {
				if ($(item).is(":checked")) {
					if (idx == 0) {
						roomNames = roomNames + ",브룩필드홀";
						roomPick = $(item).val();
					}
					if (idx == 1) {
						roomNames = roomNames + ",IFC Hall 301";
						roomPick = $(item).val();
					}
					if (idx == 2) {
						roomNames = roomNames + ",IFC Hall 302";
						roomPick = $(item).val();
					}
					if (idx == 3) {
						roomNames = roomNames + ",IFC Hall 303";
						roomPick = $(item).val();
					}
					if (idx == 4) {
						roomNames = roomNames + ",IFC Hall 304";
						roomPick = $(item).val();
					}
					roomNos = roomNos + "," + $(item).val();
				}
			});
			if (roomNos.substring(0,1) == ",") {
				roomNos = roomNos.substring(1, roomNos.length);
			}
			if (roomNames.substring(0,1) == ",") {
				roomNames = roomNames.substring(1, roomNames.length);
			}

			$("td[id*=td_time_" + roomPick + "]").each(function(idx,item) {
				if ($(item).hasClass("selected")) {
					reserveTimes = reserveTimes + "," + (idx+9);
				}
			});
			if (reserveTimes.substring(0,1) == ",") {
				reserveTimes = reserveTimes.substring(1, reserveTimes.length);
			}

			if ($("#sel_free_time").val() != "0" && roomNos == "B0001") {
				alert("브룩필드드홀은 무료시간을 사용할 수 없습니다.");
				return true;
			}

			// if (reserveTimes.substring(0,2) == "12") {
			// 	alert("오후 12시 시작은 예약이 불가능합니다.\n\n이용 시간대를 다시 선택해주세요.");
			// 	return true;
			// }

			$("#ROOM_NOS").val(roomNos);
			$("#ROOM_NAMES").val(roomNames);
			$("#RESERVE_TIMES").val(reserveTimes);

			var frm = document.frm_reservation1;
			frm.submit();
		}
	}
</script>
<style>
/* for popup */
.layer_popup {display:block;float:left;position:fixed;top:50%;left: 50%;z-index: 9999;border:solid 1px #888; background:#fff; padding:0; top: 150px; left: 50px;}
.js_PopClose, .jsAxpClose {display:block;position:absolute;top:24px;right:24px;width:12px;height:12px;}
.js_PopClose.type2{font-size:14px;color:#fff;width:40px;height:20px;background-position:-460px -55px}
.noticePopWarp {position:fixed;border:0;float:none;line-height:0;}
.noticePopWarp .btn_area{background: #1a1917;color:#fff;padding-top:4px;padding-left:9px;height:25px;line-height:21px}
.noticePopWarp .btn_area .js_PopClose{position:relative;top:0px;right:0;display:inline-block;margin:0 12px 0 5px;}
.fr { float: right; }
.vjs-fullscreen-control { display: none; }
.vjs-volume-control, .vjs-mute-control { right: 20px;}
</style>

<form name="frm_reservation1" action="reservation_detail.asp" method="post">
	<input type="hidden" id="YEAR" name="YEAR" value="" />
	<input type="hidden" id="MONTH" name="MONTH" value="" />
	<input type="hidden" id="DAY" name="DAY" value="" />
	<input type="hidden" id="P_COUNT" name="P_COUNT" value="" />
	<input type="hidden" id="R_QTY" name="R_QTY" value="" />
	<input type="hidden" id="T_QTY" name="T_QTY" value="" />
	<input type="hidden" id="POLYCOM" name="POLYCOM" value="" />
	<input type="hidden" id="ETC" name="ETC" value="" />
	<input type="hidden" id="ORI_AMOUNT" name="ORI_AMOUNT" value="" />
	<input type="hidden" id="DISCOUNT" name="DISCOUNT" value="" />
	<input type="hidden" id="ETC_AMOUNT" name="ETC_AMOUNT" value="0" />
	<input type="hidden" id="FREE_TIME" name="FREE_TIME" value="" />
	<input type="hidden" id="TOTAL_AMOUNT" name="TOTAL_AMOUNT" value="" />
	<input type="hidden" id="ROOM_NOS" name="ROOM_NOS" value="" />
	<input type="hidden" id="ROOM_NAMES" name="ROOM_NAMES" value="" />
	<input type="hidden" id="RESERVE_TIMES" name="RESERVE_TIMES" value="" />
    <input type="hidden" id="OFFDAY" name="OFFDAY" value="F" />
    <input type="hidden" id="OFFTIME" name="OFFTIME" value="F" />
</form>

<div class="contBody" id="div_top">
		<div style="float:right;margin-top:50px">
			<a href="#?w=840" class="nbtnG02 jsbtnLyp" rel="layerPop02">회의실 예약 도움말</a>
		</div>
	<div class="pTitle">
		<h2>회의실 예약내역</h2>
	</div>

	<dl class="rmrWrap">
		<dt>step01. 날짜선택</dt>
		<dd class="settingBox">
			<div class="boxL">
				<div class="stitWrap">
					<strong class="titS">예약 월 선택</strong>
				</div>
				<div class="selectYM">
					<div class="textYM">
						<span class="year"><%=Year(strNOW)%></span>년
						<span class="month"><%=Month(strNOW)%></span>월
					</div>
					<ul id="ul_Month">
						<%

						Dim strDay, endDay, iCnt, resMonth()

						'시작일과 마지막일
						strDay  = now()
						endDay  = DateAdd("d", 120, now())

						'표시되어야 하는 월수 계산하여 ReDim
						iCnt = DateDiff("m", strDay, endDay)
						ReDim resMonth(iCnt)

						'예약가능한 월 정보를 저장
						For i=0 To (iCnt)
						    resMonth(i) = Year(strDay) & "-" & Right("0" & Month(strDay)+i, 2)

                            ' 년도가 바뀔 경우
						    If CInt(Right(resMonth(i), 2)) > 12 Then
						        resMonth(i) = (Left(resMonth(i), 4)+1) & "-0" & (Right(resMonth(i), 2)-12)
						    End If
						Next

						'maxMonth = 12
						'If 12 - Month(strNOW) > 3 Then
						'	maxMonth = Month(strNOW) + 3
						'End If
						'For i = Month(strNOW) To maxMonth
						For i = 0 To iCnt
						%>
							<!-- <li id="li_cal_month<%=i%>"><button type="button" onClick="fnMonthChg('<%=i%>');"><%=i%>월</button></li> -->
							<li id="li_cal_month<%=Right(resMonth(i), 2)%>"><button type="button" onClick="fnMonthChg('<%=Left(resMonth(i), 4)%>', '<%=Right(resMonth(i), 2)%>');"><%=CInt(Right(resMonth(i), 2))%>월</button></li>
						<%
						Next
						%>
					</ul>
				</div>

				<div class="stitWrap">
					<strong class="titS">예약 정보 (<em class="infoYM" id="em_cal_YM"></em>)</strong>
				</div>
				<ul class="usingInfoT">
					<li>
						<span class="tits">
							월 예약 가능시간 <em><%=objRs("LIMIT_TIME")%>시간기준</em>
						</span>
						<span class="conts">
							<em id="em_cal_limit_time"></em>시간
						</span>
					</li>
					<li>
						<span class="tits">
							월 잔여 무상시간 <em><%=objRs("MONTHLY_FREE_TIME")%>시간기준</em>
						</span>
						<span class="conts">
							<em id="em_cal_free_time"></em>시간
						</span>
					</li>
				</ul>
			</div>
			<div class="boxR">
				<div class="stitWrap">
					<strong class="titS">예약 날짜 선택</strong>
				</div>
				<div class="calendarWrap">
					<div class="hgroup">
						<p>SUN</p>
						<p>MON</p>
						<p>TUE</p>
						<p>WED</p>
						<p>THU</p>
						<p>FRI</p>
						<p>SAT</p>
					</div>
					<ul class="dgroup" id="ul_cal_day"></ul>
				</div>
			</div>
		</dd>

		<dt>step02. 참석인원 / 회의실 / 예약시간 선택</dt>
		<dd class="settingBox">
			<div class="stitWrap">
				<strong class="titS">참석인원 선택</strong>
			</div>
			<ul class="peopleNumBox" id="ul_pCount">
				<li id="li_pCount25" value="25"><button type="button" onClick="fnPCountChk('25');">25명</button></li>
				<li id="li_pCount50" value="50"><button type="button" onClick="fnPCountChk('50');">50명</button></li>
				<li id="li_pCount75" value="75"><button type="button" onClick="fnPCountChk('75');">75명</button></li>
				<li id="li_pCount100" value="100"><button type="button" onClick="fnPCountChk('100');">100명</button></li>
			</ul>
            <ul class="icotxtBox">
                <li>정부의 사회적 거리두기 지침에 따라 회의실 참석인원이 조정되었습니다</li>
                <li>50명 이상의 예약은 관리자에게 문의해주시면 감사하겠습니다.(문의 : 02-6137-2100) </li>
            </ul>

			<div class="stitWrap">
				<strong class="titS">회의실 선택</strong>
				<div class="posR">
					<a href="#?w=840" class="nbtnG02 jsbtnLyp" rel="layerPop01">요금표 보기</a>
					<a href="#?w=840" class="nbtnG02 jsbtnLyp" rel="layerPop03">회의실 안내</a>
				</div>
			</div>

			<div class="roomSelectBox" id="div_room">
				<div class="stateGuide">
					<span class="state01">예약가능</span>
					<span class="state02">이미 예약 중 (예약 불가)</span>
					<span class="state03">예약 불가</span>
					<span class="state04">선택완료</span>
				</div>

				<div class="meetingRoom">
					<div class="room01" id="div_room_B0001" onClick="fnRoomChk('B0001');" onMouseOver="fnRoomMOver('B0001');" onMouseOut="fnRoomMOut();">
						<span>
							<input type="checkbox" id="chk_room_B0001" disabled value="B0001" />
							<label>브룩필드홀 - 25석</label>
						</span>
					</div>
					<div class="room02" id="div_room_R0001" onClick="fnRoomChk('R0001');" onMouseOver="fnRoomMOver('R0001');" onMouseOut="fnRoomMOut();">
						<span>
							<input type="checkbox" id="chk_room_R0001" disabled value="R0001" />
							<label>IFC Hall 301 - 25석</label>
						</span>
					</div>
					<div class="room03" id="div_room_R0002" onClick="fnRoomChk('R0002');" onMouseOver="fnRoomMOver('R0002');" onMouseOut="fnRoomMOut();">
						<span>
							<input type="checkbox" id="chk_room_R0002" disabled value="R0002" />
							<label>IFC Hall302-25석</label>
						</span>
					</div>
					<div class="room04" id="div_room_R0003" onClick="fnRoomChk('R0003');" onMouseOver="fnRoomMOver('R0003');" onMouseOut="fnRoomMOut();">
						<span>
							<input type="checkbox" id="chk_room_R0003" disabled value="R0003" />
							<label>IFC Hall 303 - 25석</label>
						</span>
					</div>
					<div class="room05" id="div_room_R0004" onClick="fnRoomChk('R0004');" onMouseOver="fnRoomMOver('R0004');" onMouseOut="fnRoomMOut();">
						<span>
							<input type="checkbox" id="chk_room_R0004" disabled value="R0004" />
							<label>IFC Hall 304-25석</label>
						</span>
					</div>
				</div>
			</div>

			<ul class="icotxtBox">
				<li>참석인원보다 많은 회의실을 예약하고자 하는 경우 참석인원을 변경해주세요.</li>
				<li>브룩필드홀과 IFC Hall은 동시에 예약 불가능 합니다. (브룩필드홀은 무료 이용시간 사용 불가)</li>
			</ul>

			<div class="stitWrap" id="div_timeScroll">
				<strong class="titS">예약시간 선택</strong>
				<div class="stateGuide">
					<span class="state01">예약가능</span>
					<span class="state02">예약 중 (예약 불가)</span>
					<span class="state03">클리닝 타임 (예약 불가)</span>
					<span class="state04">선택완료</span>
				</div>
			</div>

			<div class="timeTbl">
				<table>
					<caption>회의실 예약시간 정보</caption>
					<colgroup>
						<col style="width:10%;" /><col style="width:10%;" /><col style="width:10%;" /><col style="width:10%;" /><col style="width:10%;" /><col style="width:10%;" /><col style="width:10%;" /><col style="width:10%;" /><col style="width:10%;" /><col style="width:10%;" /><col style="width:10%;" />
					</colgroup>
					<thead>
						<tr>
							<th>회의실</th><th>&nbsp;&nbsp;09:00<br/>~10:00</th>
							<th>&nbsp;&nbsp;10:00<br/>~11:00</th>
							<th>&nbsp;&nbsp;11:00<br/>~12:00</th>
							<th>&nbsp;&nbsp;12:00<br/>~13:00</th>
							<th>&nbsp;&nbsp;13:00<br/>~14:00</th>
							<th>&nbsp;&nbsp;14:00<br/>~15:00</th>
							<th>&nbsp;&nbsp;15:00<br/>~16:00</th>
							<th>&nbsp;&nbsp;16:00<br/>~17:00</th>
							<th>&nbsp;&nbsp;17:00<br/>~18:00</th>
						</tr>
					</thead>
					<tbody>
						<tr id="tr_time_B0001"></tr>
						<tr id="tr_time_R0001"></tr>
						<tr id="tr_time_R0002"></tr>
						<tr id="tr_time_R0003"></tr>
						<tr id="tr_time_R0004"></tr>
					</tbody>
				</table>
			</div>

			<ul class="icotxtBox">
				<li>최소 2시간 이상, 이후 1시간 단위로 추가 가능 (단, IFC홀 전체사용시 최소4시간 이상)</li>
				<li>기존 예약 된 회의 전/후 1시간은 예약 불가</li>
				<!-- <li>24시간 이전 예약 불가 (당일 예약 불가)</li> -->
				<!-- <li>오후 12시는 예약시작시간으로 선택이 불가능합니다.</li> -->
			</ul>

		</dd>
		<dt>step03. 예약정보 확인</dt>
		<dd class="settingBox">
			<div class="stitWrap">
				<strong class="titS">선택한 예약정보</strong>
			</div>

			<div class="tblStyle1">
				<table>
					<caption>예약 정보</caption>
					<colgroup>
						<col style="width:17%">
						<col style="width:37%">
						<col style="width:15%">
						<col style="width:*%">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row"><span>예약날짜</span></th>
							<td id="td_info_reserve_dt"></td>
							<th scope="row"><span>예약시간</span></th>
							<td id="td_info_time"></td>
						</tr>
						<tr>
							<th scope="row"><span>회의실</span></th>
							<td id="td_info_room"></td>
							<th scope="row"><span>참석인원</span></th>
							<td id="td_info_p_count"></td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="roomSelectBox cplt">
				<div class="meetingRoom">
					<div class="room01" id="div_info_B0001"></div>
					<div class="room02" id="div_info_R0001"></div>
					<div class="room03" id="div_info_R0002"></div>
					<div class="room04" id="div_info_R0003"></div>
					<div class="room05" id="div_info_R0004"></div>
				</div>
			</div>

		</dd>
		<dt>step04. 기자재 선택 및 요금정보 확인</dt>
		<dd class="settingBox">
			<div class="tblStyle1">
				<table>
					<caption>예약 정보</caption>
					<colgroup>
						<col style="width:17%">
						<col style="width:*%">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row"><span>폴리콤(3자통화)</span></th>
							<td>
								<div class="gearList">
									<button class="nbtnSel" id="btn_poly1" onClick="fnPolyClick('N',1);">사용안함</button>
									<button class="nbtnSel" id="btn_poly2" onClick="fnPolyClick('L',2);">국내전화 사용</button>
									<button class="nbtnSel" id="btn_poly3" onClick="fnPolyClick('G',3);">국제전화 사용</button>
								</div>
							</td>
						</tr>
						<tr>
							<th scope="row"><span>외부기자재 반입</span></th>
							<td>
								<div class="gearList">
									<button class="nbtnSel" id="btn_etc1" onClick="fnEtcClick('N',1);">반입안함</button>
									<button class="nbtnSel" id="btn_etc2" onClick="fnEtcClick('Y',2);">반입함</button>
								</div>
							</td>
						</tr>
						<tr>
							<th scope="row"><span>기본요금</span></th>
							<td>
								<span class="price" id="spn_ori_amount">0<em>원</em></span> <span id="spn_discount"></span>
							</td>
						</tr>
						<tr>
							<th scope="row"><span>무료이용시간</span></th>
							<td id ="td_free_time">
								<!-- <select id="sel_free_time" name="sel_free_time" onChange="fnFreeTimeChg(this.value);">>
									<option value="0" selected>0시간</option>
									<% For i = 2 To objRs("USE_FREE_TIME") %>
										<option value="<%=i%>"><%=i%>시간</option>
									<% Next %>
								</select>
								<span class="gearNoti">현재 <%=objRs("USE_FREE_TIME")%>시간 까지 이용 가능</span> -->
							</td>
						</tr>
					</tbody>
				</table>
				<table class="totalP">
					<caption>예약 정보</caption>
					<colgroup>
						<col style="width:17%">
						<col style="width:*%">
					</colgroup>
					<tbody>
						<tr>
							<th><span>최종금액</span></th>
							<td><span class="priceT" id="spn_total_amount">0<em>원</em></span></td>
						</tr>
					</tbody>
				</table>
			</div>
		</dd>
	</dl>
	<ul class="listDot">
        <li>퀵서비스 이용시 회의실에선 수령이 불가하오니 1층로비에서 수령바랍니다. </li>
		<li>외부기자재 반입하는 경우 관리자에게 별도 문의 바랍니다.</li>
		<li>폴리콤(3자 통화) 사용 시 별도 비용이 청구됩니다.</li>
		<li>안내데스크 : 02-6137-2100</li>
	</ul>
	<div class="btnWrap">
		<button class="nbtnW" id="btn_reset" name="btn_reset" onClick="fnReset();">선택 초기화</button>
		<button class="nbtnG" id="btn_reservation" name="btn_reservation" onClick="fnReservation();">예약정보 입력</button>
	</div>
</div>
<!--#include file = "reservation_layer.asp" -->
<!--#include file = "index_footer.asp" -->

<!-- layer popup -->
<% If Request.Cookies("noticePop_211110")  <> "true" Then %>
<div id="noticePop_211110" class="layer_popup noticePopWarp" style="position: absolute; z-index: 99999; line-height: 0;">
	<div class="video_wrap">
		<img src="/img/popup/20220407_popup.png" usemap="#popMap" alt="공지사항" style="width:320px;" />
	</div>
	<div class="btn_area clearfix" style="background-color: black; color: white; margin-top: 0px; height: 20px; line-height: 20px;">
		<label><input type="checkbox" class="vertical_m" /> <%=lang_text(1)%> </label> <a href="#none" class="js_PopClose fr type2" style="color: white;">Close</a>
	</div>
</div>
<% End If %>
 <!-- //layer popup -->
