<!DOCTYPE html>
<html xml:lang="ko" lang="ko">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<!--<meta http-equiv='refresh' content='0; url=https://ifcseoul.com/index.asp'target='_top'>-->
    <title>IFC Seoul</title>
    <link rel="stylesheet" type="text/css" href="css/base.css" />
    <link rel="stylesheet" type="text/css" href="css/base_ko.css" />
    <link rel="stylesheet" type="text/css" href="css/layout.css" />
    <link rel="stylesheet" type="text/css" href="css/common.css" />
    <link rel="stylesheet" type="text/css" href="css/animation.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	<!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script> -->
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.event.drag-1.5.min.js"></script>
    <script type="text/javascript" src="js/jquery.touchSlider.js"></script>
	<script type="text/javascript" src="js/jquery.bxslider.js"></script>
    <script type="text/javascript" src="js/ifc_ui.js"></script>
    <script type="text/javascript" src="js/ifc_ui02.js"></script>
    <script type="text/javascript" src="js/ifc_custom.js"></script>
	<script type="text/javascript" src="js/md5.js"></script>
	<script type="text/javascript" src="js/swiper.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>

	<script type="text/javascript">
	$(document).ready(function() {
		/*var selectYMFn = new selectYM('.selectYM > ul', 5);
		$('.settingBox .infoYM').text(selectYMFn.getYear() + '.' + selectYMFn.getMonth());

		$('.selectYM > ul button').click(function() {
			var $this = $(this),
				  $textYM = $this.parents('.selectYM').find('.textYM');
			var $dataY = $this.data('year'),
				  $month = $(this).text()
				  $textY = parseInt($('.year', $textYM).text());

			if(!$this.parent().hasClass('on')) {
				if($textY != $dataY) {
					$('.year', $textYM).text($dataY);
				}
				$('.month', $textYM).text($month);

				$('li', $this.parents('.selectYM')).removeClass('on');
				$this.parent().addClass('on');

				selectYMFn.chYear($dataY);
				selectYMFn.chMonth(parseInt($month));
				$('.settingBox .infoYM').text(selectYMFn.getYear() + '.' + selectYMFn.getMonth());

				calendarFn.reDrawFn(selectYMFn.getYear(), selectYMFn.getMonth());
			}
		});

		var calendarFn = new calendar('#calendarBox', {
			weekText : ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT']
		});

		$('#calendarBox').on('click', 'td a', function(e) {e.preventDefault();
			var $this = $(this);
			if(!$this.hasClass('on')) {
				$('#calendarBox td').removeClass('on');
				$this.parent().addClass('on');
			}
		});*/
	});
	</script>
    <!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
        <script type="text/javascript" src="js/respond.min.js"></script>
		<script type="text/javascript">
			var ltIE9 = true;
		</script>
    <![endif]-->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-61219190-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-61219190-1');
</script>

</head>
<!--[if lt IE 7]><body class="msie ie6 lt-ie9 lt-ie8 lt-ie7 lt-css3"><![endif]-->
<!--[if IE 7]>   <body class="msie ie7 lt-ie9 lt-ie8 lt-css3"><![endif]-->
<!--[if IE 8]>   <body class="msie ie8 lt-ie9 lt-css3"><![endif]-->
<!--[if IE 9]>   <body class="msie ie9 css3"><![endif]-->
<!--[if gt IE 9]><!-->
<body>
	<div id="index_loading_overlay">
		<div class="index_loading_overlay_img">
			<img src="img/ajax-loader.gif" alt="overlay loading img" />
		</div>
	</div>
<!--<![endif]-->
	<div id="wrap" class="">
        <!-- #include file = "../com/com_snb_all.html" -->
		<!-- //snb -->
		<div class="snbShadow"></div>
		<nav id="snb">
			<span class="tit v01">The Forum at IFC<br />예약</span>
			<ul class="menuList">
				<li><a href="forum_info.asp">회의실 소개<span></span></a></li>
				<li><a href="reservation.asp">회의실 예약<span></span></a></li>
				<% If strUserId&"" <> "" Then%>
					<li><a href="reservation_list.asp">회의실 예약내역<span></span></a></li>
					<li><a href="user_view.asp" id="a_myPage">My page<span></span></a></li>
				<% End If %>
			</ul>
		</nav>
		<!-- //snb -->
		<div id="containerWrap">
		<header>
			<!-- u_skip -->
			<div id="u_skip"></div>
			<!-- //u_skip -->
			<!-- header -->
			<div id="header">
				<div class="container">
					<a href="#none" class="btnMenu">홈</a>
					<h1><a href="/ko/MN_00_00.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': 'IFC SEOUL LOGO'});"><span>IFC Seoul</span></a></h1>
					<a href="HF_01_00.asp" class="btnMap">위치 및 지도</a>
				</div>
				<div id="gnb">
					<ul>
						<li><a href="/ko/MN_00_00.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': '홈'});">홈</a></li>
						<li><a href="/ko/HF_01_00.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': '위치 및 지도'});">위치 및 지도</a></li>
						<li><a href="/ko/HF_02_00.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': 'Contact'});">Contact</a></li>
						<% If Request("current_type")&"" = "en" Then %>
							<li><a href="forum_info.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': 'Korean'});">Korean</a></li>
						<% Else %>
							<li><a href="forum_info_EN.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': 'ENGLISH'});">ENGLISH</a></li>
						<% End If %>
						<% If strUserId&"" <> "" Then%>
							<li><a href="logout.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': '로그아웃'});">로그아웃</a></li>
						<% Else %>
							<!-- <li><a href="index.asp">입주사 로그인</a></li> -->
						<% End If %>
					</ul>
				</div>
				<div id="lnb">
					<ul>
						<li><a href="/ko/IS_01_00.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': 'IFC 서울'});">IFC 서울</a></li>
						<li><a href="/ko/BD_01_00.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': '빌딩'});">빌딩</a></li>
						<li><a href="/ko/NM_01_00.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': '주변 지역 안내'});">주변 지역 안내</a></li>
						<li><a href="/ko/NE_01_00.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': '소식'});">소식</a></li>
						<li><a href="../visitor/index.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': '방문자예약 및 포털'});">방문자예약 및 건물사용신청</a></li>
						<li><a href="forum_info.asp" onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': 'The Forum at IFC 예약'});">The Forum at IFC 예약</a></li>
                        <li><a href="../visitor/smartsuites.asp " onclick="gtag('event', '버튼클릭', {'event_category': '메인배너', 'event_label': 'The Smart Suites at IFC'});">The Smart Suites at IFC</a></li>
					</ul>
				</div>
			</div>
			<!-- //header -->
		</header>
		<section>
			<!-- container -->
			<div id="container">
				<!-- lineMap -->
				<div class="lineMapWrap">
					<ul class="lineMap"></ul>
				</div>
				<!-- //lineMap -->
				<div id="content">
