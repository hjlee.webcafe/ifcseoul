<!--#include file = "default_control.asp" -->
<%
re_url = "reservation_list"
%>
<!--#include file = "session_check.asp" -->
<!--#include file = "index_header.asp" -->
<%
Dim selectQuery, reserve_no
reserve_no = Request.QueryString("reserve_no")

selectQuery = "select (SELECT MIN(CONVERT(INT,RESERVE_TIME)) FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MIN_TIME, (SELECT MAX(CONVERT(INT,RESERVE_TIME))+1 FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MAX_TIME, (SELECT BUSINESS_RE_NO FROM TBL_USER_INFO WHERE UID=RI.UID) AS BUSINESS_RE_NO, * from TBL_RESERVATION_INFO RI where RESERVE_NO = '"& reserve_no &"' AND RI.UID = '" & strUserId & "'"

set objRs = SendQuery(objConn,selectQuery)

If objRs.EOF Then
%>
	<script>
		alert("비정상적인 접속입니다.");
		location.href="reservation_list.asp";
	</script>
<%
Else
	strReserveStatus = ""
	If objRs("RESERVE_STATUS") = "R" Then
		strReserveStatus = "예약 완료"
	ElseIf objRs("RESERVE_STATUS") = "C" Then
		strReserveStatus = "예약 취소"
	ElseIf objRs("RESERVE_STATUS") = "Y" Then
		strReserveStatus = "이용 완료"
	End If

	strBUSINESS_RE_NO = objRs("BUSINESS_RE_NO")
	If Len(strBUSINESS_RE_NO) > 7 Then
		strBUSINESS_RE_NO = Left(strBUSINESS_RE_NO, 7) & "*****"
	End If

	strReserveDt = objRs("RESERVE_DT")
	strReserveDt_Temp = ""
	strReserveDt_Temp2 = ""
	If Len(strReserveDt) = 8 Then
		strReserveDt = Mid(strReserveDt,1,4) & "년 " & Mid(strReserveDt,5,2) & "월 " & Mid(strReserveDt,7,2) & "일 "
		strReserveDt_Temp = Mid(objRs("RESERVE_DT"),1,4) & "-" & Mid(objRs("RESERVE_DT"),5,2) & "-" & Mid(objRs("RESERVE_DT"),7,2)
		strReserveDt_Temp2 = strReserveDt_Temp & " " & objRs("MIN_TIME") & ":00:00"
	End If

    '예약시간표시
    preTime = ""
    strTimeInfo = ""
    arrReserveTime = Split(objRs("RESERVE_TIMES"), ",")
    For i=0 To UBound(arrReserveTime)
        If i = 0 Then
            strTimeInfo = strTimeInfo & arrReserveTime(i) & ":00"
        ElseIf (CInt(preTime+1) <> CInt(arrReserveTime(i))) Then
            strTimeInfo = strTimeInfo & " ~ " & CInt(preTime+1) & ":00<br/> " & arrReserveTime(i) & ":00"
        End If
        preTime = arrReserveTime(i)
    Next
    strTimeInfo = strTimeInfo & " ~ " & CInt(arrReserveTime(UBound(arrReserveTime))+1) & ":00"

	strRequirement = objRs("REQUIREMENT")
	If strRequirement&"" <> "" Then
		strRequirement = Replace(Replace(strRequirement, "&lt", "<"),"<br><br>","<br>")
	End If

	strPolycom = ""
	If objRs("POLYCOM")&"" = "N" Then
		strPolycom = "사용안함"
	ElseIf objRs("POLYCOM")&"" = "L" Then
		strPolycom = "국내전화"
	ElseIf objRs("POLYCOM")&"" = "G" Then
		strPolycom = "국외전화"
	End If

	strEtc = ""
	If objRs("ETC")&"" = "N" Then
		strEtc = "반입안함"
	ElseIf objRs("ETC")&"" = "Y" Then
		strEtc = "반입함"
	End If

    strFilEquipment = ""
    If objRs("FILMING_EQUIPMENT")&"" = "N" Then
        strFilEquipment = "반입안함"
    ElseIf objRs("FILMING_EQUIPMENT")&"" = "Y" Then
        strFilEquipment = "반입함"
    End If

	strOriAmount = FnMoneySet(objRs("ORI_AMOUNT")) & " 원"
	strTotalAmount = FnMoneySet(objRs("TOTAL_AMOUNT")) & " 원"
	strCancelAmount = FnMoneySet(objRs("CAN_AMOUNT")) & " 원 (취소패널티)"

	strOneDayYN = "N"
	If DateDiff("d",Left(objRs("REG_DT"),10),CDate(strReserveDt_Temp)) < 1 Then
		strOneDayYN = "Y"
	End If
End If
%>
<script type="text/javascript">
	function fnGoEdit() {
		//get_link = "reservation_list_edit.asp?reserve_no=<%=reserve_no%>";
		//location.href = get_link;
	}

	function fnCancel(objVal) {
        <% If DateDiff("d", NOW, CDate(strReserveDt)) <= 30 Then %>
            alert("페널티기간입니다. 취소시 포름관리자에게 문의 바랍니다. (02-6137-2100) ")
            return true;
		<% Else %>
			var reserve_no = "<%=reserve_no%>";
			var can_uid = "<%=strUserId%>";

			if (!confirm("취소 시 위약금이 발생할 수 있습니다.\n\n취소 하시겠습니까?"))
				return true;

			$("#index_loading_overlay").show();

			$.ajax ({
				type : "POST",
				url : "./ajax/reservation_proc.asp",
				dataType : "json",
				data : ({
							"mode" : "cancel",
							"RESERVE_NO" : reserve_no,
							"CAN_UID" : can_uid
						}),
				success : function (data) {
					$("#index_loading_overlay").hide();

					if (typeof data != 'object') {
						alert("Error :: Ajax Return Error");
					}
					else
					{
						var error_msg = data.error_msg;
						if (data.result == "") {
							alert(error_msg);
						}
						else {
							alert("예약 취소가 완료되었습니다.");
							location.href = "reservation_list.asp";
						}
					}
				},
				error : function (data) {
					$("#index_loading_overlay").hide();
					alert("Error : Ajax Error : " + data);
				}
			});
		<% End If %>
	}

	function fnGoList() {
		get_link = "reservation_list.asp";
		location.href = get_link;
	}

	function fnGoPrint() {
		get_link = "reservation_list_detail.asp?reserve_no=<%=reserve_no%>";
		window.open(get_link, "invoice");
	}
</script>
<div class="contBody">
	<div class="pTitle"><h2>회의실 예약 상세정보</h2></div>
	<div class="tblStyle2">
		<table>
			<caption>회의실 예약 상세정보</caption>
			<colgroup>
				<col style="width:17%"/><col style="width:35%"/><col style="width:17%"/><col style="width:*%"/>
			</colgroup>
			<tbody>
				<tr>
					<th scope="row">예약번호</th>
					<td><%=objRs("RESERVE_NO")%></td>
					<th scope="row">예약 상태</th>
					<td><%=strReserveStatus%></td>
				</tr>
				<tr>
					<th scope="row">회사명</th>
					<td><%=objRs("USER_NAME")%></td>
					<th scope="row">사업자등록번호</th>
					<td><%=strBUSINESS_RE_NO%></td>
				</tr>
				<tr>
					<th scope="row">회의실</th>
					<td><%=objRs("ROOM_NAMES")%></td>
					<th scope="row">이용시간</th>
					<td><%=strReserveDt%> <br/><%=strTimeInfo%></td>
				</tr>
				<tr>
					<th scope="row">회의명</th>
					<td colspan="3"><%=objRs("SUBJECT")%></td>
				</tr>
				<tr>
					<th scope="row">예약자명</th>
					<td><%=objRs("RESERVE_NAME")%></td>
					<th scope="row">참석인원</th>
					<td><%=objRs("P_COUNT")%>명</td>
				</tr>
				<tr>
					<th scope="row">전화번호</th>
					<td><%=FnSetNum(objRs("TELNUM"))%></td>
					<th scope="row">FAX</th>
					<td><%=FnSetNum(objRs("FAXNUM"))%></td>
				</tr>
				<tr>
					<th scope="row">휴대폰번호</th>
					<td><%=FnSetNum(objRs("MOBILENUM"))%></td>
					<th scope="row">이메일</th>
					<td><%=FnSetNum(objRs("EMAIL"))%></td>
				</tr>
				<tr>
					<th scope="row">요청사항</th>
					<td colspan="3"><%=strRequirement%></td>
				</tr>
				<tr>
					<th scope="row">예약일</th>
					<td colspan="3"><%=FnFormatDateTime(objRs("REG_DT"))%></td>
				</tr>
				<tr>
					<th scope="row">폴리콤(3차통화)</th>
					<td><%=strPolycom%></td>
					<th scope="row">외부기자재 반입</th>
					<td><%=strEtc%></td>
				</tr>
                <tr>
                    <th scope="row">촬영장비반입</th>
                    <td colspan="3"><%=strFilEquipment%></td>
                </tr>
				<tr>
					<th scope="row">기본요금</th>
					<td><%=strOriAmount%> (할인율 <%=objRs("DISCOUNT")%>% 적용)</td>
					<th scope="row">무료 이용시간</th>
					<td><%=objRs("FREE_TIME")%>시간</td>
				</tr>
				<% If objRs("RESERVE_STATUS") <> "C" Then %>
					<tr>
						<th scope="row">최종금액</th>
						<td colspan="3"><%=strTotalAmount%> (VAT 별도)</td>
					</tr>
				<% Else %>
					<tr>
						<th scope="row">최종금액</th>
						<td><%=strCancelAmount%></td>
						<th scope="row">예약 취소일</th>
						<td><%=FnFormatDateTime(objRs("CAN_DT"))%></td>
					</tr>
				<% End If %>
			</tbody>
		</table>
	</div>

	<p class="listDot">폴리콤(3자 통화) 사용 시 비용은 통화량에 확인 후 별도 안내해 드립니다.</p>

	<% If objRs("RESERVE_STATUS") = "R" Then %>
		<div class="notiBox">
			<h3 class="tit">예약 취소 시 유의사항</h3>
			<div class="gBox">
				<ul class="listDot">
					<li>하루 전에 예약한 회의실은 취소 불가</li>
					<li>예약 취소로 인한 패널티 위약금은 무상시간을 우선 차감</li>
					<li>취소 예약금은 세금계산서 발행되지 않음</li>
				</ul>
				<dl class="listDot">
					<dt>위약금 규정</dt>
					<dd>30일전까지 : 100% 환급</dd>
					<dd>29일 ~ 15일 : 총 결제금액의 50%</dd>
					<dd>14일 ~ 이용당일  : 총결제금액의 100% </dd>
				</dl><br/>
				<ul style="font-size:14px">
					<li>위약금은 결제금에 반환함을 원칙으로 합니다. 현금 입금의 경우 최대 30일이 소요될 수 있습니다.</li>
					<li>카드결제 승인 취소 및 반환의 경우, 결재시 카드영수증을 제출하여 주셔야 하며, 카드 취소는 카드사의 사정에 따라 10일이 소요될 수 있습니다.</li>
					<li>임대인은 불가항력 또는 국가시책 변경에 의해 계약이 해제되는 경우에는 임차인에게 임대료 반환 이외의 손해배상금은 지급되지않습니다.</li>
				</ul>
			</div>
		</div>
	<% End If %>

	<div class="btnWrap">
		<% If objRs("RESERVE_STATUS") = "R" Then %>
			<button  class="nbtnW" onClick="fnGoPrint();">인쇄</button>
			<button  class="nbtnW" onClick="fnCancel('<%=reserve_no%>');">예약 취소</button>
			<button  class="nbtnW" onClick="fnGoEdit();" style="display:none">수정하기</button>
        <% ElseIf objRs("RESERVE_STATUS") = "Y" Then %>
            <button  class="nbtnW" onClick="fnGoPrint();">인쇄</button>
		<% End If %>
		<button  class="nbtnG" onClick="fnGoList();">확인</button>
	</div>
</div>
<!--#include file = "index_footer.asp" -->
