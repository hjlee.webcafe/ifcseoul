<!--#include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%
'Login Check
Dim strUserId, strUserName, strUserAuth, loginFlag, re_url
strUserId = Session("F_UID")
strUserName = Session("F_USER_NAME")
strUserAuth = ""
loginFlag = True
re_url = Request("re_url")

If strUserId = "" OR strUserName = "" Then
	loginFlag = false
Else
	Dim objConn, objRs, arrRs, strQuery

	strQuery = "select * from TBL_USER_INFO where UID = '"& strUserId &"' and USER_NAME = '"& strUserName &"' AND USE_FLAG='Y'"

	set objConn = OpenDBConnection()
	set objRs = SendQuery(objConn,strQuery)

	If objRs.EOF Then
		loginFlag = False
	Else
		strUserAuth = objRs("AUTHORITY")
	End If

	objRs.Close
End If

If loginFlag Then
	If re_url&"" <> "" Then
		If re_url&"" = "reservation" Then
			Response.Redirect "reservation.asp"
		ElseIf re_url&"" = "reservation_list" Then
			Response.Redirect = "reservation_list.asp"
		ElseIf re_url&"" = "user_view" Then
			Response.Redirect = "user_view.asp"
		End If
	Else
		Response.Redirect "forum_info.asp"
	End If
End If

%>
<script type="text/javascript" src="js/user_script.js?v=1"></script>
<div class="visualBox">
	<!-- <span><img src="../../img/com/visual_login.png" alt="" /></span> -->
	<div class="visualTxt">
		<span class="txt01">The Forum at IFC</span>
		<span class="txt02">
			The Forum at IFC는 국제금융센터의 명성에 맞는 첨단 국제회의 시설을 제공하며<br />
			입주사 여러분의 특별한 경험을 통하여 비즈니스 가치와 품격을 높여줍니다.
		</span>
	</div>
</div>
<div class="contBody">
	<div class="loginBox">
		<ul>
			<li><input type="text" id="user_id" title="아이디" placeholder="아이디" maxlength="30" style="ime-mode:disabled" onkeyup="this.value=this.value.replace(/[^A-Za-z0-9]/g,'');" /></li>
			<li><input type="password" id="user_pw" title="비밀번호" placeholder="비밀번호" maxlength="16" /></li>
			<li><button class="nbtnG" id="login_btn">로그인</button></li>
			<input type="hidden" id="hdn_re_url" value="<%=re_url%>" />
		</ul>
		<div class="loginCopy">
		    <span class="logTit">The Forum at IFC는 입주사 전용시설로서, 예약을 위해서는 로그인이 필요합니다.</span>
    		<span class="logTit"> ※ 회원가입 및 ID/PW 관련 문의사항</span>
			<span class="logCont">02-6137-2100</span>
		</div>
	</div>
</div>
<!--#include file = "index_footer.asp" -->

<script type="text/javascript">
$('.pop-layer').find('a.btn-layerClose').click(function(){
  $('.pop-layer').fadeOut(); // 닫기 버튼을 클릭하면 레이어가 닫힌다.
  return false;
});
</script>
