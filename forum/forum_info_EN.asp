<!--#include file = "default_control.asp" -->
<%
Dim strUserId, strUserName, strUserAuth

strUserId = Session("F_UID")
strUserName = Session("F_USER_NAME")
strUserAuth = Session("F_AUTHORITY")

If Request("current_type")&"" = "en" Then
	response.redirect = "forum_info_EN.asp"
End If
%>
<!--#include file = "index_header_EN.asp" -->
<script type="text/javascript">
	$(document).ready(function() {
		var introV01, introV02;
		$('.tabType02 button, .tabType02 a').on('click', function() {
			var $this = $(this);
			var $parent = $this.parent();
			var $tabCont = $('.tabCont');
			var $idx = $parent.index();

			if(!$parent.hasClass('_active')) {
				$('.tabType02 li').removeClass('_active');
				$parent.addClass('_active');
				$tabCont.hide().eq($idx).show();
			}

			if($idx == 0) {
				if(!introV01) {//console.log('introV01');
					if($('#introV01 ul li').length > 1) {
						$('#introV01 .controllBox').show();
						introV01 = new swipeSlidFn('#introV01', {
							pageNav : true
							, dirNav : true
						});
						introV01.update();
					}
				}
			}else if($idx == 1) {
				if(!introV02) {//console.log('introV02');
					if($('#introV02 ul li').length > 1) {
						$('#introV02 .controllBox').show();
						introV02 = new swipeSlidFn('#introV02', {
							pageNav : true
							, dirNav : true
						});
						introV02.update();
					}
				}


			}
		});

		if($('#introV01 ul li').length > 1) {
			$('#introV01 .controllBox').show();
			introV01 = new swipeSlidFn('#introV01', {
				pageNav : true
				, dirNav : true
			});
		}

		fnIFCHallChg(1);
	});

	function fnIFCHallChg(idx) {
		$("button[id*=btn_hall]").removeClass("on");
		$("tr[id*=tr_hall]").hide();

		$("#btn_hall" + idx).addClass("on");
		$("#tr_hall" + idx).show();
	}

	function fnAmountPop() {
		window.open("pop_amount.asp","pop_amount","direction=no, location=no, menubar=no, scrollbars=no, status=no, toolbar=no, resizeble=no, width=894, height=707");
	}

	function fnReservation() {
		location.href = "reservation.asp"
	}
</script>
<div class="visualBox">
	<!-- <span><img src="img/com/visual_login.png" alt="" /></span> -->
	<div class="visualTxt">
		<span class="txt01">The Forum at IFC</span>
		<span class="txt02">
			The Forum at IFC offers a meeting space and state-of-the-art audiovisual technology reflecting<br />
			the reputation of the International Financial Center Seoul that provides tenants with real value and service.
		</span>
	</div>
</div>

<div class="contBody">
	<div class="pTitle">
		<h2>Customized boardroom and multi-purpose conference room for user</h2>
		<span class="titCopy02">The Forum provides a profesional atmosphere for your executive training, town hall meetings, and board meetings with the facility providing customizable meeting room sizes to be tailored to audience size.</span>
	</div>

	<ul class="mroomcInfo">
		<li class="info01">
			<strong>Location</strong>
			<span>Two IFC 3F</span>
		</li>
		<li class="info02">
			<strong>Contact</strong>
			<span>02) 6137 - 2100 </span>
		</li>
		<li class="info03">
			<strong>E-mail</strong>
			<span>forum@ifcseoul.com</span>
		</li>
	</ul>

	<ul class="tabType02">
		<li class="_active"><button type="button">Brookfield Hall</button></li>
		<li><button type="button">IFC Hall</button></li>
	</ul>

	<div class="tabCont" style="display:block;">
		<div id="introV01" class="rVisualWrap">
			<ul class="swiper-wrapper">
				<li class="swiper-slide"><span><img src="img/com/intro_visual01_1.jpg" alt="" /></span></li>
				<li class="swiper-slide"><span><img src="img/com/intro_visual01_2.png" alt="" /></span></li>
			</ul>
			<div class="controllBox">
				<button type="button" class="swipePrev"><em>이전</em></button>
				<span class="navBox"></span>
				<button type="button" class="swipeNext"><em>다음</em></button>
			</div>
		</div>

		<div class="pTitle">
			<h3 class="ptit">Brookfield Hall</h3>
			<span class="titCopy02">An auditorium configuration provides modern audiovisual equipment, and the perfect venue for important executive and client meetings.<br />Tenants can maximize their impact on critical presentations and project introductions through the use of this most advanced audiovisual equipment.</span>
		</div>

		<div class="blitBox v02">
			<ul>
				<li>Simultaneous interpretation system which optimizes international and global standard conferences.</li>
				<li>Simultaneous interpretation room, and VIP waiting room.</li>
				<li>Video Conference System.</li>
				<li>State-of-the-art LED projector screen (180 ") with individual microphones</li>
				<li>The Latest Multimedia Video Call System</li>
				<li>Hi-Speed Broadband Wi-Fi  Access</li>
				<li>Suitable for executive seminars or client meetings with a capacity of 47 people</li>
			</ul>
		</div>

		<div class="tblStyle5">
			<table>
				<caption>예약/이용 현황</caption>
				<colgroup>
					<col style="width:15%;">
					<col style="width:12%;">
					<col style="width:15%;">
					<col style="width:auto;">
					<col style="width:16%;">
					<col style="width:15%;">
				</colgroup>
				<thead>
					<tr>
						<th scope="col">Area</th>
						<th scope="col">Height</th>
						<th scope="col">Banner capacity size</th>
						<th scope="col">Provided equipment</th>
						<th scope="col">Seat</th>
						<th scope="col">Price</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>186.89sqm<br />(56.53 pyeong)</td>
						<td>6.8m</td>
						<td>5m in width</td>
						<td>Projector<br />MIC</td>
						<td>
							<ul>
								<li>47</li>
							</ul>
						</td>
						<td><!-- <a href="#?w=840" class="nbtnG03 jsbtnLyp" rel="layerPop01" style="min-width:108px;height:38px;line-height:38px;"> -->Check on reservation<!-- </a> --></td>
					</tr>
					<!-- <tr>
						<td colspan="7"><p class="noData">검색결과가 존재하지 않습니다.</p></td>
					</tr> -->
				</tbody>
			</table>
		</div>
	</div>

	<div class="tabCont">
		<div id="introV02" class="rVisualWrap">
			<ul class="swiper-wrapper">
				<li class="swiper-slide"><span><img src="img/com/intro_visual02_1.jpg" alt="" /></span></li>
				<li class="swiper-slide"><span><img src="img/com/intro_visual02_2.png" alt="" /></span></li>
			</ul>
			<div class="controllBox">
				<button type="button" class="swipePrev"><em>이전</em></button>
				<span class="navBox"></span>
				<button type="button" class="swipeNext"><em>다음</em></button>
			</div>
		</div>

		<div class="pTitle">
			<h3 class="ptit">IFC Hall</h3>
			<span class="titCopy02">IFC Hall is a flexible conference room space that can be tailored to audience size and conference prupose, and offering space for 40 to 240 people.<br />From small meetings to corporate seminars, IFC Hall enhances the quality of the meeting and event by providing minimalist interiors and cutting-edge multimedia to assist with presentations and audience impact.</span>
		</div>

		<div class="blitBox v02">
			<ul>
				<li>Includes state-of-the-art LED wide screen (200 ") and individual microphones in rooms 301, 302, 303.</li>
				<li>State-of-the-art large 230 "LED TV available in room 304.</li>
				<li>Customizable AV system auto-adjustment panels.</li>
				<li>Broadband, Wi-Fi Access.</li>
			</ul>
		</div>

		<div class="tblTopBx">
			<ul class="roomNum">
				<li><button type="button" id="btn_hall1" onClick="fnIFCHallChg(1);"># 301</button></li>
				<li><button type="button" id="btn_hall2" onClick="fnIFCHallChg(2);"># 302</button></li>
				<li><button type="button" id="btn_hall3" onClick="fnIFCHallChg(3);"># 303</button></li>
				<li><button type="button" id="btn_hall4" onClick="fnIFCHallChg(4);"># 304</button></li>
			</ul>
			<div class="rBox">
				<a href="#?w=650" class="nbtnG02 jsbtnLyp" rel="layerPop02">Seat Guide</a>
				<!-- <a href="#?w=840" class="nbtnG02 jsbtnLyp" rel="layerPop01">Price list</a> -->
			</div>
		</div>
		<div class="tblStyle5">
			<table>
				<caption>예약/이용 현황</caption>
				<colgroup>
					<col style="width:;">
					<col style="width:11%;">
					<col style="width:10%">
					<col style="width:12%">
					<col style="width:10%">
					<col style="width:12%">
					<col style="width:14%">
				</colgroup>
				<thead>
					<tr>
						<th scope="col">Location</th>
						<th scope="col">Area</th>
						<th scope="col">Height</th>
						<th scope="col">Banner capacity size</th>
						<th scope="col">Screen</th>
						<th scope="col">Provided equipment</th>
						<th scope="col">Seat</th>
					</tr>
				</thead>
				<tbody>
					<tr id="tr_hall1" style="display:none">
						<td><img src="img/com/blueprint01.jpg" alt="IFC Hall 301호 설계도" /></td>
						<td>113.96sqm<br />(34.47 pyeong)</td>
						<td>6.8m</td>
						<td>4.5m in width</td>
						<td>200’’</td>
						<td>Projector<br />MIC</td>
						<td>
							<ul>
								<li>School 40seats</li>
								<li>Theatre60seats</li>
								<li>Ushape24seats</li>
							</ul>
						</td>
					</tr>
					<tr id="tr_hall2" style="display:none">
						<td><img src="img/com/blueprint02.jpg" alt="IFC Hall 302호 설계도" /></td>
						<td>89.59sqm<br />(27.10 pyeong)</td>
						<td>6.8m</td>
						<td>4.5m in width</td>
						<td>200’’</td>
						<td>Projector<br />MIC</td>
						<td>
							<ul>
								<li>School 40seats</li>
								<li>Theatre60seats</li>
								<li>Ushape24seats</li>
							</ul>
						</td>
					</tr>
					<tr id="tr_hall3" style="display:none">
						<td><img src="img/com/blueprint03.jpg" alt="IFC Hall 303호 설계도" /></td>
						<td>110.39sqm<br />(33.39 pyeong)</td>
						<td>6.8m</td>
						<td>4.5m in width</td>
						<td>200’’</td>
						<td>Projector<br />MIC</td>
						<td>
							<ul>
								<li>School 40seats</li>
								<li>Theatre60seats</li>
								<li>Ushape24seats</li>
							</ul>
						</td>
					</tr>
					<tr id="tr_hall4" style="display:none">
						<td><img src="img/com/blueprint04.jpg" alt="IFC Hall 304호 설계도" /></td>
						<td>88.21sqm<br />(26.68 pyeong)</td>
						<td>6.8m</td>
						<td>4.8m in width</td>
						<td>230’’ (LED)</td>
						<td>Projector<br />MIC</td>
						<td>
							<ul>
								<li>School 24seats</li>
								<li>Theatre30seats</li>
								<li>Ushape20seats</li>
							</ul>
						</td>
					</tr>
					<!-- <tr>
						<td colspan="7"><p class="noData">검색결과가 존재하지 않습니다.</p></td>
					</tr> -->
				</tbody>
			</table>
		</div>
	</div>

	<div class="btnWrap">
		<button class="nbtnG" onClick="fnReservation();">Reservation</button>
	</div>
</div>

<div id="layerPop01" class="lyPopWrap bw">
	<div class="lyPopBody">
		<strong class="pTit">Price List</strong>
		<div class="lyPopContWrap scrollH">
			<div class="stitWrap">
				<strong class="titS">Brookfield Hall</strong>
			</div>
			<div class="tblStyle4">
				<table>
					<caption>Brookfield Hall Price</caption>
					<colgroup>
						<col style="width:20%"/>
						<col style="width:20%"/>
						<col style="width:20%"/>
						<col style="width:20%"/>
						<col style="width:20%"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col"></th>
							<th scope="col">2Hour</th>
							<th scope="col">4Hour</th>
							<th scope="col">6Hour</th>
							<th scope="col">All day</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">Price</th>
							<td>KRW 480,000</td>
							<td>KRW 768,000</td>
							<td>KRW 1,008,000</td>
							<td>KRW 1,152,000</td>
						</tr>
						<tr>
							<th scope="row">Discount rate</th>
							<td>0%</td>
							<td>20%</td>
							<td>30%</td>
							<td>40%</td>
						</tr>
					</tbody>
				</table>
			</div>
			<ul class="listDot" style="margin:17px 0 0;">
				<li>Price/1Hour : KRW 240,000</li>
				<li>Please contact us if you are bringing in external equipment.</li>
				<li>You will be charged for using Polycom (3-way call).</li>
				<li>Information desk : 02-6137-2100</li>
			</ul>
			<div class="stitWrap">
				<strong class="titS">IFC Hall</strong>
			</div>
			<div class="tblStyle4">
				<table>
					<caption>IFC Hall Price</caption>
					<colgroup>
						<col style="width:*%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col"></th>
							<th scope="col"></th>
							<th scope="col">2Hour</th>
							<th scope="col">4Hour</th>
							<th scope="col">6Hour</th>
							<th scope="col">All day</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row" rowspan="2" class="thBg">1EA</th>
							<th scope="row">Price</th>
							<td>KRW 240,000</td>
							<td>KRW 384,000</td>
							<td>KRW 540,000</td>
							<td>KRW 624,000</td>
						</tr>
						<tr>
							<th scope="row">Discount rate</th>
							<td>0%</td>
							<td>20%</td>
							<td>25%</td>
							<td>35%</td>
						</tr>
						<tr>
							<th scope="row" rowspan="2" class="thBg">2EA</th>
							<th scope="row">Price</th>
							<td>KRW 456,000</td>
							<td>KRW 720,000</td>
							<td>KRW 1,008,000</td>
							<td>KRW 1,152,000</td>
						</tr>
						<tr>
							<th scope="row">Discount rate</th>
							<td>5%</td>
							<td>25%</td>
							<td>30%</td>
							<td>40%</td>
						</tr>
						<tr>
							<th scope="row" rowspan="2" class="thBg">3EA</th>
							<th scope="row">Price</th>
							<td>KRW 648,000</td>
							<td>KRW 1,008,000</td>
							<td>KRW 1,404,000</td>
							<td>KRW 1,584,000</td>
						</tr>
						<tr>
							<th scope="row">Discount Rate</th>
							<td>10%</td>
							<td>30%</td>
							<td>35%</td>
							<td>45%</td>
						</tr>
						<tr>
							<th scope="row" rowspan="2" class="thBg">4EA</th>
							<th scope="row">Price</th>
							<td>KRW 816,000</td>
							<td>KRW 1,248,000</td>
							<td>KRW 1,728,000</td>
							<td>KRW 1,920,000</td>
						</tr>
						<tr>
							<th scope="row">Discount rate</th>
							<td>15%</td>
							<td>35%</td>
							<td>40%</td>
							<td>50%</td>
						</tr>
					</tbody>
				</table>
			</div>
			<ul class="listDot" style="margin:17px 0 0;">
				<li>Price/1Hour : KRW 120,000</li>
				<li>Please contact us if you are bringing in external equipment.</li>
				<li>You will be charged for using Polycom (3-way call).</li>
				<li>Information desk : 02-6137-2100</li>
			</ul>
		</div>
		<div class="btnWrap">
			<button class="nbtnG04 jsPopClose">Close</button>
		</div>
	</div>
	<a href="#none" class="popCls jsPopClose">Close</a>
</div>

<div id="layerPop02" class="lyPopWrap bw">
	<div class="lyPopBody">
		<strong class="pTit">Seat Guide</strong>
		<div class="lyPopContWrap scrollH">
			<div class="seatGuide">
				<ul>
					<li>
						<img src="img/com/seat_guide01.jpg" alt="theatre" />
						<span> Theatre</span>
					</li>
					<li>
						<img src="img/com/seat_guide02.jpg" alt="School" />
						<span> School</span>
					</li>
					<li>
						<img src="img/com/seat_guide03.jpg" alt="Ushape" />
						<span>Ushape</span>
					</li>
				</ul>

				<p>Depending on the size and nature of the meeting, various seating arrangements are possible.<br />IFC Hall is offering space for 40 to 240 people.</p>
			</div>
		</div>
		<div class="btnWrap">
			<button class="nbtnG04 jsPopClose">Close</button>
		</div>
	</div>
	<a href="#none" class="popCls jsPopClose">Close</a>
</div>
<!--#include file = "index_footer_EN.asp" -->
