<!--#include file = "default_control.asp" -->
<%
re_url = "reservation_list"
%>
<!--#include file = "session_check.asp" -->
<!--#include file = "index_header.asp" -->
<%
Dim countQuery, selectQuery, num_count, list_array, page_result, t_page, query_where

strNOW = Left(NOW, 10)

start_date = Left(DateAdd("m", -1, NOW), 10)
end_date = Left(NOW, 10)
If Request("start_date")&"" <> "" Then
	start_date = Request("start_date")
End If
If Request("end_date")&"" <> "" Then
	end_date = Request("end_date")
End If

' UPCOMMING
selectQuery = " SELECT *, (SELECT MIN(CONVERT(INT,RESERVE_TIME)) FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MIN_TIME, (SELECT MAX(CONVERT(INT,RESERVE_TIME))+1 FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MAX_TIME FROM TBL_RESERVATION_INFO RI WHERE UID = '" & strUserId & "' AND RESERVE_DT > '" & strNOW & "' AND RESERVE_STATUS = 'R' ORDER BY RESERVE_DT ASC "

set objURs = SendQuery(objConn,selectQuery)

' SUMMARY
selectQuery = " SELECT	(SELECT COUNT(*) FROM TBL_RESERVATION_INFO WHERE UID = '" & strUserId & "' AND RESERVE_STATUS <> 'C' ) AS RESERVE_CNT, "
selectQuery = selectQuery & " (SELECT COUNT(*) FROM TBL_RESERVATION_INFO WHERE UID = '" & strUserId & "' AND RESERVE_STATUS = 'Y' ) AS COM_CNT, "
selectQuery = selectQuery & " (SELECT COUNT(*) FROM TBL_RESERVATION_INFO WHERE UID = '" & strUserId & "' AND RESERVE_STATUS = 'C' ) AS CAN_CNT, "
selectQuery = selectQuery & " (SELECT ISNULL(SUM(T_QTY),0) FROM TBL_RESERVATION_INFO WHERE UID = '" & strUserId & "' AND RESERVE_STATUS = 'Y' ) AS USE_TIME, "
selectQuery = selectQuery & " (SELECT USE_FREE_TIME FROM TBL_USER_INFO WHERE UID = '" & strUserId & "') AS USE_FREE_TIME "

set objSRs = SendQuery(objConn,selectQuery)

// search string
query_where = ""
If Request("search_text")&"" <> "" Then
	query_where = query_where & " and " & Request("search_type") & " Like '%" & GetTag2Text(Request("search_text")) & "%' "
End If
If Request("room_type")&"" <> "" Then
	query_where = query_where & " and LEFT(RESERVE_NO,1) = '" & Request("room_type") & "' "
End If
If Request("reserve_status")&"" <> "" Then
	query_where = query_where & " and RESERVE_STATUS = '" & Request("reserve_status") & "' "
End If
If start_date <> "" And end_date <> "" Then
	query_where = query_where & " and RESERVE_DT BETWEEN '" & Replace(start_date,"-","") & "' AND '" & Replace(end_date,"-","") & " 23:59:59' "
End If

// counting list
countQuery = "SELECT COUNT(*) AS COUNT FROM dbo.TBL_RESERVATION_INFO WHERE UID = '" & strUserId & "' "& query_where

set objRs = SendQuery(objConn,countQuery)
total_count = objRs("count")
num_count = total_count - (page - 1) * page_limit
set objRs = Nothing

// get list
selectQuery = " SELECT TOP " & page_limit & " RESERVE_NO, ROOM_NAMES, RESERVE_DT, (SELECT MIN(CONVERT(INT,RESERVE_TIME)) FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MIN_TIME, (SELECT MAX(CONVERT(INT,RESERVE_TIME))+1 FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MAX_TIME, T_QTY, FREE_TIME, SUBJECT, RESERVE_NAME, USER_NAME, TOTAL_AMOUNT, REG_DT, RESERVE_STATUS, PAY_STATUS, CAN_PAY_STATUS, CAN_AMOUNT, CHK_STATUS, RESERVE_TIMES "
selectQuery = selectQuery & " FROM dbo.TBL_RESERVATION_INFO RI WHERE UID = '" & strUserId & "' AND RESERVE_ID NOT IN (SELECT TOP " & current_count & " RESERVE_ID FROM dbo.TBL_RESERVATION_INFO WHERE UID = '" & strUserId & "' "& query_where &" ORDER BY RESERVE_DT ASC) "& query_where & " ORDER BY RESERVE_DT ASC "

set objRs = SendQuery(objConn,selectQuery)

'page_create(p_total_count, p_page, p_page_limit, p_page_length)
t_page = page
page_result = page_create(total_count, t_page, page_limit, page_length)
%>
<script type="text/javascript">
	$(document).ready(function() {
		var calSetDate = new Date();
		var calSetY = calSetDate.getFullYear();
		var calSetM = calSetDate.getMonth();
		var calSetD = calSetDate.getDate();

		$("#datepicker01" ).datepicker({
			dateFormat: 'yy-mm-dd',
			monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
			monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
			dayNames: ['SUN','MON','TUE','WED','THU','FRI','SAT'],
			dayNamesShort: ['SUN','MON','TUE','WED','THU','FRI','SAT'],
			dayNamesMin: ['SUN','MON','TUE','WED','THU','FRI','SAT'],
			showMonthAfterYear: true,
			yearSuffix: '년',
			//maxDate : new Date(calSetY, calSetM, calSetD)
		});
		$("#datepicker02" ).datepicker({
			dateFormat: 'yy-mm-dd',
			monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
			monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
			dayNames: ['SUN','MON','TUE','WED','THU','FRI','SAT'],
			dayNamesShort: ['SUN','MON','TUE','WED','THU','FRI','SAT'],
			dayNamesMin: ['SUN','MON','TUE','WED','THU','FRI','SAT'],
			showMonthAfterYear: true,
			yearSuffix: '년',
			//maxDate : new Date(calSetY, calSetM, calSetD)
		});

		<% If start_date <> "" Then %>
			$("#datepicker01").val($.datepicker.formatDate('yy-mm-dd', new Date('<%=start_date%>')));
		<% End If %>
		<% If end_date <> "" Then %>
			$("#datepicker02").val($.datepicker.formatDate('yy-mm-dd', new Date('<%=end_date%>')));
		<% End If %>
	});

	function fnSearch() {
		var startDate = $("#datepicker01").val();
		var endDate = $("#datepicker02").val();

		if ((startDate != "" && endDate == "") || (startDate == "" && endDate != "")) {
			alert("예약일 구간을 바르게 설정하여 주십시오.");
			return true;
		}
		if (startDate != "" && endDate != "") {
			var startArray = startDate.split('-');
			var endArray = endDate.split('-');
			var start_date = new Date(startArray[0], startArray[1], startArray[2]);
			var end_date = new Date(endArray[0], endArray[1], endArray[2]);
			if(start_date.getTime() > end_date.getTime()) {
				alert("종료날짜보다 시작날짜가 작아야합니다.");
				return true;
			}
		}

		document.location.href = "reservation_list.asp?search_type=" + $("#search_type").val() + "&search_text=" + $("#search_text").val() + "&room_type=" + $("#ROOM_TYPE").val() + "&reserve_status=" + $("#RESERVE_STATUS").val() + "&start_date=" + startDate + "&end_date=" + endDate;
	};

    function fnExcelDownload() {
        var startDate = $("#datepicker01").val();
        var endDate = $("#datepicker02").val();

        if ((startDate != "" && endDate == "") || (startDate == "" && endDate != "")) {
            alert("등록일 구간을 바르게 설정하여 주십시오.");
            return true;
        }
        if (startDate != "" && endDate != "") {
            var startArray = startDate.split('-');
            var endArray = endDate.split('-');
            var start_date = new Date(startArray[0], startArray[1], startArray[2]);
            var end_date = new Date(endArray[0], endArray[1], endArray[2]);
            if(start_date.getTime() > end_date.getTime()) {
                alert("종료날짜보다 시작날짜가 작아야합니다.");
                return true;
            }
        }

        parent.window.open("reservation_list_xls.asp?search_type=" + $("#search_type").val() + "&search_text=" + escape($("#search_text").val()) + "&room_type=" + $("#ROOM_TYPE").val() + "&reserve_status=" + $("#RESERVE_STATUS").val() + "&start_date=" + startDate + "&end_date=" + endDate);
    }
</script>
<div class="contBody">
	<div class="pTitle"><h2>회의실 예약내역</h2></div>
	<div class="pTitle"><h3>UPCOMMING</h3></div>
	<div class="reservationList">
		<ul>
			<% if objURs.EOF Then %>
				<li><p class="noData">이용 예정 중인 예약이 없습니다.</p></li>
			<%
			Else
				For i = 0 to objURs.RecordCount - 1
					strRoomType = Left(objURs("RESERVE_NO"), 1)
					strReserveDt = objURs("RESERVE_DT")

                    '예약시간표시
                    arrReserveTime = Split(objURs("RESERVE_TIMES"), ",")
                    For k=0 To UBound(arrReserveTime)
                        strLastTime = CInt(arrReserveTime(k)) + 1
                    Next

					If Len(strReserveDt) = 8 Then
						strReserveDt = Mid(strReserveDt,1,4) & "년 " & Mid(strReserveDt,5,2) & "월 " & Mid(strReserveDt,7,2) & "일 "
					End If
			%>
					<li>
						<span class="mgBx"><img src="img/temp/@img_300x170_<%=strRoomType%>.jpg" alt="회의실"/></span>
						<dl>
							<dt><a href="javascript:;" onClick="location.href='reservation_list_view.asp?reserve_no=<%=objURs("RESERVE_NO")%>';"><%=objURs("SUBJECT")%></a></dt>
                            <dd><em>예약자명</em><span><%=objURs("RESERVE_NAME")%></span></dd>
							<dd><em>예약날짜</em><span><%=strReserveDt%> <%=objURs("MIN_TIME")%>:00 ~ <%=strLastTime%>:00</span></dd>
							<dd><em>회의실</em><span><%=objURs("ROOM_NAMES")%></span></dd>
							<dd><em>참석인원</em><span><%=objURs("P_COUNT")%>명</span></dd>
						</dl>
					</li>
			<%
					objURs.MoveNext
				Next
			End If
			%>
		</ul>
	</div>
	<div class="pTitle"><h3>SUMMARY</h3></div>
	<div class="tblStyle4" style="margin-bottom:20px;">
		<table>
			<caption>SUMMARY</caption>
			<colgroup>
				<col style="width:25%"/><col style="width:25%"/><col style="width:25%"/><col style="width:25%"/>
			</colgroup>
			<thead>
				<tr>
					<th scope="col">총 예약 건수</th>
					<th scope="col">총 이용 건수</th>
					<th scope="col">총 취소 건수</th>
					<th scope="col">총 이용 시간</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><%=objSRs("RESERVE_CNT")%>건</td>
					<td><%=objSRs("COM_CNT")%>건</td>
					<td><%=objSRs("CAN_CNT")%>건</td>
					<td><%=objSRs("USE_TIME")%>시간</td>
				</tr>
			</tbody>
		</table>
	</div>
    <div style="float:right;margin-bottom:50px;width:100%;text-align:right;">
        <a href="reservation_schedule.asp" class="nbtnG02 jsbtnLyp">달력으로보기</a>
    </div>

	<div class="pTitle"><h3>예약/이용 현황</h3></div>
	<div class="inquiryBx">
		<div class="tblStyle3">
			<table>
				<caption>예약정보 입력</caption>
				<colgroup>
					<col style="width:10%"/><col style="width:40%"/><col style="width:11%"/><col style="width:*%"/>
				</colgroup>
				<tbody>
					<tr>
						<th scope="row"><label for="ROOM_TYPE">회의실</label></th>
						<td>
							<select id="ROOM_TYPE" name="ROOM_TYPE" style="width:250px;">
								<option value="">전체</option>
								<option value="B" <%If Request("ROOM_TYPE")&""="B" Then%>selected<%End If%>>브룩필드홀</option>
								<option value="R" <%If Request("ROOM_TYPE")&""="R" Then%>selected<%End If%>>IFC Hall</option>
							</select>
						</td>
						<th scope="row"><label for="RESERVE_STATUS">예약상태</label></th>
						<td>
							<select id="RESERVE_STATUS" name="RESERVE_STATUS" style="width:272px;">
								<option value="">전체</option>
								<option value="R" <%If Request("RESERVE_STATUS")&""="R" Then%>selected<%End If%>>예약완료</option>
								<option value="C" <%If Request("RESERVE_STATUS")&""="C" Then%>selected<%End If%>>예약취소</option>
								<option value="Y" <%If Request("RESERVE_STATUS")&""="Y" Then%>selected<%End If%>>이용완료</option>
							</select>
						</td>
					</tr>
					<tr>
						<th scope="row"><label for="search_type">검색어</label></th>
						<td colspan="3">
							<div class="inpBox">
								<select id="search_type" name="search_type" style="width:250px;">
									<option value="RESERVE_NO" <%If Request("search_type")&""="RESERVE_NO" Then%>selected<%End If%>>예약번호</option>
									<option value="SUBJECT" <%If Request("search_type")&""="SUBJECT" Then%>selected<%End If%>>회의명</option>
									<option value="RESERVE_NAME" <%If Request("search_type")&""="RESERVE_NAME" Then%>selected<%End If%>>예약자</option>
								</select>
								<input type="text" id="search_text" name="search_text" value="<%=Request("search_text")%>" placeholder="검색어" onKeydown="javascript:if(event.keyCode == 13){fnSearch();return false; }" style="width:380px;" />
							</div>
						</td>
					</tr>
					<tr>
						<th scope="row"><label for="inp03">사용일</label></th>
						<td colspan="3">
							<div class="inpCalen">
								<span><input type="text" id="datepicker01"></span>
								<span class="ds">~</span>
								<span><input type="text" id="datepicker02"></span>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<button class="nbtnG02" onClick="fnSearch();">조회</button>
	</div>
	<div class="tblStyle4" style="margin-bottom:20px;">
		<table>
			<caption>예약/이용 현황</caption>
			<colgroup>
				<col style="width:10%"/>
				<col style="width:12%"/>
				<col style="width:16%"/>
				<col style="width:*%"/>
				<col style="width:9%"/>
				<col style="width:12%"/>
				<col style="width:9%"/>
				<col style="width:9%"/>
			</colgroup>
			<thead>
				<tr>
					<th scope="col">예약번호</th>
					<th scope="col">회의실</th>
					<th scope="col">이용시간</th>
					<th scope="col">무상시간</th>
					<th scope="col">회의명</th>
					<th scope="col">예약자</th>
					<th scope="col">예약일</th>
					<th scope="col">예약상태</th>
					<th scope="col">최종금액</th>
				</tr>
			</thead>
			<tbody>
				<% If objRs.EOF Then %>
					<tr>
						<td colspan="9"><p class="noData">검색결과가 존재하지 않습니다.</p></td>
					</tr>
				<%
				Else
					For k = 0 to objRs.RecordCount - 1
						strReserveDt = objRs("RESERVE_DT")
						If Len(strReserveDt) = 8 Then
							strReserveDt = Mid(strReserveDt,1,4) & "년 " & Mid(strReserveDt,5,2) & "월 " & Mid(strReserveDt,7,2) & "일 "
						End If

                        '예약시간표시
                        arrReserveTime = Split(objRs("RESERVE_TIMES"), ",")
                        For i=0 To UBound(arrReserveTime)
                            strLastTime = CInt(arrReserveTime(i)) + 1
                        Next

						strReserveStatus = ""
						If objRs("RESERVE_STATUS")&"" = "R" Then
							strReserveStatus = "<span class='ptC01'>예약완료</span>"
						ElseIf objRs("RESERVE_STATUS")&"" = "C" Then
							strReserveStatus = "<span class='ptC02'>예약취소</span>"
						ElseIf objRs("RESERVE_STATUS")&"" = "Y" Then
							strReserveStatus = "이용완료"
						End If

						strPayAmount = FnMoneySet(objRs("TOTAL_AMOUNT")) & "원"
						If objRs("RESERVE_STATUS")&"" = "C" Then
							strPayAmount = FnMoneySet(objRs("CAN_AMOUNT")) & "원"
						End If
				%>
						<tr>
							<td><%=objRs("RESERVE_NO")%></td>
							<td><%=objRs("ROOM_NAMES")%></td>
							<td><%=strReserveDt%><br /><%=objRs("MIN_TIME")%>:00~<%=strLastTime%>:00</td>
							<td><%=objRs("FREE_TIME")%></td>
							<td class="txtL"><a href="javascript:;" onClick="location.href='reservation_list_view.asp?reserve_no=<%=objRs("RESERVE_NO")%>';"><%=objRs("SUBJECT")%></td>
							<td><%=objRs("RESERVE_NAME")%></td>
							<td><%=FnFormatDateTime(objRs("REG_DT"))%></td>
							<td><%=strReserveStatus%></td>
							<td><%=strPayAmount%></td>
						</tr>
				<%
						objRs.MoveNext
					Next
				End If
				%>
			</tbody>
		</table>
	</div>
    <div style="float:right;margin-bottom:50px;width:100%;">
        <!-- <a href="#" class="nbtnG02 jsbtnLyp" onClick="fnReservationExcel();">엑셀 다운로드</a> -->
        <input type="button" class="nbtnG02 jsbtnLyp" style="padding:0 30px;cursor:pointer;" value="엑셀 다운로드" onClick="fnExcelDownload();" />
    </div>
	<div class="pagiging">
		<!-- #include file = "page_template.asp" -->
	</div>
</div>

<!--#include file = "index_footer.asp" -->
