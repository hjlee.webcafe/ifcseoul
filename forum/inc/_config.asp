<% @language='vbscript' codepage='65001'%>
<%
Response.ChaRset="UTF-8"
Session.Codepage=65001
' default config file
Dim current_path, path_prefix
' get current real path
current_path = Request.ServerVariables("APPL_PHYSICAL_PATH")
' prefix for include for absolute path
path_prefix = "/"

// set session timeout
Session.Timeout=1440

// for error check
Err.clear
On Error Resume Next

%>

<!-- #include file = "default_function.asp" -->
<!-- #include file = "checkParam.asp" -->
<!-- #include file = "dbconn.asp" -->
<!-- #include file = "JSON_2.0.4.asp" -->