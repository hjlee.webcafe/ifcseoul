<!--#include file = "default_control.asp" -->
<%
re_url = "user_view"
%>
<!--#include file = "session_check.asp" -->
<!--#include file = "index_header.asp" -->
<%
Dim selectQuery, uid
uid = Request.QueryString("uid")

selectQuery = "select * from TBL_USER_INFO where UID = '"& uid &"'"

set objRs = SendQuery(objConn,selectQuery)

If objRs.EOF Then
%>
	<script>
		alert("비정상적인 접속입니다.");
		location.href="../index.asp";
	</script>
<%
End If
%>

<script type="text/javascript">
	function fnGoBack() {
		location.href = "user_view.asp?uid=<%=uid%>";
	}

	function fnPwdChg(objVal) {
		if ($("#ORI_PASSWORD").val() == "") {
			alert("기존 비밀번호를 입력해주세요.");
			$("#ORI_PASSWORD").focus();
			return true;
		}
		if ($("#NEW_PASSWORD").val() == "") {
			alert("신규 비밀번호를 입력해주세요.");
			$("#NEW_PASSWORD").focus();
			return true;
		}
		else {
			if (!chkPwd($.trim($("#NEW_PASSWORD").val()))) {
				alert("신규 비밀번호를 확인하세요.\n\n영문,숫자를 혼합하여 8~16자 이내");
				$("#NEW_PASSWORD").focus();
				return true;
			}
		}

		if ($("#NEW_PASSWORD").val() != $("#CHK_PASSWORD").val()) {
			alert("신규 비밀번호가 일치하지 않습니다.\n\n비밀번호를 확인해주세요.");
			$("#CHK_PASSWORD").focus();
			return true;
		}

		var oriPassword = "";
		var newPassword = "";
		if ($("#NEW_PASSWORD").val() != "") {
			oriPassword = hex_md5($("#ORI_PASSWORD").val());
			newPassword = hex_md5($("#NEW_PASSWORD").val());
		}

		if (!confirm("비밀번호를 수정 하시겠습니까?"))
			return true;

		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/user_proc.asp",
			dataType : "json",
			data : ({
						"mode" : "pwd_edit",
						"UID" : objVal,
						"ORI_PASSWORD" : oriPassword,
						"NEW_PASSWORD" : newPassword,
						"UPD_UID" : objVal
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();
			
				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == -1) {
						alert("기존 비밀번호가 다릅니다.\n\n기존 비밀번호를 확인해주세요.");
						$("#ORI_PASSWORD").focus();
					}
					else if (data.result == 0) {
						alert("비밀번호 변경이 완료되었습니다.");
						get_link = "user_view.asp?uid=" + objVal;//$("#list_link_hidden").val();
						location.href = get_link;
					}
					else {
						alert(error_msg);
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}

	function chkPwd(str) {
		var reg_pwd = /^.*(?=.{8,16})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;

		if (str.length < 8 || str.length > 16) {
			return false;
		}

		if (!reg_pwd.test(str)) {
			return false;
		}
		return true;
	}
</script>

<div class="contBody">
	<div class="pTitle">
		<h2>비밀번호 변경</h2>
		<span class="titCopy">비밀번호 변경을 위해 아래 정보를 입력해주세요.</span>
	</div>

	<div class="tblStyle3 brB0">
		<table>
			<caption>비밀번호 변경</caption>
			<colgroup>
				<col style="width:20%"/>
				<col style="width:*%"/>
			</colgroup>
			<tbody>
				<tr>
					<th scope="row"><label for="ORI_PASSWORD">기존 비밀번호</label></th>
					<td>
						<input type="password" id="ORI_PASSWORD" name="ORI_PASSWORD" style="width:640px;" maxlength="16" />
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="NEW_PASSWORD">신규 비밀번호</label></th>
					<td>
						<input type="password" id="NEW_PASSWORD" name="NEW_PASSWORD" style="width:640px;" maxlength="16" />
					</td>
				</tr>
				<tr>
					<th scope="row"><label for="CHK_PASSWORD">신규 비밀번호 재입력</label></th>
					<td>
						<input type="password" id="CHK_PASSWORD" name="CHK_PASSWORD" style="width:640px;" maxlength="16" />
						<p class="gearNoti">영문/숫자 조합 8자 ~ 16자</p>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="btnWrap">
		<button  class="nbtnW" id="btn_goBack" onClick="fnGoBack();">취소</button>
		<button  class="nbtnG" id="btn_pwdChg" onClick="fnPwdChg('<%=uid%>');">확인</button>
	</div>
</div>
<!--#include file = "index_footer.asp" -->