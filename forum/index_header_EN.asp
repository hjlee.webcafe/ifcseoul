<!DOCTYPE html>
<html xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<!--<meta http-equiv='refresh' content='0; url=https://ifcseoul.com/index.asp'target='_top'>-->
    <title>IFC Seoul</title>
    <link rel="stylesheet" type="text/css" href="css/base.css" />
    <link rel="stylesheet" type="text/css" href="css/base_ko.css" />
    <link rel="stylesheet" type="text/css" href="css/layout.css" />
    <link rel="stylesheet" type="text/css" href="css/common.css" />
    <link rel="stylesheet" type="text/css" href="css/animation.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	<!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script> -->
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.event.drag-1.5.min.js"></script>
    <script type="text/javascript" src="js/jquery.touchSlider.js"></script>
	<script type="text/javascript" src="js/jquery.bxslider.js"></script>
    <script type="text/javascript" src="js/ifc_ui.js"></script>
    <script type="text/javascript" src="js/ifc_ui02.js"></script>
    <script type="text/javascript" src="js/ifc_custom.js"></script>
	<script type="text/javascript" src="js/md5.js"></script>
	<script type="text/javascript" src="js/swiper.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>

	<script type="text/javascript">
	$(document).ready(function() {
		/*var selectYMFn = new selectYM('.selectYM > ul', 5);
		$('.settingBox .infoYM').text(selectYMFn.getYear() + '.' + selectYMFn.getMonth());

		$('.selectYM > ul button').click(function() {
			var $this = $(this),
				  $textYM = $this.parents('.selectYM').find('.textYM');
			var $dataY = $this.data('year'),
				  $month = $(this).text()
				  $textY = parseInt($('.year', $textYM).text());

			if(!$this.parent().hasClass('on')) {
				if($textY != $dataY) {
					$('.year', $textYM).text($dataY);
				}
				$('.month', $textYM).text($month);

				$('li', $this.parents('.selectYM')).removeClass('on');
				$this.parent().addClass('on');

				selectYMFn.chYear($dataY);
				selectYMFn.chMonth(parseInt($month));
				$('.settingBox .infoYM').text(selectYMFn.getYear() + '.' + selectYMFn.getMonth());

				calendarFn.reDrawFn(selectYMFn.getYear(), selectYMFn.getMonth());
			}
		});

		var calendarFn = new calendar('#calendarBox', {
			weekText : ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT']
		});

		$('#calendarBox').on('click', 'td a', function(e) {e.preventDefault();
			var $this = $(this);
			if(!$this.hasClass('on')) {
				$('#calendarBox td').removeClass('on');
				$this.parent().addClass('on');
			}
		});*/
	});
	</script>
    <!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
        <script type="text/javascript" src="js/respond.min.js"></script>
		<script type="text/javascript">
			var ltIE9 = true;
		</script>
    <![endif]-->

</head>
<!--[if lt IE 7]><body class="msie ie6 lt-ie9 lt-ie8 lt-ie7 lt-css3"><![endif]-->
<!--[if IE 7]>   <body class="msie ie7 lt-ie9 lt-ie8 lt-css3"><![endif]-->
<!--[if IE 8]>   <body class="msie ie8 lt-ie9 lt-css3"><![endif]-->
<!--[if IE 9]>   <body class="msie ie9 css3"><![endif]-->
<!--[if gt IE 9]><!-->
<body>
	<div id="index_loading_overlay">
		<div class="index_loading_overlay_img">
			<img src="img/ajax-loader.gif" alt="overlay loading img" />
		</div>
	</div>
<!--<![endif]-->
	<div id="wrap" class="">
		<!-- //snb -->
		<div class="snbShadow"></div>
		<nav id="snb">
			<span class="tit v01">The Forum at IFC<br />Reservation</span>
			<ul class="menuList">
				<li><a href="forum_info_EN.asp">Introduction<span></span></a></li>
				<li><a href="reservation.asp">Reservation<span></span></a></li>
			</ul>
		</nav>
		<!-- //snb -->
		<div id="containerWrap">
		<header>
			<!-- u_skip -->
			<div id="u_skip"></div>
			<!-- //u_skip -->
			<!-- header -->
			<div id="header">
				<div class="container">
					<a href="#none" class="btnMenu">Home</a>
					<h1><a href="/en/MN_00_00.asp"><span>IFC Seoul</span></a></h1>
					<a href="HF_01_00.asp" class="btnMap">Location & Maps</a>
				</div>
				<div id="gnb">
					<ul>
						<li><a href="/en/MN_00_00.asp">Home</a></li>
						<li><a href="/en/HF_01_00.asp">Location & Maps</a></li>
						<li><a href="/en/HF_02_00.asp">Contact</a></li>
						<li><a href="forum_info.asp">Korean</a></li>
						<% If strUserId&"" <> "" Then%>
							<li><a href="logout.asp">Tenant Logout</a></li>
						<% Else %>
							<!-- <li><a href="index.asp">Tenant Login</a></li> -->
						<% End If %>
					</ul>
				</div>
				<div id="lnb">
					<ul>
						<li><a href="/en/IS_01_00.asp">IFC Seoul</a></li>
						<li><a href="/en/BD_01_00.asp">Buildings</a></li>
						<li><a href="/en/NM_01_00.asp">Neighborhood Maps</a></li>
						<li><a href="/en/NE_01_00.asp">News&Events</a></li>
						<li><a href="../visitor/index.asp">Visitor Registration</a></li>
						<li><a href="forum_info_EN.asp">The Forum at IFC Reservation</a></li>
                        <li><a href="../visitor/smartsuites.asp ">The Smart Suites at IFC</a></li>
					</ul>
				</div>
			</div>
			<!-- //header -->
		</header>
		<section>
			<!-- container -->
			<div id="container">
				<!-- lineMap -->
				<div class="lineMapWrap">
					<ul class="lineMap"></ul>
				</div>
				<!-- //lineMap -->
				<div id="content">
