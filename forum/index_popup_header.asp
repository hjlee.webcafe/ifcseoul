<!DOCTYPE html>
<html xml:lang="ko" lang="ko">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <!--<meta http-equiv='refresh' content='0; url=https://ifcseoul.com/index.asp'target='_top'>-->
    <title>IFC Seoul</title>
    <link rel="stylesheet" type="text/css" href="css/base.css" />
    <link rel="stylesheet" type="text/css" href="css/base_ko.css" />
    <link rel="stylesheet" type="text/css" href="css/layout.css" />
    <link rel="stylesheet" type="text/css" href="css/common.css" />
    <link rel="stylesheet" type="text/css" href="css/animation.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />

    <style>
       @page a4sheet {size:21.0cm 29.7cm; }
        #contBody { page:a4sheet; page-break-after: always; }
    </style>
    <style media=print>
        .btnWrap { display:none; }
    </style>

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script> -->
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.event.drag-1.5.min.js"></script>
    <script type="text/javascript" src="js/jquery.touchSlider.js"></script>
    <script type="text/javascript" src="js/jquery.bxslider.js"></script>
    <script type="text/javascript" src="js/ifc_ui.js"></script>
    <script type="text/javascript" src="js/ifc_ui02.js"></script>
    <script type="text/javascript" src="js/ifc_custom.js"></script>
    <script type="text/javascript" src="js/md5.js"></script>
    <script type="text/javascript" src="js/swiper.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        /*var selectYMFn = new selectYM('.selectYM > ul', 5);
        $('.settingBox .infoYM').text(selectYMFn.getYear() + '.' + selectYMFn.getMonth());

        $('.selectYM > ul button').click(function() {
            var $this = $(this),
                  $textYM = $this.parents('.selectYM').find('.textYM');
            var $dataY = $this.data('year'),
                  $month = $(this).text()
                  $textY = parseInt($('.year', $textYM).text());

            if(!$this.parent().hasClass('on')) {
                if($textY != $dataY) {
                    $('.year', $textYM).text($dataY);
                }
                $('.month', $textYM).text($month);

                $('li', $this.parents('.selectYM')).removeClass('on');
                $this.parent().addClass('on');

                selectYMFn.chYear($dataY);
                selectYMFn.chMonth(parseInt($month));
                $('.settingBox .infoYM').text(selectYMFn.getYear() + '.' + selectYMFn.getMonth());

                calendarFn.reDrawFn(selectYMFn.getYear(), selectYMFn.getMonth());
            }
        });

        var calendarFn = new calendar('#calendarBox', {
            weekText : ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT']
        });

        $('#calendarBox').on('click', 'td a', function(e) {e.preventDefault();
            var $this = $(this);
            if(!$this.hasClass('on')) {
                $('#calendarBox td').removeClass('on');
                $this.parent().addClass('on');
            }
        });*/
    });
    </script>
    <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
        <script type="text/javascript" src="js/respond.min.js"></script>
        <script type="text/javascript">
            var ltIE9 = true;
        </script>
    <![endif]-->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-61219190-1"></script>

<!-- Save PDF Library -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-61219190-1');


    function fnGoPrint() {
        var initBody = document.body.innerHTML;
        window.onbeforeprint = function () {
            document.body.innerHTML = document.getElementById("contBody").innerHTML;
        }

        window.onafterprint = function () {
            document.body.innerHTML = initBody;
        }

        window.print();
    }

    function fnSavePDF(reserve_no) {
        html2canvas(document.getElementById("pdf_div"), {
            onrendered: function (canvas) {
                var img = canvas.toDataURL('image/png');
                var doc = new jsPDF();
                doc.addImage(img, 'JPEG', 20, 20, 180, 260);
                doc.save(reserve_no + '.pdf');
            }
        });
    }
</script>

</head>
<!--[if lt IE 7]><body class="msie ie6 lt-ie9 lt-ie8 lt-ie7 lt-css3"><![endif]-->
<!--[if IE 7]>   <body class="msie ie7 lt-ie9 lt-ie8 lt-css3"><![endif]-->
<!--[if IE 8]>   <body class="msie ie8 lt-ie9 lt-css3"><![endif]-->
<!--[if IE 9]>   <body class="msie ie9 css3"><![endif]-->
<!--[if gt IE 9]><!-->
<body>
    <div id="index_loading_overlay">
        <div class="index_loading_overlay_img">
            <img src="img/ajax-loader.gif" alt="overlay loading img" />
        </div>
    </div>
<!--<![endif]-->
    <div id="wrap" class="">
        <div id="containerWrap">
            <section>
                <!-- container -->
                <div id="container">
                    <div id="content" style="margin-left:0;border:0;">
