
				</div>
			</div>
			<!-- //container -->
		</section>
		<!-- footer -->
		<footer>
			<div id="footer">
				<div class="container top">
					<div class="lCon">
						<ul>
							<li><a href="/en/HF_06_00.asp">Real Estate Notice</a></li>
							<li><a href="/en/HF_07_00.asp">Site Map</a></li>
						</ul>
					</div>
					<div class="rCon">
						<!-- select id="family_site">
							<option value="">Family Site</option>
							<option value="http://www.ifcmallseoul.com">IFC Mall</option>
							<option value="http://www.conradseoul.co.kr">CONRAD Seoul</option>
							<option value="">AIG GRE</option>
						</select -->
						<div class="selectList">
							<p><a href="#none">FAMILY SITE</a></p>
							<ul class="hide">
								<li><a href="http://www.ifcmallseoul.com" target="_blank" title="Open New Window">IFC MALL</a></li>
								<li><a href="http://www.conradseoul.co.kr" target="_blank" title="Open New Window">CONRAD Seoul</a></li>
								<li><a href="https://www.brookfield.com/" target="_blank" title="Open New Window">Brookfield</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="container bottom">
					<address>10, Gukjegeumyung-ro, Youngdeungpo-gu, SEOUL, 150-945 KOREA</address>
					<p class="copy">COPYRIGHT &copy; 2014 IFC SEOUL ALL RIGHT RESERVED.</p>
				</div>
			</div>
		</footer>
		<!-- //footer -->
		</div>
	</div><!-- //wrap -->
</body>
</html>
