<!DOCTYPE html>
<html xml:lang="ko" lang="ko">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <title>IFC Seoul</title>
    <link rel="stylesheet" type="text/css" href="css/base_ko.css" />
    <link rel="stylesheet" type="text/css" href="css/layout.css" />
    <link rel="stylesheet" type="text/css" href="css/common.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/animation.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	<!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script> -->
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.event.drag-1.5.min.js"></script>
    <script type="text/javascript" src="js/jquery.touchSlider.js"></script>
	<script type="text/javascript" src="js/jquery.bxslider.js"></script>
    <!-- <script type="text/javascript" src="js/ifc_ui.js"></script> -->
    <script type="text/javascript" src="js/ifc_ui02.js"></script>
    <script type="text/javascript" src="js/ifc_custom.js"></script>

    <!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
        <script type="text/javascript" src="js/respond.min.js"></script>
		<script type="text/javascript">
			var ltIE9 = true;
		</script>
    <![endif]-->

</head>
<!--[if lt IE 7]><body class="msie ie6 lt-ie9 lt-ie8 lt-ie7 lt-css3"><![endif]-->
<!--[if IE 7]>   <body class="msie ie7 lt-ie9 lt-ie8 lt-css3"><![endif]-->
<!--[if IE 8]>   <body class="msie ie8 lt-ie9 lt-css3"><![endif]-->
<!--[if IE 9]>   <body class="msie ie9 css3"><![endif]-->
<!--[if gt IE 9]><!-->
<body>
<!--<![endif]-->
<!-- 개발적용시 styel 속성 삭제 -->
<div id="layerPop01" class="lyPopWrap bw">
	<div class="lyPopBody">
		<strong class="pTit">요금표</strong>
		<div class="lyPopContWrap scrollH">
			<div class="stitWrap">
				<strong class="titS">보드룸</strong>
			</div>
			<div class="tblStyle4">
				<table>
					<caption>보드룸 요금표</caption>
					<colgroup>
						<col style="width:20%"/>
						<col style="width:20%"/>
						<col style="width:20%"/>
						<col style="width:20%"/>
						<col style="width:20%"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col"></th>
							<th scope="col">2시간</th>
							<th scope="col">3시간</th>
							<th scope="col">4시간</th>
							<th scope="col">All day</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">가격</th>
							<td>480,000원</td>
							<td>576,000원</td>
							<td>840,000원</td>
							<td>1,152,000원</td>
						</tr>
						<tr>
							<th scope="row">할인율</th>
							<td>0%</td>
							<td>20%</td>
							<td>30%</td>
							<td>40%</td>
						</tr>
					</tbody>
				</table>
			</div>
			<ul class="listDot" style="margin:17px 0 0;">
				<li>시간별 금액 : 240,000원</li>
				<li>외부기자재 반입하는 경우 관리자에서 확인이 필요하니 별도 문의해주시기 바랍니다.</li>
				<li>폴리콤(3자 통화) 사용 시 별도 비용이 청구될 수 있습니다.</li>
				<li>안내데스크 : 02-6137-XXXX</li>
			</ul>
			<div class="stitWrap">
				<strong class="titS">멀티홀</strong>
			</div>
			<div class="tblStyle4">
				<table>
					<caption>멀티홀 요금표</caption>
					<colgroup>
						<col style="width:*%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col"></th>
							<th scope="col"></th>
							<th scope="col">2시간</th>
							<th scope="col">3시간</th>
							<th scope="col">5시간</th>
							<th scope="col">All day</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row" rowspan="2" class="thBg">1개</th>
							<th scope="row">가격</th>
							<td>240,000원</td>
							<td>288,000원</td>
							<td>450,000원</td>
							<td>624,000원</td>
						</tr>
						<tr>
							<th scope="row">할인율</th>
							<td>0%</td>
							<td>20%</td>
							<td>25%</td>
							<td>35%</td>
						</tr>
						<tr>
							<th scope="row" rowspan="2" class="thBg">2개</th>
							<th scope="row">가격</th>
							<td>456,000원</td>
							<td>540,000원</td>
							<td>840,000원</td>
							<td>1,152,000원</td>
						</tr>
						<tr>
							<th scope="row">할인율</th>
							<td>5%</td>
							<td>25%</td>
							<td>30%</td>
							<td>40%</td>
						</tr>
						<tr>
							<th scope="row" rowspan="2" class="thBg">3개</th>
							<th scope="row">가격</th>
							<td>648,000원</td>
							<td>756,000원</td>
							<td>1,170,000원</td>
							<td>1,584,000원</td>
						</tr>
						<tr>
							<th scope="row">할인율</th>
							<td>10%</td>
							<td>30%</td>
							<td>35%</td>
							<td>45%</td>
						</tr>
						<tr>
							<th scope="row" rowspan="2" class="thBg">4개</th>
							<th scope="row">가격</th>
							<td>816,000원</td>
							<td>936,000원</td>
							<td>1,440,000원</td>
							<td>1,920,000원</td>
						</tr>
						<tr>
							<th scope="row">할인율</th>
							<td>15%</td>
							<td>35%</td>
							<td>40%</td>
							<td>50%</td>
						</tr>
					</tbody>
				</table>
			</div>
			<p class="listDot">시간별 금액 : 120,000원</p>
		</div>
		<div class="btnWrap">
			<button class="nbtnG04 jsPopClose" onClick="self.close();">닫기</button>
		</div>
	</div>
	<a href="javascript:;" onClick="self.close();" class="popCls jsPopClose">Close</a>
</div>
</body>
</html>
