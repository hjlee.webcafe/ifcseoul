<!-- #include file = "inc/_config.asp" -->
<!-- #include file = "inc/page_handling.asp" -->
<%
' remove cache
Response.Expires = -1
Response.ExpiresAbsolute = now() - 1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>

<%
re_url = "reservation_list"
%>
<!--#include file = "session_check.asp" -->
<!--#include file = "index_popup_header.asp" -->
<%
Dim selectQuery, reserve_no
reserve_no = Request.QueryString("reserve_no")

selectQuery = "select (SELECT MIN(CONVERT(INT,RESERVE_TIME)) FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MIN_TIME, (SELECT MAX(CONVERT(INT,RESERVE_TIME))+1 FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MAX_TIME, (SELECT BUSINESS_RE_NO FROM TBL_USER_INFO WHERE UID=RI.UID) AS BUSINESS_RE_NO, * from TBL_RESERVATION_INFO RI where RESERVE_NO = '"& reserve_no &"' AND RI.UID = '" & strUserId & "'"

set objRs = SendQuery(objConn,selectQuery)

'예약상태
strReserveStatus = ""
If objRs("RESERVE_STATUS") = "R" Then
    strReserveStatus = "예약 완료"
ElseIf objRs("RESERVE_STATUS") = "C" Then
    strReserveStatus = "예약 취소"
ElseIf objRs("RESERVE_STATUS") = "Y" Then
    strReserveStatus = "이용 완료"
End If

'사업자등록번호
strBUSINESS_RE_NO = objRs("BUSINESS_RE_NO")
If Len(strBUSINESS_RE_NO) > 7 Then
    strBUSINESS_RE_NO = Left(strBUSINESS_RE_NO, 7) & "*****"
End If

'이용시간
strReserveDt = objRs("RESERVE_DT")
strReserveDt_Temp = ""
strReserveDt_Temp2 = ""
If Len(strReserveDt) = 8 Then
    strReserveDt = Mid(strReserveDt,1,4) & "년 " & Mid(strReserveDt,5,2) & "월 " & Mid(strReserveDt,7,2) & "일 "
    strReserveDt_Temp = Mid(objRs("RESERVE_DT"),1,4) & "-" & Mid(objRs("RESERVE_DT"),5,2) & "-" & Mid(objRs("RESERVE_DT"),7,2)
    strReserveDt_Temp2 = strReserveDt_Temp & " " & objRs("MIN_TIME") & ":00:00"
End If

'부가세 계산
strTotalAmount = FnMoneySet(objRs("TOTAL_AMOUNT"))                                  'VAT별도금액
strAddTax = FnMoneySet(Fix(objRs("TOTAL_AMOUNT")) * 0.1)                            '부가세
strPayAmount = FnMoneySet(objRs("TOTAL_AMOUNT") + (objRs("TOTAL_AMOUNT")*0.1))      '최종 결제금액
'strSupplyValue = FnMoneySet(objRs("TOTAL_AMOUNT") - strAddTax)                     '공급가액

'예약시간표시
arrReserveTime = Split(objRs("RESERVE_TIMES"), ",")
For i=0 To UBound(arrReserveTime)
    strLastTime = CInt(arrReserveTime(i)) + 1
Next

'요청사항
strRequirement = objRs("REQUIREMENT")
If strRequirement&"" <> "" Then
    strRequirement = Replace(Replace(strRequirement, "&lt", "<"),"<br><br>","<br>")
End If

'폴리콤
strPolycom = ""
If objRs("POLYCOM")&"" = "N" Then
    strPolycom = "사용안함"
ElseIf objRs("POLYCOM")&"" = "L" Then
    strPolycom = "국내전화"
ElseIf objRs("POLYCOM")&"" = "G" Then
    strPolycom = "국외전화"
End If

'외부기자재 반입
strEtc = ""
If objRs("ETC")&"" = "N" Then
    strEtc = "반입안함"
ElseIf objRs("ETC")&"" = "Y" Then
    strEtc = "반입함"
End If

%>
<div id="contBody" class="contBody" style="margin:0 auto;padding:0 20px 20px 20px;">

<div id="pdf_div">
    <div class="iTitle">
        <table>
            <tr>
                <td>
                    <img src="/overseer_room/img/logo1.png" style="height:70%"/>
                </td>
                <td style="padding:25px 0 0 80px; vertical-align:top; line-height:normal; ">
                    <h1 style="font-size:30px;">IFC Seoul Association</h1>
                    <h3 style="font-size:20px; margin:30px 0 15px 0;">IFC Seoul관리단</h3>
                    <h4 style="font-size:14px;">10, Gukjegeumyung-ro,Youngdeungpo-gu, Seoul, 07326 Korea Tel. 82.2.6137.2100</h4>
                </td>
            </tr>
        </table>

       <!--  <div class="fl" style="margin-left:-100px;"><img src="/overseer_room/img/logo.png"/></div>
        <div class="fl" style="padding:25px 0 0 100px;">
            <h1 style="font-size:50px;">IFC Seoul Association</h1>
            <h3 style="font-size:30px;">IFC Seoul관리단</h3>
            <h4 style="font-size:16px;">10, Gukjegeumyung-ro,Youngdeungpo-gu, Seoul, 07326 Korea Tel. 82.2.6333.2100</h4>
        </div> -->
    </div>

    <div class="pTitle" style="float:left; margin-top:0"><h3>To : <%=objRs("USER_NAME")%></h3></div>
        <div class="tblStyle2 invoice">
            <table>
                <caption>회의실 예약 상세정보</caption>
                <colgroup>
                    <col style="width:17%"/><col style="width:35%"/><col style="width:17%"/><col style="width:*%"/>
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="row">예약번호</th>
                        <td><%=objRs("RESERVE_NO")%></td>
                        <th scope="row">예약 상태</th>
                        <td><%=strReserveStatus%></td>
                    </tr>
                    <tr>
                        <th scope="row">회사명</th>
                        <td><%=objRs("USER_NAME")%></td>
                        <th scope="row">사업자등록번호</th>
                        <td><%=strBUSINESS_RE_NO%></td>
                    </tr>
                    <tr>
                        <th scope="row">회의실</th>
                        <td><%=objRs("ROOM_NAMES")%></td>
                        <th scope="row">이용시간</th>
                        <td><%=strReserveDt%> <%=objRs("MIN_TIME")%>:00~<%=strLastTime%>:00</td>
                    </tr>
                    <tr>
                        <th scope="row">회의명</th>
                        <td colspan="3"><%=objRs("SUBJECT")%></td>
                    </tr>
                    <tr>
                        <th scope="row">예약자명</th>
                        <td><%=objRs("RESERVE_NAME")%></td>
                        <th scope="row">참석인원</th>
                        <td><%=objRs("P_COUNT")%>명</td>
                    </tr>
                    <tr>
                        <th scope="row">전화번호</th>
                        <td><%=FnSetNum(objRs("TELNUM"))%></td>
                        <th scope="row">FAX</th>
                        <td><%=FnSetNum(objRs("FAXNUM"))%></td>
                    </tr>
                    <tr>
                        <th scope="row">휴대폰번호</th>
                        <td><%=FnSetNum(objRs("MOBILENUM"))%></td>
                        <th scope="row">이메일</th>
                        <td><%=FnSetNum(objRs("EMAIL"))%></td>
                    </tr>
                    <tr>
                        <th scope="row">요청사항</th>
                        <td colspan="3"><%=strRequirement%></td>
                    </tr>
                    <tr>
                        <th scope="row">예약일</th>
                        <td colspan="3"><%=FnFormatDateTime(objRs("REG_DT"))%></td>
                    </tr>
                    <tr>
                        <th scope="row">폴리콤(3차통화)</th>
                        <td><%=strPolycom%></td>
                        <th scope="row">외부기자재 반입</th>
                        <td><%=strEtc%></td>
                    </tr>
                    <tr>
                        <th scope="row">기본요금</th>
                        <td><%=strTotalAmount%> (할인율 <%=objRs("DISCOUNT")%>% 적용)</td>
                        <th scope="row">무료 이용시간</th>
                        <td><%=objRs("FREE_TIME")%>시간</td>
                    </tr>

                    <% If objRs("RESERVE_STATUS") <> "C" Then %>
                    <!--     <tr>
                            <th scope="row">최종금액</th>
                            <td colspan="3"><%=strTotalAmount%></td>
                        </tr> -->
                    <% Else %>
                        <!-- <tr>
                            <th scope="row">최종금액</th>
                            <td><%=strCancelAmount%></td>
                            <th scope="row">예약 취소일</th>
                            <td><%=FnFormatDateTime(objRs("CAN_DT"))%></td>
                        </tr> -->
                    <% End If %>
                    <tr><td colspan="4" class="bb"><br/></td></tr>
                    <tr>
                        <td colspan="3">: The Forum at IFC 이용요금<br/>VAT(10%)</tds>
                        <td>KRW <%=strTotalAmount %><br/>KRW <%=strAddTax %></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="bb">Total amount</td>
                        <td class="bb">KRW <%=strPayAmount%></td>
                    </tr>
                    <tr>
                        <td colspan="3">Grand Total due for this matter<br/>*Refer to attached for detail</td>
                        <td>KRW <%=strPayAmount %></td>
                    </tr>
                </tbody>
            </table>
        </div>

         <div class="notiBox invoice" style="margin-top:20px;">
            <div class="gBox company" style="padding:10px;">
                <table class="tblStyle6">
                    <caption>회의실 예약 상세정보</caption>
                    <colgroup>
                        <col style="width:70%"/><col style="width:30%"/>
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>Company</th>
                            <th class="pl10">Amount</th>
                        </tr>
                        <tr>
                            <td><%=objRs("USER_NAME")%></td>
                            <td class="pl10">KRW  <%=strPayAmount %></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="notiBox invoice" style="margin-top:20px;">
            <h3 class="tit">Payment Instructions</h3>
            <div class="gBox payment" style="padding:10px;">
                <ul class="listDot">
                    <li>Credit Card Payment Only </li>
                    <li>Payment required within 7days from the date booking made</li>
                </ul><br/>
                <ul class="listDot">
                    <li>신용카드결제만 가능합니다.</li>
                    <li>예약 취소로 인한 패널티 위약금은 무상시간을 우선 차감</li>
                </ul>
    <!--
                <table class="tblStyle6">
                    <caption>회의실 예약 상세정보</caption>
                    <colgroup>
                        <col style="width:50%"/><col style="width:50%"/>
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>Bank</th>
                            <td>Kookmin Bank</td>
                        </tr>
                        <tr>
                            <th>Account NO.</th>
                            <td>001501-04-080622</td>
                        </tr>
                        <tr>
                            <th>Name on Acc.</th>
                            <td>SIFC Tower2 외4</td>
                        </tr>
                    </tbody>
                </table> -->
            </div>
        </div>
    </div>
</div>

<div class="btnWrap" style="padding-bottom:50px;">
    <button  class="nbtnG" onClick="fnGoPrint();">프린트하기</button>
    <button  class="nbtnG" onClick="fnSavePDF('<%=reserve_no%>');">PDF로저장</button>
</div>
<!-- index_popup_footer -->
                    </div>
                </div>
            </section>
        </div>
    </div>
</body>
</html>


