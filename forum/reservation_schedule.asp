<!--#include file = "default_control.asp" -->
<%
re_url = "reservation_list"
%>
<!--#include file = "session_check.asp" -->
<!--#include file = "index_header.asp" -->
<link href='../js/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='../js/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link href='../js/plugins/fullcalendar/scheduler.min.css' rel='stylesheet' />
<link href='../js/plugins/qtip/jquery.qtip.min.css' rel='stylesheet' />

<script src='../js/plugins/moment/moment.min.js'></script>
<!-- <script src='../js/lib/jquery.min.js'></script> -->

<!-- fullcalendar -->
<script src='../js/plugins/fullcalendar/fullcalendar.js'></script>

<!-- bootstrap -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>

<!-- qtip -->
<script src='../js/plugins/qtip/jquery.qtip.min.js'></script>

<!-- Calendar CSS -->
<style>
    #calendar {
      width: 95%;
      margin: 30px auto;
    }
</style>
<div id='calendar'></div>
<div class="btnWrap" style="padding-bottom:50px;">
    <button  class="nbtnG" onClick="javascript:history.back();">이전화면</button>
</div>

<!-- 캘린더 불러오기 -->
<script type="text/javascript">
    $('#calendar').fullCalendar({
        defaultView: 'month',
        header: {
          left: 'today prev,next',
          center: 'title',
          right: 'month,agendaWeek,agendaDay,listMonth'
        },
        displayEventTime: false,
        eventSources: [{
            url: 'ajax/calendar_proc.asp'
        }],
        eventRender: function(eventObj, $el) {
          $el.qtip({
            show: 'click',
            hide: 'unfocus',
            position: {
              my: 'top left',
              at: 'bottom right',
              adjust: {
                method: 'shift none'
              }
            },
          content: {
            title: eventObj.title,
            text: eventObj.description
          }
        });
      }
    });
</script>
<!--#include file = "index_footer.asp" -->
