

<script type="text/javascript">
	function fnIFCHallChg(idx) {
		$("button[id*=btn_hall]").removeClass("on");
		$("div[id*=div_hall]").hide();

		$("#btn_hall" + idx).addClass("on");
		$("#div_hall" + idx).show();
	}
</script>


<div id="layerPop01" class="lyPopWrap bw">
	<div class="lyPopBody">
		<strong class="pTit">요금표</strong>
		<div class="lyPopContWrap scrollH">
			<div class="stitWrap">
				<strong class="titS">Brookfield Hall</strong>
			</div>
			<div class="tblStyle4">
				<table>
					<caption>Brookfield Hall 요금표</caption>
					<colgroup>
						<col style="width:20%"/>
						<col style="width:20%"/>
						<col style="width:20%"/>
						<col style="width:20%"/>
						<col style="width:20%"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col"></th>
							<th scope="col">2시간</th>
							<th scope="col">4시간</th>
							<th scope="col">6시간</th>
							<th scope="col">All day</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">가격</th>
							<td>480,000원</td>
							<td>768,000원</td>
							<td>1,008,000원</td>
							<td>1,296,000원</td>
						</tr>
						<tr>
							<th scope="row">할인율</th>
							<td>0%</td>
							<td>20%</td>
							<td>30%</td>
							<td>40%</td>
						</tr>
					</tbody>
				</table>
			</div>
			<ul class="listDot" style="margin:17px 0 0;">
				<li>시간별 금액 : 240,000원</li>
				<li>외부기자재 반입하는 경우 별도 문의주시기 바랍니다.</li>
				<li>폴리콤(3자 통화) 사용 시 별도 비용이 청구됩니다.</li>
				<li>안내데스크 : 02-6137-2100</li>
			</ul>
			<div class="stitWrap">
				<strong class="titS">IFC Hall</strong>
			</div>
			<div class="tblStyle4">
				<table>
					<caption>IFC Hall 요금표</caption>
					<colgroup>
						<col style="width:*%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col"></th>
							<th scope="col"></th>
							<th scope="col">2시간</th>
							<th scope="col">4시간</th>
							<th scope="col">6시간</th>
							<th scope="col">All day</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row" rowspan="2" class="thBg">1개</th>
							<th scope="row">가격</th>
							<td>240,000원</td>
							<td>384,000원</td>
							<td>540,000원</td>
							<td>702,000원</td>
						</tr>
						<tr>
							<th scope="row">할인율</th>
							<td>0%</td>
							<td>20%</td>
							<td>25%</td>
							<td>35%</td>
						</tr>
						<tr>
							<th scope="row" rowspan="2" class="thBg">2개</th>
							<th scope="row">가격</th>
							<td>456,000원</td>
							<td>720,000원</td>
							<td>1,008,000원</td>
							<td>1,296,000원</td>
						</tr>
						<tr>
							<th scope="row">할인율</th>
							<td>5%</td>
							<td>25%</td>
							<td>30%</td>
							<td>40%</td>
						</tr>
						<tr>
							<th scope="row" rowspan="2" class="thBg">3개</th>
							<th scope="row">가격</th>
							<td>648,000원</td>
							<td>1,008,000원</td>
							<td>1,404,000원</td>
							<td>1,782,000원</td>
						</tr>
						<tr>
							<th scope="row">할인율</th>
							<td>10%</td>
							<td>30%</td>
							<td>35%</td>
							<td>45%</td>
						</tr>
						<tr>
							<th scope="row" rowspan="2" class="thBg">4개</th>
							<th scope="row">가격</th>
							<td>816,000원</td>
							<td>1,248,000원</td>
							<td>1,728,000원</td>
							<td>2,160,000원</td>
						</tr>
						<tr>
							<th scope="row">할인율</th>
							<td>15%</td>
							<td>35%</td>
							<td>40%</td>
							<td>50%</td>
						</tr>
					</tbody>
				</table>
			</div>
			<ul class="listDot" style="margin:17px 0 0;">
				<li>시간별 금액 : 120,000원</li>
				<li>외부기자재 반입하는 경우 관리자에서 별도 문의주시기 바랍니다.</li>
				<li>폴리콤(3자 통화) 사용 시 별도 비용이 청구됩니다.</li>
				<li>안내데스크 : 02-6137-2100</li>
			</ul>
		</div>
		<div class="btnWrap">
			<button class="nbtnG04 jsPopClose">닫기</button>
		</div>
	</div>
	<a href="#none" class="popCls jsPopClose">Close</a>
</div>

<div id="layerPop02" class="lyPopWrap bw">
	<div class="lyPopBody">
		<strong class="pTit">회의실 예약 도움말</strong>
		<div class="lyPopContWrap scrollH">
			<div class="stitWrap"><strong class="titS">회의실 선택</strong></div>
			<div class="guideBox01">
				<ul>
					<li>선착순 예약 / 예약 즉시 완료</li>
					<li>입주사 별 월 예약 가능 시간 및 무상 시간 제공
						<ul>
							<li>월 예약 시간 사용 초과 시 해당 월 예약 불가</li>
						</ul>
					</li>
					<li>예약 가능 날짜/시간
						<ul>
							<li>예약일로 부터 120일 이내 예약 가능</li>
							<li>09:00~18:00</li>
							<li>최소 2시간 이상, 이후 1시간 단위로 추가 가능</li>
                            <li>IFC홀 전체 사용시 최소 예약 시간은 4시간 부터입니다.</li>
						</ul>
					</li>
					<li>예약 불가한 경우
						<ul>
							<li>당일/익일/주말/공휴일 예약 불가</li>
							<li>기존 예약 된 회의 전/후 1시간은 예약 불가</li>
							<li>동일한 조건(회의실/이용시간) 4일 이상 예약 불가</li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="stitWrap"><strong class="titS">요금안내</strong></div>
			<div class="guideBox01">
				<ul>
					<li>회의실 및 이용 시간대 별 요금 적용 (요금표 확인)</li>
					<li>이용 시간 초과 시 초과 요금 발생 (초과 요금 단위 1시간)</li>
					<li>기자재 사용 시 사용료 별도 부과</li>
					<li>폴리콤 이용 시 통화료 별도 부과</li>
				</ul>
			</div>
			<div class="stitWrap"><strong class="titS">무료 이용시간 안내</strong></div>
			<div class="guideBox01">
				<ul>
					<li>입주사 별 연간 무료 이용 시간 제공</li>
					<li>매년 1월 1일 오전 00시에 초기화</li>
					<li>예약 시 무료 이용 시간 우선 차감</li>
					<li>브룩필드홀은 무료 이용 시간 사용 불가</li>
			</div>
			<div class="stitWrap"><strong class="titS">취소 및 환불규정</strong></div>
			<div class="guideBox01">
				<ul>
					<li>대관료 결제는 사전 결제를 원칙으로 합니다.</li>
					<li>예약 48시간 이내는 예약 취소가 불가합니다.</li>
					<li>24시간 이전의 예약은 예약후 취소가 불가합니다. 신중한 예약을 부탁드립니다.</li>
					<li>예약취소로 인한 위약금은 무상시간을 우선 차감합니다.</li>
					<li>이용시간 단축 조기퇴실이 발생시에도 예약시간 기준으로 요금이 부과됩니다.</li>
					<li>취소위약금은 세금계산서 발행이 되지않습니다.</li>
					<li>위약금 규졍 (계산방법)
						<ul>
							<!-- <li>30일전까지 : 100% 환급</li>
							<li>29일 ~ 15일 : 총 결제금액의 50%</li>
							<li>14일 ~ 이용당일  : 총결제금액의 100% </li> -->
                            <li>패널티기간 : 예약일 29일전 ~ 행사당일</li>
                            <li>코로나 19로 인한 예약취소는 위약금 및 패널티가 발생하지 않습니다. </li>
                            <!-- <li>그러나 패널티기간에 반영된 입주사 무상시간은 전산상 원복이 불가능합니다. </li> -->
						</ul>
					</li>
					<li>위약금은 결제금에 반환함을 원칙으로 합니다. 현금 입금의 경우 최대 30일이 소요될 수 있습니다.</li>
					<li>카드결제 승인 취소 및 반환의 경우, 결재시 카드영수증을 제출하여 주셔야 하며, 카드 취소는 카드사의 사정에 따라 10일이 소요될 수 있습니다.</li>
					<li>임대인은 불가항력 또는 국가시책 변경에 의해 계약이 해제되는 경우에는 임차인에게 임대료 반환 이외의 손해배상금은 지급되지않습니다.</li>
				</ul>
			</div>
		</div>
		<div class="btnWrap">
			<button class="nbtnG04 jsPopClose">닫기</button>
		</div>
	</div>
	<a href="#none" class="popCls jsPopClose">Close</a>
</div>

<div id="layerPop03" class="lyPopWrap bw">
	<div class="lyPopBody">
		<strong class="pTit">회의실 안내</strong>
		<div class="lyPopContWrap scrollH">
			<ul class="tabType01">
				<li class="_active"><a href="#">Brookfield Hall</a></li>
				<li><a href="#">IFC Hall</a></li>
			</ul>
			<div class="tabCont" style="display:block;">
				<div id="introV01" class="rVisualWrap">
					<ul class="swiper-wrapper">
						<li class="swiper-slide"><span><img src="img/com/intro_visual01.jpg" alt="" /></span></li>
					</ul>
					<div class="controllBox">
						<button type="button" class="swipePrev"><em>이전</em></button>
						<span class="navBox"></span>
						<button type="button" class="swipeNext"><em>다음</em></button>
					</div>
				</div>


				<div class="pTitle">
					<strong class="ptit">Brookfield Hall</strong>
					<span class="titCopy">현대적인 시청각 장비가 마련된 계단형 보드룸 Brookfield Hall은 중요 임원진 회의나 클라이언트 모임에 가장 적합한 장소를 제공합니다. 첨단 시청각 장비를 통하여 중요 프레젠테이션 및 프로젝트 소개에 있어 효과를 극대화 합니다.</span>
				</div>

				<div class="blitBox v02">
					<ul>
						<li>국제회의에 최적화된 동시통역 시스템</li>
						<li>동시통역실, VIP 대기실 제공</li>
						<li>비디오 컨퍼런스 시스템</li>
						<li>최첨단 대형 스크린 (180")과 개별 마이크</li>
						<li>최신 멀티미디어 화상 통화 시스템</li>
						<li>첨단 디지털 AV 제어 시스템</li>
						<li>초고속 인터넷, 와이파이 연결</li>
						<li>47명 규모로 중역 세미나 또는 클라이언트 회의에 적합한 장소</li>
					</ul>
				</div>

				<div class="pTitle">
					<strong class="ptit02">스펙</strong>
				</div>

				<div class="blitBox v02">
					<ul>
						<li>
							<span class="boxL">면적</span>
							<span class="boxR">186.89sqm (56.53평)</span>
						</li>
						<li>
							<span class="boxL">높이</span>
							<span class="boxR">6.8m</span>
						</li>
						<li>
							<span class="boxL">배너 사이즈</span>
							<span class="boxR">가로 5m</span>
						</li>
						<li>
							<span class="boxL">제공 기기</span>
							<span class="boxR">프로젝터, 개별 마이크</span>
						</li>
						<li>
							<span class="boxL">좌석</span>
							<span class="boxR">47seats</span>
						</li>
					</ul>
				</div>
			</div>

			<div class="tabCont">
				<div id="introV02" class="rVisualWrap">
					<ul class="swiper-wrapper">
						<li class="swiper-slide"><span><img src="img/com/intro_visual02.jpg" alt="" /></span></li>
						<li class="swiper-slide"><span><img src="img/temp/temp_visual02.jpg" alt="" /></span></li>
						<li class="swiper-slide"><span><img src="img/temp/temp_visual03.jpg" alt="" /></span></li>
						<li class="swiper-slide"><span><img src="img/temp/temp_visual04.jpg" alt="" /></span></li>
					</ul>
					<!--이미지 슬라이딩 히든 20180323
				    <div class="controllBox">
						<button type="button" class="swipePrev"><em>이전</em></button>
						<span class="navBox"></span>
						<button type="button" class="swipeNext"><em>다음</em></button>
					</div>-->
				</div>

				<div class="pTitle">
					<strong class="ptit">IFC Hall</strong>
					<span class="titCopy">활용성이 뛰어난 컨퍼런스룸 IFC Hall은 용도에 따라 분리 또는 확장하여 40명 부터 240명까지 수용할수 있는 다양한 공간을 제공합니다. 소규모 미팅부터 생산성이 요구되는 기업 세미나까지 미니멀한 인테리어와 첨단 멀티미디어 제공을 통해 미팅의 품격을 높혀줍니다.</span>
				</div>

				<div class="blitBox v02">
					<ul>
						<li>40인이상 240명까지 용도에 따른  다양한 공간 연출 가능</li>
						<li>최첨단 대형 스크린 (200")과 개별 마이크 (301호, 302호, 303호 이용 시 제공)</li>
						<li>최첨단 대형 230" LED 스크린 (304호 이용 시 제공)</li>
						<li>맞춤형 AV 시스템 자동 조절 패널</li>
						<li>초고속 인터넷, 와이파이 연결</li>
					</ul>
				</div>

				<div class="pTitle">
					<strong class="ptit02">스펙</strong>
				</div>

				<ul class="roomNum">
					<li><button type="button" id="btn_hall1" onClick="fnIFCHallChg(1);">301호</button></li>
					<li><button type="button" id="btn_hall2" onClick="fnIFCHallChg(2);">302호</button></li>
					<li><button type="button" id="btn_hall3" onClick="fnIFCHallChg(3);">303호</button></li>
					<li><button type="button" id="btn_hall4" onClick="fnIFCHallChg(4);">304호</button></li>
				</ul>

				<div class="blitBox v02" id="div_hall1" style="display:none">
					<ul>
						<li>
							<span class="boxL">면적</span>
							<span class="boxR">113.96sqm (34.47평)</span>
						</li>
						<li>
							<span class="boxL">높이</span>
							<span class="boxR">6.8m</span>
						</li>
						<li>
							<span class="boxL">배너 사이즈</span>
							<span class="boxR">가로 5m</span>
						</li>
						<li>
							<span class="boxL">스크린</span>
							<span class="boxR">200”</span>
						</li>
						<li>
							<span class="boxL">제공 기기</span>
							<span class="boxR">프로젝터, 개별 마이크</span>
						</li>
						<li>
							<span class="boxL">좌석</span>
							<span class="boxR">School 40seats / Theatre 60seats / Ushape 24seats</span>
						</li>
					</ul>
				</div>

				<div class="blitBox v02" id="div_hall2" style="display:none">
					<ul>
						<li>
							<span class="boxL">면적</span>
							<span class="boxR">89.59sqm (27.10평)</span>
						</li>
						<li>
							<span class="boxL">높이</span>
							<span class="boxR">6.8m</span>
						</li>
						<li>
							<span class="boxL">배너 사이즈</span>
							<span class="boxR">가로 5m</span>
						</li>
						<li>
							<span class="boxL">스크린</span>
							<span class="boxR">200”</span>
						</li>
						<li>
							<span class="boxL">제공 기기</span>
							<span class="boxR">프로젝터, 개별 마이크</span>
						</li>
						<li>
							<span class="boxL">좌석</span>
							<span class="boxR">School 40seats / Theatre 60seats / Ushape 24seats</span>
						</li>
					</ul>
				</div>

				<div class="blitBox v02" id="div_hall3" style="display:none">
					<ul>
						<li>
							<span class="boxL">면적</span>
							<span class="boxR">110.39sqm (33.39평)</span>
						</li>
						<li>
							<span class="boxL">높이</span>
							<span class="boxR">6.8m</span>
						</li>
						<li>
							<span class="boxL">배너 사이즈</span>
							<span class="boxR">가로 5m</span>
						</li>
						<li>
							<span class="boxL">스크린</span>
							<span class="boxR">200”</span>
						</li>
						<li>
							<span class="boxL">제공 기기</span>
							<span class="boxR">프로젝터, 개별 마이크</span>
						</li>
						<li>
							<span class="boxL">좌석</span>
							<span class="boxR">School 40seats / Theatre 60seats / Ushape 24seats</span>
						</li>
					</ul>
				</div>

				<div class="blitBox v02" id="div_hall4" style="display:none">
					<ul>
						<li>
							<span class="boxL">면적</span>
							<span class="boxR">88.21sqm (26.68평)</span>
						</li>
						<li>
							<span class="boxL">높이</span>
							<span class="boxR">6.8m</span>
						</li>
						<li>
							<span class="boxL">배너 사이즈</span>
							<span class="boxR">가로 5m</span>
						</li>
						<li>
							<span class="boxL">스크린</span>
							<span class="boxR">230” (LED)</span>
						</li>
						<li>
							<span class="boxL">제공 기기</span>
							<span class="boxR">프로젝터, 개별 마이크</span>
						</li>
						<li>
							<span class="boxL">좌석</span>
							<span class="boxR">School 24seats / Theatre 30seats / Ushape 20seats</span>
						</li>
					</ul>
				</div>
			</div>

		</div>
		<div class="btnWrap">
			<button class="nbtnG04 jsPopClose">닫기</button>
		</div>
	</div>
	<a href="#none" class="popCls jsPopClose">Close</a>
</div>
