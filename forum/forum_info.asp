<!--#include file = "default_control.asp" -->
<%
Dim strUserId, strUserName, strUserAuth

strUserId = Session("F_UID")
strUserName = Session("F_USER_NAME")
strUserAuth = Session("F_AUTHORITY")

If Request("current_type")&"" = "en" Then
	Response.Redirect "forum_info_EN.asp"
End If
%>
<!--#include file = "index_header.asp" -->
<script type="text/javascript">
	$(document).ready(function() {
		var introV01, introV02;
		$('.tabType02 button, .tabType02 a').on('click', function() {
			var $this = $(this);
			var $parent = $this.parent();
			var $tabCont = $('.tabCont');
			var $idx = $parent.index();

			if(!$parent.hasClass('_active')) {
				$('.tabType02 li').removeClass('_active');
				$parent.addClass('_active');
				$tabCont.hide().eq($idx).show();
			}

			if($idx == 0) {
				if(!introV01) {//console.log('introV01');
					if($('#introV01 ul li').length > 1) {
						$('#introV01 .controllBox').show();
						introV01 = new swipeSlidFn('#introV01', {
							pageNav : true
							, dirNav : true
						});
						introV01.update();
					}
				}
			}else if($idx == 1) {
				if(!introV02) {//console.log('introV02');
					if($('#introV02 ul li').length > 1) {
						$('#introV02 .controllBox').show();
						introV02 = new swipeSlidFn('#introV02', {
							pageNav : true
							, dirNav : true
						});
						introV02.update();
					}
				}


			}
		});

		if($('#introV01 ul li').length > 1) {
			$('#introV01 .controllBox').show();
			introV01 = new swipeSlidFn('#introV01', {
				pageNav : true
				, dirNav : true
			});
		}

		fnIFCHallChg(1);
	});

	function fnIFCHallChg(idx) {
		$("button[id*=btn_hall]").removeClass("on");
		$("tr[id*=tr_hall]").hide();

		$("#btn_hall" + idx).addClass("on");
		$("#tr_hall" + idx).show();
	}

	function fnAmountPop() {
		window.open("pop_amount.asp","pop_amount","direction=no, location=no, menubar=no, scrollbars=no, status=no, toolbar=no, resizeble=no, width=894, height=707");
	}

	function fnReservation() {
		location.href = "reservation.asp"
	}
</script>
<div class="visualBox">
	<!-- <span><img src="../../img/com/visual_login.png" alt="" /></span> -->
	<div class="visualTxt">
		<span class="txt01">The Forum at IFC</span>
		<span class="txt02">
			The Forum at IFC는 국제금융센터의 명성에 맞는 첨단 국제회의 시설을 제공하며<br />
			입주사 여러분의 특별한 경험을 통하여 비즈니스 가치와 품격을 높여줍니다.
		</span>
	</div>
</div>
<div class="contBody">

	<div class="pTitle">
		<h2>사용자를 위한 맞춤형 보드룸, 다목적 컨퍼런스룸 제공</h2>
		<span class="titCopy02">중역세미나에 적합한 보드룸 Brookfield Hall과 다목적 컨퍼런스룸 IFC Hall이 입주사 여러분과 사용자를 위한 맞춤형 공간을 제공, 성공적인 미팅으로 안내합니다.</span>
	</div>

	<ul class="mroomcInfo">
		<li class="info01">
			<strong>위치</strong>
			<span>Two IFC 3층</span>
		</li>
		<li class="info02">
			<strong>문의</strong>
			<span>02) 6137 - 2100</span>
		</li>
		<li class="info03">
			<strong>이메일</strong>
			<span>forum@ifcseoul.com</span>
		</li>
	</ul>

	<ul class="tabType02">
		<li class="_active"><button type="button">Brookfield Hall</button></li>
		<li><button type="button">IFC Hall</button></li>
	</ul>

	<div class="tabCont" style="display:block;">
		<div id="introV01" class="rVisualWrap">
			<ul class="swiper-wrapper">
				<li class="swiper-slide"><span><img src="img/com/intro_visual01_1.jpg" alt="" /></span></li>
				<li class="swiper-slide"><span><img src="img/com/intro_visual01_2.png" alt="" /></span></li>
			</ul>
			<div class="controllBox">
				<button type="button" class="swipePrev"><em>이전</em></button>
				<span class="navBox"></span>
				<button type="button" class="swipeNext"><em>다음</em></button>
			</div>
		</div>

		<div class="pTitle">
			<h3 class="ptit">Brookfield Hall</h3>
			<span class="titCopy02">현대적인 시청각 장비가 마련된 계단형 보드룸 Brookfield Hall은 중요 임원진 회의나 클라이언트 모임에 가장 적합한 장소를 제공합니다. 첨단 시청각 장비를 통하여 중요 프레젠테이션 및 프로젝트 소개에 있어 효과를 극대화 합니다.</span>
		</div>

		<div class="blitBox v02">
			<ul>
				<li>국제회의에 최적화된 동시통역 시스템</li>
				<li>동시통역실, VIP 대기실 제공</li>
				<li>비디오 컨퍼런스 시스템</li>
				<li>최첨단 대형 스크린 (180"), 프로젝터, 개별 마이크, 동시통역기기</li>
				<li>최신 멀티미디어 화상 통화 시스템</li>
				<li>첨단 디지털 AV 제어 시스템</li>
				<li>초고속 인터넷, 와이파이 연결</li>
				<li>47명 규모로 중역 세미나 또는 클라이언트 회의에 적합한 장소</li>
			</ul>
		</div>

		<div class="tblStyle5">
			<table>
				<caption>예약/이용 현황</caption>
				<colgroup>
					<col style="width:15%;">
					<col style="width:12%;">
					<col style="width:15%;">
					<col style="width:auto;">
					<col style="width:16%;">
					<col style="width:15%;">
				</colgroup>
				<thead>
					<tr>
						<th scope="col">면적</th>
						<th scope="col">높이</th>
						<th scope="col">배너사이즈</th>
						<th scope="col">제공기기</th>
						<th scope="col">좌석</th>
						<th scope="col">요금</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>186.89sqm<br />(56.53평)</td>
						<td>6.8m</td>
						<td>가로 5m</td>
						<td>프로젝터<br />개별 마이크</td>
						<td>
							<ul>
								<li>47seats</li>
							</ul>
						</td>
						<td><!--<a href="#?w=840" class="nbtnG03 jsbtnLyp" rel="layerPop01" style="min-width:108px;height:38px;line-height:38px;">-->예약시 확인가능<!-- </a> --></td>
					</tr>
					<!-- <tr>
						<td colspan="7"><p class="noData">검색결과가 존재하지 않습니다.</p></td>
					</tr> -->
				</tbody>
			</table>
		</div>
	</div>

	<div class="tabCont">
		<div id="introV02" class="rVisualWrap">
			<ul class="swiper-wrapper">
				<li class="swiper-slide"><span><img src="img/com/intro_visual02_forum1.jpg" alt="" /></span></li>
				<li class="swiper-slide"><span><img src="img/com/intro_visual02_forum2.jpg" alt="" /></span></li>
                <li class="swiper-slide"><span><img src="img/com/intro_visual02_forum3.jpg" alt="" /></span></li>
                <li class="swiper-slide"><span><img src="img/com/intro_visual02_forum4.jpg" alt="" /></span></li>
			</ul>
			<div class="controllBox">
				<button type="button" class="swipePrev"><em>이전</em></button>
				<span class="navBox"></span>
				<button type="button" class="swipeNext"><em>다음</em></button>
			</div>
		</div>

		<div class="pTitle">
			<h3 class="ptit">IFC Hall</h3>
			<span class="titCopy02">활용성이 뛰어난 컨퍼런스룸 IFC Hall은 용도에 따라 분리 또는 확장하여 40명 부터 240명까지 수용할수 있는 다양한 공간을 제공합니다. 소규모 미팅부터 생산성이 요구되는 기업 세미나까지 미니멀한 인테리어와 첨단 멀티미디어 제공을 통해 미팅의 품격을 높혀줍니다.</span>
		</div>

		<div class="blitBox">
			<ul>
				<li>40인이상 240명까지 용도에 따른  다양한 공간 연출 가능</li>
				<li>맞춤형 AV 시스템 자동 조절 패널</li>
				<li>최첨단 대형 스크린 (200")과 개별 마이크 (301호, 302호, 303호 이용 시 제공)</li>
				<li>초고속 인터넷, 와이파이 연결</li>
				<li>최첨단 대형 230" LED 스크린 (304호 이용 시 제공)</li>
			</ul>
		</div>
		<div class="tblTopBx">
			<ul class="roomNum">
				<li><button type="button" id="btn_hall1" onClick="fnIFCHallChg(1);">301호</button></li>
				<li><button type="button" id="btn_hall2" onClick="fnIFCHallChg(2);">302호</button></li>
				<li><button type="button" id="btn_hall3" onClick="fnIFCHallChg(3);">303호</button></li>
				<li><button type="button" id="btn_hall4" onClick="fnIFCHallChg(4);">304호</button></li>
				<div class="rBox">
					<a href="#?w=650" class="nbtnG02 jsbtnLyp" rel="layerPop02">좌석안내</a>
					<!-- <a href="#?w=840" class="nbtnG02 jsbtnLyp" rel="layerPop01">요금표 보기</a> -->
				</div>
			</ul>
		</div>
		<div class="tblStyle5">
			<table>
				<caption>예약/이용 현황</caption>
				<colgroup>
					<col style="width:;">
					<col style="width:8%;">
					<col style="width:10%">
					<col style="width:11%">
					<col style="width:11%">
					<col style="width:13%">
					<col style="width:16%">
					</colgroup>
				<thead>
					<tr>
					<th scope="col">위치</th>
					<th scope="col">면적</th>
					<th scope="col">높이</th>
					<th scope="col">배너사이즈</th>
					<th scope="col">스크린</th>
					<th scope="col">제공기기</th>
					<th scope="col">좌석</th>
										</tr>
				</thead>
				<tbody>
					<tr id="tr_hall1" style="display:none">
						<td><img src="img/com/blueprint01.jpg" alt="IFC Hall 301호 설계도"></td>
						<td>113.96sqm<br>(34.47평)</td>
						<td>6.8m</td>
						<td>가로 4.5m</td>
						<td>200’’</td>
						<td>프로젝터<br>개별 마이크</td>
						<td>
							<ul>
								<li>School 40seats</li>
								<li>Theatre60seats</li>
								<li>Ushape24seats</li>
							</ul>
						</td>
					</tr>
					<tr id="tr_hall2" style="display:none">
						<td><img src="img/com/blueprint02.jpg" alt="IFC Hall 302호 설계도"></td>
							<td>89.59sqm<br>(27.10평)</td>
							<td>6.8m</td>
							<td>가로 4.5m</td>
							<td>200’’</td>
							<td>프로젝터<br>개별 마이크</td>
							<td>
							<ul>
								<li>School 40seats</li>
								<li>Theatre60seats</li>
								<li>Ushape24seats</li>
							</ul>
						</td>
					</tr>
					<tr id="tr_hall3" style="display:none">
						<td><img src="img/com/blueprint03.jpg" alt="IFC Hall 303호 설계도"></td>
						<td>110.39sqm<br>(33.39평)</td>
						<td>6.8m</td>
						<td>가로 4.5m</td>
						<td>200’’</td>
						<td>프로젝터<br>개별 마이크</td>
						<td>
							<ul>
								<li>School 40seats</li>
								<li>Theatre60seats</li>
								<li>Ushape24seats</li>
							</ul>
						</td>
					</tr>
					<tr id="tr_hall4" style="display:none">
						<td><img src="img/com/blueprint04.jpg" alt="IFC Hall 304호 설계도"></td>
						<td>88.21sqm<br>(26.68평)</td>
						<td>6.8m</td>
						<td>가로 4.8m</td>
						<td>230’’(LED)</td>
						<td>프로젝터<br>개별 마이크</td>
						<td>
							<ul>
								<li>School 24seats</li>
								<li>Theatre30seats</li>
								<li>Ushape20seats</li>
							</ul>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="btnWrap">
		<button class="nbtnG" onClick="fnReservation();">예약하기</button>
	</div>
</div>

<div id="layerPop01" class="lyPopWrap bw">
	<div class="lyPopBody">
		<strong class="pTit">요금표</strong>
		<div class="lyPopContWrap scrollH">
			<div class="stitWrap">
				<strong class="titS">Brookfield Hall</strong>
			</div>
			<div class="tblStyle4">
				<table>
					<caption>Brookfield Hall 요금표</caption>
					<colgroup>
						<col style="width:20%"/>
						<col style="width:20%"/>
						<col style="width:20%"/>
						<col style="width:20%"/>
						<col style="width:20%"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col"></th>
							<th scope="col">2시간</th>
							<th scope="col">4시간</th>
							<th scope="col">6시간</th>
							<th scope="col">All day</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">가격</th>
							<td>480,000원</td>
							<td>768,000원</td>
							<td>1,008,000원</td>
							<td>1,152,000원</td>
						</tr>
						<tr>
							<th scope="row">할인율</th>
							<td>0%</td>
							<td>20%</td>
							<td>30%</td>
							<td>40%</td>
						</tr>
					</tbody>
				</table>
			</div>
			<ul class="listDot" style="margin:17px 0 0;">
				<li>시간별 금액 : 240,000원</li>
				<li>외부기자재 반입하는 경우 별도 문의주시기 바랍니다.</li>
				<li>폴리콤(3자 통화) 사용 시 별도 비용이 청구됩니다.</li>
				<li>안내데스크 : 02-6137-2100</li>
			</ul>
			<div class="stitWrap">
				<strong class="titS">IFC Hall</strong>
			</div>
			<div class="tblStyle4">
				<table>
					<caption>IFC Hall 요금표</caption>
					<colgroup>
						<col style="width:*%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
						<col style="width:16%"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="col"></th>
							<th scope="col"></th>
							<th scope="col">2시간</th>
							<th scope="col">4시간</th>
							<th scope="col">6시간</th>
							<th scope="col">All day</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row" rowspan="2" class="thBg">1Unit</th>
							<th scope="row">가격</th>
							<td>240,000원</td>
							<td>384,000원</td>
							<td>540,000원</td>
							<td>624,000원</td>
						</tr>
						<tr>
							<th scope="row">할인율</th>
							<td>0%</td>
							<td>20%</td>
							<td>25%</td>
							<td>35%</td>
						</tr>
						<tr>
							<th scope="row" rowspan="2" class="thBg">2Unit</th>
							<th scope="row">가격</th>
							<td>456,000원</td>
							<td>720,000원</td>
							<td>1,008,000원</td>
							<td>1,152,000원</td>
						</tr>
						<tr>
							<th scope="row">할인율</th>
							<td>5%</td>
							<td>25%</td>
							<td>30%</td>
							<td>40%</td>
						</tr>
						<tr>
							<th scope="row" rowspan="2" class="thBg">3Unit</th>
							<th scope="row">가격</th>
							<td>648,000원</td>
							<td>1,008,000원</td>
							<td>1,404,000원</td>
							<td>1,584,000원</td>
						</tr>
						<tr>
							<th scope="row">할인율</th>
							<td>10%</td>
							<td>30%</td>
							<td>35%</td>
							<td>45%</td>
						</tr>
						<tr>
							<th scope="row" rowspan="2" class="thBg">4Unit</th>
							<th scope="row">가격</th>
							<td>816,000원</td>
							<td>1,248,000원</td>
							<td>1,728,000원</td>
							<td>1,920,000원</td>
						</tr>
						<tr>
							<th scope="row">할인율</th>
							<td>15%</td>
							<td>35%</td>
							<td>40%</td>
							<td>50%</td>
						</tr>
					</tbody>
				</table>
			</div>
			<ul class="listDot" style="margin:17px 0 0;">
				<li>시간별 금액 : 120,000원</li>
				<li>외부기자재 반입하는 경우 관리자에서 별도 문의주시기 바랍니다.</li>
				<li>폴리콤(3자 통화) 사용 시 별도 비용이 청구됩니다.</li>
				<li>안내데스크 : 02-6137-2100</li>
			</ul>
		</div>
		<div class="btnWrap">
			<button class="nbtnG04 jsPopClose">닫기</button>
		</div>
	</div>
	<a href="#none" class="popCls jsPopClose">Close</a>
</div>

<div id="layerPop02" class="lyPopWrap bw">
	<div class="lyPopBody">
		<strong class="pTit">좌석안내</strong>
		<div class="lyPopContWrap scrollH">
			<div class="seatGuide">
				<ul>
					<li>
						<img src="img/com/seat_guide01.jpg" alt="theatre" />
						<span> Theatre</span>
					</li>
					<li>
						<img src="img/com/seat_guide02.jpg" alt="School" />
						<span> School</span>
					</li>
					<li>
						<img src="img/com/seat_guide03.jpg" alt="Ushape" />
						<span>Ushape</span>
					</li>
				</ul>

				<p>회의 규모와 성격에 따라 다양한 좌석 배치가 가능합니다.<br />다목적 컨퍼런스룸 IFC hall을 모두 연결하여 이용하면 최대 240명까지 이용이 가능합니다.</p>
			</div>
		</div>
		<div class="btnWrap">
			<button class="nbtnG04 jsPopClose">닫기</button>
		</div>
	</div>
	<a href="#none" class="popCls jsPopClose">Close</a>
</div>
<!--#include file = "index_footer.asp" -->
