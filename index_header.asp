<!DOCTYPE html>
<html xml:lang="ko" lang="ko">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta http-equiv='refresh' content=='0; url=https://ifcseoul.com/index.asp'target='_top'>
    <title>IFC Seoul</title>
    <%=css_include%>
    <link rel="stylesheet" type="text/css" href="/css/layout.css" />
    <link rel="stylesheet" type="text/css" href="/css/common.css" />
    <link rel="stylesheet" type="text/css" href="/css/animation.css" />
	<link rel="stylesheet" type="text/css" href="/css/jquery.bxslider.css" />
    <!-- <script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script> -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="/js/modernizr.custom.min.js"></script>
    <script type="text/javascript" src="/js/jquery.event.drag-1.5.min.js"></script>
    <script type="text/javascript" src="/js/jquery.touchSlider.js"></script>
	<script type="text/javascript" src="/js/jquery.bxslider.js"></script>
    <script type="text/javascript" src="/js/ifc_ui.js"></script>
    <script type="text/javascript" src="/js/ifc_custom.js"></script>


    <!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
        <script type="text/javascript" src="/js/respond.min.js"></script>
		<script type="text/javascript">
			var ltIE9 = true;
		</script>
    <![endif]-->
	<%=js_include%>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-61219190-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-61219190-1');
    </script>

</head>
<!--[if lt IE 7]><body class="msie ie6 lt-ie9 lt-ie8 lt-ie7 lt-css3"><![endif]-->
<!--[if IE 7]>   <body class="msie ie7 lt-ie9 lt-ie8 lt-css3"><![endif]-->
<!--[if IE 8]>   <body class="msie ie8 lt-ie9 lt-css3"><![endif]-->
<!--[if IE 9]>   <body class="msie ie9 css3"><![endif]-->
<!--[if gt IE 9]><!-->
<body>
<!--<![endif]-->

    <div id="wrap" class="<%=wrap_main%>">
    	<!-- #include file = "com/com_snb_all.html" -->
        <% If checkFileExist(snb_name) Then Server.Execute(snb_name) End If %>
        <div id="containerWrap">
        <header>
            <!-- #include file = "com/com_skip.html" -->
            <!-- #include file = "com/com_header.html" -->
        </header>
        <section>
            <!-- container -->
            <div id="container">
                <!-- #include file = "com/linemap.html" -->
