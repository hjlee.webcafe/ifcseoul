<!DOCTYPE html>
<html xml:lang="ko" lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<title>IFC Seoul</title>
	<link rel="stylesheet" type="text/css" href="/css/base_ko.css" /><link rel="stylesheet" type="text/css" href="/css/MN_00_00/style.css" />
	<link rel="stylesheet" type="text/css" href="/css/layout.css" />
	<link rel="stylesheet" type="text/css" href="/css/common.css" />
	<link rel="stylesheet" type="text/css" href="/css/animation.css" />
	<link rel="stylesheet" type="text/css" href="/css/jquery.bxslider.css" />
	<!-- <script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script> -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
	<script type="text/javascript" src="/js/jquery.placeholder.js"></script>
	<script type="text/javascript" src="/js/modernizr.custom.min.js"></script>
	<script type="text/javascript" src="/js/jquery.event.drag-1.5.min.js"></script>
	<script type="text/javascript" src="/js/jquery.touchSlider.js"></script>
	<script type="text/javascript" src="/js/jquery.bxslider.js"></script>
	<script type="text/javascript" src="/js/ifc_ui.js"></script>
	<script type="text/javascript" src="/js/ifc_custom.js"></script>


	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/js/respond.min.js"></script>
	<script type="text/javascript">
		var ltIE9 = true;
	</script>
	<![endif]-->
	<script type="text/javascript" src="/js/MN_00_00/script.js"></script>
</head>
<!--[if lt IE 7]><body class="msie ie6 lt-ie9 lt-ie8 lt-ie7 lt-css3"><![endif]-->
<!--[if IE 7]>   <body class="msie ie7 lt-ie9 lt-ie8 lt-css3"><![endif]-->
<!--[if IE 8]>   <body class="msie ie8 lt-ie9 lt-css3"><![endif]-->
<!--[if IE 9]>   <body class="msie ie9 css3"><![endif]-->
<!--[if gt IE 9]><!-->
<body>
<!--<![endif]-->

<div id="wrap" class="main">
<!-- snb -->
<div class="snbShadow"></div>
<nav id="snbAll" class="snbAll">
	<div class="clsWrap"><a href="#none" class="jsNavSnbClose">닫기</a></div>
	<ul class="menuAllList">
		<li><a href="#none" class="on">IFC 서울<span></span></a>
			<ul style="display:block">
				<li><a href="IS_01_00.asp">소개</a></li>
				<li><a href="#none">IFC 서울 프로젝트</a>
					<ul>
						<li><a href="IS_02_01.asp">개요</a></li>
						<li><a href="IS_02_02.asp">의의 및 비전</a></li>
						<li><a href="IS_02_03.asp">특징</a></li>
						<li><a href="IS_02_04.asp">연혁</a></li>
					</ul>
				</li>
				<li><a href="IS_03_00.asp">개발사</a></li>
				<li><a href="IS_04_00.asp">프로젝트 팀</a></li>
				<li><a href="IS_05_00.asp">여의도 연혁</a></li>
				<li><a href="IS_06_00.asp">수상</a></li>
				<li><a href="#none">갤러리</a>
					<ul>
						<li><a href="IS_07_01.asp">IFC 서울 갤러리</a></li>
						<li><a href="IS_07_02.asp">디지털 브로슈어</a></li>
						<li><a href="IS_07_03.asp">IFC 서울 필름</a></li>
						<li><a href="IS_07_04.asp">헬리캠 영상</a></li>
						<li><a href="IS_07_05.asp">외부 전경</a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li><a href="#none">빌딩<span></span></a>
			<ul>
				<li><a href="BD_01_00.asp">개요</a></li>
				<li><a href="BD_02_00.asp">컨셉</a></li>
				<li><a href="BD_03_00.asp">특징</a></li>
				<li><a href="BD_04_00.asp">평면도</a></li>
				<li><a href="BD_05_00.asp">설계 사양</a></li>
				<!--li><a href="BD_06_00.asp">입주사 추천사</a></li-->
				<li><a href="#none">시설</a>
					<ul>
						<li><a href="BD_07_01.asp">CONRAD 서울</a></li>
						<li><a href="BD_07_02.asp">IFC 몰</a></li>
						<li><a href="BD_07_03.asp">예술작품</a></li>
						<li><a href="BD_07_04.asp">서비스</a></li>
						<li><a href="BD_07_05.asp">주차시설</a></li>
						<!-- li><a href="#none">Security &amp; Saety</a></li -->
						<li><a href="BD_07_07.asp">갤러리</a></li>
					</ul>
				</li>
				<li><a href="#none">임대 문의</a>
					<ul>
						<li><a href="BD_08_01.asp">특장점</a></li>
						<li><a href="BD_08_02.asp">오피스 임대</a></li>
						<li><a href="BD_08_03.asp">리테일 임대</a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li><a href="NM_01_00.asp" class="tL">주변 지역 안내<span></span></a>
			<ul>
				<li><a href="NM_01_00.asp">주변 지역 안내</a></li>
			</ul>
		</li>
		<li><a href="#none">소식<span></span></a>
			<ul>
				<li><a href="NE_01_00.asp">공지</a></li>
				<li><a href="NE_02_00.asp">보도자료</a></li>
				<li><a href="NE_03_00.asp">이벤트</a></li>
			</ul>
		</li>
	</ul>
	<ul class="menuEtcList">
		<li><a href="/ko/MN_00_00.asp" class="etcBtnHome">홈<span></span></a></li>
		<li><a href="HF_01_00.asp" class="etcBtnLoc">위치 및 지도<span></span></a></li>
		<li><a href="HF_02_00.asp" class="etcBtnContact">Contact<span></span></a></li>

		<li><a href="#ko" class="etcBtnLang lang">ENGLISH<span></span></a></li>


	</ul>
</nav>
<!-- //snb -->
<!-- snb -->
<div class="snbShadow"></div>
<nav id="snb">
	<div class="clsWrap"><a href="#none" class="jsNavSnbClose">닫기</a></div>
	<ul class="menuList">
		<li><a href="IS_01_00.asp" class="on">IFC 서울<span></span></a></li>
		<li><a href="BD_01_00.asp">빌딩<span></span></a></li>
		<li><a href="NM_01_00.asp" class="tL">주변 지역 안내<span></span></a></li>
		<li><a href="NE_01_00.asp">소식<span></span></a></li>
	</ul>
	<ul class="menuEtcList">
		<li><a href="/ko/MN_00_00.asp" class="etcBtnHome">홈<span></span></a></li>
		<li><a href="HF_01_00.asp" class="etcBtnLoc">위치 및 지도<span></span></a></li>
		<li><a href="HF_02_00.asp" class="etcBtnContact">Contact<span></span></a></li>

		<li><a href="#ko" class="etcBtnLang lang">ENGLISH<span></span></a></li>

	</ul>
</nav>
<!-- //snb -->
<div id="containerWrap">
<header>
	<!-- u_skip -->
	<div id="u_skip">
	</div>
	<!-- //u_skip -->
	<!-- header -->
	<div id="header">
		<div class="container">
			<a href="#none" class="btnMenu">홈</a>
			<h1><a href="/ko/MN_00_00.asp"><span>IFC Seoul</span></a></h1>
			<a href="HF_01_00.asp" class="btnMap">위치 및 지도</a>
		</div>
		<div id="gnb">
			<ul>
				<li><a href="/ko/MN_00_00.asp">홈</a></li>
				<li><a href="HF_01_00.asp">위치 및 지도</a></li>
				<li><a href="HF_02_00.asp">Contact</a></li>

				<li><a href="#ko" class="lang">ENGLISH</a></li>

			</ul>
		</div>
		<div id="lnb">
			<ul>
				<li class="m1"><a href="IS_01_00.asp">IFC 서울</a></li>
				<li class="m2"><a href="BD_01_00.asp">빌딩</a></li>
				<li class="m3"><a href="NM_01_00.asp">주변 지역 안내</a></li>
				<li class="m4"><a href="NE_01_00.asp">소식</a></li>
			</ul>
		</div>
	</div>
	<!-- //header -->

</header>
<section>
<!-- container -->
<div id="container">
<!-- lineMap -->
<div class="lineMapWrap">
	<ul class="lineMap"></ul>
</div>
<!-- //lineMap -->
<script type="text/javascript" src="/js/video.js"></script>
<link rel="stylesheet" type="text/css" href="/css/video-js.css">



<script type="text/javascript">
	$(document).ready(function () {
		$('.col1Slider').bxSlider($.extend({
			auto: true,
			autoControls: true,
			pause: 4000,
			slideMargin: 0,
			touchEnabled: false
		}, typeof ltIE9 !== 'undefined' && ltIE9 ? {mode:'fade'} : {}));
		$('.col2Slider').bxSlider({
			mode: 'fade',
			auto: true,
			autoControls: true,
			pause: 4000
		});
		$('.col3Slider').bxSlider({
			mode: 'fade',
			auto: true,
			autoControls: true,
			pause: 4000
		});
		$('.col4Slider').bxSlider({
			minSlides: 3,
			maxSlides: 3,
			slideWidth: 360,
			slideMargin: 10,
			infiniteLoop: false,
			hideControlOnEnd: true
		});
	});



	$(function () {
		$('.gallNavi li a').each(function (index) {
			$('.gallNavi li a').eq(index).click(function (e) {
				e.preventDefault();
				$('.viewGall').removeClass('viewOn');
				$('.viewGall').eq(index).addClass('viewOn');
			});
		});
	});

	videojs.options.flash.swf = "/swf/video-js.swf";
</script>

<!-- main -->
<div id="main" class="main">
	<div class="box col1">
		<div class="mainSectionCont">
			<!-- <p><a href="IS_01_00.asp">Newly rising with perfect freshness</a></p> -->
			<h2><a href="IS_01_00.asp">THE NEW, <strong>IFC SEOUL</strong></a></h2>
		</div>
		<div class="bdWrap">
			<ul class="col1Slider">
				<li class="sliderBg1"><span class="blind">CONRAD Seoul External Image1</span></li>
				<li class="sliderBg2"><span class="blind">CONRAD Seoul External Image2</span></li>
				<li class="sliderBg3"><span class="blind">CONRAD Seoul External Image3</span></li>
			</ul>
		</div>
	</div>
	<div class="box col2">
		<div class="mainSectionCont">
			<a href="BD_07_01.asp">
				<p>The luxury of Being yourself,</p>
				<h2><strong>CONRAD SEOUL</strong></h2>
			</a>
		</div>
		<div class="bdWrap">
			<ul class="col2Slider">
				<li><img src="../img/main/col2_1.jpg" alt="CONRAD Seoul External Image" /></li>
				<li><img src="../img/main/col2_2.jpg" alt="CONRAD Seoul External Image" /></li>
				<li><img src="../img/main/col2_3.jpg" alt="CONRAD Seoul External Image" /></li>
			</ul>
		</div>
	</div>
	<div class="box col3">
		<div class="mainSectionCont">
			<a href="BD_07_02.asp">
				<p>The place you can spend<br />the most enjoyable time,</p>
				<h2><strong>IFC MALL</strong></h2>
			</a>
		</div>
		<div class="bdWrap">
			<ul class="col3Slider">
				<li><img src="../img/main/col3_1.jpg" alt="IFC Mall External Image" /></li>
				<li><img src="../img/main/col3_2.jpg" alt="IFC Mall External Image" /></li>
				<li><img src="../img/main/col3_3.jpg" alt="IFC Mall External Image" /></li>
			</ul>
		</div>
	</div>
	<div class="box col4">
		<div class="bdWrap">
			<div class="gall_top">
				<div class="mainSectionCont">
					<h2><a href="IS_07_01.asp">IFC SEOUL GALLERY</a></h2>
				</div>
			</div>
			<div class="viewZone">
				<ul>

					<li class="viewGall viewOn"><img src="/images/dd7b0460(원).jpg" alt="IFC Seoul 4" /></li>

					<li class="viewGall"><img src="/images/North atrium(1).jpg" alt="North atrium" /></li>

					<li class="viewGall"><img src="/images/Conrad Seoul_Executive Suite(원).jpg" alt="Executive 룸" /></li>

					<li class="viewGall viewOn"><img src="/images/_WOO1935(원).jpg" alt="IFC Seoul 5" /></li>

					<li class="viewGall"><img src="/images/dd7b0594.JPG" alt="IFC Mall 1" /></li>

					<li class="viewGall"><img src="/images/Conrad Seoul_Conrad Suite_Living Room(원).jpg" alt="Conrad Suite" /></li>

					<li class="viewGall viewOn"><img src="/images/dd7b0857(1).jpg" alt="IFC Seoul 6" /></li>

					<li class="viewGall"><img src="/images/IMG_3107(원).jpg" alt="IFC Mall 3" /></li>

					<li class="viewGall"><img src="/images/Conrad Seoul_Penthouse Living Room(원).jpg" alt="Penthouse " /></li>

					<li class="viewGall viewOn"><img src="/images/dd7b0484(1).jpg" alt="IFC Seoul 7" /></li>

					<li class="viewGall"><img src="/images/IFC041(원).jpg" alt="IFC Mall 4" /></li>

					<li class="viewGall"><img src="/images/Conrad Seoul_Grand Ballroom_Theater1(원).jpg" alt="연회장" /></li>

					<li class="viewGall viewOn"><img src="/images/dd7b0856(1).jpg" alt="IFC Seoul 10" /></li>

					<li class="viewGall"><img src="/images/_WOO2033(1).jpg" alt="IFC Mall 5" /></li>

					<li class="viewGall"><img src="/images/Conrad Seoul_Board Room(원).jpg" alt="Board Room" /></li>

				</ul>
			</div>
			<div class="bkBox">&nbsp;</div>
			<div class="listShadow">&nbsp;</div>
			<div class="gallNavi">
				<ul class="imgList col4Slider">

					<li><a href="#none"><span class="text">IFC Seoul</span><img src="/images/dd7b0460(원).jpg" alt="IFC Seoul 4" /></a></li>

					<li><a href="#none"><span class="text">IFC Mall</span><img src="/images/North atrium(1).jpg" alt="North atrium" /></a></li>

					<li><a href="#none"><span class="text">CONRAD Seoul</span><img src="/images/Conrad Seoul_Executive Suite(원).jpg" alt="Executive 룸" /></a></li>

					<li><a href="#none"><span class="text">IFC Seoul</span><img src="/images/_WOO1935(원).jpg" alt="IFC Seoul 5" /></a></li>

					<li><a href="#none"><span class="text">IFC Mall</span><img src="/images/dd7b0594.JPG" alt="IFC Mall 1" /></a></li>

					<li><a href="#none"><span class="text">CONRAD Seoul</span><img src="/images/Conrad Seoul_Conrad Suite_Living Room(원).jpg" alt="Conrad Suite" /></a></li>

					<li><a href="#none"><span class="text">IFC Seoul</span><img src="/images/dd7b0857(1).jpg" alt="IFC Seoul 6" /></a></li>

					<li><a href="#none"><span class="text">IFC Mall</span><img src="/images/IMG_3107(원).jpg" alt="IFC Mall 3" /></a></li>

					<li><a href="#none"><span class="text">CONRAD Seoul</span><img src="/images/Conrad Seoul_Penthouse Living Room(원).jpg" alt="Penthouse " /></a></li>

					<li><a href="#none"><span class="text">IFC Seoul</span><img src="/images/dd7b0484(1).jpg" alt="IFC Seoul 7" /></a></li>

					<li><a href="#none"><span class="text">IFC Mall</span><img src="/images/IFC041(원).jpg" alt="IFC Mall 4" /></a></li>

					<li><a href="#none"><span class="text">CONRAD Seoul</span><img src="/images/Conrad Seoul_Grand Ballroom_Theater1(원).jpg" alt="연회장" /></a></li>

					<li><a href="#none"><span class="text">IFC Seoul</span><img src="/images/dd7b0856(1).jpg" alt="IFC Seoul 10" /></a></li>

					<li><a href="#none"><span class="text">IFC Mall</span><img src="/images/_WOO2033(1).jpg" alt="IFC Mall 5" /></a></li>

					<li><a href="#none"><span class="text">CONRAD Seoul</span><img src="/images/Conrad Seoul_Board Room(원).jpg" alt="Board Room" /></a></li>

				</ul>
				<!--<a href="#" class="prev"><img src="../img/main/prev.png" alt="prev" /></a>
				<a href="#" class="next"><img src="../img/main/next.png" alt="next" /></a>-->
			</div>
		</div>
	</div>
	<div class="boxGroup">
		<div class="boxInner">
			<div class="box col5">
				<div class="bdWrap">
					<div class="mainSectionCont">
						<h2>IFC SEOUL<br /><span>NEWS</span></h2>
						<p class="mt15"><a href="NE_01_01.asp?num=9">2014 IFC Seoul Website Renewal</a></p>
						<span class="moreBtn"><a href="NE_01_00.asp" class="goLink">Read more</a></span>
					</div>
				</div>
			</div>
			<div class="box col6">
				<div class="bdWrap">
					<img src="../img/main/m_4.png" alt="IFC Seoul Building External Image" />
					<div class="mainSectionCont">
						<a href="IS_07_03.asp"><img src="../img/main/txt5.png" alt="IFC SEOUL FILM" /></a>
					</div>
				</div>
			</div>
			<div class="box col7">
				<div class="bdWrap">
					<div class="mainSectionCont">
						<h2>SPECIFICATIONS</h2>
						<p class="mt10">
							● 10 ft floor to ceiling<Br />
							● 15 ft slab to slab<Br />
							● 6 inch full access raised floor<Br />
						</p>
						<span class="moreBtn"><a href="BD_05_00.asp" class="goLink">Read more</a></span>
					</div>
				</div>
			</div>
			<div class="box col8">
				<div class="bdWrap">
					<div class="mainSectionCont">
						<h2><span>AMENITIES</span></h2>
						<p class="mt30 ml30"><a href="BD_07_03.asp"><img src="../img/main/icon_ifcseoul.png" alt="AMENITIES" /></a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- layer popup -->

<div id="noticePop_1" class="layer_popup noticePopWarp">
	<div class="video_wrap">
		<video id="ad_movie" class="video-js vjs-default-skin"
			controls preload="auto" width="100%" height="150"
			poster="" data-setup='{"techOrder" : ["html5","flash","other supported tech"]}'>
			<source src="http://team-all.dlacc.skcdn.com/ifcseoul/intro.mp4" type='video/mp4' />
		</video>
	</div>
	<div class="btn_area clearfix">
		<label><input type="checkbox" class="vertical_m" /> 하루동안 열지 않기 </label> <a href="#none" class="js_PopClose fr type2">Close</a>
	</div>
</div>

<!-- //layer popup -->            </div>
<!-- //container -->
</section>
<!-- footer -->
<footer>
	<div id="footer">
		<div class="container top">
			<div class="lCon">
				<ul>
					<li><a href="HF_06_00.asp">부동산 고지</a></li>
					<li><a href="HF_07_00.asp">사이트 맵</a></li>
				</ul>
			</div>
			<div class="rCon">
				<!-- select id="family_site">
					<option value="">Family Site</option>
					<option value="http://www.ifcmallseoul.com">IFC Mall</option>
					<option value="http://www.conradseoul.co.kr">CONRAD Seoul</option>
					<option value="">AIG GRE</option>
				</select -->
				<div class="selectList">
					<p><a href="#none">FAMILY SITE</a></p>
					<ul class="hide">
						<li><a href="http://www.ifcmallseoul.com" target="_blank" title="Open New Window">IFC MALL</a></li>
						<li><a href="http://www.conradseoul.co.kr" target="_blank" title="Open New Window">CONRAD Seoul</a></li>
						<li><a href="http://www.aig.com/real-estate_3171_523524.html" target="_blank" title="Open New Window">AIG GRE</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="container bottom">
			<address>서울특별시 영등포구 여의도동 국제금융로 10, (우)150-945</address>
			<p class="copy">&copy; 2014 American International Group Inc. ALL RIGHT RESERVED.</p>
		</div>
	</div>
</footer>
<!-- //footer -->
<!-- //footer -->
</div>
</div><!-- //wrap -->

</body>
</html>