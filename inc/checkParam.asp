<%
Function CheckParamType(kind,val)
	Dim bFlag
  If val = "" Or IsNull(val) Then
    CheckParamType = val
  Else
		Select Case kind
			Case "int" : bFlag = IsNumeric(val)
			Case "date" : bFlag = IsDate(val)
		End Select
    If Not bFlag Then
      MsgBoxHistoryBack "허용되지 않은 데이터 타입이 사용되었습니다"
    Else
      CheckParamType = val
    End If
  End If
End Function

Function CheckParamLimitedList(val,arrList)
  Dim boolMatch : booMatch = False
  If val = "" Or IsNull(val) Then
    CheckParamLimitedList = val
  Else
    For i=0 To UBound(arrList)
      If val = arrList(i) Then boolMatch = True
    Next
    If boolMatch Then
      CheckParamLimitedList = val
    Else
      MsgBoxHistoryBack "허용되지 않은 데이터가 사용되었습니다"
    End If
  End If
End Function

Function CheckParamLength(val,length)
  If val = "" Or IsNull(val) Then
    CheckParamLength = val
  Else
    If Len(val) > length Then
      MsgBoxHistoryBack "데이터가 허용된 문자의 길이("& length &"자)를 초과하였습니다"
    Else
      CheckParamLength = val
    End If
  End If
End Function

Sub CheckCompulsoryData(arrData)
	If IsArray(arrData) Then
		For j=0 To UBound(arrData)
			If arrData(j) = "" Or IsNull(arrData(j)) Then MsgBoxHistoryBack "반드시 필요한 데이터가 존재하지 않습니다."
		Next
	End If
End Sub

Function GetTag2Text(str)
	If IsNull(str) = False Then
		str = Replace(str,"&amp;","&amp;amp;")
		str = Replace(str,"&lt;","&amp;lt;")
		str = Replace(str,"&gt;","&amp;gt;")
		str = Replace(str,"&quot;","&amp;quot;")
		str = Replace(str,"<","&lt;")
		str = Replace(str,Chr(13)&Chr(10),"<br />")
		str = Replace(str,Chr(34),"&quot;")
		str = Replace(str,"script","..")
		str = Replace(str,"'","&quot;")
		str = Replace(str,"'","&quot;")
		str = Replace(str, "'", "''" )
		str = Replace(str, ";", "" )
		str = Replace(str, "--", "" )
		str = Replace(str, "1=1", "", 1, -1, 1 )
		str = Replace(str, "sp_", "", 1, -1, 1 )
		str = Replace(str, "xp_", "", 1, -1, 1 )
		str = Replace(str, "@variable", "", 1, -1, 1 )
		str = Replace(str, "@@variable", "", 1, -1, 1 )
		str = Replace(str, "exec", "", 1, -1, 1 )
		str = Replace(str, "sysobject", "", 1, -1, 1 )
		str = InjectionCheck(str)
		GetTag2Text = str
	End If
End Function

Function InjectionCheck(ByVal str)
	Dim arrInjection(38)
	Dim ijtI, ijtTnf
	arrInjection(0) = "information_schema.tables"
	arrInjection(1) = "xp_"
	arrInjection(2) = "sp_"
	arrInjection(3) = "dtproperties"
	arrInjection(4) = "syscolumns"
	arrInjection(5) = "syscomments"
	arrInjection(6) = "sysdepends"
	arrInjection(7) = "sysfilegroups"
	arrInjection(8) = "sysfiles"
	arrInjection(9) = "sysfiles1"
	arrInjection(10) = "sysforeignkeys"
	arrInjection(11) = "sysfulltextcatalogs"
	arrInjection(12) = "sysfulltextnotify"
	arrInjection(13) = "sysindexes"
	arrInjection(14) = "sysindexkeys"
	arrInjection(15) = "sysmembers"
	arrInjection(16) = "sysobjects"
	arrInjection(17) = "syspermissions"
	arrInjection(18) = "sysproperties"
	arrInjection(19) = "sysprotects"
	arrInjection(20) = "sysreferences"
	arrInjection(21) = "systypes"
	arrInjection(22) = "sysusers"
	arrInjection(23) = "master.dbo."
	arrInjection(24) = "master.."
	arrInjection(25) = "union"
	arrInjection(26) = "drop"
	arrInjection(27) = "update"
	arrInjection(28) = "insert"
	arrInjection(29) = "select"
	arrInjection(30) = "delete"
	arrInjection(31) = "' or ''='"
	arrInjection(32) = "' or 1=1 ;--"
	arrInjection(33) = "<script"
	arrInjection(34) = "<object"
	arrInjection(35) = "<iframe"
	arrInjection(36) = "varchar("
	arrInjection(37) = "exec("
	arrInjection(38) = "<meta"
	ijtTnf = True
	If IsNull(str) Or IsEmpty(str) Or Trim(str) = "" Then
		InjectionCheck = ""
		Exit Function
	End If
	For ijtI = 0 To UBound(arrInjection) - 1
		If InStr(LCase(str), arrInjection(ijtI)) > 0 Then
			If(arrInjection(ijtI) = "select" Or arrInjection(ijtI) = "insert" Or arrInjection(ijtI) = "update" Or arrInjection(ijtI) = "drop" Or arrInjection(ijtI) = "delete") Then
				If arrInjection(ijtI) = "select" And (InStr(LCase(str), "from") > 0 Or Instr(Lcase(str), "into") > 0) Then
					ijtTnf = False
				ElseIf(arrInjection(ijtI) = "update" And InStr(LCase(str), "set") > 0) Then
					ijtTnf = False
				ElseIf(arrInjection(ijtI) = "insert" And InStr(LCase(str), "into") > 0) Then
					ijtTnf = False
				ElseIf(arrInjection(ijtI) = "delete" And InStr(LCase(str), "from") > 0) Then
					ijtTnf = False
				ElseIf(arrInjection(ijtI) = "drop") Then
					ijtTnf = False
				End If
			Else
				ijtTnf = False
			End If
		End If
		If Not ijtTnf Then MsgBoxHistoryBack "올바른 경로의 접근이 아닙니다."
	Next
	InjectionCheck = str
End Function
%>