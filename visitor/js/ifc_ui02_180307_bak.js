/****************************************************
## 배너 슬라이드
****************************************************/
var swipeSlidFn = function(wrap, options) {
	var options = options || {};
	var $wrap = $(wrap);
	var $swipe_wrapper = $('.swiper-wrapper', $wrap);
	var $swipe_slide = $('.swiper-slide', $swipe_wrapper);
	var $controllBox = $('.controllBox', $wrap);

	var $pageNav = options.pageNav ? {
		el : '.navBox'
		, type : 'custom'
		, renderCustom: function (swiper, current, total) {
			return current + ' of ' + total;;
		}
	} : {};

	var $dirNav = options.dirNav ? {
		nextEl : '.swipeNext'
		, prevEl : '.swipePrev'
	} : {};

	var initFn = function(target) {
		var $this = target;
		$($this.slides).css('width', $($this.$el).outerWidth());
		$($this.$wrapperEl).css('width', $($this.$el).outerWidth() * $this.slides.length);
	}

	var swipeFn = new Swiper($wrap, {
		loop : options.loop || false
		, speed : 800
		, allowTouchMove : false
		, pagination: $pageNav
		, navigation : $dirNav
		, on : {
			init : function() {
				initFn(this);
			}
			, transitionEnd : function() {
				$swipe_slide.attr('aria-hidden', 'true').eq(this.realIndex).attr('aria-hidden', 'false');
			}
			, resize : function() {
				initFn(this);
			}
		}
	});

	return {
		update : function() {
			swipeFn.update();
		}
		, destroy : function(ince, styleC) {
			swipeFn.destroy(ince, styleC);
		}
	}

}


/****************************************************
## 월 생성 스크립트 var 
****************************************************/
var selectYM = function(wrap, viewNum) {
	var $wrap = $(wrap),
		  $textYM = $wrap.parents('.selectYM').find('.textYM');
	var $viewN = viewNum;
	var $today = new Date(),
		  $year = $today.getFullYear(),
		  $mon = $today.getMonth()+1;
	var $html = '';
	
	var setUp = function() {
		$('.year', $textYM).text($year);
		$('.month', $textYM).text($mon);
		
		for(var i=0; i < $viewN; i++) {
			var nowM = $mon + i;
			
			if(i == 0) {
				$html += '<li class="on">';
			}else{
				$html += '<li>'
			}
			
			if(nowM%12 == 0) {
				$html += '<button type="button" ';
				$html += 'data-year="' +  $year + '">';
				$html += 12;
				$html += '</button>';
				$year += 1;
			}else{
				$html += '<button type="button" ';
				$html += 'data-year="' +  $year + '">';
				$html += nowM%12;
				$html += '</button>';
			}
			$html += '</li>';
		}
		
		$wrap.html($html);
	}
	setUp();

	return{
		  getYear : function() {return $year;}
		, getMonth : function() {return $mon;}
		, chYear : function(val) {
			$year = val;
		}
		, chMonth : function(val) {
			$mon = val;
		}
	}
}

/****************************************************
## 달력 생성 스크립트
****************************************************/
function calendar(wrapper, options) {
	var options = options || {}
	var wrapper = $(wrapper);
	var wrapW = wrapper.outerWidth();
	var itemW = wrapW/7;
	var today, year, mon, nowDay, firstDay, firstMon, totalDay, rowNum, totalCell, nowMon ,realMon, realYear;
	var tempDay = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]; // 1월부터 12월까지 일수를 배열로 저장한다.
	var weekText = options.weekText || ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일']; // 요일을 배열로 저장한다.
	
	function setup() {
		today = new Date(); // 날짜 객체 생성;
		year = today.getFullYear(); // 날짜 객체에서 year만 불러온다.
		mon = today.getMonth(); // 날짜 객체에서 month만 불러온다.
		nowMon = mon;
		nowDay = today.getDate();
	
		realYear = year;
		realMon = mon;
	
		dayCalculate (); //2월 윤년 평년에 따른 일 계산
		writeHtml();
	}
	
	function dayCalculate () {
		firstDay = new Date(year, mon, 1); // firstDay변수에 현재 년,월에 1일 날짜의 객체를 생성한다.
		firstMon = firstDay.getDay(); // 현재 년, 월에 1일 날짜의 요일값을 불러온다. (일:0, 월:1, 화:2, 수:3, 목:4, 금:5, 토:6)
	
		if(((year%4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
			tempDay[1] = 29; // 윤년은 29일
		}else{
			tempDay[1] = 28; // 평년은 28일
		}
	
		totalDay = tempDay[mon]; // 현재월에 전체일수를 구한다.
	}
	
	function writeHtml() {
		rowNum = Math.ceil((firstMon + totalDay)/7); // 달력에 필요한 줄(행) 수를 구하는 공식 = 전체셀(칸) / 7
		totalCell = rowNum*7; // 전체 셀(칸)수
	
		$('.cldMonth .date').text(year + '년 ' + (mon+1)+'월');
		var txtCalendar = '<table>';
		txtCalendar += '<thead><tr>';
	
		for(var i=0; i<weekText.length; i++) {
			if(i == 0) {
				txtCalendar += '<th class="sun" style="width:';
				txtCalendar += itemW;
				txtCalendar += 'px">';
			}else if(i == weekText.length-1) {
				txtCalendar += '<th class="sat" style="width:';
				txtCalendar += itemW;
				txtCalendar += 'px">';
			}else{
				txtCalendar += '<th style="width:';
				txtCalendar += itemW;
				txtCalendar += 'px">';
			}
			txtCalendar += weekText[i];
			txtCalendar += '</th>';
		}
		txtCalendar += '</tr></thead><tbody>';
	
		var dayNum = 1; // 일을 나타낼때사용
		var blankNum = 0; // 달력의 빈칸을 나타낼때 사용
	
		for(var j=1; j<=rowNum; j++) {
			txtCalendar += '<tr>';
			for(var k=1; k<=7; k++) {
				if(blankNum < firstMon) {
					txtCalendar += '<td class="empty"><span></span></td>';
					blankNum++;
				}else if(dayNum <= totalDay) {
					if(nowDay == dayNum) {
						if(k == 1) {
							txtCalendar += '<td class="sun today"><span>';
							txtCalendar += dayNum;
							txtCalendar += '<em>Today</em></span></td>';
						}else if(k == 7) {
							txtCalendar += '<td class="sat today"><span>';
							txtCalendar += dayNum;
							txtCalendar += '<em>Today</em></span></td>';
						}else{
							txtCalendar += '<td class="today"><a href="#">';
							txtCalendar += dayNum;
							txtCalendar += '<em>Today</em></a></td>';
						}
					}else if(nowDay > dayNum) {
						if(k == 1) {
							txtCalendar += '<td class="sun"><span>';
							txtCalendar += dayNum;
							txtCalendar += '</span></td>';
						}else if(k == 7) {
							txtCalendar += '<td class="sat"><span>';
							txtCalendar += dayNum;
							txtCalendar += '</span></td>';
						}else{
							txtCalendar += '<td class="disabled"><span>';
							txtCalendar += dayNum;
							txtCalendar += '</span></td>';
						}
					}else{
						if(k == 1) {
							txtCalendar += '<td class="sun"><span>';
							txtCalendar += dayNum;
							txtCalendar += '</span></td>';
						}else if(k == 7) {
							txtCalendar += '<td class="sat"><span>';
							txtCalendar += dayNum;
							txtCalendar += '</span></td>';
						}else{
							txtCalendar += '<td><a href="#">';
							txtCalendar += dayNum;
							txtCalendar += '</a></td>';
						}
					}
					dayNum++;
				}else{
					txtCalendar += '<td class="empty"><span></span></td>';
					blankNum++;
				}
			}
			txtCalendar += '</tr>';
		}
		txtCalendar += '</tbody></table>';
	
		wrapper.append(txtCalendar);
	}
	setup();
	
	return {
		reDrawFn : function(setY, setM) {
			year = setY;
			mon = setM-1;
			nowMon = mon;
			
			if(realMon == mon && realYear == year) {
				nowDay = today.getDate();
			}else {
				nowDay = 0;
			}
			
			dayCalculate ();
			wrapper.empty();
			writeHtml();
		},
		next : function() {
			if(nowMon + 1 > 11) {
				year = year + 1;
				mon = 0;
				nowMon = mon;
				if(realMon == mon && realYear == year) {
					nowDay = today.getDate();
				}else {
					nowDay = 0;
				}
			}else{
				mon = nowMon + 1; 
				nowMon = mon;
				if(realMon == mon && realYear == year) {
					nowDay = today.getDate();
				}else {
					nowDay = 0;
				}
			}
	
			dayCalculate ();
			wrapper.empty();
			writeHtml();
		},
		prev : function() {
			if(nowMon - 1 < 0) {
				year = year - 1;
				mon = 11
				nowMon = mon;
				if(realMon == mon && realYear == year) {
					nowDay = today.getDate();
				}else {
					nowDay = 0;
				}
			}else{
				mon = nowMon - 1; 
				nowMon = mon;
				if(realMon == mon && realYear == year) {
					nowDay = today.getDate();
				}else {
					nowDay = 0;
				}
			}
	
			dayCalculate ();
			wrapper.empty();
			writeHtml();
		},
		getDay : function() {return nowDay;}
	}
}