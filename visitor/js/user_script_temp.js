$(function() {

	/* add new admin section */
	$("#chk_as_id").click(function() {
		chk_new_id();
	});
	
	$("#add_admin").click(function() {
		add_new_admin();
	});
	
	$("#as_name").keypress(function(e) {
	    if (e.which == 13)
	    {
	    	if ($(this).val().length == 0)
	    	{
		    	alert("이름을 입력 해 주세요");
		    	$(this).focus();
		    	return;
	    	}
	    	
	    	$("#as_id").focus();
	    }
    });
    
    $("#as_id").keypress(function(e) {
	    if (e.which == 13)
	    {
	    	if ($(this).val().length == 0)
	    	{
		    	alert("아이디를을 입력 해 주세요");
		    	$(this).focus();
		    	return;
	    	}
	    	
	    	var id_chk_result = $("#id_chk_result").val();
	    	
	    	if (id_chk_result !== 'true')
			{
				alert("아이디 중복을 확인해주세요");
				return;
			}
	    	
	    	$("#as_pw").focus();
	    }
    });
    
    $("#as_pw").keypress(function(e) {
	    if (e.which == 13)
	    {
	    	if ($(this).val().length == 0)
	    	{
		    	alert("비밀번호를 입력 해 주세요");
		    	$(this).focus();
		    	return;
	    	}
	    	
	    	$("#as_pw_chk").focus();
	    }
    });
    
    $("#as_pw_chk").keypress(function(e) {
	    if (e.which == 13)
	    {
	    	if ($(this).val().length == 0)
	    	{
		    	alert("비밀번호를 한번 더 입력 해 주세요");
		    	$(this).focus();
		    	return;
	    	}
	    	
	    	if ($("#as_pw").val() != $(this).val())
	    	{
		    	alert("비밀번호가 틀립니다");
		    	$(this).focus();
		    	return;
	    	}
	    	
	    	$("#as_tel").focus();
	    }
    });
    
    $("#as_tel").keypress(function(e) {
	    if (e.which == 13)
	    {
	    	$("#as_email").focus();
	    }
    });
    
    $("#as_email").keypress(function(e) {
	    if (e.which == 13)
	    {
	    	add_new_admin();
	    }
    });
    /* //add new admin section */
    
    /* edit admin section */
    $("#edit_admin").click(function() {
		edit_admin();
	});
	
	$("#del_admin").click(function() {
		var select = "";
		
		if ($("#as_num").length > 0)
		{
			select = $("#as_num").val() + "|";
		}
		else
		{
			if ($("#table_list>tbody").find("input:checkbox:checked").length == 0)
			{
				alert("선택된것이 없네요");
				return;
			}

			$("#table_list>tbody").find("input:checkbox:checked").each(function() {
				select += $(this).val() + "|";
			});
		}
		
		if (!confirm("정말로 지우실거에요?"))
				return;
			
		if (!confirm("한번 지우면 끝이에요!"))
			return;
		
		del_admin(select);
	});
    /* //edit admin section */
    
    /* press list */
    $("#add_press").click(function() {
		if (!confirm("보도자료를 입력 하시겠습니까?"))
			return;
	
		add_edit_press("add");
	});
	
	$("#edit_press").click(function() {
		if (!confirm("보도자료를 수정 하시겠습니까?"))
			return;
	
		add_edit_press("edit");
	});
	
	$("#del_press").click(function() {
		var select = "";
		
		if ($("#p_num").length > 0)
		{
			select = $("#p_num").val() + "|";
		}
		else
		{
			if ($("#table_list>tbody").find("input:checkbox:checked").length == 0)
			{
				alert("선택된것이 없습니다.");
				return;
			}

			$("#table_list>tbody").find("input:checkbox:checked").each(function() {
				select += $(this).val() + "|";
			});
		}
		
		if (!confirm("보도자료를 지우시겠습니까?"))
				return;
			
		del_press(select);
	});
    /* //press list */
    
	/* facility gallary */
    $("#add_fg").click(function() {
		if (!confirm("갤러리를 입력 하시겠습니까?"))
			return;
	
		add_edit_fg("add");
	});
	
	$("#edit_fg").click(function() {
		if (!confirm("갤러리를 수정 하시겠습니까?"))
			return;
	
		add_edit_fg("edit");
	});
	
	$("#del_fg").click(function() {
		var select = "";
		
		if ($("#fg_num").length > 0)
		{
			select = $("#fg_num").val() + "|";
		}
		else
		{
			if ($("#table_list>tbody").find("input:checkbox:checked").length == 0)
			{
				alert("선택된것이 없습니다.");
				return;
			}

			$("#table_list>tbody").find("input:checkbox:checked").each(function() {
				select += $(this).val() + "|";
			});
		}
		
		if (!confirm("갤러리를 지우시겠습니까?"))
				return;
			
		del_fg(select);
	});
	/* //facility gallary */
});

// check entered id is exist or not
function chk_new_id()
{
	as_id = $("#as_id").val();
	if (as_id.length == 0)
	{
		alert("아이디를 입력해 주세요");
		$("#as_id").focus();
		return;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "/overseer/ajax/admin_proc.asp",
		dataType : "json",
		data : ({"as_id" : as_id, "mode" : "chk_id"}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
			
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					alert("아이디 사용 가능해요");
					$("#as_pw").focus();
					$("#chk_as_id").addClass("btn-success").removeClass("btn-default").removeClass("btn-warning");
					$("#id_chk_result").val("true");
					$("#id_chk_store").val(as_id);
				}
				else if (data.result == 1)
				{
					alert("아이디가 존재하네요");
					$("#as_id").focus();
					$("#chk_as_id").removeClass("btn-success").removeClass("btn-default").addClass("btn-warning");
					$("#id_chk_result").val("");
					$("#id_chk_store").val("");
				}
				else
				{
					alert(error_msg);
					$("#as_id").focus();
					$("#chk_as_id").removeClass("btn-success").removeClass("btn-default").addClass("btn-danger");
					$("#id_chk_result").val("");
					$("#id_chk_store").val("");
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}

// add new admin user
function add_new_admin()
{
	var as_id = $("#as_id").val();
	var as_name = $("#as_name").val();
	var as_pw = $("#as_pw").val();
	var as_pw_chk = $("#as_pw_chk").val();
	var as_tel = $("#as_tel").val();
	var as_email = $("#as_email").val();
	var id_chk_result = $("#id_chk_result").val();
	var id_chk_store = $("#id_chk_store").val();
	
	if (as_id.length == 0)
	{
		alert("아이디가 비었어요");
		$("#as_id").focus();
		return;
	}
	
	if (id_chk_result !== 'true' || as_id != id_chk_store)
	{
		alert("아이디 중복을 확인해주세요");
		return;
	}
	
	if (as_name.length == 0)
	{
		alert("이름이 비었어요");
		$("#as_id").focus();
		return;
	}
	
	if (as_pw.length == 0)
	{
		alert("비밀번호가 비었어요");
		$("#as_pw").focus();
		return;
	}
	
	if (as_pw_chk.length == 0)
	{
		alert("비밀번호를 한번 더 입력 해 주세요");
		$("#as_pw_chk").focus();
		return;
	}
	
	if (as_pw !== as_pw_chk)
	{
		alert("비밀번호가 틀렸어요");
		$("#as_pw_chk").focus();
		return;
	}
	
	if (as_tel.length > 0)
	{
		if (!as_tel.match(/[0-9]{1,12}/g))
		{
			alert("연락처는 최대 12자리 숫자만 입력 가능합니다.")
			return;
		}	
	}
	
	if (as_email.length > 0)
	{
		var emailReg = new RegExp(/^([0-9a-zA-Z_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/);
		if (!emailReg.test(as_email))
		{
			alert("이메일 주소가 잘못되었습니다..");
			$("#as_email").focus();
			return;
		}
	}
	
	if (!confirm("등록 하시겠습니까?"))
		return;
	
	as_pw = hex_md5(as_pw);
	
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "/overseer/ajax/admin_proc.asp",
		dataType : "json",
		data : ({
					"mode" : "add", 
					"as_id" : as_id, 
					"as_name" : as_name, 
					"as_pw" : as_pw,
					"as_tel" : as_tel,
					"as_email" : as_email
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					alert("추가에 성공했어요");
					get_link = $("#list_link_hidden").val();
					location.href = get_link;
				}
				else
				{
					alert(error_msg);
					$("#as_id").focus();
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}

// add new admin user
function edit_admin()
{
	var as_num = $("#as_num").val();
	var as_name = $("#as_name").val();
	var as_pw = $("#as_pw").val();
	var as_pw_chk = $("#as_pw_chk").val();
	var as_tel = $("#as_tel").val();
	var as_email = $("#as_email").val();
	var id_chk_result = $("#id_chk_result").val();
	var id_chk_store = $("#id_chk_store").val();
	
	if (as_name.length == 0)
	{
		alert("이름이 비었어요");
		$("#as_id").focus();
		return;
	}
	
	if (as_pw.length != 0)
	{
		if (as_pw_chk.length == 0)
    	{
	    	alert("비밀번호를 한번 더 입력 해 주세요");
	    	$(this).focus();
	    	return;
    	}
    	
    	if (as_pw != as_pw_chk)
    	{
	    	alert("비밀번호가 틀립니다");
	    	$(this).focus();
	    	return;
    	}
	}
	
	if (as_tel.length > 0)
	{
		if (!as_tel.match(/[0-9]{1,12}/g))
		{
			alert("연락처는 최대 12자리 숫자만 입력 가능합니다.")
			return;
		}	
	}
	
	if (as_email.length > 0)
	{
		var emailReg = new RegExp(/^([0-9a-zA-Z_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/);
		if (!emailReg.test(as_email))
		{
			alert("이메일 주소가 잘못되었습니다..");
			$("#as_email").focus();
			return;
		}
	}
	
	if (!confirm("수정 하시겠습니까?"))
		return;
	
	as_pw = hex_md5(as_pw);
	
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "/overseer/ajax/admin_proc.asp",
		dataType : "json",
		data : ({
					"mode" : "edit", 
					"as_num" : as_num, 
					"as_name" : as_name, 
					"as_pw" : as_pw,
					"as_tel" : as_tel,
					"as_email" : as_email
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					alert("어드민 정보를 수정했어요.");
					location.href = location.href;
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}

function del_admin(select)
{
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "/overseer/ajax/admin_proc.asp",
		dataType : "json",
		data : ({
					"mode" : "del", 
					"select" : select
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					alert("어드민 계정을 삭제했어요.");
					if ($("#as_num").length > 0)
					{
						location.href = $("#list_link_hidden").val();
					}
					else
					{
						location.href = location.href;
					}
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}

function add_edit_press(mode)
{
	var p_num = $("#p_num").val();
	var p_group = $(".p_group:checked").val();
	//var p_img = $("#p_img").val();
	//var p_summary = $("#p_summary").val();
	var p_title = $("#p_title").val();
	var p_text = CKEDITOR.instances['p_text'].getData();
		
	if (p_title.length == 0)
	{
		alert("제목을 입력 해 주세요");
		$("#p_title").focus();
		return;
	}
	
	if (p_text.length == 0)
	{
		alert("내용을 입력 해 주세요");
		$("#p_text").focus();
		return;
	}
	
	var img = "";
	$(".img_section").find("img").each(function() {
		img += $(this).attr("src") + "|";
	})
	
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "/overseer/ajax/press_release_proc.asp",
		dataType : "json",
		data : ({
					"mode" : mode, 
					"p_num" : p_num,
					"p_group" : p_group,
					"p_title" : p_title,
					//"p_img" : p_img,
					//"p_summary" : p_summary,
					"p_text" : p_text,
					"img" : img
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					if (mode == "add")
					{
						alert("보도자료를 등록했습니다.");
						location.href = $("#list_link_hidden").val();
					}
					else
					{
						alert("보도자료를 수정했습니다.");
						location.href = location.href;
					}
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}

function del_press(select)
{
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "/overseer/ajax/press_release_proc.asp",
		dataType : "json",
		data : ({
					"mode" : "del", 
					"select" : select
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					alert("보도자료를 삭제했습니다.");
					if ($("#p_num").length > 0)
					{
						location.href = $("#list_link_hidden").val();
					}
					else
					{
						location.href = location.href;
					}
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}

function add_edit_fg(mode)
{
	var fg_num = $("#fg_num").val();
	var fg_title_ko = $("#fg_title_ko").val();
	var fg_title_en = $("#fg_title_en").val();
	var fg_list_title = $("#fg_list_title").val();
	var fg_img = $("#fg_img").val();
		
	if (fg_title_ko.length == 0)
	{
		alert("제목을 입력 해 주세요");
		$("#fg_title_ko").focus();
		return;
	}
	
	if (fg_img.length == 0)
	{
		alert("이미지를 넣어 주세요");
		$("#fg_img").focus();
		return;
	}
	
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "/overseer/ajax/facility_gallary_proc.asp",
		dataType : "json",
		data : ({
					"mode" : mode, 
					"fg_num" : fg_num,
					"fg_title_ko" : fg_title_ko,
					"fg_title_en" : fg_title_en,
					"fg_list_title" : fg_list_title,
					"fg_img" : fg_img
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					if (mode == "add")
					{
						alert("갤러리를 등록했습니다.");
						location.href = $("#list_link_hidden").val();
					}
					else
					{
						alert("갤러리를 수정했습니다.");
						location.href = location.href;
					}
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}

function del_fg(select)
{
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "/overseer/ajax/facility_gallary_proc.asp",
		dataType : "json",
		data : ({
					"mode" : "del", 
					"select" : select
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					alert("갤러리를 삭제했습니다.");
					if ($("#fg_num").length > 0)
					{
						location.href = $("#list_link_hidden").val();
					}
					else
					{
						location.href = location.href;
					}
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}