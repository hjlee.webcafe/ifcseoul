$(function() {
	// for submenu
	// get file name
	var url = location.href;
	var url_array = url.split("/");

    // for prevent duplicate check
    var isMatch = false;

	url_array = url_array[url_array.length - 1].split("?");
	var file_name_array = url_array[0].split(".");
	var file_name = file_name_array[0];
    var sub_file_name = file_name.replace(/\_[0-9]{1,}$/, "");   // remove numbers for current depth submenu

	if (file_name.length == 0 || file_name == 'MN_00_00') {
		$(".menuList > li:first-child > a").addClass("on")
        isMatch = true;
    }

	// for regular express check
    var reg_patt = new RegExp(sub_file_name)
    var snb_array = new Array();

    //PC버전 메뉴 컨트롤
    if (!isMatch) {
        $("#snb").find("a").each(function() {

            $(this).removeClass("on")

            //check file name
            var link = $(this).attr("href");
            if (reg_patt.test(link) && !isMatch)
            {

                $(this).addClass("on");

                isMatch = true;

                // lineMap을 표시하기 위한 정보저장
                var root_link = $(this).closest("ul").find("a:eq(0)").attr("href");
                snb_array.push('<a href="' + root_link + '">' + $("#snb").find(".tit").html().replace("<br>", " ") + '</a>');
                snb_array.push('<a href="' + link + '">' + $(this).text() + '</a>');
            }
        });
    }

    // Mobile버전 메뉴 컨트롤
    // for regular express check
    var reg_patt = new RegExp(file_name)
    $(".menuAllList li > a").siblings("ul").slideUp();
    isMatch = false;

    $(".menuAllList li > a").each( function() {

        $(this).removeClass("on")

        //check file name
        var link = $(this).attr("href");

        if (reg_patt.test(link) && !isMatch)
        {
            $(this).addClass("on");

            isMatch = true

            $(this).parent("li").parent("ul").slideDown();
            $(this).parent("li").parent("ul").siblings("a").addClass("on");

            $(this).parent("li").parent("ul").parent("li").parent("ul").slideDown();
            $(this).parent("li").parent("ul").parent("li").parent("ul").siblings("a").addClass("on");
        }
    });
	// for submenu end

	if ($(".tabWrap").length > 0)
	{
		snb_array.push($(".tabWrap").find(".on").text());
	}

	var text = "<li><span><a href=\"/\">Home</a></span></li>";
	for (var i = 0; i < snb_array.length; i++)
	{
		snb_array[i] = snb_array[i].replace(/\<\/{0,1}br.{0,1}\/{0,1}\>/gi, " ");
		if (i == (snb_array.length-1))
		{
			if (snb_array[i] != snb_array[i-1])
				text += "<li class=\"last\">" + snb_array[i] + "</li>";
		}
		else
			text += "<li>" + snb_array[i] + "</li>";
	}

	$(".lineMap").html(text);

	// for family site
	$("#family_site").change(function() {
		var link = $(this).val();
		if (link.length == 0)
			return;
		window.open(link);
	});
	// for family site end

	// for language change
	$(".lang").click(function() {
		var current_type = $(this).attr("href").replace("#", "");
		var link_array = location.href.split("/");

		if (current_type == "ko")
		{
			lang = "en";
		}
		else
		{
			lang = "ko";
		}

		new_link = "/" + lang + "/" + link_array[link_array.length-1];
		location.href = new_link;
	})
	// end for language change

	// for type select
	$(".selectList").click(function() {
		$(this).find("p").toggleClass("open");
		$(this).find("ul").toggle();
	});
	// end for type select

	// prevent right click
	$(document).bind("contextmenu",function(){
       return false;
    });
})
