<!--#include file = "default_control.asp" -->
<%
Dim strUserId, strUserName, strUserAuth

strUserId = Session("F_UID")
strUserName = Session("F_USER_NAME")
strUserAuth = Session("F_AUTHORITY")
%>
<!--#include file = "index_header.asp" -->
<div class="visualBox">
	<!-- <span><img src="../../img/com/visual_login.png" alt="" /></span> -->
	<div class="visualTxt">
		<span class="txt01">방문자 예약신청,월주차 등록</span>
		<span class="txt02">
			입주자 여러분들의 성곡적인 비즈니스 미팅을 위한 방문자 예약신청<br />보다  쉽고 편리한 월 주차등록 서비스를 제공합니다.
		</span>
	</div>
</div>
<div class="contBody">
	<div class="visitReserve">
		IFC방문예약시스템은 방문자및 입주사의 출입 편의를 쉽고 간편하게 제공합니다. IFC입주사 임직원의 월정기권및 선불권 구매 절차를 온라인으로 간편하고 안전하게 제공합니다.
	</div>

	<div class="btnWrap">
		<a href="https://visitor.ifcseoul.com/mng/login/gate.do" target="_blank" class="nbtnG">방문자 및 포탈 사이트</a>
		<a href="https://parking.ifcseoul.com/mng/login/login.do" target="_blank" class="nbtnG04">IFC입주사 월정기권 및 선불권</a>
	</div>

</div>
<!--#include file = "index_footer.asp" -->

<!-- POPUP COMMING SOON -->
<div id="layer1" class="pop-layer">
    <div class="pop-container">
        <div class="pop-conts">
            <!--content //-->
           <img src="img/visitor_comming_soon.jpg">
            <div class="btn-r">
                <a href="#" class="btn-layerClose">Close</a>
            </div>
            <!--// content-->
        </div>
    </div>
</div>
<!--// POPUP COMMING SOON -->

<script type="text/javascript">
$('.pop-layer').find('a.btn-layerClose').click(function(){
  $('.pop-layer').fadeOut(); // 닫기 버튼을 클릭하면 레이어가 닫힌다.
  return false;
});
</script>
