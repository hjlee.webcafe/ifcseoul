<!DOCTYPE html>
<html xml:lang="ko" lang="ko">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<!--<meta http-equiv='refresh' content=='0; url=https://ifcseoul.com/index.asp'target='_top'>-->
    <title>IFC Seoul</title>
    <link rel="stylesheet" type="text/css" href="css/base_ko.css" />
    <link rel="stylesheet" type="text/css" href="css/layout.css" />
    <link rel="stylesheet" type="text/css" href="css/common.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/animation.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	<!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script> -->
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.event.drag-1.5.min.js"></script>
    <script type="text/javascript" src="js/jquery.touchSlider.js"></script>
	<script type="text/javascript" src="js/jquery.bxslider.js"></script>
    <script type="text/javascript" src="js/ifc_ui.js"></script>
    <script type="text/javascript" src="js/ifc_custom.js"></script>
	<!-- /* 퍼블리싱 추가 파일 */ -->
	<script type="text/javascript" src="js/swiper.min.js"></script>
    <script type="text/javascript" src="js/ifc_ui02.js"></script>

	<script type="text/javascript">
	$(document).ready(function() {

	});
	</script>

		<!--[if IE]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<!--[if lt IE 9]>
			<script type="text/javascript" src="../../js/respond.min.js"></script>
			<script type="text/javascript">
				var ltIE9 = true;
			</script>
		<![endif]-->

	</head>
	<!--[if lt IE 7]><body class="msie ie6 lt-ie9 lt-ie8 lt-ie7 lt-css3"><![endif]-->
	<!--[if IE 7]>   <body class="msie ie7 lt-ie9 lt-ie8 lt-css3"><![endif]-->
	<!--[if IE 8]>   <body class="msie ie8 lt-ie9 lt-css3"><![endif]-->
	<!--[if IE 9]>   <body class="msie ie9 css3"><![endif]-->
	<!--[if gt IE 9]><!-->
	<body>
	<!--<![endif]-->
	<div id="wrap" class="">
		<div class="snbShadow"></div>
		<nav id="snb">
			<span class="tit v01">방문자 예약신청	</span>
			<ul class="menuList">
				<li><a href="visitor.asp">방문자 예약신청	<span></span></a></li>
			</ul>
		</nav>
		<!-- //snb -->
		<div id="containerWrap">
		<header>
			<!-- u_skip -->
			<div id="u_skip"></div>
			<!-- //u_skip -->
			<!-- header -->
			<div id="header">
				<div class="container">
					<a href="#none" class="btnMenu">홈</a>
					<h1><a href="/ko/MN_00_00.asp"><span>IFC Seoul</span></a></h1>
					<a href="HF_01_00.asp" class="btnMap">위치 및 지도</a>
				</div>
				<div id="gnb">
					<ul>
						<li><a href="/ko/MN_00_00.asp">홈</a></li>
						<li><a href="/ko/HF_01_00.asp">위치 및 지도</a></li>
						<li><a href="/ko/HF_02_00.asp">Contact</a></li>
						<% If strUserId&"" <> "" Then%>
							<li><a href="../forum/logout.asp">로그아웃</a></li>
						<% Else %>
							<li><a href="../forum/index.asp">입주사 로그인</a></li>
						<% End If %>
					</ul>
				</div>
				<div id="lnb">
					<ul>
						<li><a href="/ko/IS_01_00.asp">IFC 서울</a></li>
						<li><a href="/ko/BD_01_00.asp">빌딩</a></li>
						<li><a href="/ko/NM_01_00.asp">주변 지역 안내</a></li>
						<li><a href="/ko/NE_01_00.asp">소식</a></li>
						<li><a href="visitor.asp">방문자 예약신청</a></li>
						<li><a href="../forum/forum_info.asp">The Forum at IFC 예약</a></li>
					</ul>
				</div>
			</div>
			<!-- //header -->
		</header>
		<section>
			<!-- container -->
			<div id="container">
				<!-- lineMap -->
				<div class="lineMapWrap">
					<ul class="lineMap"></ul>
				</div>
				<!-- //lineMap -->
				<div id="content">