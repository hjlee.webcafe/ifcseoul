
				</div>
			</div>
			<!-- //container -->
		</section>
		<!-- footer -->
		<footer>
			<div id="footer">
				<div class="container top">
					<div class="lCon">
						<ul>
							<li><a href="/ko/HF_06_00.asp">부동산 고지</a></li>
							<li><a href="/ko/HF_07_00.asp">사이트 맵</a></li>
						</ul>
					</div>
					<div class="rCon">
						<!-- select id="family_site">
							<option value="">Family Site</option>
							<option value="http://www.ifcmallseoul.com">IFC Mall</option>
							<option value="http://www.conradseoul.co.kr">CONRAD Seoul</option>
							<option value="">AIG GRE</option>
						</select -->
						<div class="selectList">
							<p><a href="#none">FAMILY SITE</a></p>
							<ul class="hide">
								<li><a href="http://www.ifcmallseoul.com" target="_blank" title="Open New Window">IFC MALL</a></li>
								<li><a href="http://www.conradseoul.co.kr" target="_blank" title="Open New Window">CONRAD Seoul</a></li>
								<li><a href="https://www.brookfield.com/" target="_blank" title="Open New Window">Brookfield</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="container bottom">
					<address>서울특별시 영등포구 여의도동 국제금융로 10, 07326</address>
					<p class="copy">COPYRIGHT &copy; 2014 IFC SEOUL ALL RIGHT RESERVED.</p>
				</div>
			</div>
		</footer>
		<!-- //footer -->
		</div>
	</div><!-- //wrap -->
</body>
</html>
