<!--#include file = "default_control.asp" -->
<%
Dim strUserId, strUserName, strUserAuth

strUserId = Session("F_UID")
strUserName = Session("F_USER_NAME")
strUserAuth = Session("F_AUTHORITY")
%>
<!--#include file = "smartsuites_header.asp" -->
<div class="visualBox SmartSuites">
    <!-- <span><img src="../../img/com/smartsuites_bg.png" alt="" /></span> -->
    <div class="visualTxt">
        <span class="txt01">The Smart Suites at IFC</span>
        <span class="txt02">
            The Smart Suites at IFC는 한국 최고의 오피스 IFC Seoul이 만든 프리미엄 소형 퍼니쉬드 오피스입니다.
        </span>
    </div>
</div>
<div class="contBody">
    <div class="pTitle">
        <h2>The Smart Suites at IFC, 프리미엄 소형 퍼니쉬드 오피스</h2>
        <span class="titCopy02">The Smart Suites at IFC 는 인테리어 및 가구부터 업무에 필요한 환경이 미리 조성되어 있고 고급스러우면서 독립된 사무공간을 제공하며, <br/>보다 유연한 임대차 계약이 가능한 프리미엄 소형 퍼니쉬드 오피스입니다.</span>
    </div>

    <ul class="mroomcInfo">
        <li class="info01">
            <strong>위치</strong>
            <span>Two IFC 12층</span>
        </li>
        <li class="info02">
            <strong>문의</strong>
            <span>02) 6137 - 2121</span>
        </li>
    </ul>

    <div class="btnWrap">
        <a href="https://www.sifcsmartsuites.com" target="_blank" class="nbtnG04" onclick="gtag('event', '버튼클릭', {'event_category':'The Smart Suites at IFC', 'event_label':'Smart Suites 더 알아보기'});">더 알아보기</a>
    </div>

</div>
<!--#include file = "index_footer.asp" -->

<script type="text/javascript">
$('.pop-layer').find('a.btn-layerClose').click(function(){
  $('.pop-layer').fadeOut(); // 닫기 버튼을 클릭하면 레이어가 닫힌다.
  return false;
});
</script>
