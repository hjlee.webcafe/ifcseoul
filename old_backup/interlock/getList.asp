<%@CodePage = "65001" %>
<!-- #include file = "../thisLibs/dbfunc.asp" -->
<!-- #include file = "../thisLibs/checkParam.asp" -->
<!-- #include file = "../thisLibs/msgbox.asp" -->
<!-- #include file = "../thisLibs/removeCache.asp" -->
<%
Dim strQuery, objRs, objConn, arrRs
Dim intPage, strLanguage, strBoard, intPageSize, strDomain
Dim intCurCnt, intTotalCnt, intTotalpages, intLineNum, strThumbnail

intPageSize = CheckParamType("int", Trim(Request("pageSize")))
intPage = CheckParamType("int", Trim(Request("pageNum")))
strLanguage = CheckParamLimitedList(Trim(Request("lang")), Array("KOR","ENG"))
strBoard = CheckParamLimitedList(Trim(Request("type")), Array("Notice","News","Work"))
If intPage = "" Then intPage = 1

CheckCompulsoryData Array(strLanguage,strBoard)
strDomain = "http://" & Request.ServerVariables("HTTP_HOST") &"/updata"

Set objConn = OpenDBConnection()

strQuery = "Select Count(*) From tblBoard Where strBoard=? And strLanguage=?"
Set objRs = SetDatabaseByParam(objConn, strQuery, Array(strBoard, strLanguage), Null, "rs")
intTotalCnt = objRs(0)
If intTotalCnt > 0 Then 
  intNowPos = intPage * intPageSize
  intCurCnt = intTotalPages Mod intPageSize
  intTotalPages = Fix(intTotalCnt / intPageSize)
  If intTotalCnt Mod intPageSize > 0 Then intTotalPages = intTotalPages + 1
Else
  intTotalpages = 0
End If
objRs.Close

If intTotalCnt > 0 Then
  strQuery =  "Select Top "& intPageSize &" intSeq, strName, strTitle, dateRegist, strContentFile From tblBoard Where strBoard=? And strLanguage=? And intSeq Not In ("  &_
              "Select Top "& (intPage - 1) * intPageSize &" intSeq From tblBoard Where  strBoard=? And strLanguage=? Order By intSeq Desc) Order By intSeq Desc"
  
  Set objRs = SetDatabaseByParam(objConn, strQuery, Array(strBoard, strLanguage, strBoard, strLanguage), Null, "rs")
  If Not objRs.EOF Then arrRs = objRs.GetRows
  objRs.Close : Set objRs = Nothing
End If

objConn.Close : Set objConn = Nothing

Function getThumbnail(strFile)
  Dim result
  If strFile <> "" Then
    extIndex = InstrRev(strFile, ".")
    result = "Thumb_"& Left(strFile, extIndex) &"jpg"
  Else
    result = ""
  End If
  getThumbnail = result
End Function
%>
<?xml version="1.0" encoding="utf-8" ?>
<records totalCnt="<%=intTotalCnt%>" totalPage="<%=intTotalPages%>">
<%
If intTotalCnt > 0 Then
  intLineNum = intTotalCnt - (intPage - 1) * intPageSize
  For i = 0 To UBound(arrRs, 2)
%>
	<record idx="<%=arrRs(0,i)%>">
		<num><%=intLineNum%></num>
		<id><%=arrRs(1,i)%></id>
		<title><![CDATA[<%=arrRs(2,i)%>]]> </title>
		<date><%=Left(arrRs(3,i),10)%></date>
		<thumbUrl><%If strBoard="Work" Then Response.Write strDomain &"/"& strBoard &"/"& getThumbnail(arrRs(4,i))%></thumbUrl>
	</record>
<%
    intLineNum = intLineNum - 1
  Next
End If
%>
</records>