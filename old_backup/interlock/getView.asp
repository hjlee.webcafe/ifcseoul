<%@CodePage = "65001" %>
<!-- #include file = "../thisLibs/dbfunc.asp" -->
<!-- #include file = "../thisLibs/checkParam.asp" -->
<!-- #include file = "../thisLibs/msgbox.asp" -->
<!-- #include file = "../thisLibs/removeCache.asp" -->
<%
Dim strQuery, objRs, objConn, arrRs
Dim intIdx, intPage, strLanguage, strBoard
Dim strName, strTitle, strContentFile, strDownloadFile, dateRegist, strContent
Dim strContentFilePath, strDownloadFilePath, strDomain

intIdx = CheckParamType("int", Trim(Request("idx")))
intPage = CheckParamType("int", Trim(Request("pageNum")))
strLanguage = CheckParamLimitedList(Trim(Request("lang")), Array("KOR","ENG"))
strBoard = CheckParamLimitedList(Trim(Request("type")), Array("Notice","News","Work"))

CheckCompulsoryData Array(intIdx,strLanguage,strBoard)
strDomain = "http://" & Request.ServerVariables("HTTP_HOST")

Set objConn = OpenDBConnection()

strQuery = "Select strName, strTitle, strContentFile, strDownloadFile, dateRegist, strContent From tblBoard Where intSeq = ?"
Set objRs = SetDatabaseByParam(objConn, strQuery, Array(intIdx), Null, "rs")

If objRs.EOF Then
  MsgBoxHistoryBack "데이터가 존재하지 않습니다"
Else
  strName = Trim(objRs(0))
  strTitle = Trim(objRs(1))
  strContentFile = Trim(objRs(2))
  strDownloadFile = Trim(objRs(3))
  dateRegist = Left(Trim(objRs(4)),10)
  strContent = Trim(objRs(5))
  strContent = Replace(strContent, vbcrlf, "<br>")
  If strContentFile <> "" Then strContentFilePath = strDomain &"/updata/"& LCase(strBoard) &"/"& strContentFile
  If strDownloadFile <> "" Then strDownloadFilePath = strDomain &"/updata/"& LCase(strBoard) &"/"& strDownloadFile
End If

strQuery = "Update tblBoard Set intRead = intRead + 1 Where intSeq = ?"
SetDatabaseByParam objConn, strQuery, Array(intIdx), Null, "exec"

CloseDB objRs, objConn
%>
<?xml version="1.0" encoding="utf-8" ?>
<record idx="<%=intIdx%>">
		<num></num>
		<id><%=strName%></id>
		<title><![CDATA[<%=strTitle%>]]></title>
		<description><![CDATA[<%=strContent%>]]></description>
		<imgUrl><%=strContentFilePath%></imgUrl>
		<downUrl><%=strDownloadFilePath%></downUrl>
		<date><%=dateRegist%></date>
</record>