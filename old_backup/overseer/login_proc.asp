<!-- #include file = "../thisLibs/dbfunc.asp" -->
<!-- #include file = "../thisLibs/md5.asp" -->
<!-- #include file = "../thisLibs/checkParam.asp" -->
<!-- #include file = "../thisLibs/msgbox.asp" -->
<!-- #include file = "../thisLibs/removeCache.asp" -->
<%
Dim objConn, objRs, strQuery
Dim strID, strPW, strReturnUrl

With Request
  strID = GetTag2Text(Trim(CheckParamLength(.Form("id"),20)))
  strPW = GetTag2Text(Trim(CheckParamLength(.Form("pw"),20)))
  strReturnUrl = GetTag2Text(Trim(.Form("GoUrl")))
End With

CheckCompulsoryData Array(strID,strPW)

strQuery = "Select strName From tblManager Where strID=? And strPW=?"

Set objConn = OpenDBConnection()
Set objRs = SetDatabaseByParam(objConn, strQuery, Array(strID, MD5(strPW)), Null, "rs")

If objRs.EOF Then
  MsgBoxHistoryBack "등록되지 않은 아이디이거나 비밀번호가 틀립니다."
Else
  Session("ifc_mngID") = strID
  Session("ifc_mngName") = objRs(0)
End If

CloseDB objRs, objConn

If strReturnUrl <> "" Then Response.Redirect strReturnUrl Else Response.Redirect "publiclist.asp"
%>