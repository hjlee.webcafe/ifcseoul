<!-- #include file = "../thisLibs/dbfunc.asp" -->
<!-- #include file = "../thisLibs/func.asp" -->
<!-- #include file = "../thisLibs/uploadfunc.asp" -->
<!-- #include file = "../thisLibs/checkParam.asp" -->
<!-- #include file = "../thisLibs/msgbox.asp" -->
<!-- #include file = "../thisLibs/removeCache.asp" -->
<%
Dim objConn, objRs, strQuery
Dim intSeq, strBoard, strFileName
Dim fso, strPhysicalPath, objDownload

intSeq = CheckParamType("int",Trim(Request("idx")))
strBoard = CheckParamLimitedList(Trim(Request("type")),Array("Notice","News","Work"))
CheckCompulsoryData Array(intSeq, strBoard)

strQuery = "Select strDownloadFile From tblBoard Where intSeq=? And strBoard=?"
Set objConn = OpenDBConnection()
Set objRs = SetDatabaseByParam(objConn, strQuery, Array(intSeq, strBoard), Null, "rs")
strFileName = Trim(objRs(0))
CloseDB objRs, objConn
strPhysicalPath = Server.MapPath("/updata/"& strBoard &"/"& strFileName)

Set fso = Server.CreateObject("Scripting.FileSystemObject")
If Not fso.FileExists(strPhysicalPath) Then MsgBoxHistoryBack "해당파일이 삭제되었거나 존재하지 않습니다"

Set objDownload = Server.CreateObject("DEXT.FileDownload")
objDownload.Download strPhysicalPath, strFileName, True, False
Set objDownload = Nothing
%>