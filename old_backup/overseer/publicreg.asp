<!-- #include file = "../thisLibs/dbfunc.asp" -->
<!-- #include file = "../thisLibs/func.asp" -->
<!-- #include file = "../thisLibs/checkParam.asp" -->
<!-- #include file = "../thisLibs/msgbox.asp" -->
<!-- #include file = "../thisLibs/removeCache.asp" -->
<!-- #include file = "loginCheck.asp" -->
<%
Dim objConn, objRs, strQuery
Dim idx, pg, fld, kwd
Dim strMode, strLanguage, strTitle, strContentFile

idx = CheckParamType("int",Trim(Request("idx")))
pg = CheckParamType("int",Trim(Request("pg")))
fld = CheckParamLimitedList(Trim(Request("fld")),Array("strTitle","strContent"))
kwd = GetTag2Text(CheckParamLength(Trim(Request("kwd")),20))

If idx <> "" Then
  strMode = "Modify"
  strQuery = "Select strLanguage, strTitle, strContentFile From tblBoard Where strBoard='Work' And intSeq=?"
  Set objConn = OpenDBConnection()
  Set objRs = SetDatabaseByParam(objConn, strQuery, Array(idx), Null, "rs")
  If Not objRs.EOF Then
    strLanguage = Trim(objRs(0))
    strTitle = Trim(objRs(1))
    strContentFile = Trim(objRs(2))
  End If
  CloseDB objRs, objConn
Else
  strMode = "Regist"
End If
%>
<html>
<head>
  <!-- #include file = "inc/docHead.asp" -->
  <script type="text/javascript" src="common/js/valid.js"></script>
  <script type="text/javascript" src="common/js/board.js"></script>
  <script type="text/javascript" src="common/js/jquery-1.4.2.min.js"></script>
  <script type="text/javascript" src="common/js/reg_func.js"></script>
  <style type="text/css">
    #bgContainer {position:absolute; top:0; left:0; background-color: #000; display:none}
    #imgContainer {position:absolute; display:none}
	  .nwdiv {width:671px; height:50px; margin:25px 0px}
	  .filedel {height:25px; vertical-align:middle; padding-top:10px}
	  .filedel img {vertical-align:text-bottom; cursor:pointer}
  </style>
</head>
<body>
<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
	<!-- Top Area -->
  <!-- #include file="inc/top.asp" -->
  <!-- //Top Area -->
  
  <!-- Middle Area -->
  <tr valign="top">
        
    <!-- LNB -->
    <td width="320">            
    <!-- #include file="inc/lnb.asp" -->                   
    </td>
    <!-- //LNB -->

    <!-- contents -->
    <td>
        
      <!-- 여백 -->
      <table cellpadding="0" cellspacing="0" border="0">
        <tr><td><img src="images/trans.gif" height="49" alt="" /></td></tr>
      </table>
      <!-- //여백 -->
        
      <!-- top -->
		  <table border="0" cellpadding="0" cellspacing="0">  
        <tr><td style="padding-bottom:20"><img src="images/board/community_title_03.gif" alt="공사현황" border="0" /></td></tr>
      </table>
      <!-- //top -->
        
      <table>
        <tr><td><img src="images/trans.gif" height="13" alt="" /></td></tr>
      </table>
        
      <!-- table -->
      <form name="Frm" method="post" enctype="multipart/form-data" action="board_proc.asp?mode=<%=strMode%>&type=Work" onsubmit="return checkRegForm('Work')">
      <input type="hidden" id="idx" name="idx" value="<%=idx%>" />
      <input type="hidden" name="pg" value="<%=pg%>" />
      <input type="hidden" name="fld" value="<%=fld%>" />
      <input type="hidden" name="kwd" value="<%=kwd%>" />
			<table class="noticeTable addView spaceB_20" border="0" cellpadding="0" cellspacing="0">
			<colgroup>
			  <col width="15%" />
			  <col />
			</colgroup>
      <tr>
				<th><img src="images/board/th_11.gif" alt="구분" /></th>
			  <td class="text">
			    <label><input type="radio" class="radio" name="language" value="KOR"<%If strLanguage="KOR" Or strLanguage="" Then Response.Write " checked"%> /> KOR</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    <label><input type="radio" class="radio" name="language" value="ENG"<%If strLanguage="ENG" Then Response.Write " checked"%> /> ENG</label>
			  </td>
			</tr>
			<tr>
				<th><img src="images/board/th_12.gif" alt="제목" /></th>
				<td class="text"><input id="title" name="title" type="text" style="width:550px;font-weight:bold" value="<%=strTitle%>" /></td>
			</tr>
      <tr>
        <th><img src="images/board/th_model.gif" alt="이미지첨부" align="top" /></th>
        <td class="text"><input type="file" name="contentFile" style="width:550px;font-weight:bold" /><%If strContentFile<>"" Then Response.Write "<div class=""filedel""><a class=""preview"">"& strContentFile &"</a>&nbsp;&nbsp;&nbsp;<img id=""contentFileDelete_Work"" src=""images/board/btn_del.gif"" alt=""삭제"" /></div>"%></td>
      </tr>
			</table>
      <!-- //table -->

      <div class="nwdiv">
        <table border="0">
          <tr>
            <td align="right" width="90%"><input type="image" src="images/board/btn_ok.gif" alt="OK" class="btnType" /></td>
            <td align="right"><a href="publiclist.asp"><img src="images/board/btn_cancle.gif" alt="CANCEL" /></a></td>
          </tr>
        </table>
      </div>
      </form>
    </td>
    <!-- //contents-->
      
  </tr>
  <!-- //Middle Area -->

  <!-- Footer Area -->
  <!-- #include file="inc/bottom.asp" -->
  <!-- //Footer Area -->
</table>
<div id="bgContainer"></div>
<div id="imgContainer"><img src="/updata/Work/<%=strContentFile%>" alt="" /></div>
</body>
</html>