$(document).ready(function() {
  $bgBox = $("#bgContainer").css({ width: document.body.scrollWidth, height: document.body.scrollHeight, opacity: 0.5 });
  $imgBox = $("#imgContainer").css("width", "670px");
  $(".filedel .preview").css("cursor", "pointer").click(function(e) {
    e.stopPropagation();
    $bgBox.show("fast");
    var originWidth, originHeight, applyWidth, applyHeight;
    var thisImg = new Image();
    thisImg.src = $imgBox.children().attr("src");
    originWidth = thisImg.width;
    originHeight = thisImg.height;
    if (originWidth > 750) {
      thisImg.style.width = 750;
      thisImg.style.height = (750 * thisImg.height) / originWidth;
      applyWidth = 750;
      applyHeight = parseInt(thisImg.style.height);
      $imgBox.children().css({ width: applyWidth, height: applyHeight });
    }
    else {
      applyWidth = originWidth;
      applyHeight = originHeight;
    }
    $imgBox.css({ top: document.body.scrollHeight / 2 - applyHeight / 2, left: document.body.scrollWidth / 2 - applyWidth / 2, cursor: "pointer" }).show("slow").children().click(function() {
      $imgBox.hide("fast");
      $bgBox.hide("slow");
    });
  });
  $(".filedel img").click(function(e) {
    e.stopPropagation();
    var idx = $("#idx").val();
    var params = $(this).attr("id").split("_");
    var result = $.ajax({ type: "POST", url: "board_proc.asp", data: "mode=" + params[0] + "&type=" + params[1] + "&idx=" + idx, async: false }).responseText;

    switch (result) {
      case "Not Login":
        alert("로그인 정보가 없습니다. 로그인 페이지로 이동합니다.");
        location.href = "login.asp";
        break;
      case "Not Exist":
        alert("파일이 삭제되었거나 존재하지 않습니다.");
        break;
      case "Success":
        $(this).parent().remove();
        break;
      default:
        alert("필요한 인수가 제공되지 않았거나 네트워크 오류가 발생되었습니다.");
        break;
    }
  });
});