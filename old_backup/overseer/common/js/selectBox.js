Object.extend=function(a, b){
	for (var property in b) a[property] = b[property];
	return a;
};

Function.prototype.bind = function(){
	var __method = this, args = $A(arguments), object = args.shift();
	return function() {
		return __method.apply(object, args.concat($A(arguments)));
	}
};

Function.prototype.bindAsEventListener = function(){
	var __method = this, args = $A(arguments), object = args.shift();
	return function(event) {
		return __method.apply(object, [event || window.event].concat(args));
	}
}
		

var UI = {};
UI.$=function(s) { return document.getElementById(s) };
UI.StringBuffer=function(){this.buffer=new Array()}
UI.StringBuffer.prototype={append:function(s){this.buffer.push(s)},toString:function(){return this.buffer.join("")}};
//add event
UI.addEvent=function(object, type, listener) {	
	if(object.addEventListener) {if(type=='mousewheel')type='DOMMouseScroll'; object.addEventListener(type, listener, false)}
	else { object.attachEvent("on"+type, listener); }
};
//delete event
UI.delEvent=function(object, type, listener){
	if (object.removeEventListener) {if(type=='mousewheel')type='DOMMouseScroll'; object.removeEventListener(type, listener, false)}
	else object.detachEvent('on'+type, listener);
};
//stop event
UI.stopEvent=function(event) {
	var e=event || window.event;
	if(e.preventDefault) {e.preventDefault(); e.stopPropagation(); }
	else {e.returnValue = false; e.cancelBubble = true;}
};
UI.getEl=function(e){var E=UI.getE(e);return E.target || E.srcElement}
UI.getE=function(e){return e || window.event}
UI.Select = function(id,skin){
	this.skin={
		topbox:'border-bottom:2px solid #888888;font-size:11px;font-family:Dotum;color:#666;background-color:#fff;cursor:pointer;text-align:left;font-weight:bold',
		subbox:'border:1px solid #959595;font-size:11px;font-family:Dotum;color:#666;background-color:#fff;margin-top:-1px;text-align:left;font-weight:bold',
		default_txt:'color:#666;background-color:#fff;padding:2px 0px 2px 2px;cursor:pointer;font-weight:bold',
		selected_txt:'color:#9e9e9e;background-color:#f1f1f1;padding:2px 0px 2px 2px;cursor:pointer;font-weight:bold',
		arrow:'<img src="images/board/selectArrow.gif" width="7" height="4" style="margin-top:6px;margin-right:5px;">',
		width:150,
		padding:2,
		sub_height:18,
		max_option:10
	};	
	Object.extend(this.skin, skin);
	
	if ( this.skin.width != "auto" ){
		this.skin.width = parseInt( this.skin.width )+"px";
		this.skin.topbox += ';width:'+this.skin.width;
	}
	this.input=UI.$(id);
	this.id=id;
	this.list=[];
	this.isopen=0;
	this.selectedIndex=UI.$(id).selectedIndex;
	this.value='';
	this.print();	

};
UI.Select.prototype={
	close:function(){
		if(!this.isopen) return;
		UI.$('UISelectSub_'+this.id).style.display='none';
		this.isopen=0;
		this.focus();
	},
	blur:function(){
		var topSelectClassName = UI.$('UISelectSel_'+this.id).className.replace("selected_txt","default_txt");
		UI.$('UISelectSel_'+this.id).className=topSelectClassName;
	},
	focus:function(){
		var topSelectClassName = UI.$('UISelectSel_'+this.id).className.replace("default_txt","selected_txt");
		UI.$('UISelectSel_'+this.id).className=topSelectClassName;
	},
	open:function(){
		if(this.isopen) return;	
		var thisObj = UI.$('UISelectSub_'+this.id);
		thisObj.style.display='block';
		this.blur();
		var thisClassName = thisObj.getElementsByTagName('DIV')[this.selectedIndex].className.replace("default_txt","selected_txt");
		thisObj.getElementsByTagName('DIV')[this.selectedIndex].className=thisClassName;
		this.isopen=1;		
	},
	toogle:function(e){
		if(this.isopen)	this.close();
		else this.open();
		UI.stopEvent(e);
	},
	subOver:function(e){
		this.el=UI.getEl(e)
		var thisClassName = this.el.className.replace("default_txt","selected_txt");
		var selectedClassName = UI.$('UISelectSub_'+this.id).getElementsByTagName('DIV')[this.selectedIndex].className.replace("selected_txt","default_txt");

		UI.$('UISelectSub_'+this.id).getElementsByTagName('DIV')[this.selectedIndex].className=selectedClassName;
		this.el.className=thisClassName;
	},
	subOut:function(e){
		this.el=UI.getEl(e);
		var thisClassName = this.el.className.replace("selected_txt","default_txt");
		this.el.className=thisClassName;
	},
	subClick:function(e){
		this.el=UI.getEl(e);
		var selectedIndex=this.el.index;
		this.value=this.list[selectedIndex].value;

		UI.$('UISelectSel_'+this.id).innerHTML=this.list[selectedIndex].text;
		UI.$(this.id).value=this.list[selectedIndex].value;

		if ( this.skin.path == true ){
			var topWidth = UI.$('UISelectTop_'+this.id).offsetWidth;
			var subWidth = UI.$('UISelectSubInner_'+this.id).offsetWidth;
			if ( topWidth>this.autoSize ){
				UI.$('UISelectSubInner_'+this.id).style.width = topWidth + "px";
			} else {
				UI.$('UISelectSubInner_'+this.id).style.width = this.autoSize + "px";
			}
		}
		this.close();

		UI.stopEvent(e);
		if(selectedIndex!=this.selectedIndex) {
			if(this.input.onchange) { this.input.onchange.call(this);}
		}
		this.selectedIndex=selectedIndex;
	},

	print:function(){
		var id=this.id;
		var opt=this.input.options;
		var sb = new UI.StringBuffer();
		var autoWidth = 0;
		for(var i=0,cnt=opt.length; i<cnt; i++)
		{

			var checkAttr = opt[i].getAttribute("checked");
			var checkDiv = "";
			var className = 'default_txt';

			this.list[i]={text:opt[i].text,value:opt[i].value,check:checkAttr};
			if ( autoWidth < opt[i].text.length ){
				autoWidth = checkDiv+opt[i].text.length ;
			}
			sb.append('<div class="'+className+'" style="padding:'+this.skin.padding+';line-height:'+this.skin.sub_height+'px;height:'+this.skin.sub_height+'px">'+checkDiv+opt[i].text+'</div>');
		}
		
		if ( this.skin.width == "auto" ){
			this.skin.widthAuto = (parseInt(autoWidth)+3)+"em";
		} else {
			var getWidth = this.skin.width;
			var getWidthValue = getWidth.split("px")[0];
			var setWidth = getWidthValue - 2; // subbox ũ�� �����ϱ�
			this.skin.widthAuto = setWidth + "px";
			
		}


		this.value=this.list[0].value;
		document.write('<style type="text/css">div.default_txt{font:11px ���� ;'+this.skin.default_txt+'} div.selected_txt{font:11px ����;'+this.skin.selected_txt+'} div.check_on{color:#FF0000;} div.check_off{color:#00FF00;}</style>');
		var s='\
		<div id="UISelectTop_'+id+'" style="position:relative;'+this.skin.topbox+'">\
			<div id="UISelectSel_'+id+'" style="font:11px ����;padding:'+this.skin.padding+'px">'+this.list[this.selectedIndex].text+'</div>\
			<div style="position:absolute;right:0;top:0;">'+this.skin.arrow+'</div>\
		</div>\
		<div id="UISelectSub_'+id+'" style="display:none;position:absolute;"><span id="UISelectSubInner_'+id+'" style="display:block;width:'+this.skin.widthAuto+';'+this.skin.subbox+'">'+sb.toString()+'</span></div>';

		this.input.style.display='none';
		document.write(s);

		if(cnt>this.skin.max_option) {
			var sub = UI.$('UISelectSubInner_'+this.id);
			sub.style.height = this.skin.max_option * this.skin.sub_height + "px";
			sub.style.overflowY = "auto";
			sub.style.overflowX = "hidden";
		}

		var self=this;
		UI.addEvent(UI.$('UISelectTop_'+id), "mousedown", function(e) { self.toogle(e) } );
		UI.addEvent(document, "mousedown", function() { self.close();self.blur(); } );
		UI.addEvent(UI.$('UISelectSub_'+id), "mousedown", function(e) { UI.stopEvent(e) } );
		var sub=UI.$('UISelectSub_'+id).getElementsByTagName('div');
		for(var i=0; i<sub.length; i++)
		{	
			sub[i].index=i;
			UI.addEvent(sub[i], "mouseover", function(e) { self.subOver(e) } );
			UI.addEvent(sub[i], "mouseout",  function(e) { self.subOut(e) } );
			UI.addEvent(sub[i], "mousedown", function(e) { self.subClick(e) } );
		}
		UI.$('UISelectSub_'+id).style.display = "block";
		this.autoSize = UI.$('UISelectSub_'+id).offsetWidth;
		UI.$('UISelectSub_'+id).style.display = "none";

	}
};
