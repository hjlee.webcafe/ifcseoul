function checkRegForm(type) {
  if (type == "Work") {
  }
  else {
    if (!chkCheckBox("language", "구분을 선택해 주세요")) return false;
    if (!chkText("title", "제목을 입력해 주세요")) return false;
    if (!chkText("content", "내용을 입력해 주세요")) return false;
  }
  return true;
}

function chkDelete() {
  if (confirm("정말 삭제하시겠습니까?"))
    document.delFrm.submit();
}

function chkSearch() {
  var obj = document.schFrm;
  if (obj.kwd.value == "") {
    alert("검색어를 입력해 주세요");
    obj.kwd.focus();
    return false;
  }
  return true;
}

function deleteCheckedAll() {
  var obj = document.listFrm;
  var objLength = obj.length;
  if (objLength <= 3) return;
  var cnt = 0;
  if (objLength == 4) {
    if (!obj.idx.checked) {
      alert("삭제할 데이터를 선택해 주세요.");
      return;
    }
  }
  else {
    var idxLength = obj.idx.length;
    for (i = 0; i < idxLength; i++) {
      if (obj.idx[i].checked) cnt++;
    }
    if (cnt <= 0) {
    }
  }
  if (confirm("정말 삭제하시겠습니까"))
    obj.submit();
  else
    return;
}