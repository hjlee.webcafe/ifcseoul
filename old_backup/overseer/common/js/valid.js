function chkCheckBox(params, msg) {
	var obj = document.getElementsByName(params);
	var checkedCnt = 0;
	for (i = 0; i < obj.length; i++)
		if (obj.item(i).checked == true) checkedCnt++;

	if (checkedCnt > 0) return true;
	else {
	  alert(msg);
	  return false;
	}
}

function chkText(params, msg) {
	var obj = document.getElementById(params);
	if (obj.getAttribute("value") == "") {
	  alert(msg);
		obj.focus();
		return false;
	}
	else
		return true;
}

function chkNumeric(params, msg1, msg2) {
	var obj = document.getElementById(params);
	if (obj.getAttribute("value") == "") {
		alert(msg1);
		obj.focus();
		return false;
	}
	else {
		var regexp = /[^0-9]/;
		if (regexp.test(obj.getAttribute("value"))) {
			alert(msg2);
			obj.select();
			return false;
		}
	}
	return true;
}

function chkZipcode(params) {
	var obj = document.getElementById(params);
	if (obj.getAttribute("value") == "") {
		return false;
	}
	else
		return true;
}

function chkMobile(params) {
	var obj = document.getElementsByName(params);
	var regexp = Array(/01[016-9]/, /([1-9]{1}[0-9]{2,3})/, /([0-9]{4})/);
	var value = "";
	for (i = 0; i < obj.length; i++) {
		value = obj.item(i).getAttribute("value");
		if (value == "") {
			alert("번호를 입력해 주십시오");
			obj.item(i).select();
			return false;
		}
		else if (!regexp[i].test(value)) {
			alert("형식에 맞지 않는 번호입니다");
			obj.item(i).select();
			return false;
		}
	}
	return true;
}

function chkEmail(params) {
	var obj = document.getElementsByName(params);
	if (obj.item(0).getAttribute("value") == "") {
		alert("이메일 계정을 입력해 주십시오");
		obj.item(0).focus();
		return false;
	}
	if (obj.item(1).getAttribute("value") == "") {
		alert("이메일 호스트를 선택해 주십시오");
		obj.item(1).select();
		return false;
	}
	var value = obj.item(0).getAttribute("value")+"@"+obj.item(1).getAttribute("value");
	var regexp = /^[_a-zA-Z0-9-\.]+@[\.a-zA-Z0-9-]+\.[a-zA-Z]+$/;
	if (!regexp.test(value)) {
		alert("형식에 맞지 않는 이메일입니다");
		obj.item(0).select();
		return false;
	}
	return true;
}

function chkDate(params, str, btime, bopt) {
	var obj = document.getElementsByName(params);
	
	if (btime) var msg = Array("년", "월", "일", "시간")
	else var msg = Array("년", "월", "일")

	for (i = 0; i < obj.length; i++) {
		if (obj.item(i).getAttribute("value") == "") {
			alert(msg[i]+"을 선택해 주십시오");
			return false;
		}
	}

	var yy = obj.item(0).getAttribute("value");
	var mm = parseInt(obj.item(1).getAttribute("value"))-1;
	var dd = obj.item(2).getAttribute("value");

	if (btime) var inputDate = new Date(yy, mm, dd, obj.item(3).getAttribute("value"));
	else var inputDate = new Date(yy, mm, dd);

	if (!(inputDate.getFullYear() == yy && inputDate.getMonth() == mm && inputDate.getDate() == dd)) {
		alert("선택하신 날짜는 존재하지 않는 날짜입니다");
		return false;
	}

	if (bopt) {
		if (Date.parse(inputDate) < Date.parse(new Date())) {
			if (str != "") alert(str);
			return false;
		}
	}

	return true;
}

function isVaildDate(yy, mm, dd) {
	--mm;
	var dateVar = new Date(yy, mm, dd);
	return (dateVar.getFullYear() == yy && dateVar.getMonth() == mm && dateVar.getDate() == dd) ? true : false;
}