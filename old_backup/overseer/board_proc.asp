<!-- #include file = "../thisLibs/dbfunc.asp" -->
<!-- #include file = "../thisLibs/func.asp" -->
<!-- #include file = "../thisLibs/uploadfunc.asp" -->
<!-- #include file = "../thisLibs/checkParam.asp" -->
<!-- #include file = "../thisLibs/msgbox.asp" -->
<!-- #include file = "../thisLibs/removeCache.asp" -->
<%
Dim strLoginID, strLoginName, bLogin
strLoginID = Session("ifc_mngID")
strLoginName = Session("ifc_mngName")
If strLoginID <> "" Then bLogin = True Else bLogin = False

Dim objConn, objRs, strQuery, arrParam, arrParamLong, arrParamString
Dim upload, objImage, fso
Dim idx, pg, fld, kwd
Dim strFileExt, strNowContentFile, strNowDownloadFile, strUploadDiretory
Dim strLanguage, strTitle, strContent, strContentFile, strDownloadFile
Dim strReturnUrl, strThumbnail
Dim strMode, strType, strListPage, strViewPage, strWritePage

strMode = CheckParamLimitedList(Trim(Request("mode")),Array("Regist","Modify","Delete","checkedDelete","contentFileDelete","downloadFileDelete"))
strType = CheckParamLimitedList(Trim(Request("type")),Array("Notice","News","Work"))
CheckCompulsoryData Array(strMode,strType)

Select Case strType
  Case "Notice":
    strListPage = "noticelist.asp"
    strViewPage = "noticeview.asp"
    strWritePage = "noticereg.asp"
  Case "News":
    strListPage = "newslist.asp"
    strViewPage = "newsview.asp"
    strWritePage = "newsreg.asp"
  Case "Work":
    strListPage = "publiclist.asp"
    strViewPage = "publicreg.asp"
    strWritePage = "publicreg.asp"
End Select

arrParam = Null : arrParamLong = Null
strUploadDirectory = Server.MapPath("/updata/"& strType) &"\"

Select Case strMode
  Case "Regist","Modify":
    
    Set upload = Server.CreateObject("DEXT.FileUpload")
    upload.DefaultPath = strUploadDirectory
    
    idx = CheckParamType("int",Trim(upload("idx")))
    pg = CheckParamType("int",Trim(upload("pg")))
    fld = CheckParamLimitedList(Trim(upload("fld")),Array("strTitle","strContent"))
    kwd = GetTag2Text(CheckParamLength(Trim(upload("kwd")),20))
    
    strLanguage = CheckParamLimitedList(Trim(upload("language")),Array("KOR","ENG"))
    strTitle = GetTag2Text(Trim(upload("title")))
    strContent = InjectionCheck(Trim(upload("content")))
    
    If strType = "Work" Then CheckCompulsoryData Array(strLanguage,strTitle) Else CheckCompulsoryData Array(strLanguage,strTitle,strContent)
    
    If strMode = "Regist" Then
      LoginCheck bLogin, "로그인 정보가 없습니다. 로그인 페이지로 이동합니다.", Server.URLEncode(strWritePage)
    Else 
      LoginCheck bLogin, "로그인 정보가 없습니다. 로그인 페이지로 이동합니다.", Server.URLEncode(serializeLinkPage(strWritePage,Array("idx","pg","fld","kwd"),Array(idx,pg,fld,kwd)))
      CheckCompulsoryData Array(idx)
    End If

		If strType = "Work" Then
			If upload("contentFile") <> "" Then
				Set objImage = Server.CreateObject("DEXT.ImageProc")
				If objImage.SetSourceFile(upload("contentFile").TempFilePath) Then
					strThumbnail = "Thumb_"& upload("contentFile").FileNameWithoutExt & ".jpg"
					objImage.Quality = 90
					objImage.SaveasThumbnail strUploadDirectory & strThumbnail, objImage.ImageWidth/5, objImage.ImageHeight/5, false, false
				End If
			End If
		End If

    strContentFile = SaveFile(upload("contentFile"),Array("jpg","gif","png"),4*1024*1024)
    strDownloadFile = SaveFile(upload("downloadFile"),Array("zip","pdf"),10*1024*1024)
    
    Set objConn = OpenDBConnection()
    
    If strMode = "Regist" Then
      
      strQuery =  "Insert Into tblBoard (strBoard, strID, strName, strLanguage, strTitle, strContentFile, strDownloadFile, strContent) Values (?,?,?,?,?,?,?,?)"
      arrParam = Array(strType,strLoginID,strLoginName,strLanguage,strTitle,strContentFile,strDownloadFile)
      arrParamLong = Array(strContent)
      strReturnUrl = strListPage
    
    Else 
      
      Set objRs = SetDatabaseByParam(objConn, "Select strContentFile, strDownloadFile From tblBoard Where strBoard=? And intSeq=?", Array(strType,idx), Null, "rs")
      strNowContentFile = Trim(objRs(0))
      strNowDownloadFile = Trim(objRs(1))
      objRs.Close : Set objRs = Nothing
      
      If strContentFile <> "" Then upload.DeleteFile strUploadDirectory & strNowContentFile Else strContentFile = strNowContentFile
      If strDownloadFile <> "" Then upload.DeleteFile strUploadDirectory & strNowDownloadFile Else strDownloadFile = strNowDownloadFile
      
      strQuery = "Update tblBoard Set strLanguage=?, strTitle=?, strContentFile=?, strDownloadFile=?, strContent=? Where strBoard='"& strType &"' And intSeq="& idx
      arrParam = Array(strLanguage,strTitle,strContentFile,strDownloadFile)
      arrParamLong = Array(strContent)
      strReturnUrl = serializeLinkPage(strViewPage,Array("idx","pg","fld","kwd"),Array(idx,pg,fld,kwd))
      
    End If
    
    SetDatabaseByParam objConn, strQuery, arrParam, arrParamLong, "exec"
    objConn.Close : Set objConn = Nothing
    Response.Redirect strReturnUrl
    
  Case "checkedDelete","Delete":
  
    If strMode = "checkedDelete" Then
  
      For i = 1 To Request.Form("idx").Count
        If Not IsNumeric(Request.Form("idx")(i)) Then MsgBoxHistoryBack "허용되지 않은 데이터 타입이 사용되었습니다."
      Next
      
      With Request
        idx = Request.Form("idx")
        pg = CheckParamType("int",Trim(.Form("pg")))
        fld = CheckParamLimitedList(Trim(.Form("fld")),Array("strTitle","strContent"))
        kwd = GetTag2Text(CheckParamLength(Trim(.Form("kwd")),20))
      End With
    
      strReturnUrl = serializeLinkPage(strListPage,Array("pg","fld","kwd"),Array(pg,fld,kwd))
      LoginCheck bLogin, "로그인 정보가 없습니다. 로그인 페이지로 이동합니다.", Server.URLEncode(strReturnUrl)
      
    Else
    
      With Request
        idx = CheckParamType("int",Trim(.Form("idx")))
        pg = CheckParamType("int",Trim(.Form("pg")))
        fld = CheckParamLimitedList(Trim(.Form("fld")),Array("strTitle","strContent"))
        kwd = GetTag2Text(CheckParamLength(Trim(.Form("kwd")),20))
      End With
    
      strReturnUrl = serializeLinkPage(strViewPage,Array("idx","pg","fld","kwd"),Array(idx,pg,fld,kwd))
      LoginCheck bLogin, "로그인 정보가 없습니다. 로그인 페이지로 이동합니다.", Server.URLEncode(strReturnUrl)
    
    End If
    
    CheckCompulsoryData Array(idx)
    
    strQuery = "Select strContentFile, strDownloadFile From tblBoard Where strBoard=? And intSeq In ("& idx &")"
    Set objConn = OpenDBConnection()
    Set objRs = SetDatabaseByParam(objConn, strQuery, Array(strType), Null, "rs")
    If Not objRs.EOF Then
      Set fso = Server.CreateObject("Scripting.FileSystemObject")
      Do Until objRs.EOF
        If fso.FileExists(strUploadDirectory & Trim(objRs(0))) Then fso.DeleteFile strUploadDirectory & Trim(objRs(0))
        If fso.FileExists(strUploadDirectory & "Thumb_"& Trim(objRs(0))) Then fso.DeleteFile strUploadDirectory & "Thumb_"& Trim(objRs(0))
        If fso.FileExists(strUploadDirectory & Trim(objRs(1))) Then fso.DeleteFile strUploadDirectory & Trim(objRs(1))
        objRs.MoveNext
      Loop
      Set fso = Nothing
    End If
    
    strQuery = "Delete From tblBoard Where strBoard=? And intSeq In ("& idx &")"
    SetDatabaseByParam objConn, strQuery, Array(strType), Null, "exec"
    CloseDB objRs, objConn
    Response.Redirect serializeLinkPage(strListPage,Array("pg","fld","kwd"),Array(pg,fld,kwd))
  
  Case "contentFileDelete","downloadFileDelete":
  
    If Not bLogin Then
      Response.Write "Not Login"
      Response.End
    End If
  
    idx = CheckParamType("int",Trim(Request("idx")))
    CheckCompulsoryData Array(idx)
    
    If strMode = "contentFileDelete" Then strQuery = "Select strContentFile From tblBoard Where intSeq=?" Else strQuery = "Select strDownloadFile From tblBoard Where intSeq=?"
    
    Set objConn = OpenDBConnection()
    Set objRs = SetDatabaseByParam(objConn, strQuery, Array(idx), Null, "rs")
    
    Set fso = Server.CreateObject("Scripting.FileSystemObject")
    If fso.FileExists(strUploadDirectory & Trim(objRs(0))) Then 
      fso.DeleteFile strUploadDirectory & Trim(objRs(0))
    Else
      Response.Write "Not Exist"
      Response.End
    End If
    If strMode = "contentFileDelete" Then
      If fso.FileExists(strUploadDirectory & "Thumb_"& Trim(objRs(0))) Then fso.DeleteFile strUploadDirectory & "Thumb_"& Trim(objRs(0))
    End If
    Set fso = Nothing
    
    If strMode = "contentFileDelete" Then strQuery = "Update tblBoard Set strContentFile='' Where intSeq=?" Else strQuery = "Update tblBoard Set strDownloadFile='' Where intSeq=?"
    SetDatabaseByParam objConn, strQuery, Array(idx), Null, "exec"
    
    CloseDB objRs, objConn
    
    Response.Write "Success"
    Response.End
  
End Select
%>