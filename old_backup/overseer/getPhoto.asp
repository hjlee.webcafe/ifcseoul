<!-- #include file = "../thisLibs/dbfunc.asp" -->
<!-- #include file = "../thisLibs/removeCache.asp" -->
<%
Response.CharSet = "EUC-KR"
Dim objConn, objRs, strQuery
Dim intSeq, strBoard, strPhoto
Dim fso, strWebPath, strPhysicalPath

intSeq = Trim(Request.Form("idx"))

If intSeq = "" Then
  Response.Write "Give"
  Response.End
Else
  If Not IsNumeric(intSeq) Then
    Response.Write "InValid Data"
    Response.End
  End If

  strQuery = "Select strContentFile, strBoard From tblBoard Where intSeq=?"
  Set objConn = OpenDBConnection()
  Set objRs = SetDatabaseByParam(objConn, strQuery, Array(intSeq), Null, "rs")
  strPhoto = Trim(objRs(0))
  strBoard = Trim(objRs(1))
  CloseDB objRs, objConn
  strWebPath = "/updata/"& strBoard &"/"& strPhoto
  strPhysicalPath = Server.MapPath(strWebPath)
  
  Set fso = Server.CreateObject("Scripting.FileSystemObject")
  If Not fso.FileExists(strPhysicalPath) Then 
    Response.Write "None"
    Response.End
  End If
  
  Response.Write strWebPath
  
End If
%>