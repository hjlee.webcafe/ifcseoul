<!-- #include file = "../thisLibs/dbfunc.asp" -->
<!-- #include file = "../thisLibs/func.asp" -->
<!-- #include file = "../thisLibs/checkParam.asp" -->
<!-- #include file = "../thisLibs/msgbox.asp" -->
<!-- #include file = "../thisLibs/removeCache.asp" -->
<!-- #include file = "loginCheck.asp" -->
<%
Dim objConn, objRs, arrRs, strQuery, strWhere, strOrderBy, arrParam
Dim pg, fld, kwd
Dim intPageSize, intCurPage, intTotalCnt, intTotalPages, intModValue
Dim intStartNum, intArrayCnt, strCssString

pg = CheckParamType("int",Trim(Request("pg")))
fld = CheckParamLimitedList(Trim(Request("fld")),Array("strTitle","strContent"))
kwd = GetTag2Text(CheckParamLength(Trim(Request("kwd")),20))
arrParam = Null
intPageSize = 10

If pg = "" Then
	intCurPage = 1
Else
	intCurPage = pg
	If intCurPage < 1 Then intCurPage = 1
End If

strQuery = "Select Count(*) From tblBoard Where strBoard='Work'"

Set objConn = OpenDBConnection()
Set objRs = SetDatabaseByParam(objConn, strQuery, Null, Null, "rs")
intTotalCnt = objRs(0)
objRs.Close

intTotalPages = Fix(intTotalCnt \ intPageSize)
intModValue = intTotalCnt Mod intPageSize
If intModValue <> 0 Then intTotalPages = intTotalPages + 1
intStartNum = intTotalCnt - ((intCurPage - 1) * intPageSize)

If (fld <> "" And kwd <> "") Then
	strWhere = "And ("& fld & " Like '%' + ? + '%') "
	arrParam = Array(kwd, kwd)
End IF
strOrderBy = "Order By intSeq Desc"


strQuery =  "Select Top "& intPageSize &" intSeq, strLanguage, strTitle, Convert(Varchar(10), dateRegist, 102) dateRegist, intRead From tblBoard Where strBoard='Work' " &_
            "And intSeq Not In (Select Top "& (intCurPage - 1) * intPageSize &" intSeq From tblBoard Where strBoard='Work' "& strWhere & strOrderBy &") "& strWhere & strOrderBy

Set objRs = SetDatabaseByParam(objConn, strQuery, arrParam, Null, "rs")
If Not objRs.EOF Then arrRs = objRs.GetRows

CloseDB objConn, objRs
%>
<html>
<head>
  <!-- #include file = "inc/docHead.asp" -->
  <script type="text/javascript" src="common/js/board.js"></script>
  <script type="text/javascript" src="common/js/selectBox.js"></script>
</head>
<body>
<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
	<!-- Top Area -->
  <!-- #include file="inc/top.asp" -->
  <!-- //Top Area -->
    
  <!-- Middle Area -->
  <tr valign="top">
        
    <!-- LNB -->
    <td width="320">            
    <!-- #include file="inc/lnb.asp" -->                   
    </td>
    <!-- //LNB -->
        
    <!-- contents -->
    <td>
      <table cellpadding="0" cellspacing="0" border="0">
        <tr><td><img src="images/trans.gif" height="49" alt="" /></td></tr>
      </table>
                   
      <h2 style="padding-bottom:20"><img src="images/board/community_title_03.gif" alt="공사현황" /></h2>
      
      <form name="listFrm" method="post" action="board_proc.asp?mode=checkedDelete&type=Work">
      <input type="hidden" name="pg" value="<%=pg%>" />
      <input type="hidden" name="fld" value="<%=fld%>" />
      <input type="hidden" name="kwd" value="<%=kwd%>" />
      <table class="noticeTable spaceB_10" border="0" cellpadding="0" cellspacing="0">
      <colgroup>
        <col width="8%" />
        <col width="8%" />
        <col width="8%" />
		    <col width="55%" />
		    <col width="15%" />
		    <col width="12%" />
      </colgroup>
      <tr>
      	<th><img src="images/board/th_01.gif" alt="선택" /></th>
        <th><img src="images/board/th_11.gif" alt="구분" /></th>
     	  <th><img src="images/board/th_no.gif" alt="NO" /></th>
		    <th><img src="images/board/th_12.gif" alt="제목" /></th>
		    <th><img src="images/board/th_10.gif" alt="등록일" /></th>
		    <th><img src="images/board/th_hitsmall.gif" alt="조회수" /></th>
      </tr>
<%
If IsArray(arrRs) Then
  intArrayCnt = UBound(arrRs, 2)
  For k = 0 To intArrayCnt
    If k = 0 And k = intArrayCnt Then
      strCssString = " class=""first last"""
    ElseIf k = 0 Then
      strCssString = " class=""first"""
    ElseIf k = intArrayCnt Then
      strCssString = " class=""last"""
    Else
      strCssString = ""
    End If
%>
      <tr<%=strCssString%>>
      	<td><input type="checkbox" name="idx" class="chBox" value="<%=arrRs(0,k)%>" /></td>
        <td><%=arrRs(1,k)%></td>
        <td class="num"><%=intStartNum%></td>
        <td class="text"><a href="<%=serializeLinkPage("publicreg.asp",Array("idx","pg","fld","kwd"),Array(arrRs(0,k),pg,fld,kwd))%>"><%=arrRs(2,k)%></a></td>
        <td><%=arrRs(3,k)%></td>
        <td><%=arrRs(4,k)%></td>
      </tr>
<%
    intStartNum = intStartNum - 1
  Next
End If
%>
      </table>
      </form>
        
      <table class="noticeTable2"  border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            <img src="images/board/btn_delete_off.gif" alt="DELETE" style="cursor:pointer" onclick="deleteCheckedAll()" align="left" />
            <a href="publicreg.asp"><img src="images/board/btn_write.gif" alt="WRITE" border="0" align="right" /></a>
          </td>
        </tr>
        <tr>
          <td style="padding-top:10">
            <p class="paging"><%Paging intCurPage, intTotalPages, 10%></p>
          </td>         
        </tr>   
      </table>
        
      <div class="titleSearch">
      <form name="schFrm" method="get" onsubmit="return chkSearch()">
        <select id="search" name="fld" style="width:52px;">
          <option value="strTitle"<%If fld="strTitle" Then Response.Write " selected"%>>제목</option>
          <option value="strContent"<%If fld="strContent" Then Response.Write " selected"%>>내용</option>
        </select>
        <div style="width:52px;position:absolute;top:0px;left:0px"><script type="text/javascript">new UI.Select("search", { width: 52 });</script></div>
        <span style="margin:0 8px 0 59px"><input type="text" name="kwd" value="<%=kwd%>" class="search" style="width:84px" /></span>
        <input type="image" class="schbtn" src="images/board/btn_search.gif" alt="search" style="margin-top:-1px" />
      </form>
      </div>
    </td>
    <!-- //contents-->
  </tr>
  <!-- //Middle Area -->

  <!-- Footer Area -->
  <!-- #include file="inc/bottom.asp" -->
  <!-- //Footer Area -->
</table>
</body>
</html>