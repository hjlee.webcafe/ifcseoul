<!-- #include file = "../thisLibs/dbfunc.asp" -->
<!-- #include file = "../thisLibs/func.asp" -->
<!-- #include file = "../thisLibs/checkParam.asp" -->
<!-- #include file = "../thisLibs/msgbox.asp" -->
<!-- #include file = "../thisLibs/removeCache.asp" -->
<!-- #include file = "loginCheck.asp" -->
<%
Dim objConn, objRs, strQuery
Dim idx, pg, fld, kwd
Dim strTitle, dateRegist, intRead, strContent, strContentFile, strDownloadFile

idx = CheckParamType("int",Trim(Request("idx")))
pg = CheckParamType("int",Trim(Request("pg")))
fld = CheckParamLimitedList(Trim(Request("fld")),Array("strTitle","strContent"))
kwd = GetTag2Text(CheckParamLength(Trim(Request("kwd")),20))

CheckCompulsoryData Array(idx)

strQuery = "Select strTitle, intRead, Convert(Varchar(10),dateRegist,102), strContentFile, strDownloadFile, strContent From tblBoard Where strBoard='News' And intSeq=?"

Set objConn = OpenDBConnection()
Set objRs = SetDatabaseByParam(objConn, strQuery, Array(idx), Null, "rs")

If Not objRs.EOF Then
  strTitle = Trim(objRs(0))
  intRead = Trim(objRs(1))
  dateRegist = Trim(objRs(2))
  strContentFile = Trim(objRs(3))
  strDownloadFile = Trim(objRs(4))
  strContent = Trim(objRs(5))
  strContent = Replace(strContent, vbcrlf, "<br />")
End If

CloseDB objRs, objConn
%>
<html>
<head>
  <!-- #include file = "inc/docHead.asp" -->
  <script type="text/javascript" src="common/js/board.js"></script>
  <style type="text/css">
    .imgContainer {width:100%;text-align:center;margin-top:20px;}
  </style>
</head>
<body>
<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
	<!-- Top Area -->
  <!-- #include file="inc/top.asp" -->
  <!-- //Top Area -->
    
  <!-- Middle Area -->
  <tr valign="top">
        
    <!-- LNB -->
    <td width="320">            
    <!-- #include file="inc/lnb.asp" -->                   
    </td>
    <!-- //LNB -->
    
    <!-- contents -->
    <td>
      <table cellpadding="0" cellspacing="0" border="0">
        <tr><td><img src="images/trans.gif" height="49" border="0"></td></tr>
      </table>

      <h2 style="padding-bottom:20"><img src="images/board/community_title_02.gif" alt="새소식"></h2>
	    <h4 align="right" style="padding-bottom:8"><img src="images/trans.gif" height="11"></h4>
        
      <table class="noticeTable addView spaceB_20" border="0" cellpadding="0" cellspacing="0">
			<colgroup>
			  <col width="10%" />
			  <col width="53%" />
			  <col width="8%" />
			  <col width="13%" />
			  <col width="8%" />
			  <col width="8%" />
			</colgroup>
			<tr>
				<th><img src="images/board/th_title.gif" alt="제목" /></th>
			  <td class="text"><%=strTitle%></td>
				<td><%=dateRegist%></td>
				<th><img src="images/board/th_hitsmall.gif" alt="조회수" /></th>
			  <td><%=intRead%></td>
			</tr>
			<tr>
				<td colspan="6" class="view">
				  <%
				  Response.Write strContent
				  If strContentFile <> "" Then Response.Write "<div class=""imgContainer"">"& getImageTag(670, "/updata/News/"& strContentFile) &"</div>"
				  %>
				</td>
      </tr>                     
			</table>
			
      <table class="btndd">
        <tr>
          <td width="50%" align="left">
            <a href="<%=serializeLinkPage("newsreg.asp",Array("idx","pg","fld","kwd"),Array(idx,pg,fld,kwd))%>"><img src="images/board/btn_modify.gif" alt="수정" border="0" /></a>  
            <img src="images/board/btn_delete_off.gif" alt="삭제" style="cursor:pointer" onclick="chkDelete()" />
          </td>
          <td align="right">
            <a href="<%=serializeLinkPage("newslist.asp",Array("pg","fld","kwd"),Array(pg,fld,kwd))%>"><img src="images/board/btn_lilst.gif" alt="목록" border="0" /></a> 
          </td>
        </tr>
      </table>
    </td>
    <!-- //contents-->
  </tr>
  <!-- //Middle Area -->

  <!-- Footer Area -->
  <!-- #include file="inc/bottom.asp" -->
  <!-- //Footer Area -->
</table>
<form name="delFrm" method="post" action="board_proc.asp?mode=Delete&type=News">
<input type="hidden" name="idx" value="<%=idx%>" />
<input type="hidden" name="pg" value="<%=pg%>" />
<input type="hidden" name="fld" value="<%=fld%>" />
<input type="hidden" name="kwd" value="<%=kwd%>" />
</form>
</body>
</html>