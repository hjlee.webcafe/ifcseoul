<%
Function serializeParams(arrParams, arrValues)
  Dim strResult
  For i=0 To UBound(arrParams)
    If arrValues(i)<>"" Then
      If strResult<>"" Then strResult = strResult &"&"
      strResult = strResult & arrParams(i) &"="& arrValues(i)
    End If
  Next
  serializeParams = strResult
End Function

Function serializeLinkPage(scriptName ,arrParams, arrValues)
  Dim strResult
  For i=0 To UBound(arrParams)
    If arrValues(i)<>"" Then
      If strResult<>"" Then strResult = strResult &"&"
      strResult = strResult & arrParams(i) &"="& arrValues(i)
    End If
  Next
	If strResult <> "" Then strResult = scriptName &"?"& strResult Else strResult = scriptName
  serializeLinkPage = strResult
End Function

Function setReturnUrl()
	Dim strQueryString, strReturnUrl

	strReturnUrl = Request.ServerVariables("PATH_INFO")
	strQueryString = Request.ServerVariables("QUERY_STRING")
	If strQueryString <> "" Then strReturnUrl = strReturnUrl &"?"& strQueryString

	strReturnUrl = Server.UrlEncode(strReturnUrl)
	strReturnUrl = Replace(strReturnUrl,"&",";")

	setReturnUrl = strReturnUrl
End Function

Sub LoginCheck(bFlag, strMsg, strReturnUrl)
	Dim strUrl : strUrl = "/overseer/login.asp"
	If strReturnUrl <> "" Then strUrl = strUrl &"?GoUrl="& strReturnUrl
	If Not bFlag Then MsgBoxLocationHref strMsg, strUrl
End Sub

Sub Paging(curPage, totalPage, pagingSize)

  Dim intLoopSize, intBlock, intStartNum, intFinishNum, intLoop
  intBlock = Fix(curPage / pagingSize)
  If curPage Mod pagingSize > 0 Then intBlock = intBlock + 1
  intStartNum = (intBlock - 1) * pagingSize + 1
  intFinishNum = intBlock * pagingSize
  If intFinishNum > totalPage Then intFinishNum = totalPage

  If intBlock > 1 Then
    Response.Write "<a href="""& serializeLinkPage("",Array("pg","fld","kwd"),Array(1,fld,kwd)) &"""><img src=""images/board/btn_pre2.gif"" alt=""최근""></a>"
    Response.Write "<a href="""& serializeLinkPage("",Array("pg","fld","kwd"),Array(intStartNum-pagingSize,fld,kwd)) &"""><img src=""images/board/btn_pre.gif"" alt=""이전""></a>"
  Else
    Response.Write "<a><img src=""images/board/btn_pre2.gif"" alt=""최근""></a>"
    Response.Write "<a><img src=""images/board/btn_pre.gif"" alt=""이전""></a>"
  End If
  
  For intLoop = intStartNum To intFinishNum
    If intLoop = Int(curPage) Then 
      Response.Write "<a class=""on"">"& intLoop &"</a>"
    Else
      Response.Write "<a href="""& serializeLinkPage("",Array("pg","fld","kwd"),Array(intLoop,fld,kwd)) &""">"& intLoop &"</a>"
    End If
  Next
  
  If intFinishNum < totalPage Then
    Response.Write "<a href="""& serializeLinkPage("",Array("pg","fld","kwd"),Array(intStartNum+pagingSize,fld,kwd)) &"""><img src=""images/board/btn_next.gif"" alt=""다음""></a>"
    Response.Write "<a href="""& serializeLinkPage("",Array("pg","fld","kwd"),Array(totalPage,fld,kwd)) &"""><img src=""images/board/btn_next2.gif"" alt=""마지막""></a>"
  Else
    Response.Write "<a><img src=""images/board/btn_next.gif"" alt=""다음""></a>"
    Response.Write "<a><img src=""images/board/btn_next2.gif"" alt=""마지막""></a>"
  End If

End Sub

Sub mailSend(send_name,send_mail,resv_name,resv_mail,msgTitle,msgBody,html_yn)
	Dim fromMail,toMail,myMail,iConf
	fromMail = send_name & "<"&send_mail&">"
	toMail   = resv_name & "<"&resv_mail&">"
	Set myMail=server.CreateObject("CDO.Message")
	Set iConf = myMail.Configuration
	With iConf.Fields
		.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 1
		.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "C:\Inetpub\mailroot\Pickup"
		.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "127.0.0.1"
		.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30
		.Update
	End with
	myMail.From = fromMail
	myMail.To   = toMail
	myMail.Subject = msgTitle
	If html_yn="Y" Then 
	myMail.htmlBody = msgBody
	Else
	myMail.textBody = msgBody 'msgbody가 텍스트메일인경우
	End If 
	myMail.Send
	Set myMail = Nothing
End Sub

Function getImageTag(intWidth, strPath)
  Dim objPicture, intImgWidth
  Set objPicture = LoadPicture(Server.MapPath(strPath))
  If objPicture.Width > intWidth Then intImgWidth = intWidth Else intImgWidth = objPicture.Width
  Set objPicture = Nothing
  getImageTag = "<img src="""& strPath &""" width="""& intImgWidth &""" alt="""" />"
End Function
%>