<!--#include virtual = "/thisLibs/adovbs.inc"-->
<%
Const DB_INSERT_ERROR = "처리 중에 문제가 발생하였습니다. <a href=""javascript:history.back()"">돌아가기</a>"

Function OpenDBConnection()

	Set objConnection = Server.CreateObject("ADODB.Connection")
	objConnection.Provider ="SQLOLEDB"
	objConnection.open "Server=211.43.212.170;Database=ifcseoul;UID=ifcseoul;PWD=woody"
	Set OpenDBConnection = objConnection
	
End Function

Sub CloseDB(objRecordset, oConn)

	If Not objRecordset Is Nothing Then
		If objRecordset.State <> 0 Then objRecordset.Close()
		Set objRecordset = Nothing
	End If

	If Not oConn Is Nothing Then
		If oConn.State <> 0 Then oConn.Close()
		Set oConn = Nothing
	End If
	
End Sub

Function GetRecordSetByProc(Conn, procName, arrParam1, arrParam2, arrParamLong, execOrder) '프로시저 실행

	Set Comm= Server.CreateObject("ADODB.Command")
	Set rs = Server.CreateObject("ADODB.RecordSet")
	With Comm
		.ActiveConnection = Conn
		.CommandText = procName
		.CommandType = 4 'adcmdstoredproc
		For this_i = 0 To UBound( arrParam1 )
			Select Case VarType(arrParam1(this_i))
		    Case vbInteger
		    	.Parameters.Append .CreateParameter("& arrParam2(this_i) &", 3, &H0001, , arrParam1(this_i) & "" )
		    Case vbLong
		    	.Parameters.Append .CreateParameter("& arrParam2(this_i) &", 20, &H0001, , arrParam1(this_i) & "")
		    Case vbDate
		    	.Parameters.Append .CreateParameter("& arrParam2(this_i) &", 7, &H0001, , arrParam1(this_i) & "")
		    Case vbNull
		    	.Parameters.Append .CreateParameter("& arrParam2(this_i) &", 0, &H0001, , arrParam1(this_i) & "")
			  Case Else
		    	.Parameters.Append .CreateParameter("& arrParam2(this_i) &", 200, &H0001, LenB(arrParam1(this_i))+1, arrParam1(this_i) & "")
		  End Select
		Next
		
		If IsArray( arrParamLong ) Then
			For this_i = 0 To UBound( arrParamLong )
				If LenB( arrParamLong(this_i) ) = 0 Then
					.Parameters.Append .CreateParameter(this_i, 201, &H0001, 1, ""  )
				Else
					.Parameters.Append .CreateParameter(this_i, 201, &H0001, LenB(arrParamLong(this_i)), arrParamLong(this_i) )
				End If
			Next
		End If
	End With 

	Set GetRecordSetByProc = orderDivide(Comm, execOrder, Conn)
	
End Function

Function GetRecordSetByProcBlank(Conn, procName, execOrder) '프로시저 실행

	Set  Comm= Server.CreateObject("ADODB.Command")
	With Comm
		.ActiveConnection = Conn
		.CommandText = procName
		.CommandType = 4 'adcmdstoredproc	
	End With 	
	Set rs = Server.CreateObject("ADODB.RecordSet")
	Set GetRecordSetByProcBlank = orderDivide(Comm, execOrder, Conn)
	
End Function

Function SetDatabaseByParam(Conn, strQuery, arrParam, arrParamLong, execOrder) 

	Set  Comm= Server.CreateObject("ADODB.command")
	With Comm
		.ActiveConnection = Conn
		.CommandText = strQuery
		.CommandType = &H0001 'adcmdtext
		
		If IsArray( arrParam ) Then
			For this_i = 0 to UBound( arrParam )
				Select Case VarType(arrParam(this_i))
			    Case vbInteger
			    	.Parameters.Append .CreateParameter(this_i, 3, &H0001, , arrParam(this_i) & "" )
			    Case vbLong
			    	.Parameters.Append .CreateParameter(this_i, 20, &H0001, , arrParam(this_i) & "")
			    Case vbDate
			    	.Parameters.Append .CreateParameter(this_i, 7, &H0001, , arrParam(this_i) & "")
			    Case vbNull
			    	.Parameters.Append .CreateParameter(this_i, 0, &H0001, , arrParam(this_i) & "")
				  Case Else
			    	.Parameters.Append .CreateParameter(this_i, 200, &H0001, LenB(arrParam(this_i))+1, arrParam(this_i) & "")
			  End Select
			Next
		End If
		
		If IsArray( arrParamLong ) Then
			For this_i = 0 to UBound( arrParamLong )
				If LenB( arrParamLong(this_i) ) = 0 Then
					.Parameters.Append .CreateParameter(this_i, 201, &H0001, 1, ""  )
				Else
					.Parameters.Append .CreateParameter(this_i, 201, &H0001, LenB(arrParamLong(this_i)), arrParamLong(this_i))
				End If
			Next
		End If
		
	End With 
	Set SetDatabaseByParam = orderDivide(Comm, execOrder, Conn)
	
End Function

Function orderDivide (Comm, execOrder, Conn)

	Set rs = Server.CreateObject("ADODB.RecordSet")
	If execOrder="exec" Then
		On Error Resume Next
		Comm.Prepared = True
		Conn.BeginTrans
		Comm.Execute

		If Conn.Errors.Count <> 0 Then
			Response.Write ("Error : " & Err.Description & "<br>")
			Response.Write (DB_INSERT_ERROR)
			Conn.Errors.Clear
			Conn.RollBackTrans
			Response.End
		Else
			Conn.Errors.Clear
			Conn.CommitTrans
		End If
		
		Set orderDivide = Nothing
	Else 
		rs.CursorType = 0 'adOpenForwardOnly
		rs.CursorLocation = 3 'adUseClient
		rs.LockType = 1 'adLockReadOnly
		rs.Open Comm
		
		Set orderDivide = rs
	End If
	
End Function

Function SetDatabaseByblank(Conn, strQuery, execOrder)

	'response.write strquery
	Set  Comm= Server.CreateObject("ADODB.command")
	with Comm
		.ActiveConnection = Conn
		.CommandText = strQuery
		.CommandType = &H0001 'adcmdtext
	End With
	Set rs = Server.CreateObject("ADODB.RecordSet")
	Set SetDatabaseByblank = orderDivide(Comm, execOrder, Conn)
	
End Function
%>