<%
Function CheckValidFileExtension(strExt, arrLimitedExt)
	Dim bResult : bResult = False
	For i=0 To UBound(arrLimitedExt)
		If arrLimitedExt(i) = strExt Then
			bResult = True
			Exit For
		End If
	Next
	CheckValidFileExtension = bResult
End Function

Function CheckValidFileLength(intLength, intLimitedLength)
	Dim bResult : bResult = False
	If intLength <= intLimitedLength Then bResult = True
	CheckValidFileLength = bResult
End Function

Function SaveFile(objUpload, arrLimitedExt, intLimitedLen)
	
	Dim strFileExt : strFileExt = LCase(objUpload.FileExtension)
	Dim intFileLen : intFileLen = objUpload.FileLen

	If objUpload <> "" Then

		If IsArray(arrLimitedExt) Then
			If Not CheckValidFileExtension(strFileExt, arrLimitedExt) Then 
				MsgBoxHistoryBack "허용되지 않은 파일 형식입니다."
				Exit Function
			End If
		End If
		
		If intLimitedLen <> "" Then
			If intFileLen > intLimitedLen Then 
				MsgBoxHistoryBack "파일의 용량이 허용된 용량보다 큽니다.(허용용량 : "& Fix(intLimitedLen/1024*1024) &" MB 이하)"
				Exit Function
			End If
		End If
		
		objUpload.Save , False

		SaveFile = objUpload.lastSavedFileName

	Else

		SaveFile = ""

	End If

End Function
%>