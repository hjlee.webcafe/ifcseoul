<%
Sub MsgBoxLocationHref(strMsg, strReturnUrl)
  Response.Write "<script type='text/javascript'>"& vbcrlf
  Response.Write "<!--'>"& vbcrlf
  Response.Write "alert('"& strMsg &"');"& vbcrlf
  Response.Write "location.href='"& strReturnUrl &"';"& vbcrlf
  Response.Write "//-->"& vbcrlf
  Response.Write "</script>"
	Response.End
End Sub

Sub MsgBoxConfirmLocationHref(strMsg, strReturnUrl)
  Response.Write "<script type='text/javascript'>"& vbcrlf
  Response.Write "<!--'>"& vbcrlf
  Response.Write "if (confirm('"& strMsg &"'))"& vbcrlf
  Response.Write "  location.href = 'join.asp';"& vbcrlf
  Response.Write "else"& vbcrlf
  Response.Write "  location.href='"& strReturnUrl &"';"& vbcrlf
  Response.Write "//-->"& vbcrlf
  Response.Write "</script>"
	Response.End
End Sub

Sub MsgBoxHistoryBack(strMsg)
  Response.Write "<script type='text/javascript'>"& vbcrlf
  Response.Write "<!--'>"& vbcrlf
  Response.Write "alert('"& strMsg &"');"& vbcrlf
  Response.Write "history.back();"& vbcrlf
  Response.Write "//-->"& vbcrlf
  Response.Write "</script>"
	Response.End
End Sub

Sub MsgBoxClose(strMsg)
	Response.Write "<script type=""text/javascript"">"& vbcrlf
	Response.Write "<!--"& vbcrlf
	Response.Write "alert("""& strMsg &""");"& vbcrlf
	Response.Write "self.close();"& vbcrlf
	Response.Write "//-->"& vbcrlf
	Response.Write "</script>"
	Response.End
End Sub
%>