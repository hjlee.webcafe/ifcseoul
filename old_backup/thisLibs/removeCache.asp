<%
With Response
	.Expires = -1
	.ExpiresAbsolute = now() - 1
	.AddHeader "pragma","no-cache"
	.AddHeader "cache-control","private"
	.CacheControl = "no-cache"
End With
%>