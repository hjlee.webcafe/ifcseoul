<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%
'db dbo.tblFacGal
Dim countQuery, selectQuery, num_count, list_array, page_result, t_page, query_where

// search string
If Request.QueryString("search_text") <> "" Then
	query_where = " and (fg_title_ko Like '%" & Request.QueryString("search_text") & "%' or fg_title_en Like '%" & Request.QueryString("search_text") & "%') "
End If

// counting list
countQuery = "select count(*) as count from dbo.tblFacGal where 1 = 1 "& query_where

set objConn = OpenDBConnection()
set objRs = SendQuery(objConn,countQuery)
total_count = objRs("count")
num_count = total_count - (page - 1) * page_limit
set objRs = Nothing

// get list
selectQuery = "select Top " & page_limit & " * from dbo.tblFacGal where fg_num Not In (select Top " & current_count & " fg_num from dbo.tblFacGal where 1 = 1 "& query_where &") "& query_where & " order by fg_num desc "

set objRs = SendQuery(objConn,selectQuery)

'page_create(p_total_count, p_page, p_page_limit, p_page_length)
t_page = page
page_result = page_create(total_count, t_page, page_limit, page_length)

%>
<div class="top_btn_wrap">
	<ul>
		<li>
			전체 <%=total_count%> 건
		</li>
		<li class="top_btn_right">
			<label>리스트 갯수</label>
			<select name="page_limit_btn" id="page_limit_btn">
				<option value="15" <%=page_limit_select.Item("15")%>>
					15
				</option>
				<option value="20" <%=page_limit_select.Item("20")%>>
					20
				</option>
				<option value="30" <%=page_limit_select.Item("30")%>>
					30
				</option>
				<option value="50" <%=page_limit_select.Item("50")%>>
					50
				</option>
				<option value="100" <%=page_limit_select.Item("100")%>>
					100
				</option>
			</select>
		</li>
	</ul>
</div>
<div class="table_list">
	<table class="tbl_list text-center" id="table_list" border="1" cellspacing="0" summary="Gallary List">
		<caption>시설 갤러리</caption>
		<colgroup>
			<col width="5%" >
			<col width="5%">
			<col>
			<col>
			<col>
		</colgroup>
		<thead>
			<tr>
				<th>
					<input type="checkbox" id="select_all_list" name="select_all_list" />&nbsp;선택
				</th>
				<th>
					번호
				</th>
				<th>
					이미지
				</th>
				<th>
					제목
				</th>
				<th>
					등록일
				</th>
			</tr>
		</thead>
		<tbody>
			<% if objRs.EOF Then %>
			<tr>
				<td colspan="5" align="center">
					리스트가 없습니다.
				</td>
			</tr>
			<% Else %>
				<% For k = 0 to objRs.RecordCount - 1 %>
			<tr>
				<td>
					<input type="checkbox" id="fg_<%=objRs("fg_num")%>" name="fg_<%=objRs("fg_num")%>" value="<%=objRs("fg_num")%>" />
				</td>
				<td>
					<%=num_count-k%>
				</td>
				<td>
					<a href="facility_gallary_detail.asp<%=list_link%>&num=<%=objRs("fg_num")%>"><img src="<%=objRs("fg_img")%>" alt="preview image" class="preview_img" /></a>
				</td>
				<td>
					<a href="facility_gallary_detail.asp<%=list_link%>&num=<%=objRs("fg_num")%>"><%=objRs("fg_title_ko")%></a>
				</td>
				<td>
					<%=objRs("fg_reg_date")%>
				</td>
			</tr>
				<% objRs.MoveNext %>
				<% Next %>
			<% End If %>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-danger btn-sm" id="del_fg" name="del_fg" value="삭제하기" />&nbsp;
		</li>
		<li class="btm_btn_right">
			<a href="facility_gallary_new.asp"><input type="button" class="btn btn-primary btn-sm" value="등록하기" /></a>&nbsp;
		</li>
	</ul>
</div>
<div class="page_wrap">
	<!-- #include file = "../view/page_template.asp" -->
</div>
<div class="search_wrap">
	<form name="search" id="search" method="get">
		<input type="text" class="form-control input-sm width_150" name="search_text" value="" placeholder="검색어" />
		<input type="submit" class="btn btn-sm btn-primary ml10" name="search" value="검색" />
		<input type="hidden" id="list_link_hidden" value="seoul_gallary.asp<%=list_link%>" />
		<input type="hidden" id="page_limit" name="page_limit" value="<%=page_limit%>" />
	</form>
</div>
<!--#include file = "index_footer.asp" -->