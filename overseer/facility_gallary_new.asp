<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->

<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="관리자 등록">
		<caption>갤러리등록</caption>
		<colgroup>
			<col width="150px" >
			<col>
		</colgroup>
		<tbody>
			<tr>
				<th>
					타이틀(국문)
				</th>
				<td>
					<input name="fg_title_ko" id="fg_title_ko" type="text" class="width_250" maxlength="12" value="" /> (12자 이내)
				</td>
			</tr>
			<tr>
				<th>
					타이틀(영문)
				</th>
				<td>
					<input name="fg_title_en" id="fg_title_en" type="text" class="width_250" maxlength="15" value="" /> (15자 이내)
				</td>
			</tr>
			<tr>
				<th>
					리스트 제목
				</th>
				<td>
					<input name="fg_list_title" id="fg_list_title" type="text" class="width_250" maxlength="15" value="" /> (15자 이내)
				</td>
			</tr>
			<tr>
				<th>
					첨부파일
				</th>
				<td>
					<input name="fg_img" id="fg_img" type="text" class="i_text50" value="" />
					<div id="fg_img_upload" class="upload_btn_2">Upload</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-primary btn-sm" id="add_fg" name="add_fg" value="등록하기" />&nbsp;
			<input type="button" class="btn btn-warning btn-sm" id="back_to_list" name="back_to_list" value="목록으로" />
		</li>
		<li class="btm_btn_right">
			<input type="hidden" id="list_link_hidden" value="facility_gallary.asp<%=list_link%>"/>
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->