<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%

Dim selectQuery, num, check(1), imgRs
num = Request.QueryString("num")
selectQuery = "select * from tblNotice where n_num = '"& num &"'"

set objRs = SendQuery(objConn,selectQuery)
check(objRs("n_group")) = "checked"

// get image list
selectQuery = "select * from tblNotice_img where n_num = '"& num &"'"

set imgRs = SendQuery(objConn,selectQuery)

%>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="공지 등록">
		<caption>공지 등록</caption>
		<colgroup>
			<col width="150px" >
			<col>
		</colgroup>
		<tbody>
			<tr>
				<th>
					구분
				</th>
				<td>
					<label><input type="radio" name="n_group" class="n_group" value="0" <%=check(0)%> /> 국문</label>
					<label><input type="radio" name="n_group" class="n_group ml20" value="1" <%=check(1)%> /> 영문</label>
				</td>
			</tr>
			<tr>
				<th>
					제목
				</th>
				<td>
					<input type="text" class="i_text50" name="n_title" id="n_title" maxlength="80" value="<%=objRs("n_title")%>" placeholder="제목을 입력 해 주세요." /> (80자 이내)
				</td>
			</tr>
			<tr>
				<th>
					내용
				</th>
				<td>
					<textarea type="text" class="ckeditor" name="n_text" id="n_text" value="" placeholder="내용을 입력 해 주세요"><%=objRs("n_text")%></textarea>
				</td>
			<tr>
				<th>
					이미지
				</th>
				<td>
					<div class="img_section">
						<ul>
							<% For i = 0 To imgRs.RecordCount - 1 %>
							<li class="img_wrap"><img src="<%=imgRs("ni_img")%>" class="upload_img" /><span class="upload_img_del">x</span></li>
							<% imgRs.MoveNext %>
							<% Next %>
						</ul>
						<input type="hidden" class="ckeditor_img" value="n_text_img" />
					</div>
					<div class="insert img">
						<div id="img_upload" class="upload_btn">Upload</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-danger btn-sm" id="del_bulletin" name="del_bulletin" value="삭제하기" />&nbsp;
			<input type="button" class="btn btn-primary btn-sm" id="edit_bulletin" name="edit_bulletin" value="수정하기" />&nbsp;
			<input type="button" class="btn btn-warning btn-sm" id="back_to_list" name="back_to_list" value="목록으로" />
		</li>
		<li class="btm_btn_right">
			<input type="hidden" id="list_link_hidden" value="bulletin.asp<%=list_link%>"/>
			<input type="hidden" id="n_num" value="<%=num%>" />
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->