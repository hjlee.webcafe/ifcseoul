<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->

<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="Gallary List">
		<caption>서울 갤러리</caption>
		<colgroup>
			<col width="150px" >
			<col>
		</colgroup>
		<tbody>
			<tr>
				<th>
					구분
				</th>
				<td>
					<select id="stat_type" name="stat_type" class="stat_type form-control input-sm width_100">
						<option value="0">Deloitte</option>
						<option value="1">BNY Mellon</option>
						<option value="2">Sony</option>
						<option value="3">OTIS</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>
					메인 노출
				</th>
				<td>
					<label><input type="radio" name="g_show" class="g_show" value="0" checked /> 비노출 </label>&nbsp;&nbsp;&nbsp;
					<label><input type="radio" name="g_show" class="g_show" value="1" /> 노출 </label>
					(세로컷일 경우 메인에 노출하지 않음)
				</td>
			</tr>
			<tr>
				<th>
					제목(국문)
				</th>
				<td>
					<input type="text" id="g_title_ko" name="g_title_ko" class="width_250" maxlength="12" placeholder="제목을 입력해주세요."/> (12자 이내)
				</td>
			</tr>
			<tr>
				<th>
					제목(영문)
				</th>
				<td>
					<input type="text" id="g_title_en" name="g_title_en" class="width_250" maxlength="15" placeholder="제목을 입력해주세요."/> (15자 이내)
				</td>
			</tr>
			<tr>
				<th>
					리스트 제목
				</th>
				<td>
					<input type="text" id="g_list_title" name="g_list_title" class="width_250" maxlength="15" placeholder="제목을 입력해주세요."/> (15자 이내)
				</td>
			</tr>
			<tr>
				<th>
					첨부파일
				</th>
				<td>
					<input name="g_img" id="g_img" type="text" class="width_250" value="" placeholder="이미지를 등록해주세요."/>
					<div id="g_img_upload" class="upload_btn_2">Upload</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-primary btn-sm" id="add_tenant_gallary" name="add_tenant_gallary" value="등록하기" />&nbsp;
			<input type="button" class="btn btn-warning btn-sm" id="back_to_list" name="back_to_list" value="목록으로" />
		</li>
		<li class="btm_btn_right">
			<input type="hidden" id="list_link_hidden" value="tenant_gallary.asp<%=list_link%>"/>
		</li>
	</ul>
</div>
<!--#include file = "index_footer.asp" -->