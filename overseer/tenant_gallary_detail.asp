<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%

Dim selectQuery, num , check(1), type_array(3), show_array(1)
num = Request.QueryString("num")
selectQuery = "select * from dbo.tblTenantGall where g_num = '"& num &"'"

set objRs = SendQuery(objConn,selectQuery)
type_array(objRs("g_type")) = "selected"
show_array(objRs("g_show")) = "checked"

g_img_hide = "hide_row"

If objRs("g_img") <> "" Then
	g_img_hide = ""
	img_name_array = Split(objRs("g_img"), "/")
	g_img_name = img_name_array(UBound(img_name_array))
End If

%>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="서울 갤러리 등록">
		<caption>서울 갤러리 등록</caption>
		<colgroup>
			<col width="150px" >
			<col>
		</colgroup>
		<tbody>
			<tr>
				<th>
					구분
				</th>
				<td>
					<select id="stat_type" name="stat_type" class="stat_type form-control input-sm width_100">
						<option value="0" <%=type_array(0)%>>Deloitte</option>
						<option value="1" <%=type_array(1)%>>BNY Mellon</option>
						<option value="2" <%=type_array(2)%>>Sony</option>
						<option value="3" <%=type_array(3)%>>OTIS</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>
					메인 노출
				</th>
				<td>
					<label><input type="radio" name="g_show" class="g_show" value="0" <%=show_array(0)%> /> 비노출 </label>&nbsp;&nbsp;&nbsp;
					<label><input type="radio" name="g_show" class="g_show" value="1" <%=show_array(1)%> /> 노출 </label>
					(세로컷일 경우 메인에 노출하지 않음)
				</td>
			</tr>
			<tr>
				<th>
					제목(국문)
				</th>
				<td>
					<input type="text" id="g_title_ko" name="g_title_ko" class="width_350" maxlength="12" value="<%=objRs("g_title_ko")%>" /> (12자 이내)
				</td>
			</tr>
			<tr>
				<th>
					제목(영문)
				</th>
				<td>
				<input type="text" id="g_title_en" name="g_title_en" class="width_350" maxlength="15" value="<%=objRs("g_title_en")%>"/> (15자 이내)
				</td>
			</tr>
			<tr>
				<th>
					리스트 제목
				</th>
				<td>
					<input type="text" id="g_list_title" name="g_list_title" class="width_250" maxlength="15" value="<%=objRs("g_list_title")%>" /> (15자 이내)
				</td>
			</tr>
			<tr>
				<th>
					등록일
				</th>
				<td>
					<%=objRs("g_reg_date")%>
				</td>
			</tr>
			<tr>
				<th>
					첨부파일
				</th>
				<td>
					<input name="g_img" id="g_img" type="text" class="i_text50 ii_img" value="<%=objRs("g_img")%>" />
					<div id="g_img_upload" class="upload_btn_2">Upload</div>
					<br />
					<div class="current_image_wrap <%=g_img_hide%>">
						<span id="g_img_name"><%=g_img_name%></span>
						<input type="hidden" id="g_img_path" name="g_imgpath" value="<%=objRs("g_img")%>" />
						<input type="button" class="btn btn-sm btn-danger delete_btn" id="g_img_del" name="g_img_del" value="삭제" />
					</div>
					<br />
					<img src="<%=objRs("g_img")%>" id="g_img_preview" height="200px" />
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-danger btn-sm" id="del_tenant_gallary" name="del_tenant_gallary" value="삭제하기" />&nbsp;
			<input type="button" class="btn btn-primary btn-sm" id="edit_tenant_gallary" name="edit_tenant_gallary" value="수정하기" />&nbsp;
			<input type="button" class="btn btn-warning btn-sm" id="back_to_list" name="back_to_list" value="목록으로" />
		</li>
		<li class="btm_btn_right">
			<input type="hidden" id="list_link_hidden" value="tenant_gallary.asp<%=list_link%>" />
			<input type="hidden" id="g_num" value="<%=num%>" />
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->