<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%

Dim countQuery, selectQuery, num_count, list_array, page_result, t_page, query_where, type_array(2), show_array(1), gtype_array(2)
type_array(0)="IFC SEOUL"
type_array(1)="CONRAD Seoul"
type_array(2)="IFC MALL"
show_array(0)="비노출"
show_array(1)="노출"

// search string
If Request.QueryString("search_text") <> "" Then
	query_where = " and (g_title_ko Like '%" & Request.QueryString("search_text") & "%' or g_title_en Like '%" & Request.QueryString("search_text") & "%' or g_list_title Like '%" & Request.QueryString("search_text") & "%' ) "
End If

If Request.QueryString("g_type") <> "" and Request.QueryString("g_type") <> "all" Then
	query_where = " and g_type = " & Request.QueryString("g_type")
	gtype_array(Request.QueryString("g_type")) = "selected"
End If

// counting list
countQuery = "select count(*) as count from dbo.tblGall where 1 = 1 "& query_where

set objConn = OpenDBConnection()
set objRs = SendQuery(objConn,countQuery)
total_count = objRs("count")
num_count = total_count - (page - 1) * page_limit
set objRs = Nothing

query_where = query_where & "order by g_num desc"

// get list
selectQuery = "select Top " & page_limit & " * from dbo.tblGall where g_num Not In (select Top " & current_count & " g_num from dbo.tblGall where 1 = 1 "& query_where &") "& query_where
set objRs = SendQuery(objConn,selectQuery)

'page_create(p_total_count, p_page, p_page_limit, p_page_length)
t_page = page
page_result = page_create(total_count, t_page, page_limit, page_length)
%>
<div class="top_btn_wrap">
	<ul>
		<li>
			전체 <%=total_count%> 건
			<select id="g_type" class="ml20">
				<option value="all">전체</option>
				<option value="0" <%=gtype_array(0)%>>IFC SEOUL</option>
				<option value="1" <%=gtype_array(1)%>>CONRAD SEOUL</option>
				<option value="2" <%=gtype_array(2)%>>IFC MALL</option>
			</select>
		</li>
		<li class="top_btn_right">
			<label>리스트 갯수</label>
			<select name="page_limit_btn" id="page_limit_btn">
				<option value="15" <%=page_limit_select.Item("15")%>>
					15
				</option>
				<option value="20" <%=page_limit_select.Item("20")%>>
					20
				</option>
				<option value="30" <%=page_limit_select.Item("30")%>>
					30
				</option>
				<option value="50" <%=page_limit_select.Item("50")%>>
					50
				</option>
				<option value="100" <%=page_limit_select.Item("100")%>>
					100
				</option>
			</select>
		</li>
	</ul>
</div>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="Admin List">
		<caption>IFC 서울 갤러리</caption>
		<colgroup>
			<col width="5%" >
			<col width="5%">
			<col>
			<col>
			<col>
			<col>
			<col>
		</colgroup>
		<thead>
			<tr>
				<th>
					<input type="checkbox" id="select_all_list" name="select_all_list" />&nbsp;선택
				</th>
				<th>
					번호
				</th>
				<th>
					구분
				</th>
				<th>
					메인노출
				</th>
				<th>
					이미지
				</th>
				<th>
					제목
				</th>
				<th>
					등록일
				</th>
			</tr>
		</thead>
		<tbody>
			<% if objRs.EOF Then %>
			<tr>
				<td colspan="7" align="center">
					리스트가 없습니다.
				</td>
			</tr>
			<% Else %>
				<% For k = 0 to objRs.RecordCount - 1 %>
			<tr>
				<td class="text-center">
					<input type="checkbox" id="gall_<%=objRs("g_num")%>" name="gall_<%=objRs("g_num")%>" value="<%=objRs("g_num")%>" />
				</td>
				<td class="text-center">
					<%=objRs.RecordCount-k%>
				</td>
				<td class="text-center">
					<%=show_array(objRs("g_show"))%>
				</td>
				<td class="text-center">
					<%=type_array(objRs("g_type"))%>
				</td>
				<td width="400" align="center">
					<a href="seoul_gallary_detail.asp<%=list_link%>&num=<%=objRs("g_num")%>"><img src="<%=objRs("g_img")%>" class="preview_img" /></a>
				</td>
				<td>
					<a href="seoul_gallary_detail.asp<%=list_link%>&num=<%=objRs("g_num")%>"><%=objRs("g_title_ko")%></a>
				</td>
				<td>
					<%=objRs("g_reg_date")%>
				</td>
			</tr>
				<% objRs.MoveNext %>
				<% Next %>
			<% End If %>
			
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-danger btn-sm" id="del_gallary" name="del_gallary" value="삭제하기" />&nbsp;
			<input type="button" class="btn btn-info btn-sm show_hide" id="show_gallary" name="show_gallary" value="메인노출" />&nbsp;
			<input type="button" class="btn btn-warning btn-sm show_hide" id="hide_gallary" name="hide_gallary" value="비노출" />&nbsp;
		</li>
		<li class="btm_btn_right">
			<a href="seoul_gallary_new.asp"><input type="button" class="btn btn-primary btn-sm" value="등록하기" /></a>&nbsp;
		</li>
	</ul>
</div>
<div class="page_wrap">
	<!-- #include file = "../view/page_template.asp" -->
</div>
<div class="search_wrap">
	<form name="search" id="search" method="get">
		<input type="text" class="form-control input-sm width_150" name="search_text" value="" placeholder="검색어" />
		<input type="submit" class="btn btn-sm btn-primary ml10" name="search" value="검색" />
		<input type="hidden" id="list_link_hidden" value="seoul_gallary.asp<%=list_link%>" />
		<input type="hidden" id="page_limit" name="page_limit" value="<%=page_limit%>" />
	</form>
</div>

<!--#include file = "index_footer.asp" -->