$(function() {
		
	/*gallary*/
	 $("#add_gallary").click(function(){
			if (!confirm("등록하시겠습니까?"))
				return;
			add_edit_gallary("add");
	 });
	 $("#edit_gallary").click(function(){
			if (!confirm("수정하시겠습니까?"))
				return;
			add_edit_gallary("edit");
	 });
	 
	 $("#g_type").change(function() {
		 var g_type = $(this).val();
		 location.href = "?g_type=" + g_type
	 });
	
	 $("#del_gallary").click(function() {
		var select = "";
		
		if ($("#g_num").length > 0)
		{
			select = $("#g_num").val() + "|";
		}
		else
		{
			if ($("#table_list>tbody").find("input:checkbox:checked").length == 0)
			{
				alert("선택된것이 없네요");
				return;
			}

			$("#table_list>tbody").find("input:checkbox:checked").each(function() {
				select += $(this).val() + "|";
			});
		}
		
		if (!confirm("정말로 지우실거에요?"))
				return;
			
		if (!confirm("한번 지우면 끝이에요!"))
			return;
		
		del_gallary(select);
	});
	
	$(".show_hide").click(function() {
		var select = "";
		
		if ($("#g_num").length > 0)
		{
			select = $("#g_num").val() + "|";
		}
		else
		{
			if ($("#table_list>tbody").find("input:checkbox:checked").length == 0)
			{
				alert("선택된것이 없네요");
				return;
			}

			$("#table_list>tbody").find("input:checkbox:checked").each(function() {
				select += $(this).val() + "|";
			});
		}
		
		if (!confirm("수정하시겠습니까?"))
			return;
		var id = $(this).attr("id");
		var type = 0;
		if (id == "show_gallary")
			type = 1;
		show_hide(select, type);
	});
	
	/*tanent gallary*/
	 $("#add_tenant_gallary").click(function(){
			if (!confirm("등록하시겠습니까?"))
				return;
			add_edit_tenant_gallary("add");
	 });
	 $("#edit_tenant_gallary").click(function(){
			if (!confirm("수정하시겠습니까?"))
				return;
			add_edit_tenant_gallary("edit");
	 });
	
	 $("#del_tenant_gallary").click(function() {
		var select = "";
		
		if ($("#g_num").length > 0)
		{
			select = $("#g_num").val() + "|";
		}
		else
		{
			if ($("#table_list>tbody").find("input:checkbox:checked").length == 0)
			{
				alert("선택된것이 없네요");
				return;
			}

			$("#table_list>tbody").find("input:checkbox:checked").each(function() {
				select += $(this).val() + "|";
			});
		}
		
		if (!confirm("정말로 지우실거에요?"))
				return;
			
		if (!confirm("한번 지우면 끝이에요!"))
			return;
		
		del_tenant_gallary(select);
	});
	
	$(".show_hide_tenant").click(function() {
		var select = "";
		
		if ($("#g_num").length > 0)
		{
			select = $("#g_num").val() + "|";
		}
		else
		{
			if ($("#table_list>tbody").find("input:checkbox:checked").length == 0)
			{
				alert("선택된것이 없네요");
				return;
			}

			$("#table_list>tbody").find("input:checkbox:checked").each(function() {
				select += $(this).val() + "|";
			});
		}
		
		if (!confirm("수정하시겠습니까?"))
			return;
		var id = $(this).attr("id");
		var type = 0;
		if (id == "show_tenant_gallary")
			type = 1;
		show_hide_tenant(select, type);
	});
	
	 /*bullentin*/
	$("#add_bulletin").click(function(){
			if (!confirm("등록하시겠습니까?"))
				return;
			add_edit_bulletin("add");
	 });
	 $("#edit_bulletin").click(function(){
			if (!confirm("수정하시겠습니까?"))
				return;
			add_edit_bulletin("edit");
	 });
	
	$("#del_bulletin").click(function() {
			var select = "";
			
			if ($("#n_num").length > 0)
			{
				select = $("#n_num").val() + "|";
			}
			else
			{
				if ($("#table_list>tbody").find("input:checkbox:checked").length == 0)
				{
					alert("선택된것이 없네요");
					return;
				}

				$("#table_list>tbody").find("input:checkbox:checked").each(function() {
					select += $(this).val() + "|";
				});
			}
			
			if (!confirm("정말로 지우실거에요?"))
					return;
				
			if (!confirm("한번 지우면 끝이에요!"))
				return;
			
			del_bulletin(select);
		});
	 
	 /*Event*/
	 $("#add_event").click(function(){
			if (!confirm("등록하시겠습니까?"))
				return;
			add_edit_event("add");
	 });
	 $("#edit_event").click(function(){
			if (!confirm("수정하시겠습니까?"))
				return;
			add_edit_event("edit");
	 });
	
	 $("#del_event").click(function() {
			var select = "";
			
			if ($("#e_num").length > 0)
			{
				select = $("#e_num").val() + "|";
			}
			else
			{
				if ($("#table_list>tbody").find("input:checkbox:checked").length == 0)
				{
					alert("선택된것이 없네요");
					return;
				}

				$("#table_list>tbody").find("input:checkbox:checked").each(function() {
					select += $(this).val() + "|";
				});
			}
			
			if (!confirm("정말로 지우실거에요?"))
					return;
				
			if (!confirm("한번 지우면 끝이에요!"))
				return;
			
			del_event(select);
		});

    /* QRCode  */
    $("#add_qrcode").click(function() {
      if (!confirm("등록하시겠습니까?")) return;
      add_edit_qrcode("add");
    });
    $("#edit_qrcode").click(function() {
      if (!confirm("수정하시겠습니까?")) return;
      add_edit_qrcode("edit");
    });
    $("#del_qrcode").click(function() {
      var select = "";
      if ($("#q_num").length > 0) {
        select = $("#q_num").val() + "|";
      } else {
        if ($("#table_list>tbody").find("input:checkbox:checked").length == 0) {
          alert("선택된것이 없네요");
          return;
        }
        $("#table_list>tbody").find("input:checkbox:checked").each(function() {
            select += $(this).val() + "|";
        });
      }

      if (!confirm("정말로 지우실거에요?")) return;
      if (!confirm("한번 지우면 끝이에요!")) return;

      del_qrcode(select);
    });
});

/* IFC 서울갤러리 */
function add_edit_gallary(mode) {
	var g_num = $("#g_num").val();
	var stat_type = $("#stat_type").val();
	var g_show = $(".g_show:checked").val();
	var g_title_ko = $("#g_title_ko").val();
	var g_title_en = $("#g_title_en").val();
	var g_list_title = $("#g_list_title").val();
	var g_img = $("#g_img").val();
	var g_group =Number($(".type_select:checked").val());
	
	if(g_title_ko.length == 0){
		
		alert("제목(국문) 을 입력해주세요.");
		$("#g_title_ko").focus();
		
		return
	}
	if(g_title_en.length == 0){
		
		alert("제목(영문) 을 입력해주세요.");
		$("#g_title_en").focus();
		
		return
	}
	
	$.ajax ({
		type : "POST",
		url : "ajax/gallary_proc.asp",
		dataType : "json",
		data : ({
					"mode" : mode,
					"g_num" : g_num,
					"stat_type" : stat_type,
					"g_show" : g_show,
					"g_title_ko" : g_title_ko,
					"g_title_en" : g_title_en,
					"g_list_title" : g_list_title,
					"g_img" : g_img,
					"g_group" : g_group
				}),
				success : function (data)
				{
					$("#index_loading_overlay").hide();
				
					if (typeof data != 'object')
					{
						alert("Error :: Ajax Return Error");
					}
					else
					{
						var error_msg = data.error_msg;
						if (data.result == 0)
						{
							if (mode == 'add')
							{
								alert("갤러리 정보를 추가했습니다.");
								location.href = $("#list_link_hidden").val();
							}
							else
							{
								alert("정보를 수정했습니다.");
								location.href = location.href;
							}
						}
						else
						{
							alert(error_msg);
						}
					}
				},
				error : function (data)
				{
					var error_msg = data.error_msg;
					alert(error_msg);
					alert("Error : Ajax Error");
					$("#index_loading_overlay").hide();
				}
			});
}

function del_gallary(select) {
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "ajax/gallary_proc.asp",
		dataType : "json",
		data : ({
					"mode" : "del", 
					"select" : select
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					alert("갤러리 정보 삭제했어요.");
					if ($("#g_num").length > 0)
					{
						location.href = $("#list_link_hidden").val();
					}
					else
					{
						location.href = location.href;
					}
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}

function show_hide(select, type) {
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "ajax/gallary_proc.asp",
		dataType : "json",
		data : ({
					"mode" : "show", 
					"select" : select,
					"type" : type
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					alert("갤러리 정보 수정했습니다.");
					location.href = location.href;
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}

/* IFC 입주사 갤러리 */
function add_edit_tenant_gallary(mode) {
	var g_num = $("#g_num").val();
	var stat_type = $("#stat_type").val();
	var g_show = $(".g_show:checked").val();
	var g_title_ko = $("#g_title_ko").val();
	var g_title_en = $("#g_title_en").val();
	var g_list_title = $("#g_list_title").val();
	var g_img = $("#g_img").val();
	var g_group =Number($(".type_select:checked").val());
	
	if(g_title_ko.length == 0){
		
		alert("제목(국문) 을 입력해주세요.");
		$("#g_title_ko").focus();
		
		return
	}
	if(g_title_en.length == 0){
		
		alert("제목(영문) 을 입력해주세요.");
		$("#g_title_en").focus();
		
		return
	}
	
	$.ajax ({
		type : "POST",
		url : "ajax/tenant_proc.asp",
		dataType : "json",
		data : ({
					"mode" : mode,
					"g_num" : g_num,
					"stat_type" : stat_type,
					"g_show" : g_show,
					"g_title_ko" : g_title_ko,
					"g_title_en" : g_title_en,
					"g_list_title" : g_list_title,
					"g_img" : g_img,
					"g_group" : g_group
				}),
				success : function (data)
				{
					$("#index_loading_overlay").hide();
				
					if (typeof data != 'object')
					{
						alert("Error :: Ajax Return Error");
					}
					else
					{
						var error_msg = data.error_msg;
						if (data.result == 0)
						{
							if (mode == 'add')
							{
								alert("갤러리 정보를 추가했습니다.");
								location.href = $("#list_link_hidden").val();
							}
							else
							{
								alert("정보를 수정했습니다.");
								location.href = location.href;
							}
						}
						else
						{
							alert(error_msg);
						}
					}
				},
				error : function (data)
				{
					var error_msg = data.error_msg;
					alert(error_msg);
					alert("Error : Ajax Error");
					$("#index_loading_overlay").hide();
				}
			});
}

function del_tenant_gallary(select) {
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "ajax/tenant_proc.asp",
		dataType : "json",
		data : ({
					"mode" : "del", 
					"select" : select
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					alert("갤러리 정보 삭제했어요.");
					if ($("#g_num").length > 0)
					{
						location.href = $("#list_link_hidden").val();
					}
					else
					{
						location.href = location.href;
					}
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}

function show_hide_tenant(select, type) {
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "ajax/tenant_proc.asp",
		dataType : "json",
		data : ({
					"mode" : "show", 
					"select" : select,
					"type" : type
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					alert("갤러리 정보 수정했습니다.");
					location.href = location.href;
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}

/* 공지사항 */
function add_edit_bulletin(mode) {
	var n_num = $("#n_num").val();
	var n_group = $(".n_group:checked").val();
	var n_title = $("#n_title").val();
	var n_text = CKEDITOR.instances['n_text'].getData();
		
	if (n_title.length == 0)
	{
		alert("제목을 입력 해 주세요");
		$("#n_title").focus();
		return;
	}
	
	if (n_text.length == 0)
	{
		alert("내용을 입력 해 주세요");
		$("#n_text").focus();
		return;
	}
	
	var img = "";
	$(".img_section").find("img").each(function() {
		img += $(this).attr("src") + "|";
	})
	
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "ajax/bulletin_proc.asp",
		dataType : "json",
		data : ({
					"mode" : mode, 
					"n_num" : n_num,
					"n_group" : n_group,
					"n_title" : n_title,
					"n_text" : n_text,
					"img" : img
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					if (mode == "add")
					{
						alert("공지를 등록했습니다.");
						location.href = $("#list_link_hidden").val();
					}
					else
					{
						alert("공지를 수정했습니다.");
						location.href = location.href;
					}
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}

function del_bulletin(select) {
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "ajax/bulletin_proc.asp",
		dataType : "json",
		data : ({
					"mode" : "del", 
					"select" : select
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					alert("공지를 삭제했어요.");
					if ($("#n_num").length > 0)
					{
						location.href = $("#list_link_hidden").val();
					}
					else
					{
						location.href = location.href;
					}
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}

/* 이벤트 */
function add_edit_event(mode) {
	var e_num = $("#e_num").val();
	var e_group = $(".e_group:checked").val();
	var e_title = $("#e_title").val();
	var e_img = $("#e_img").val();
	var e_text = CKEDITOR.instances['e_text'].getData();
	var e_state = $(".stat_type option:selected").val();
	var e_st_date = $("#start_date").val();
	var e_ed_date = $("#end_date").val();
	
	//alert("test");alert(e_state);
		
	if (e_title.length == 0)
	{
		alert("제목1을 입력 해 주세요");
		$("#e_title").focus();
		return;
	}
	
	if (e_text.length == 0)
	{
		alert("내용을 입력 해 주세요");
		$("#e_text").focus();
		return;
	}
	if (e_st_date.length == 0)
	{
		alert("시작 날짜를 입력 해 주세요");
		$("#start_date").focus();
		return;
	}
	if (e_ed_date.length == 0)
	{
		alert("종료 날짜를 입력 해 주세요");
		$("#end_date").focus();
		return;
	}
	
	var img = "";
	$(".img_section").find("img").each(function() {
		img += $(this).attr("src") + "|";
	})
	
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "ajax/event_proc.asp",
		dataType : "json",
		data : ({
					"mode" : mode, 
					"e_num" : e_num,
					"e_group" : e_group,
					"e_state" : e_state,
					"e_title" : e_title,
					"e_img" : e_img,
					"e_text" : e_text,
					"e_st_date": e_st_date,
					"e_ed_date": e_ed_date,
					"img" : img
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					if (mode == "add")
					{
						alert("이벤트를 등록했습니다.");
						location.href = $("#list_link_hidden").val();
					}
					else
					{
						alert("이벤트를 수정했습니다.");
						location.href = location.href;
					}
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}

function del_event(select) {
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "ajax/event_proc.asp",
		dataType : "json",
		data : ({
					"mode" : "del", 
					"select" : select
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					alert("이벤트 정보 삭제했어요.");
					if ($("#g_num").length > 0)
					{
						location.href = $("#list_link_hidden").val();
					}
					else
					{
						location.href = location.href;
					}
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
}

/* QR코드 관리 */
function add_edit_qrcode(mode) {

    var q_num = $("#q_num").val();
    var q_title = $("#q_title").val();
    var q_text = window.editor.getData();

    if (q_title.length == 0) {
        alert("제목을 입력 해 주세요");
        $("#q_title").focus();
        return;
    }

    if (q_text.length == 0) {
        alert("내용을 입력 해 주세요");
        $("#q_text").focus();
        return;
    }

    if ($("#index_loading_overlay").css("display") != 'none') {
        alert("입력 중 입니다");
        return false;
    }

    $("#index_loading_overlay").show();
    $.ajax ({
      type : "POST",
      url : "ajax/qrcode_proc.asp",
      dataType : "json",
      data : ({
        "mode" : mode,
        "q_num" : q_num,
        "q_title" : q_title,
        "q_text" : q_text
      }),
      success : function (data) {
        $("#index_loading_overlay").hide();
        if (typeof data != 'object') {
          alert("Error :: Ajax Return Error" + data.result);
        } else {
          var error_msg = data.error_msg;
          if (data.result == 0) {
            if (mode == "add") {
              alert("게시글을 등록했습니다.");
              location.href = $("#list_link_hidden").val();
            } else {
              alert("게시글을 수정했습니다.");
              console.log(location.href);
            }
          } else {
            alert(error_msg);
          }
        }
      },
      error : function (data) {
        alert("Erxror : Ajax Error");
        $("#inde_loading_overlay").hide();
      }
    });
}

function del_qrcode(select) {

    if ($("#index_loading_overlay").css("display") != 'none') {
        alert("입력 중 입니다");
        return false;
    }

    $("#index_loading_overlay").show();
    $.ajax ({
        type : "POST",
        url : "ajax/qrcode_proc.asp",
        dataType : "json",
        data : ({
                    "mode" : "del",
                    "select" : select
                }),
        success : function (data)
        {
            console.log(data);
            $("#index_loading_overlay").hide();
            if (typeof data != 'object') {
                alert("Error :: Ajax Return Error");
            } else {
                var error_msg = data.error_msg;
                if (data.result == 0) {
                    alert("QR코드를 삭제했어요.");
                    if ($("#q_num").length > 0) {
                        location.href = $("#list_link_hidden").val();
                    } else {
                        location.href = location.href;
                    }
                } else {
                    alert(error_msg);
                }
            }
        },
        error : function (data)
        {
            alert("Error : Ajax Error");
            $("#index_loading_overlay").hide();
        }
    });
}
