<!-- #include file = "../inc/_config.asp" -->
<%
'Login Check
Dim intAdminSeq, strAdminId, strAdminName, loginFlag
intAdminSeq = Session("admin_seq")
strAdminId = Session("admin_id")
strAdminName = Session("admin_name")
loginFlag = true

If intAdminSeq = "" OR strAdminId = "" OR strAdminName = "" Then
	loginFlag = false
Else
	Dim objConn, objRs, arrRs, strQuery

	strQuery = "select * from tblManager where intSeq  = '"& intAdminSeq &"' and strID = '"& strAdminId &"' and strName = '"& strAdminName &"'"

	set objConn = OpenDBConnection()
	set objRs = SendQuery(objConn,strQuery)

	If objRs.EOF Then
		loginFlag = false
	End If

	objRs.Close
End If

If loginFlag Then
	Response.Redirect "admin_list.asp"
End If

%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>:::IFC 관리자:::</title>
	<link rel="stylesheet" type="text/css" href="css/base.css" />
	<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="/js/jquery.placeholder.js"></script>
	<script type="text/javascript" src="/js/md5.js"></script>
	<script type="text/javascript" src="js/admin_script.js"></script>

	<!-- for bootstrap -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<!-- link rel="stylesheet" href="/css/bootstrap-theme.min.css" / -->
	<!-- //for bootstrap -->

	<!-- for jqueryui -->
	<script type="text/javascript" src="/js/jquery-ui-1.10.4.min.js"></script>
	<link rel="stylesheet" href="/css/jquery-ui-1.10.4.min.css" />
	<!-- //for jqueryui -->

	<!-- for upload -->
	<script type="text/javascript" src="/js/jquery.uploadfile.min.js"></script>
	<link rel="stylesheet" href="/css/uploadfile.css" />
	<!-- //for upload -->

	<!-- for ckeditor -->
	<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
	<!-- //for ckeditor -->

	<!-- for placeholder -->
	<script type="text/javascript" src="/js/placeholders.min.js"></script>
	<!-- //for placeholder -->
</head>
<body>
<div id="index_loading_overlay">
	<div class="index_loading_overlay_img">
		<img src="/img/ajax-loader.gif" alt="overlay loading img" />
	</div>
</div>
<div class="index_wrap">
	<div class="index">
		<ul>
			<li class="index_1">
				<img src="img/logo.png" />
			</li>
			<li class="index_2">
				<h1>IFC Seoul Website Administrator</h1>
			</li>
			<!--<li class="index_3">
				<p>아이디</p>
				<p class="mt10">패스워드</p>-->
			</li>
			<li class="index_4">
				<p><input type="text" class="form-control" id="admin_id" value="" placeholder="아이디" /></p>
				<p><input type="password" class="form-control" id="admin_pw" class="mt10" value="" placeholder="비밀번호" /></p>
			</li>
			<li class="index_5">
				<button type="button" class="btn btn-primary btn-sm" id="login_btn"><h4>LOGIN</h4></button>
			</li>
		</ul>
	</div>
</div>
</body>
</html>
