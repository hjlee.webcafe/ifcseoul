<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%

Dim selectQuery, num, check(1),state_array(2),imgRs,state
num = Request.QueryString("num")
state = Request.QueryString("state")

selectQuery = "select * from dbo.tblEvent where e_num = '"& num &"'"

e_img_hide = "hide_row"

set objRs = SendQuery(objConn,selectQuery)
check(objRs("e_group")) = "checked"
state_array(objRs("e_state")) = "selected"

If objRs("e_img") <> "" Then
	e_img_hide = ""
	img_name_array = Split(objRs("e_img"), "/")
	e_img_name = img_name_array(UBound(img_name_array))
End If

// get image list
selectQuery = "select * from dbo.tblEvent_img where e_num = '"& num &"'"

set imgRs = SendQuery(objConn,selectQuery)

%>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="이벤트 수정">
		<caption>이벤트 수정</caption>
		<colgroup>
			<col width="150px" >
			<col>
		</colgroup>
		<tbody>
			<tr>
				<th>
					구분
				</th>
				<td>
					<label><input type="radio" name="e_group" class="e_group" value="0" <%=check(0)%> /> 국문</label>
					<label><input type="radio" name="e_group" class="e_group ml20" value="1" <%=check(1)%> /> 영문</label>
				</td>
			</tr>
			<tr>
				<th>
					제목
				</th>
				<td>
					<input type="text" class="i_text50" name="e_title" id="e_title" maxlength="80" value="<%=objRs("e_title")%>" placeholder="제목을 입력 해 주세요." /> (80자 이내)
				</td>
			</tr>
			<tr>
				<th>
					구분
				</th>
				<td>
					<select id="stat_type" name="stat_type" class="stat_type form-control input-sm width_100">
					<option value="0"<%=state_array(0)%>>Seoul</option>
					<option value="1"<%=state_array(1)%>>Mall</option>
					<option value="2"<%=state_array(2)%>>Conrad</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>
					썸네일
				</th>
				<td>
					<input name="e_img" id="e_img" type="text" class="i_text50 e_img" value="<%=objRs("e_img")%>" />
					<div id="e_img_upload" class="upload_btn_2">Upload</div>
					<br />
					<div class="current_image_wrap <%=e_img_hide%>">
						<span id="e_img_name"><%=e_img_name%></span>
						<input type="hidden" id="e_img_path" name="e_imgpath" value="<%=objRs("e_img")%>" />
						<input type="button" class="btn btn-sm btn-danger delete_btn" id="e_img_del" name="e_img_del" value="삭제" />
					</div>
					<br />
					<img src="<%=objRs("e_img")%>" id="e_img_preview" />
				</td>
			</tr>
			<tr>
				<th>
					기간
				</th>
				<td>
					<input type="text" id="start_date" name="start_date" value="<%=objRs("e_st_date")%>" readonly />~<input type="text" id="end_date" name="end_date" value="<%=objRs("e_ed_date")%>" readonly />
				</td>
			</tr>
			<tr>
				<th>
					상태
				</th>			
				<td>
					<%=state%>
				</td>
			</tr>
			<tr>
				<th>
					내용
				</th>
				<td>
					<textarea type="text" class="ckeditor" name="e_text" id="e_text" value="" placeholder="내용을 입력 해 주세요"><%=objRs("e_text")%></textarea>
				</td>
			<tr>
				<th>
					이미지
				</th>
				<td>
					<div class="img_section">
						<ul>
							<% For i = 0 To imgRs.RecordCount - 1 %>
							<li class="img_wrap"><img src="<%=imgRs("ei_img")%>" class="upload_img" /><span class="upload_img_del">x</span></li>
							<% imgRs.MoveNext %>
							<% Next %>
						</ul>
						<input type="hidden" class="ckeditor_img" value="e_text_img" />
					</div>
					<div class="insert img">
						<div id="img_upload" class="upload_btn">Upload</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-danger btn-sm" id="del_event" name="del_event" value="삭제하기" />&nbsp;
			<input type="button" class="btn btn-primary btn-sm" id="edit_event" name="edit_event" value="수정하기" />&nbsp;
			<input type="button" class="btn btn-warning btn-sm" id="back_to_list" name="back_to_list" value="목록으로" />
		</li>
		<li class="btm_btn_right">
			<!--<input type="hidden" id="list_link_hidden" value="site_event.asp<%=list_link%>"/>-->
			<input type="hidden" id="list_link_hidden" value="site_event.asp"/>
			<input type="hidden" id="e_num" value="<%=num%>" />
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->