<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%

Dim selectQuery, num, p_group(1), imgRs
num = Request.QueryString("num")
selectQuery = "select * from tblPress where p_num = '"& num &"'"

set objRs = SendQuery(objConn,selectQuery)
p_group(objRs("p_group")) = "checked"

p_img_hide = "hide_row"

If objRs("p_img") <> "" Then
	p_img_hide = ""
	img_name_array = Split(objRs("p_img"), "/")
	p_img_name = img_name_array(UBound(img_name_array))
End If

// get image list
selectQuery = "select * from tblPress_img where p_num = '"& num &"'"

set imgRs = SendQuery(objConn,selectQuery)

%>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="제품 등록">
		<caption>제품 등록</caption>
		<colgroup>
			<col width="150px" >
			<col>
		</colgroup>
		<tbody>
			<tr>
				<th>
					구분
				</th>
				<td>
					<label><input type="radio" name="p_group" class="p_group" value="0" <%=p_group(0)%> /> 국문</label>
					<label><input type="radio" name="p_group" class="p_group ml20" value="1" <%=p_group(1)%> /> 영문</label>
				</td>
			</tr>
			<tr>
				<th>
					제목
				</th>
				<td>
					<input type="text" class="i_text50" name="p_title" id="p_title" maxlength="80" value="<%=objRs("p_title")%>" placeholder="제목을 입력 해 주세요." /> (80자 이내)
				</td>
			</tr>
			<!--
			<tr>
				<th>
					요약
				</th>
				<td>
					<input type="text" class="i_text100" name="p_summary" id="p_summary" maxlength="100" value="<%=objRs("p_summary")%>" placeholder="요약을 입력 해 주세요." />
				</td>
			</tr>
			<tr>
				<th>
					썸네일
				</th>
				<td>
					<input name="p_img" id="p_img" type="text" class="i_text50 ii_img" value="<%=objRs("p_img")%>" />
					<div id="p_img_upload" class="upload_btn_2">Upload</div>
					<br />
					<div class="current_image_wrap <%=p_img_hide%>">
						<span id="p_img_name"><%=p_img_name%></span>
						<input type="hidden" id="p_img_path" name="p_imgpath" value="<%=objRs("p_img")%>" />
						<input type="button" class="btn btn-sm btn-danger delete_btn" id="p_img_del" name="p_img_del" value="삭제" />
					</div>
					<br />
					<img src="<%=objRs("p_img")%>" id="p_img_preview" />
				</td>
			</tr>
			-->
			<tr>
				<th>
					내용
				</th>
				<td>
					<textarea type="text" class="ckeditor" name="p_text" id="p_text" value="" placeholder="내용을 입력 해 주세요"><%=objRs("p_text")%></textarea>
				</td>
			<tr>
				<th>
					이미지
				</th>
				<td>
					<div class="img_section">
						<ul>
							<% For i = 0 To imgRs.RecordCount - 1 %>
							<li class="img_wrap"><img src="<%=imgRs("pi_img")%>" class="upload_img" /><span class="upload_img_del">x</span></li>
							<% imgRs.MoveNext %>
							<% Next %>
						</ul>
						<input type="hidden" class="ckeditor_img" value="p_text_img" />
					</div>
					<div class="insert img">
						<div id="img_upload" class="upload_btn">Upload</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-danger btn-sm" id="del_press" name="del_press" value="삭제하기" />&nbsp;
			<input type="button" class="btn btn-primary btn-sm" id="edit_press" name="edit_press" value="수정하기" />&nbsp;
			<input type="button" class="btn btn-warning btn-sm" id="back_to_list" name="back_to_list" value="목록으로" />
		</li>
		<li class="btm_btn_right">
			<input type="hidden" id="list_link_hidden" value="press_release.asp<%=list_link%>"/>
			<input type="hidden" id="p_num" value="<%=num%>" />
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->