<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>:::IFC 관리자:::</title>
    <link rel="stylesheet" type="text/css" href="css/base.css" />
    <script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="/js/md5.js"></script>
    <script type="text/javascript" src="js/admin_script.js"></script>
    <script type="text/javascript" src="js/admin_script_temp.js"></script>
    <script type="text/javascript" src="js/gallary_script.js"></script>

    <!-- for bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <!-- //for bootstrap -->

    <!-- for jqueryui -->
    <script type="text/javascript" src="/js/jquery-ui-1.10.4.min.js"></script>
    <link rel="stylesheet" href="/css/jquery-ui-1.10.4.min.css" />
    <!-- //for jqueryui -->

    <!-- for upload -->
    <script type="text/javascript" src="/js/jquery.uploadfile.min.js"></script>
    <link rel="stylesheet" href="/css/uploadfile.css" />
    <!-- //for upload -->

    <!-- for ckeditor -->
    <script type="text/javascript" src="/ckeditor5/build/ckeditor.js"></script>
    <!-- //for ckeditor -->

    <!-- for placeholder -->
    <script type="text/javascript" src="/js/placeholders.min.js"></script>
    <!-- //for placeholder -->
</head>
<body>
<div id="index_loading_overlay">
    <div class="index_loading_overlay_img">
        <img src="/img/ajax-loader.gif" alt="overlay loading img" />
    </div>
</div>
<div class="wrap">
    <!-- top -->
    <div class="top">
        <ul>
            <li>
                <img src="img/logo.png" height="60px" /> <h3>IFC Seoul Website Administrator</h3>
            </li>
            <li class="text-right">
                <button type="button" id="logout" class="btn btn-dark btn-sm mr20">LOGOUT</button>
            </li>
        </ul>
    </div>
    <!-- //top -->
    <div class="content_wrap">
        <!-- side menu -->
        <div class="left_menu">
            <ul>
                <!-- li class="menu_title">
                    Title
                </li-->
                <li>
                    <a href="admin_list.asp">관리자</a>
                </li>
                <li>
                    <a href="seoul_gallary.asp">IFC 서울 갤러리</a>
                </li>
                <li>
                    <a href="tenant_gallary.asp">IFC 입주사 갤러리</a>
                </li>
                <li>
                    <a href="facility_gallary.asp">IFC 시설 갤러리 관리</a>
                </li>
                <li>
                    <a href="qrcode_list.asp">QR코드 관리</a>
                </li>
                <li>
                    <a href="bulletin.asp">공지사항</a>
                </li>
                <li>
                    <a href="press_release.asp">보도자료 관리</a>
                </li>
                <li>
                    <a href="site_event.asp">이벤트 관리</a>
                </li>
            </ul>
        </div>
        <!-- //side menu -->
        <!-- contents -->
        <div class="contents">
            <nav aria-label="breadcrumb">
                <ol id="menu_nav" class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                </ol>
            </nav>
