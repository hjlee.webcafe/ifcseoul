<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->

<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="관리자 등록">
		<caption>관리자등록</caption>
		<colgroup>
			<col width="150px" >
			<col>
		</colgroup>
		<tbody>
			<tr>
				<th>
					관리자 이름
				</th>
				<td>
					<input name="as_name" id="as_name" type="text" class="width_150" value="" maxlength="50" />
				</td>
			</tr>
			<tr>
				<th>
					관리자 아이디
				</th>
				<td>
					<input name="as_id" id="as_id" type="text" class="width_150" value="" maxlength="20" />
					<input type="button" class="btn btn-info btn-sm" id="chk_as_id" value="아이디확인" />
					<input type="hidden" id="id_chk_result" name="id_chk_result" value="" />
					<input type="hidden" id="id_chk_store" name="id_chk_store" value="" />
				</td>
			</tr>
			<tr>
				<th>
					관리자 비밀번호
				</th>
				<td>
					<input name="as_pw" id="as_pw" type="password" class="width_150" value="" />
				</td>
			</tr>
			<tr>
				<th>
					관리자 비밀번호 확인
				</th>
				<td>
					<input name="as_pw_chk" id="as_pw_chk" type="password" class="width_150" value="" />
				</td>
			</tr>
			<tr>
				<th>
					관리자 연락처
				</th>
				<td>
					<input name="as_tel" id="as_tel" type="text" class="width_150" value="" maxlength="12" />
				</td>
			</tr>
			<tr>
				<th>
					관리자 이메일
				</th>
				<td>
					<input name="as_email" id="as_email" type="text" class="i_text50" value="" />
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-primary btn-sm" id="add_admin" name="add_admin" value="등록하기" />&nbsp;
			<input type="button" class="btn btn-warning btn-sm" id="back_to_list" name="back_to_list" value="목록으로" />
		</li>
		<li class="btm_btn_right">
			<input type="hidden" id="list_link_hidden" value="admin_list.asp<%=list_link%>"/>
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->