<!-- #include file = "../../inc/_config.asp" -->
<%
' remove cache
Response.Expires = -1
Response.ExpiresAbsolute = now() - 1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!-- #include file = "../admin_check.asp" -->
<%

Dim mode, result
Set result = jsObject()
result("result") = 0
result("error_msg") = ""

if Request.Form("mode") = "" Then
	result("result") = 2
	result("error_msg") = "The mode is not selected"
	result.Flush
	Response.End
End If

mode = Request.Form("mode")

Dim g_num,g_title_ko,g_title_en,g_img,g_group,error_flag,num, g_list_title
Err.clear
On Error Resume Next

If mode = "add" Then

	error_flag = false

	If Request.Form("g_title_ko") = "" Then
		result("result") = 2
		result("error_msg") = "the id is empty | "
		error_flag = true
	End If
	If Request.Form("g_img") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the img is empty | "
		error_flag = true
	End If
	If Request.Form("g_group") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the group is empty | "
		error_flag = true
	End If	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	g_type = Request.Form("stat_type")
	g_show = Request.Form("g_show")
	g_title_ko = Request.Form("g_title_ko")
	g_title_en = Request.Form("g_title_en")
	g_img = Request.Form("g_img")
	g_list_title = Request.Form("g_list_title")
	
	// insert 
	strQuery = "insert into dbo.tblGall (g_type, g_show, g_title_ko, g_title_en, g_list_title, g_img) values ('"& g_type &"', '"& g_show &"', '"& g_title_ko &"', '"& g_title_en &"', '"& g_list_title &"', '"& g_img &"')"
	
	objConn.Execute(strQuery)
	
	If (objConn.Errors.Count > 0) Then
		result("result") = 2
		result("error_msg") = "Insert Error : " & strQuery
		result.Flush
		Response.End
	End If
	
	result.Flush
	Response.End

ElseIf mode = "edit" Then

	error_flag = false
	
	If Request.Form("g_num") = "" Then
		result("result") = 2
		result("error_msg") = "the num is empty | "
		error_flag = true
	End If
	If Request.Form("g_title_ko") = "" Then
		result("result") = 2
		result("error_msg") = "the title is empty | "
		error_flag = true
	End If
	If Request.Form("g_img") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the img is empty | "
		error_flag = true
	End If
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	g_num = Request.Form("g_num")
	g_type = Request.Form("stat_type")
	g_show = Request.Form("g_show")
	g_title_ko = Request.Form("g_title_ko")
	g_title_en = Request.Form("g_title_en")
	g_list_title = Request.Form("g_list_title")
	g_img = Request.Form("g_img")
	
	// update 
	strQuery = "update dbo.tblGall set g_type = '" & g_type & "', g_show = '"& g_show &"', g_title_ko='"& g_title_ko &"',g_title_en='"&g_title_en&"', g_list_title = '"& g_list_title &"', g_img='"& g_img &"',g_mod_date=getDate() where g_num='"&g_num&"'"
	
	objConn.Execute(strQuery)
	
	If (objConn.Errors.Count > 0) Then
		result("result") = 2
		result("error_msg") = "Insert Error : " & strQuery
		result.Flush
		Response.End
	End If
	
	result.Flush
	Response.End

ElseIf mode = "del" Then

	error_flag = false

	If Request.Form("select") = "" Then
		result("result") = 2
		result("error_msg") = "the select is empty"
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	selected = Request.Form("select")
	select_array = Split(selected, "|")
	
	For i = 0 To UBound(select_array) - 1
		num = select_array(i)
		// delete 
		strQuery = "delete from tblGall where g_num = '" & num & "'"
		objConn.Execute(strQuery)
		
		If Err.Number <> 0 Then
			result("result") = 2
			result("error_msg") = "Delete Error : " & Err.Description & " " & strQuery
			result.Flush
			Response.End
		End If
	Next
	
	result.Flush
	Response.End

ElseIf mode = "show" Then

	error_flag = false

	If Request.Form("select") = "" Then
		result("result") = 2
		result("error_msg") = "the select is empty"
		error_flag = true
	End If
	
	If Request.Form("type") = "" Then
		result("result") = 2
		result("error_msg") = "the type is empty"
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	selected = Request.Form("select")
	gtype = Request.Form("type")
	select_array = Split(selected, "|")
	
	For i = 0 To UBound(select_array) - 1
		num = select_array(i)
		// delete 
		strQuery = "update tblGall set g_show = '"& gtype &"' where g_num = '" & num & "'"
		objConn.Execute(strQuery)
		
		If Err.Number <> 0 Then
			result("result") = 2
			result("error_msg") = "Delete Error : " & Err.Description & " " & strQuery
			result.Flush
			Response.End
		End If
	Next
	
	result.Flush
	Response.End

End If

%>