<!-- #include file = "../../inc/_config.asp" -->
<%
' remove cache
Response.Expires = -1
Response.ExpiresAbsolute = now() - 1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!-- #include file = "../admin_check.asp" -->
<%

Dim mode, result
Set result = jsObject()
result("result") = 0
result("error_msg") = ""

if Request.Form("mode") = "" Then
	result("result") = 2
	result("error_msg") = "The mode is not selected"
	result.Flush
	Response.End
End If

mode = Request.Form("mode")

Dim as_num, as_name, as_id, as_pw, as_tel, as_email, error_flag, selected, select_array, num, pw_where
Err.clear
On Error Resume Next

If mode = "chk_id" Then

	If Request.Form("as_id") = "" Then
		result("result") = 2
		result("error_msg") = "the id is empty"
		result.Flush
		Response.End
	End If
	
	as_id = Request.Form("as_id")
	
	// check id 
	strQuery = "select * from tblManager where strID = '"& as_id &"' "

	set objRs = SendQuery(objConn,strQuery)
	
	If objRs.EOF Then
		result("result") = 0
	Else
		result("result") = 1
	End If
	
	result.Flush
	Response.End

ElseIf mode = "add" Then

	error_flag = false

	If Request.Form("as_id") = "" Then
		result("result") = 2
		result("error_msg") = "the id is empty | "
		error_flag = true
	End If
	If Request.Form("as_name") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the name is empty | "
		error_flag = true
	End If
	If Request.Form("as_pw") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the pw is empty"
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	as_id = Request.Form("as_id")
	as_name = Request.Form("as_name")
	as_pw = Request.Form("as_pw")
	as_tel = Request.Form("as_tel")
	as_email = Request.Form("as_email")
	
	// insert 
	strQuery = "insert into tblManager (strID, strName, strPW, regDate, strTel, strEmail) values ('"& as_id  &"', '"& as_name &"', '"& as_pw &"', getDate(), '"& as_tel &"', '"& as_email &"')"
	objConn.Execute(strQuery)
	
	If Err.Number <> 0 Then
		result("result") = 2
		result("error_msg") = "Insert Error : " & Err.Description
		result.Flush
		Response.End
	End If
	
	result.Flush
	Response.End

ElseIf mode = "edit" Then

	error_flag = false

	If Request.Form("as_num") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the number is empty | "
		error_flag = true
	End If
	If Request.Form("as_name") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the name is empty | "
		error_flag = true
	End If
	If Request.Form("as_pw") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the pw is empty"
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	as_num = Request.Form("as_num")
	as_name = Request.Form("as_name")
	as_pw = Request.Form("as_pw")
	as_tel = Request.Form("as_tel")
	as_email = Request.Form("as_email")
	
	If as_pw <> "" Then
		pw_where = ", strPW = '"& as_pw &"'"
	End If
	
	// insert 
	strQuery = "update tblManager  set strName = '"& as_name &"', strTel = '"& as_tel &"', strEmail = '"& as_email &"' "& pw_where &" where intSeq = '"& as_num &"'"
	objConn.Execute(strQuery)
	
	If Err.Number <> 0 Then
		result("result") = 2
		result("error_msg") = "Update Error : " & Err.Description & " " & strQuery
		result.Flush
		Response.End
	End If
	
	result.Flush
	Response.End

ElseIf mode = "del" Then

	error_flag = false

	If Request.Form("select") = "" Then
		result("result") = 2
		result("error_msg") = "the select is empty"
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	selected = Request.Form("select")
	select_array = Split(selected, "|")
	
	For i = 0 To UBound(select_array) - 1
		num = select_array(i)
		// delete 
		strQuery = "delete from tblManager where intSeq = '" & num & "'"
		objConn.Execute(strQuery)
		
		If Err.Number <> 0 Then
			result("result") = 2
			result("error_msg") = "Delete Error : " & Err.Description & " " & strQuery
			result.Flush
			Response.End
		End If
	Next
	
	result.Flush
	Response.End

End If

%>