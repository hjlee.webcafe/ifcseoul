<!-- #include file = "../../inc/_config.asp" -->
<%
' remove cache
Response.Expires = -1
Response.ExpiresAbsolute = now() - 1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!-- #include file = "../admin_check.asp" -->
<%

Dim mode, result
Set result = jsObject()
result("result") = 0
result("error_msg") = ""

if Request.Form("mode") = "" Then
	result("result") = 2
	result("error_msg") = "The mode is not selected"
	result.Flush
	Response.End
End If

mode = Request.Form("mode")

Dim p_num, p_group, p_title, p_text, error_flag, selected, select_array, num, img, img_array, p_summary
Err.clear
On Error Resume Next

If mode = "add" Then

	'"p_num" : p_num,
	'"p_group" : p_group,
	'"p_title" : p_title,
//	'"p_img" : p_img,
//	'"p_summary" : p_summary,
	'"p_text" : p_text,
	'"img" : img

	error_flag = false

	If Request.Form("p_title") = "" Then
		result("result") = 2
		result("error_msg") = "the title is empty | "
		error_flag = true
	End If
	If Request.Form("p_text") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the content is empty | "
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	p_group = Request.Form("p_group")
	p_title = Request.Form("p_title")
	p_text = Request.Form("p_text")
//	p_img = Request.Form("p_img")
	img = Request.Form("img")
//	p_summary = Request.Form("p_summary")
	
	// for flag
	dim thisTime, unixEpoch, thisUTime
	thisTime = DateSerial(Year(Now), Month(Now), Day(Now)) + TimeSerial(Hour(Now), Minute(Now), Second(Now))
	unixEpoch = "01/01/1970 00:00:00"
	thisUTime = DateDiff("s", unixEpoch, thisTime)
	
	// insert 
	strQuery = "insert into tblPress (p_group, p_title, p_text, p_reg_date, p_flag) values ('"& p_group  &"', '"& p_title &"', '"& p_text &"', getDate(), '"& thisUTime &"')"
	objConn.Execute(strQuery)
	
	If Err.Number <> 0 Then
		result("result") = 2
		result("error_msg") = "Insert Error : " & Err.Description
		result.Flush
		Response.End
	End If
	
	If img <> "" Then
		// get p_num
		strQuery = "select p_num from tblPress where p_flag = '"& thisUTime &"'"
		set objRs = SendQuery(objConn,strQuery)
		num = objRs("p_num")
		
		img_array = Split(img, "|")
		For i = 0 To UBound(img_array)-1
			If img_array(i) <> "" Then
				strQuery = "insert into tblPress_img (p_num, pi_img) values ('"& num  &"', '"& img_array(i) &"')"
				objConn.Execute(strQuery)
				
				If Err.Number <> 0 Then
					result("result") = 2
					result("error_msg") = "Insert Error : " & Err.Description
					result.Flush
					Response.End
				End If
			End If
		Next
	End If
	
	result.Flush
	Response.End

ElseIf mode = "edit" Then

	'"p_num" : p_num,
	'"p_group" : p_group,
	'"p_title" : p_title,
//	'"p_img" : p_img,
//	'"p_summary" : p_summary,
	'"p_text" : p_text,
	'"img" : img

	error_flag = false
	
	If Request.Form("p_num") = "" Then
		result("result") = 2
		result("error_msg") = "the ㅜㅕㅡㅠㄷㄱ is empty | "
		error_flag = true
	End If
	If Request.Form("p_title") = "" Then
		result("result") = 2
		result("error_msg") = "the title is empty | "
		error_flag = true
	End If
	If Request.Form("p_text") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the content is empty | "
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	p_num = Request.Form("p_num")
	p_group = Request.Form("p_group")
	p_title = Request.Form("p_title")
	p_text = Request.Form("p_text")
//	p_img = Request.Form("p_img")
//	p_summary = Request.Form("p_summary")
	img = Request.Form("img")
	
	// insert 
	strQuery = "update tblPress set p_group = '"& p_group  &"', p_title = '"& p_title &"', p_text = '"& p_text &"', p_mod_date = getDate() where p_num = '" & p_num & "'"
	objConn.Execute(strQuery)
	
	If Err.Number <> 0 Then
		result("result") = 2
		result("error_msg") = "update Error : " & Err.Description
		result.Flush
		Response.End
	End If
	
	strQuery = "delete from tblPress_img where p_num = '" & p_num & "'"
	objConn.Execute(strQuery)
	
	If Err.Number <> 0 Then
		result("result") = 2
		result("error_msg") = "Delete Error : " & Err.Description & " " & strQuery
		result.Flush
		Response.End
	End If
	
	If img <> "" Then
		img_array = Split(img, "|")
		For i = 0 To UBound(img_array)-1
			If img_array(i) <> "" Then
				strQuery = "insert into tblPress_img (p_num, pi_img) values ('"& p_num  &"', '"& img_array(i) &"')"
				objConn.Execute(strQuery)
				
				If Err.Number <> 0 Then
					result("result") = 2
					result("error_msg") = "Insert Error : " & Err.Description
					result.Flush
					Response.End
				End If
			End If
		Next
	End If
	
	result.Flush
	Response.End

ElseIf mode = "del" Then

	error_flag = false

	If Request.Form("select") = "" Then
		result("result") = 2
		result("error_msg") = "the select is empty"
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	selected = Request.Form("select")
	select_array = Split(selected, "|")
	
	For i = 0 To UBound(select_array) - 1
		num = select_array(i)
		// delete 
		strQuery = "delete from tblPress where p_num = '" & num & "'"
		objConn.Execute(strQuery)
		
		If Err.Number <> 0 Then
			result("result") = 2
			result("error_msg") = "Delete Error : " & Err.Description & " " & strQuery
			result.Flush
			Response.End
		End If
		
		strQuery = "delete from tblPress_img where p_num = '" & num & "'"
		objConn.Execute(strQuery)
		
		If Err.Number <> 0 Then
			result("result") = 2
			result("error_msg") = "Delete Error : " & Err.Description & " " & strQuery
			result.Flush
			Response.End
		End If
	Next
	
	result.Flush
	Response.End

End If

%>