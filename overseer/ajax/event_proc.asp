<!-- #include file = "../../inc/_config.asp" -->
<%
' remove cache
Response.Expires = -1
Response.ExpiresAbsolute = now() - 1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!-- #include file = "../admin_check.asp" -->
<%

Dim mode, result
Set result = jsObject()
result("result") = 0
result("error_msg") = ""

if Request.Form("mode") = "" Then
	result("result") = 2
	result("error_msg") = "The mode is not selected"
	result.Flush
	Response.End
End If

mode = Request.Form("mode")

Dim e_num, e_group, e_title, e_text,e_state,e_st_date,e_ed_date,error_flag, selected, select_array, num, img, img_array, e_img
Err.clear
On Error Resume Next

If mode = "add" Then

	'"e_num" : e_num,
	'"e_group" : e_group,
	'"e_state" : e_state,
	'"e_title" : e_title,
	'"e_img" : e_img,
	'"e_text" : e_text,
	'"e_st_date" : e_st_date,
	'"e_ed_date" : e_ed_date,
	'"img" : img

	error_flag = false

	If Request.Form("e_title") = "" Then
		result("result") = 2
		result("error_msg") = "the title is empty | "
		error_flag = true
	End If
	If Request.Form("e_text") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the text is empty | "
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	e_group = Request.Form("e_group")
	e_state = Request.Form("e_state")
	e_title = Request.Form("e_title")
	e_img = Request.Form("e_img")
	e_text = Request.Form("e_text")
	e_st_date = Request.Form("e_st_date")
	e_ed_date = Request.Form("e_ed_date")
	img = Request.Form("img")
	
	// for flag
	dim thisTime, unixEpoch, thisUTime
	thisTime = DateSerial(Year(Now), Month(Now), Day(Now)) + TimeSerial(Hour(Now), Minute(Now), Second(Now))
	unixEpoch = "01/01/1970 00:00:00"
	thisUTime = DateDiff("s", unixEpoch, thisTime)
	
	// insert 
	strQuery = "insert into tblEvent (e_group,e_state, e_title, e_img, e_text,e_st_date,e_ed_date,e_reg_date,e_flag) values ('"& e_group  &"','"& e_state &"','"& e_title &"', '"& e_img &"', '"& e_text &"','"& e_st_date &"','"& e_ed_date &"',getDate(),'"& thisUTime &"')"
	objConn.Execute(strQuery)
	
	If Err.Number <> 0 Then
		result("result") = 2
		result("error_msg") = "Insert Error : " & Err.Description
		result.Flush
		Response.End
	End If
	
	If img <> "" Then
		// get p_num
		strQuery = "select e_num from tblEvent where e_flag = '"& thisUTime &"'"
		set objRs = SendQuery(objConn,strQuery)
		num = objRs("e_num")
		
		img_array = Split(img, "|")
		For i = 0 To UBound(img_array)-1
			If img_array(i) <> "" Then
				strQuery = "insert into tblEvent_img (e_num, ei_img) values ('"& num  &"', '"& img_array(i) &"')"
				objConn.Execute(strQuery)
				
				If Err.Number <> 0 Then
					result("result") = 2
					result("error_msg") = "Insert Error : " & Err.Description
					result.Flush
					Response.End
				End If
			End If
		Next
	End If
	
	result.Flush
	Response.End

ElseIf mode = "edit" Then

	'"e_num" : e_num,
	'"e_group" : e_group,
	'"e_state" : e_state,
	'"e_title" : e_title,
	'"e_img" : e_img,
	'"e_text" : e_text,
	'"e_st_date" : e_st_date,
	'"e_ed_date" : e_ed_date,
	'"img" : img

	error_flag = false
	
	If Request.Form("e_num") = "" Then
		result("result") = 2
		result("error_msg") = "the num is empty | "
		error_flag = true
	End If
	If Request.Form("e_title") = "" Then
		result("result") = 2
		result("error_msg") = "the title is empty | "
		error_flag = true
	End If
	If Request.Form("e_text") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the text is empty | "
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	e_num = Request.Form("e_num")
	e_group = Request.Form("e_group")
	e_state = Request.Form("e_state")
	e_title = Request.Form("e_title")
	e_img = Request.Form("e_img")
	e_text = Request.Form("e_text")
	e_st_date = Request.Form("e_st_date")
	e_ed_date = Request.Form("e_ed_date")
	img = Request.Form("img")
	
	// insert 
	strQuery = "update dbo.tblEvent set e_group = '"& e_group  &"',e_state='"&e_state&"', e_title = '"& e_title &"', e_img = '"& e_img &"', e_text = '"& e_text &"',e_st_date='"&e_st_date&"',e_ed_date='"& e_ed_date &"', e_mod_date = getDate() where e_num = '" & e_num & "'"
	objConn.Execute(strQuery)
	
	If Err.Number <> 0 Then
		result("result") = 2
		result("error_msg") = "update Error : " & Err.Description
		result.Flush
		Response.End
	End If
	
	strQuery = "delete from dbo.tblEvent_img where e_num = '" & e_num & "'"
	objConn.Execute(strQuery)
	
	If Err.Number <> 0 Then
		result("result") = 2
		result("error_msg") = "Delete Error : " & Err.Description & " " & strQuery
		result.Flush
		Response.End
	End If
	
	If img <> "" Then
		img_array = Split(img, "|")
		For i = 0 To UBound(img_array)-1
			If img_array(i) <> "" Then
				strQuery = "insert into tblEvent_img (e_num, ei_img) values ('"& e_num  &"', '"& img_array(i) &"')"
				objConn.Execute(strQuery)
				
				If Err.Number <> 0 Then
					result("result") = 2
					result("error_msg") = "Insert Error : " & Err.Description
					result.Flush
					Response.End
				End If
			End If
		Next
	End If
	
	result.Flush
	Response.End

ElseIf mode = "del" Then

	error_flag = false

	If Request.Form("select") = "" Then
		result("result") = 2
		result("error_msg") = "the select is empty"
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	selected = Request.Form("select")
	select_array = Split(selected, "|")
	
	For i = 0 To UBound(select_array) - 1
		num = select_array(i)
		// delete 
		strQuery = "delete from tblEvent where e_num = '" & num & "'"
		objConn.Execute(strQuery)
		
		If Err.Number <> 0 Then
			result("result") = 2
			result("error_msg") = "Delete Error : " & Err.Description & " " & strQuery
			result.Flush
			Response.End
		End If
		
		strQuery = "delete from tblEvent_img where e_num = '" & num & "'"
		objConn.Execute(strQuery)
		
		If Err.Number <> 0 Then
			result("result") = 2
			result("error_msg") = "Delete Error : " & Err.Description & " " & strQuery
			result.Flush
			Response.End
		End If
	Next
	
	result.Flush
	Response.End

End If

%>