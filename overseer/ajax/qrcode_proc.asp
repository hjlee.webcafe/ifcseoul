<!-- #include file = "../../inc/_config.asp" -->
<%
' remove cache
Response.Expires = -1
Response.ExpiresAbsolute = now() - 1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!-- #include file = "../admin_check.asp" -->
<%

Dim mode, result
Set result = jsObject()
result("result") = 0
result("error_msg") = ""

if Request.Form("mode") = "" Then
    result("result") = 2
    result("error_msg") = "The mode is not selected"
    result.Flush
    Response.End
End If

mode = Request.Form("mode")

Dim q_num, q_title, q_text, error_flag, selected, select_array, num, img, img_array
Err.clear
On Error Resume Next

If mode = "add" Then

    error_flag = false

    If Request.Form("q_title") = "" Then
        result("result") = 2
        result("error_msg") = "the title is empty | "
        error_flag = true
    End If
    If Request.Form("q_text") = "" Then
        result("result") = 2
        result("error_msg") = result("error_msg") & "the text is empty | "
        error_flag = true
    End If

    If error_flag Then
        result.Flush
        Response.End
    End If

    q_title = Request.Form("q_title")
    q_text = Request.Form("q_text")

    // for flag
    dim thisTime, unixEpoch, thisUTime
    thisTime = DateSerial(Year(Now), Month(Now), Day(Now)) + TimeSerial(Hour(Now), Minute(Now), Second(Now))
    unixEpoch = "01/01/1970 00:00:00"
    thisUTime = DateDiff("s", unixEpoch, thisTime)

    // insert
    strQuery = "insert into tblQrcode (q_title, q_text,q_flag) values ('"& q_title  &"', '"& q_text &"', '"& thisUTime &"')"
    objConn.Execute(strQuery)

    If Err.Number <> 0 Then
        result("result") = 2
        result("error_msg") = "Insert Error : " & Err.Description
        result.Flush
        Response.End
    End If

    ' If img <> "" Then
    '     // get q_num
    '     strQuery = "select q_num from tblQrcode where n_flag = '"& thisUTime &"'"
    '     set objRs = SendQuery(objConn,strQuery)
    '     num = objRs("q_num")

    '     img_array = Split(img, "|")
    '     For i = 0 To UBound(img_array)-1
    '         If img_array(i) <> "" Then
    '             strQuery = "insert into tblNotice_img (q_num, ni_img) values ('"& num  &"', '"& img_array(i) &"')"
    '             objConn.Execute(strQuery)

    '             If Err.Number <> 0 Then
    '                 result("result") = 2
    '                 result("error_msg") = "Insert Error : " & Err.Description
    '                 result.Flush
    '                 Response.End
    '             End If
    '         End If
    '     Next
    ' End If

    result.Flush
    Response.End

ElseIf mode = "edit" Then

    '"n_num" : n_num,
    '"n_group" : n_group,
    '"n_title" : n_title,
    '"n_text" : n_text,
    '"img" : img

    error_flag = false

    If Request.Form("q_num") = "" Then
        result("result") = 2
        result("error_msg") = "the num is empty | "
        error_flag = true
    End If
    If Request.Form("q_title") = "" Then
        result("result") = 2
        result("error_msg") = "the title is empty | "
        error_flag = true
    End If
    If Request.Form("q_text") = "" Then
        result("result") = 2
        result("error_msg") = result("error_msg") & "the text is empty | "
        error_flag = true
    End If

    If error_flag Then
        result.Flush
        Response.End
    End If

    q_num = Request.Form("q_num")
    q_title = Request.Form("q_title")
    q_text = Request.Form("q_text")

    // insert
    strQuery = "update tblQrcode set q_title = '"& q_title &"', q_text = '"& q_text &"', q_mod_date = getDate() where q_num = '" & q_num & "'"
    objConn.Execute(strQuery)

    If Err.Number <> 0 Then
        result("result") = 2
        result("error_msg") = "update Error : " & Err.Description
        result.Flush
        Response.End
    End If

    result.Flush
    Response.End

ElseIf mode = "del" Then

    error_flag = false

    If Request.Form("select") = "" Then
        result("result") = 2
        result("error_msg") = "the select is empty"
        error_flag = true
    End If

    If error_flag Then
        result.Flush
        Response.End
    End If

    selected = Request.Form("select")
    select_array = Split(selected, "|")

    For i = 0 To UBound(select_array) - 1
        num = select_array(i)
        // delete
        strQuery = "delete from tblQrcode where q_num = '" & num & "'"
        objConn.Execute(strQuery)

        If Err.Number <> 0 Then
            result("result") = 2
            result("error_msg") = "Delete Error : " & Err.Description & " " & strQuery
            result.Flush
            Response.End
        End If

    Next

    result.Flush
    Response.End

End If

%>
