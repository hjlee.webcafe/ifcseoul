<!-- #include file = "../../inc/_config.asp" -->
<%
' remove cache
Response.Expires = -1
Response.ExpiresAbsolute = now() - 1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!-- #include file = "../admin_check.asp" -->
<%

Dim mode, result
Set result = jsObject()
result("result") = 0
result("error_msg") = ""

if Request.Form("mode") = "" Then
	result("result") = 2
	result("error_msg") = "The mode is not selected"
	result.Flush
	Response.End
End If

mode = Request.Form("mode")

Dim fg_num, fg_title_ko, fg_title_en, fg_img, fg_list_title, error_flag, selected, select_array
Err.clear
On Error Resume Next

If mode = "add" Then

	'"fg_num" : fg_num,
	'"fg_title_ko" : fg_title_ko,
	'"fg_title_en" : fg_title_en,
	'"fg_img" : fg_img

	error_flag = false

	If Request.Form("fg_title_ko") = "" Then
		result("result") = 2
		result("error_msg") = "the title is empty | "
		error_flag = true
	End If
	If Request.Form("fg_img") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the image is empty | "
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	fg_title_ko = Request.Form("fg_title_ko")
	fg_title_en = Request.Form("fg_title_en")
	fg_img = Request.Form("fg_img")
	fg_list_title = Request.Form("fg_list_title")
	
	// insert 
	strQuery = "insert into tblFacGal (fg_title_ko, fg_title_en, fg_list_title, fg_img, fg_reg_date) values ('"& fg_title_ko  &"', '"& fg_title_en &"', '"& fg_list_title &"', '"& fg_img &"', getDate())"
	objConn.Execute(strQuery)
	
	If Err.Number <> 0 Then
		result("result") = 2
		result("error_msg") = "Insert Error : " & Err.Description
		result.Flush
		Response.End
	End If
	
	result.Flush
	Response.End

ElseIf mode = "edit" Then

	'"fg_num" : fg_num,
	'"fg_title_ko" : fg_title_ko,
	'"fg_title_en" : fg_title_en,
	'"fg_img" : fg_img

	error_flag = false

	If Request.Form("fg_title_ko") = "" Then
		result("result") = 2
		result("error_msg") = "the title is empty | "
		error_flag = true
	End If
	If Request.Form("fg_img") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the image is empty | "
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	fg_num = Request.Form("fg_num")
	fg_title_ko = Request.Form("fg_title_ko")
	fg_title_en = Request.Form("fg_title_en")
	fg_img = Request.Form("fg_img")
	fg_list_title = Request.Form("fg_list_title")
	
	// insert 
	strQuery = "update tblFacGal set fg_title_ko = '"& fg_title_ko  &"', fg_title_en = '"& fg_title_en &"', fg_list_title = '"& fg_list_title &"', fg_img = '"& fg_img &"', fg_mod_date = getDate() where fg_num = '" & fg_num & "'"
	objConn.Execute(strQuery)
	
	If Err.Number <> 0 Then
		result("result") = 2
		result("error_msg") = "update Error : " & Err.Description
		result.Flush
		Response.End
	End If
	
	result.Flush
	Response.End

ElseIf mode = "del" Then

	error_flag = false

	If Request.Form("select") = "" Then
		result("result") = 2
		result("error_msg") = "the select is empty"
		error_flag = true
	End If
	
	If error_flag Then
		result.Flush
		Response.End
	End If
	
	selected = Request.Form("select")
	select_array = Split(selected, "|")
	
	For i = 0 To UBound(select_array) - 1
		num = select_array(i)
		// delete 
		strQuery = "delete from tblFacGal where fg_num = '" & num & "'"
		objConn.Execute(strQuery)
		
		If Err.Number <> 0 Then
			result("result") = 2
			result("error_msg") = "Delete Error : " & Err.Description & " " & strQuery
			result.Flush
			Response.End
		End If
		
		strQuery = "delete from tblPress_img where p_num = '" & num & "'"
		objConn.Execute(strQuery)
		
		If Err.Number <> 0 Then
			result("result") = 2
			result("error_msg") = "Delete Error : " & Err.Description & " " & strQuery
			result.Flush
			Response.End
		End If
	Next
	
	result.Flush
	Response.End

End If

%>