
<script type="text/javascript" src="js/qrcode/jquery.qrcode.js"></script>
<script type="text/javascript" src="js/qrcode/qrcode.js"></script>
<style>

    /*navi*/
    .breadcrumb { background-color:#f5f5f5; padding:5px 10px; }

    /* .btn-xs 추가 */
    .btn-group-xs > .btn, .btn-xs {
        padding  : .25rem .4rem;
        font-size  : .875rem;
        line-height  : .5;
        border-radius : .2rem;
    }
    .btn-xs, .btn-group-xs>.btn {
        padding: 1px 5px;
        font-size: 12px;
        line-height: 1.5;
        border-radius: 3px;
        }
    /* .btn-xs 추가 */


    /*테이블 상단*/
    .top_btn_wrap * { font-family:'Nanum Gothic','돋움',Dotum,Helvetica,AppleGothic,sans-serif; font-size:13px; font-weight:bold; }
    .QrDiv canvas { width:20px; height:20px; float: left; margin-right: 10px;}
    .DetailQrDiv canvas { width:200; height:200px; }
    .popupQrcodeImg canvas { margin:0 auto; }

    a[id^=QrDownload_] {color: #ffc107; font-weight: bold;}
    .left_menu ul, .top ul { padding-left:0px; }

</style>
