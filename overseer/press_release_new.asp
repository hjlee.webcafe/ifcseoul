<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->

<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="제품 등록">
		<caption>제품 등록</caption>
		<colgroup>
			<col width="150px" >
			<col>
		</colgroup>
		<tbody>
			<tr>
				<th>
					구분
				</th>
				<td>
					<label><input type="radio" name="p_group" class="p_group" value="0" checked="checked" /> 국문</label>
					<label><input type="radio" name="p_group" class="p_group ml20" value="1" /> 영문</label>
				</td>
			</tr>
			<tr>
				<th>
					제목
				</th>
				<td>
					<input type="text" class="i_text50" name="p_title" id="p_title" maxlength="80" value="" placeholder="제목을 입력 해 주세요." /> (80자 이내)
				</td>
			</tr>
			<!--
			<tr>
				<th>
					요약
				</th>
				<td>
					<input type="text" class="i_text100" name="p_summary" id="p_summary" maxlength="100" value="" placeholder="요약을 입력 해 주세요." />
				</td>
			</tr>
			<tr>
				<th>
					썸네일
				</th>
				<td>
					<input name="p_img" id="p_img" type="text" class="i_text50" value="" />
					<div id="p_img_upload" class="upload_btn_2">Upload</div>
				</td>
			</tr>
			-->
			<tr>
				<th>
					내용
				</th>
				<td>
					<textarea type="text" class="ckeditor" name="p_text" id="p_text" value="" placeholder="내용을 입력 해 주세요"></textarea>
				</td>
			<tr>
				<th>
					이미지
				</th>
				<td>
					<div class="img_section">
						<ul>
						</ul>
						<input type="hidden" class="ckeditor_img" value="p_text_img" />
					</div>
					<div class="insert img">
						<div id="img_upload" class="upload_btn">Upload</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-primary btn-sm" id="add_press" name="add_press" value="등록하기" />&nbsp;
			<input type="button" class="btn btn-warning btn-sm" id="back_to_list" name="back_to_list" value="목록으로" />
		</li>
		<li class="btm_btn_right">
			<input type="hidden" id="list_link_hidden" value="press_release.asp<%=list_link%>"/>
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->