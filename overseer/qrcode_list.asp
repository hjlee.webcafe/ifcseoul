<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header_new.asp" -->
<!--#include file = "qrcode_header.asp" -->
<%

  Dim countQuery, selectQuery, num_count, list_array, page_result, t_page, query_where

  // search string
  If Request.QueryString("search_text") <> "" Then
    query_where = " and " & Request.QueryString("search_type") & " Like '%" & Request.QueryString("search_text") & "%' "
  End If

  // counting list
  countQuery = "select count(*) as count from dbo.tblQrcode where 1 = 1 "& query_where


  set objConn = OpenDBConnection()
  set objRs = SendQuery(objConn,countQuery)
  total_count = objRs("count")
  num_count = total_count - (page - 1) * page_limit
  set objRs = Nothing

  // get list
  selectQuery = "select Top " & page_limit & " * from dbo.tblQrcode where q_num Not In (select Top " & current_count & " q_num from dbo.tblQrcode where 1 = 1 "& query_where &" order by q_num desc) "& query_where & " order by q_num desc"

  set objRs = SendQuery(objConn,selectQuery)

  'page_create(p_total_count, p_page, p_page_limit, p_page_length)
  t_page = page
  page_result = page_create(total_count, t_page, page_limit, page_length)

%>

<div class="top_btn_wrap">
    <ul>
        <li>
            전체 <%=total_count%> 건
        </li>
        <li class="top_btn_right">
            <label>리스트 갯수 &nbsp;</label>
            <select name="page_limit_btn" id="page_limit_btn">
                <option value="15" <%=page_limit_select.Item("10")%>>10</option>
                <option value="20" <%=page_limit_select.Item("20")%>>20</option>
                <option value="30" <%=page_limit_select.Item("30")%>>30</option>
                <option value="50" <%=page_limit_select.Item("50")%>>50</option>
                <option value="100" <%=page_limit_select.Item("100")%>>100</option>
            </select>
        </li>
    </ul>
</div>
<div>
  <table class="table" id="table_list">
    <caption>공지사항</caption>
    <colgroup>
      <col width="5%" >
      <col width="5%">
      <col width="40%">
      <col width="20%">
      <col width="10%">
      <col width="20%">
    </colgroup>
    <thead>
      <tr>
        <th scope="col" class="text-center"><input type="checkbox" id="select_all_list" name="select_all_list" /></th>
        <th scope="col" class="text-center">번호</th>
        <th scope="col">제목</th>
        <th scope="col">QR코드</th>
        <th scope="col" class="text-center">조회수</th>
        <th scope="col" class="text-center">등록일</th>
      </tr>
    </thead>
    <tbody>
      <% if objRs.EOF Then %>
        <tr>
          <td colspan="6" align="center">
            리스트가 없습니다.
          </td>
        </tr>
      <% Else %>
        <% For k = 0 to objRs.RecordCount - 1 %>
        <tr>
          <td class="text-center">
            <input type="checkbox" id="qr_<%=objRs("q_num")%>" name="qr_<%=objRs("q_num")%>" value="<%=objRs("q_num")%>" />
          </td>
          <td scope="row" class="text-center">
            <%=objRs.RecordCount-k%>
          </td>
          <td>
            <a href="qrcode_detail.asp<%=list_link%>&num=<%=objRs("q_num")%>"><%=objRs("q_title")%></a>
          </td>
          <td id="Qrcode_<%=objRs("q_flag")%>">
            <%
              'QR코드 링크로 연결될 링크주소 정의
              Dim QrLinkText
              QrLinkText = "https://ifcseoul.com/qrcode.asp?num=" & objRs("q_num") & "&flag=" & objRs("q_flag")
            %>
            <!-- QR코드 -->
            <div class="QrDiv open-modal" data-bs-toggle="modal" data-bs-target="#popupQrcode"></div>
            <a id="QrDownload_<%=objRs("q_flag")%>" class="btn btn-outline-warning btn-xs" href="" download="qrcode_<%=objRs("q_flag")%>.png">Download</a>
            <a id="QrLink" class="btn btn-link btn-xs" href="<%=QrLinkText%>" target="_blank">Link</a>
            <script>
              // QR코드 이미지 노출
              $("#Qrcode_<%=objRs("q_flag")%> .QrDiv").qrcode({   //qrcode 시작
                render : "canvas",                            //table, canvas 형식 두 종류가 있다.
                width  : 300,                                 //넓이 조절
                height : 300,                                 //높이 조절
                text   : "<%=QrLinkText%>"                    //QR코드에 실릴 문자열
              });
            </script>                
          </td>
          <td class="text-center"><%=objRs("q_check")%></td>
          <td class="text-center"><%=objRs("q_reg_date")%></td>
        </tr>
        <% objRs.MoveNext %>
        <% Next %>
      <% End If %>
    </tbody>
  </table>
</div>
<div class="btm_btn_wrap">
  <ul>
    <li>
      <input type="button" class="btn btn-danger btn-sm" id="del_qrcode" name="del_qrcode" value="삭제하기" />&nbsp;
    </li>
    <li class="btm_btn_right">
      <a href="qrcode_new.asp"><input type="button" class="btn btn-success btn-sm" value="등록하기" /></a>&nbsp;
    </li>
  </ul>
</div>
<div class="page_wrap">
  <!-- #include file = "../view/page_template.asp" -->
</div>
<div class="search_wrap">
  <form name="search" id="search" method="get">
    <select name="search_type" class="form-control input-sm width_100">
      <option value="q_title" <% If Request.QueryString("search_type") = "q_title" Then Response.Write "selected" End If %>>제목</option>
      <option value="q_text" <% If Request.QueryString("search_type") = "q_text" Then Response.Write "selected" End If %>>내용</option>
    </select>
    <input type="text" class="form-control input-sm width_150" name="search_text" value="" placeholder="검색어" />
    <input type="submit" class="btn btn-sm btn-primary ml10" name="search" value="검색" />
    <input type="hidden" id="list_link_hidden" value="<%=list_link%>" />
    <input type="hidden" id="page_limit" name="page_limit" value="<%=page_limit%>" />
  </form>
</div>

<!-- Modal:: QR코드 크게보기 -->
<div class="modal fade" id="popupQrcode" tabindex="-1" aria-labelledby="popupQrcode" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="popupQrcode">QR코드</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="popupQrcodeImg"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
<script>
  //QR코드 모달창으로 보기
  $('.open-modal').click(function(e) {
    e.preventDefault();

    $(".popupQrcodeImg").html("");
    $(".popupQrcodeImg").qrcode({   //qrcode 시작
      render : "canvas",                            // table, canvas 형식 두 종류가 있다.
      width : 300,                                  // 넓이 조절
      height : 300,                                 // 높이 조절
      text   : $('.open-modal').nextAll('#QrLink').attr('href')        // QR코드에 실릴 문자열
    });

    $('#popupQrcode').modal('show');
  });

  //QR코드 다운로드
  $('[id^=Qrcode_]').each( function() {
    var id = $(this).attr('id');
    var flag = id.replace('Qrcode_', '');

    var canvas = document.querySelector('#' + id + ' canvas');
    q = canvas.getContext('2d');
    q.fillStyle = 'hotpink';

    document.querySelector('#QrDownload_' + flag).addEventListener('click', event =>
      event.target.href = canvas.toDataURL()
    );
  });
</script>
<!--#include file = "index_footer.asp" -->
