<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header_new.asp" -->
<!--#include file = "qrcode_header.asp" -->
<%

'게시글 조회
Dim selectQuery, num, check(1), imgRs
num = Request.QueryString("num")
selectQuery = "select * from tblQrcode where q_num = '"& num &"'"
set objRs = SendQuery(objConn,selectQuery)

' 조회수 업데이트
selectQuery = "update tblQrcode set q_check = q_check + 1 where q_num = '" & objRs("q_num") & "'"
objConn.Execute(selectQuery)

%>

<style>
    .ck-editor__editable_inline {
        min-height: 300px;
    }
</style>

<div class="table_list">
    <!-- <table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="QR코드 등록"> -->
        <table class="table" id="table_list">
        <!-- <table class="table" id="table_list"> -->
        <caption>QR코드 등록</caption>
        <colgroup>
            <col width="150px" >
            <col>
        </colgroup>
        <tbody>
            <tr>
                <th>
                    제목
                </th>
                <td>
                    <input type="text" class="form-control-sm" name="q_title" id="q_title" value="<%=objRs("q_title")%>" maxlength="80" placeholder="제목을 입력 해 주세요." /> (80자 이내)
                </td>
                <%
                  ''  Dim QrLinkText
                   '' QrLinkText = "http://182.162.90.223//qrcode.asp?num=" & objRs("q_num") & "&flag=" & objRs("q_flag")
                %>
               <!--  <td id="QrcodeImg_<%=objRs("q_flag")%>" style="float: right;">

                    <div class="DetailQrDiv"></div>
                    <script>
                      // QR코드 이미지 노출
                      $("#QrcodeImg_<%=objRs("q_flag")%> .DetailQrDiv").qrcode({   //qrcode 시작
                            render : "canvas",                            //table, canvas 형식 두 종류가 있다.
                            width : 300,                                  //넓이 조절
                            height : 300,                                  //높이 조절
                            text   : "<%=QrLinkText%>"       //QR코드에 실릴 문자열
                        });
                    </script>
                </td> -->
            </tr>
            <tr>
                <th>
                    QR코드
                </th>
                <td id="QrcodeImg_<%=objRs("q_flag")%>">
                    <%
                       Dim QrLinkText
                       QrLinkText = "https://ifcseoul.com/qrcode.asp?num=" & objRs("q_num") & "&flag=" & objRs("q_flag")
                    %>
                    <div class="DetailQrDiv"></div>
                    <script>
                      // QR코드 이미지 노출
                      $("#QrcodeImg_<%=objRs("q_flag")%> .DetailQrDiv").qrcode({   //qrcode 시작
                            render : "canvas",                            //table, canvas 형식 두 종류가 있다.
                            width : 300,                                  //넓이 조절
                            height : 300,                                  //높이 조절
                            text   : "<%=QrLinkText%>"       //QR코드에 실릴 문자열
                        });
                    </script>
                </td>
            </tr>
            <tr>
                <th>
                    내용
                </th>
                <td>
                    <textarea type="text" class="ckeditorQrcode" name="q_text" id="q_text" placeholder="내용을 입력 해 주세요">
                        <%=objRs("q_text")%>
                    </textarea>
                </td>
        </tbody>
    </table>
</div>
<div class="btm_btn_wrap">
    <ul>
        <li>
            <input type="button" class="btn btn-danger btn-sm" id="del_qrcode" name="del_qrcode" value="삭제하기" />&nbsp;
            <input type="button" class="btn btn-success btn-sm" id="edit_qrcode" name="edit_qrcode" value="수정하기" />&nbsp;
            <input type="button" class="btn btn-warning btn-sm" id="back_to_list" name="back_to_list" value="목록으로" />
        </li>
        <li class="btm_btn_right">
            <input type="hidden" id="list_link_hidden" value="qrcode_list.asp<%=list_link%>"/>
            <input type="hidden" id="q_num" value="<%=num%>" />
        </li>
    </ul>
</div>

<script>
    ClassicEditor.create( document.querySelector( '.ckeditorQrcode' ), {
        toolbar: {
            items: [
                'heading',
                '|',
                'bold',
                'italic',
                'link',
                'bulletedList',
                'numberedList',
                '|',
                'outdent',
                'indent',
                '|',
                'imageUpload',
                'blockQuote',
                'insertTable',
                'mediaEmbed',
                'undo',
                'redo'
            ]
        },
        language: 'en',
        licenseKey: '',
        simpleUpload: {
            uploadUrl: '/ajax/upload_ckeditor5.asp'
        },
        image: {
            styles: [
                'alignLeft', 'alignCenter', 'alignRight'
            ],
            toolbar: [
                'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight', '|', 'resizeImage', '|', 'imageTextAlternative'
            ]
        }
    })
    .then( editor => {
        window.editor = editor;
    })
    .catch( error => {
        console.error( error );
    });

</script>

<!--#include file = "index_footer.asp" -->
