<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->

<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="공지사항 등록">
		<caption>공지사항 등록</caption>
		<colgroup>
			<col width="150px" >
			<col>
		</colgroup>
		<tbody>
			<tr>
				<th>
					구분
				</th>
				<td>
					<label><input type="radio" name="e_group" class="e_group" value="0" checked="checked" /> 국문</label>
					<label><input type="radio" name="e_group" class="e_group ml20" value="1" /> 영문</label>
				</td>
			</tr>
			<tr>
				<th>
					제목
				</th>
				<td>
					<input type="text" class="i_text50" name="e_title" id="e_title" maxlength="80" value="" placeholder="제목을 입력 해 주세요." /> (80자 이내)
				</td>
			</tr>
			<tr>
				<th>
					구분
				</th>
				<td>
					<select id="stat_type" name="stat_type" class="stat_type form-control input-sm width_100">
					<option value="0">Seoul</option>
					<option value="1">Mall</option>
					<option value="2">Conrad</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>
					썸네일
				</th>
				<td>
					<input name="e_img" id="e_img" type="text" class="i_text50" value="" />
					<div id="e_img_upload" class="upload_btn_2">Upload</div>
				</td>
			</tr>
			<tr>
				<th>
					기간
				</th>
				<td>
				<input type="text" id="start_date" value="" placeholder="날짜를 입력해주세요." readonly />~<input type="text" id="end_date" value="" placeholder="날짜를 입력해주세요." readonly />
				</td>
			</tr>
			<tr>
				<th>
					상태
				</th>	
				<td>
					대기
				</td>		
			</tr>
			<tr>
				<th>
					내용
				</th>
				<td>
					<textarea type="text" class="ckeditor" name="e_text" id="e_text" value="" placeholder="내용을 입력 해 주세요."></textarea>
				</td>
			<tr>
				<th>
					이미지
				</th>
				<td>
					<div class="img_section">
						<ul>
						</ul>
						<input type="hidden" class="ckeditor_img" value="e_text_img" />
					</div>
					<div class="insert img">
						<div id="img_upload" class="upload_btn">Upload</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-primary btn-sm" id="add_event" name="add_event" value="등록하기" />&nbsp;
			<input type="button" class="btn btn-warning btn-sm" id="back_to_list" name="back_to_list" value="목록으로" />
		</li>
		<li class="btm_btn_right">
			<input type="hidden" id="list_link_hidden" value="site_event.asp<%=list_link%>"/>
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->