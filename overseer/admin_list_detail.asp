<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%

Dim selectQuery, num
num = Request.QueryString("num")
selectQuery = "select * from tblManager where intSeq = '"& num &"'"

set objRs = SendQuery(objConn,selectQuery)

%>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="관리자 등록">
		<caption>관리자등록</caption>
		<colgroup>
			<col width="180px" >
			<col>
		</colgroup>
		<tbody>
			<tr>
				<th>
					관리자 이름
				</th>
				<td>
					<input name="as_name" id="as_name" type="text" class="width_150" maxlength="50" value="<%=objRs("strNAme")%>" />
				</td>
			</tr>
			<tr>
				<th>
					관리자 아이디
				</th>
				<td>
					<%=objRs("strID")%>
				</td>
			</tr>
			<tr>
				<th>
					관리자 비밀번호
				</th>
				<td>
					<input name="as_pw" id="as_pw" type="password" class="width_150" value="" />
				</td>
			</tr>
			<tr>
				<th>
					관리자 비밀번호 확인
				</th>
				<td>
					<input name="as_pw_chk" id="as_pw_chk" type="password" class="width_150" value="" />
				</td>
			</tr>
			<tr>
				<th>
					관리자 연락처
				</th>
				<td>
					<input name="as_tel" id="as_tel" type="text" class="width_150" maxlength="12" value="<%=objRs("strTel")%>" />
				</td>
			</tr>
			<tr>
				<th>
					관리자 이메일
				</th>
				<td>
					<input name="as_email" id="as_email" type="text" class="i_text50" value="<%=objRs("strEmail")%>" />
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-danger btn-sm" id="del_admin" name="del_admin" value="삭제하기" />&nbsp;
			<input type="button" class="btn btn-primary btn-sm" id="edit_admin" name="edit_admin" value="수정하기" />&nbsp;
			<input type="button" class="btn btn-warning btn-sm" id="back_to_list" name="back_to_list" value="목록으로" />
		</li>
		<li class="btm_btn_right">
			<input type="hidden" id="list_link_hidden" value="admin_list.asp<%=list_link%>" />
			<input type="hidden" id="as_num" value="<%=num%>" />
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->