<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%

Dim countQuery, selectQuery, num_count, list_array, page_result, t_page, query_where ,group_array(2), lang_array(1)
Dim state_array(),state,ny,nm,nd,n_alldate,y,m,d,alldate,ey,em,ed,e_alldate,objCo
group_array(0)="Seoul"
group_array(1)="Mall"
group_array(2)="Conrad"

lang_array(0)="국문"
lang_array(1)="영문"

ny = FormatDateTime(now(),2)
ny = left(ny,4)
nm = FormatDateTime(now(),2)
nm = mid(nm,6,2)
nd = FormatDateTime(now(),2)
nd = mid(nd,9,2)

n_alldate = ny&nm&nd

query_where = "order by e_num desc"

// search string
If Request.QueryString("search_text") <> "" Then
	query_where = " and " & Request.QueryString("search_type") & " Like '%" & Request.QueryString("search_text") & "%' "
End If

// counting list
countQuery = "select count(*) as count from dbo.tblEvent where 1 = 1 "& query_where

set objConn = OpenDBConnection()
set objRs = SendQuery(objConn,countQuery)
total_count = objRs("count")
num_count = total_count - (page - 1) * page_limit
set objRs = Nothing

// get list
selectQuery = "select Top " & page_limit & " * from dbo.tblEvent where e_num Not In (select Top " & current_count & " e_num from tblEvent where 1 = 1 "& query_where &") "& query_where
set objRs = SendQuery(objConn,selectQuery)

// get Y-m-s list
selectQuery = "select Top " & page_limit & " * from dbo.tblEvent where e_num Not In (select Top " & current_count & " e_num from tblEvent where 1 = 1 "& query_where &") "& query_where
set objCo = SendQuery(objConn,selectQuery)

if objCo.RecordCount >=0 then
	
ReDim state_array(objCo.RecordCount - 1)
	
	For k = 0 to objCo.RecordCount - 1 
	
	y=objCo("e_st_date")
	y=left(y,4)
	m=objCo("e_st_date")
	m=mid(m,6,2)
	d=objCo("e_st_date")
	d=mid(d,9,2)
	
	alldate=y&m&d
	
	ey=objCo("e_ed_date")
	ey=left(ey,4)
	em=objCo("e_ed_date")
	em=mid(em,6,2)
	ed=objCo("e_ed_date")
	ed=mid(ed,9,2)
	
	e_alldate=ey&em&ed
	
	'n_alldate : 현재날짜, alldate : 시작일 , e_alldate : 종료일
	
		if(n_alldate < alldate) then
			state_array(k) = "예정"
		end if
		
		if(n_alldate >= alldate and n_alldate <= e_alldate) then
			state_array(k) = "진행중"		
		end if 
		
		if(n_alldate > e_alldate) then
			state_array(k) = "종료일"
		end if
	
	objCo.MoveNext
 	Next
end if

'page_create(p_total_count, p_page, p_page_limit, p_page_length)
t_page = page
page_result = page_create(total_count, t_page, page_limit, page_length)
%>

<div class="top_btn_wrap">
	<ul>
		<li>
			전체 <%=objRs.RecordCount%> 건
		</li>
		<li class="top_btn_right">
			<label>리스트 갯수</label>
			<select name="page_limit_btn" id="page_limit_btn">
				<option value="15" <%=page_limit_select.Item("15")%>>
					15
				</option>
				<option value="20" <%=page_limit_select.Item("20")%>>
					20
				</option>
				<option value="30" <%=page_limit_select.Item("30")%>>
					30
				</option>
				<option value="50" <%=page_limit_select.Item("50")%>>
					50
				</option>
				<option value="100" <%=page_limit_select.Item("100")%>>
					100
				</option>
			</select>
		</li>
	</ul>
</div>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="이벤트">
		<caption>이벤트</caption>
		<colgroup>
			<col width="5%" >
			<col width="5%">
			<col>
			<col>
			<col>
			<col>
			<col>
			<col>
			<col>
		</colgroup>
		<thead>
			<tr>
				<th>
					<input type="checkbox" id="select_all_list" name="select_all_list" />&nbsp;선택
				</th>
				<th>
					번호
				</th>
				<th>
					언어
				</th>
				<th>
					구분
				</th>
				<th>
					제목
				</th>
				<th>
					기간
				</th>
				<th>
					상태
				</th>
				<th>
					조회수
				</th>
				<th>
					등록일
				</th>
			</tr>
		</thead>
		<tbody>
			<% if objRs.EOF Then %>
			<tr>
				<td colspan="9" align="center">
					리스트가 없습니다.
				</td>
			</tr>
			<% Else %>
				<% For k = 0 to objRs.RecordCount - 1 %>
			<tr>
				<td class="text-center">
					<input type="checkbox" id="se_<%=objRs("e_num")%>" name="se_<%=objRs("e_num")%>" value="<%=objRs("e_num")%>" />
				</td>
				<td class="text-center">
					<%=objRs.RecordCount-k%>
				</td>
				<td class="text-center">
					<%=lang_array(objRs("e_group"))%>
				</td>
				<td class="text-center">
					<%=group_array(objRs("e_state"))%>
				</td>
				<td>
					<a href="site_event_detail.asp<%=list_link%>&num=<%=objRs("e_num")%>&state=<%=state_array(k)%>"><%=objRs("e_title")%></a>
				</td>
				<td>
					<%=objRs("e_st_date")%>~<%=objRs("e_ed_date")%>
				</td>
				<td>
					<%=state_array(k)%>
				</td>
				<td>
					<%=objRs("e_count")%>
				</td>
				<td>
					<%=objRs("e_reg_date")%>
				</td>
			</tr>
				<% objRs.MoveNext %>
				<% Next %>
			<% End If %>
			
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-danger btn-sm" id="del_event" name="del_event" value="삭제하기" />&nbsp;
		</li>
		<li class="btm_btn_right">
			<a href="site_event_new.asp"><input type="button" class="btn btn-primary btn-sm" value="등록하기" /></a>&nbsp;
		</li>
	</ul>
</div>
<div class="page_wrap">
	<!-- #include file = "../view/page_template.asp" -->
</div>
<div class="search_wrap">
	<form name="search" id="search" method="get">
		<select name="search_type" class="form-control input-sm width_100">
			<option value="e_title" <% If Request.QueryString("search_type") = "e_title" Then Response.Write "selected" End If %>>제목</option>
			<option value="e_text" <% If Request.QueryString("search_type") = "e_text" Then Response.Write "selected" End If %>>내용</option>
		</select>
		<input type="text" class="form-control input-sm width_150" name="search_text" value="" placeholder="검색어" />
		<input type="submit" class="btn btn-sm btn-primary ml10" name="search" value="검색" />
		<input type="hidden" id="list_link_hidden" value="<%=list_link%>" />
		<input type="hidden" id="page_limit" name="page_limit" value="<%=page_limit%>" />
	</form>
</div>
<!--#include file = "index_footer.asp" -->