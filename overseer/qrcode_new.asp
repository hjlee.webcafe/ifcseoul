<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header_new.asp" -->
<!--#include file = "qrcode_header.asp" -->

<style>
    .ck-editor__editable_inline {
        min-height: 300px;
    }
</style>

<div class="table_list">
    <!-- <table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="QR코드 등록"> -->
        <table class="table" id="table_list">
        <caption>QR코드 등록</caption>
        <colgroup>
            <col width="150px" >
            <col>
        </colgroup>
        <tbody>
            <tr>
                <th>
                    제목
                </th>
                <td>
                    <input type="text" class="form-control-sm i_text50" name="q_title" id="q_title" value="" maxlength="80" placeholder="제목을 입력 해 주세요." /> (80자 이내)
                </td>
            </tr>
            <tr>
                <th>
                    내용
                </th>
                <td>
                    <textarea type="text" class="ckeditor5 ckeditor-qrcode" name="q_text" id="q_text" value="" placeholder="내용을 입력 해 주세요"></textarea>
                </td>
        </tbody>
    </table>
</div>
<div class="btm_btn_wrap">
    <ul>
        <li>
            <input type="button" class="btn btn-success btn-sm" id="add_qrcode" name="add_qrcode" value="등록하기" />&nbsp;
            <input type="button" class="btn btn-warning btn-sm" id="back_to_list" name="back_to_list" value="목록으로" />
        </li>
        <li class="btm_btn_right">
            <input type="hidden" id="list_link_hidden" value="qrcode_list.asp<%=list_link%>"/>
        </li>
    </ul>
</div>
<script>
    
    ClassicEditor.create( document.querySelector( '.ckeditor-qrcode' ), {
        toolbar: {
            items: [
                'heading',
                '|',
                'bold',
                'italic',
                'link',
                'bulletedList',
                'numberedList',
                '|',
                'outdent',
                'indent',
                '|',
                'imageUpload',
                'blockQuote',
                'insertTable',
                'mediaEmbed',
                'undo',
                'redo'
            ]
        },
        language: 'en',
        licenseKey: '',
        simpleUpload: {
            uploadUrl: '/ajax/upload_ckeditor5.asp'
        },
        image: {
            styles: [
                'alignLeft', 'alignCenter', 'alignRight'
            ],
            toolbar: [
                'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight', '|', 'resizeImage', '|', 'imageTextAlternative'
            ]
        }
    } )
    .then( editor => {
        window.editor = editor;
    } )
    .catch( error => {
        console.error( error );
    } );

</script>

<!--#include file = "index_footer.asp" -->
