<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%

Dim countQuery, selectQuery, num_count, list_array, page_result, t_page, query_where

// search string
If Request.QueryString("search_text") <> "" Then
	query_where = " and " & Request.QueryString("search_type") & " Like '%" & Request.QueryString("search_text") & "%' "
End If

// counting list
countQuery = "select count(*) as count from dbo.tblManager where 1 = 1 "& query_where

set objConn = OpenDBConnection()
set objRs = SendQuery(objConn,countQuery)
total_count = objRs("count")
num_count = total_count - (page - 1) * page_limit
set objRs = Nothing

// get list
selectQuery = "select Top " & page_limit & " * from dbo.tblManager where intSeq Not In (select Top " & current_count & " intSeq from dbo.tblManager where 1 = 1 "& query_where &") "& query_where & " order by intSeq desc "

set objRs = SendQuery(objConn,selectQuery)

'page_create(p_total_count, p_page, p_page_limit, p_page_length)
t_page = page
page_result = page_create(total_count, t_page, page_limit, page_length)
%>
<div class="top_btn_wrap">
	<ul>
		<li>
			전체 <%=total_count%> 건
		</li>
		<li class="top_btn_right">
			<label>리스트 갯수</label>
			<select name="page_limit_btn" id="page_limit_btn">
				<option value="15" <%=page_limit_select.Item("15")%>>
					15
				</option>
				<option value="20" <%=page_limit_select.Item("20")%>>
					20
				</option>
				<option value="30" <%=page_limit_select.Item("30")%>>
					30
				</option>
				<option value="50" <%=page_limit_select.Item("50")%>>
					50
				</option>
				<option value="100" <%=page_limit_select.Item("100")%>>
					100
				</option>
			</select>
		</li>
	</ul>
</div>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="Admin List">
		<caption>관리자목록</caption>
		<colgroup>
			<col width="5%" >
			<col width="5%">
			<col>
			<col>
			<col>
			<col>
			<col>
		</colgroup>
		<thead>
			<tr>
				<th>
					번호
				</th>
				<th>
					이름
				</th>
				<th>
					아이디
				</th>
				<th>
					연락처
				</th>
				<th>
					등록일
				</th>
				<th>
					최종접속일
				</th>
			</tr>
		</thead>
		<tbody>
			<% if objRs.EOF Then %>
			<tr>
				<td colspan="6" align="center">
					리스트가 없습니다.
				</td>
			</tr>
			<% Else %>
				<% For k = 0 to objRs.RecordCount - 1 %>
			<tr>
				<td class="text-center">
					<%=num_count-k%>
				</td>
				<td>
					<%=objRs("strName")%>
				</td>
				<td>
					<a href="admin_list_detail.asp<%=list_link%>&num=<%=objRs("intSeq")%>"><%=objRs("strID")%></a>
				</td>
				<td>
					<%=objRs("strTel")%>
				</td>
				<td>
					<%=objRs("regDate")%>
				</td>
				<td>
					<%=objRs("lastLoginDate")%>
				</td>
			</tr>
				<% objRs.MoveNext %>
				<% Next %>
			<% End If %>
			
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<!-- img src="img/btn_delete_off.gif" class="pointer" id="del_admin" name="del_admin" alt="delete" / -->&nbsp;
		</li>
		<li class="btm_btn_right">
			<a href="admin_list_new.asp"><input type="button" class="btn btn-primary btn-sm" value="등록하기" /></a>&nbsp;
		</li>
	</ul>
</div>
<div class="page_wrap">
	<!-- #include file = "../view/page_template.asp" -->
</div>
<div class="search_wrap">
	<form name="search" id="search" method="get">
		<select name="search_type" class="form-control input-sm width_100">
			<option value="strName" <% If Request.QueryString("search_type") = "strName" Then Response.Write "selected" End If %>>이름</option>
			<option value="strID" <% If Request.QueryString("search_type") = "strID" Then Response.Write "selected" End If %>>아이디</option>
		</select>
		<input type="text" class="form-control input-sm width_150" name="search_text" value="" placeholder="검색어" />
		<input type="submit" class="btn btn-sm btn-primary ml10" name="search" value="검색" />
		<input type="hidden" id="list_link_hidden" value="<%=list_link%>" />
		<input type="hidden" id="page_limit" name="page_limit" value="<%=page_limit%>" />
	</form>
</div>

<!--#include file = "index_footer.asp" -->