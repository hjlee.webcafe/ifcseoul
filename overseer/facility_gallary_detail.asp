<!-- #include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%

Dim selectQuery, num, fg_img_hide, fg_img_name, img_name_array
num = Request.QueryString("num")
selectQuery = "select * from tblFacGal where fg_num = '"& num &"'"

set objRs = SendQuery(objConn,selectQuery)

fg_img_hide = "hide_row"

If objRs("fg_img") <> "" Then
	fg_img_hide = ""
	img_name_array = Split(objRs("fg_img"), "/")
	fg_img_name = img_name_array(UBound(img_name_array))
End If

%>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="관리자 등록">
		<caption>갤러리등록</caption>
		<colgroup>
			<col width="150px" >
			<col>
		</colgroup>
		<tbody>
			<tr>
				<th>
					타이틀(국문)
				</th>
				<td>
					<input name="fg_title_ko" id="fg_title_ko" type="text" class="width_250" maxlength="12" value="<%=objRs("fg_title_ko")%>" /> (12자 이내)
				</td>
			</tr>
			<tr>
				<th>
					타이틀(영문)
				</th>
				<td>
					<input name="fg_title_en" id="fg_title_en" type="text" class="width_250" maxlength="15" value="<%=objRs("fg_title_en")%>" /> (15자 이내)
				</td>
			</tr>
			<tr>
				<th>
					리스트 제목
				</th>
				<td>
					<input name="fg_list_title" id="fg_list_title" type="text" class="width_250" maxlength="15" value="<%=objRs("fg_list_title")%>" /> (15자 이내)
				</td>
			</tr>
			<tr>
				<th>
					등록일
				</th>
				<td>
					<%=objRs("fg_reg_date")%>
				</td>
			</tr>
			<tr>
				<th>
					첨부파일
				</th>
				<td>
					<input name="fg_img" id="fg_img" type="text" class="i_text50 ii_img" value="<%=objRs("fg_img")%>" />
					<div id="fg_img_upload" class="upload_btn_2">Upload</div>
					<br />
					<div class="current_image_wrap <%=fg_img_hide%>">
						<span id="fg_img_name"><%=fg_img_name%></span>
						<input type="hidden" id="fg_img_path" name="fg_imgpath" value="<%=objRs("fg_img")%>" />
						<input type="button" class="btn btn-sm btn-danger delete_btn" id="fg_img_del" name="fg_img_del" value="삭제" />
					</div>
					<br />
					<img src="<%=objRs("fg_img")%>" id="fg_img_preview" height="200px" />
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-danger btn-sm" id="del_fg" name="del_fg" value="삭제하기" />&nbsp;
			<input type="button" class="btn btn-primary btn-sm" id="edit_fg" name="edit_fg" value="수정하기" />&nbsp;
			<input type="button" class="btn btn-warning btn-sm" id="back_to_list" name="back_to_list" value="목록으로" />
		</li>
		<li class="btm_btn_right">
			<input type="hidden" id="list_link_hidden" value="facility_gallary.asp<%=list_link%>"/>
			<input type="hidden" id="fg_num" value="<%=num%>" />
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->