$(function() {
	// check for CSS3 3D transformations and WebGL
	if (ggHasHtml5Css3D() || ggHasWebGL()) {
	// use HTML5 panorama

		// create the panorama player with the container
		pano=new pano2vrPlayer("pano");
		// add the skin object
		skin=new pano2vrSkin(pano);
		// load the configuration
		pano.readConfigUrl("/js/outside_panorama_crop_out.xml");
		// hide the URL bar on the iPhone
		//hideUrlBar();
		
		$("#flashContent").remove();
	} 
	else if (swfobject.hasFlashPlayerVersion("9.0.0")) {
		$("#pano").remove();
		var flashvars = {};
		var params = {};
		params.quality = "high";
		params.bgcolor = "#ffffff";
		params.allowscriptaccess = "sameDomain";
		params.allowfullscreen = "true";
		var attributes = {};
		attributes.id = "pano";
		attributes.name = "pano";
		attributes.align = "middle";
		swfobject.embedSWF(
			"/js/outside_panorama_crop_out.swf", "flashContent", 
			"1024", "254",
			"9.0.0", "", 
			flashvars, params, attributes);
		
	}
	
})

$(window).load(function() {
	$(window).resize(function() {
		if ($("object#pano").length > 0)
		{
			var w_width = $(document).width();
			$("object#pano").attr("width", w_width);
		}
	})
})