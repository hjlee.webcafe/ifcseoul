$(function() {
	$(".jsbtnLyp").click(function() {
		if ($('#popFade').length < 1) {
            $('body').append('<div id="popFade"></div>')
        }
		
		$("#popFade").unbind("click").click(function() {
			$(this).hide();
			$(".lyPopWrap").hide();
		});
	})
	
	$(document).on("click", "#hospital_tab", function() {
		if ($("#testMap").length > 0 && $("#hospital_map").html().length == 0)
		{
			if ($("#testMap").length > 0)
			{
				goverment_popup2();
			}
		}
		else if ($("#google_map2").html().length == 0)
		{
			initialize_hospital();
		}
	});
	
	$(".list_p").click(function(event) {
		event.preventDefault();
		var id = $(this).attr("href");
		$(".hospital_list").addClass("hide");
		$(id).removeClass("hide");
		
		$(".list_p").each(function() {
			if ($(this).attr("href") == id)
				$(this).html("<strong>" + $(this).text() + "</strong>");
			else
				$(this).text($(this).text());
		});
	});
})

$(window).load(function() {
	if ($("#testMap").length > 0)
	{
		goverment_popup();
	}
	else
	{
		loadScript();
	}
})

function goverment_popup()
{
	var oPoint = new naver.maps.LatLng(37.5254000, 126.9256750);	
	// naver.maps.setDefaultPoint('LatLng');
	oMap = new naver.maps.Map('testMap' ,{
	                        point : oPoint,
	                        zoom : 9,
	                        //enableWheelZoom : true,
	                        enableDragPan : true,
	                        enableDblClickZoom : false,
	                        mapMode : 0,
	                        activateTrafficMap : false,
	                        activateBicycleMap : false,
	                        minMaxLevel : [ 1, 14 ],
	                        size : new naver.maps.Size(592, 510)
	                });

	// zooControl
	oMap.setOptions({
        zoomControl: true,
        zoomControlOptions: {
            position: naver.maps.Position.TOP_LEFT
        }
    });
    
	
	// var oSize = new naver.maps.Size(28, 37);
    // var oOffset = new naver.maps.Size(14, 37);
    // var oIcon = new naver.maps.Icon('http://static.naver.com/maps2/icons/pin_spot2.png', oSize, oOffset);

    // var mapInfoTestWindow = new naver.maps.InfoWindow(); // - info window 생성
    //mapInfoTestWindow.setVisible(false); // - infowindow 표시 여부 지정.
    //oMap.addOverlay(mapInfoTestWindow);     // - 지도에 추가.     
	
    //var oLabel = new naver.maps.MarkerLabel(); // - 마커 라벨 선언.
    //oMap.addOverlay(oLabel); // - 마커 라벨 지도에 추가. 기본은 라벨이 보이지 않는 상태로 추가됨.

    // mapInfoTestWindow.attach('changeVisible', function(oCustomEvent) {
        // if (oCustomEvent.visible) {
        //     oLabel.setVisible(false);
        // }
    // });
    // naver.maps.Event.addListener(mapInfoTestWindow, 'changeVisible', function(oCustomEvent) { 
    // if (oCustomEvent.visible) {
        //     oLabel.setVisible(false);
        // }
    // });

    var markers = new Array();
    
    markers.push(["IFC Seoul", new naver.maps.LatLng(37.5254000, 126.9256750)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(0)").text(), new naver.maps.LatLng(37.5320453, 126.9141499)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(1)").text(), new naver.maps.LatLng(37.5312547, 126.9171584)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(2)").text(), new naver.maps.LatLng(37.5264360, 126.8960040)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(3)").text(), new naver.maps.LatLng(37.5224735, 126.9263277)]);
    
    var data_array = new Array();

	for (var i = 0; i < markers.length; i++)
	{
		// var oMarker = new naver.maps.Marker(oIcon, { title : markers[i][0] });
		var markerOptions = {
			position: markers[i][1],
			title : markers[i][0],
			icon: {
				url: 'http://static.naver.com/maps2/icons/pin_spot2.png',
				size: new naver.maps.Size(28,37),
				anchor:  new naver.maps.Size(14, 37)
			}
		};
		var oMarker = new naver.maps.Marker(markerOptions);

		// oMarker.setPoint(markers[i][1]);
		// oMap.addOverlay(oMarker);
		oMarker.setMap(oMap);
		data_array[markers[i][0]] = "layerPop0" + i;
	}
	
	// oMap.attach('mouseenter', function(oCustomEvent) {	
 	naver.maps.Event.addListener(oMap, 'mouseenter', function(oCustomEvent) {
        var oTarget = oCustomEvent.target;
        // 마커위에 마우스 올라간거면
        if (oTarget instanceof naver.maps.Marker) {
            var oMarker = oTarget;
            oLabel.setVisible(true, oMarker); // - 특정 마커를 지정하여 해당 마커의 title을 보여준다.
        }
    });

    // oMap.attach('mouseleave', function(oCustomEvent) {
        naver.maps.Event.addListener(oMap, 'mouseleave', function(oCustomEvent) {
        var oTarget = oCustomEvent.target;
        // 마커위에서 마우스 나간거면
        if (oTarget instanceof naver.maps.Marker) {
        	oLabel.setVisible(false);
        }
    });
    

    // oMap.attach('click', function(oCustomEvent) {
    naver.maps.Event.addListener(oMap, 'click', function(oCustomEvent) {
        var oPoint = oCustomEvent.point;
        var oTarget = oCustomEvent.target;
        mapInfoTestWindow.setVisible(false);
        // 마커 클릭하면
        if (oTarget instanceof naver.maps.Marker) {
        	if (oCustomEvent.clickCoveredMarker)
        		return;
        
            var mId = oTarget.getTitle();
			var winWidth = window.innerWidth;
	       	var popID = data_array[mId];
	       	
	       	if (data_array[mId] == "layerPop00")
	       		return;
		   	
		   	var width = 300;
	       	var $popObj = $('#' + popID);
            $('body').append($popObj);

            $popObj.show().css({ 'width': Number(width) }); //Number(width)

            var popMargTop = ($popObj.height()) / 2;
            var popMargLeft = ($popObj.width()) / 2;

            $popObj.css({
                'margin-top': -popMargTop,
                'margin-left': -popMargLeft
            });

            var $wrap = $('body');

            if ($wrap.find('#popFade').length < 1) {
                $wrap.append('<div id="popFade"></div>')
            }
            $('#popFade').show();
            $('html').addClass('hidden');
            
            $("#popFade").unbind("click").click(function() {
				$(this).hide();
				$(".lyPopWrap").hide();
			});
            
			return;
        }
    });
    
    oMap.setSize(new naver.maps.Size($("#map_wrap").width(), $("#map_wrap").height())); 
    
    $(window).resize(function() {
		oMap.setSize(new naver.maps.Size($("#map_wrap").width(), $("#map_wrap").height())); 
    });
    
    $(".publicOffice > .placePosWrap > li > a").click(function() {
	    var $obj = $(this);
        var popID = $obj.attr('rel');
        
        for (var index in data_array)
        {
	        if (data_array[index] == popID)
	        {
	        	for (var i = 0; i < markers.length; i++)
	        	{
		        	if (markers[i][0] == index)
		        	{
			        	oMap.setCenterAndLevel(markers[i][1], 10, { useEffect : true });
		        	}
	        	}
	        }
        }
    });
}

function goverment_popup2()
{
	var oPoint = new naver.maps.LatLng(37.5254000, 126.9256750);
	// naver.maps.setDefaultPoint('LatLng');
	oMap = new naver.maps.Map('hospital_map' ,{
	                        point : oPoint,
	                        zoom : 11,
	                        //enableWheelZoom : true,
	                        enableDragPan : true,
	                        enableDblClickZoom : false,
	                        mapMode : 0,
	                        activateTrafficMap : false,
	                        activateBicycleMap : false,
	                        minMaxLevel : [ 1, 14 ],
	                        size : new naver.maps.Size(592, 510)
	                });
	// var oSlider = new naver.maps.ZoomControl();
	// oMap.addControl(oSlider);
	// oSlider.setPosition({
	//         top : 10,
	//         left : 10
	// });
	// oSlider.setMap(zoomControl);
	
	// zooControl
	oMap.setOptions({
        zoomControl: true,
        zoomControlOptions: {
            position: naver.maps.Position.TOP_LEFT
        }
    });

	
	// var oSize = new naver.maps.Size(28, 37);
 //    var oOffset = new naver.maps.Size(14, 37);
 //    var oIcon = new naver.maps.Icon('http://static.naver.com/maps2/icons/pin_spot2.png', oSize, oOffset);
    
    // var mapInfoTestWindow = new naver.maps.InfoWindow(); // - info window 생성
    // mapInfoTestWindow.setVisible(false); // - infowindow 표시 여부 지정.
    // oMap.addOverlay(mapInfoTestWindow);     // - 지도에 추가.     
    
    // var oLabel = new naver.maps.MarkerLabel(); // - 마커 라벨 선언.
    // oMap.addOverlay(oLabel); // - 마커 라벨 지도에 추가. 기본은 라벨이 보이지 않는 상태로 추가됨.

    // mapInfoTestWindow.attach('changeVisible', function(oCustomEvent) {
    //     if (oCustomEvent.visible) {
    //         oLabel.setVisible(false);
    //     }
    // });

    var markers = new Array();
    markers.push(["IFC Seoul", new naver.maps.LatLng(37.5254000, 126.9256750)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(0)").text(), new naver.maps.LatLng(37.5204147, 126.9239888)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(1)").text(), new naver.maps.LatLng(37.5216583, 126.9248750)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(2)").text(), new naver.maps.LatLng(37.5214658, 126.9250847)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(3)").text(), new naver.maps.LatLng(37.5227260, 126.9239910)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(4)").text(), new naver.maps.LatLng(37.5189394, 126.9307527)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(5)").text(), new naver.maps.LatLng(37.5226445, 126.9249565)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(6)").text(), new naver.maps.LatLng(37.5242623, 126.9248899)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(7)").text(), new naver.maps.LatLng(37.5238233, 126.9241529)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(8)").text(), new naver.maps.LatLng(37.5225150, 126.9246490)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(9)").text(), new naver.maps.LatLng(37.5204924, 126.9239892)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(10)").text(), new naver.maps.LatLng(37.5204924, 126.9239892)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(11)").text(), new naver.maps.LatLng(37.5214695, 126.9227668)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(12)").text(), new naver.maps.LatLng(37.5236234, 126.9254903)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(13)").text(), new naver.maps.LatLng(37.5183253, 126.9367517)]);
    
    
    var data_array = new Array();

	for (var i = 0; i < markers.length; i++)
	{
		// var oMarker = new naver.maps.Marker(oIcon, { title : markers[i][0] });
		var markerOptions = {
			position: markers[i][1],
			title : markers[i][0],
			icon: {
				url: 'http://static.naver.com/maps2/icons/pin_spot2.png',
				size: new naver.maps.Size(28,37),
				anchor:  new naver.maps.Size(14, 37)
			}
		};
		var oMarker = new naver.maps.Marker(markerOptions);

		// oMarker.setPoint(markers[i][1]);
		// oMap.addOverlay(oMarker);
		oMarker.setMap(oMap);

		var new_num = i + 4;
		if (new_num == 4)
			new_num = 0;

		if (new_num < 10)
			data_array[markers[i][0]] = "layerPop0" + new_num;
		else
			data_array[markers[i][0]] = "layerPop" + new_num;
	}
	
	// oMap.attach('mouseenter', function(oCustomEvent) {
	naver.maps.Event.addListener(oMap, 'mouseenter', function(oCustomEvent) {
        var oTarget = oCustomEvent.target;
        // 마커위에 마우스 올라간거면
        if (oTarget instanceof naver.maps.Marker) {
            var oMarker = oTarget;
            oLabel.setVisible(true, oMarker); // - 특정 마커를 지정하여 해당 마커의 title을 보여준다.
        }
    });

    // oMap.attach('mouseleave', function(oCustomEvent) {
    naver.maps.Event.addListener(oMap, 'mouseleave', function(oCustomEvent) {
        var oTarget = oCustomEvent.target;
        // 마커위에서 마우스 나간거면
        if (oTarget instanceof naver.maps.Marker) {
        	oLabel.setVisible(false);
        }
    });
    
    // oMap.attach('click', function(oCustomEvent) {
    naver.maps.Event.addListener(oMap, 'click', function(oCustomEvent) {
        var oPoint = oCustomEvent.point;
        var oTarget = oCustomEvent.target;
        mapInfoTestWindow.setVisible(false);
        // 마커 클릭하면
        if (oTarget instanceof naver.maps.Marker) {
        	if (oCustomEvent.clickCoveredMarker)
        		return;
        
            var mId = oTarget.getTitle();
			var winWidth = window.innerWidth;
	       	var popID = data_array[mId];

	       	if (data_array[mId] == "layerPop00")
	       		return;
	       		
		   	var width = 300;
	       	var $popObj = $('#' + popID);
            $('body').append($popObj);

            $popObj.show().css({ 'width': Number(width) }); //Number(width)

            var popMargTop = ($popObj.height()) / 2;
            var popMargLeft = ($popObj.width()) / 2;

            $popObj.css({
                'margin-top': -popMargTop,
                'margin-left': -popMargLeft
            });

            var $wrap = $('body');

            if ($wrap.find('#popFade').length < 1) {
                $wrap.append('<div id="popFade"></div>')
            }
            $('#popFade').show();
            $('html').addClass('hidden');
            
            $("#popFade").unbind("click").click(function() {
				$(this).hide();
				$(".lyPopWrap").hide();
			});
            
			return;
        }
    });
    
    oMap.setSize(new naver.maps.Size($("#map_wrap2").width(), $("#map_wrap2").height())); 
    
    $(window).resize(function() {
		oMap.setSize(new naver.maps.Size($("#map_wrap2").width(), $("#map_wrap2").height())); 
    });
    
    $(".hospital > .placePosWrap > li > a").click(function() {
	    var $obj = $(this);
        var popID = $obj.attr('rel');
        
        for (var index in data_array)
        {
	        if (data_array[index] == popID)
	        {
	        	for (var i = 0; i < markers.length; i++)
	        	{
		        	if (markers[i][0] == index)
		        	{
			        	oMap.setCenterAndLevel(markers[i][1], 11, { useEffect : true });
		        	}
	        	}
	        }
        }
    });
}


// for google map
function loadScript() {
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = "http://maps.googleapis.com/maps/api/js?sensor=true&callback=initialize";
	document.body.appendChild(script);
}

function initialize() {
	geocoder = new google.maps.Geocoder();
	var mapOptions = {
			zoom: 12,
			center: new google.maps.LatLng(37.5254000, 126.9256750),
			mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	map = new google.maps.Map(document.getElementById("google_map"), mapOptions);
	
	$("#google_map").css("width", $("#map_wrap").width() + "px");
	google.maps.event.trigger(map, "resize");
	
	$(window).resize(function() {
		$("#google_map").css("width", $("#map_wrap").width() + "px");
		google.maps.event.trigger(map, "resize");
    });
	
	set_marker();
}

function set_marker()
{
	var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';
	
	var shadow = {
      anchor: new google.maps.Point(15,33),
      url: iconURLPrefix + 'msmarker.shadow.png'
    };
	
	var infowindow = new google.maps.InfoWindow({
		maxWidth: 150
    });
    
    var markers = new Array();
    
    markers.push(["IFC Seoul", new google.maps.LatLng(37.5254000, 126.9256750)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(0)").text(), new google.maps.LatLng(37.5320453, 126.9141499)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(1)").text(), new google.maps.LatLng(37.5312547, 126.9171584)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(2)").text(), new google.maps.LatLng(37.5264360, 126.8960040)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(3)").text(), new google.maps.LatLng(37.5224735, 126.9263277)]);
    
    var data_array = new Array();

	for (var i = 0; i < markers.length; i++)
	{
		data_array[markers[i][0]] = "layerPop0" + i;
	}
    
    var mark;
    var marks = new Array();
    
    for (var i = 0; i < markers.length; i++) {  
      mark = new google.maps.Marker({
        position: markers[i][1],
        map: map,
        shadow: shadow,
        animation: google.maps.Animation.DROP
        //icon: icon_img
      });

      marks.push(mark);

      google.maps.event.addListener(mark, 'click', (function(mark, i) {
        return function() {
        	infowindow.setContent("<div class='infowindow'>" + markers[i][0] + "</div>");
			infowindow.open(map, mark);
			
			$(".infowindow").click(function() {
			    open_popup($(this).text(), data_array);
		    }).css("cursor", "pointer");
        }
      })(mark, i));
	}
	
	var bounds = new google.maps.LatLngBounds();
	
    //  Go through each...
    $.each(marks, function (index, marker) {
        bounds.extend(marker.position);
    });

    //  Fit these bounds to the map
    map.fitBounds(bounds);
    
    var listener = google.maps.event.addListener(map, "idle", function() {
		if (map.getZoom() > 16)
	    	map.setZoom(16);
	    google.maps.event.removeListener(listener);
    });
    
    $(".publicOffice > .placePosWrap > li > a").click(function() {
	    var $obj = $(this);
        var popID = $obj.attr('rel');
        
        for (var index in data_array)
        {
	        if (data_array[index] == popID)
	        {
	        	for (var i = 0; i < markers.length; i++)
	        	{
		        	if (markers[i][0] == index)
		        	{
			        	map.setZoom(15);
			        	map.setCenter(markers[i][1]);
		        	}
	        	}
	        }
        }
    });
}

function loadScript_hospital() {
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = "http://maps.googleapis.com/maps/api/js?sensor=true&callback=initialize_hospital";
	document.body.appendChild(script);
}

function initialize_hospital() {
	geocoder = new google.maps.Geocoder();
	var mapOptions = {
			zoom: 12,
			center: new google.maps.LatLng(37.489834,127.033989),
			mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	map = new google.maps.Map(document.getElementById("google_map2"), mapOptions);
	
	$("#google_map2").css("width", $("#map_wrap2").width() + "px");
	google.maps.event.trigger(map, "resize");
	
	$(window).resize(function() {
		$("#google_map2").css("width", $("#map_wrap2").width() + "px");
		google.maps.event.trigger(map, "resize");
    });
	
	set_marker_hospital();
}

function set_marker_hospital()
{
	var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';
	
	var shadow = {
      anchor: new google.maps.Point(15,33),
      url: iconURLPrefix + 'msmarker.shadow.png'
    };
	
	var infowindow = new google.maps.InfoWindow({
		maxWidth: 300,
		maxHeight: 200
    });
    
    var markers = new Array();
    
    markers.push(["IFC Seoul", new google.maps.LatLng(37.5254000, 126.9256750)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(0)").text(), new google.maps.LatLng(37.5204147, 126.9239888)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(1)").text(), new google.maps.LatLng(37.5216583, 126.9248750)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(2)").text(), new google.maps.LatLng(37.5214658, 126.9250847)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(3)").text(), new google.maps.LatLng(37.5227260, 126.9239910)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(4)").text(), new google.maps.LatLng(37.5189394, 126.9307527)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(5)").text(), new google.maps.LatLng(37.5226445, 126.9249565)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(6)").text(), new google.maps.LatLng(37.5242623, 126.9248899)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(7)").text(), new google.maps.LatLng(37.5238233, 126.9241529)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(8)").text(), new google.maps.LatLng(37.5225150, 126.9246490)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(9)").text(), new google.maps.LatLng(37.5204924, 126.9239892)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(10)").text(), new google.maps.LatLng(37.5204924, 126.9239892)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(11)").text(), new google.maps.LatLng(37.5214695, 126.9227668)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(12)").text(), new google.maps.LatLng(37.5236234, 126.9254903)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(13)").text(), new google.maps.LatLng(37.5183253, 126.9367517)]);
    
    var data_array = new Array();

	for (var i = 0; i < markers.length; i++)
	{
		var new_num = i + 4;
		if (new_num == 4)
			new_num = 0;

		if (new_num < 10)
			data_array[markers[i][0]] = "layerPop0" + new_num;
		else
			data_array[markers[i][0]] = "layerPop" + new_num;
	}
    
    var mark;
    var marks = new Array();
    
    for (var i = 0; i < markers.length; i++) {  
      mark = new google.maps.Marker({
        position: markers[i][1],
        map: map,
        shadow: shadow,
        animation: google.maps.Animation.DROP
        //icon: icon_img
      });

      marks.push(mark);

      google.maps.event.addListener(mark, 'click', (function(mark, i) {
        return function() {
        	infowindow.setContent("<div class='infowindow'>" + markers[i][0] + "</div>");
			infowindow.open(map, mark);
			
			$(".infowindow").click(function() {
			    open_popup($(this).text(), data_array);
		    }).css("cursor", "pointer");
        }
      })(mark, i));
	}
	
	var bounds = new google.maps.LatLngBounds();
	
    //  Go through each...
    $.each(marks, function (index, marker) {
        bounds.extend(marker.position);
    });

    //  Fit these bounds to the map
    map.fitBounds(bounds);
    
    var listener = google.maps.event.addListener(map, "idle", function() {
		if (map.getZoom() > 16)
	    	map.setZoom(16);
	    google.maps.event.removeListener(listener);
    });
    
    $(".hospital > .placePosWrap > li > a").click(function() {
	    var $obj = $(this);
        var popID = $obj.attr('rel');
        
        for (var index in data_array)
        {
	        if (data_array[index] == popID)
	        {
	        	for (var i = 0; i < markers.length; i++)
	        	{
		        	if (markers[i][0] == index)
		        	{
			        	map.setZoom(16);
			        	map.setCenter(markers[i][1]);
		        	}
	        	}
	        }
        }
    });
}

function open_popup(title, data_array)
{
	var mId = title;
	var winWidth = window.innerWidth;
   	var popID = data_array[mId];
   	
   	if (data_array[mId] == "layerPop00")
   		return;
   	
   	var width = 300;
   	var $popObj = $('#' + popID);
    $('body').append($popObj);

    $popObj.show().css({ 'width': Number(width) }); //Number(width)

    var popMargTop = ($popObj.height()) / 2;
    var popMargLeft = ($popObj.width()) / 2;

    $popObj.css({
        'margin-top': -popMargTop,
        'margin-left': -popMargLeft
    });

    var $wrap = $('body');

    if ($wrap.find('#popFade').length < 1) {
        $wrap.append('<div id="popFade"></div>')
    }
    $('#popFade').show();
    $('html').addClass('hidden');
    
    $("#popFade").unbind("click").click(function() {
		$(this).hide();
		$(".lyPopWrap").hide();
	});
    
	return;
}