$(function() {
	$(".jsbtnLyp").click(function() {
		if ($('#popFade').length < 1) {
            $('body').append('<div id="popFade"></div>')
        }

		$("#popFade").unbind("click").click(function() {
			$(this).hide();
			$(".lyPopWrap").hide();
		});
	})

    // 병원 탭 클릭시
	$(document).on("click", "#hospital_tab", function() {
        $("#surrounding_info").show();
        if ($("#hospital_map").html().length == 0) {
            goverment_popup2();
        } else if ($("#google_map2").html().length == 0) {
            initialize_hospital();
        }
	});

    // 관공서 탭 클릭시
    $(document).on("click", "#publicoffice_tab", function() {
        $("#surrounding_info").show();
        if ($("#testMap").length > 0) {
            goverment_popup();
        }
    });

    //교통상황 탭 클릭시
    $(document).on("click", "#traffic_tab", function() {
        $("#surrounding_info").hide();
    });

	$(".list_p").click(function(event) {
		event.preventDefault();
		var id = $(this).attr("href");
		$(".hospital_list").addClass("hide");
		$(id).removeClass("hide");

		$(".list_p").each(function() {
			if ($(this).attr("href") == id)
				$(this).html("<strong>" + $(this).text() + "</strong>");
			else
				$(this).text($(this).text());
		});
	});
})

$(window).load(function() {
	if ($("#trafficMap").length > 0)
	{
        trafficMapSetting();
        $("#surrounding_info").hide();
	}
	else
	{
		loadScript();
	}
})

function trafficMapSetting() {

    // IFC몰 좌표설정
    var oIfcmallPoint = new naver.maps.LatLng( 37.5254000, 126.9256750 ),
    oMap = new naver.maps.Map('trafficMap', {
        center: oIfcmallPoint.destinationPoint(0, 500),
        zoom: 16,                                        // 줌설정 : 1~14, 수치가 클수록 지도 확대(줌인), 이 옵션 생략시 기본값 9
        // 교통상황표시
        /* mapTypes: new naver.maps.MapTypeRegistry({
            'normal': naver.maps.NaverStyleMapTypeOption.getNormalMap(
              {
                overlayType: 'bg.ol.ts.ctt.lko'
              }
            )
        }),*/
        zoomControl : true,                              // 줌 컨트롤 표시, 지정하지 않으면 false 가 기본값
        zoomControlOptions : {
            position : naver.maps.Position.TOP_RIGHT     // 오른쪽 위로 설정
        },
        mapTypeControl : true                         // 일반ㆍ위성 지도보기 컨트롤 표시, 지정하지 않으면 false 가 기본값
    }),
    marker = new naver.maps.Marker({        // 마커표시
        map: oMap,
        position: oIfcmallPoint
    });

    // IFC몰 중앙으로 위치하도록 지도위치 셋팅
    oMap.setCenter(oIfcmallPoint);

    // 교통상황 레이어
    var trafficLayer = new naver.maps.TrafficLayer({
         interval: 300000 // 5분마다 새로고침 (최소값 5분)
    });
    trafficLayer.setMap(oMap);

    // 교통상황 버튼 클릭시 교통상황 노출되도록
    var btn = $('#btnTraffic');
    btn.on("click", function(e) {
        e.preventDefault();
        if (trafficLayer.getMap()) {
            $(this).removeClass("control-on");
            trafficLayer.setMap(null);
        } else {
            $(this).addClass("control-on");
            trafficLayer.setMap(oMap);

            // 사고통제 마커표시
            // var acc_markers = new Array();
            // acc_markers.push(["TRAFFIC", new naver.maps.LatLng(37.5245592, 126.9235842)]);
            // acc_markers.push(["TRAFFIC1", new naver.maps.LatLng(447315.7396240234, 191633.8998031616)]);

            // for (var i = 0; i < acc_markers.length; i++)
            // {
            //     var markerOptions = {
            //         position: acc_markers[i][1],
            //         title : acc_markers[i][0],
            //         icon: {
            //             url: 'https://navermaps.github.io/maps.js/docs/img/example/sally.png',
            //             size: new naver.maps.Size(28,37),
            //             anchor:  new naver.maps.Size(14, 37)
            //         }
            //     };
            //     var oTrafficMarker = new naver.maps.Marker(markerOptions);
            //     oTrafficMarker.setMap(oMap);
            // }
            //-- 사고통제 마커표시
        }
    });

    var iPage = 0;                          // 돌발리스트 페이지수
    var sPagingHtml = "";                   // 페이징Html

    printTrafficInfo(0);                    // 기본 돌발3건 노출

    // 돌발리스트 페이징처리
    iPage = Math.ceil( getTotalCnt("516c7a764c63656e383479704f554a", "/xml/AccInfo/1") / 3);
    if (iPage==0) {
        $("#trafficInfo").append('<strong>돌발상황 및 통제정보가 없습니다. </strong>');
    } else {
        for(var i=1; i<iPage; i++) {
           sPagingHtml = '<a href="#" ';
           if (i==1) sPagingHtml += 'class="selected"';
           sPagingHtml += ">"+ i + "</a>";
           $("#trafficDiv .paginate").append(sPagingHtml);
        }
    }

    $("#trafficDiv .paginate a").on("click", function(e) {
        printTrafficInfo($(this).text() - 1);
        $("#trafficDiv .paginate a").removeClass();
        $(this).addClass("selected");
    });



    oMap.setSize(new naver.maps.Size($("#map_wrap0").width(), $("#map_wrap0").height()));

    $(window).resize(function() {
        oMap.setSize(new naver.maps.Size($("#map_wrap0").width(), $("#map_wrap0").height()));
    });
}

/**
 * 서울시 실시간 돌발 정보 출력
 * @param int iPage 페이지정보
 * /xml/AccInfo/1/5  => /형식/AccInfo/StartIndex/EndIndex
 *
 * Api Url : http://openapi.seoul.go.kr:8088/516c7a764c63656e383479704f554a/xml/AccInfo/1/5/
 * Ref Url : http://data.seoul.go.kr/dataList/OA-13315/A/1/datasetView.do
 */
function printTrafficInfo(iPage) {

    // 서울시 OPEN Api 인증키
    var AuthKey = '516c7a764c63656e383479704f554a';

    // 페이지번호에 따른 Start/End 인덱스 정의
    var iStartIndex, iEndIndex;
    for (i=0; i<Math.ceil(iPage)+1; i++) {
        iStartIndex = i*3+1;
        iEndIndex = iStartIndex + 2;
    }

    var aAccMainType = new Array();             // 돌발 유형
    var aAccSubType = new Array();              // 돌발 세부유형
 //   aAccMainType = getAccTypeArray(AuthKey, 'main');
 //   aAccSubType  = getAccTypeArray(AuthKey, 'sub');

    // Api Url
    //var sUrl = "http://openapi.seoul.go.kr:8088/" + AuthKey + "/xml/AccInfo/"+ iStartIndex + "/" + iEndIndex;
    var sUrl = "/demo/xml_parshing_test.asp?type=accinfo&start=" + iStartIndex + "&end=" + iEndIndex;
    $.ajax({
        crossOrigin: true,
        url: sUrl,                        //ajax로 ajax_xml.xml파일을 불러온다.
        cache: false,                     //사용자캐시를 사용할 것인가.
        dataType: "json",                  //서버로부터 받을 것으로 예상되는 데이터 타입.
        success: function(data){          //ajax요청을 통해 반환되는 데이터 data.

            $("#trafficInfo").html("");
            $.each(data, function(idx) {
                var sHtml = "<table class='trafficTable tblType noticeList1'>";
                sHtml += "<tr><td class='txtL'>기간</td><td class='txtL'>" + this.acc_datetime + "</td></tr>";
                sHtml += "<tr><td class='txtL'>돌발유형</td><td class='txtL'>" + this.acc_type + "</td></tr>";
                sHtml += "<tr><td class='txtL'>도로</td><td class='txtL'>" + this.acc_linkinfo + "</td></tr>";
                sHtml += "<tr><td class='txtL'>내용</td><td class='txtL'>" + this.acc_info + "</td></tr></table>"
                $("#trafficInfo").append(sHtml);
            });

           // $(data).find("row").each(function(){

                //var occr_datetime = $(this).find("occr_date").text() + $(this).find("occr_time").text();
                //var exp_clr_datetime = $(this).find("exp_clr_date").text() + $(this).find("exp_clr_time").text();

               //var sHtml = "<table class='trafficTable tblType noticeList1'>";
                //sHtml += "<tr><td class='txtL'>기간</td><td class='txtL'>" + occr_datetime.substring(0,4) + '-' + occr_datetime.substring(4,6) + '-' + occr_datetime.substring(6,8) + ' ' + occr_datetime.substring(8,10) + ":" + occr_datetime.substring(10,12) + ":" + occr_datetime.substring(12,14) + " ~ ";
                //sHtml += exp_clr_datetime.substring(0,4) + '-' + exp_clr_datetime.substring(4,6) + '-' + exp_clr_datetime.substring(6,8) + ' ' + exp_clr_datetime.substring(8,10) + ":" + exp_clr_datetime.substring(10,12) + ":" + exp_clr_datetime.substring(12,14)  + "</td></tr>";
                // sHtml += "<tr><td class='txtL'>돌발유형</td><td class='txtL'>" + aAccMainType[$(this).find("acc_type").text()] + "(" + aAccSubType[$(this).find("acc_dtype").text()] + ")</td></tr>";
                // sHtml += "<tr><td class='txtL'>도로</td><td class='txtL'>" + getLinkInfo(AuthKey, $(this).find("link_id").text()) + "</td></tr>"
                //sHtml += "<tr><td class='txtL'>내용</td><td class='txtL'>" + $(this).find("acc_info").text() + "</td></tr></table>"
                    // sHtml = $(this).find("acc_info").text() + "</br>";
                //$("#trafficInfo").append(sHtml);
           // });
        }
    });
}

/**
 * 서울시 실시간 돌발정보 카운트
 * 돌발 및 통제 건수 확인
 * API : http://openapi.seoul.go.kr:8088/(인증키)/xml/AccInfo/1/5/
 * 관련링크 : http://data.seoul.go.kr/dataList/OA-13315/A/1/datasetView.do
​ */
function getTotalCnt(key, url) {

    var iTotalCnt = 0;
    $.ajax({
        // url: "http://openapi.seoul.go.kr:8088/" + key  + url + "/1",
        url: "/demo/xml_parshing_test.asp?type=accinfo_t",
        cache: false,                     //사용자캐시를 사용할 것인가.
        async: false,
        timeout : 200,
        dataType: "json",                  //서버로부터 받을 것으로 예상되는 데이터 타입.
        success: function(data) {

            iTotalCnt = parseInt(data.total_cnt);
             if (url == '/xml/AccMainCode/1') {
                return iTotalCnt;
            }
        },
        error: function() {
            console.log("서울시 실시간 돌발정보 카운트를 가져올 수 없습니다. ");
        }
    });

    return iTotalCnt;
}

/**
 * 돌발유형
 */
function getAccTypeArray(key, sAccType) {

    var aResult = new Array();
    var sUrl;

    if (sAccType == "main") {
        sUrl = " http://openapi.seoul.go.kr:8088/" + key +  "/xml/AccMainCode/1/" + getTotalCnt(key, "/xml/AccMainCode/1");
    } else if (sAccType == 'sub') {
        sUrl = " http://openapi.seoul.go.kr:8088/" + key +  "/xml/AccSubCode/1/" + getTotalCnt(key, "/xml/AccSubCode/1");
    }

    $.ajax({
        crossOrigin: true,
        url: sUrl,
        cache: false,                     //사용자캐시를 사용할 것인가.
        async: false,
        timeout : 200,
        dataType: "xml",                  //서버로부터 받을 것으로 예상되는 데이터 타입.
        success: function(data) {
            // iTotalCnt = parseInt($(data).find('list_total_count').te
            $(data).find("row").each(function(){
                // console.log($(this).find('acc_type').text());
                if (sAccType == 'main') {
                    aResult[$(this).find('acc_type').text()] = $(this).find('acc_type_nm').text();
                } else if (sAccType == 'sub') {
                    aResult[$(this).find('acc_dtype').text()] = $(this).find('acc_dtype_nm').text();
                }
            });
        }
    });
    return aResult;
}

/**
 * 돌발 링크정보
 */
function getLinkInfo(key, link_id) {
    var sResult = ''
    var sUrl;

    sUrl = " http://openapi.seoul.go.kr:8088/" + key +  "/xml/LinkInfo/1/5/" + link_id
    $.ajax({
        crossOrigin: true,
        url: sUrl,
        cache: false,                     //사용자캐시를 사용할 것인가.
        async: false,
        timeout : 200,
        dataType: "xml",                  //서버로부터 받을 것으로 예상되는 데이터 타입.
        success: function(data) {
            $(data).find("row").each(function(){
                sResult = $(this).find('road_name').text() + "(" + $(this).find('st_node_nm').text() + "->" + $(this).find('ed_node_nm').text() + ")";
            });
        }
    });
    return sResult;
}

function goverment_popup()
{
	var oPoint = new naver.maps.LatLng(37.5254000, 126.9256750);
	oMap = new naver.maps.Map('testMap' ,{
        //useStyleMap : true,
        point : oPoint,
        zoom : 15,
        enableWheelZoom : true,
        enableDragPan : true,
        enableDblClickZoom : false,
        mapMode : 0,
        activateTrafficMap : false,
        activateBicycleMap : false,
        minMaxLevel : [ 1, 14 ],
        size : new naver.maps.Size(592, 510)
    });

	// IFC몰 중앙으로 위치하도록 지도위치 셋팅
	oMap.setCenter(oPoint);

	// zooControl
	oMap.setOptions({
        zoomControl: true,
        zoomControlOptions: {
            position: naver.maps.Position.TOP_LEFT
        }
    });

    // var mapInfoTestWindow = new naver.maps.InfoWindow(); // - info window 생성
    // mapInfoTestWindow.setVisible(true); // - infowindow 표시 여부 지정.
    // oMap.addOverlay(mapInfoTestWindow);     // - 지도에 추가.

    // var oLabel = new naver.maps.MarkerLabel(); // - 마커 라벨 선언.
    // oMap.addOverlay(oLabel); // - 마커 라벨 지도에 추가. 기본은 라벨이 보이지 않는 상태로 추가됨.

    // mapInfoTestWindow.attach('changeVisible', function(oCustomEvent) {
    //     if (oCustomEvent.visible) {
    //         oLabel.setVisible(false);
    //     }
    // });
    // naver.maps.Event.addListener(mapInfoTestWindow, 'changeVisible', function(oCustomEvent) {
    // if (oCustomEvent.visible) {
    //         oLabel.setVisible(false);
    //     }
    // });

    var markers = new Array();

    markers.push(["IFC Seoul", new naver.maps.LatLng(37.5254000, 126.9256750)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(0)").text(), new naver.maps.LatLng(37.5320453, 126.9141499)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(1)").text(), new naver.maps.LatLng(37.5312547, 126.9171584)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(2)").text(), new naver.maps.LatLng(37.5264360, 126.8960040)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(3)").text(), new naver.maps.LatLng(37.5224735, 126.9263277)]);

    var data_array = new Array();

	for (var i = 0; i < markers.length; i++)
	{
		var markerOptions = {
			position: markers[i][1],
			title : markers[i][0],
			icon: {
				url: 'http://static.naver.com/maps2/icons/pin_spot2.png',
				size: new naver.maps.Size(28,37),
				anchor:  new naver.maps.Size(14, 37)
			}
		};
		var oMarker = new naver.maps.Marker(markerOptions);
		oMarker.setMap(oMap);

		data_array[markers[i][0]] = "layerPop0" + i;
	}

	naver.maps.Event.addListener(oMap, 'mouseenter', function(oCustomEvent) {
        var oTarget = oCustomEvent.target;
        // 마커위에 마우스 올라간거면
        if (oTarget instanceof naver.maps.Marker) {
            var oMarker = oTarget;
            oLabel.setVisible(true, oMarker); // - 특정 마커를 지정하여 해당 마커의 title을 보여준다.
        }
    });

    naver.maps.Event.addListener(oMap, 'mouseleave', function(oCustomEvent) {
        var oTarget = oCustomEvent.target;
        // 마커위에서 마우스 나간거면
        if (oTarget instanceof naver.maps.Marker) {
        	oLabel.setVisible(false);
        }
    });

    naver.maps.Event.addListener(oMap, 'click', function(oCustomEvent) {
        var oPoint = oCustomEvent.point;
        var oTarget = oCustomEvent.target;
        mapInfoTestWindow.setVisible(false);
        // 마커 클릭하면
        if (oTarget instanceof naver.maps.Marker) {
        	if (oCustomEvent.clickCoveredMarker)
        		return;

            var mId = oTarget.getTitle();
			var winWidth = window.innerWidth;
	       	var popID = data_array[mId];

	       	if (data_array[mId] == "layerPop00")
	       		return;

		   	var width = 300;
	       	var $popObj = $('#' + popID);
            $('body').append($popObj);

            $popObj.show().css({ 'width': Number(width) }); //Number(width)

            var popMargTop = ($popObj.height()) / 2;
            var popMargLeft = ($popObj.width()) / 2;

            $popObj.css({
                'margin-top': -popMargTop,
                'margin-left': -popMargLeft
            });

            var $wrap = $('body');

            if ($wrap.find('#popFade').length < 1) {
                $wrap.append('<div id="popFade"></div>')
            }
            $('#popFade').show();
            $('html').addClass('hidden');

            $("#popFade").unbind("click").click(function() {
				$(this).hide();
				$(".lyPopWrap").hide();
			});

			return;
        }
    });

    oMap.setSize(new naver.maps.Size($("#map_wrap1").width(), $("#map_wrap1").height()));

    $(window).resize(function() {
		oMap.setSize(new naver.maps.Size($("#map_wrap1").width(), $("#map_wrap1").height()));
    });

    $(".publicOffice > .placePosWrap > li > a").click(function() {
	    var $obj = $(this);
        var popID = $obj.attr('rel');

        for (var index in data_array)
        {
	        if (data_array[index] == popID)
	        {
	        	for (var i = 0; i < markers.length; i++)
	        	{
		        	if (markers[i][0] == index)
		        	{
			        	oMap.setCenterAndLevel(markers[i][1], 10, { useEffect : true });
		        	}
	        	}
	        }
        }
    });
}

function goverment_popup2()
{
	var oPoint = new naver.maps.LatLng(37.5254000, 126.9256750);
	// naver.maps.setDefaultPoint('LatLng');

	oMap = new naver.maps.Map('hospital_map' ,{
	                        point : oPoint,
	                        zoom : 15,
	                        //enableWheelZoom : true,
	                        enableDragPan : true,
	                        enableDblClickZoom : false,
	                        mapMode : 0,
	                        activateTrafficMap : false,
	                        activateBicycleMap : false,
	                        minMaxLevel : [ 1, 14 ],
	                        size : new naver.maps.Size(592, 510)
	                });

	// move center
	oMap.setCenter(oPoint);

	// zooControl
	oMap.setOptions({
        zoomControl: true,
        zoomControlOptions: {
            position: naver.maps.Position.TOP_LEFT
        }
    });

    // var mapInfoTestWindow = new naver.maps.InfoWindow(); // - info window 생성
    // mapInfoTestWindow.setVisible(false); // - infowindow 표시 여부 지정.
    // oMap.addOverlay(mapInfoTestWindow);     // - 지도에 추가.

    // var oLabel = new naver.maps.MarkerLabel(); // - 마커 라벨 선언.
    // oMap.addOverlay(oLabel); // - 마커 라벨 지도에 추가. 기본은 라벨이 보이지 않는 상태로 추가됨.

    // mapInfoTestWindow.attach('changeVisible', function(oCustomEvent) {
    //     if (oCustomEvent.visible) {
    //         oLabel.setVisible(false);
    //     }
    // });

    var markers = new Array();
    markers.push(["IFC Seoul", new naver.maps.LatLng(37.5254000, 126.9256750)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(0)").text(), new naver.maps.LatLng(37.5204147, 126.9239888)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(1)").text(), new naver.maps.LatLng(37.5216583, 126.9248750)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(2)").text(), new naver.maps.LatLng(37.5214658, 126.9250847)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(3)").text(), new naver.maps.LatLng(37.5227260, 126.9239910)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(4)").text(), new naver.maps.LatLng(37.5189394, 126.9307527)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(5)").text(), new naver.maps.LatLng(37.5226445, 126.9249565)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(6)").text(), new naver.maps.LatLng(37.5242623, 126.9248899)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(7)").text(), new naver.maps.LatLng(37.5238233, 126.9241529)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(8)").text(), new naver.maps.LatLng(37.5225150, 126.9246490)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(9)").text(), new naver.maps.LatLng(37.5204924, 126.9239892)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(10)").text(), new naver.maps.LatLng(37.5204924, 126.9239892)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(11)").text(), new naver.maps.LatLng(37.5214695, 126.9227668)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(12)").text(), new naver.maps.LatLng(37.5236234, 126.9254903)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(13)").text(), new naver.maps.LatLng(37.5183253, 126.9367517)]);


    var data_array = new Array();

	for (var i = 0; i < markers.length; i++)
	{
		var markerOptions = {
			position: markers[i][1],
			title : markers[i][0],
			icon: {
				url: 'http://static.naver.com/maps2/icons/pin_spot2.png',
				size: new naver.maps.Size(28,37),
				anchor:  new naver.maps.Size(14, 37)
			}
		};
		var oMarker = new naver.maps.Marker(markerOptions);
		oMarker.setMap(oMap);

		var new_num = i + 4;
		if (new_num == 4)
			new_num = 0;

		if (new_num < 10)
			data_array[markers[i][0]] = "layerPop0" + new_num;
		else
			data_array[markers[i][0]] = "layerPop" + new_num;
	}

	naver.maps.Event.addListener(oMap, 'mouseenter', function(oCustomEvent) {
        var oTarget = oCustomEvent.target;
        // 마커위에 마우스 올라간거면
        if (oTarget instanceof naver.maps.Marker) {
            var oMarker = oTarget;
            oLabel.setVisible(true, oMarker); // - 특정 마커를 지정하여 해당 마커의 title을 보여준다.
        }
    });

    naver.maps.Event.addListener(oMap, 'mouseleave', function(oCustomEvent) {
        var oTarget = oCustomEvent.target;
        // 마커위에서 마우스 나간거면
        if (oTarget instanceof naver.maps.Marker) {
        	oLabel.setVisible(false);
        }
    });

    naver.maps.Event.addListener(oMap, 'click', function(oCustomEvent) {
        var oPoint = oCustomEvent.point;
        var oTarget = oCustomEvent.target;
        mapInfoTestWindow.setVisible(false);
        // 마커 클릭하면
        if (oTarget instanceof naver.maps.Marker) {
        	if (oCustomEvent.clickCoveredMarker)
        		return;

            var mId = oTarget.getTitle();
			var winWidth = window.innerWidth;
	       	var popID = data_array[mId];

	       	if (data_array[mId] == "layerPop00")
	       		return;

		   	var width = 300;
	       	var $popObj = $('#' + popID);
            $('body').append($popObj);

            $popObj.show().css({ 'width': Number(width) }); //Number(width)

            var popMargTop = ($popObj.height()) / 2;
            var popMargLeft = ($popObj.width()) / 2;

            $popObj.css({
                'margin-top': -popMargTop,
                'margin-left': -popMargLeft
            });

            var $wrap = $('body');

            if ($wrap.find('#popFade').length < 1) {
                $wrap.append('<div id="popFade"></div>')
            }
            $('#popFade').show();
            $('html').addClass('hidden');

            $("#popFade").unbind("click").click(function() {
				$(this).hide();
				$(".lyPopWrap").hide();
			});

			return;
        }
    });

    oMap.setSize(new naver.maps.Size($("#map_wrap2").width(), $("#map_wrap2").height()));

    $(window).resize(function() {
		oMap.setSize(new naver.maps.Size($("#map_wrap2").width(), $("#map_wrap2").height()));
    });

    $(".hospital > .placePosWrap > li > a").click(function() {
	    var $obj = $(this);
        var popID = $obj.attr('rel');

        for (var index in data_array)
        {
	        if (data_array[index] == popID)
	        {
	        	for (var i = 0; i < markers.length; i++)
	        	{
		        	if (markers[i][0] == index)
		        	{
			        	oMap.setCenterAndLevel(markers[i][1], 11, { useEffect : true });
		        	}
	        	}
	        }
        }
    });
}


// for google map
function loadScript() {
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyAS183iaU1s89qCTyh9CSlTn3YXxvoRD7Y&sensor=true&callback=initialize";
	document.body.appendChild(script);
}

function initialize() {
	geocoder = new google.maps.Geocoder();
	var mapOptions = {
			zoom: 12,
			center: new google.maps.LatLng(37.5254000, 126.9256750),
			mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	map = new google.maps.Map(document.getElementById("google_map"), mapOptions);

	$("#google_map").css("width", $("#map_wrap0").width() + "px");
	google.maps.event.trigger(map, "resize");

	$(window).resize(function() {
		$("#google_map").css("width", $("#map_wrap0").width() + "px");
		google.maps.event.trigger(map, "resize");
    });

	set_marker();
}

function set_marker()
{
	var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';

	var shadow = {
      anchor: new google.maps.Point(15,33),
      url: iconURLPrefix + 'msmarker.shadow.png'
    };

	var infowindow = new google.maps.InfoWindow({
		maxWidth: 150
    });

    var markers = new Array();

    markers.push(["IFC Seoul", new google.maps.LatLng(37.5254000, 126.9256750)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(0)").text(), new google.maps.LatLng(37.5320453, 126.9141499)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(1)").text(), new google.maps.LatLng(37.5312547, 126.9171584)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(2)").text(), new google.maps.LatLng(37.5264360, 126.8960040)]);
    markers.push([$(".publicOffice").find(".placePosWrap li a:eq(3)").text(), new google.maps.LatLng(37.5224735, 126.9263277)]);

    var data_array = new Array();

	for (var i = 0; i < markers.length; i++)
	{
		data_array[markers[i][0]] = "layerPop0" + i;
	}

    var mark;
    var marks = new Array();

    for (var i = 0; i < markers.length; i++) {
      mark = new google.maps.Marker({
        position: markers[i][1],
        map: map,
        shadow: shadow,
        animation: google.maps.Animation.DROP
        //icon: icon_img
      });

      marks.push(mark);

      google.maps.event.addListener(mark, 'click', (function(mark, i) {
        return function() {
        	infowindow.setContent("<div class='infowindow'>" + markers[i][0] + "</div>");
			infowindow.open(map, mark);

			$(".infowindow").click(function() {
			    open_popup($(this).text(), data_array);
		    }).css("cursor", "pointer");
        }
      })(mark, i));
	}

	var bounds = new google.maps.LatLngBounds();

    //  Go through each...
    $.each(marks, function (index, marker) {
        bounds.extend(marker.position);
    });

    //  Fit these bounds to the map
    map.fitBounds(bounds);

    var listener = google.maps.event.addListener(map, "idle", function() {
		if (map.getZoom() > 16)
	    	map.setZoom(16);
	    google.maps.event.removeListener(listener);
    });

    $(".publicOffice > .placePosWrap > li > a").click(function() {
	    var $obj = $(this);
        var popID = $obj.attr('rel');

        for (var index in data_array)
        {
	        if (data_array[index] == popID)
	        {
	        	for (var i = 0; i < markers.length; i++)
	        	{
		        	if (markers[i][0] == index)
		        	{
			        	map.setZoom(15);
			        	map.setCenter(markers[i][1]);
		        	}
	        	}
	        }
        }
    });
}

function loadScript_hospital() {
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyAS183iaU1s89qCTyh9CSlTn3YXxvoRD7Y&sensor=true&callback=initialize_hospital";
	document.body.appendChild(script);
}

function initialize_hospital() {
	geocoder = new google.maps.Geocoder();
	var mapOptions = {
			zoom: 12,
			center: new google.maps.LatLng(37.489834,127.033989),
			mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	map = new google.maps.Map(document.getElementById("google_map2"), mapOptions);

	$("#google_map2").css("width", $("#map_wrap2").width() + "px");
	google.maps.event.trigger(map, "resize");

	$(window).resize(function() {
		$("#google_map2").css("width", $("#map_wrap2").width() + "px");
		google.maps.event.trigger(map, "resize");
    });

	set_marker_hospital();
}

function set_marker_hospital()
{
	var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';

	var shadow = {
      anchor: new google.maps.Point(15,33),
      url: iconURLPrefix + 'msmarker.shadow.png'
    };

	var infowindow = new google.maps.InfoWindow({
		maxWidth: 300,
		maxHeight: 200
    });

    var markers = new Array();

    markers.push(["IFC Seoul", new google.maps.LatLng(37.5254000, 126.9256750)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(0)").text(), new google.maps.LatLng(37.5204147, 126.9239888)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(1)").text(), new google.maps.LatLng(37.5216583, 126.9248750)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(2)").text(), new google.maps.LatLng(37.5214658, 126.9250847)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(3)").text(), new google.maps.LatLng(37.5227260, 126.9239910)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(4)").text(), new google.maps.LatLng(37.5189394, 126.9307527)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(5)").text(), new google.maps.LatLng(37.5226445, 126.9249565)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(6)").text(), new google.maps.LatLng(37.5242623, 126.9248899)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(7)").text(), new google.maps.LatLng(37.5238233, 126.9241529)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(8)").text(), new google.maps.LatLng(37.5225150, 126.9246490)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(9)").text(), new google.maps.LatLng(37.5204924, 126.9239892)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(10)").text(), new google.maps.LatLng(37.5204924, 126.9239892)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(11)").text(), new google.maps.LatLng(37.5214695, 126.9227668)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(12)").text(), new google.maps.LatLng(37.5236234, 126.9254903)]);
    markers.push([$(".hospital").find(".placePosWrap li a:eq(13)").text(), new google.maps.LatLng(37.5183253, 126.9367517)]);

    var data_array = new Array();

	for (var i = 0; i < markers.length; i++)
	{
		var new_num = i + 4;
		if (new_num == 4)
			new_num = 0;

		if (new_num < 10)
			data_array[markers[i][0]] = "layerPop0" + new_num;
		else
			data_array[markers[i][0]] = "layerPop" + new_num;
	}

    var mark;
    var marks = new Array();

    for (var i = 0; i < markers.length; i++) {
      mark = new google.maps.Marker({
        position: markers[i][1],
        map: map,
        shadow: shadow,
        animation: google.maps.Animation.DROP
        //icon: icon_img
      });

      marks.push(mark);

      google.maps.event.addListener(mark, 'click', (function(mark, i) {
        return function() {
        	infowindow.setContent("<div class='infowindow'>" + markers[i][0] + "</div>");
			infowindow.open(map, mark);

			$(".infowindow").click(function() {
			    open_popup($(this).text(), data_array);
		    }).css("cursor", "pointer");
        }
      })(mark, i));
	}

	var bounds = new google.maps.LatLngBounds();

    //  Go through each...
    $.each(marks, function (index, marker) {
        bounds.extend(marker.position);
    });

    //  Fit these bounds to the map
    map.fitBounds(bounds);

    var listener = google.maps.event.addListener(map, "idle", function() {
		if (map.getZoom() > 16)
	    	map.setZoom(16);
	    google.maps.event.removeListener(listener);
    });

    $(".hospital > .placePosWrap > li > a").click(function() {
	    var $obj = $(this);
        var popID = $obj.attr('rel');

        for (var index in data_array)
        {
	        if (data_array[index] == popID)
	        {
	        	for (var i = 0; i < markers.length; i++)
	        	{
		        	if (markers[i][0] == index)
		        	{
			        	map.setZoom(16);
			        	map.setCenter(markers[i][1]);
		        	}
	        	}
	        }
        }
    });
}

function open_popup(title, data_array)
{
	var mId = title;
	var winWidth = window.innerWidth;
   	var popID = data_array[mId];

   	if (data_array[mId] == "layerPop00")
   		return;

   	var width = 300;
   	var $popObj = $('#' + popID);
    $('body').append($popObj);

    $popObj.show().css({ 'width': Number(width) }); //Number(width)

    var popMargTop = ($popObj.height()) / 2;
    var popMargLeft = ($popObj.width()) / 2;

    $popObj.css({
        'margin-top': -popMargTop,
        'margin-left': -popMargLeft
    });

    var $wrap = $('body');

    if ($wrap.find('#popFade').length < 1) {
        $wrap.append('<div id="popFade"></div>')
    }
    $('#popFade').show();
    $('html').addClass('hidden');

    $("#popFade").unbind("click").click(function() {
		$(this).hide();
		$(".lyPopWrap").hide();
	});

	return;
}
