$(function() {
	$(".tempArea").find("a").click(function() {
		var url = $(this).attr("data-url");
		var text = $(this).find(".img_title").val();
		
		$(".zoom").attr("href", url).attr("title", text);
	});

	$(".zoomBtn").click(function() {
		var current = $(".zoom").attr("href");
		$(".lightbox_list").find("a").attr("data-lightbox", "gallery");
		$(".lightbox_list").find("a").each(function() {
			if (current == $(this).attr("href"))
			{
				$(this).attr("data-lightbox", "");
			}
		});
	});
	
	$(window).load(function() {
		reposition_img();
	})
	
	$(window).resize(function() {
		reposition_img();
	});
})

function reposition_img()
{
	c_height = 521;
	m_height = $(".previewImg").height();

	if (m_height <= c_height && $(document).width() >= 768)
	{
		var n_top = Math.floor((c_height - m_height)/2);
		$(".previewImg").css("margin-top", n_top + "px");
	}
	else
	{
		$(".previewImg").css("margin-top", "0px");
	}
}