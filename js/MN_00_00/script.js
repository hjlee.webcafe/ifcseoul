$(function() {
	$(window).resize(function() {
		set_video_size();
	});

	$(".js_PopClose").click(function(event) {
		event.preventDefault();
		event.stopPropagation();
		$(this).parent().parent().hide();
		if ($(this).parent().find(".vertical_m").prop("checked"))
		{
			var id = $(this).closest(".layer_popup").attr("id");
			setCookie(id, "true", 1);
		}
	});
})

$(window).load(function() {
	set_video_size();
	setTimeout(function(){set_video_size();}, 200);
	$('.video_wrap img[usemap]').rwdImageMaps();
})

function set_video_size()
{
	if ($(".video_wrap img").length > 0)
	{
		var player = $(".video_wrap img");
		var popup = $("#noticePop_211110");

		var temp = new Image();
		temp.src = player.attr('src');
		var w = temp.width;
		var h = temp.height;
		var left = 0;
		var top = 0;

		if ($(window).width() > w) {
			left = ($(window).width() - w) / 2;
			top = 170;
		}
		else {
			left =0;
			top =0;
			player.width($(window).width());
		}

		popup.css("top", top + "px").css("left", left + "px");
	}
}

function setCookie(cName, cValue, cDay){
	var expire = new Date();
	expire.setDate(expire.getDate() + cDay);
	cookies = cName + '=' + escape(cValue) + '; path=/ ';
	if(typeof cDay != 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
	document.cookie = cookies;
}

$.fn.rwdImageMaps = function() {
	var $img = this;

	var rwdImageMap = function() {
		$img.each(function() {
			if (typeof($(this).attr('usemap')) == 'undefined')
				return;

			var that = this,
				$that = $(that);

			// Since WebKit doesn't know the height until after the image has loaded, perform everything in an onload copy
			$('<img />').load(function() {
				var attrW = 'width',
					attrH = 'height',
					w = $that.attr(attrW),
					h = $that.attr(attrH);

				if (!w || !h) {
					var temp = new Image();
					temp.src = $that.attr('src');
					if (!w)
						w = temp.width;
					if (!h)
						h = temp.height;
				}

				var wPercent = $that.width()/100,
					hPercent = $that.height()/100,
					map = $that.attr('usemap').replace('#', ''),
					c = 'coords';

				$('map[name="' + map + '"]').find('area').each(function() {
					var $this = $(this);
					if (!$this.data(c))
						$this.data(c, $this.attr(c));

					var coords = $this.data(c).split(','),
						coordsPercent = new Array(coords.length);

					for (var i = 0; i < coordsPercent.length; ++i) {
						if (i % 2 === 0)
							coordsPercent[i] = parseInt(((coords[i]/w)*100)*wPercent);
						else
							coordsPercent[i] = parseInt(((coords[i]/h)*100)*hPercent);
					}
					$this.attr(c, coordsPercent.toString());
				});
			}).attr('src', $that.attr('src'));
		});
	};
	$(window).resize(rwdImageMap).trigger('resize');

	return this;
};
