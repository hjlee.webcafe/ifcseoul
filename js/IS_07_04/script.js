$(function() {
	videojs.options.flash.swf = "/swf/video-js.swf";
	
	$(window).resize(function() {
		set_video_size();
	});
})

$(window).load(function() {
	set_video_size();
})

function set_video_size()
{
	var player = $("#ad_movie");
	var aspect = 9/16;
	var width = $(".videoWrap").width();
	var height = width * aspect;
	player.width(width).height(height);
	$(".vjs-tech").width(width).height(height);
}