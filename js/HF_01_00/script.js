$(function() {

    // IFCMall 위치설정 : 위도/경도 설정
    var ifcmall = new naver.maps.LatLng( 37.5254000, 126.9256750 ),

    map = new naver.maps.Map('ifcmall_naver_map', {
        center: ifcmall.destinationPoint(0, 500),
        zoom: 15,                                        // 줌설정 : 1~14, 수치가 클수록 지도 확대(줌인), 이 옵션 생략시 기본값 9
        zoomControl : true,                              // 줌 컨트롤 표시, 지정하지 않으면 false 가 기본값
        zoomControlOptions : {
            position : naver.maps.Position.TOP_RIGHT     // 오른쪽 위로 설정
        },
        mapTypeControl : true                         // 일반ㆍ위성 지도보기 컨트롤 표시, 지정하지 않으면 false 가 기본값
    }),
    marker = new naver.maps.Marker({        // 마커표시
        map: map,
        position: ifcmall
    });

    //정보창 내용 설정 및 표시
    var contentString = [
        '<div style="padding:16px;">',
            '<h2>IFC SEOUL</h2>',
            $("#ifcmall_address").val(),
        '</div>'
    ].join('');
    var infowindow = new naver.maps.InfoWindow({
        content: contentString
    });

    // 마커 클릭시 정보창 오픈/클로즈
    naver.maps.Event.addListener(marker, "click", function(e) {
        if (infowindow.getMap()) {
            infowindow.close();
        } else {
            infowindow.open(map, marker);
        }
    });

    infowindow.open(map, marker);

})
