<% @language="vbscript" codepage="949"%>
<%
	Response.ChaRset="EUC-KR"
	Session.Codepage=949

	// for error check
	Err.clear
	On Error Resume Next
%>
<!-- #include file = "inc/default_function.asp" -->
<!-- #include file = "inc/dbconn.asp" -->
<%
	'Response.ChaRset="euc-kr"
	Response.Buffer = true
	Response.ContentType="excel/application"
	Response.AddHeader "Content-Disposition","attachment; filename=AdminList_" & date() & ".xls" 

	Dim intAdminSeq, strAdminId, strAdminName, loginFlag
	Dim objConn, objRs, arrRs, strQuery, selectQuery

	strAdminId = Session("R_UID")
	strAdminName = Session("R_USER_NAME")
	loginFlag = True

	strQuery = "select * from TBL_USER_INFO where UID = '"& strAdminId &"' and USER_NAME = '"& strAdminName &"' AND USE_FLAG = 'Y' AND AUTHORITY = 'S' "
	
	set objConn = OpenDBConnection()
	set objRs = SendQuery(objConn,strQuery)
	
	If objRs.EOF Then
		loginFlag = false
	End If
	
	objRs.Close

	If Not loginFlag Then
		Response.End()
	End If

	selectQuery = " select * from dbo.TBL_USER_INFO where AUTHORITY IN ('S','A') order by REG_DT desc "

	set objRs = SendQuery(objConn,selectQuery)
%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<title>:::IFC 관리자:::</title>
	<meta charset="EUC-KR">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
</head>
<body>
	<div class="wrap">
		<div class="content_wrap">
			<div class="contents">
				<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="Admin List">
					<caption>관리자목록</caption>
					<colgroup>
						<col width="2%"><col width="6%"><col width="6%"><col width="5%"><col width="5%"><col width="5%"><col width="5%"><col width="7%"><col width="5%"><col width="5%"><col width="26%"><col width="3%"><col width="5%"><col width="5%"><col width="5%"><col width="5%">
					</colgroup>
					<thead>
						<tr>
							<th>번호</th><th>아이디</th><th>이름</th><th>등급</th><th>전화번호</th><th>휴대폰번호</th><th>팩스번호</th><th>이메일</th><th>부서</th><th>직책</th><th>메모</th><th>사용여부</th><th>최종접속일</th><th>등록일(등록자)</th><th>최종수정일(수정자)</th><th>탈퇴일(탈퇴자)</th>
						</tr>
					</thead>
					<tbody>
						<% if objRs.EOF Then %>
						<tr>
							<td colspan="16" align="center">
								리스트가 없습니다.
							</td>
						</tr>
						<%
						Else
							For k = 0 to objRs.RecordCount - 1
								strAuthority = ""
								If objRs("AUTHORITY")&"" = "S" Then
									strAuthority = "SuperAdmin"
								ElseIf objRs("AUTHORITY")&"" = "A" Then
									strAuthority = "Admin"
								End If

								strLastLoginDt = ""
								If objRs("LAST_LOGIN_DT")&"" <> "" Then
									strLastLoginDt = FnFormatDateTime(objRs("LAST_LOGIN_DT"))
								End If

								strUpdDt = ""
								If objRs("UPD_DT")&"" <> "" Then
									strUpdDt = FnFormatDateTime(objRs("UPD_DT")) & "(" & objRs("UPD_UID") & ")"
								End If

								strDelDt = ""
								If objRs("DEL_DT")&"" <> "" Then
									strDelDt = FnFormatDateTime(objRs("DEL_DT")) & "(" & objRs("DEL_UID") & ")"
								End If
						%>
								<tr>
									<td class="text-center"><%=k+1%></td>
									<td><%=objRs("UID")%></td>
									<td><%=objRs("USER_NAME")%></td>
									<td><%=strAuthority%></td>
									<td><%=FnSetNum(objRs("TELNUM"))%></td>
									<td><%=FnSetNum(objRs("MOBILENUM"))%></td>
									<td><%=FnSetNum(objRs("FAXNUM"))%></td>
									<td><%=objRs("EMAIL")%></td>
									<td><%=objRs("DEPT_NAME")%></td>
									<td><%=objRs("DEPT_LEVEL")%></td>
									<td><%=objRs("DESCRIPTION")%></td>
									<td><%=objRs("USE_FLAG")%></td>
									<td><%=strLastLoginDt%></td>
									<td><%=FnFormatDateTime(objRs("REG_DT"))%>(<%=objRs("REG_UID")%>)</td>
									<td><%=strUpdDt%></td>
									<td><%=strDelDt%></td>
								</tr>
						<%
								objRs.MoveNext
							Next
						End If
						%>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>