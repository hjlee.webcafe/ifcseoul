<% @language="vbscript" codepage="949"%>
<%
	Response.ChaRset="EUC-KR"
	Session.Codepage=949

	// for error check
	Err.clear
	On Error Resume Next
%>
<!-- #include file = "inc/default_function.asp" -->
<!-- #include file = "inc/checkParam.asp" -->
<!-- #include file = "inc/dbconn.asp" -->
<%
	'Response.ChaRset="euc-kr"
	Response.Buffer = true
	Response.ContentType="excel/application"
	Response.AddHeader "Content-Disposition","attachment; filename=reservation_list_" & date() & ".xls"

	Dim intAdminSeq, strAdminId, strAdminName, loginFlag
	Dim objConn, objRs, arrRs, strQuery, selectQuery

	strAdminId = Session("R_UID")
	strAdminName = Session("R_USER_NAME")
	loginFlag = True

	strQuery = "select * from TBL_USER_INFO where UID = '"& strAdminId &"' and USER_NAME = '"& strAdminName &"' AND USE_FLAG = 'Y' AND AUTHORITY IN ('S','A') "

	set objConn = OpenDBConnection()
	set objRs = SendQuery(objConn,strQuery)

	If objRs.EOF Then
		loginFlag = false
	End If

	objRs.Close

	If Not loginFlag Then
		Response.End()
	End If

	query_where = ""
	If unescape(Request("search_text"))&"" <> "" Then
		query_where = query_where & " and " & Request("search_type") & " Like '%" & GetTag2Text(unescape(Request("search_text"))) & "%' "
	End If
	If Request("room_type")&"" <> "" Then
		query_where = query_where & " and LEFT(RESERVE_NO,1) = '" & Request("room_type") & "' "
	End If
	If Request("reserve_status")&"" <> "" Then
		query_where = query_where & " and RESERVE_STATUS = '" & Request("reserve_status") & "' "
	End If
	If Request("pay_status")&"" <> "" Then
		If Request("reserve_status")&"" = "C" Then ' 예약취소이면 취소지불상태를 검색
			query_where = query_where & " and CAN_PAY_STATUS = '" & Request("pay_status") & "' "
		Else
			query_where = query_where & " and PAY_STATUS = '" & Request("pay_status") & "' "
		End If
	End If
	If Request("chk_status")&"" <> "" Then
		query_where = query_where & " and CHK_STATUS = '" & Request("chk_status") & "' "
	End If
	If Request("start_date")&"" <> "" And Request("end_date")&"" <> "" Then
		query_where = query_where & " and RESERVE_DT BETWEEN '" & Replace(Request("start_date"),"-","") & "' AND '" & Replace(Request("end_date"),"-","") & "' "
	End If

	selectQuery = " select (SELECT MIN(CONVERT(INT,RESERVE_TIME)) FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MIN_TIME, (SELECT MAX(CONVERT(INT,RESERVE_TIME))+1 FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MAX_TIME, (SELECT CD_VENR FROM TBL_USER_INFO WHERE UID=RI.UID) AS CD_VENR, * from dbo.TBL_RESERVATION_INFO RI WHERE 1=1 " & query_where & " order by REG_DT desc "

	set objRs = SendQuery(objConn,selectQuery)
%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<title>:::IFC 관리자:::</title>
	<meta charset="EUC-KR">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
</head>
<body>
	<div class="wrap">
		<div class="content_wrap">
			<div class="contents">
				<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="Admin List">
					<caption>예약리스트</caption>
					<colgroup>
						<col width="3%"><col width="4%"><col width="3%"><col width="4%"><col width="3%"><col width="3%"><col width="4%"><col width="4%"><col width="4%"><col width="4%"><col width="4%"><col width="3%"><col width="3%"><col width="3%"><col width="3%"><col width="3%"><col width="3%"><col width="3%"><col width="3%"><col width="3%"><col width="3%"><col width="3%"><col width="3%"><col width="3%"><col width="7%"><col width="7%"><col width="4%"><col width="4%"><col width="4%"><col width="4%"><col width="4%">
					</colgroup>
					<thead>
						<tr>
							<th>예약번호</th><th>회의실</th><th>이용날짜</th><th>이용시간</th><th>예약상태</th><th>인원</th><th>회의명</th><th>예약자ID</th><th>업체코드</th><th>예약자명</th><th>입주사명</th><th>기본요금</th><th>할인율</th><th>무료이용시간</th><th>총요금</th><th>폴리콤</th><th>폴리콤요금</th><th>외부기자재반입</th><th>페널티요금</th><th>납부상태</th><th>취소납부상태</th><th>예약확인</th><th>요구사항</th><th>메모</th><th>등록일(등록자)</th><th>취소일(취소자)</th><th>납부일(납부자)</th><th>예약확인일(예약확인자)</th><th>이용완료(이용완료자)</th>
						</tr>
					</thead>
					<tbody>
						<% if objRs.EOF Then %>
						<tr>
							<td colspan="29" align="center">
								리스트가 없습니다.
							</td>
						</tr>
						<%
						Else
							For k = 0 to objRs.RecordCount - 1
								strReserveDt = objRs("RESERVE_DT")
								If Len(strReserveDt) = 8 Then
									strReserveDt = Mid(strReserveDt,1,4) & "." & Mid(strReserveDt,5,2) & "." & Mid(strReserveDt,7,2)
								End If

								strTotalAmount = FnMoneySet(objRs("TOTAL_AMOUNT"))

								strReserveStatus = ""
								If objRs("RESERVE_STATUS")&"" = "R" Then
									strReserveStatus = "예약완료"
								ElseIf objRs("RESERVE_STATUS")&"" = "C" Then
									strReserveStatus = "예약취소"
								ElseIf objRs("RESERVE_STATUS")&"" = "Y" Then
									strReserveStatus = "이용완료"
								End If

								strPayStatus = ""
								' 예약이 되어있으면 정상 결제상태를
								If objRs("PAY_STATUS")&"" = "N" Then
									strPayStatus = "대기"
								ElseIf objRs("PAY_STATUS")&"" = "F" Then
									strPayStatus = "현장결제"
								ElseIf objRs("PAY_STATUS")&"" = "M" Then
									strPayStatus = "관리비합산"
								End If

								strCanPayStatus = ""
								' 예약이 취소되었으면 취소 결제상태를
								If objRs("CAN_PAY_STATUS")&"" = "N" Then
									strCanPayStatus = "대기"
									If objRs("CAN_AMOUNT")&"" = "0" Then
										strCanPayStatus = "없음"
									End If
								ElseIf objRs("CAN_PAY_STATUS")&"" = "F" Then
									strCanPayStatus = "현장결제"
								ElseIf objRs("CAN_PAY_STATUS")&"" = "M" Then
									strCanPayStatus = "관리비합산"
								End If

								strCanAmount = ""
								If objRs("RESERVE_STATUS")&"" = "C" Then
									strCanAmount = FnMoneySet(objRs("CAN_AMOUNT"))
								End If

								strChkStatus = ""
								If objRs("CHK_STATUS")&"" = "N" Then
									strChkStatus = "미확인"
								ElseIf objRs("CHK_STATUS")&"" = "Y" Then
									strChkStatus = "예약확인"
								End If

								strRequirement = objRs("REQUIREMENT")
								If strRequirement&"" <> "" Then
									strRequirement = Replace(Replace(strRequirement, "&lt", "<"),"<br><br>"," ")
								End If

								strDescription = objRs("DESCRIPTION")
								If strDescription&"" <> "" Then
									strDescription = Replace(Replace(strDescription, "&lt", "<"),"<br><br>", " ")
								End If

								strPolycom = ""
								If objRs("POLYCOM")&"" = "N" Then
									strPolycom = "사용안함"
								ElseIf objRs("POLYCOM")&"" = "L" Then
									strPolycom = "국내전화"
								ElseIf objRs("POLYCOM")&"" = "G" Then
									strPolycom = "국외전화"
								End If

								strEtc = ""
								If objRs("ETC")&"" = "N" Then
									strEtc = "반입안함"
								ElseIf objRs("ETC")&"" = "Y" Then
									strEtc = "반입함"
								End If
						%>
								<tr>
									<td><%=objRs("RESERVE_NO")%></td>
									<td><%=objRs("ROOM_NAMES")%></td>
									<td><%=strReserveDt%></td>
									<td><%=objRs("MIN_TIME")%>:00~<%=objRs("MAX_TIME")%>:00 (<%=objRs("T_QTY")%>시간)</td>
									<td><%=strReserveStatus%></td>
									<td><%=objRs("P_COUNT")%></td>
									<td><%=objRs("SUBJECT")%></td>
									<td><%=objRs("UID")%></td>
									<td><%=objRs("CD_VENR")%></td>
									<td><%=objRs("RESERVE_NAME")%></td>
									<td><%=objRs("USER_NAME")%></td>
									<td><%=FnMoneySet(objRs("ORI_AMOUNT"))%></td>
									<td><%=FnMoneySet(objRs("DISCOUNT")) & "%"%></td>
									<td><%=FnMoneySet(objRs("FREE_TIME"))%></td>
									<td><%=FnMoneySet(objRs("TOTAL_AMOUNT"))%></td>
									<td><%=strPolycom%></td>
									<td><%=FnMoneySet(objRs("ETC_AMOUNT"))%></td>
									<td><%=strEtc%></td>
									<td><%=strCanAmount%></td>
									<td><%=strPayStatus%></td>
									<td><%=strCanPayStatus%></td>
									<td><%=strChkStatus%></td>
									<td><%=strRequirement%></td>
									<td><%=strDescription%></td>
									<td><%=FnFormatDateTime(objRs("REG_DT"))%>(<%=objRs("REG_UID")%>)</td>
									<td><%=FnFormatDateTime(objRs("CAN_DT"))%>(<%=objRs("CAN_UID")%>)</td>
									<td><%=FnFormatDateTime(objRs("PAY_DT"))%>(<%=objRs("PAY_UID")%>)</td>
									<td><%=FnFormatDateTime(objRs("CHK_DT"))%>(<%=objRs("CHK_UID")%>)</td>
									<td><%=FnFormatDateTime(objRs("COM_DT"))%>(<%=objRs("COM_UID")%>)</td>
								</tr>
						<%
								objRs.MoveNext
							Next
						End If
						%>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>
