<%

'page handling section
Dim search_link, list_link, query_string, query_array, data_array, request_dic
Dim page_limit, page_limit_select, page, current_count, total_count, page_length
Set page_limit_select = Server.CreateObject("scripting.Dictionary")
page_length = 10
page_limit_select.Add "10", ""
page_limit_select.Add "20", ""
page_limit_select.Add "30", ""
page_limit_select.Add "50", ""
page_limit_select.Add "100", ""
page_limit_select.Add "200", ""
page_limit_select.Add "500", ""
page_limit_select.Add "1000", ""

Set request_dic = Server.CreateObject("scripting.Dictionary")

query_string = Request.QueryString
query_array = Split(query_string, "&")

Dim temp_array

If query_string <> "" Then
	For Each item In query_array
		temp_array = Split(item, "=")
		If (request_dic.Exists(temp_array(0))) Then
			request_dic.Item(temp_array(0)) = temp_array(1)
		Else
			request_dic.add temp_array(0), temp_array(1)
		End IF
	Next
End If

If query_string <> "" Then
	For Each item In request_dic.Keys
		If item = "mode" or item = "num" or item = "search_btn" or item = "key" or item = "page" Then
		Else
			list_link = list_link & item & "=" & request_dic.item(item) & "&"
		End If
	Next
End If

If list_link = "" Then 'this if is for search_link
	list_link = "?page=1"
Else
	list_link = Left(list_link, Len(list_link)-1)
	search_link = "&" & list_link

	If request_dic.item("page") <> "" Then
		list_link = "?page=" & request_dic.item("page") & "&" & list_link
	Else
		list_link = "?" & list_link
	End If
End If

If request_dic.item("page_limit") = "" Then
	page_limit = 10
Else
	page_limit = CInt(request_dic.item("page_limit"))
End If

page_limit_select.Item(CStr(page_limit)) = "selected"

If request_dic.item("page") = "" Then
	page = 1
Else
	page = CInt(request_dic.item("page")) 'current page
End If

current_count = (page - 1) * page_limit 'set count number of current page
total_count = 0 'default set for count

Function page_create(p_total_count, p_page, p_page_limit, p_page_length)
	Dim p_total_page, p_start_page, p_last_page, p_prev_page, p_next_page, result
	p_total_page = Ceil(p_total_count / p_page_limit)
	
	If p_total_page = 0 Then
		p_total_page = 1
	End If
	
	If p_page > p_total_page Then
		p_page = p_total_page
	End If
	
	If (p_page mod p_page_length) = 0 Then
		p_page = p_page - 1	
	End If

	p_start_page = floor(p_page/p_page_length) * p_page_length + 1
	p_last_page = floor(p_page/p_page_length) * p_page_length + p_page_length

	p_prev_page = p_start_page - 1
	p_next_page = p_last_page + 1
	
	If p_start_page = 1 Then
		 p_prev_page = ""
	End If
	
	If p_last_page >= p_total_page Then
		p_next_page = ""
	End If
	
	If p_last_page > p_total_page Then
		p_last_page = p_total_page
	End If
	
	result = array(p_prev_page, p_start_page, p_last_page, p_next_page, p_total_page)
	page_create = result
End Function
%>