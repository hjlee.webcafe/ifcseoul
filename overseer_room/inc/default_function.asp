<%

'check file exist
Function checkFileExist(path)
	Dim isExist, fs, temp_path
	temp_path = path
	isExist = false
	
	'replace "/" to "\"
	If InStr(1,path,":") = 0 Then
        temp_path = Server.MapPath(path)
    End If

	set fs=Server.CreateObject("Scripting.FileSystemObject")
	isExist = fs.FileExists(temp_path)
	'return result with boolean
	checkFileExist = isExist
End Function

'for include
Function include(sFile)
    dim mfo, mf, sTemp, arTemp, arTemp2, lTemp, sTemp2, lTemp2, sFile2
 
    If InStr(1,sFile,":") = 0 Then
        sFile = Server.MapPath(sFile)
    End If
 
    'first read the file into a variable use FSO
    set mfo = Server.CreateObject("Scripting.FileSystemObject")

    'does file exist?
    If mfo.FileExists(sFile) Then
        'read it
        set mf = mfo.OpenTextFile(sFile, 1, false, -2)
        sTemp = mf.ReadAll
        mf.close
        set mfo = nothing
    Else
        sTemp = ""
    End If
 
    If sTemp <> "" Then
        'sTemp contains the mixed ASP and HTML, so the next task is to dynamically replace the inline HTML with response.write statements
        arTemp = Split(sTemp,"<" & "%")
        sTemp = ""
 
        For lTemp = LBound(arTemp) to UBound(arTemp)
 
            If InStr(1,arTemp(lTemp),"%" & ">") > 0 Then
                'inline asp
                arTemp2 = Split(arTemp(lTemp),"%" & ">")
 
                'everything up to the % > is ASP code
 
                sTemp2 = trim(arTemp2(0))
 
                If Left(sTemp2,1) = "=" Then
                    'need to replace with response.write
                    sTemp2 = "Response.Write " & mid(sTemp2,2)
                End If
 
                sTemp = sTemp & sTemp2 & vbCrLf
 
                'everything after the % > is HTML
                sTemp2 = arTemp2(1)
 
            Else
                'inline html only
                sTemp2 = arTemp(lTemp)
 
            End If
 
            arTemp2 = Split(sTemp2,vbCrLf)
            For lTemp2 = LBound(arTemp2) to UBound(arTemp2)
                sTemp2 = Replace(arTemp2(lTemp2),"""","""""")   'replace quotes with doubled quotes
                sTemp2 = "Response.Write """ & sTemp2 & """"    'add response.write and quoting
 
                If lTemp2 < Ubound(arTemp2) Then
                    sTemp2 = sTemp2 & " & vbCrLf"   'add cr+lf if not the last line inlined
                End If
 
                sTemp = sTemp & sTemp2 & vbCrLf 'add to running variable
            Next
 
        Next
 
        Execute sTemp
 
        ExecInclude = True
 
    End If
End Function

' Returns the largest integer less than or equal to the specified number.
function Floor(x)
    dim temp

    temp = Round(x)

    if temp > x then
        temp = temp - 1
    end if

    floor = temp
end function

' Returns the smallest integer greater than or equal to the specified number.
function Ceil(x)
    dim temp

    temp = Round(x)

    if temp < x then
        temp = temp + 1
    end if

    ceil = temp
end Function

Function FnAlert(fnStr)
	Response.write "<script>"
	Response.write "alert('"&fnStr&"')"
	Response.write "</script>"
End Function

Function FnBR(str_)
	If str_&"" <> "" Then
		str_ = Replace(str_, Chr(13), "<br>", 1, -1, 1)
		str_ = Replace(str_, Chr(10), "<br>", 1, -1, 1)
	End If
	Response.write str_
End Function

Function FnBR2(str_)
	If str_&"" <> "" Then
		str_ = Replace(str_, "<br>", Chr(13), 1, -1, 1)
		str_ = Replace(str_, Chr(10), "", 1, -1, 1)
	End If
	Response.write str_
End Function

Function FnFormatDateTime(fnDateTime)
	strDateTime = ""
	If fnDateTime&"" <> "" Then
		strDateTime = FormatDateTime(fnDateTime,2) & " " & FormatDateTime(fnDateTime,4)
	End If

	FnFormatDateTime = strDateTime
End Function

Function FnSetNum(fnNum)
	If fnNum&"" <> "" Then
		fnNum = Trim(Replace(fnNum,"-",""))

		Select Case Len(fnNum)
			Case 8    '1588-xxxx
				t1 = Mid(fnNum,1,4)
				t2 = Mid(fnNum,5,4)
				fnNum = t1 & "-" &t2
			Case 9 '02-xxx-xxxx
				t1 = Mid(fnNum,1,2)
				t2 = Mid(fnNum,3,3)
				t3 = Mid(fnNum,6,4)
				fnNum = t1 & "-" &t2 & "-" &t3
			Case 10 '�޴���ȭ 010-xxx-xxxx
				If Mid(fnNum,1,2) = "01" Then '�޴���ȭ 010-xxx-xxxx
					t1 = Mid(fnNum,1,3)
					t2 = Mid(fnNum,4,3)
					t3 = Mid(fnNum,7,4)
					fnNum = t1 & "-" &t2 & "-" &t3
				Else '�Ϲ���ȭ
					If Mid(fnNum,1,2) = "02" Then
						t1 = Mid(fnNum,1,2)
						t2 = Mid(fnNum,3,4)
						t3 = Mid(fnNum,7,4)
						fnNum = t1 & "-" &t2 & "-" &t3
					Else
						t1 = Mid(fnNum,1,3)
						t2 = Mid(fnNum,4,3)
						t3 = Mid(fnNum,7,4)
						fnNum = t1 & "-" &t2 & "-" &t3
					End If
				End If
			Case 11 'xxx-xxxx-xxxx(�޴���ȭ,070)
				t1 = Mid(fnNum,1,3)
				t2 = Mid(fnNum,4,4)
				t3 = Mid(fnNum,8,4)
				fnNum = t1 & "-" &t2 & "-" &t3
			Case Else
				fnNum = fnNum
		End Select
	End If

	FnSetNum = fnNum
End Function

Function FnMoneySet(fnMoney)
	If Len(fnMoney) = 4 Then
		fnMoney = Mid(fnMoney,1,1) & "," & Mid(fnMoney,2,3)
	ElseIf Len(fnMoney) = 5 Then
		fnMoney = Mid(fnMoney,1,2) & "," & Mid(fnMoney,3,3)
	ElseIf Len(fnMoney) = 6 Then
		fnMoney = Mid(fnMoney,1,3) & "," & Mid(fnMoney,4,3)
	ElseIf Len(fnMoney) = 7 Then
		fnMoney = Mid(fnMoney,1,1) & "," & Mid(fnMoney,2,3) & "," & Mid(fnMoney,5,3)
	ElseIf Len(fnMoney) = 8 Then
		fnMoney = Mid(fnMoney,1,2) & "," & Mid(fnMoney,3,3) & "," & Mid(fnMoney,6,3)
	End If

	FnMoneySet = fnMoney
End Function
%>