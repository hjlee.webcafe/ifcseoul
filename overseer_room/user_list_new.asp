<!--#include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%
	USE_FREE_TIME = 24 - ((Month(NOW) -1) * 2)
%>
<script type="text/javascript">
	function fnGoBack() {
		get_link = $("#list_link_hidden").val();
		location.href = get_link;
	}

    function fnAuthority() {
        if ($("#AUTHORITY").val() == "N") {
            document.getElementById("MONTHLY_FREE_TIME").value = 0;
            document.getElementById("MONTHLY_FREE_TIME").disabled = true;
            document.getElementById("CD_VENR").value = "";
            document.getElementById("CD_VENR").disabled = true;
            document.getElementById("USER_NM").value = "";
            document.getElementById("USER_NM").disabled = true;
        } else {
            document.getElementById("MONTHLY_FREE_TIME").value = "";
            document.getElementById("MONTHLY_FREE_TIME").disabled = false;
            document.getElementById("CD_VENR").disabled = false;
            document.getElementById("USER_NM").disabled = false;
        }
    }

	function chkPwd(str) {
		var reg_pwd = /^.*(?=.{8,16})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;

		if (str.length < 8 || str.length > 16) {
			return false;
		}

		if (!reg_pwd.test(str)) {
			return false;
		}
		return true;
	}

	function fnChkUID() {
		if ($("#UID").val() == "") {
			alert("아이디를 입력해주세요.");
			$("#UID").focus();
			return true;
		}

		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/user_proc.asp",
			dataType : "json",
			data : ({"UID" : $("#UID").val(), "mode" : "chk_id"}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == 0) {
						alert("사용가능한 아이디입니다.");
						$("#PASSWORD").focus();
						$("#btn_ChkUID").addClass("btn-success").removeClass("btn-default").removeClass("btn-warning");
						$("#UID_CHK").val("true");
						$("#UID_STORE").val($("#UID").val());
					}
					else if (data.result == 1) {
						alert("존재하는 아이디입니다.");
						$("#UID").focus();
						$("#btn_ChkUID").addClass("btn-success").removeClass("btn-default").removeClass("btn-warning");
						$("#UID_CHK").val("");
						$("#UID_STORE").val("");
					}
					else {
						alert(error_msg);
						$("#UID").focus();
						$("#btn_ChkUID").addClass("btn-success").removeClass("btn-default").removeClass("btn-warning");
						$("#UID_CHK").val("");
						$("#UID_STORE").val("");
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}

	function fnAdd() {
		if ($("#UID").val() == "") {
			alert("아이디를 입력해주세요.");
			$("#UID").focus();
			return true;
		}

		if ($("#UID_CHK").val() !== 'true' || $("#UID").val() != $("#UID_STORE").val()) {
			alert("아이디 중복을 확인해주세요.");
			return;
		}

		if ($("#AUTHORITY").val() == "U" && $("#CD_VENR").val() == "") {
			alert("업체코드를 입력해주세요.");
			$("#CD_VENR").focus();
			return true;
		}

		if ($("#PASSWORD").val() == "") {
			alert("비밀번호를 입력해주세요.");
			$("#PASSWORD").focus();
			return true;
		}
		else {
			if (!chkPwd($.trim($("#PASSWORD").val()))) {
				alert("비밀번호를 확인하세요.\n\n영문,숫자를 혼합하여 8~16자 이내");
				$("#PASSWORD").focus();
				return true;
			}
		}

		if ($("#PASSWORD_CHK").val() == "") {
			alert("비밀번호를 한번 더 입력해주세요.");
			$("#PASSWORD_CHK").focus();
			return true;
		}

		if ($("#PASSWORD").val() != $("#PASSWORD_CHK").val()) {
			alert("비밀번호가 다릅니다.\n\n비밀번호를 확인해주세요.");
			$("#PASSWORD_CHK").focus();
			return true;
		}

		if ($("#AUTHORITY").val() == "U" && $("#USER_NM").val() == "") {
			alert("입주사명을 입력해주세요.");
			$("#USER_NM").focus();
			return true;
		}
		/*
		if ($("#BUSINESS_RE_NO1").val() == "") {
			alert("사업자 등록번호를 입력해주세요.");
			$("#BUSINESS_RE_NO1").focus();
			return true;
		}

		if ($("#BUSINESS_RE_NO2").val() == "") {
			alert("사업자 등록번호를 입력해주세요.");
			$("#BUSINESS_RE_NO2").focus();
			return true;
		}

		if ($("#BUSINESS_RE_NO3").val() == "") {
			alert("사업자 등록번호를 입력해주세요.");
			$("#BUSINESS_RE_NO3").focus();
			return true;
		}
		*/
		if ($("#MANAGER_NAME").val() == "") {
			alert("담당자명을 입력해주세요.");
			$("#MANAGER_NAME").focus();
			return true;
		}
		/*
		if ($("#TELNUM").val() == "") {
			alert("전화번호를 입력해주세요.");
			$("#TELNUM").focus();
			return true;
		}

		if ($("#FAXNUM").val() == "") {
			alert("팩스번호를 입력해주세요.");
			$("#FAXNUM").focus();
			return true;
		}

		if ($("#MOBILENUM").val() == "") {
			alert("휴대폰번호를 입력해주세요.");
			$("#MOBILENUM").focus();
			return true;
		}
		*/
		if ($("#EMAIL").val() == "") {
			alert("이메일을 입력해주세요.");
			$("#EMAIL").focus();
			return true;
		}
		else {
			var emailReg = new RegExp(/^([0-9a-zA-Z_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,3}$/);
			if (!emailReg.test($("#EMAIL").val())) {
				alert("이메일 주소가 잘못되었습니다.");
				$("#EMAIL").focus();
				return true;
			}
		}
		/*
		if ($("#DEPT_NAME").val() == "") {
			alert("부서를 입력해주세요.");
			$("#DEPT_NAME").focus();
			return true;
		}

		if ($("#DEPT_LEVEL").val() == "") {
			alert("직책을 입력해주세요.");
			$("#DEPT_LEVEL").focus();
			return true;
		}
		*/

        if ($("#MONTHLY_FREE_TIME").val() == "") {
            alert("월별무상제공시간을 입력해주세요.");
            $("#MONTHLY_FREE_TIME").focus();
            return true;
        }

		if (!confirm("등록 하시겠습니까?"))
			return true;

		if ($("#index_loading_overlay").css("display") != 'none') {
			alert("등록 중 입니다");
			return false;
		}
		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/user_proc.asp",
			dataType : "json",
			data : ({
						"mode" : "add",
						"UID" : $("#UID").val(),
						"CD_VENR" : $("#CD_VENR").val(),
						"PASSWORD" : hex_md5($("#PASSWORD").val()),
						"AUTHORITY" : $("#AUTHORITY").val(),
						"USER_NM" : $("#USER_NM").val(),
						"BUSINESS_RE_NO" : $("#BUSINESS_RE_NO1").val() + "-" + $("#BUSINESS_RE_NO2").val() + "-" + $("#BUSINESS_RE_NO3").val(),
						"MANAGER_NAME" : $("#MANAGER_NAME").val(),
						"TELNUM" : $("#TELNUM").val(),
						"FAXNUM" : $("#FAXNUM").val(),
						"MOBILENUM" : $("#MOBILENUM").val(),
						"EMAIL" : $("#EMAIL").val(),
						"DEPT_NAME" : $("#DEPT_NAME").val(),
						"DEPT_LEVEL" : $("#DEPT_LEVEL").val(),
						"MONTHLY_FREE_TIME" : $("#MONTHLY_FREE_TIME").val(),
						"LIMIT_TIME" : 64
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == 0) {
						alert("등록되었습니다.");
						get_link = "user_list.asp";//$("#list_link_hidden").val();
						location.href = get_link;
					}
					else {
						alert(error_msg);
						$("#UID").focus();
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}
</script>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="입주사 계정등록">
		<caption>입주사 계정등록</caption>
		<colgroup>
			<col width="15%"><col width="35%"><col width="15%"><col width="35%">
		</colgroup>
		<tbody>
            <tr>
                <th>등급</th>
                <td colspan="3">
                    <select id="AUTHORITY" name="AUTHORITY" class="form-control input-sm width_100" onChange="fnAuthority()">
                        <option value="U" <%If Request("authority")&""="U" Then%>selected<%End If%>>Tenant</option>
                        <option value="N" <%If Request("authority")&""="N" Then%>selected<%End If%>>NonTenant</option>
                    </select>
                </td>
			<tr>
				<th>아이디</th>
				<td>
					<input id="UID" name="UID" type="text" class="width_150" value="<%=UID%>" maxlength="30" style="ime-mode:disabled" onkeyup="this.value=this.value.replace(/[^A-Za-z0-9]/g,'');" />
					<input type="button" class="btn btn-info btn-sm" id="btn_ChkUID" value="아이디확인" onClick="fnChkUID()" />
					<input type="hidden" id="UID_CHK" name="UID_CHK" value="" />
					<input type="hidden" id="UID_STORE" name="UID_STORE" value="" />
				</td>
				<th>업체코드</th>
				<td><input id="CD_VENR" name="CD_VENR" type="text" class="width_150" value="<%=CD_VENR%>" maxlength="30" style="ime-mode:disabled" onkeyup="this.value=this.value.replace(/[^A-Za-z0-9]/g,'');" /></td>
			</tr>
			<tr>
				<th>비밀번호</th>
				<td><input id="PASSWORD" name="PASSWORD" type="password" class="width_150" value="<%=PASSWORD%>" maxlength="16" /><br><span>※ 영문 / 숫자 조합 8~16자리</span></td>
				<th>비밀번호 확인</th>
				<td><input id="PASSWORD_CHK" name="PASSWORD_CHK" type="password" class="width_150" value="" maxlength="16" /><br><span>※ 비밀번호를 한번 더 입력해주세요.</span></td>
			</tr>
			<tr>
				<th>입주사명</th>
				<td><input id="USER_NM" name="USER_NM" type="text" class="width_150" value="<%=USER_NAME%>" maxlength="50" /></td>
				<th>사업자 등록번호</th>
				<td>
					<input id="BUSINESS_RE_NO1" name="BUSINESS_RE_NO1" type="text" class="width_50" value="<%=BUSINESS_RE_NO1%>" maxlength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /> -
					<input id="BUSINESS_RE_NO2" name="BUSINESS_RE_NO2" type="text" class="width_50" value="<%=BUSINESS_RE_NO2%>" maxlength="2" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /> -
					<input id="BUSINESS_RE_NO3" name="BUSINESS_RE_NO3" type="text" class="width_100" value="<%=BUSINESS_RE_NO3%>" maxlength="5" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" />
				</td>
			</tr>
			<tr>
				<th>담당자명</th>
				<td><input id="MANAGER_NAME" name="MANAGER_NAME" type="text" class="width_150" value="<%=MANAGER_NAME%>" maxlength="50" /></td>
				<th>전화번호</th>
				<td><input id="TELNUM" name="TELNUM" type="text" class="width_150" value="<%=TELNUM%>" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
			</tr>
			<tr>
				<th>FAX</th>
				<td><input id="FAXNUM" name="FAXNUM" type="text" class="width_150" value="<%=FAXNUM%>" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
				<th>휴대폰번호</th>
				<td><input id="MOBILENUM" name="MOBILENUM" type="text" class="width_150" value="<%=MOBILENUM%>" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
			</tr>
			<tr>
				<th>이메일</th>
				<td><input id="EMAIL" name="EMAIL" type="text" class="i_text50" value="<%=EMAIL%>" maxlength="100" style="ime-mode:disabled" /></td>
				<th>부서</th>
				<td><input id="DEPT_NAME" name="DEPT_NAME" type="text" class="width_150" value="<%=DEPT_NAME%>" maxlength="50" /></td>
			</tr>
			<tr>
				<th>직책</th>
				<td><input id="DEPT_LEVEL" name="DEPT_LEVEL" type="text" class="width_150" value="<%=DEPT_LEVEL%>" maxlength="20" /></td>
				<th>월별무상제공시간</th>
				<td><input id="MONTHLY_FREE_TIME" name="MONTHLY_FREE_TIME" type="text" class="width_50" value="<%=MONTHLY_FREE_TIME%>" maxlength="2" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" />시간</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
		</li>
		<li class="btm_btn_right">
			<input type="button" class="btn btn-warning btn-sm" id="btn_list" name="btn_list" value="목록" onClick="fnGoBack();" />&nbsp;
			<input type="button" class="btn btn-primary btn-sm" id="btn_add" name="btn_add" value="등록" onClick="fnAdd();" />
			<input type="hidden" id="list_link_hidden" value="user_list.asp<%=list_link%>"/>
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->
