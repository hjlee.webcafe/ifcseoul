<!-- #include file = "inc/_config.asp" -->
<%
' remove cache
Response.Expires = -1
Response.ExpiresAbsolute = now() - 1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

%>
<!--#include file = "index_iframe_header.asp" -->
<%
Dim countQuery, selectQuery, num_count, list_array, page_result, t_page, query_where

// search string
query_where = ""
If Request("uid")&"" <> "" Then
	query_where = query_where & " and UID = '" & Request("uid") & "' "
End If
If Request("start_date")&"" <> "" And Request("end_date")&"" <> "" Then
	query_where = query_where & " and RESERVE_DT BETWEEN '" & Replace(Request("start_date"),"-","") & "' AND '" & Replace(Request("end_date"),"-","") & "' "
End If

// get list
set objConn = OpenDBConnection()
' 예약현황
selectQuery = " SELECT	(SELECT COUNT(*) FROM TBL_RESERVATION_INFO WHERE 1=1 " & query_where & ") AS RESERVE_CNT, "
selectQuery = selectQuery & " (SELECT COUNT(*) FROM TBL_RESERVATION_INFO WHERE RESERVE_STATUS = 'Y' " & query_where & ") AS COM_CNT, "
selectQuery = selectQuery & " (SELECT COUNT(*) FROM TBL_RESERVATION_INFO WHERE RESERVE_STATUS = 'C' " & query_where & ") AS CAN_CNT, "
selectQuery = selectQuery & " (SELECT ISNULL(SUM(T_QTY),0) FROM TBL_RESERVATION_INFO WHERE RESERVE_STATUS <> 'C' " & query_where & ") AS USE_TIME "
set objRRs = SendQuery(objConn,selectQuery)

If Not objRRs.EOF Then
	RESERVE_CNT = objRRs("RESERVE_CNT")
	COM_CNT = objRRs("COM_CNT")
	CAN_CNT = objRRs("CAN_CNT")
	USE_TIME = FnMoneySet(objRRs("USE_TIME"))
End If
set objRRs = Nothing

' 납부현황
selectQuery = " SELECT	(SELECT ISNULL(SUM(TOTAL_AMOUNT),0) FROM TBL_RESERVATION_INFO WHERE RESERVE_STATUS <> 'C' AND PAY_STATUS = 'F' " & query_where & ") AS FIELD_PAY, "
selectQuery = selectQuery & " (SELECT ISNULL(SUM(TOTAL_AMOUNT),0) FROM TBL_RESERVATION_INFO WHERE RESERVE_STATUS <> 'C' AND PAY_STATUS = 'M' " & query_where & ") AS MANAGE_PAY, "
selectQuery = selectQuery & " (SELECT ISNULL(SUM(TOTAL_AMOUNT),0) FROM TBL_RESERVATION_INFO WHERE RESERVE_STATUS <> 'C' AND PAY_STATUS = 'N' " & query_where & ") AS NON_PAY, "
selectQuery = selectQuery & " (SELECT ISNULL(SUM(CAN_AMOUNT),0) FROM TBL_RESERVATION_INFO WHERE RESERVE_STATUS = 'C' AND CAN_PAY_STATUS = 'F' " & query_where & ") AS CAN_FIELD_PAY, "
selectQuery = selectQuery & " (SELECT ISNULL(SUM(CAN_AMOUNT),0) FROM TBL_RESERVATION_INFO WHERE RESERVE_STATUS = 'C' AND CAN_PAY_STATUS = 'M' " & query_where & ") AS CAN_MANAGE_PAY, "
selectQuery = selectQuery & " (SELECT ISNULL(SUM(CAN_AMOUNT),0) FROM TBL_RESERVATION_INFO WHERE RESERVE_STATUS = 'C' AND CAN_PAY_STATUS = 'N' " & query_where & ") AS CAN_NON_PAY "
set objPRs = SendQuery(objConn,selectQuery)

If Not objPRs.EOF Then
	FIELD_PAY = FnMoneySet(objPRs("FIELD_PAY") + objPRs("CAN_FIELD_PAY"))
	MANAGE_PAY = FnMoneySet(objPRs("MANAGE_PAY") + objPRs("CAN_MANAGE_PAY"))
	NON_PAY = FnMoneySet(objPRs("NON_PAY") + objPRs("CAN_NON_PAY"))
End If
set objPRs = Nothing

' 입주사
selectQuery = " SELECT * FROM TBL_USER_INFO "
set objURs = SendQuery(objConn,selectQuery)
%>
<script type="text/javascript">
	$(document).ready(function() {
		if($("#reservation_summary").outerHeight() > 0){
			pageheight = $("#reservation_summary").outerHeight() + 50;
			$("#ifrm_summary", parent.document).attr("height", pageheight + "px");
		}

		<% If Request("start_date")&"" <> "" Then %>
			$("#start_date").val($.datepicker.formatDate('yy-mm-dd', new Date('<%=Request("start_date")%>')));
		<% End If %>
		<% If Request("end_date")&"" <> "" Then %>
			$("#end_date").val($.datepicker.formatDate('yy-mm-dd', new Date('<%=Request("end_date")%>')));
		<% End If %>
	});

	function fnSearch() {
		var startDate = $("#start_date").val();
		var endDate = $("#end_date").val();

		if ((startDate != "" && endDate == "") || (startDate == "" && endDate != "")) {
			alert("등록일 구간을 바르게 설정하여 주십시오.");
			return true;
		}
		if (startDate != "" && endDate != "") {
			var startArray = startDate.split('-');
			var endArray = endDate.split('-');
			var start_date = new Date(startArray[0], startArray[1], startArray[2]);
			var end_date = new Date(endArray[0], endArray[1], endArray[2]);
			if(start_date.getTime() > end_date.getTime()) {
				alert("종료날짜보다 시작날짜가 작아야합니다.");
				return true;
			}
		}

		document.location.href = "reservation_list_summary.asp?uid=" + $("#UID").val() + "&start_date=" + startDate + "&end_date=" + endDate;
	};

	function fnReservationExcel() {
		parent.window.open("reservation_summary_xls.asp");
	};

	function fnPayExcel() {
		parent.window.open("pay_summary_xls.asp");
	};
</script>
<div id="reservation_summary">
	<div class="search_wrap" style="margin-top:20px;">
		<div class="table_list">
			<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="회의실 예약리스트">
				<caption>회의실 예약리스트</caption>
				<colgroup>
					<col width="15%"><col width="35%"><col width="15%"><col width="35%">
				</colgroup>
				<tbody>
					<tr>
						<th>이용날짜</th>
						<td style="text-align:left">
							<input type="text" class="form-control input-sm width_150" id="start_date" name="start_date" value="<%=start_date%>" style="background-color:#fff;cursor:default;" placeholder="YYYY-MM-DD" readonly />
							<input type="text" class="form-control input-sm width_150" id="end_date" name="end_date" value="<%=end_date%>" style="background-color:#fff;cursor:default;" placeholder="YYYY-MM-DD" readonly />
						</td>
						<th>입주사</th>
						<td style="text-align:left">
							<select id="UID" name="UID" class="form-control input-sm width_200">
								<option value="" selected>선택</option>
								<%
								If Not objURs.EOF Then
									For k = 0 to objURs.RecordCount - 1
								%>
										<option value="<%=objURs("UID")%>"><%=objURs("USER_NAME")%>(<%=objURs("UID")%>)</option>
								<%
										objURs.MoveNext
									Next
								End If
								%>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<input type="hidden" id="list_link_hidden" value="<%=list_link%>" />
		<div class="btm_btn_wrap">
			<input type="button" class="btn btn-info btn-sm" id="btn_search" name="btn_search" value="검 색" onClick="fnSearch();" />
		</div>
	</div>
	<dl class="rmrWrap">
		<dt>
			예약 현황
			<input type="button" class="btn btn-info btn-sm" id="reservation_excel_down" name="reservation_excel_down" value="엑셀다운로드" onClick="fnReservationExcel();">
		</dt>
		<dd class="settingBox">
			<div class="tblStyle1">

				<table>
					<caption>예약 정보</caption>
					<colgroup>
						<col style="width:17%"><col style="width:*%">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row"><span>예약 건수</span></th>
							<td><span class="totalNum"><%=RESERVE_CNT%> <em>건</em></span></td>
						</tr>
						<tr>
							<th scope="row"><span>이용 건수</span></th>
							<td><span class="totalNum02"><%=COM_CNT%> <em>건</em></span></td>
						</tr>
						<tr>
							<th scope="row"><span>취소 건수</span></th>
							<td><span class="totalNum03"><%=CAN_CNT%> <em>건</em></span></td>
						</tr>
						<tr>
							<th scope="row"><span>총 이용시간</span></th>
							<td><span class="totalNum"><%=FnMoneySet(USE_TIME)%> <em>시간</em></span></td>
						</tr>
					</tbody>
				</table>
			</div>
		</dd>
		<dt>
			납부 현황
			<input type="button" class="btn btn-info btn-sm" id="pay_excel_down" name="pay_excel_down" value="엑셀다운로드" onClick="fnPayExcel();">
		</dt>
		<dd class="settingBox">
		<div class="tblStyle1">
				<table>
					<caption>납부 현황</caption>
					<colgroup>
						<col style="width:17%"><col style="width:*%">
					</colgroup>
					<tbody>
						<tr>
							<th scope="row"><span>현장 결제 금액</span></th>
							<td><span class="totalNum02"><%=FIELD_PAY%> <em>원</em></span></td>
						</tr>
						<tr>
							<th scope="row"><span>관리자 합산 금액</span></th>
							<td><span class="totalNum02"><%=MANAGE_PAY%> <em>원</em></span></td>
						</tr>
						<tr>
							<th scope="row"><span>납부 예정 금액</span></th>
							<td><span class="totalNum03"><%=NON_PAY%> <em>원</em></span></td>
						</tr>
					</tbody>
				</table>
			</div>
		</dd>
	</dl>
</div>
<!--#include file = "index_footer.asp" -->
