$(function() {
	// add form-control style to all input:text, textarea
	$("input[type=text]").addClass("form-control").addClass("input-sm");
	$("input[type=password]").addClass("form-control").addClass("input-sm");
	$("textarea").addClass("form-control").addClass("input-sm");

	/* index */
	$("#admin_id").focus();

	$("#admin_id").css("ime-mode","disabled");
	
	$("#admin_id").keypress(function(e) {
		if (e.which == 13)
		{
			e.stopPropagation();
			e.preventDefault();
			if (check_login("id"))
				$("#admin_pw").focus();
		}
    });
    
    $("#admin_pw").keypress(function(e) {    
	    if (e.which == 13)
	    {
	    	e.stopPropagation();
			e.preventDefault();
	    	if (check_login())
	    		login_function();
	    }
    });
	
	$("#login_btn").click(function() {
		login_function();
	});
	/* //index */
	
	/* default script */
	// td btn with a tag attr
	$(".td_btn").click(function() {
		if ($(this).find("a").length == 0)
			return;

		var link = $(this).find("a").attr("href");
		location.href = link;
	});

	// page limit select setting
	$("#page_limit_btn").change(function() {
		page_limit = $(this).val();
		list_link = $("#list_link_hidden").val();
		location.href = list_link + "&page=1&page_para=&page_limit=" + page_limit;
	});
	
	// cancel search option
	$("#cancel_search").click(function() {
		var link = location.href.split("?");
		location.href = link[0];
	});
	
	// add new btn
	$("#new_btn").click(function() {
		get_link = $("#list_link_hidden").val();
		location.href= get_link + "&mode=new";
	});
	
	// back to list btn
	$("#back_to_list").click(function() {
		get_link = $("#list_link_hidden").val();
		location.href = get_link;
	});
	
	// search button
	$("#search_btn").click(function() {
		$(this).parents("form").submit();
	})
	
	// check all of table list
	$("#select_all_list").change(function() {
		$("#table_list>tbody").find("input:checkbox").prop('checked', $("#select_all_list").prop('checked'));
	});
	
	// for data picker with jquery ui
	$.datepicker.regional['ko'] = {
		monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일', '일요일'],
		dayNamesShort: ['일', '월', '화', '수', '목', '금', '토', '일'],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토', '일'],
		dateFormat: "yy-mm-dd",
		showOtherMonths: true,
		showMonthAfterYear: true,
		selectOtherMonths: true,
		firstDay: 0,
		isRTL: false,
		yearSuffix: '년'
	};
	
	$.datepicker.setDefaults($.datepicker.regional['ko']);
	
	if ($( "#start_date" ).length > 0)
		$( "#start_date" ).datepicker(
		/*{
				showOn : "both",
				buttonImage: "images/calendar.gif",
				buttonImageOnly: true,
				buttonText: "날짜"
		}*/
		);
	
	if ($( "#end_date" ).length > 0)
		$( "#end_date" ).datepicker(
		/*{
				showOn : "both",
				buttonImage: "images/calendar.gif",
				buttonImageOnly: true,
				buttonText: "날짜"
		}*/
		);
	
	if ($( "#winner_date" ).length > 0)
		$( "#winner_date" ).datepicker({});
	
	// upload2 delete btn
	$(".delete_btn").each(function() {
		$(this).click(function() {
			if (!confirm("파일을 삭제 하시겠어요?"))
				return false;
			
			var id = $(this).attr("id").replace(/_del/, "");
			var image_path = $("#" + id + "_path").val();
			del_image_2(image_path, id);
		});
	});
	
	$(".upload_btn_2").each(function() {
		upload2($(this));
	});
	/* //default script */
	
	// upload btn
	$(".upload_btn").each(function() {
		upload($(this));
	});
	
	set_img_btn();
	
	// logout btn
	$("#logout").click(function() {
		location.href = "logout.asp"
	});

	$("#btn_userInfo").click(function() {
		location.href = "admin_list_edit.asp?uid=" + $("#hdn_uid").val();
	});

	// set current nav
	$(".left_menu").find("a").each(function() {
		var link = $(this).attr("href").replace(".asp", "").replace(/\_/, "\\_");
		var current_link = location.href;
		var reg = new RegExp(link);
		if (reg.test(current_link))
		{
			var text = $(this).html();
			$("#menu_nav").html("<li class='active'>" + text + "</li>");
			$(this).css("background-color", "gray");
		}
	})
});

/* index */
function login_function()
{
	if (!check_login())
		return false;
	
	$("#index_loading_overlay").show();
	
	var id = $("#admin_id").val();
	var passwd = hex_md5($("#admin_pw").val());
	//var id = $("#admin_pw").val();
	//console.log(passwd);

	$.ajax ({
		type : "POST",
		url : "./ajax/login_proc.asp",
		dataType : "json",
		data : ({"admin_id" : id ,"admin_pw" : passwd}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
			
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
				$("#login_pw").focus();
			}
			else
			{
				var error_msg = data.error_msg;
				switch (data.result)
				{
					case 1:
						alert("아이디를 확인 해 주세요");
						$("#admin_id").focus();
						break;
					case 2:
					case 3:
						alert("비밀번호를 확인 해 주세요");
						$("#admin_pw").focus();
						break;
					default :
						location.href = data.link;
				}
			}
		},
		error : function (data)
		{
			$("#index_loading_overlay").hide();
			alert("Error : Ajax Error");
			$("#admin_pw").focus();
		}
	});
}

function check_login(c_data)
{
	
	if ($("#admin_id").val().length == 0)
	{
		alert("아이디 입력을 확인해주세요");
		$("#admin_id").focus();
		return false;
	}

	if (typeof c_data == 'undefined' && $("#admin_pw").val().length == 0)
	{
		alert("비밀번호 입력을 확인해주세요");
		$("#admin_pw").focus();
		return false;
	}
	
	return true;
}
/* //index */

/* for upload */
function upload(obj) 
{
	var id = obj.attr("id").replace("_upload", "");

	$(".upload_btn").each(function() {
		var id = $(this).attr("id").replace("_upload", "");
		
		$("#" + id + "_upload").uploadFile({
			url:"/ajax/upload.asp",
			allowedTypes:"png,gif,jpg,jpeg",
			fileName:"myfile",
			formData:{},
			showAbort:true,
			//showDone:true,
			//showDelete: true,
			showStatusAfterSuccess: false,
			uploadButtonClass:"ajax-file-upload-green",
			returnType:"json",
			maxFileSize:1024*1024*10,
			onSuccess:function(files,data,xhr)
			{
				if (data.result == 0)
				{
					//$("#" + id).val(data.file[0]);
					var img_text = '<li class="img_wrap"><img src="' + data.file[0] + '" class="upload_img" /><span class="upload_img_del">x</span></li>';
					$(".img_section>ul").append(img_text);
					set_img_btn();
				}
				else
					alert(data.error_msg);
			},
			onError: function(files,status,errMsg)
			{
				alert(JSON.stringify(files));
			},
			deleteCallback: function(data, pd)
			{
				var file_array = data.file[0].split("/");
				var file_name = file_array[file_array.length-1];
				$.post("/ajax/delete.asp", {"op": "delete", "type": type, "mode" : mode, "name": file_name},
						function (resp, textStatus, jqXHR)
						{
							result = JSON.parse(resp);
							if (result.result == 0)
							{
								$("img.upload_img").each(function() {
									if ($(this).attr("src") == data.file[0])
									{
										$(this).parent().remove();
									}
								});
							}
							else
							{
								alert(result.error_msg);
							}
						});
				pd.statusbar.hide();
			}
		});
	});
}

function upload2(obj) 
{
	var id = obj.attr("id").replace("_upload", "");
	
	$("#" + id + "_upload").uploadFile({
		url:"/ajax/upload.asp",
		allowedTypes:"png,gif,jpg,jpeg",
		fileName:"myfile",
		formData:{},
		showAbort:true,
		//showDone:true,
		//showDelete: true,
		showStatusAfterSuccess: false,
		uploadButtonClass:"ajax-file-upload-green",
		returnType:"json",
		maxFileSize:1024*1024*10,
		onSuccess:function(files,data,xhr)
		{	
			
			if (data.result == 0)
				$("#" + id).val(data.file[0]);
			else
				alert(data.error_msg);
		},
		onError: function(files,status,errMsg)
		{
			alert(JSON.stringify(files));
		},
		deleteCallback: function(data, pd)
		{
			var file_array = data.file[0].split("/");
			var file_name = file_array[file_array.length-1];
			
			$.post("/ajax/delete.asp", {"op": "delete", "type": type, "mode" : mode, "name": file_name},
					function (resp, textStatus, jqXHR)
					{
						result = JSON.parse(resp);
						if (result.result == 0)
						{
							$("#" + id).val("");
						}
						else
						{
							alert(result.error_msg);
						}
					});
			pd.statusbar.hide();
		}
	});
	
	if ($("#" + id).val().length > 0)
	{
		var file_path = $("#" + id).val();
		file_array = file_path.split("/");
		$("#" + id).parent().append($(".delete_img_hidden").html());
		$("#" + id).parent().find(".image_name").attr("id", id + "_name").html(file_array[file_array.length-1]);
		$("#" + id).parent().find(".image_path").attr("id", id + "_path").val(file_path);
		$("#" + id).parent().find(".delete_btn").attr("id", id + "_del");
	}
}

function set_img_btn()
{
	$(".upload_img").unbind("click")
	.click(function() {
		var img_src = $(this).attr("src");
		var img = '<img src="' + img_src + '" />';
		var id = $(this).parents(".img_section").find(".ckeditor_img").val().replace("_img", "");
		CKEDITOR.instances[id].insertHtml(img);
	});
	
	$(".upload_img_del").unbind("click")
	.click(function() {
		var img_src = $(this).parent().find("img").attr("src");
		if (!confirm("파일을 삭제 하시겠어요?"))
			return false;
		
		del_image(img_src, $(this));
	})
}


function del_image(image_path, obj)
{
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	obj.parent().remove();
	
	/*
$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "/ajax/delete.php",
		dataType : "json",
		data : ({
					"op" : "delete",
					"path" : image_path
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					alert("파일을 삭제했어요");
					obj.parent().remove();
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
*/
}

function del_image_2(image_path, id)
{
	if ($("#index_loading_overlay").css("display") != 'none')
	{
		alert("입력 중 입니다");
		return false;
	}
	
	if ($("#" + id + "_path").val() == $("#" + id).val())
		$("#" + id).val("");
	$("#" + id + "_path").val("");
	$("#" + id + "_name").html("").parent().addClass("hide_row");
	$("#" + id + "_preview").remove();
	
	/*
$("#index_loading_overlay").show();
	
	$.ajax ({
		type : "POST",
		url : "/ajax/delete.php",
		dataType : "json",
		data : ({
					"op" : "delete",
					"path" : image_path
				}),
		success : function (data)
		{
			$("#index_loading_overlay").hide();
		
			if (typeof data != 'object')
			{
				alert("Error :: Ajax Return Error");
			}
			else
			{
				var error_msg = data.error_msg;
				if (data.result == 0)
				{
					alert("파일을 삭제했어요");
					if ($("#" + id + "_path").val() == $("#" + id).val())
						$("#" + id).val("");
					$("#" + id + "_path").val("");
					$("#" + id + "_name").html("").parent().addClass("hide_row");
					$("#" + id + "_preview").remove();
				}
				else
				{
					alert(error_msg);
				}
			}
		},
		error : function (data)
		{
			alert("Error : Ajax Error");
			$("#index_loading_overlay").hide();
		}
	});
*/
}

/* //for upload */