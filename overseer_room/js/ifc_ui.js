(function () {
    var Class = {
        winHeight: 0
        , popZIndex: 5000
		, winWidth: 0
		, isMobile: navigator.userAgent.match(/Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/i) ? true : false
		, isWide: false
		, evTouchStart: navigator.userAgent.match(/Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/i) ? 'touchstart' : 'mousedown'
		, evTouchMove: navigator.userAgent.match(/Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/i) ? 'touchmove' : 'mousemove'
		, evTouchEnd: navigator.userAgent.match(/Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/i) ? 'touchend' : 'mouseup'
		, animEndEventName: {
		    'WebkitAnimation': 'webkitAnimationEnd',
		    'OAnimation': 'oAnimationEnd',
		    'msAnimation': 'MSAnimationEnd',
		    'animation': 'animationend'
		}[Modernizr.prefixed('animation')]
        /* debug */, debug: function debug(str) {
            var $debug = $('#debug');
            if ($debug.length < 1) {
                $debug = $('<div id="debug"></div>');
                $('body').append($debug);
            }
            $debug.append('<span>' + str + '</span>').scrollTop(1000000);
        }
        /* 브라우저 체크 */, initBrowserOnce: function initBrowserOnce(a, z) {
            a = navigator.userAgent;
            var u = 'unknown'
				, x = 'X'
				, m = function (r, h) {
				    for (var i = 0; i < h.length; i = i + 1) {
				        r = r.replace(h[i][0], h[i][1]);
				    }
				    return r;
				}
				, c = function (i, a, b, c) {
				    var r = { name: m((a.exec(i) || [u, u])[1], b) };
				    r[r.name] = true;
				    r.version = (c.exec(i) || [x, x, x, x])[3];
				    if (r.name.match(/safari/) && r.version > 400) {
				        r.version = '2.0';
				    }
				    if (r.name === 'presto') {
				        r.version = ($.browser.version > 9.27) ? 'futhark' : 'linear_b';
				    }
				    r.versionNumber = parseFloat(r.version) || 0;
				    r.versionX = (r.version !== x) ? (r.version + '').substr(0, 1) : x; r.className = r.name + r.versionX;
				    return r;
				};
            a = (a.match(/Opera|Navigator|Minefield|KHTML|Chrome/) ? m(a, [[/(Firefox|MSIE|KHTML,\slike\sGecko|Konqueror)/, ''], ['Chrome Safari', 'Chrome'], ['KHTML', 'Konqueror'], ['Minefield', 'Firefox'], ['Navigator', 'Netscape']]) : a).toLowerCase();
            $.browser = $.extend((!z) ? $.browser : {}, c(a, /(camino|chrome|firefox|netscape|konqueror|lynx|msie|opera|safari)/, [], /(camino|chrome|firefox|netscape|netscape6|opera|version|konqueror|lynx|msie|safari)(\/|\s)([a-z0-9\.\+]*?)(;|dev|rel|\s|$)/));
            $.layout = c(a, /(gecko|konqueror|msie|opera|webkit)/, [['konqueror', 'khtml'], ['msie', 'trident'], ['opera', 'presto']], /(applewebkit|rv|konqueror|msie)(:|\/|\s)([a-z0-9\.]*?)(;|\)|\s)/);
            $.os = { name: (/(win|mac|linux|sunos|solaris|iphone)/.exec(navigator.platform.toLowerCase()) || [u])[0].replace('sunos', 'solaris') };
            if (!z) {
                $('html').addClass([$.os.name, $.browser.name, $.browser.className, $.layout.name, $.layout.className].join(' '));
            }

        }
        /* placeholder 세팅 */, initPlaceholder: function initPlaceholder() {
            $(this).find('input[placeholder], textarea[placeholder]').placeholder();
        }
        /* body에 브라우저 CSS 추가 */, initBodyCssOnce: function initBodyCssOnce() {
            //browser
            if ($.browser.name == 'msie') {
                //$('body').addClass($.browser.name);
            } else {
                $('body').addClass($.browser.name);
            }

            //Agent
            if ((/Chrome/i).test(navigator.userAgent)) {
                $('html').addClass('Chrome');
            } else if ((/Android/i).test(navigator.userAgent)) {
                $('html').addClass('Android');
            } else if ((/iPad|iPhone|iPod/i).test(navigator.userAgent)) {
                $('html').addClass('iOS');
            }
        }
        /* snbTree */, initSnbTreeOnce: function initSnbTreeOnce() {
            var $li = $('.menuAllList li');
            var $liA = $('.menuAllList li > a');
            var $ul = $('.menuAllList li ul');

            $liA.click(function (event) {
                if (this == event.target) {
                    if ($(this).parent('li').children('ul').is(':hidden')) {
                        $(this).parent('li').children('ul').slideDown();
                        $(this).parent('li').siblings('li').children('ul').slideUp();
                        $(this).parent('li').siblings('li').children('ul').children('li').children('ul').slideUp();
                    }
                    else {
                        $(this).parent('li').children('ul').slideUp();
                        $(this).parent('li').siblings('li').children('ul').slideUp();
                    }
                }
               // return false;
            });
            var $link = $('.menuList > li > a, .menuAllList li a');
            $link.click(function () {
                $link.removeClass('on');
                $(this).addClass('on');
                $(this).parent().parent().siblings('a').addClass('on');
                $(this).parent().parent().parent().parent().siblings('a').addClass('on');
            });

        }
        /* navSnb */, initNavSnbOnce: function initNavSnbOnce() {
            $(document)
				.on('show', 'nav#snb', function () {
				    $('html').removeClass('snbOff');
				    $('html').addClass('snbView');
				    $('a.btnMenu').removeClass('jsNavSnb');
				    //$('a.btnMenu').addClass('jsNavSnbClose');
				})
				.on('hide', 'nav#snb', function () {
				    $('html').removeClass('snbView');
				    $('html').addClass('snbOff');
				    //$('a.btnMenu').removeClass('jsNavSnbClose');
				    $('a.btnMenu').addClass('jsNavSnb');
				})
				.on(Class.evTouchStart, 'a.jsNavSnb', function (e) {
				    if (!$('html').hasClass('snbView')) {
				        $('nav#snb').trigger('show');
				        e.stopPropagation();
				    }
				    e.preventDefault();
				})
                .on('click', 'a.jsNavSnb', function (e) {
                    if (!$('html').hasClass('snbView')) {
                        $('nav#snb').trigger('show');
                        e.stopPropagation();
                    }
                    e.preventDefault();
                })
                
                .on('click', 'a.jsNavSnbClose', function (e) {
                    $('nav#snb').trigger('hide');
                })
				.on(Class.evTouchStart, '#containerWrap', function (e) {
				    $('nav#snb').trigger('hide');
				})

        }
        /* tab 세팅 */, initTabOnce: function initTabOnce() {
            $(document)
                    .on('click', 'ul.tabs>li', function (e) {
                        var $current = $(this);
                        var index = $current.index();
                        var $panels = $current.parent().siblings('.panels');

                        var cls = $current.attr('class').match(/tc[0-9]*/)[0];

                        $current.addClass(cls + '-selected').siblings().removeClass(cls + '-selected');
                        var $target = $panels.find('>.' + cls + '-panel:eq(' + index + ')');
                        $target.addClass(cls + '-selected').siblings().removeClass(cls + '-selected');
                        $target.find('.titleWrap').trigger('ellipsis', true);

                        var $lyPopWrap = $current.parents('.lyPopWrap');
                        if ($lyPopWrap.length > 0) {
                            $lyPopWrap.trigger('reposition');
                        }

                        e.stopPropagation();
                    });
        }
        /* history 세팅 */, initHistoryOnce: function initHistoryOnce() {
            $(document)
            .on('marginAin', '.historyArea', function (e, idx) {
                $obj = $(this);
                if (typeof idx === 'undefined') {
                    if (typeof $obj.data('idx') === 'undefined') {
                        idx = 0;
                    } else {
                        idx = $obj.data('idx');
                    }
                }

                $obj.data('idx', idx);
                var $historyArea = $('.historyArea'),
                    $y1H = $historyArea.find('.y1').outerHeight(),
                    $y2H = $historyArea.find('.y2').outerHeight(),
                    $y3H = $historyArea.find('.y3').outerHeight(),
                    $y4H = $historyArea.find('.y4').outerHeight(),
                    $y5H = $historyArea.find('.y5').outerHeight(),
                    $y6H = $historyArea.find('.y6').outerHeight(),
                    $y7H = $historyArea.find('.y7').outerHeight(),
                    $y8H = $historyArea.find('.y8').outerHeight(),
                    $y9H = $historyArea.find('.y9').outerHeight(),
                    $y10H = $historyArea.find('.y10').outerHeight(),
                    $sum1 = $y1H + $y2H + $y3H + $y4H + $y5H,
                    $sum2 = $y1H + $y2H + $y3H + $y4H + $y5H + $y6H + $y7H + $y8H + $y9H + $y10H;

                if (idx == 0) {
                    $('.historyArea').animate({ marginTop: -35 });
                } else if (idx == 1) {
                    $('.historyArea').animate({ marginTop: -($sum1 + 35) });
                } else {
                    $('.historyArea').animate({ marginTop: -($sum2 + 35) });
                }
            })
            .on('click', '.tabArea li a, .tabList li a', function (e) {
                $('.historyArea').trigger('marginAin', $(this).parent('li').index());
                var $obj = $('.tabArea li'),
                $objM = $('.tabList li');

                $obj.each(function (index) {
                    $obj.eq(index).click(function (e) {
                        $obj.removeClass('on');
                        $obj.eq(index).addClass('on');
                        $objM.removeClass('on');
                        $objM.eq(index).addClass('on');
                    });
                });
                $objM.each(function (index) {
                    $objM.eq(index).click(function (e) {
                        $obj.removeClass('on');
                        $obj.eq(index).addClass('on');
                        $objM.removeClass('on');
                        $objM.eq(index).addClass('on');
                    });
                });
            });
            //var resizeTimer = null;
            //$(window)
            //    .on('resize', function () {
            //        clearTimeout(resizeTimer);
            //        resizeTimer = setTimeout(function () {
            //            $('.historyArea').trigger('marginAin');
            //        }, 500);
            //    });
        }
        /* h2Siblings */, initH2SiblingsOnce: function initH2SiblingsOnce() {
            $(document)
				.on('show', '.h2Siblings', function (e) {
					var $obj = $(this);
				    $('html').addClass('h2SiblingsShow');
					//$obj.css({'display':'block'});
					$obj.siblings('.titleWrap h2').addClass('on'); 
					e.stopPropagation();
				})
				.on('hide', '.h2Siblings', function (e) {
					var $obj = $(this);
				    $('html').removeClass('h2SiblingsShow');
					//$obj.css({'display':'none'});
				    $obj.siblings('.titleWrap h2').removeClass('on');
					e.stopPropagation();
				})
                .on('click', '.mView .titleWrap h2 a', function (e) {
					if($('html').hasClass('h2SiblingsShow')){
					 $('.h2Siblings').trigger('hide');
					} else{
					$('.h2Siblings').trigger('show');
					}
                })
        }
        /* h3Siblings */, initH3SiblingsOnce: function initH3SiblingsOnce() {
            $(document)
				.on('show', '.h3Siblings', function (e) {
				    var $obj = $(this);
				    $('html').addClass('h3SiblingsShow');
				    //$obj.css({ 'display': 'block' });
				    $obj.siblings('.titleWrap h3').addClass('on');
				    e.stopPropagation();
				})
				.on('hide', '.h3Siblings', function (e) {
				    var $obj = $(this);
				    $('html').removeClass('h3SiblingsShow');
				    //$obj.css({ 'display': 'none' });
				    $obj.siblings('.titleWrap h3').removeClass('on');
				    e.stopPropagation();
				})
                .on('click', '.mView .titleWrap h3 a', function (e) {
                    if ($('html').hasClass('h3SiblingsShow')) {
                        $('.h3Siblings').trigger('hide');
                    } else {
                        $('.h3Siblings').trigger('show');
                    }
                })
        }
        /* PlacePos*/, initPlacePosOnce: function initPlacePosOnce() {
            $(document)
            .on('show', '.placePosWrap > li > a', function (e) {
                $('.placePosWrap > li').removeClass('on');
                $(this).parent('li').addClass('on');
                
                e.stopPropagation();
            })
            .on('hide', '.placePosWrap > li > a', function (e) {
                var $obj = $(this);
                $('.placePosWrap > li').removeClass('on');
                e.stopPropagation();
            })
            .on('click', '.placePosWrap > li > a', function () {
                var $obj = $(this);
                $('.lyPopWrap').hide();
                if ($obj.parent('li').hasClass('on')) {
                    $obj.trigger('hide');
                } else {
                    $obj.trigger('show');
                }
                
            })
        }
        /* 레이어 팝업 버튼 세팅 */, initLayerPopupOnce: function initLayerPopupOnce() {
            $(document)
				.on('click', 'a.jsbtnLyp', function (e) {
				    var popID = $(this).attr('rel');
				    var popURL = $(this).attr('href');
				    var width = popURL.match(/w=([0-9]+)/);

				    if (width.length > 1) {
				        var popWidth = width[1];

				        Class.layerPopupOpen(popID, popWidth);
				    }
				    e.preventDefault();
				    e.stopPropagation();
				})
				.on('click', '.lyPopWrap .jsPopClose', function (e) {
				    $(this).parents('.lyPopWrap').trigger('closePopup');
				    e.preventDefault();
				})
				.on('closePopup', '.lyPopWrap', function (e) {
				    $(this).hide();
				    $('#popFade').hide();
				    $('.placePosWrap > li > a').trigger('hide');
				    if ($('.lyPopWrap').length < 1)
				        $('html').removeClass('hidden');

				    e.stopPropagation();
				});
        }
        /* Layer Popup 열기 */, layerPopupOpen: function layerPopOpen(id, width) {
            var $popObj = $('#' + id);
            $('body').append($popObj);

            $popObj.show().css({ 'width': Number(width) }); //Number(width)

            var popMargTop = ($popObj.height()) / 2;
            var popMargLeft = ($popObj.width()) / 2;

            $popObj.css({
                'margin-top': -popMargTop,
                'margin-left': -popMargLeft
            });

            var $wrap = $('body');

            if ($wrap.find('#popFade').length < 1) {
                $wrap.append('<div id="popFade"></div>')
            }
            $('#popFade').show();
            $('html').addClass('hidden');
        }
        /* 팝업 위치 조정 */, repositionPopup: function repositionPopup() {
            var $popCont = $(this);

            if ($popCont.length > 0) {
                var $popInner = $popCont.find('.popInner');

                var popContWidth = $popInner.outerWidth() + 60;
                var popContHeight = $popInner.outerHeight() + 55;
                if (popContHeight > Class.winHeight - 50) {
                    popContHeight = Class.winHeight - 50;
                    $popCont.css({ height: popContHeight - 55, overflowY: 'scroll', paddingRight: '16px' });
                } else {
                    $popCont.css({ height: popContHeight - 55, overflowY: 'hidden', paddingRight: 30 });
                }

                var left = (Class.winWidth - popContWidth) / 2;
                var top = (Class.winHeight - popContHeight) / 2;

                $popCont.css({ left: left, top: top });
            }
        }
        /* 레이아웃 세팅 */, initLayoutOnce: function initLayoutOnce() {
            $(document).on('setLayout', function () {
                var $wrap = $('#wrap'),
                    $header = $('header'),
                    $containerWrap = $('#containerWrap'),
                    $container = $('#container'),
                    $content = $('#content'),
                    $contWrap = $('#contWrap');

            });
        }
        // 항상 마지막에
        /* 스크린 크기 변경시 */, initResizeOnce: function initResizeOnce() {
            var resizeTimer = null;
            var tempTimer = null;
            $(window)
                .on('resize', function () {
                    var winWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
                    var winHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
                    if (winWidth != Class.winWidth || winHeight != Class.winHeight) {
                        Class.winWidth = winWidth;
                        Class.winHeight = winHeight;
                        if (Class.winWidth < 768) {
                            $('html').removeClass('wView').removeClass('tView').addClass('mView');
                            Class.isWide = true;
                        } else if (Class.winWidth >= 768 && Class.winWidth <= 1024) {
                            $('html').removeClass('wView').removeClass('mView').addClass('tView');
                            Class.isWide = false;
                        } else {
                            $('html').removeClass('mView').removeClass('tView').addClass('wView');
                            Class.isWide = false;
                        }

                        clearTimeout(resizeTimer);
                        resizeTimer = setTimeout(function () {
                            $('.historyArea').trigger('marginAin');
                        }, 500);
                        
                        //console.log('resize no ie8');
                        $('nav#snb').trigger('hide');
                        $('.h2Siblings').trigger('hide');
                        $('.h3Siblings').trigger('hide');
                        $(document).trigger('setLayout');
                        $('#popFade').hide();
                        $('.lyPopWrap').hide();
                        //$('.lyPopWrap').trigger('closePopup');
                        $('.placePosWrap > li > a').trigger('hide');
                        //$('.box.col7 .mainSectionCont p').css({ width: $('.box.col7').outerWidth() -50 })
						//$('.jsSlider').trigger('touchSlider');
						//console.log('touchSlider')
						clearTimeout(tempTimer);
						if ($(".sliderhistory").length > 0)
						{
							tempTimer = setTimeout(function () {
	                            $(".bx-viewport").find("li").width($(".bx-viewport").css("width"));
	                        }, 300);	
						}
                    }
                })
            .trigger('resize');

           
				
        }
        /* ifcUI 초기화 */, init: function () {
            for (var func in Class) {
                if (Class.hasOwnProperty(func)) {
                    if (func !== 'init' && func.indexOf('init') == 0) {
                        var $document = $(document);
                        if (func.lastIndexOf('Once') + 4 == func.length && !$document.data(func)) {
                            $document.data(func, true);
                            Class[func].call(this);
                        } else if (func.lastIndexOf('Once') + 4 != func.length) {
                            Class[func].call(this);
                        }

                    }
                }
            }
        }

    };

    if (typeof this['ifcUI'] === 'undefined') {
        this['ifcUI'] = Class;
    }
})();


$.fn.ifcUI = ifcUI.init;
$(function () {
    $(document).ifcUI();
});

$(window).load(function() {
	if ($(".sliderhistory").length > 0)
	{
		setTimeout(function () {
            $(".bx-viewport").find("li").width($(".bx-viewport").css("width"));
        }, 100);	
	}
})

$(function () {
    $('.tempArea li > a').bind("click touchstart", function (e) {

        var $obj = $(this);
        var $img = $obj.data("url");
        var $alt = $obj.find('img').attr('alt');
        var $previewImg = $('.previewImg');
        var $previewImgWrap = $('.previewImgWrap');

        $img = typeof $img === "undefined" ? false : $img;

        $previewImg.attr("src", $img).attr('alt', $alt);
        $previewImgWrap.find('.previewImg').attr("src", $img).attr('alt', $alt);

        e.preventDefault();
    });

    if ($('.floorPlansWrap').length > 0) {
        var $floorPlansArea = $('.floorPlansArea'),
            $plansMap1 = $('.plansMap1 a, .plansMap2 a, .plansMap3 a'),
            $plansMap4 = $('.plansMap4 a'),
            $plansMap5 = $('.plansMap5 a'),
            $itemWrap01 = $('.itemWrap01'),
            $itemWrap02 = $('.itemWrap02'),
            $itemWrap03 = $('.itemWrap03'),
            $floorPlansBtn = $('.floorPlansBtn'),
            $floorPlansBtnA = $('.floorPlansBtn a');


        $floorPlansBtnA.click(function () {
            $(this).parent('li').siblings('li').removeClass('on');
            $(this).parent('li').addClass('on');
        });

        $itemWrap01.addClass('on');

        $plansMap1.click(function () {
            $(this).parent('li').siblings('li').removeClass('on');
            $plansMap1.parent('li').addClass('on');
            $itemWrap01.addClass('on');
            $itemWrap02.removeClass('on');
            $itemWrap03.removeClass('on');
            $('.IfcOfficeTowers, .IfcMall, .ConradSeoulHotel').addClass('hide');
            $('.IfcOfficeTowers').removeClass('hide');
            $floorPlansBtn.children().children('li').removeClass('on');
            $itemWrap01.find($floorPlansBtn).find('li').first('li').addClass('on');
        });

        $plansMap4.click(function () {
            $(this).parent('li').siblings('li').removeClass('on');
            $plansMap4.parent('li').addClass('on');
            $itemWrap01.removeClass('on');
            $itemWrap02.addClass('on');
            $itemWrap03.removeClass('on');
            $('.IfcOfficeTowers, .IfcMall, .ConradSeoulHotel').addClass('hide');
            $('.IfcMall').removeClass('hide');
            $floorPlansBtn.children().children('li').removeClass('on');
            $itemWrap02.find($floorPlansBtn).find('li').first('li').addClass('on');
        });
        
        $plansMap5.click(function () {
            $(this).parent('li').siblings('li').removeClass('on');
            $plansMap5.parent('li').addClass('on');
            $itemWrap01.removeClass('on');
            $itemWrap02.removeClass('on');
            $itemWrap03.addClass('on');
            $('.IfcOfficeTowers, .IfcMall, .ConradSeoulHotel').addClass('hide');
            $('.ConradSeoulHotel').removeClass('hide');
            $floorPlansBtn.children().children('li').removeClass('on');
            $itemWrap03.find($floorPlansBtn).find('li').first('li').addClass('on');
        });

        //if ($current.hasClass('.page1')) {
        //    $('.IfcOfficeTowers').addClass('hide');
        //    $('.IfcMall').removeClass('hide');
        //    $('.ConradSeoulHotel').addClass('hide');
        //} else if ($current.hasClass('.page2')) {
        //    $('.IfcOfficeTowers').addClass('hide');
        //    $('.IfcMall').removeClass('hide');
        //    $('.ConradSeoulHotel').addClass('hide');
        //} else if ($current.hasClass('.page3')) {
        //    $('.IfcOfficeTowers').addClass('hide');
        //    $('.IfcMall').addClass('hide');
        //    $('.ConradSeoulHotel').removeClass('hide');
        //}
    }
		//마우스 오버시 빌딩 각 층수 이미지
		$('.floorPlansBtn').find('a').on('mouseenter',function(){
			var $idx = $(this).parent('li').index();
			$('.floorPlansBtn').find('li').removeClass('on')
			$(this).parent('li').addClass('on')
			$(this).parents('.floorPlansInfo').find('.PlansMoreImg').children('span').hide();
			$(this).parents('.floorPlansInfo').find('.PlansMoreImg').children('span').eq($idx).show();
		})

})

//basicLayer 레이어
function toggleLayer(id) {
	var objDisplay = document.getElementById(id).style.display;
	if (objDisplay == 'block') {
		$('.basicLayer').hide();
		document.getElementById(id).style.display = 'none';
	} else {
		$('.basicLayer').hide();
		document.getElementById(id).style.display = 'block';
	}
}

//메인 갤러리 전환
$(function () {
	$('.gallNavi li a').each(function (index) {
		$('.gallNavi li a').eq(index).click(function (e) {
			e.preventDefault();
			$('.viewGall').removeClass('viewOn');
			$('.viewGall').eq(index).addClass('viewOn');
		});
	});
});