<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>:::IFC 관리자:::</title>
	<link rel="stylesheet" type="text/css" href="css/base.css" />
	<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.placeholder.js"></script>
	<script type="text/javascript" src="js/md5.js"></script>
	<script type="text/javascript" src="js/admin_script.js"></script>
	<script type="text/javascript" src="js/admin_script_temp.js"></script>
	<script type="text/javascript" src="js/gallary_script.js"></script>

	<!-- for bootstrap -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<!-- link rel="stylesheet" href="/css/bootstrap-theme.min.css" / -->
	<!-- //for bootstrap -->

	<!-- for jqueryui -->
	<script type="text/javascript" src="js/jquery-ui-1.10.4.min.js"></script>
	<link rel="stylesheet" href="/css/jquery-ui-1.10.4.min.css" />
	<!-- //for jqueryui -->

	<!-- for upload -->
	<script type="text/javascript" src="js/jquery.uploadfile.min.js"></script>
	<link rel="stylesheet" href="/css/uploadfile.css" />
	<!-- //for upload -->

	<!-- for ckeditor -->
	<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
	<!-- //for ckeditor -->

	<!-- for placeholder -->
	<script type="text/javascript" src="js/placeholders.min.js"></script>
	<!-- //for placeholder -->

    <!-- Select2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <!-- //Select2 -->

	<script type="text/javascript">
		$(document).ready(function() {
			setInterval("getNowDateTime()",1000);
		});

		function getNowDateTime() {
			// now datetime
			var now = new Date();
			var year = now.getFullYear();
			var month = now.getMonth() +1;
			var day = now.getDate();
			var hour = now.getHours();
			var minute = now.getMinutes();
			var second = now.getSeconds();
			if (String(month).length < 2) {
				month = "0" + String(month);
			}
			if (String(day).length < 2) {
				day = "0" + String(day);
			}
			if (String(hour).length < 2) {
				hour = "0" + String(hour);
			}
			if (String(minute).length < 2) {
				minute = "0" + String(minute);
			}
			if (String(second).length < 2) {
				second = "0" + String(second);
			}
			$("#spn_datetime").text(year + "." + month + "." + day + " " + hour + ":" + minute + ":" + second);
		}
	</script>
</head>
<body>
<div id="index_loading_overlay">
	<div class="index_loading_overlay_img">
		<img src="img/ajax-loader.gif" alt="overlay loading img" />
	</div>
</div>
<div class="wrap">
	<!-- top -->
	<div class="top">
		<ul>
			<li>
				<img src="img/logo.png" height="60px" /> <h3>The Forum at IFC Administrator</h3>
			</li>
			<li class="text-right">
				<input type="hidden" id="hdn_uid" value="<%=strAdminId%>" />
				<span id="spn_datetime" class="mr20"></span><br>
				<span id="spn_name" class="mr10"><span style="color:#357ebd;font-weight:bold;"><%=strAdminName%></span>님 환영합니다.</span><button type="button" id="btn_userInfo" class="btn btn-xm mr10">정보수정</button><button type="button" id="logout" class="btn btn-xm mr10">LOGOUT</button>
			</li>
		</ul>
	</div>
	<!-- //top -->
	<div class="content_wrap">
		<!-- side menu -->
		<div class="left_menu">
			<ul>
				<li class="menu_title">
					Admin Menu
				</li>
				<% If strAdminAuth&"" = "S" Then %>
					<li>
						<a href="admin_list.asp">관리자 계정관리</a>
					</li>
					<li>
						<a href="user_list.asp">입주사 계정관리</a>
					</li>
				<% End If %>
				<li>
					<a href="reservation_list.asp">회의실 예약관리</a>
				</li>
				<li>
					<a href="reservation_new.asp">회의실 예약하기</a>
				</li>
				<li>
					<a href="reservation_schedule.asp">회의실 예약현황</a>
				</li>
				<% If strAdminAuth&"" = "S" And False Then %>
					<li>
						<a href="user_pay_list.asp">정산 현황</a>
					</li>
				<% End If %>
				<li>
					<a href="reservation_report.asp?y=<%=YEAR(DATE())%>">회의실 리포트</a>
				</li>
				<li>
					<a href="mail_report.asp">메일 발송현황</a>
				</li>
			</ul>
		</div>
		<!-- //side menu -->
		<!-- contents -->
		<div class="contents">
			<ol id="menu_nav" class="breadcrumb"></ol>
