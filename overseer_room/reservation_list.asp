<!--#include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%
Dim countQuery, selectQuery, num_count, list_array, page_result, t_page, query_where

// search string
query_where = ""
If Request("search_text")&"" <> "" Then
    query_where = query_where & " and " & Request("search_type") & " Like '%" & GetTag2Text(Request("search_text")) & "%' "
End If
If Request("room_type")&"" <> "" Then
    query_where = query_where & " and LEFT(RESERVE_NO,1) = '" & Request("room_type") & "' "
End If
If Request("reserve_status")&"" <> "" Then
    query_where = query_where & " and RESERVE_STATUS = '" & Request("reserve_status") & "' "
End If
If Request("pay_status")&"" <> "" Then
    If Request("reserve_status")&"" = "C" Then ' 예약취소이면 취소지불상태를 검색
        query_where = query_where & " and CAN_PAY_STATUS = '" & Request("pay_status") & "' "
    Else
        query_where = query_where & " and PAY_STATUS = '" & Request("pay_status") & "' "
    End If
End If
If Request("chk_status")&"" <> "" Then
    query_where = query_where & " and CHK_STATUS = '" & Request("chk_status") & "' "
End If
If Request("start_date")&"" <> "" And Request("end_date")&"" <> "" Then
    query_where = query_where & " and RESERVE_DT BETWEEN '" & Replace(Request("start_date"),"-","") & "' AND '" & Replace(Request("end_date"),"-","") & "' "
End If

// counting list
countQuery = "SELECT COUNT(*) AS COUNT FROM dbo.TBL_RESERVATION_INFO WHERE 1=1 "& query_where

set objConn = OpenDBConnection()
set objRs = SendQuery(objConn,countQuery)
total_count = objRs("count")
num_count = total_count - (page - 1) * page_limit
set objRs = Nothing

// get list
selectQuery = " SELECT TOP " & page_limit & " RESERVE_NO, ROOM_NAMES, RESERVE_DT, (SELECT MIN(CONVERT(INT,RESERVE_TIME)) FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MIN_TIME, (SELECT MAX(CONVERT(INT,RESERVE_TIME))+1 FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MAX_TIME, T_QTY, RESERVE_NAME, USER_NAME, TOTAL_AMOUNT, REG_DT, RESERVE_STATUS, PAY_STATUS, CAN_PAY_STATUS, CAN_AMOUNT, CHK_STATUS, RESERVE_TIMES "
selectQuery = selectQuery & " FROM dbo.TBL_RESERVATION_INFO RI WHERE 1=1 AND RESERVE_ID NOT IN (SELECT TOP " & current_count & " RESERVE_ID FROM dbo.TBL_RESERVATION_INFO WHERE 1=1 "& query_where &" ORDER BY REG_DT DESC) "& query_where & " ORDER BY REG_DT DESC "

set objRs = SendQuery(objConn,selectQuery)

'page_create(p_total_count, p_page, p_page_limit, p_page_length)
t_page = page
page_result = page_create(total_count, t_page, page_limit, page_length)
%>
<script type="text/javascript">
    $(document).ready(function() {
        if($("#reservation_list").outerHeight() > 0){
            pageheight = $("#reservation_list").outerHeight() + 100;
            $("#ifrm_list", parent.document).attr("height", pageheight + "px");
        }

        <% If Request("start_date")&"" <> "" Then %>
            $("#start_date").val($.datepicker.formatDate('yy-mm-dd', new Date('<%=Request("start_date")%>')));
        <% End If %>
        <% If Request("end_date")&"" <> "" Then %>
            $("#end_date").val($.datepicker.formatDate('yy-mm-dd', new Date('<%=Request("end_date")%>')));
        <% End If %>
    });

    function fnSearch() {
        var startDate = $("#start_date").val();
        var endDate = $("#end_date").val();

        if ((startDate != "" && endDate == "") || (startDate == "" && endDate != "")) {
            alert("등록일 구간을 바르게 설정하여 주십시오.");
            return true;
        }
        if (startDate != "" && endDate != "") {
            var startArray = startDate.split('-');
            var endArray = endDate.split('-');
            var start_date = new Date(startArray[0], startArray[1], startArray[2]);
            var end_date = new Date(endArray[0], endArray[1], endArray[2]);
            if(start_date.getTime() > end_date.getTime()) {
                alert("종료날짜보다 시작날짜가 작아야합니다.");
                return true;
            }
        }

        document.location.href = "reservation_list.asp?search_type=" + $("#search_type").val() + "&search_text=" + $("#search_text").val() + "&room_type=" + $("#ROOM_TYPE").val() + "&reserve_status=" + $("#RESERVE_STATUS").val() + "&pay_status=" + $("#PAY_STATUS").val() + "&chk_status=" + $("#CHK_STATUS").val() + "&start_date=" + startDate + "&end_date=" + endDate;
    };

    function fnReservationExcel() {
        var startDate = $("#start_date").val();
        var endDate = $("#end_date").val();

        if ((startDate != "" && endDate == "") || (startDate == "" && endDate != "")) {
            alert("등록일 구간을 바르게 설정하여 주십시오.");
            return true;
        }
        if (startDate != "" && endDate != "") {
            var startArray = startDate.split('-');
            var endArray = endDate.split('-');
            var start_date = new Date(startArray[0], startArray[1], startArray[2]);
            var end_date = new Date(endArray[0], endArray[1], endArray[2]);
            if(start_date.getTime() > end_date.getTime()) {
                alert("종료날짜보다 시작날짜가 작아야합니다.");
                return true;
            }
        }

        parent.window.open("reservation_list_xls.asp?search_type=" + $("#search_type").val() + "&search_text=" + escape($("#search_text").val()) + "&room_type=" + $("#ROOM_TYPE").val() + "&reserve_status=" + $("#RESERVE_STATUS").val() + "&pay_status=" + $("#PAY_STATUS").val() + "&chk_status=" + $("#CHK_STATUS").val() + "&start_date=" + startDate + "&end_date=" + endDate);
    };
</script>
<div id="reservation_list">
    <div class="search_wrap" style="margin-top:20px;">
        <div class="table_list">
            <table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="회의실 예약리스트">
                <caption>회의실 예약리스트</caption>
                <colgroup>
                    <col width="15%"><col width="35%"><col width="15%"><col width="35%">
                </colgroup>
                <tbody>
                    <tr>
                        <th>회의실</th>
                        <td style="text-align:left">
                            <select id="ROOM_TYPE" name="ROOM_TYPE" class="form-control input-sm width_100">
                                <option value="">전체</option>
                                <option value="B" <%If Request("ROOM_TYPE")&""="B" Then%>selected<%End If%>>브룩필드홀</option>
                                <option value="R" <%If Request("ROOM_TYPE")&""="R" Then%>selected<%End If%>>IFC홀</option>
                            </select>
                        </td>
                        <th>이용날짜</th>
                        <td style="text-align:left">
                            <input type="text" class="form-control input-sm width_150" id="start_date" name="start_date" value="<%=start_date%>" style="background-color:#fff;cursor:default;" placeholder="YYYY-MM-DD" readonly />
                            <input type="text" class="form-control input-sm width_150" id="end_date" name="end_date" value="<%=end_date%>" style="background-color:#fff;cursor:default;" placeholder="YYYY-MM-DD" readonly />
                        </td>
                    </tr>
                    <tr>
                        <th>예약상태</th>
                        <td style="text-align:left">
                            <select id="RESERVE_STATUS" name="RESERVE_STATUS" class="form-control input-sm width_100">
                                <option value="">전체</option>
                                <option value="R" <%If Request("RESERVE_STATUS")&""="R" Then%>selected<%End If%>>예약완료</option>
                                <option value="C" <%If Request("RESERVE_STATUS")&""="C" Then%>selected<%End If%>>예약취소</option>
                                <option value="Y" <%If Request("RESERVE_STATUS")&""="Y" Then%>selected<%End If%>>이용완료</option>
                            </select>
                        </td>
                        <th>납부</th>
                        <td style="text-align:left">
                            <select id="PAY_STATUS" name="PAY_STATUS" class="form-control input-sm width_100">
                                <option value="">전체</option>
                                <option value="N" <%If Request("PAY_STATUS")&""="N" Then%>selected<%End If%>>대기(없음)</option>
                                <option value="F" <%If Request("PAY_STATUS")&""="F" Then%>selected<%End If%>>현장결제</option>
                                <option value="M" <%If Request("PAY_STATUS")&""="M" Then%>selected<%End If%>>관리비합산</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>예약확인</th>
                        <td style="text-align:left">
                            <select id="CHK_STATUS" name="CHK_STATUS" class="form-control input-sm width_100">
                                <option value="">전체</option>
                                <option value="N" <%If Request("CHK_STATUS")&""="N" Then%>selected<%End If%>>미확인</option>
                                <option value="Y" <%If Request("CHK_STATUS")&""="Y" Then%>selected<%End If%>>예약확인</option>
                            </select>
                        </td>
                        <th>검색어</th>
                        <td style="text-align:left">
                            <select id="search_type" name="search_type" class="form-control input-sm width_100">
                                <option value="RESERVE_NAME" <%If Request("search_type")&""="RESERVE_NAME" Then%>selected<%End If%>>예약자명</option>
                                <option value="RESERVE_NO" <%If Request("search_type")&""="RESERVE_NO" Then%>selected<%End If%>>예약번호</option>
                                <option value="USER_NAME" <%If Request("search_type")&""="USER_NAME" Then%>selected<%End If%>>입주사명</option>
                            </select>
                            <input type="text" class="form-control input-sm width_300" id="search_text" name="search_text" value="<%=Request("search_text")%>" placeholder="검색어" onKeydown="javascript:if(event.keyCode == 13){fnSearch();return false; }" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <input type="hidden" id="list_link_hidden" value="<%=list_link%>" />
        <div class="btm_btn_wrap">
            <input type="button" class="btn btn-info btn-sm" id="btn_search" name="btn_search" value="예약 내역 조회" onClick="fnSearch();" />
        </div>
    </div>
    <div class="top_btn_wrap">
        <ul>
            <li>
                전체 <span style="color:#357ebd;font-weight:bold;"><%=total_count%></span> 건
            </li>
        </ul>
    </div>
    <div class="table_list">
        <table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="Admin List">
            <caption>예약목록</caption>
            <colgroup>
                <col width="6%"><col width="18%"><col width="6%"><col width="11%"><col width="10%"><col width="10%"><col width="6%"><col width="9%"><col width="6%"><col width="6%"><col width="6%"><col width="6%">
            </colgroup>
            <thead>
                <tr>
                    <th>예약번호</th><th>회의실</th><th>이용날짜</th><th>이용시간</th><th>예약자명</th><th>입주사명</th><th>요금</th><th>예약일</th><th>예약상태</th><th>납부</th><th>페널티요금</th><th>예약확인</th>
                </tr>
            </thead>
            <tbody>
                <% If objRs.EOF Then %>
                <tr>
                    <td colspan="12" align="center">
                        리스트가 없습니다.
                    </td>
                </tr>
                <%
                Else
                    For k = 0 to objRs.RecordCount - 1
                        strReserveDt = objRs("RESERVE_DT")

                        '예약시간표시
                        arrReserveTime = Split(objRs("RESERVE_TIMES"), ",")
                        For i=0 To UBound(arrReserveTime)
                            strLastTime = CInt(arrReserveTime(i)) + 1
                        Next

                        If Len(strReserveDt) = 8 Then
                            strReserveDt = Mid(strReserveDt,1,4) & "." & Mid(strReserveDt,5,2) & "." & Mid(strReserveDt,7,2)
                        End If

                        strTotalAmount = FnMoneySet(objRs("TOTAL_AMOUNT"))

                        strReserveStatus = ""
                        If objRs("RESERVE_STATUS")&"" = "R" Then
                            strReserveStatus = "예약완료"
                        ElseIf objRs("RESERVE_STATUS")&"" = "C" Then
                            strReserveStatus = "<span style='color:red'>예약취소</span>"
                        ElseIf objRs("RESERVE_STATUS")&"" = "Y" Then
                            strReserveStatus = "이용완료"
                        End If

                        strPayStatus = ""
                        ' 예약이 되어있으면 정상 결제상태를
                        If objRs("RESERVE_STATUS")&"" <> "C" Then
                            If objRs("PAY_STATUS")&"" = "N" Then
                                strPayStatus = "대기"
                            ElseIf objRs("PAY_STATUS")&"" = "F" Then
                                strPayStatus = "현장결제"
                            ElseIf objRs("PAY_STATUS")&"" = "M" Then
                                strPayStatus = "관리비합산"
                            End If
                        ' 예약이 취소되었으면 취소 결제상태를
                        Else
                            If objRs("CAN_PAY_STATUS")&"" = "N" Then
                                strPayStatus = "대기"
                                If objRs("CAN_AMOUNT")&"" = "0" Then
                                    strPayStatus = "없음"
                                End If
                            ElseIf objRs("CAN_PAY_STATUS")&"" = "F" Then
                                strPayStatus = "현장결제"
                            ElseIf objRs("CAN_PAY_STATUS")&"" = "M" Then
                                strPayStatus = "관리비합산"
                            End If
                        End If

                        strCanAmount = ""
                        If objRs("RESERVE_STATUS")&"" = "C" Then
                            strCanAmount = FnMoneySet(objRs("CAN_AMOUNT")) & "원"
                        End If

                        strChkStatus = ""
                        If objRs("CHK_STATUS")&"" = "N" Then
                            strChkStatus = "미확인"
                        ElseIf objRs("CHK_STATUS")&"" = "Y" Then
                            strChkStatus = "예약확인"
                        End If
                %>
                        <tr>
                            <td class="text-center"><a href="javascript:;" onClick="parent.location.href='reservation_list_edit.asp?reserve_no=<%=objRs("RESERVE_NO")%>';"><%=objRs("RESERVE_NO")%></a></td>
                            <td><%=objRs("ROOM_NAMES")%></td>
                            <td class="text-center"><%=strReserveDt%></td>
                            <td class="text-center"><%=objRs("MIN_TIME")%>:00~<%=strLastTime%>:00 (<%=objRs("T_QTY")%>시간)</td>
                            <td><%=objRs("RESERVE_NAME")%></td>
                            <td><%=objRs("USER_NAME")%></td>
                            <td class="text-center"><%=strTotalAmount%>원</td>
                            <td class="text-center"><%=FnFormatDateTime(objRs("REG_DT"))%></td>
                            <td class="text-center"><%=strReserveStatus%></td>
                            <td class="text-center"><%=strPayStatus%></td>
                            <td><%=strCanAmount%></td>
                            <td class="text-center"><%=strChkStatus%></td>
                        </tr>
                <%
                        objRs.MoveNext
                    Next
                End If
                %>
            </tbody>
        </table>
    </div>
    <div class="btm_btn_wrap">
        <ul>
            <li>
                <!-- img src="img/btn_delete_off.gif" class="pointer" id="del_admin" name="del_admin" alt="delete" / -->&nbsp;
                <input type="button" class="btn btn-warning btn-sm" id="reservation_excel_down" name="reservation_excel_down" value="엑셀다운로드" onClick="fnReservationExcel();" />
            </li>
            <li class="btm_btn_right">
                <a href="javascript:;" onClick="parent.location.href='reservation_new.asp';"><input type="button" class="btn btn-primary btn-sm" value="예약하기" /></a>&nbsp;
            </li>
        </ul>
    </div>
    <div class="page_wrap">
        <!-- #include file = "page_template.asp" -->
    </div>
</div>

<!--#include file = "index_footer.asp" -->
