<!--#include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%
strNOW = Left(NOW, 10)

If Request("TEST_DATE")&"" <> "" Then
	strNOW = Request("TEST_DATE")
End If

selectQuery = "select * from TBL_USER_INFO where USE_FLAG <> 'N' ORDER BY USER_NAME ASC"

set objRs = SendQuery(objConn,selectQuery)
%>

<script type="text/javascript">
	function fnUserChg(objVal) {
		if ($("#UID").val() == "") {
			alert("입주사를 선택해주세요.");
			$("#AUTHORITY").val("");
			$("#TELNUM").val("");
			$("#MOBILENUM").val("");
			$("#FAXNUM").val("");
			$("#EMAIL").val("");
			$("#UID").focus();
			return true;
		}

		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/reservation_proc.asp",
			dataType : "json",
			data : ({"UID" : $("#UID").val(), "mode" : "uid_info"}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == "") {
						alert("존재하지 않는 아이디입니다.\n다시 시도하시기 바랍니다.");
						$("#UID").focus();
					}
					else {
						var userData = data.result.split('|');
						$("#USER_NAME").val(userData[0]);
						$("#AUTHORITY").val(userData[1]);
						$("#TELNUM").val(userData[2]);
						$("#MOBILENUM").val(userData[3]);
						$("#FAXNUM").val(userData[4]);
						$("#EMAIL").val(userData[5]);
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}

	function fnGoBack() {
		get_link = $("#list_link_hidden").val();
		location.href = get_link;
	}

	function fnGoNext() {
		if ($("#UID").val() == "") {
			alert("입주사를 선택해주세요.");
			$("#UID").focus();
			return true;
		}

		if ($("#SUBJECT").val() == "") {
			alert("회의명을 입력해주세요.");
			$("#SUBJECT").focus();
			return true;
		}

		if ($("#P_COUNT").val() == "") {
			alert("참석인원을 입력해주세요.");
			$("#P_COUNT").focus();
			return true;
		}

		if ($("#TELNUM").val() == "") {
			alert("전화번호를 입력해주세요.");
			$("#TELNUM").focus();
			return true;
		}

		if ($("#MOBILENUM").val() == "") {
			alert("휴대폰번호를 입력해주세요.");
			$("#MOBILENUM").focus();
			return true;
		}

		if ($("#EMAIL").val() == "") {
			alert("이메일을 입력해주세요.");
			$("#EMAIL").focus();
			return true;
		}
		else {
			var emailReg = new RegExp(/^([0-9a-zA-Z_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,3}$/);
			if (!emailReg.test($("#EMAIL").val())) {
				alert("이메일 주소가 잘못되었습니다.");
				$("#EMAIL").focus();
				return true;
			}
		}

		var frm = document.frm_reservation1;
		frm.submit();
	}
</script>
<div class="table_list">
	<form name="frm_reservation1" action="reservation_new_detail.asp" method="post">
		<input type="hidden" id="strNOW" name="strNOW" value="<%=strNOW%>">
		<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="회의실 예약하기">
			<caption>회의실 예약하기</caption>
			<colgroup>
				<col width="15%"><col width="35%"><col width="15%"><col width="35%">
			</colgroup>
			<tbody>
				<tr>
					<th>* 입주사</th>
					<td colspan="3">
						<select id="UID" name="UID" class="form-control input-sm width_200" onChange="fnUserChg(this.value);">
							<option value="" selected>선택</option>
							<%
							If Not objRs.EOF Then
								For k = 0 to objRs.RecordCount - 1
							%>
									<option value="<%=objRs("UID")%>"><%=objRs("USER_NAME")%>(<%=objRs("UID")%>)</option>
							<%
									objRs.MoveNext
								Next
							End If
							%>
						</select>
						<input type="hidden" id="USER_NAME" name="USER_NAME" value="">
						<input type="hidden" id="AUTHORITY" name="AUTHORITY" value="">
					</td>
				</tr>
				<tr>
					<th>* 회의명</th>
					<td colspan="3"><input id="SUBJECT" name="SUBJECT" type="text" class="i_text100" value="" maxlength="30" /></td>
				</tr>
				<tr>
					<th>* 예약자명</th>
					<td><input id="RESERVE_NAME" name="RESERVE_NAME" type="text" class="width_150" value="<%=strAdminName%>" maxlength="50" /></td>
					<th>* 참석인원</th>
					<td><input id="P_COUNT" name="P_COUNT" type="text" class="width_50" value="" maxlength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
				</tr>
				<tr>
					<th>* 전화번호</th>
					<td><input id="TELNUM" name="TELNUM" type="text" class="width_150" value="" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
					<th>FAX</th>
					<td><input id="FAXNUM" name="FAXNUM" type="text" class="width_150" value="" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
				</tr>
				<tr>
					<th>* 휴대폰번호</th>
					<td><input id="MOBILENUM" name="MOBILENUM" type="text" class="width_150" value="" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
					<th>* 이메일</th>
					<td><input id="EMAIL" name="EMAIL" type="text" class="i_text50" value="" maxlength="100" style="ime-mode:disabled" /></td>
				</tr>
                <tr>
                    <th>* 촬영장비반입</th>
                    <td colspan="3">
                        <label><input type="radio" name="FILMING_EQUIPMENT" value="Y"> 반입</label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <label><input type="radio" name="FILMING_EQUIPMENT" value="N" checked> 미반입</label>
                    </td>
                </tr>
				<tr>
					<th>요청사항</th>
					<td colspan="3">
						<textarea id="REQUIREMENT" name="REQUIREMENT" type="text" style="height:80px;" maxlength="300"></textarea>
					</td>
				</tr>
				<tr>
					<th>메모</th>
					<td colspan="3">
						<textarea id="DESCRIPTION" name="DESCRIPTION" type="text" style="height:80px;" maxlength="300"></textarea>
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</div>
<div>
	<div>

	</div>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
		</li>
		<li class="btm_btn_right">
			<input type="button" class="btn btn-warning btn-sm" id="btn_list" name="btn_list" value="목록" onClick="fnGoBack();" />&nbsp;
			<input type="button" class="btn btn-primary btn-sm" id="btn_next" name="btn_next" value="다음" onClick="fnGoNext();" />
			<input type="hidden" id="list_link_hidden" value="reservation_list.asp<%=list_link%>"/>
		</li>
	</ul>
</div>

<script type="text/javascript">

    //입주사 셀렉트 박스 자동완성기능 추가
    $("#UID").select2();

</script>

<!--#include file = "index_footer.asp" -->
