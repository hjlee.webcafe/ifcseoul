<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>:::IFC 관리자:::</title>
	<link rel="stylesheet" type="text/css" href="css/base.css" />
	<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.placeholder.js"></script>
	<script type="text/javascript" src="js/md5.js"></script>
	<script type="text/javascript" src="js/admin_script.js"></script>
	<script type="text/javascript" src="js/admin_script_temp.js"></script>
	<script type="text/javascript" src="js/gallary_script.js"></script>
	
	<!-- for bootstrap -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<!-- link rel="stylesheet" href="/css/bootstrap-theme.min.css" / -->
	<!-- //for bootstrap -->
	
	<!-- for jqueryui -->
	<script type="text/javascript" src="js/jquery-ui-1.10.4.min.js"></script>
	<link rel="stylesheet" href="/css/jquery-ui-1.10.4.min.css" />
	<!-- //for jqueryui -->
	
	<!-- for upload -->
	<script type="text/javascript" src="js/jquery.uploadfile.min.js"></script>
	<link rel="stylesheet" href="/css/uploadfile.css" />
	<!-- //for upload -->
	
	<!-- for ckeditor -->
	<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
	<!-- //for ckeditor -->
	
	<!-- for placeholder -->
	<script type="text/javascript" src="js/placeholders.min.js"></script>
	<!-- //for placeholder -->

	<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
<div id="index_loading_overlay">
	<div class="index_loading_overlay_img">
		<img src="img/ajax-loader.gif" alt="overlay loading img" />
	</div>
</div>
<div class="wrap">
	<div class="content_wrap">
		<!-- contents -->
		<div class="contents">
			<input type="hidden" id="hdn_uid" value="<%=strAdminId%>" />