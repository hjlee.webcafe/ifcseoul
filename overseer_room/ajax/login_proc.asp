<!-- #include file = "../inc/_config.asp" -->
<%
' remove cache
Response.Expires = -1
Response.ExpiresAbsolute = now() - 1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

'Login Check
Dim strAdminId, strAdminPw, loginFlag, loginIP, result
strAdminId = Request.Form("admin_id")
strAdminPw = Request.Form("admin_pw")
loginFlag = false

Dim objConn, objRs, arrRs, strQuery
set objConn = OpenDBConnection()

If strAdminId = "" OR strAdminPw = "" Then
	loginFlag = false
Else

	strQuery = "select UID, USER_NAME, AUTHORITY from TBL_USER_INFO where UID = '"& strAdminId &"' and PASSWORD = '"& strAdminPw &"' AND USE_FLAG='Y' AND AUTHORITY IN ('S','A')"

	set objRs = SendQuery(objConn,strQuery)
	
	If objRs.EOF Then
		loginFlag = false
	Else
		loginFlag = true
	End If
End If

Set result = jsObject()

If loginFlag Then
	result("result") = 0
	Session("R_UID") = strAdminId
	Session("R_USER_NAME") = objRs(1)
	Session("R_AUTHORITY") = objRs(2)

	If objRs(2)&"" <> "S" Then
		result("link") = "reservation_list.asp"
	Else
		result("link") = "admin_list.asp"
	End If
	
	loginIP = Request.ServerVariables("REMOTE_ADDR")
	
	// update admin data
	strQuery = "update TBL_USER_INFO set LAST_LOGIN_DT = getdate() where UID = '"& strAdminId &"'" 
	//Response.write strQuery
	objConn.Execute(strQuery)
	//set objRs = SendQuery(objConn,strQuery)
Else
	strQuery = "select * from TBL_USER_INFO where UID = '"& strAdminId &"' AND USE_FLAG='Y'"

	set objRs = SendQuery(objConn,strQuery)
	
	If objRs.EOF Then
		result("result") = 2
	Else
		result("result") = 1
	End If
End If

objRs.Close

result.Flush

%>