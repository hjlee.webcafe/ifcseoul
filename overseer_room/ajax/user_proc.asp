<!-- #include file = "../inc/_config.asp" -->
<%
' remove cache
Response.Expires = -1
Response.ExpiresAbsolute = now() - 1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!-- #include file = "../admin_check.asp" -->
<%

Dim mode, result
Set result = jsObject()
result("result") = 0
result("error_msg") = ""

if Request.Form("mode") = "" Then
	result("result") = 2
	result("error_msg") = "The mode is not selected"
	result.Flush
	Response.End
End If

mode = Request.Form("mode")

Dim as_num, as_name, as_id, as_pw, as_tel, as_email, error_flag, selected, select_array, num, pw_where
Err.clear
On Error Resume Next

If mode = "chk_id" Then
	If Request.Form("UID") = "" Then
		result("result") = 2
		result("error_msg") = "the id is empty"
		result.Flush
		Response.End
	End If

	uid = Request.Form("UID")

	// check id
	strQuery = "select * from TBL_USER_INFO where UID = '"& uid &"' "

	set objRs = SendQuery(objConn,strQuery)

	If objRs.EOF Then
		result("result") = 0
	Else
		result("result") = 1
	End If

	result.Flush
	Response.End
ElseIf mode = "add" Then
	error_flag = false

	If Request.Form("UID") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the uid is empty | "
		error_flag = true
	End If
	If Request.Form("AUTHORITY") = "U" And Request.Form("CD_VENR") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the cd_venr is empty | "
		error_flag = true
	End If
	If Request.Form("PASSWORD") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the pw is empty"
		error_flag = true
	End If
	If Request.Form("AUTHORITY") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the authority is empty | "
		error_flag = true
	End If
	If Request.Form("AUTHORITY") = "U" And Request.Form("USER_NM") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the username is empty | "
		error_flag = true
	End If
	'If Request.Form("BUSINESS_RE_NO") = "" Then
	'	result("result") = 2
	'	result("error_msg") = result("error_msg") & "the business_re_no is empty | "
	'	error_flag = true
	'End If
	If Request.Form("MANAGER_NAME") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the managername is empty | "
		error_flag = true
	End If
	'If Request.Form("TELNUM") = "" Then
	'	result("result") = 2
	'	result("error_msg") = result("error_msg") & "the telnum is empty"
	'	error_flag = true
	'End If
	'If Request.Form("FAXNUM") = "" Then
	'	result("result") = 2
	'	result("error_msg") = result("error_msg") & "the faxnum is empty"
	'	error_flag = true
	'End If
	'If Request.Form("MOBILENUM") = "" Then
	'	result("result") = 2
	'	result("error_msg") = result("error_msg") & "the mobilenum is empty"
	'	error_flag = true
	'End If
	If Request.Form("EMAIL") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the email is empty"
		error_flag = true
	End If
	'If Request.Form("DEPT_NAME") = "" Then
	'	result("result") = 2
	'	result("error_msg") = result("error_msg") & "the dpet_name is empty"
	'	error_flag = true
	'End If
	'If Request.Form("DEPT_LEVEL") = "" Then
	'	result("result") = 2
	'	result("error_msg") = result("error_msg") & "the dept_level is empty"
	'	error_flag = true
	'End If
	If Request.Form("MONTHLY_FREE_TIME") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the monthly_free_time is empty"
		error_flag = true
	End If
	If Request.Form("LIMIT_TIME") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the limit_time is empty"
		error_flag = true
	End If

	If error_flag Then
		result.Flush
		Response.End
	End If

	UID = GetTag2Text(Request.Form("UID"))
	CD_VENR = GetTag2Text(Request.Form("CD_VENR"))
	PASSWORD = Request.Form("PASSWORD")
	AUTHORITY = GetTag2Text(Request.Form("AUTHORITY"))
	USER_NAME = GetTag2Text(Request.Form("USER_NM"))
	BUSINESS_RE_NO = GetTag2Text(Request.Form("BUSINESS_RE_NO"))
	MANAGER_NAME = GetTag2Text(Request.Form("MANAGER_NAME"))
	TELNUM = GetTag2Text(Request.Form("TELNUM"))
	FAXNUM = GetTag2Text(Request.Form("FAXNUM"))
	MOBILENUM = GetTag2Text(Request.Form("MOBILENUM"))
	EMAIL = GetTag2Text(Request.Form("EMAIL"))
	DEPT_NAME = GetTag2Text(Request.Form("DEPT_NAME"))
	DEPT_LEVEL = GetTag2Text(Request.Form("DEPT_LEVEL"))
	MONTHLY_FREE_TIME = GetTag2Text(Request.Form("MONTHLY_FREE_TIME"))
	LIMIT_TIME = GetTag2Text(Request.Form("LIMIT_TIME"))

    '등급이 NonTenant 이면 무상시간을 0으로 처리.
    If AUTHORITY = "N" Then
        MONTHLY_FREE_TIME = "0"
    End If

	// insert (TBL_USER_INFO)
	strQuery = " INSERT INTO TBL_USER_INFO (UID, CD_VENR, USER_NAME, PASSWORD, AUTHORITY, MONTHLY_FREE_TIME, LIMIT_TIME, MANAGER_NAME, TELNUM, MOBILENUM, FAXNUM, EMAIL, BUSINESS_RE_NO, DEPT_NAME, DEPT_LEVEL, USE_FLAG, REG_DT, REG_UID) "
	strQuery = strQuery & " VALUES ('"& UID  &"', '"& CD_VENR  &"', '"& USER_NAME &"', '"& PASSWORD &"', '"& AUTHORITY &"', "& MONTHLY_FREE_TIME &", "& LIMIT_TIME &", '"& MANAGER_NAME &"', '"& TELNUM &"', '"& MOBILENUM &"', '"& FAXNUM &"', '"& EMAIL &"', '"& BUSINESS_RE_NO &"', '"& DEPT_NAME &"', '"& DEPT_LEVEL &"', 'Y', GETDATE(), '"& strAdminId &"') "
	objConn.Execute(strQuery)

	If Err.Number <> 0 Then
		result("result") = 2
		result("error_msg") = "Insert Error : " & Err.Description
		result.Flush
		Response.End
	End If

    '무상시간 입력 프로시저 실행
    strQuery = "exec RESERVE_SCH_INSERT_FREE_TIME '"& UID &"', '"& MONTHLY_FREE_TIME &"'"
    set objRs = SendQuery(objConn,strQuery)

    If objRs.EOF Then
        result("result") = ""
        result("error_msg") = "Insert Error : Free Time Insert Fail "
        result.Flush
        Response.End
    End If

	result.Flush
	Response.End
ElseIf mode = "edit" Then

	error_flag = false

	If Request.Form("UID") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the uid is empty | "
		error_flag = true
	End If
	If Request.Form("AUTHORITY") = "U" And Request.Form("USER_NM") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the username is empty | "
		error_flag = true
	End If
	'If Request.Form("BUSINESS_RE_NO") = "" Then
	'	result("result") = 2
	'	result("error_msg") = result("error_msg") & "the business_re_no is empty | "
	'	error_flag = true
	'End If
	If Request.Form("MANAGER_NAME") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the managername is empty | "
		error_flag = true
	End If
	'If Request.Form("TELNUM") = "" Then
	'	result("result") = 2
	'	result("error_msg") = result("error_msg") & "the telnum is empty"
	'	error_flag = true
	'End If
	'If Request.Form("FAXNUM") = "" Then
	'	result("result") = 2
	'	result("error_msg") = result("error_msg") & "the faxnum is empty"
	'	error_flag = true
	'End If
	'If Request.Form("MOBILENUM") = "" Then
	'	result("result") = 2
	'	result("error_msg") = result("error_msg") & "the mobilenum is empty"
	'	error_flag = true
	'End If
	If Request.Form("EMAIL") = "" Then
		result("result") = 2
		result("error_msg") = result("error_msg") & "the email is empty"
		error_flag = true
	End If
	'If Request.Form("DEPT_NAME") = "" Then
	'	result("result") = 2
	'	result("error_msg") = result("error_msg") & "the dpet_name is empty"
	'	error_flag = true
	'End If
	'If Request.Form("DEPT_LEVEL") = "" Then
	'	result("result") = 2
	'	result("error_msg") = result("error_msg") & "the dept_level is empty"
	'	error_flag = true
	'End If

	If Request.Form("UPD_UID") = "" Then
		result("result") = 2
		result("error_msg") = "the upd_uid is empty"
		error_flag = true
	End If

	If error_flag Then
		result.Flush
		Response.End
	End If

	UID = GetTag2Text(Request.Form("UID"))
	CD_VENR = GetTag2Text(Request.Form("CD_VENR"))
	USER_NAME = GetTag2Text(Request.Form("USER_NM"))
    AUTHORITY = GetTag2Text(Request.Form("AUTHORITY"))
	BUSINESS_RE_NO = GetTag2Text(Request.Form("BUSINESS_RE_NO"))
	MANAGER_NAME = GetTag2Text(Request.Form("MANAGER_NAME"))
	PASSWORD = Request.Form("PASSWORD")
	TELNUM = GetTag2Text(Request.Form("TELNUM"))
	FAXNUM = GetTag2Text(Request.Form("FAXNUM"))
	MOBILENUM = GetTag2Text(Request.Form("MOBILENUM"))
	EMAIL = GetTag2Text(Request.Form("EMAIL"))
	DEPT_NAME = GetTag2Text(Request.Form("DEPT_NAME"))
	DEPT_LEVEL = GetTag2Text(Request.Form("DEPT_LEVEL"))
	DESCRIPTION = GetTag2Text(Request.Form("DESCRIPTION"))
	UPD_UID = GetTag2Text(Request.Form("UPD_UID"))

	If PASSWORD <> "" Then
		pw_where = ", PASSWORD = '"& PASSWORD &"'"
	End If

	// update
	strQuery = "UPDATE TBL_USER_INFO SET CD_VENR = '"& CD_VENR &"', USER_NAME = '"& USER_NAME &"', AUTHORITY = '" & AUTHORITY & "',  BUSINESS_RE_NO = '"& BUSINESS_RE_NO &"', MANAGER_NAME = '"& MANAGER_NAME &"', TELNUM = '"& TELNUM &"', FAXNUM = '"& FAXNUM &"', MOBILENUM = '"& MOBILENUM &"', EMAIL = '"& EMAIL &"', DEPT_NAME = '"& DEPT_NAME &"', DEPT_LEVEL = '"& DEPT_LEVEL &"', DESCRIPTION = '"& DESCRIPTION &"', UPD_DT = GETDATE(), UPD_UID = '"& UPD_UID &"' "& pw_where &" WHERE UID = '"& UID &"'"
	objConn.Execute(strQuery)

    '등급이 NonTenant 일때 해당년도 무상시간을 0으로 처리.
    If AUTHORITY = "N" Then
        strQuery = "UPDATE TBL_USER_INFO SET MONTHLY_FREE_TIME = '0' WHERE UID = '"& UID &"'"
        objConn.Execute(strQuery)

        strQuery = "UPDATE TBL_FREE_TIME SET REMAINING_FREE_TIME = '0' WHERE FT_UID = '"& UID &"' AND FT_YEAR = YEAR(GETDATE())"
        objConn.Execute(strQuery)
    End If

	If Err.Number <> 0 Then
		result("result") = 2
		result("error_msg") = "Update Error : " & Err.Description & " " & strQuery
		result.Flush
		Response.End
	End If

	result.Flush
	Response.End

ElseIf mode = "del" Then

	error_flag = false

	If Request.Form("UID") = "" Then
		result("result") = 2
		result("error_msg") = "the uid is empty"
		error_flag = true
	End If
	If Request.Form("DEL_UID") = "" Then
		result("result") = 2
		result("error_msg") = "the del_uid is empty"
		error_flag = true
	End If

	If error_flag Then
		result.Flush
		Response.End
	End If

	UID = Request.Form("UID")
	DEL_UID = Request.Form("DEL_UID")
	'select_array = Split(uid, "|")
	'
	'For i = 0 To UBound(select_array) - 1
	'	num = select_array(i)
	'	// delete
		strQuery = "UPDATE TBL_USER_INFO SET USE_FLAG = 'N', DEL_DT = GETDATE(), DEL_UID = '"& DEL_UID &"' WHERE UID = '" & UID & "'"
		objConn.Execute(strQuery)

		If Err.Number <> 0 Then
			result("result") = 2
			result("error_msg") = "Delete Error : " & Err.Description & " " & strQuery
			result.Flush
			Response.End
		End If
	'Next

	result.Flush
	Response.End

End If

%>
