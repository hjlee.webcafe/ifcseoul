<!-- #include file = "../inc/_config.asp" -->
<%

Dim data, list
Set list = jsArray()

'get list
set objConn = OpenDBConnection()
selectQuery = "SELECT RESERVE_NO, USER_NAME, SUBJECT, ROOM_NAMES, RESERVE_TIMES, DESCRIPTION, CONVERT(DATE, RESERVE_DT) AS RESERVE_DT, REPLACE(LEFT(RESERVE_TIMES, 2), ',', '') AS MIN_TIME, REPLACE(RIGHT(RESERVE_TIMES, 2), ',', '')+1 AS MAX_TIME, P_COUNT, UID FROM TBL_RESERVATION_INFO AS t WHERE RESERVE_STATUS <> 'C' ORDER BY RESERVE_DT DESC"

set objRs = SendQuery(objConn,selectQuery)

'최대값 구하는 함수
Function MaxArray(arr)
    Dim max
    max = arr(0)
    For i=0 To UBound(arr)
      If CInt(arr(i)) > CInt(max) Then
        max = arr(i)
      End If
      MaxArray = max
    Next
End Function

'최소값 구하는 함수
Function MinArray(arr)
    Dim min
    min = arr(0)
    For i=0 To UBound(arr)
      If CInt(arr(i)) < CInt(min) Then
        min = arr(i)
      End If
      MinArray = min
    Next
End Function

If Not objRs.EOF Then
    For i = 0 to objRs.RecordCount - 1

        '블록필드홈인경우 색상을 다르게 처리
        Dim col
        If Left(objRs("RESERVE_NO"), 1) = "B" Then
            col = "#e0a539"
        Else
            col = "#428bca"
        End If

        '예약시간표시
        preTime = ""
        strTimeInfo = ""
        arrReserveTime = Split(objRs("RESERVE_TIMES"), ",")

        For j=0 To UBound(arrReserveTime)
            If j = 0 Then
                strTimeInfo = strTimeInfo & arrReserveTime(j) & ":00"
            ElseIf (CInt(preTime+1) <> CInt(arrReserveTime(j))) Then
                strTimeInfo = strTimeInfo & " ~ " & CInt(preTime+1) & ":00, " & arrReserveTime(j) & ":00"
            End If
            preTime = arrReserveTime(j)
        Next
        strTimeInfo = strTimeInfo & " ~ " & CInt(arrReserveTime(UBound(arrReserveTime))+1) & ":00"

        '예약정보 데이터 저장
        Set data = jsObject()
        data("resourceId")  = objRs("RESERVE_NO")          '예약번호
        data("title")       = "[" & objRs("USER_NAME") & "] " & objRs("SUBJECT")                       '회의명
        data("room")        = objRs("ROOM_NAMES")
        data("date")        = Right("0" & objRs("MIN_TIME"), 2) & ":00 ~ " & Right("0" & objRs("MAX_TIME"), 2) & ":00"   
        data("color")        = col
        'data("textColor")    = "red"
        data("description") = "예약번호 : " & objRs("RESERVE_NO") & "<br/>"                            '메모(예약번호)
        data("description") = data("description") & "업체명 : " & objRs("RESERVE_DT") & "<br/>"        '메모(업체명)
        data("description") = data("description") & "회의실 : " & objRs("ROOM_NAMES") & "<br/>"        '메모(회의실)
        data("description") = data("description") & "날짜 : " & objRs("RESERVE_DT") & "<br/>"          '메모(날짜)
        data("description") = data("description") & "시간 : " & strTimeInfo & "<br/>"    '메모(시간)
        data("description") = data("description") & "인원 : " & objRs("P_COUNT") & "<br/>"             '메모(참석인원)
        data("description") = data("description") & "메모 : " & objRs("DESCRIPTION")  & "<br/>"        '메모(메모)
        data("start")       = objRs("RESERVE_DT") & " " & Right("0" & objRs("MIN_TIME"), 2) & ":00"                                   '이용날짜
        data("end")         = objRs("RESERVE_DT") & " " & Right("0" & objRs("MAX_TIME"), 2) & ":00"                                '이용날짜

        Set list(i) = data
        objRs.MoveNext
    Next
End If

list.Flush

%>
