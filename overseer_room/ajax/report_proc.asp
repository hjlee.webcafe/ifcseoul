<!-- #include file = "../inc/_config.asp" -->
<%

Dim result
Set result = jsObject()

result("error_msg") = ""

if Request.Form("c_year") = "" Then
    result("error_msg") = "Not Selected"
    result.Flush
    Response.End
End If

Dim data, list, C_YEAR
Set list = jsArray()

C_YEAR = Request.Form("c_year")

'get list
set objConn = OpenDBConnection()
selectQuery = "SELECT LEFT(RESERVE_DT, 6) AS YearMonth, SUM(TOTAL_AMOUNT) AS Revenue FROM TBL_RESERVATION_INFO WHERE LEFT(RESERVE_DT, 4) = '" & C_YEAR & "' AND RESERVE_STATUS <> 'C' GROUP BY LEFT(RESERVE_DT, 6) ORDER BY YearMonth ASC"
set objRs = SendQuery(objConn,selectQuery)

If Not objRs.EOF Then
    For i = 0 to objRs.RecordCount - 1

        '예약정보 데이터 저장
        Set data = jsObject()
        data("month")   = objRs("YearMonth")       '월
        data("revenue") = objRs("Revenue")         '월별매출액 합계

        Set list(i) = data
        objRs.MoveNext
    Next
End If

list.Flush

%>
