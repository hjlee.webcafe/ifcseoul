<!--#include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->

<!-- chartjs -->
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<style>
    /*#RevenueChart { width:100% !important; margin:0 auto;}*/
    #RevenueChart { /*min-width:500px !important; width:1000px !important;*/ padding:10px; }
    #pieChart { width:150px !important; height:150px !important; }
    .tbl_list {margin-bottom:50px;}
    .tbl_list td { height:50px; }
</style>

<%
Dim C_YEAR
C_YEAR = request.QueryString("y")

Dim  selectQuery1, selectQuery2

// Number of Event
selectQuery1 = "SELECT A.M_MONTH AS YearMonth, COUNT(RI.RESERVE_DT) AS NumberOfEvent,SUM(RI.TOTAL_AMOUNT) AS Revenue, A.M_MONTH AS AvalilableHour,SUM(RI.R_QTY * RI.T_QTY)-SUM(FREE_TIME) AS PaidHour, SUM(RI.FREE_TIME) AS FreeHour, SUM(RI.R_QTY * RI.T_QTY) AS TotalHour, SUM(RI.P_COUNT) AS Persons FROM (SELECT  LEFT(CONVERT(VARCHAR(10), DATEADD(MONTH, number, '" & C_YEAR & "-01-01'), 112), 6) AS M_MONTH FROM    MASTER..spt_values WHERE   type = 'P' AND number < DATEDIFF(MONTH, '" & C_YEAR & "-01-01', DATEADD(YEAR, 1, '" & C_YEAR & "-01-01'))) A LEFT JOIN TBL_RESERVATION_INFO RI ON A.M_MONTH = LEFT(RI.RESERVE_DT, 6) WHERE LEFT(A.M_MONTH,4) = '" & C_YEAR & "' AND (RI.RESERVE_DT IS NULL OR RI.RESERVE_STATUS <> 'C') GROUP BY A.M_MONTH ORDER BY A.M_MONTH ASC"

set objConn = OpenDBConnection()
set objRs = SendQuery(objConn,selectQuery1)

selectQuery2 = "SELECT  M_MONTH, ((COUNT(*)-(SELECT COUNT(*) FROM TBL_OFFDAY WHERE O_YEAR+O_MONTH = M_MONTH AND DATEPART(DW, O_YEAR+O_MONTH+O_DAY) NOT IN (1,7)))*8*5) AS AvalilableHour FROM    ( SELECT  LEFT(CONVERT(VARCHAR(10), DATEADD(DAY, number,  '" & C_YEAR & "-01-01'), 112), 6) AS M_MONTH, RIGHT(CONVERT( VARCHAR(10), DATEADD(DAY, number,  '" & C_YEAR & "-01-01'), 112), 2) AS M_DAY FROM MASTER..spt_values WHERE   type = 'P' AND number < DATEDIFF(DAY, '" & C_YEAR & "-01-01', DATEADD(YEAR, 1,  '" & C_YEAR & "-01-01'))) A WHERE DATEPART(DW, M_MONTH+M_DAY) NOT IN (1,7) GROUP BY M_MONTH"

set objARs = SendQuery(objConn,selectQuery2)

%>

<div class="top_btn_wrap">
    <h4>The Forum at IFC Event Booking Status</h4>
    <select id="S_YEAR" style="padding:10px 50px;" onchange="if(this.value) location.href=('/overseer_room/reservation_report.asp?y='+this.value);">
        <%
        For i=C_YEAR-3 To C_YEAR+3 Step 1
            response.write "<option value=" & i
            If (CInt(i) = CInt(C_YEAR)) Then
                Response.Write " selected"
            End If
            response.write ">" & i & "</option>"
        Next
        %>
    </select>
</div>
<div >
    <canvas id="RevenueChart" height="250px"></canvas>
</div>

<div class="table_list">
    <table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="Admin List">
        <caption>관리자목록</caption>
      <!--   <colgroup>
            <col width="5%"><col width="10%"><col width="10%"><col width="10%"><col width="10%"><col width="10%"><col width="10%"><col width="10%"><col width="15%"><col width="10%">
        </colgroup> -->
        <thead>
            <tr>
                <th style="text-align:left; padding-left:20px; background-color:#355f93; color:#fff">Y<%=C_YEAR%> On Book Status</th>
                <th>Jan</th>
                <th>Feb</th>
                <th>Mar</th>
                <th>Apr</th>
                <th>May</th>
                <th>Jun</th>
                <th>Jul</th>
                <th>Aug</th>
                <th>Sep</th>
                <th>Oct</th>
                <th>Nov</th>
                <th>Dec</th>
                <th>Total</th>
                <th>Ratio</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Number of Event</th>
                <%
                Total = 0
                For k = 0 to objRs.RecordCount - 1

                %>
                    <td class="text-center">
                        <%=objRs("NumberOfEvent")%>
                    </td>
                <%
                Total = Total + CInt(objRs("NumberOfEvent"))
                objRs.MoveNext
                Next
                %>
                <td><%=Total%></td>
                <td></td>
            </tr>
            <tr>
                <th>Revenue</th>
                <%
                objRs.MoveFirst
                Total = 0
                For k = 0 to objRs.RecordCount - 1
                %>
                    <td class="text-center">
                        <%=FormatNumber(objRs("Revenue"), 0) %>
                    </td>
                <%
                Total = Total + Cdbl(objRs("Revenue"))
                objRs.MoveNext
                Next
                %>
                <td><%=FormatNumber(Total,0)%></td>
                <td></td>
            </tr>
            <tr>
                <th>Total Avalilable Space/Hour</th>
                <%
                AvalTotal = 0
                For k = 0 to objARs.RecordCount - 1
                %>
                    <td class="text-center">
                        <%=objARs("AvalilableHour")%>
                    </td>
                <%
                AvalTotal = AvalTotal + CInt(objARs("AvalilableHour"))
                objARs.MoveNext
                Next
                %>
                <td><%=FormatNumber(AvalTotal,0)%></td>
                <td></td>
            </tr>
            <tr>
                <th>Paid Hour</th>
                <%
                objRs.MoveFirst
                PaidHourTotal = 0
                For k = 0 to objRs.RecordCount - 1
                %>
                    <td class="text-center">
                        <%=objRs("PaidHour")%>
                    </td>
                <%
                PaidHourTotal = PaidHourTotal + CInt(objRs("PaidHour"))
                objRs.MoveNext
                Next
                %>
                <td><%=FormatNumber(PaidHourTotal,0)%></td>
                <td rowspan="2">
                    <canvas id="pieChart" width=100 height=100></canvas>
                </td>
            </tr>
            <tr>
                <th>Free Hour</th>
                <%
                objRs.MoveFirst
                FreeHourTotal = 0
                For k = 0 to objRs.RecordCount - 1
                %>
                    <td class="text-center">
                        <%=objRs("FreeHour")%>
                    </td>
                <%
                FreeHourTotal = FreeHourTotal + CInt(objRs("FreeHour"))
                objRs.MoveNext
                Next
                %>
                <td><%=FormatNumber(FreeHourTotal,0)%></td>
            </tr>
            <tr>
                 <th>Total Occupied Space/Hour</th>
                <%
                objRs.MoveFirst
                TotalHourTotal = 0
                For k = 0 to objRs.RecordCount - 1
                %>
                    <td class="text-center">
                        <%=objRs("TotalHour") %>
                    </td>
                <%
                TotalHourTotal = TotalHourTotal + CInt(objRs("TotalHour"))
                objRs.MoveNext
                Next
                %>
                <td><%=FormatNumber(TotalHourTotal,0)%></td>
                <td></td>
            </tr>
            <tr>
                <th>Monthly Occupancy %</th>
                <%
                objRs.MoveFirst
                objARs.MoveFirst
                Total = 0
                For k = 0 to objRs.RecordCount - 1
                %>
                    <td class="text-center">
                        <%'=(Cdbl(objRs("TotalHour"))/Cdbl(objARs("AvalilableHour")))%>
                        <%'FormatPercent( Cdbl(objRs("TotalHour")) / Cdbl(objARs("AvalilableHour")) )%>
                        <%=FormatPercent( CInt(objRs("TotalHour")) / CInt(objARs("AvalilableHour")))%>
                    </td>
                <%
                'sTotal = Total + CInt(objRs("Persons"))
                objRs.MoveNext
                objARs.MoveNext
                Next
                %>
                <td><%=FormatPercent(TotalHourTotal/AvalTotal)%></td>
                <td></td>
            </tr>
            <tr>
                <th>Persons</th>
                <%
                objRs.MoveFirst
                Total = 0
                For k = 0 to objRs.RecordCount - 1
                %>
                    <td class="text-center">
                        <%=FormatNumber(objRs("Persons"), 0) %>
                    </td>
                <%
                Total = Total + CInt(objRs("Persons"))
                objRs.MoveNext
                Next
                %>
                <td><%=FormatNumber(Total,0)%></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

<script>
    $.ajax({
        type: "POST",
        url: "ajax/report_proc.asp",
        dataType: "json",
        data : ({"c_year" : $("#S_YEAR").val()}),
        success: function (data) {

            if (data.error_msg != '' && data.error_msg == 'undefined') {
                alert("Error :: Ajax Return Error : " + data.error_msg);
            }

            var monEngShortName = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

            var revenue = []
            $.each(data, function(key, value){
              revenue[parseInt(value.month.substring(4,6))-1] = value.revenue;
            });

            for (var i=0; i<12; i++) {
              if(typeof revenue[i] == 'undefined') {
                    revenue[i] = 0;
              }
            }

            var lineChartCanvas = $("#RevenueChart");
            var myLineChart = new Chart(lineChartCanvas, {
                type: 'bar',
                data: {
                    labels: monEngShortName,
                    datasets: [{
                        data: revenue,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)',
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                          ],
                          borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)',
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                          ],
                        borderWidth: 1
                    }]
                },
            options: {
                responsive: true,
                animation: false,
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                scales: {
                   yAxes: [{
                     ticks: {
                       callback: function(value, index, values) {
                         return value.toLocaleString();
                       }
                     }
                   }]
                 }
               }
            });
        },
        error: function (err) {
            alert("Error : Ajax Error : " + err);
        }
    });

    //Ratio
    var data = {
        datasets: [{
            data: [
                <%=FormatNumber(PaidHourTotal/TotalHourTotal*100, 2)%>,
                <%=FormatNumber(FreeHourTotal/TotalHourTotal*100, 2)%>
            ],
            borderColor: ["#FFC000", "#F47378"],
            backgroundColor: ["#FFFCCC", "#FBCFD0"],
            // backgroundColor: [
            //     "#FF6384",
            //     "#FFCE56"
            // ],
            label: 'My dataset' // for legend
        }],
        labels: [
            "PaidHour",
            "FreeHour"
        ]
    };

    var pieOptions = {
      events: false,
      animation: {
        duration: 500,
        easing: "easeOutQuart",
        onComplete: function () {
          var ctx = this.chart.ctx;
          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';

          this.data.datasets.forEach(function (dataset) {

            for (var i = 0; i < dataset.data.length; i++) {
              var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                  total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                  mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                  start_angle = model.startAngle,
                  end_angle = model.endAngle,
                  mid_angle = start_angle + (end_angle - start_angle)/2;

              var x = mid_radius * Math.cos(mid_angle);
              var y = mid_radius * Math.sin(mid_angle);

              ctx.font= "10px Verdana";
              ctx.fillStyle = '#B31319';
              if (i == 3){ // Darker text color for lighter background
                ctx.fillStyle = '#444';
              }
              //var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
              ctx.fillText(dataset.data[i]+"%", model.x + x, model.y + y);
              // Display percent in another line, line break doesn't work for fillText
             // ctx.fillText(percent, model.x + x, model.y + y + 15);
            }
          });
        }
      }
    };

        var pieChartCanvas = $("#pieChart");
        var myPieChart = new Chart(pieChartCanvas, {
                type: 'pie',
                data: data,
                 options: pieOptions
            });


</script>
<!--#include file = "index_footer.asp" -->
