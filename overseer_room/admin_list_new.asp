<!--#include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->

<script type="text/javascript">
	function fnGoBack() {
		get_link = $("#list_link_hidden").val();
		location.href = get_link;
	}

	function fnChkUID() {
		if ($("#UID").val() == "") {
			alert("아이디를 입력해주세요.");
			$("#UID").focus();
			return true;
		}

		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/admin_proc.asp",
			dataType : "json",
			data : ({"UID" : $("#UID").val(), "mode" : "chk_id"}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == 0) {
						alert("사용가능한 아이디입니다.");
						$("#USER_NM").focus();
						$("#btn_ChkUID").addClass("btn-success").removeClass("btn-default").removeClass("btn-warning");
						$("#UID_CHK").val("true");
						$("#UID_STORE").val($("#UID").val());
					}
					else if (data.result == 1) {
						alert("존재하는 아이디입니다.");
						$("#UID").focus();
						$("#btn_ChkUID").addClass("btn-success").removeClass("btn-default").removeClass("btn-warning");
						$("#UID_CHK").val("");
						$("#UID_STORE").val("");
					}
					else {
						alert(error_msg);
						$("#UID").focus();
						$("#btn_ChkUID").addClass("btn-success").removeClass("btn-default").removeClass("btn-warning");
						$("#UID_CHK").val("");
						$("#UID_STORE").val("");
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}

	function fnAdd() {
		if ($("#UID").val() == "") {
			alert("아이디를 입력해주세요.");
			$("#UID").focus();
			return true;
		}

		if ($("#UID_CHK").val() !== 'true' || $("#UID").val() != $("#UID_STORE").val()) {
			alert("아이디 중복을 확인해주세요.");
			return;
		}
		
		if ($("#USER_NM").val() == "") {
			alert("이름를 입력해주세요.");
			$("#USER_NM").focus();
			return true;
		}
		
		if ($("#PASSWORD").val() == "") {
			alert("비밀번호를 입력해주세요.");
			$("#PASSWORD").focus();
			return true;
		}
		
		if ($("#PASSWORD_CHK").val() == "") {
			alert("비밀번호를 한번 더 입력해주세요.");
			$("#PASSWORD_CHK").focus();
			return true;
		}
		
		if ($("#PASSWORD").val() != $("#PASSWORD_CHK").val()) {
			alert("비밀번호가 다릅니다.\n\n비밀번호를 확인해주세요.");
			$("#PASSWORD_CHK").focus();
			return true;
		}
		/*
		if ($("#DEPT_NAME").val() == "") {
			alert("부서를 입력해주세요.");
			$("#DEPT_NAME").focus();
			return true;
		}
		
		if ($("#DEPT_LEVEL").val() == "") {
			alert("직책을 입력해주세요.");
			$("#DEPT_LEVEL").focus();
			return true;
		}
		
		if ($("#TELNUM").val() == "") {
			alert("전화번호를 입력해주세요.");
			$("#TELNUM").focus();
			return true;
		}
		
		if ($("#MOBILENUM").val() == "") {
			alert("휴대폰번호를 입력해주세요.");
			$("#MOBILENUM").focus();
			return true;
		}
		*/
		if ($("#EMAIL").val() == "") {
			alert("이메일을 입력해주세요.");
			$("#EMAIL").focus();
			return true;
		}
		else {
			var emailReg = new RegExp(/^([0-9a-zA-Z_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/);
			if (!emailReg.test($("#EMAIL").val())) {
				alert("이메일 주소가 잘못되었습니다.");
				$("#EMAIL").focus();
				return true;
			}
		}

		if (!confirm("등록 하시겠습니까?"))
			return true;
		
		if ($("#index_loading_overlay").css("display") != 'none') {
			alert("등록 중 입니다");
			return false;
		}
		$("#index_loading_overlay").show();
		
		$.ajax ({
			type : "POST",
			url : "./ajax/admin_proc.asp",
			dataType : "json",
			data : ({
						"mode" : "add",
						"AUTHORITY" : $("#AUTHORITY").val(),
						"UID" : $("#UID").val(),
						"USER_NM" : $("#USER_NM").val(),
						"PASSWORD" : hex_md5($("#PASSWORD").val()),
						"DEPT_NAME" : $("#DEPT_NAME").val(),
						"DEPT_LEVEL" : $("#DEPT_LEVEL").val(),
						"TELNUM" : $("#TELNUM").val(),
						"MOBILENUM" : $("#MOBILENUM").val(),
						"EMAIL" : $("#EMAIL").val(),
						"USE_FREE_TIME" : 0,
						"LIMIT_TIME" : 0
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();
			
				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == 0) {
						alert("등록되었습니다.");
						get_link = "admin_list.asp";//$("#list_link_hidden").val();
						location.href = get_link;
					}
					else {
						alert(error_msg);
						$("#UID").focus();
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}
</script>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="관리자 계정등록">
		<caption>관리자 계정등록</caption>
		<colgroup>
			<col width="15%"><col width="35%"><col width="15%"><col width="35%">
		</colgroup>
		<tbody>
			<tr>
				<th>등급</th>
				<td colspan="3">
					<select id="AUTHORITY" name="AUTHORITY" class="form-control input-sm width_100">
						<option value="S" <%If Request("authority")&""="S" Then%>selected<%End If%>>SuperAdmin</option>
						<option value="A" <%If Request("authority")&""="A" Then%>selected<%End If%>>Admin</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>아이디</th>
				<td>
					<input id="UID" name="UID" type="text" class="width_150" value="<%=UID%>" maxlength="30" style="ime-mode:disabled" onkeyup="this.value=this.value.replace(/[^A-Za-z0-9]/g,'');" />
					<input type="button" class="btn btn-info btn-sm" id="btn_ChkUID" value="아이디확인" onClick="fnChkUID()" />
					<input type="hidden" id="UID_CHK" name="UID_CHK" value="" />
					<input type="hidden" id="UID_STORE" name="UID_STORE" value="" />
				</td>
				<th>이름</th>
				<td><input id="USER_NM" name="USER_NM" type="text" class="width_150" value="<%=USER_NAME%>" maxlength="50" /></td>
			</tr>
			<tr>
				<th>비밀번호</th>
				<td><input id="PASSWORD" name="PASSWORD" type="password" class="width_150" value="<%=PASSWORD%>" maxlength="16" /><br><!--<span>※ 영문 / 숫자 조합 8~16자리</span>--></td>
				<th>비밀번호 확인</th>
				<td><input id="PASSWORD_CHK" name="PASSWORD_CHK" type="password" class="width_150" value="" maxlength="16" /><br><span>※ 비밀번호를 한번 더 입력해주세요.</span></td>
			</tr>
			<tr>
				<th>부서</th>
				<td><input id="DEPT_NAME" name="DEPT_NAME" type="text" class="width_150" value="<%=DEPT_NAME%>" maxlength="50" /></td>
				<th>직책</th>
				<td><input id="DEPT_LEVEL" name="DEPT_LEVEL" type="text" class="width_150" value="<%=DEPT_LEVEL%>" maxlength="20" /></td>
			</tr>
			<tr>
				<th>전화번호</th>
				<td><input id="TELNUM" name="TELNUM" type="text" class="width_150" value="<%=TELNUM%>" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
				<th>휴대폰번호</th>
				<td><input id="MOBILENUM" name="MOBILENUM" type="text" class="width_150" value="<%=MOBILENUM%>" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
			</tr>
			<tr>
				<th>이메일</th>
				<td colspan="3"><input id="EMAIL" name="EMAIL" type="text" class="i_text25" value="<%=EMAIL%>" maxlength="100" style="ime-mode:disabled" /></td>
			</tr>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
		</li>
		<li class="btm_btn_right">
			<input type="button" class="btn btn-warning btn-sm" id="btn_list" name="btn_list" value="목록" onClick="fnGoBack();" />&nbsp;
			<input type="button" class="btn btn-primary btn-sm" id="btn_add" name="btn_add" value="등록" onClick="fnAdd();" />
			<input type="hidden" id="list_link_hidden" value="admin_list.asp<%=list_link%>"/>
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->