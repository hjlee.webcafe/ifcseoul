<!--#include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<link href='../js/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='../js/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link href='../js/plugins/fullcalendar/scheduler.min.css' rel='stylesheet' />
<link href='../js/plugins/qtip/jquery.qtip.min.css' rel='stylesheet' />

<script src='../js/plugins/moment/moment.min.js'></script>

<!-- fullcalendar -->
<script src='../js/plugins/fullcalendar/fullcalendar.js'></script>

<!-- bootstrap -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>

<!-- qtip -->
<script src='../js/plugins/qtip/jquery.qtip.min.js'></script>

<style>
    #calendar {
      width: 95%;
      margin: 30px auto;
    }
</style>
<div id='calendar'></div>

<script type="text/javascript">
    $('#calendar').fullCalendar({
        defaultView: 'month',
        header: {
          left: 'today prev,next',
          center: 'title',
          right: 'month,agendaWeek,agendaDay,listMonth'
        },
        displayEventTime: false,
        eventSources: [{
            url: 'ajax/calendar_proc.asp'
        }],
        eventRender: function(eventObj, $el) {
          $el.find(".fc-list-item-title").append(" | <span style='color:red'>" + eventObj.room + "</span> | <span style='color:blue'>" + eventObj.date + "</span>");
          $el.qtip({
            show: 'click',
            hide: 'unfocus',
            position: {
              my: 'top left',
              at: 'bottom center',
              adjust: {
                method: 'shift none'
              }
            },
          content: {
            title: eventObj.title,
            text: eventObj.description
          }
        });
      }
    });
</script>

<!--#include file = "index_footer.asp" -->
