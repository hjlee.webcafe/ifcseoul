<!--#include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%
Dim countQuery, selectQuery, num_count, list_array, page_result, t_page, query_where

// search string
query_where = ""
If Request("search_text")&"" <> "" Then
	query_where = query_where & " and " & Request("search_type") & " Like '%" & GetTag2Text(Request("search_text")) & "%' "
End If
If Request("start_date")&"" <> "" And Request("end_date")&"" <> "" Then
	query_where = query_where & " and REG_DT BETWEEN '" & Request("start_date") & "' AND '" & Request("end_date") & " 23:59:59' "
End If

// counting list
countQuery = "select count(*) as count from dbo.TBL_USER_INFO where AUTHORITY IN ('U', 'N') "& query_where

set objConn = OpenDBConnection()
set objRs = SendQuery(objConn,countQuery)
total_count = objRs("count")
num_count = total_count - (page - 1) * page_limit
set objRs = Nothing

// get list
selectQuery = " select Top " & page_limit & " * from dbo.TBL_USER_INFO where AUTHORITY IN ('U', 'N') AND UID Not In (select Top " & current_count & " UID from dbo.TBL_USER_INFO where AUTHORITY IN ('U', 'N') "& query_where &" order by REG_DT desc) "& query_where & " order by REG_DT desc "

set objRs = SendQuery(objConn,selectQuery)

'page_create(p_total_count, p_page, p_page_limit, p_page_length)
t_page = page
page_result = page_create(total_count, t_page, page_limit, page_length)
%>
<script type="text/javascript">
	$(document).ready(function() {
		<% If Request("start_date")&"" <> "" Then %>
			$("#start_date").val($.datepicker.formatDate('yy-mm-dd', new Date('<%=Request("start_date")%>')));
		<% End If %>
		<% If Request("end_date")&"" <> "" Then %>
			$("#end_date").val($.datepicker.formatDate('yy-mm-dd', new Date('<%=Request("end_date")%>')));
		<% End If %>
	});

	function fnSearch() {
		var startDate = $("#start_date").val();
		var endDate = $("#end_date").val();

		if ((startDate != "" && endDate == "") || (startDate == "" && endDate != "")) {
			alert("등록일 구간을 바르게 설정하여 주십시오.");
			return true;
		}
		if (startDate != "" && endDate != "") {
			var startArray = startDate.split('-');
			var endArray = endDate.split('-');
			var start_date = new Date(startArray[0], startArray[1], startArray[2]);
			var end_date = new Date(endArray[0], endArray[1], endArray[2]);
			if(start_date.getTime() > end_date.getTime()) {
				alert("종료날짜보다 시작날짜가 작아야합니다.");
				return true;
			}
		}

		document.location.href = "user_list.asp?search_type=" + $("#search_type").val() + "&search_text=" + $("#search_text").val() + "&start_date=" + startDate + "&end_date=" + endDate;
	};

	function fnUserExcel() {
		window.open("user_list_xls.asp");
	};
</script>
<div class="search_wrap" style="margin-top:20px;">
	<div class="table_list">
		<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="입주사 리스트">
			<caption>입주사 리스트</caption>
			<colgroup>
				<col width="15%"><col width="35%"><col width="15%"><col width="35%">
			</colgroup>
			<tbody>
				<tr>
					<th>검색어</th>
					<td style="text-align:left">
						<select id="search_type" name="search_type" class="form-control input-sm width_100">
							<option value="UID" <%If Request("search_type")&""="UID" Then%>selected<%End If%>>아이디</option>
							<option value="MANAGER_NAME" <%If Request("search_type")&""="MANAGER_NAME" Then%>selected<%End If%>>담당자명</option>
							<option value="USER_NAME" <%If Request("search_type")&""="USER_NAME" Then%>selected<%End If%>>입주사명</option>
						</select>
						<input type="text" class="form-control input-sm width_300" id="search_text" name="search_text" value="<%=Request("search_text")%>" placeholder="검색어" onKeydown="javascript:if(event.keyCode == 13){fnSearch();return false; }" />
					</td>
					<th>등록일</th>
					<td style="text-align:left">
						<input type="text" class="form-control input-sm width_150" id="start_date" name="start_date" value="<%=start_date%>" style="background-color:#fff;cursor:default;" placeholder="YYYY-MM-DD" readonly />
						<input type="text" class="form-control input-sm width_150" id="end_date" name="end_date" value="<%=end_date%>" style="background-color:#fff;cursor:default;" placeholder="YYYY-MM-DD" readonly />
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<input type="hidden" id="list_link_hidden" value="<%=list_link%>" />
	<div class="btm_btn_wrap">
		<input type="button" class="btn btn-info btn-sm" id="btn_search" name="btn_search" value="검 색" onClick="fnSearch();" />
	</div>
</div>
<div class="top_btn_wrap">
	<ul>
		<li>
			전체 <span style="color:#357ebd;font-weight:bold;"><%=total_count%></span> 건
		</li>
	</ul>
</div>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="User List">
		<caption>입주사목록</caption>
		<colgroup>
			<col width="5%"><col width="8%"><col width="8%"><col width="13%"><col width="13%"><col width="13%"><col width="17%"><col width="13%"><col width="10%">
		</colgroup>
		<thead>
			<tr>
				<th>번호</th>
				<th>아이디</th>
				<th>담당자명</th>
				<th>입주사명</th>
				<th>전화번호</th>
				<th>휴대폰번호</th>
				<th>이메일</th>
				<th>등록일</th>
                <th>월별무상제공시간</th>
			</tr>
		</thead>
		<tbody>
			<% if objRs.EOF Then %>
			<tr>
				<td colspan="8" align="center">
					리스트가 없습니다.
				</td>
			</tr>
			<%
			Else
				For k = 0 to objRs.RecordCount - 1
					strDel = ""
					If objRs("USE_FLAG")&"" = "N" Then
						strDel = "(탈퇴)"
					End If
			%>
					<tr>
						<td class="text-center"><%=num_count-k%></td>
						<td><a href="user_list_view.asp?uid=<%=objRs("UID")%>"><%=objRs("UID")%></a><%=strDel%></td>
						<td><%=objRs("MANAGER_NAME")%></td>
						<td><%=objRs("USER_NAME")%></td>
						<td><%=FnSetNum(objRs("TELNUM"))%></td>
						<td><%=FnSetNum(objRs("MOBILENUM"))%></td>
						<td><%=objRs("EMAIL")%></td>
						<td><%=FnFormatDateTime(objRs("REG_DT"))%></td>
                        <td><%=objRs("MONTHLY_FREE_TIME")%></td>
					</tr>
			<%
					objRs.MoveNext
				Next
			End If
			%>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<!-- img src="img/btn_delete_off.gif" class="pointer" id="del_admin" name="del_admin" alt="delete" / -->&nbsp;
			<input type="button" class="btn btn-warning btn-sm" id="user_excel_down" name="user_excel_down" value="엑셀다운로드" onClick="fnUserExcel();" />
		</li>
		<li class="btm_btn_right">
			<a href="user_list_new.asp"><input type="button" class="btn btn-primary btn-sm" value="등록하기" /></a>&nbsp;
		</li>
	</ul>
</div>
<div class="page_wrap">
	<!-- #include file = "page_template.asp" -->
</div>

<!--#include file = "index_footer.asp" -->
