<!--#include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%
strNOW = Request.Form("strNOW")
UID = Request.Form("UID")
USER_NAME = Request.Form("USER_NAME")
AUTHORITY = Request.Form("AUTHORITY")
SUBJECT = Request.Form("SUBJECT")
RESERVE_NAME = Request.Form("RESERVE_NAME")
P_COUNT = Request.Form("P_COUNT")
TELNUM = Request.Form("TELNUM")
FAXNUM = Request.Form("FAXNUM")
FILMING_EQUIPMENT = Request.Form("FILMING_EQUIPMENT")
MOBILENUM = Request.Form("MOBILENUM")
EMAIL = Request.Form("EMAIL")
REQUIREMENT = Request.Form("REQUIREMENT")
DESCRIPTION = Request.Form("DESCRIPTION")

'strNOW = "2018-02-25"
'UID = "user2"
'USER_NAME = "입주사2"
'AUTHORITY = "U"
'SUBJECT = "회의명 aaa 111222"
'RESERVE_NAME = "시스템관리자"
'P_COUNT = "150"
'TELNUM = "0222222222"
'FAXNUM = "0200020002"
'MOBILENUM = "01022222222"
'EMAIL = "fw10owen@naver.com"
'REQUIREMENT = "요청사항 111"
'DESCRIPTION = "메모 121212"

selectQuery = "select * from TBL_USER_INFO where UID = '" & UID & "' ORDER BY USER_NAME ASC"

set objRs = SendQuery(objConn,selectQuery)
%>

<script type="text/javascript">
	$(document).ready(function() {
		fnMonthChg("<%=Year(strNow)%>", "<%=Month(strNow)%>");
		$("#td_info_p_count").html($("#P_COUNT").val() + " 명");
		fnPolyClick("N",1);
		fnEtcClick("N",1);
	});

	function fnMonthChg(objYear, objMonth) {
	    $("#index_loading_overlay").show();

	    $("li[id*=li_cal_month]").removeClass("on");
	    $("#li_cal_month" + objMonth).addClass("on");

	    $("#em_cal_YM").html(objYear + "." + objMonth);

	    $("#YEAR").val(objYear);
	    $("#MONTH").val(objMonth);

	    $.ajax ({
	        type : "POST",
	        url : "./ajax/reservation_proc.asp",
	        dataType : "json",
	        data : ({"YEAR" : objYear, "MONTH" : objMonth, "NOW" : "<%=strNOW%>", "UID" : $("#UID").val(), "LIMIT_TIME" : '<%=objRs("LIMIT_TIME")%>', "mode" : "get_day"}),
	        success : function (data) {

	            $("#index_loading_overlay").hide();

	            if (typeof data != 'object') {
	                alert("Error :: Ajax Return Error");
	            }
	            else {
	                var error_msg = data.error_msg;
	                if (data.result == "") {
	                    alert(error_msg);
	                }
	                else {
	                    var dateData = data.result.split('|');

	                    // 월 무상시간 표시
	                    $("#em_cal_free_time").html(dateData[1]);
						selFreeTimeHtml = '<select id="sel_free_time" name="sel_free_time" onChange="fnFreeTimeChg(this.value);">';
						for (i=0; i<=dateData[1]; i++) {
							selFreeTimeHtml += '<option value="' + i + '"> ' + i + '시간 </option>'
						}
						selFreeTimeHtml += '</select><span class="gearNoti"> 현재 ' + dateData[1] + '시간 까지 이용 가능</span>'
						$("#td_free_time").html(selFreeTimeHtml);

	                    $("#em_cal_limit_time").html(dateData[0]);
	                    $("#ul_cal_day").html(dateData[2]);
	                }
	            }
	        },
	        error : function (data) {
	            $("#index_loading_overlay").hide();
	            alert("Error : Ajax Error : " + data);
	        }
	    });

	    fnMonSelReset();

	}

	// 달 선택
	function fnMonthChgTest(objVal) {
		$("#index_loading_overlay").show();

		$("li[id*=li_cal_month]").removeClass("on");
		$("#li_cal_month" + objVal).addClass("on");

		$("#em_cal_YM").html("<%=Year(strNow)%>." + objVal);

		$("#YEAR").val("<%=Year(strNow)%>");
		$("#MONTH").val(objVal);

		$.ajax ({
			type : "POST",
			url : "./ajax/reservation_proc.asp",
			dataType : "json",
			data : ({"YEAR" : $("#YEAR").val(), "MONTH" : objVal, "NOW" : "<%=strNOW%>", "UID" : $("#UID").val(), "LIMIT_TIME" : '<%=objRs("LIMIT_TIME")%>', "mode" : "get_day"}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == "") {
						alert(error_msg);
					}
					else {
						var dateData = data.result.split('|');
						$("#em_cal_limit_time").html(dateData[0]);
						$("#ul_cal_day").html(dateData[1]);
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});

		fnMonSelReset();
	}

	// 일 선택
	function fnDayClick(beforeTodayYN ,day, dw, offYn, uday120, obj) {
  //function fnDayClick(beforeTodayYN ,day, dw, offYn, todayYn, uday90, obj) {    // forum에서 방식
		if (beforeTodayYN == "Y") {
			alert("오늘 이전의 날짜는 선택할 수 없습니다.");
			return true;
		}
		/*if (uday120 != "Y") {
			alert("회의실 예약은 120일 이전의 날짜만 예약이 가능합니다.");
			return true;
		}
		if (dw == "7") {
			alert("토요일은 선택할 수 없습니다.");
			return true;
		}
		if (dw == "1") {
			alert("일요일은 선택할 수 없습니다.");
			return true;
		}
		if (offYn == "Y") {
			alert("공휴일은 선택할 수 없습니다.");
			return true;
		}*/
		$("#ul_cal_day").children().each(function(idx,item) {
			$(this).removeClass("on");
		});
		$(obj).addClass("on");

		$("#DAY").val(day);
		$("#td_info_reserve_dt").html($("#YEAR").val() + "년 " + $("#MONTH").val() + "월 " + $("#DAY").val() + "일");
		$("#div_room").addClass("active");

		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/reservation_proc.asp",
			dataType : "json",
			data : ({"YEAR" : $("#YEAR").val(), "MONTH" : $("#MONTH").val(), "DAY" : day, "mode" : "get_detail_admin"}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == "") {
						alert(error_msg);
					}
					else {
						var dateData = data.result.split('|');
						if (dateData[0] == "N") {
							$("#div_room_B0001").addClass("disabled_v01");
							$("#chk_room_B0001").attr("disabled",true);
						}
						else {
							$("#chk_room_B0001").attr("disabled",false);
							//$("#tr_time_B0001").removeClass("disabled");
						}
						if (dateData[1] == "N") {
							$("#div_room_R0001").addClass("disabled_v01");
							$("#chk_room_R0001").attr("disabled",true);
						}
						else {
							$("#chk_room_R0001").attr("disabled",false);
							//$("#tr_time_R0001").removeClass("disabled");
						}
						if (dateData[2] == "N") {
							$("#div_room_R0002").addClass("disabled_v01");
							$("#chk_room_R0002").attr("disabled",true);
						}
						else {
							$("#chk_room_R0002").attr("disabled",false);
							//$("#tr_time_R0002").removeClass("disabled");
						}
						if (dateData[3] == "N") {
							$("#div_room_R0003").addClass("disabled_v01");
							$("#chk_room_R0003").attr("disabled",true);
						}
						else {
							$("#chk_room_R0003").attr("disabled",false);
							//$("#tr_time_R0003").removeClass("disabled");
						}
						if (dateData[4] == "N") {
							$("#div_room_R0004").addClass("disabled_v01");
							$("#chk_room_R0004").attr("disabled",true);
						}
						else {
							$("#chk_room_R0004").attr("disabled",false);
							//$("#tr_time_R0004").removeClass("disabled");
						}

						$("#tr_time_B0001").html("<td>브룩필드홀</td>" + dateData[5]);
						$("#tr_time_R0001").html("<td>IFC Hall 301</td>" + dateData[6]);
						$("#tr_time_R0002").html("<td>IFC Hall 302</td>" + dateData[7]);
						$("#tr_time_R0003").html("<td>IFC Hall 303</td>" + dateData[8]);
						$("#tr_time_R0004").html("<td>IFC Hall 304</td>" + dateData[9]);
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});

		fnDaySelReset();
	}

	// 룸 선택
	function fnRoomChk(objId) {
		if (objId == "B0001") {
			var cnt = 0;
			$("input:checkbox[id*=chk_room_R000]:checked").each(function() {
				cnt++;
			});
			if (cnt > 0) {
				alert("IFC홀과 브룩필드홀은 동시에 예약할 수 없습니다.");
				$("#chk_room_" + objId).attr("checked",false);
				return true;
			}
		}
		else {
			if ($("#chk_room_B0001").is(":checked")) {
				alert("IFC홀과 브룩필드홀은 동시에 예약할 수 없습니다.");
				$("#chk_room_" + objId).attr("checked",false);
				return true;
			}
		}

		var RoomName = ""
		if ($("#chk_room_" + objId).is(":checked")) {
			$("#div_room_" + objId).addClass("check");
			$("#tr_time_" + objId).removeClass("disabled");
			$("#div_info_" + objId).addClass("check");
		}
		else {
			$("#div_room_" + objId).removeClass("check");
			$("#tr_time_" + objId).addClass("disabled");
			$("#div_info_" + objId).removeClass("check");
		}
		$("td[id*=td_time_]").each(function(idx,item) {
			if ($(item).hasClass("selected")) {
				$(item).removeClass("selected");
			}
		});

		/* SSSSS 룸명 구하기 SSSSS */
		$("input:checkbox[id*=chk_room_]").each(function(idx,item) {
			if ($(item).is(":checked")) {
				if (idx == 0)
					RoomName = RoomName + ",브룩필드홀";
				if (idx == 1)
					RoomName = RoomName + ",IFC Hall 301";
				if (idx == 2)
					RoomName = RoomName + ",IFC Hall 302";
				if (idx == 3)
					RoomName = RoomName + ",IFC Hall 303";
				if (idx == 4)
					RoomName = RoomName + ",IFC Hall 304";
			}
		});
		if (RoomName.substring(0,1) == ",") {
			RoomName = RoomName.substring(1, RoomName.length);
		}
		$("#td_info_room").html(RoomName);
		/* EEEEE 룸명 구하기 EEEEE */

		fnInfoReset();
	}

	// 시간 선택
	function fnTimeClick(room, time, obj) {
		if ($("#tr_time_" + room).hasClass("disabled") == false) {
			if ($(obj).hasClass("disable_v01")) {
				alert("이미 예약되었습니다.");
				return false;
			}
			else {
				var selB0001YN = "N";
				var selR0001YN = "N";
				var selR0002YN = "N";
				var selR0003YN = "N";
				var selR0004YN = "N";
				if ($("#tr_time_B0001").hasClass("disabled") == false) {
					if ($("#td_time_B0001_" + time).hasClass("disable_v01")) {
						alert("브룩필드홀의 " + time + "시가 예약되어있습니다");
						return true;
					}
					else {
						selB0001YN = "Y";
					}
				}
				if ($("#tr_time_R0001").hasClass("disabled") == false) {
					if ($("#td_time_R0001_" + time).hasClass("disable_v01")) {
						alert("IFC홀1의 " + time + "시가 예약되어있습니다");
						return true;
					}
					else {
						selR0001YN = "Y";
					}
				}
				if ($("#tr_time_R0002").hasClass("disabled") == false) {
					if ($("#td_time_R0002_" + time).hasClass("disable_v01")) {
						alert("IFC홀2의 " + time + "시가 예약되어있습니다");
						return true;
					}
					else {
						selR0002YN = "Y";
					}
				}
				if ($("#tr_time_R0003").hasClass("disabled") == false) {
					if ($("#td_time_R0003_" + time).hasClass("disable_v01")) {
						alert("IFC홀3의 " + time + "시가 예약되어있습니다");
						return true;
					}
					else {
						selR0003YN = "Y";
					}
				}
				if ($("#tr_time_R0004").hasClass("disabled") == false) {
					if ($("#td_time_R0004_" + time).hasClass("disable_v01")) {
						alert("IFC홀4의 " + time + "시가 예약되어있습니다");
						return true;
					}
					else {
						selR0004YN = "Y";
					}
				}

				if (selB0001YN == "Y")
					$("#td_time_B0001_" + time).toggleClass("selected");
				if (selR0001YN == "Y")
					$("#td_time_R0001_" + time).toggleClass("selected");
				if (selR0002YN == "Y")
					$("#td_time_R0002_" + time).toggleClass("selected");
				if (selR0003YN == "Y")
					$("#td_time_R0003_" + time).toggleClass("selected");
				if (selR0004YN == "Y")
					$("#td_time_R0004_" + time).toggleClass("selected");
			}
		}

        /* SSSSS 시간 구하기 SSSSS */
        var roomId = "";
        var roomCnt = 0;
        $("input:checkbox[id*=chk_room_]:checked").each(function(idx,item) {
        	roomId = $(item).val();
        	roomCnt = roomCnt +1;
        });

        var arrTime   = new Array();
        var preTime   = "";
        var sTimeInfo = "";
        $("td[id*=td_time_" + roomId + "]").each(function(idx,item) {
        	if ($(item).hasClass("selected")) {
        		arrTime.push(idx+7);
        	}
        });

        // 예약정보에 예약시간 표시
        if (arrTime.length > 0) {
            for (var i=0; i < arrTime.length; i++) {
                if (i==0) {
                    sTimeInfo += arrTime[i] + ':00';
                } else if (Number(preTime+1) != Number(arrTime[i])) {
                    sTimeInfo += ' ~ ' + Number(preTime+1) + ':00<br/> ' + arrTime[i] + ':00';
                }
                preTime = Number(arrTime[i]);
            }
            sTimeInfo += ' ~ ' + Number(arrTime[arrTime.length-1]+1) + ':00';
        }
        $("#td_info_time").html(sTimeInfo);

        /* EEEEE 시간 구하기 EEEEE */

        /* SSSSS 금액 구하기 SSSSS */
        var timeCnt = 0;
        $("td[id*=td_time_" + roomId + "]").each(function(idx,item) {
        	if ($(item).hasClass("selected")) {
        		timeCnt = timeCnt +1;
        	}
        });

        // 총 이용시간
        var total_unit = Number(roomCnt) * Number(timeCnt);
        var total_unit_html = "총 이용시간 " + total_unit + "시간, ";

        var iAmountPerHour = new Array();               // 시간당 사용금액 정의
        iAmountPerHour['B'] = 240000;                   // 브룩필드홀
        iAmountPerHour['R'] = 120000;                   // IFC홀

        var arrDiscountRate = Array();                  // 사용시간별 할인률 정의
        arrDiscountRate = [
            [0,  0, 20, 20, 30, 30, 35, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40],    //브룩필드홀
            [0,  0, 20, 20, 25, 25, 30, 30, 35, 35, 35, 35, 35, 35, 35, 35, 35],    //IFC홀 1Unit
            [0,  5, 25, 25, 30, 30, 35, 35, 40, 40, 40, 40, 40, 40, 40, 40, 40],    //IFC홀 2Unit
            [0, 10, 30, 30, 35, 35, 40, 40, 45, 45, 45, 45, 45, 45, 45, 45, 45],    //IFC홀 3Unit
            [0, 15, 35, 35, 40, 40, 45, 45, 50, 50, 50, 50, 50, 50, 50, 50, 50]     //IFC홀 4Unit
        ];

        var iDiscountCnt = 0;                           //arrDiscountRate 키값
        var iDiscountRate = 0;                          //계산된 할인율
        var iOriAmount = 0;                             //시간당 사용금액

        // 계산된 값 셋팅
        iDiscountCnt = (room == "B0001") ? 0 : roomCnt;
        iOriAmount = iAmountPerHour[room.substring(0,1)] * timeCnt * roomCnt;

        if ("<%=objRs("AUTHORITY")%>" == "N") {
            iDiscountRate = 0;
        } else {
            iDiscountRate = arrDiscountRate[iDiscountCnt][timeCnt-1];
        }

        $("#spn_ori_amount").html(fnMoneyNum(iOriAmount) + "<em>원</em>");                       //기본요금
        $("#ORI_AMOUNT").val(iOriAmount);
        $("#spn_discount").html("("+total_unit_html+"할인율 " + iDiscountRate + "% 적용)");      //할인율
        $("#DISCOUNT").val(iDiscountRate);

        fnTotalAmountCal();
        /* EEEEE 금액 구하기 EEEEE */
	}

	// 폴리콤선택
	function fnPolyClick(objVal, idx) {
		$("button[id*=btn_poly]").removeClass("on");
		$("#btn_poly" + idx).addClass("on");
		$("#POLYCOM").val(objVal);
	}

	// 외부기자재 반입
	function fnEtcClick(objVal, idx) {
		$("button[id*=btn_etc]").removeClass("on");
		$("#btn_etc" + idx).addClass("on");
		$("#ETC").val(objVal);
	}

	// 무료이용시간 변경
	function fnFreeTimeChg(objVal) {
		var roomCnt = fnGetRoomCnt();
		var timeCnt = fnGetTimeCnt(roomCnt);
		var total_unit = Number(roomCnt) * Number(timeCnt);

		if (total_unit == 0) {
			alert("룸과 시간을 선택해주세요.");
			$("#sel_free_time").val("0");
			return true;
		}
		else if (objVal > total_unit) {
			alert("총 유닛보다 무료이용시간을 더 사용할 수 없습니다.");
			$("#sel_free_time").val("0");
			return true;
		}

		if ($("#YEAR").val() != '<%=Year(strNOW)%>') {
			alert("무료시간은 접속일 기준 해당년도만 사용가능합니다.");
			$("#sel_free_time").val("0");
			return true;
		}

		fnTotalAmountCal();
	}

	// 총 금액 계산
	function fnTotalAmountCal() {
		var roomCnt = fnGetRoomCnt();
		var timeCnt = fnGetTimeCnt(roomCnt);
		var total_unit = Number(roomCnt) * Number(timeCnt);
		var ori_amount = 0;
		var discount = 0;
		var etc_amount = 0;
		var free_time = $("#sel_free_time").val();
		var pre_amount = 0;
		var total_amount = 0;

		if ($("#ORI_AMOUNT").val() != "")
			ori_amount = $("#ORI_AMOUNT").val();
		if ($("#DISCOUNT").val() != "")
			discount = $("#DISCOUNT").val();
		if ($("#ETC_AMOUNT").val() != "")
			etc_amount = $("#ETC_AMOUNT").val();
		else
			$("#ETC_AMOUNT").val("0");

		if (free_time >= total_unit) {
			pre_amount = Number(ori_amount) + Number(etc_amount);
			total_amount = Number(etc_amount);
			total_amount = Number(fnMoneyCut(total_amount));
		}
		else {
			pre_amount = Number(ori_amount) + Number(etc_amount);
			total_amount = (Number(ori_amount) * (100 - Number(discount)) / 100) * (total_unit - free_time) / total_unit + Number(etc_amount);
			total_amount = Number(fnMoneyCut(total_amount));
		}
		$("#spn_total_amount").html(fnMoneyNum(total_amount) + "<em>원</em>");
		$("#TOTAL_AMOUNT").val(total_amount);
	}

	// 룸 선택 개수 구하기
	function fnGetRoomCnt() {
		var roomCnt = 0;

		$("input:checkbox[id*=chk_room_]:checked").each(function() {
			roomCnt = roomCnt + 1;
		});

		return roomCnt;
	}

	// 시간 선택 개수 구하기
	function fnGetTimeCnt(roomCnt) {
		var timeCnt = 0;
		var returnTime = 0;

		$("td[id*=td_time_]").each(function(idx,item) {
			if ($(item).hasClass("selected")) {
				timeCnt = timeCnt + 1;
			}
		});

		returnTime = timeCnt / roomCnt;
		// if (returnTime > 8) returnTime = 8;

		return returnTime;
	}

	// 금액 , 표시
	function fnMoneyNum(num) {
		var money = String(num)
		if (money.length == 5)
			money = money.substring(0,2) + "," + money.substring(2,5)
		else if (money.length == 6)
			money = money.substring(0,3) + "," + money.substring(3,6)
		else if (money.length == 7)
			money = money.substring(0,1) + "," + money.substring(1,4) + "," + money.substring(4,7)

		return money;
	}

	// 100단위 절삭
	function fnMoneyCut(num) {
		var cutMoney = Number(num) % 1000;
		return Number(num) - Number(cutMoney);
	}

	// 리셋
	function fnReset() {
		fnMonthChg("<%=Year(strNow)%>", "<%=Month(strNow)%>");
	}

	// 달 선택 리셋
	function fnMonSelReset() {
		$("#DAY").val("");
		$("#td_info_reserve_dt").html("");
		$("#sel_free_time").val("0");
		$("#div_room").removeClass("active");
		$("div[id*=div_room_]").each(function(idx,item) {
			$(item).removeClass("disabled_v01");
		});
		$("input:checkbox[id*=chk_room_]").each(function(idx,item) {
			$(item).attr("disabled",true);
		});
		$("#tr_time_B0001").html("<td>브룩필드홀</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>");
		$("#tr_time_R0001").html("<td>IFC Hall 301</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>");
		$("#tr_time_R0002").html("<td>IFC Hall 302</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>");
		$("#tr_time_R0003").html("<td>IFC Hall 303</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>");
		$("#tr_time_R0004").html("<td>IFC Hall 304</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>");

		fnDaySelReset();
	}

	// 날 선택 리셋
	function fnDaySelReset() {
		$("input:checkbox[id*=chk_room_]:checked").each(function(idx,item) {
			$(item).attr("checked",false);
		});
		$("div[id*=div_room_]").each(function(idx,item) {
			$(item).removeClass("check");
			$(item).removeClass("disabled_v01");
		});
		$("tr[id*=tr_time_]").each(function(idx,item) {
			$(item).addClass("disabled");
		});
		$("#td_info_room").html("");
		$("div[id*=div_info_]").each(function(idx,item) {
			$(item).removeClass("check");
		});

		fnInfoReset();
	}

	// 정보 리셋
	function fnInfoReset() {
		$("#td_info_time").html("");
		$("#ORI_AMOUNT,#DISCOUNT,#ETC_AMOUNT,#FREE_TIME,#TOTAL_AMOUNT").val("");
		$("#spn_ori_amount,#spn_total_amount").html("0<em>원</em>");
		$("#sel_free_time").val("0");
		$("button[id*=btn_etc]").each(function(idx,item) {
			$(item).removeClass("on");
		});
		fnPolyClick("N",1);
		fnEtcClick("N",1);
	}

	// 예약하기
	function fnReservation() {
		$("#R_QTY").val(fnGetRoomCnt());
		$("#T_QTY").val(fnGetTimeCnt($("#R_QTY").val()));

		if ($("#DAY").val() == "") {
			alert("예약 날짜를 선택해주시기 바랍니다.");
			var val = $("#div_target1").offset();
			$("body, html").animate({scrollTop:val.top},1000);
			return true;
		}
		else if ($("#R_QTY").val() == 0) {
			alert("회의실을 선택해주시기 바랍니다.");
			var val = $("#div_target2").offset();
			$("body, html").animate({scrollTop:val.top},1000);
			return true;
		}
		else if ($("#T_QTY").val() == 0) {
			alert("예약시간을 선택해주시기 바랍니다.");
			var val = $("#div_target3").offset();
			$("body, html").animate({scrollTop:val.top},1000);
			return true;
		}

		if ($("#sel_free_time").val() > Number($("#R_QTY").val()) * Number($("#T_QTY").val())) {
			alert("총 유닛보다 무료이용시간을 더 사용할 수 없습니다.");
			$("#sel_free_time").val("0");
			return true;
		}
		else
			$("#FREE_TIME").val($("#sel_free_time").val());

		var roomNos = "";
		var roomNames = "";
		var roomPick = "";
		var reserveTimes = "";

        // 룸아이디값과 룸네임을 정의
        var arrRoomNames = new Array(',브룩필드홀', ',IFC Hall 301', ',IFC Hall 302', ',IFC Hall 303', ',IFC Hall 304');
        $("input:checkbox[id*=chk_room_]").each(function(idx,item) {
          if ($(item).is(":checked")) {
            roomNames = roomNames + arrRoomNames[idx]
            roomPick  = $(item).val();
            roomNos   = roomNos + "," + $(item).val();
          }
        });
        roomNos   = (roomNos.substring(0,1) == ",") ? roomNos.substring(1, roomNos.length) : '';
        roomNames = (roomNames.substring(0,1) == ",") ? roomNames.substring(1, roomNames.length) : '';

        // 관리자 페이지의 경우 7시부터 예약가능함으로 idx+7로 처리.
		$("td[id*=td_time_" + roomPick + "]").each(function(idx,item) {
			if ($(item).hasClass("selected")) {
				reserveTimes = reserveTimes + "," + (idx+7);
			}
		});
        reserveTimes = (reserveTimes.substring(0,1) == ",") ? reserveTimes.substring(1, reserveTimes.length) : ''

		$("#ROOM_NOS").val(roomNos);
		$("#ROOM_NAMES").val(roomNames);
		$("#RESERVE_TIMES").val(reserveTimes);

		/*
		alert($("#UID").val());
		alert($("#USER_NAME").val());
		alert($("#AUTHORITY").val());
		alert($("#SUBJECT").val());
		alert($("#RESERVE_NAME").val());
		alert($("#P_COUNT").val());
		alert($("#TELNUM").val());
		alert($("#FAXNUM").val());
		alert($("#MOBILENUM").val());
		alert($("#EMAIL").val());
		alert($("#REQUIREMENT").val());
		alert($("#DESCRIPTION").val());
		alert($("#YEAR").val());
		alert($("#MONTH").val());
		alert($("#DAY").val());
		alert($("#R_QTY").val());
		alert($("#T_QTY").val());
		alert($("#POLYCOM").val());
		alert($("#ETC").val());
		alert($("#ORI_AMOUNT").val());
		alert($("#DISCOUNT").val());
		alert($("#ETC_AMOUNT").val());
		alert($("#FREE_TIME").val());
		alert($("#TOTAL_AMOUNT").val());
		alert($("#ROOM_NOS").val());
		alert($("#ROOM_NAMES").val());
		alert($("#RESERVE_TIMES").val());
		alert($("#REG_UID").val());
		*/

		$("#index_loading_overlay").show();

		// DB
		var reserve_no = "";
		$.ajax ({
			type : "POST",
			url : "./ajax/reservation_proc.asp",
			dataType : "json",
			data : ({	"UID" : $("#UID").val(),
						"USER_NAME" : $("#USER_NAME").val(),
						"SUBJECT" : $("#SUBJECT").val(),
						"RESERVE_NAME" : $("#RESERVE_NAME").val(),
						"P_COUNT" : $("#P_COUNT").val(),
						"TELNUM" : $("#TELNUM").val(),
						"FAXNUM" : $("#FAXNUM").val(),
						"MOBILENUM" : $("#MOBILENUM").val(),
						"EMAIL" : $("#EMAIL").val(),
						"REQUIREMENT" : $("#REQUIREMENT").val(),
						"DESCRIPTION" : $("#DESCRIPTION").val(),
                        "FILMING_EQUIPMENT" : $("#FILMING_EQUIPMENT").val(),
						"YEAR" : $("#YEAR").val(),
						"MONTH" : $("#MONTH").val(),
						"DAY" : $("#DAY").val(),
						"R_QTY" : $("#R_QTY").val(),
						"T_QTY" : $("#T_QTY").val(),
						"POLYCOM" : $("#POLYCOM").val(),
						"ETC" : $("#ETC").val(),
						"ORI_AMOUNT" : $("#ORI_AMOUNT").val(),
						"DISCOUNT" : $("#DISCOUNT").val(),
						"ETC_AMOUNT" : $("#ETC_AMOUNT").val(),
						"FREE_TIME" : $("#FREE_TIME").val(),
						"TOTAL_AMOUNT" : $("#TOTAL_AMOUNT").val(),
						"ROOM_NOS" : $("#ROOM_NOS").val(),
						"ROOM_NAMES" : $("#ROOM_NAMES").val(),
						"RESERVE_TIMES" : $("#RESERVE_TIMES").val(),
						"REG_UID" : $("#REG_UID").val(),
						"mode" : "reservation"
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == "") {
						alert(error_msg);
					}
					else {
						alert("예약이 완료되었습니다.\n\n예약번호 : " + data.result);
						reserve_no = data.result;
						location.href = "reservation_list.asp";
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}

	function fnGoBack() {
		get_link = $("#list_link_hidden").val();
		location.href = get_link;
	}
</script>
<link rel="stylesheet" type="text/css" href="css/style.css" />

<input type="hidden" id="UID" name="UID" value="<%=UID%>" />
<input type="hidden" id="USER_NAME" name="USER_NAME" value="<%=USER_NAME%>" />
<input type="hidden" id="AUTHORITY" name="AUTHORITY" value="<%=AUTHORITY%>" />
<input type="hidden" id="SUBJECT" name="SUBJECT" value="<%=SUBJECT%>" />
<input type="hidden" id="RESERVE_NAME" name="RESERVE_NAME" value="<%=RESERVE_NAME%>" />
<input type="hidden" id="P_COUNT" name="P_COUNT" value="<%=P_COUNT%>" />
<input type="hidden" id="TELNUM" name="TELNUM" value="<%=TELNUM%>" />
<input type="hidden" id="FAXNUM" name="FAXNUM" value="<%=FAXNUM%>" />
<input type="hidden" id="FILMING_EQUIPMENT" name="FILMING_EQUIPMENT" value="<%=FILMING_EQUIPMENT%>" />
<input type="hidden" id="MOBILENUM" name="MOBILENUM" value="<%=MOBILENUM%>" />
<input type="hidden" id="EMAIL" name="EMAIL" value="<%=EMAIL%>" />
<input type="hidden" id="REQUIREMENT" name="REQUIREMENT" value="<%=FnBR(REQUIREMENT)%>" />
<input type="hidden" id="DESCRIPTION" name="DESCRIPTION" value="<%=FnBR(DESCRIPTION)%>" />
<input type="hidden" id="YEAR" name="YEAR" value="" />
<input type="hidden" id="MONTH" name="MONTH" value="" />
<input type="hidden" id="DAY" name="DAY" value="" />
<input type="hidden" id="R_QTY" name="R_QTY" value="" />
<input type="hidden" id="T_QTY" name="T_QTY" value="" />
<input type="hidden" id="POLYCOM" name="POLYCOM" value="" />
<input type="hidden" id="ETC" name="ETC" value="" />
<input type="hidden" id="ORI_AMOUNT" name="ORI_AMOUNT" value="" />
<input type="hidden" id="DISCOUNT" name="DISCOUNT" value="" />
<input type="hidden" id="ETC_AMOUNT" name="ETC_AMOUNT" value="" />
<input type="hidden" id="FREE_TIME" name="FREE_TIME" value="" />
<input type="hidden" id="TOTAL_AMOUNT" name="TOTAL_AMOUNT" value="" />
<input type="hidden" id="ROOM_NOS" name="ROOM_NOS" value="" />
<input type="hidden" id="ROOM_NAMES" name="ROOM_NAMES" value="" />
<input type="hidden" id="RESERVE_TIMES" name="RESERVE_TIMES" value="" />
<input type="hidden" id="REG_UID" name="REG_UID" value="<%=strAdminId%>" />


<dl class="rmrWrap">
	<dt>step01. 날짜선택</dt>
	<dd class="settingBox">
		<div class="boxL">
			<div class="stitWrap">
				<strong class="titS">예약 월 선택</strong>
			</div>
			<div class="selectYM">
				<div class="textYM">
					<span class="year"><%=Year(strNOW)%></span>년
					<span class="month"><%=Month(strNOW)%></span>월
				</div>
				<ul id="ul_Month">
					<%

					Dim strDay, endDay, iCnt, resMonth()

					'시작일과 마지막일
					strDay  = now()
					endDay  = DateAdd("d", 120, now())

					'표시되어야 하는 월수 계산하여 ReDim
					iCnt = DateDiff("m", strDay, endDay)
					ReDim resMonth(iCnt)

					'예약가능한 월 정보를 저장
					For i=0 To (iCnt)
                        resMonth(i) = Year(strDay) & "-" & Right("0" & Month(strDay)+i, 2)
					    '년도가 바뀔경우
                        If CInt(Right(resMonth(i), 2)) > 12 Then
					        resMonth(i) = (Left(resMonth(i), 4)+1) & "-0" & (Right(resMonth(i), 2)-12)
					    End If
					Next

					'maxMonth = 12
					'If 12 - Month(strNOW) > 4 Then
					''	maxMonth = Month(strNOW) + 4
					'End If
					'For i = Month(strNOW) To maxMonth
					For i = 0 To iCnt
					%>
						<!-- <li id="li_cal_month<%=i%>"><button type="button" onClick="fnMonthChg('2018', '<%=i%>');"><%=i%>월</button></li> -->
						<li id="li_cal_month<%=Right(resMonth(i), 2)%>"><button type="button" onClick="fnMonthChg('<%=Left(resMonth(i), 4)%>', '<%=Right(resMonth(i), 2)%>');"><%=CInt(Right(resMonth(i), 2))%>월</button></li>
					<%
					Next
					%>

				</ul>
			</div>

			<div class="stitWrap">
				<strong class="titS">예약 정보 (<em class="infoYM" id="em_cal_YM"></em>)</strong>
			</div>
			<ul class="usingInfoT">
				<li>
					<span class="tits">
						월 예약 가능시간 <em><%=objRs("LIMIT_TIME")%>시간기준</em>
					</span>
					<span class="conts">
						<em id="em_cal_limit_time"></em>시간
					</span>
				</li>
				<li>
					<span class="tits">
						월 잔여 무상시간 <em><%=objRs("MONTHLY_FREE_TIME")%>시간기준</em>
					</span>
					<span class="conts">
						<em id="em_cal_free_time"></em>시간
					</span>
				</li>
			</ul>
		</div>
		<div class="boxR" id="div_target1">
			<div class="stitWrap">
				<strong class="titS">예약 날짜 선택</strong>
			</div>
			<div class="calendarWrap">
				<div class="hgroup">
					<p>SUN</p>
					<p>MON</p>
					<p>TUE</p>
					<p>WED</p>
					<p>THU</p>
					<p>FRI</p>
					<p>SAT</p>
				</div>
				<ul class="dgroup" id="ul_cal_day"></ul>
			</div>
		</div>
	</dd>

	<dt>step02. 참석인원 / 회의실 / 예약시간 선택</dt>
	<dd class="settingBox">
		<div class="stitWrap" id="div_target2">
			<strong class="titS">회의실 선택</strong>
			<div class="posR" style="display:none">
				<button type="button" class="nbtnG02">요금표 보기</button>
				<button type="button" href="#" class="nbtnG02">회의실 안내</button>
			</div>
		</div>

		<div class="roomSelectBox" id="div_room">
			<div class="stateGuide">
				<span class="state01">예약가능</span>
				<span class="state02">이미 예약 중 (예약 불가)</span>
				<span class="state03">예약 불가</span>
				<span class="state04">선택완료</span>
			</div>

			<div class="meetingRoom">
				<div class="room01" id="div_room_B0001">
					<span>
						<input type="checkbox" id="chk_room_B0001" disabled onClick="fnRoomChk('B0001');" value="B0001" />
						<label for="chk_room_B0001">브룩필드홀 - 47석</label>
					</span>
				</div>
				<div class="room02" id="div_room_R0001">
					<span>
						<input type="checkbox" id="chk_room_R0001" disabled onClick="fnRoomChk('R0001');" value="R0001" />
						<label for="chk_room_R0001">IFC Hall 301 40석</label>
					</span>
				</div>
				<div class="room03" id="div_room_R0002">
					<span>
						<input type="checkbox" id="chk_room_R0002" disabled onClick="fnRoomChk('R0002');" value="R0002" />
						<label for="chk_room_R0002">IFC Hall 302 40석</label>
					</span>
				</div>
				<div class="room04" id="div_room_R0003">
					<span>
						<input type="checkbox" id="chk_room_R0003" disabled onClick="fnRoomChk('R0003');" value="R0003" />
						<label for="chk_room_R0003">IFC Hall 303 40석</label>
					</span>
				</div>
				<div class="room05" id="div_room_R0004">
					<span>
						<input type="checkbox" id="chk_room_R0004" disabled onClick="fnRoomChk('R0004');" value="R0004" />
						<label for="chk_room_R0004">IFC Hall 304 20석</label>
					</span>
				</div>
			</div>
		</div>

		<ul class="icotxtBox">
			<li>참석인원보다 많은 회의실을 예약하고자 하는 경우 참석인원을 변경해주세요.</li>
			<li>브룩필드홀과 IFC홀은 동시에 예약 불가능 합니다. (브룩필드홀은 무료 이용시간 사용 불가)</li>
		</ul>

		<div class="stitWrap">
			<strong class="titS" id="div_target3">예약시간 선택</strong>
			<div class="stateGuide">
				<span class="state01">예약가능</span>
				<span class="state02">예약 중 (예약 불가)</span>
				<span class="state03">클리닝 타임 (예약 불가)</span>
				<span class="state04">선택완료</span>
			</div>
		</div>

		<div class="timeTbl">
			<table>
				<caption>회의실 예약시간 정보</caption>
				<colgroup>
					<col style="width:12%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
                    <col style="width:5.5%;" />
				</colgroup>
				<thead>
					<tr>
						<th>회의실</th>
                        <th>&nbsp;&nbsp;07:00<br/>~08:00</th>
                        <th>&nbsp;&nbsp;08:00<br/>~09:00</th>
                        <th>&nbsp;&nbsp;09:00<br/>~10:00</th>
						<th>&nbsp;&nbsp;10:00<br/>~11:00</th>
						<th>&nbsp;&nbsp;11:00<br/>~12:00</th>
						<th>&nbsp;&nbsp;12:00<br/>~13:00</th>
						<th>&nbsp;&nbsp;13:00<br/>~14:00</th>
						<th>&nbsp;&nbsp;14:00<br/>~15:00</th>
						<th>&nbsp;&nbsp;15:00<br/>~16:00</th>
						<th>&nbsp;&nbsp;16:00<br/>~17:00</th>
						<th>&nbsp;&nbsp;17:00<br/>~18:00</th>
                        <th>&nbsp;&nbsp;18:00<br/>~19:00</th>
                        <th>&nbsp;&nbsp;19:00<br/>~20:00</th>
                        <th>&nbsp;&nbsp;20:00<br/>~21:00</th>
                        <th>&nbsp;&nbsp;21:00<br/>~22:00</th>
                        <th>&nbsp;&nbsp;22:00<br/>~23:00</th>
                        <th>&nbsp;&nbsp;23:00<br/>~24:00</th>
					</tr>
				</thead>
				<tbody>
					<tr id="tr_time_B0001"></tr>
					<tr id="tr_time_R0001"></tr>
					<tr id="tr_time_R0002"></tr>
					<tr id="tr_time_R0003"></tr>
					<tr id="tr_time_R0004"></tr>
				</tbody>
			</table>
		</div>

		<ul class="icotxtBox">
			<li>최소 2시간 이상, 이후 1시간 단위로 추가 가능</li>
			<li>기존 예약 된 회의 전/후 1시간은 예약 불가</li>
			<li>24시간 이전 예약 불가 (당일 예약 불가)</li>
		</ul>

	</dd>
	<dt>step03. 예약정보 확인</dt>
	<dd class="settingBox">
		<div class="stitWrap">
			<strong class="titS">선택한 예약정보</strong>
		</div>

		<div class="tblStyle1">
			<table>
				<caption>예약 정보</caption>
				<colgroup>
					<col style="width:17%">
					<col style="width:37%">
					<col style="width:15%">
					<col style="width:*%">
				</colgroup>
				<tbody>
					<tr>
						<th scope="row"><span>예약날짜</span></th>
						<td id="td_info_reserve_dt"></td>
						<th scope="row"><span>예약시간</span></th>
						<td id="td_info_time"></td>
					</tr>
					<tr>
						<th scope="row"><span>회의실</span></th>
						<td id="td_info_room"></td>
						<th scope="row"><span>참석인원</span></th>
						<td id="td_info_p_count"></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="roomSelectBox cplt">
			<div class="meetingRoom">
				<div class="room01" id="div_info_B0001"></div>
				<div class="room02" id="div_info_R0001"></div>
				<div class="room03" id="div_info_R0002"></div>
				<div class="room04" id="div_info_R0003"></div>
				<div class="room05" id="div_info_R0004"></div>
			</div>
		</div>
	</dd>
	<dt>step04. 기자재 선택 및 요금정보 확인</dt>
	<dd class="settingBox">
		<div class="tblStyle1">
			<table>
				<caption>예약 정보</caption>
				<colgroup>
					<col style="width:17%">
					<col style="width:*%">
				</colgroup>
				<tbody>
					<tr>
						<th scope="row"><span>폴리콤(3자통화)</span></th>
						<td>
							<div class="gearList">
								<button class="nbtnSel" id="btn_poly1" onClick="fnPolyClick('N',1);">사용안함</button>
								<button class="nbtnSel" id="btn_poly2" onClick="fnPolyClick('L',2);">국내전화 사용</button>
								<button class="nbtnSel" id="btn_poly3" onClick="fnPolyClick('G',3);">국제전화 사용</button>
							</div>
						</td>
					</tr>
					<tr>
						<th scope="row"><span>외부기자재 반입</span></th>
						<td>
							<div class="gearList">
								<button class="nbtnSel" id="btn_etc1" onClick="fnEtcClick('N',1);">반입안함</button>
								<button class="nbtnSel" id="btn_etc2" onClick="fnEtcClick('Y',2);">반입함</button>
							</div>
						</td>
					</tr>
					<tr>
						<th scope="row"><span>기본요금</span></th>
						<td>
							<span class="price" id="spn_ori_amount">0<em>원</em></span> <span id="spn_discount"></span>
						</td>
					</tr>
					<tr>
						<th scope="row"><span>무료이용시간</span></th>
						<td id ="td_free_time">
							<!-- <select id="sel_free_time" name="sel_free_time" onChange="fnFreeTimeChg(this.value);">>
								<option value="0" selected>0시간</option>
								<% For i = 2 To objRs("USE_FREE_TIME") %>
									<option value="<%=i%>"><%=i%>시간</option>
								<% Next %>
							</select>
							<span class="gearNoti">현재 <%=objRs("USE_FREE_TIME")%>시간 까지 이용 가능</span> -->
						</td>
					</tr>
				</tbody>
			</table>
			<table class="totalP">
				<caption>예약 정보</caption>
				<colgroup>
					<col style="width:17%">
					<col style="width:*%">
				</colgroup>
				<tbody>
					<tr>
						<th><span>최종금액</span></th>
						<td><span class="priceT" id="spn_total_amount">0<em>원</em></span></td>
					</tr>
				</tbody>
			</table>
		</div>
	</dd>
</dl>
<div class="btm_btn_wrap" style="width:944px;margin:20px 0px 100px 0px">
	<ul>
		<li>
			<input type="button" class="btn btn-danger btn-sm" id="btn_list" name="btn_list" value="선택 초기화" onClick="fnReset();" />&nbsp;
		</li>
		<li class="btm_btn_right">
			<input type="button" class="btn btn-warning btn-sm" id="btn_list" name="btn_list" value="이전" onClick="fnGoBack();" />&nbsp;
			<input type="button" class="btn btn-primary btn-sm" id="btn_add" name="btn_add" value="예약" onClick="fnReservation();" />
			<input type="hidden" id="list_link_hidden" value="reservation_new.asp<%=list_link%>"/>
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->
