<!--#include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%
Dim countQuery, selectQuery, num_count, list_array, page_result, t_page, query_where

'search string
query_where = ""

'예약번호
If Request("reserve_no")&"" <> "" Then
    query_where = query_where & " and RESERVE_NO = '" & Request("reserve_no") & "' "
End If

'메일주소
If Request("receiver_email")&"" <> "" Then
    query_where = query_where & " and RECEIVER_EMAIL = '" & Request("receiver_email") & "' "
End If

'이용날짜
If Request("start_date")&"" <> "" And Request("end_date")&"" <> "" Then
    query_where = query_where & " and CONVERT(varchar(10), REQ_DT, 112) BETWEEN '" & Replace(Request("start_date"),"-","") & "' AND '" & Replace(Request("end_date"),"-","") & "' "
End If

'발송상태
If Request("send_status")&"" <> "" Then
    query_where = query_where & " and SEND_STATUS = '" & Request("send_status") & "' "
End If


// counting list
countQuery = "SELECT COUNT(*) AS COUNT FROM dbo.TBL_EMAIL_REQUEST WHERE 1=1 " & query_where

set objConn = OpenDBConnection()
set objRs = SendQuery(objConn,countQuery)
total_count = objRs("count")
num_count = total_count - (page - 1) * page_limit
set objRs = Nothing

// get list
selectQuery = "SELECT TOP 10 EMAIL_ID, RESERVE_NO, REQ_UID, RECEIVER_EMAIL, SUBJECT, REQ_DT, SEND_STATUS FROM dbo.TBL_EMAIL_REQUEST WHERE EMAIL_ID NOT IN (SELECT TOP "& current_count &" EMAIL_ID FROM dbo.TBL_EMAIL_REQUEST WHERE 1=1 " & query_where & "ORDER BY EMAIL_ID DESC)" & query_where & "ORDER BY EMAIL_ID DESC "
set objRs = SendQuery(objConn,selectQuery)

t_page = page
page_result = page_create(total_count, t_page, page_limit, page_length)

%>
<script type="text/javascript">

	$(document).ready(function() {
		<% If Request("start_date")&"" <> "" Then %>
			$("#start_date").val($.datepicker.formatDate('yy-mm-dd', new Date('<%=Request("start_date")%>')));
		<% End If %>
		<% If Request("end_date")&"" <> "" Then %>
			$("#end_date").val($.datepicker.formatDate('yy-mm-dd', new Date('<%=Request("end_date")%>')));
		<% End If %>
	});

	function fnSearch() {
		var startDate = $("#start_date").val();
		var endDate = $("#end_date").val();

		if ((startDate != "" && endDate == "") || (startDate == "" && endDate != "")) {
				alert("등록일 구간을 바르게 설정하여 주십시오.");
				return true;
		}
		if (startDate != "" && endDate != "") {
			var startArray = startDate.split('-');
			var endArray = endDate.split('-');
			var start_date = new Date(startArray[0], startArray[1], startArray[2]);
			var end_date = new Date(endArray[0], endArray[1], endArray[2]);
			if(start_date.getTime() > end_date.getTime()) {
				alert("종료날짜보다 시작날짜가 작아야합니다.");
				return true;
			}
		}

		document.location.href = "mail_report.asp?reserve_no=" + $("#reserve_no").val() + "&receiver_email=" + $("#receiver_email").val() + "&send_status=" + $("#send_status").val() + "&start_date=" + startDate + "&end_date=" + endDate;
	};

	function fnResend(emailid, status) {
		
    if (status == 'Y') {
			alert("이미 발송된 메일입니다.");
			return false;
		}

		if (!confirm("메일을 재발송 하시겠습니까?")) return true;
		$("#index_loading_overlay").show();

		$.ajax({
			type: "POST",
			url : "./ajax/reservation_proc.asp",
			dataType : "json",
			data : ({
				"mode" : "resend_mail",
				"email_id" : emailid
			}),
			success: function(data) {
				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == "") {
						alert(error_msg);
					}
					else {
						alert("재발송 요청을 완료하였습니다. ");
						get_link = $("#list_link_hidden").val();
						location.href = get_link;
					}
				}
			},
			error: function(data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}

</script>
<div id="reservation_list">
  <div class="search_wrap" style="margin-top:20px;">
		<div class="table_list">
			<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="메일 발송현황">
				<caption>메일 발송현황</caption>
				<colgroup>
					<col width="15%"><col width="35%"><col width="15%"><col width="35%">
				</colgroup>
				<tbody>
					<tr>
						<th>예약번호</th>
						<td style="text-align:left">
							<input type="text" class="form-control input-sm width_300" id="reserve_no" name="reserve_no" value="<%=Request("RESERVE_NO")%>" placeholder="예약번호" onKeydown="javascript:if(event.keyCode == 13){fnSearch();return false; }" />
						</td>
						<th>메일주소</th>
						<td style="text-align:left">
							<input type="text" class="form-control input-sm width_300" id="receiver_email" name="receiver_email" value="<%=Request("RECEIVER_EMAIL")%>" placeholder="이메일주소" onKeydown="javascript:if(event.keyCode == 13){fnSearch();return false; }" />
						</td>
					</tr>
					<tr>
						<th>발송상태</th>
						<td style="text-align:left">
							<select id="send_status" name="send_status" class="form-control input-sm width_100">
								<option value="">전체</option>
								<option value="Y" <%If Request("SEND_STATUS")&""="Y" Then%>selected<%End If%>>발송완료</option>
								<option value="F" <%If Request("SEND_STATUS")&""="F" Then%>selected<%End If%>>미발송</option>
							</select>
						</td>
						<th>메일발송날짜</th>
						<td style="text-align:left">
								<input type="text" class="form-control input-sm width_150" id="start_date" name="start_date" value="<%=start_date%>" style="background-color:#fff;cursor:default;" placeholder="YYYY-MM-DD" readonly />
								<input type="text" class="form-control input-sm width_150" id="end_date" name="end_date" value="<%=end_date%>" style="background-color:#fff;cursor:default;" placeholder="YYYY-MM-DD" readonly />
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<input type="hidden" id="list_link_hidden" value="<%=list_link%>" />
		<div class="btm_btn_wrap">
				<input type="button" class="btn btn-info btn-sm" id="btn_search" name="btn_search" value="메일 발송내역 조회" onClick="fnSearch();" />
		</div>
	</div>

	<div class="top_btn_wrap">
		<ul>
			<li>
				전체 <span style="color:#357ebd;font-weight:bold;"><%=total_count%></span> 건
			</li>
		</ul>
	</div>
	<div class="table_list">
		<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="Admin List">
			<caption>예약목록</caption>
			<colgroup>
				<col width="10%">
        <col width="10%">
        <col width="15%">
        <col width="40%">
        <col width="15%">
        <col width="10%">
			</colgroup>
			<thead>
				<tr>
					<th>예약번호</th>
					<th>요청아이디</th>
					<th>이메일주소</th>
					<th>제목</th>
					<th>메일발송날짜</th>
					<th>메일발송상태</th>
				</tr>
			</thead>
			<tbody>
			<% If objRs.EOF Then %>
				<tr>
					<td colspan="12" align="center">
						리스트가 없습니다.
					</td>
				</tr>
			<%
			Else
				For k = 0 to objRs.RecordCount - 1
			%>
				<tr>
					<td class="text-center"><%=objRs("RESERVE_NO")%></td>
					<td class="text-center"><%=objRs("REQ_UID")%></td>
					<td><%=objRs("RECEIVER_EMAIL")%></td>
					<td><%=objRs("SUBJECT")%></td>
					<td class="text-center"><%=FnFormatDateTime(objRs("REQ_DT"))%></td>
					<td class="text-center">
				  <%
						If objRs("SEND_STATUS")&"" = "Y" Then
						  btnClass = "btn-success"
							strSendStatus = "발송완료"
						ElseIf objRs("SEND_STATUS")&"" = "F" Then
						  btnClass = "btn-danger"
						  strSendStatus = "발송실패"
						Else
						  btnClass = "btn-warning"
							strSendStatus = "발송대기"
						End If
						%>
						<%'="[" & objRs("SEND_STATUS") & "]"%>
						
						<input type="button" class="btn <%=btnClass%> btn-sm" id="btnSendStatus" name="btn-send-status" value="<%=strSendStatus%>" onClick="fnResend('<%=objRs("EMAIL_ID")%>', '<%=objRs("SEND_STATUS")%>');" />
						
					</td>
				</tr>
			<%
				objRs.MoveNext
				Next
			End If
			%>
			</tbody>
		</table>
	</div>
	<div class="page_wrap">
		<!-- #include file = "page_template.asp" -->
	</div>
</div>

<!--#include file = "index_footer.asp" -->
