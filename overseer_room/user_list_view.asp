<!--#include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%
Dim selectQuery, uid
uid = Request.QueryString("uid")
selectQuery = "select * from TBL_USER_INFO where UID = '"& uid &"'"

set objRs = SendQuery(objConn,selectQuery)

If objRs.EOF Then
%>
	<script>
		alert("비정상적인 접속입니다.");
		location.href="user_list.asp<%=list_link%>";
	</script>
<%
Else
    strAuthority = ""
    If objRs("AUTHORITY")&"" = "U" Then
        strAuthority = "Tenant"
    ElseIf objRs("AUTHORITY")&"" = "N" Then
        strAuthority = "NonTenant"
    End If

	strDel = ""
	If objRs("USE_FLAG")&"" = "N" Then
		strDel = " (탈퇴)"
	End If
	strCdVenr = ""
	If objRs("CD_VENR")&"" <> "" Then
		strCdVenr = " (" & objRs("CD_VENR") & ")"
	End If
End If
%>
<script type="text/javascript">
	function fnGoBack() {
		get_link = $("#list_link_hidden").val();
		location.href = get_link;
	}

	<% If strEditYN <> "N" Then %>
		function fnEdit(objVal) {
			location.href = "user_list_edit.asp?uid=" + objVal;
		}
	<% End If %>
</script>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="입주사 상세정보">
		<caption>입주사 상세정보</caption>
		<colgroup>
			<col width="15%"><col width="35%"><col width="15%"><col width="35%">
		</colgroup>
		<tbody>
            <tr>
                <th style="height:39px">등급</th>
                <td colspan="3"><%=strAuthority%></td>
            </tr>
			<tr>
				<th style="height:39px">아이디</th>
				<td><%=objRs("UID")%><%=strCdVenr%><%=strDel%></td>
				<th>계정 등록일</th>
				<td><%=FnFormatDateTime(objRs("REG_DT"))%></td>
			</tr>
			<tr>
				<th style="height:39px">입주사명</th>
				<td><%=objRs("USER_NAME")%></td>
				<th>사업자 등록번호</th>
				<td><%=objRs("BUSINESS_RE_NO")%></td>
			</tr>
			<tr>
				<th style="height:39px">담당자명</th>
				<td><%=objRs("MANAGER_NAME")%></td>
				<th>전화번호</th>
				<td><%=FnSetNum(objRs("TELNUM"))%></td>
			</tr>
			<tr>
				<th style="height:39px">FAX</th>
				<td><%=FnSetNum(objRs("FAXNUM"))%></td>
				<th>휴대폰번호</th>
				<td><%=FnSetNum(objRs("MOBILENUM"))%></td>
			</tr>
			<tr>
				<th style="height:39px">이메일</th>
				<td><%=objRs("EMAIL")%></td>
				<th>부서</th>
				<td><%=objRs("DEPT_NAME")%></td>
			</tr>
			<tr>
				<th style="height:39px">직책</th>
				<td><%=objRs("DEPT_LEVEL")%></td>
				<th>월별무상제공시간</th>
				<td><%=objRs("MONTHLY_FREE_TIME")%> 시간</td>
			</tr>
			<tr>
				<th style="height:39px">메모</th>
				<td colspan="3"><%=FnBR(objRs("DESCRIPTION"))%></td>
			</tr>
			<tr>
				<th style="height:39px">최종 접속일</th>
				<td><%=FnFormatDateTime(objRs("LAST_LOGIN_DT"))%></td>
				<th>최종 수정일</th>
				<td><%=FnFormatDateTime(objRs("UPD_DT"))%></td>
			</tr>
			<% If objRs("USE_FLAG")&"" <> "Y" Then %>
				<tr>
					<th style="height:39px">탈퇴 처리일</th>
					<td><%=FnFormatDateTime(objRs("DEL_DT"))%></td>
					<th>탈퇴 처리자</th>
					<td><%=objRs("DEL_UID")%></td>
				</tr>
			<% End If %>
		</tbody>
	</table>


    <br/><h5>- <%=Year(NOW)%>년 월별 잔여 무상시간</h5>
    <table class="tbl_list free_time">
        <tbody>
            <tr>
            <% For i=0 To 11 %>
                <th><%=(i+1)%> 월</th>
            <% Next %>
            </tr>
            <tr>
            <%
                '월별 잔여 무상시간 출력
                Dim arrRemainingFreeTime(11)

                selectQuery = "SELECT FT_YEAR, FT_MONTH, REMAINING_FREE_TIME FROM TBL_FREE_TIME WHERE FT_UID = '"& objRs("UID") &"' AND FT_YEAR = YEAR(GETDATE())"
                set objRsT = SendQuery(objConn,selectQuery)

                For i = 0 to objRsT.RecordCount - 1
                    arrRemainingFreeTime(CInt(objRsT("FT_MONTH"))-1) = objRsT("REMAINING_FREE_TIME")
                    objRsT.MoveNext
                Next

                For i=0 to 11
                    If IsEmpty(arrRemainingFreeTime(i)) Then
                        Response.Write "<td>0</td>"
                    Else
                        Response.Write "<td>" & arrRemainingFreeTime(i) & "</td>"
                    End If
                Next
            %>
            </tr>
        </tbody>
    </table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
		</li>
		<li class="btm_btn_right">
			<input type="button" class="btn btn-warning btn-sm" id="btn_list" name="btn_list" value="목록" onClick="fnGoBack();" />&nbsp;
			<% If strEditYN <> "N" Then %>
				<input type="button" class="btn btn-primary btn-sm" id="btn_edit" name="btn_edit" value="수정" onClick="fnEdit('<%=objRs("UID")%>');" />
			<% End If %>
			<input type="hidden" id="list_link_hidden" value="user_list.asp<%=list_link%>" />
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->
