<!--#include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%
Dim selectQuery, uid
uid = Request.QueryString("uid")

selectQuery = "select * from TBL_USER_INFO where UID = '"& uid &"'"

set objRs = SendQuery(objConn,selectQuery)

If objRs.EOF Then
%>
	<script>
		alert("비정상적인 접속입니다.");
		location.href="user_list.asp<%=list_link%>";
	</script>
<%
Else
	If objRs("BUSINESS_RE_NO")&"" <> "" Then
		arrBUSINESS_RE_NO = Split(objRs("BUSINESS_RE_NO")&"--", "-")
		strBUSINESS_RE_NO1 = arrBUSINESS_RE_NO(0)
		strBUSINESS_RE_NO2 = arrBUSINESS_RE_NO(1)
		strBUSINESS_RE_NO3 = arrBUSINESS_RE_NO(2)
	End If
End If
%>

<script type="text/javascript">
	function fnGoBack() {
		get_link = $("#list_link_hidden").val();
		location.href = get_link;
	}

	function chkPwd(str) {
		var reg_pwd = /^.*(?=.{8,16})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;

		if (str.length < 8 || str.length > 16) {
			return false;
		}

		if (!reg_pwd.test(str)) {
			return false;
		}
		return true;
	}

	function fnDel(objVal) {
		if (!confirm("탈퇴 하시겠습니까?"))
			return true;

		if ($("#index_loading_overlay").css("display") != 'none') {
			alert("탈퇴 중 입니다");
			return false;
		}

		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/user_proc.asp",
			dataType : "json",
			data : ({
						"mode" : "del",
						"UID" : objVal,
						"DEL_UID" : $("#hdn_uid").val()
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else
				{
					var error_msg = data.error_msg;
					if (data.result == 0) {
						alert("탈퇴되었습니다.");
						get_link = "user_list.asp";//$("#list_link_hidden").val();
						location.href = get_link;
					}
					else {
						alert(error_msg);
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}

	function fnEdit(objVal) {
		if ($("#AUTHORITY").val() == "U" && $("#CD_VENR").val() == "") {
			alert("업체코드를 입력해주세요.");
			$("#CD_VENR").focus();
			return true;
		}

		if ($("#PASSWORD").val() != "") {
			if (!chkPwd($.trim($("#PASSWORD").val()))) {
				alert("비밀번호를 확인하세요.\n\n영문,숫자를 혼합하여 8~16자 이내");
				$("#PASSWORD").focus();
				return true;
			}
		}

		if ($("#PASSWORD").val() != $("#PASSWORD_CHK").val()) {
			alert("수정할 비밀번호가 다릅니다.\n\n비밀번호를 확인해주세요.");
			$("#PASSWORD_CHK").focus();
			return true;
		}

		if ($("#AUTHORITY").val() == "U" && $("#USER_NM").val() == "") {
			alert("입주사명을 입력해주세요.");
			$("#USER_NM").focus();
			return true;
		}
		/*
		if ($("#BUSINESS_RE_NO1").val() == "") {
			alert("사업자 등록번호를 입력해주세요.");
			$("#BUSINESS_RE_NO1").focus();
			return true;
		}

		if ($("#BUSINESS_RE_NO2").val() == "") {
			alert("사업자 등록번호를 입력해주세요.");
			$("#BUSINESS_RE_NO2").focus();
			return true;
		}

		if ($("#BUSINESS_RE_NO3").val() == "") {
			alert("사업자 등록번호를 입력해주세요.");
			$("#BUSINESS_RE_NO3").focus();
			return true;
		}
		*/
		if ($("#MANAGER_NAME").val() == "") {
			alert("담당자명을 입력해주세요.");
			$("#MANAGER_NAME").focus();
			return true;
		}
		/*
		if ($("#TELNUM").val() == "") {
			alert("전화번호를 입력해주세요.");
			$("#TELNUM").focus();
			return true;
		}

		if ($("#FAXNUM").val() == "") {
			alert("팩스번호를 입력해주세요.");
			$("#FAXNUM").focus();
			return true;
		}

		if ($("#MOBILENUM").val() == "") {
			alert("휴대폰번호를 입력해주세요.");
			$("#MOBILENUM").focus();
			return true;
		}
		*/
		if ($("#EMAIL").val() == "") {
			alert("이메일을 입력해주세요.");
			$("#EMAIL").focus();
			return true;
		}
		/*else {
			var emailReg = new RegExp(/^([0-9a-zA-Z_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/);
			if (!emailReg.test($("#EMAIL").val())) {
				alert("이메일 주소가 잘못되었습니다.");
				$("#EMAIL").focus();
				return true;
			}
		}*/
		/*
		if ($("#DEPT_NAME").val() == "") {
			alert("부서를 입력해주세요.");
			$("#DEPT_NAME").focus();
			return true;
		}

		if ($("#DEPT_LEVEL").val() == "") {
			alert("직책을 입력해주세요.");
			$("#DEPT_LEVEL").focus();
			return true;
		}
		*/

		var newPassword = "";
		if ($("#PASSWORD").val() != "") {
			newPassword = hex_md5($("#PASSWORD").val());
		}

        if ("<%=objRs("authority")%>" == "U" && $("#AUTHORITY").val() == "N" ) {
            if (!confirm("NonTenant 등급의 경우 월별무상제공시간과 해당년도의 잔여 무상시간이 0으로 변경됩니다. 수정 하시겠습니까?"))
            return true;
        } else {
            if (!confirm("수정 하시겠습니까?"))
            return true;
        }

		if ($("#index_loading_overlay").css("display") != 'none') {
			alert("수정 중 입니다");
			return false;
		}
		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/user_proc.asp",
			dataType : "json",
			data : ({
						"mode" : "edit",
						"UID" : objVal,
						"CD_VENR" : $("#CD_VENR").val(),
						"PASSWORD" : newPassword,
                        "AUTHORITY" : $("#AUTHORITY").val(),
						"USER_NM" : $("#USER_NM").val(),
						"BUSINESS_RE_NO" : $("#BUSINESS_RE_NO1").val() + "-" + $("#BUSINESS_RE_NO2").val() + "-" + $("#BUSINESS_RE_NO3").val(),
						"MANAGER_NAME" : $("#MANAGER_NAME").val(),
						"TELNUM" : $("#TELNUM").val(),
						"FAXNUM" : $("#FAXNUM").val(),
						"MOBILENUM" : $("#MOBILENUM").val(),
						"EMAIL" : $("#EMAIL").val(),
						"DEPT_NAME" : $("#DEPT_NAME").val(),
						"DEPT_LEVEL" : $("#DEPT_LEVEL").val(),
						"DESCRIPTION" : $("#DESCRIPTION").val(),
						"UPD_UID" : $("#hdn_uid").val()
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == 0) {
						alert("수정되었습니다.");
						get_link = "user_list.asp";//$("#list_link_hidden").val();
						location.href = get_link;
					}
					else {
						alert(error_msg);
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}
</script>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="입주사 계정수정">
		<caption>입주사 계정수정</caption>
		<colgroup>
			<col width="15%"><col width="35%"><col width="15%"><col width="35%">
		</colgroup>
		<tbody>
            <tr>
                <th style="height:39px">등급</th>
                <td colspan="3">
                    <%If objRs("AUTHORITY")="U" Then%>Tenant<%Else%>NonTenant<%End If%>
                    <input type="hidden" id="AUTHORITY" name="AUTHORITY" value="<%=objRs("AUTHORITY")%>"/>
                </td>
            </tr>
			<tr>
				<th style="height:39px">아이디</th>
				<td><%=objRs("UID")%></td>
				<th>업체코드</th>
				<td><input id="CD_VENR" name="CD_VENR" type="text" class="width_150" value="<%=objRs("CD_VENR")%>" maxlength="30" style="ime-mode:disabled" onkeyup="this.value=this.value.replace(/[^A-Za-z0-9]/g,'');" <% If objRs("AUTHORITY") = "N" Then %> disabled <% End If %>/></td>
			</tr>
			<tr>
				<th>비밀번호</th>
				<td><input id="PASSWORD" name="PASSWORD" type="password" class="width_150" value="" maxlength="16" /><br><span>※ 영문 / 숫자 조합 8~16자리</span></td>
				<th>비밀번호 확인</th>
				<td><input id="PASSWORD_CHK" name="PASSWORD_CHK" type="password" class="width_150" value="" maxlength="16" /><br><span>※ 비밀번호를 한번 더 입력해주세요.</span></td>
			</tr>
			<tr>
				<th>입주사명</th>
				<td><input id="USER_NM" name="USER_NM" type="text" class="width_150" value="<%=objRs("USER_NAME")%>" maxlength="50"  <% If objRs("AUTHORITY") = "N" Then %> disabled <% End If %>/></td>
				<th>사업자 등록번호</th>
				<td>
					<input id="BUSINESS_RE_NO1" name="BUSINESS_RE_NO1" type="text" class="width_50" value="<%=strBUSINESS_RE_NO1%>" maxlength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /> -
					<input id="BUSINESS_RE_NO2" name="BUSINESS_RE_NO2" type="text" class="width_50" value="<%=strBUSINESS_RE_NO2%>" maxlength="2" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /> -
					<input id="BUSINESS_RE_NO3" name="BUSINESS_RE_NO3" type="text" class="width_100" value="<%=strBUSINESS_RE_NO3%>" maxlength="5" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" />
				</td>
			</tr>
			<tr>
				<th>담당자명</th>
				<td><input id="MANAGER_NAME" name="MANAGER_NAME" type="text" class="width_150" value="<%=objRs("MANAGER_NAME")%>" maxlength="50" /></td>
				<th>전화번호</th>
				<td><input id="TELNUM" name="TELNUM" type="text" class="width_150" value="<%=objRs("TELNUM")%>" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
			</tr>
			<tr>
				<th>FAX</th>
				<td><input id="FAXNUM" name="FAXNUM" type="text" class="width_150" value="<%=objRs("FAXNUM")%>" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
				<th>휴대폰번호</th>
				<td><input id="MOBILENUM" name="MOBILENUM" type="text" class="width_150" value="<%=objRs("MOBILENUM")%>" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
			</tr>
			<tr>
				<th>이메일</th>
				<td><input id="EMAIL" name="EMAIL" type="text" class="i_text50" value="<%=objRs("EMAIL")%>" maxlength="100" style="ime-mode:disabled" /></td>
				<th>부서</th>
				<td><input id="DEPT_NAME" name="DEPT_NAME" type="text" class="width_150" value="<%=objRs("DEPT_NAME")%>" maxlength="50" /></td>
			</tr>
			<tr>
				<th>직책</th>
				<td><input id="DEPT_LEVEL" name="DEPT_LEVEL" type="text" class="width_150" value="<%=objRs("DEPT_LEVEL")%>" maxlength="20" /></td>
				<th>월별무상제공시간</th>
				<td><%=objRs("MONTHLY_FREE_TIME")%> 시간
                    <!-- <input id="MONTHLY_FREE_TIME" name="MONTHLY_FREE_TIME" type="text" class="width_50" value="<%=objRs("MONTHLY_FREE_TIME")%>" maxlength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" />시간 --></td>
			</tr>
			<tr>
				<th>메모</th>
				<td colspan="3">
					<textarea id="DESCRIPTION" name="DESCRIPTION" type="text" style="height:80px;" maxlength="1000"><%=objRs("DESCRIPTION")%></textarea>
				</td>
			</tr>
			<tr>
				<th style="height:39px">계정 등록일</th>
				<td colspan="3"><%=FnFormatDateTime(objRs("REG_DT"))%></td>
			<tr>
			<tr>
				<th style="height:39px">최종 접속일</th>
				<td><%=FnFormatDateTime(objRs("LAST_LOGIN_DT"))%></td>
				<th>최종 수정일</th>
				<td><%=FnFormatDateTime(objRs("UPD_DT"))%></td>
			</tr>
		</tbody>
	</table>

    <!-- 월별 잔여 무상시간 -->
    <br/><h5>- <%=Year(NOW)%>년 월별 잔여 무상시간</h5>
    <table class="tbl_list free_time" border="1" cellspacing="0" summary="월별 잔여 무상시간">
        <tbody>
            <tr>
                <% For i=0 to 11 %>
                    <th scope="row"><%=(i+1)%> 월</th>
                <% Next %>
            </tr>
            <tr>
                <%
                '월별 잔여 무상시간 출력
                Dim arrRemainingFreeTime(11)

                selectQuery = "SELECT FT_YEAR, FT_MONTH, REMAINING_FREE_TIME FROM TBL_FREE_TIME WHERE FT_UID = '"& uid &"' AND FT_YEAR = YEAR(GETDATE())"
                set objRsT = SendQuery(objConn,selectQuery)

                For i = 0 to objRsT.RecordCount - 1
                    arrRemainingFreeTime(CInt(objRsT("FT_MONTH"))-1) = objRsT("REMAINING_FREE_TIME")
                    objRsT.MoveNext
                Next

                For i=0 to 11
                    If IsEmpty(arrRemainingFreeTime(i)) Then
                        Response.Write "<td scope='row'>0</td>"
                    Else
                        Response.Write "<td scope='row'>" & arrRemainingFreeTime(i) & "</td>"
                    End If
                Next
                %>
            </tr>
        </tbody>
    </table>

</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-danger btn-sm" id="btn_del" name="btn_del" value="탈퇴" onClick="fnDel('<%=objRs("UID")%>');" />&nbsp;
		</li>
		<li class="btm_btn_right">
			<input type="button" class="btn btn-warning btn-sm" id="btn_list" name="btn_list" value="목록" onClick="fnGoBack();" />&nbsp;
			<input type="button" class="btn btn-primary btn-sm" id="btn_add" name="btn_add" value="수정" onClick="fnEdit('<%=objRs("UID")%>');" />
			<input type="hidden" id="list_link_hidden" value="user_list.asp<%=list_link%>"/>
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->
