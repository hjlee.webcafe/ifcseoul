<!--#include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%
Dim selectQuery, uid
uid = Request.QueryString("uid")
selectQuery = "select * from TBL_USER_INFO where UID = '"& uid &"'"

set objRs = SendQuery(objConn,selectQuery)

If objRs.EOF Then
%>
	<script>
		alert("비정상적인 접속입니다.");
		location.href="admin_list.asp<%=list_link%>";
	</script>
<%
Else
	strAuthority = ""
	If objRs("AUTHORITY")&"" = "S" Then
		strAuthority = "SuperAdmin"
	ElseIf objRs("AUTHORITY")&"" = "A" Then
		strAuthority = "Admin"
	End If

	strUseFlag = objRs("USE_FLAG")

	strEditYN = "Y"
	If (objRs("AUTHORITY")&"" = "S" And objRs("UID") <> strAdminId) Or strUseFlag = "N" Then
		strEditYN = "N"
	End If

	strDel = ""
	If objRs("USE_FLAG")&"" = "N" Then
		strDel = " (탈퇴)"
	End If
End If
%>
<script type="text/javascript">
	function fnGoBack() {
		get_link = $("#list_link_hidden").val();
		location.href = get_link;
	}

	<% If strEditYN <> "N" Then %>
		function fnEdit(objVal) {
			location.href = "admin_list_edit.asp?uid=" + objVal;
		}
	<% End If %>
</script>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="관리자 상세정보">
		<caption>관리자 상세정보</caption>
		<colgroup>
			<col width="15%"><col width="35%"><col width="15%"><col width="35%">
		</colgroup>
		<tbody>
			<tr>
				<th>등급</th>
				<td colspan="3"><%=strAuthority%></td>
			</tr>
			<tr>
				<th>아이디</th>
				<td><%=objRs("UID")%><%=strDel%></td>
				<th>이름</th>
				<td><%=objRs("USER_NAME")%></td>
			</tr>
			<tr>
				<th>부서</th>
				<td><%=objRs("DEPT_NAME")%></td>
				<th>직책</th>
				<td><%=objRs("DEPT_LEVEL")%></td>
			</tr>
			<tr>
				<th>전화번호</th>
				<td><%=FnSetNum(objRs("TELNUM"))%></td>
				<th>휴대폰번호</th>
				<td><%=FnSetNum(objRs("MOBILENUM"))%></td>
			</tr>
			<tr>
				<th>이메일</th>
				<td><%=objRs("EMAIL")%></td>
				<th>등록일</th>
				<td><%=FnFormatDateTime(objRs("REG_DT"))%></td>
			</tr>
			<tr>
				<th>메모</th>
				<td colspan="3"><%=FnBR(objRs("DESCRIPTION"))%></td>
			</tr>
			<tr>
				<th>최종 접속일</th>
				<td><%=FnFormatDateTime(objRs("LAST_LOGIN_DT"))%></td>
				<th>최종 수정일</th>
				<td><%=FnFormatDateTime(objRs("UPD_DT"))%></td>
			</tr>
			<% If strUseFlag <> "Y" Then %>
				<tr>
					<th>탈퇴 처리일</th>
					<td><%=FnFormatDateTime(objRs("DEL_DT"))%></td>
					<th>탈퇴 처리자</th>
					<td><%=objRs("DEL_UID")%></td>
				</tr>
			<% End If %>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
		</li>
		<li class="btm_btn_right">
			<input type="button" class="btn btn-warning btn-sm" id="btn_list" name="btn_list" value="목록" onClick="fnGoBack();" />&nbsp;
			<% If strEditYN <> "N" Then %>
				<input type="button" class="btn btn-primary btn-sm" id="btn_edit" name="btn_edit" value="수정" onClick="fnEdit('<%=objRs("UID")%>');" />
			<% End If %>
			<input type="hidden" id="list_link_hidden" value="admin_list.asp<%=list_link%>" />
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->