<!--#include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%
Dim selectQuery, reserve_no
reserve_no = Request.QueryString("reserve_no")

selectQuery = "select (SELECT MIN(CONVERT(INT,RESERVE_TIME)) FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MIN_TIME, (SELECT MAX(CONVERT(INT,RESERVE_TIME))+1 FROM TBL_RESERVATION_TIME WHERE RESERVE_NO = RI.RESERVE_NO) AS MAX_TIME, * from TBL_RESERVATION_INFO RI where RESERVE_NO = '"& reserve_no &"'"

set objRs = SendQuery(objConn,selectQuery)

If objRs.EOF Then
%>
	<script>
		alert("비정상적인 접속입니다.");
		location.href="reservation_list.asp";
	</script>
<%
Else
	strReserveDt = objRs("RESERVE_DT")
	If Len(strReserveDt) = 8 Then
		strReserveDt = Mid(strReserveDt,1,4) & "." & Mid(strReserveDt,5,2) & "." & Mid(strReserveDt,7,2)
	End If

    '예약시간표시
    preTime = ""
    strTimeInfo = ""
    arrReserveTime = Split(objRs("RESERVE_TIMES"), ",")
    For i=0 To UBound(arrReserveTime)
        If i = 0 Then
            strTimeInfo = strTimeInfo & arrReserveTime(i) & ":00"
        ElseIf (CInt(preTime+1) <> CInt(arrReserveTime(i))) Then
            strTimeInfo = strTimeInfo & " ~ " & CInt(preTime+1) & ":00<br/> " & arrReserveTime(i) & ":00"
        End If
        preTime = arrReserveTime(i)
    Next
    strTimeInfo = strTimeInfo & " ~ " & CInt(arrReserveTime(UBound(arrReserveTime))+1) & ":00"

	strStatusHtml1 = ""
	strStatusHtml2 = ""
	strStatusButton = ""
	strReserveStatus = ""
	strCancelButton = ""
	' 예약완료 / 미확인
	If objRs("RESERVE_STATUS")&"" = "R" And objRs("CHK_STATUS")&"" = "N" Then
		strStatusHtml1 = "<th>예약확인</th><td><input type='button' class='btn btn-info btn-sm' id='btn_chk_status' name='btn_chk_status' value='확인완료' onClick='fnChkStatus();'></td>"
		strReserveStatus = "예약완료"
		strStatusButton = "<input type='button' class='btn btn-info btn-sm' id='btn_com_status' name='btn_com_status' value='이용완료' onClick='fnComplete();'>"
		strCancelButton = "<input type='button' class='btn btn-danger btn-sm' id='btn_cancel' name='btn_cancel' value='예약취소' onClick='fnCancel();' />"
	' 예약완료 / 확인
	ElseIf objRs("RESERVE_STATUS")&"" = "R" And objRs("CHK_STATUS")&"" = "Y" Then
		strStatusHtml1 = "<th>예약확인 처리일</th><td>" & FnFormatDateTime(objRs("CHK_DT"))
		strStatusHtml2 = "<tr><th style='height:39px'>예약확인 처리자</th><td colspan='3'>" & objRs("CHK_UID") & "</td></tr>"
		strReserveStatus = "예약완료"
		strStatusButton = "<input type='button' class='btn btn-info btn-sm' id='btn_com_status' name='btn_com_status' value='이용완료' onClick='fnComplete();'>"
		strCancelButton = "<input type='button' class='btn btn-danger btn-sm' id='btn_cancel' name='btn_cancel' value='예약취소' onClick='fnCancel();' />"
	' 이용완료
	ElseIf objRs("RESERVE_STATUS")&"" = "Y" Then
		strStatusHtml1 = "<th>이용완료 처리일</th><td>" & FnFormatDateTime(objRs("COM_DT"))
		strStatusHtml2 = "<tr><th style='height:39px'>이용완료 처리자</th><td colspan='3'>" & objRs("COM_UID") & "</td></tr>"
		strReserveStatus = "이용완료"
	' 예약취소
	ElseIf objRs("RESERVE_STATUS")&"" = "C" Then
		strStatusHtml1 = "<th>예약취소 처리일</th><td>" & FnFormatDateTime(objRs("CAN_DT"))
		strStatusHtml2 = "<tr><th style='height:39px'>예약취소 처리자</th><td colspan='3'>" & objRs("CAN_UID") & "</td></tr>"
		strReserveStatus = "<span style='color:red'>예약취소</span>"
	End If

	strPayStatus = ""
	strPayButton = ""
	strPayHtml1 = ""
	strPayHtml2 = ""
	strPayPenaltyHtml = ""
	' 예약완료 / 이용완료
	If objRs("RESERVE_STATUS")&"" <> "C" Then
		If objRs("PAY_STATUS")&"" = "N" Then
			strPayStatus = "대기"
			strPayButton = "&nbsp;<input type='button' class='btn btn-info btn-sm' id='btn_pay_status' name='btn_pay_status' value='현장결제 처리' onClick='fnPayStatus(""F"");'>"
			' 이용완료일 경우 관리비 합산 추가
			If objRs("RESERVE_STATUS")&"" = "Y" Then
				strPayButton = strPayButton & "&nbsp;<input type='button' class='btn btn-info btn-sm' id='btn_pay_status2' name='btn_pay_status2' value='관리비합산 처리' onClick='fnPayStatus(""M"");'>"
			End If
		ElseIf objRs("PAY_STATUS")&"" = "F" Then
			strPayStatus = "현장결제"
			strPayHtml2 = "<tr><th style='height:39px'>납부 처리자</th><td colspan='3'>" & objRs("PAY_UID") & "</td></tr>"
		ElseIf objRs("PAY_STATUS")&"" = "M" Then
			strPayStatus = "관리비합산"
			strPayHtml2 = "<tr><th style='height:39px'>납부 처리자</th><td colspan='3'>" & objRs("PAY_UID") & "</td></tr>"
		End If
		strPayHtml1 = "<tr><th style='height:39px'>납부</th><td>" & strPayStatus &  strPayButton & "</td><th>납부처리일</th><td>" & FnFormatDateTime(objRs("PAY_DT")) & "</td>"
	' 예약취소
	Else
		strPayPenaltyHtml = "<tr><th style='height:39px'>페널티 금액</th><td colspan='3'>" & FnMoneySet(objRs("CAN_AMOUNT")) & " 원" & "</td></tr>"
		' 페널티금액 있음
		If objRs("CAN_AMOUNT")&"" <> "0" Then
			If objRs("CAN_PAY_STATUS")&"" = "N" Then
				strPayStatus = "대기"
				strPayButton = "&nbsp;<input type='button' class='btn btn-info btn-sm' id='btn_can_pay_status' name='btn_can_pay_status' value='현장결제 처리' onClick='fnCanPayStatus(""F"");'>&nbsp;<input type='button' class='btn btn-info btn-sm' id='btn_can_pay_status2' name='btn_can_pay_status2' value='관리비합산 처리' onClick='fnCanPayStatus(""M"");'>"
			ElseIf objRs("CAN_PAY_STATUS")&"" = "F" Then
				strPayStatus = "현장결제"
				strPayHtml2 = "<tr><th style='height:39px'>납부 처리자</th><td colspan='3'>" & objRs("PAY_UID") & "</td></tr>"
			ElseIf objRs("CAN_PAY_STATUS")&"" = "M" Then
				strPayStatus = "관리비합산"
				strPayHtml2 = "<tr><th style='height:39px'>납부 처리자</th><td colspan='3'>" & objRs("PAY_UID") & "</td></tr>"
			End If
			strPayHtml1 = "<tr><th style='height:39px'>납부</th><td>" & strPayStatus &  strPayButton & "</td><th>납부처리일</th><td>" & FnFormatDateTime(objRs("PAY_DT")) & "</td>"
		End If
	End If


	strRequirement = objRs("REQUIREMENT")
	If strRequirement&"" <> "" Then
		strRequirement = Replace(Replace(strRequirement, "&lt", "<"),"<br><br>","<br>")
	End If

	strDescription = objRs("DESCRIPTION")
	If strDescription&"" <> "" Then
		strDescription = Replace(Replace(strDescription, "&lt", "<"),"<br><br>", Chr(13))
	End If

	strPolycom = ""
	strPolycomHtml = ""
	If objRs("POLYCOM")&"" = "N" Then
		strPolycom = "사용안함"
		strPolycomHtml = ""
	ElseIf objRs("POLYCOM")&"" = "L" Then
		strPolycom = "국내전화"
		strPolycomHtml = "<input id='ETC_AMOUNT' name='ETC_AMOUNT' type='text' class='width_150' maxlength='7' onkeyup='this.value=this.value.replace(/[^0-9]/g,"""");' placeholder='폴리콤금액' value='"&objRs("ETC_AMOUNT")&"'/>&nbsp;<input type='button' class='btn btn-info btn-sm' id='btn_etcAmount' name='btn_etcAmount' value='폴리콤금액처리' onClick='fnEtcAmount();' />"
	ElseIf objRs("POLYCOM")&"" = "G" Then
		strPolycom = "국외전화"
		strPolycomHtml = "<input id='ETC_AMOUNT' name='ETC_AMOUNT' type='text' class='width_150' maxlength='7' onkeyup='this.value=this.value.replace(/[^0-9]/g,"""");' placeholder='폴리콤금액' value='"&objRs("ETC_AMOUNT")&"'/>&nbsp;<input type='button' class='btn btn-info btn-sm' id='btn_etcAmount' name='btn_etcAmount' value='폴리콤금액처리' onClick='fnEtcAmount();' />"
	End If

	strEtc = ""
	If objRs("ETC")&"" = "N" Then
		strEtc = "반입안함"
	ElseIf objRs("ETC")&"" = "Y" Then
		strEtc = "반입함"
	End If

    strFilEquipment = ""
    If objRs("FILMING_EQUIPMENT")&"" = "N" Then
        strFilEquipment = "반입안함"
    ElseIf objRs("FILMING_EQUIPMENT")&"" = "Y" Then
        strFilEquipment = "반입함"
    End If

	strOriAmount = FnMoneySet(objRs("ORI_AMOUNT")) & " 원"
	strTotalAmount = FnMoneySet(objRs("TOTAL_AMOUNT")) & " 원"
End If

%>

<script type="text/javascript">
	function fnGoBack() {
		//get_link = $("#list_link_hidden").val();
		//location.href = get_link;
		history.back();
	}

	function fnChkStatus() {
		if (!confirm("예약확인 하시겠습니까?"))
			return true;

		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/reservation_proc.asp",
			dataType : "json",
			data : ({
						"mode" : "chk_status",
						"RESERVE_NO" : $("#hdn_reserve_no").val(),
						"CHK_UID" : $("#hdn_uid").val()
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else
				{
					var error_msg = data.error_msg;
					if (data.result == "") {
						alert(error_msg);
					}
					else {
						alert("예약확인되었습니다.");
						get_link = "reservation_list_edit.asp?reserve_no=" + $("#hdn_reserve_no").val();
						location.href = get_link;
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}

	function fnComplete() {
		if (!confirm("이용완료 하시겠습니까?"))
			return true;

		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/reservation_proc.asp",
			dataType : "json",
			data : ({
						"mode" : "com_status",
						"RESERVE_NO" : $("#hdn_reserve_no").val(),
						"COM_UID" : $("#hdn_uid").val()
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else
				{
					var error_msg = data.error_msg;
					if (data.result == "") {
						alert(error_msg);
					}
					else {
						alert("이용완료 처리 되었습니다.");
						get_link = $("#list_link_hidden").val();
						location.href = get_link;
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}

	function fnPayStatus(statVal) {
		if (!confirm("납부처리 하시겠습니까?"))
			return true;

		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/reservation_proc.asp",
			dataType : "json",
			data : ({
						"mode" : "pay_status",
						"RESERVE_NO" : $("#hdn_reserve_no").val(),
						"PAY_STATUS" : statVal,
						"PAY_UID" : $("#hdn_uid").val()
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else
				{
					var error_msg = data.error_msg;
					if (data.result == "") {
						alert(error_msg);
					}
					else {
						alert("납부처리 처리 되었습니다.");
						get_link = "reservation_list_edit.asp?reserve_no=" + $("#hdn_reserve_no").val();
						location.href = get_link;
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}

	function fnCanPayStatus(statVal) {
		if (!confirm("납부처리 하시겠습니까?"))
			return true;

		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/reservation_proc.asp",
			dataType : "json",
			data : ({
						"mode" : "can_pay_status",
						"RESERVE_NO" : $("#hdn_reserve_no").val(),
						"CAN_PAY_STATUS" : statVal,
						"PAY_UID" : $("#hdn_uid").val()
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else
				{
					var error_msg = data.error_msg;
					if (data.result == "") {
						alert(error_msg);
					}
					else {
						alert("납부처리 처리 되었습니다.");
						get_link = "reservation_list_edit.asp?reserve_no=" + $("#hdn_reserve_no").val();
						location.href = get_link;
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}

	function fnEtcAmount() {
		if ($("#ETC_AMOUNT").val() == "") {
			$("#ETC_AMOUNT").val("0");
		}

		if (!confirm("폴리콤금액 처리 하시겠습니까?"))
			return true;

		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/reservation_proc.asp",
			dataType : "json",
			data : ({
						"mode" : "etc_amount",
						"RESERVE_NO" : $("#hdn_reserve_no").val(),
						"ETC_AMOUNT" : $("#ETC_AMOUNT").val()
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else
				{
					var error_msg = data.error_msg;
					if (data.result == "") {
						alert(error_msg);
					}
					else {
						alert("폴리콤금액 처리 되었습니다.");
						get_link = $("#list_link_hidden").val();
						location.href = get_link;
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}

	function fnCancel(objVal) {

        <%
        '팝업시 취소위약금 표시
        strReserveDt = ""
        If Len(objRs("RESERVE_DT")) = 8 Then
            strReserveDt = Mid(objRs("RESERVE_DT"),1,4) & "-" & Mid(objRs("RESERVE_DT"),5,2) & "-" & Mid(objRs("RESERVE_DT"),7,2)
        End If

        CAN_AMOUNT = 0
        If DateDiff("d", NOW, CDate(strReserveDt)) <= 14 Then               '14일~이용당일 위약금 100%
            CAN_AMOUNT = objRs("TOTAL_AMOUNT")
        ElseIf DateDiff("d", NOW, CDate(strReserveDt)) < 30 Then            '15일~29일전까지 위약금 50%
            CAN_AMOUNT = objRs("TOTAL_AMOUNT") / 2
        End If
        %>

		if (!confirm("예약취소 하시겠습니까? ( 위약금 : <%=FnMoneySet(CAN_AMOUNT)%> )"))
			return true;

		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/reservation_proc.asp",
			dataType : "json",
			data : ({
						"mode" : "cancel",
						"RESERVE_NO" : $("#hdn_reserve_no").val(),
						"CAN_UID" : $("#hdn_uid").val()
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else
				{
					var error_msg = data.error_msg;
					if (data.result == "") {
						alert(error_msg);
					}
					else {
						alert("예약취소 처리 되었습니다.");
						get_link = $("#list_link_hidden").val();
						location.href = get_link;
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}

	function fnEdit(objVal) {
		if ($("#SUBJECT").val() == "") {
			alert("회의명을 입력해주세요.");
			$("#SUBJECT").focus();
			return true;
		}

		if ($("#RESERVE_NAME").val() == "") {
			alert("예약자명을 입력해주세요.");
			$("#RESERVE_NAME").focus();
			return true;
		}

		if ($("#TELNUM").val() == "") {
			alert("전화번호를 입력해주세요.");
			$("#TELNUM").focus();
			return true;
		}

		if ($("#MOBILENUM").val() == "") {
			alert("휴대폰번호를 입력해주세요.");
			$("#MOBILENUM").focus();
			return true;
		}

		if ($("#EMAIL").val() == "") {
			alert("이메일을 입력해주세요.");
			$("#EMAIL").focus();
			return true;
		}
		else {
			var emailReg = new RegExp(/^([0-9a-zA-Z_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,3}$/);
			if (!emailReg.test($("#EMAIL").val())) {
				alert("이메일 주소가 잘못되었습니다.");
				$("#EMAIL").focus();
				return true;
			}
		}

		if (!confirm("수정 하시겠습니까?"))
			return true;

		if ($("#index_loading_overlay").css("display") != 'none') {
			alert("수정 중 입니다");
			return false;
		}
		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/reservation_proc.asp",
			dataType : "json",
			data : ({
						"mode" : "edit",
						"RESERVE_NO" : objVal,
						"SUBJECT" : $("#SUBJECT").val(),
						"RESERVE_NAME" : $("#RESERVE_NAME").val(),
						"TELNUM" : $("#TELNUM").val(),
						"MOBILENUM" : $("#MOBILENUM").val(),
						"FAXNUM" : $("#FAXNUM").val(),
						"EMAIL" : $("#EMAIL").val(),
						"DESCRIPTION" : $("#DESCRIPTION").val(),
						"UPD_UID" : $("#hdn_uid").val()
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();

				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == "") {
						alert(error_msg);
					}
					else {
						alert("수정되었습니다.");
						get_link = $("#list_link_hidden").val();
						location.href = get_link;
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}
</script>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="회의실 예약수정">
		<caption>회의실 예약수정</caption>
		<colgroup>
			<col width="15%"><col width="35%"><col width="15%"><col width="35%">
		</colgroup>
		<tbody>
			<tr>
				<th style="height:39px">예약번호</th>
				<td><%=objRs("RESERVE_NO")%><input type="hidden" id="hdn_reserve_no" value="<%=objRs("RESERVE_NO")%>" /></td>
				<th>예약일</th>
				<td><%=FnFormatDateTime(objRs("REG_DT"))%></td>
			</tr>
			<tr>
				<th style="height:39px">상태</th>
				<td><%=strReserveStatus%></td>
				<%=strStatusHtml1%>
			</tr>
			<%=strStatusHtml2%>
			<tr>
				<th style="height:39px">회의명</th>
				<td colspan="3"><input id="SUBJECT" name="SUBJECT" type="text" class="i_text100" value="<%=objRs("SUBJECT")%>" maxlength="30" /></td>
			</tr>
			<tr>
				<th style="height:39px">회의실</th>
				<td><%=objRs("ROOM_NAMES")%></td>
				<th>참석인원</th>
				<td><%=objRs("P_COUNT")%>명</td>
			</tr>
			<tr>
				<th style="height:39px">이용날짜</th>
				<td><%=strReserveDt%></td>
				<th>이용시간</th>
				<td><%=strTimeInfo%> (<%=objRs("T_QTY")%>시간)</td>
			</tr>
			<tr>
				<th style="height:39px">예약자명</th>
				<td><input id="RESERVE_NAME" name="RESERVE_NAME" type="text" class="width_150" value="<%=objRs("RESERVE_NAME")%>" maxlength="50" /></td>
				<th>입주사명</th>
				<td><%=objRs("USER_NAME")%></td>
			</tr>
			<tr>
				<th style="height:39px">전화번호</th>
				<td><input id="TELNUM" name="TELNUM" type="text" class="width_150" value="<%=objRs("TELNUM")%>" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
				<th>휴대폰번호</th>
				<td><input id="MOBILENUM" name="MOBILENUM" type="text" class="width_150" value="<%=objRs("MOBILENUM")%>" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
			</tr>
			<tr>
				<th style="height:39px">FAX</th>
				<td><input id="FAXNUM" name="FAXNUM" type="text" class="width_150" value="<%=objRs("FAXNUM")%>" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
				<th>이메일</th>
				<td><input id="EMAIL" name="EMAIL" type="text" class="i_text50" value="<%=objRs("EMAIL")%>" maxlength="100" style="ime-mode:disabled" /></td>
			</tr>
			<tr>
				<th style="height:39px">요청사항</th>
				<td colspan="3"><%=strRequirement%></td>
			</tr>
			<tr>
				<th style="height:84px">메모</th>
				<td colspan="3">
					<textarea id="DESCRIPTION" name="DESCRIPTION" type="text" style="height:80px;" maxlength="1000"><%=strDescription%></textarea>
				</td>
			</tr>
			<tr>
				<th style="height:39px">폴리콤(3자통화)</th>
				<td><%=strPolycom%>&nbsp;<%=strPolycomHtml%></td>
				<th>외부기자재 반입</th>
				<td><%=strEtc%></td>
			</tr>
            <tr>
                <th style="height:39px">촬영장비반입</th>
                <td colspan="3"><%=strFilEquipment%></td>
            </tr>
			<tr>
				<th style="height:39px">기본요금</th>
				<td><%=strOriAmount%></td>
				<th>요금할인율</th>
				<td><%=objRs("DISCOUNT")%> %</td>
			</tr>
			<tr>
				<th style="height:39px">무료이용시간</th>
				<td><%=objRs("FREE_TIME")%></td>
				<th>최종금액</th>
				<td><%=strTotalAmount%></td>
			</tr>
			<%=strPayPenaltyHtml%>
			<%=strPayHtml1%>
			<%=strPayHtml2%>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<%=strCancelButton%>
		</li>
		<li class="btm_btn_right">
			<%=strStatusButton%>
			<input type="button" class="btn btn-warning btn-sm" id="btn_list" name="btn_list" value="목록" onClick="fnGoBack();" />&nbsp;
			<input type="button" class="btn btn-primary btn-sm" id="btn_add" name="btn_add" value="수정" onClick="fnEdit('<%=objRs("RESERVE_NO")%>');" />
			<input type="hidden" id="list_link_hidden" value="reservation_list.asp"/>
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->
