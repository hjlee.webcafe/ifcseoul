<!--#include file = "default_control.asp" -->
<!--#include file = "index_header.asp" -->
<%
Dim selectQuery, uid
uid = Request.QueryString("uid")

selectQuery = "select * from TBL_USER_INFO where UID = '"& uid &"'"

set objRs = SendQuery(objConn,selectQuery)

If objRs.EOF Then
%>
	<script>
		alert("비정상적인 접속입니다.");
		location.href="admin_list.asp<%=list_link%>";
	</script>
<%
Else
	If objRs("AUTHORITY")&"" = "S" And objRs("UID")&"" <> strAdminId Then
%>
		<script>
			alert("SuperAdmin은 자신만 수정가능합니다.");
			location.href="admin_list.asp<%=list_link%>";
		</script>
<%
	Else
		strAuthority = ""
		If objRs("AUTHORITY")&"" = "S" Then
			strAuthority = "SuperAdmin"
		ElseIf objRs("AUTHORITY")&"" = "A" Then
			strAuthority = "Admin"
		End If

		strUseFlag = objRs("USE_FLAG")

		strEditYN = "Y"
		If (objRs("AUTHORITY")&"" = "S" And objRs("UID") <> strAdminId) Or strUseFlag = "N" Then
			strEditYN = "N"
		End If
	End If
End If
%>

<script type="text/javascript">
	function fnGoBack() {
		get_link = $("#list_link_hidden").val();
		location.href = get_link;
	}

	function fnDel(objVal) {
		if (!confirm("탈퇴 하시겠습니까?"))
			return true;

		if ($("#index_loading_overlay").css("display") != 'none') {
			alert("탈퇴 중 입니다");
			return false;
		}
		
		$("#index_loading_overlay").show();
		
		$.ajax ({
			type : "POST",
			url : "./ajax/admin_proc.asp",
			dataType : "json",
			data : ({
						"mode" : "del", 
						"UID" : objVal,
						"DEL_UID" : $("#hdn_uid").val()
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();
			
				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else
				{
					var error_msg = data.error_msg;
					if (data.result == 0) {
						alert("탈퇴되었습니다.");
						get_link = "admin_list.asp";//$("#list_link_hidden").val();
						location.href = get_link;
					}
					else {
						alert(error_msg);
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}

	function fnEdit(objVal) {
		if ($("#USER_NM").val() == "") {
			alert("이름를 입력해주세요.");
			$("#USER_NM").focus();
			return true;
		}
		
		if ($("#PASSWORD").val() != $("#PASSWORD_CHK").val()) {
			alert("수정할 비밀번호가 다릅니다.\n\n비밀번호를 확인해주세요.");
			$("#PASSWORD_CHK").focus();
			return true;
		}
		/*
		if ($("#DEPT_NAME").val() == "") {
			alert("부서를 입력해주세요.");
			$("#DEPT_NAME").focus();
			return true;
		}
		
		if ($("#DEPT_LEVEL").val() == "") {
			alert("직책을 입력해주세요.");
			$("#DEPT_LEVEL").focus();
			return true;
		}
		
		if ($("#TELNUM").val() == "") {
			alert("전화번호를 입력해주세요.");
			$("#TELNUM").focus();
			return true;
		}
		
		if ($("#MOBILENUM").val() == "") {
			alert("휴대폰번호를 입력해주세요.");
			$("#MOBILENUM").focus();
			return true;
		}
		*/
		if ($("#EMAIL").val() == "") {
			alert("이메일을 입력해주세요.");
			$("#EMAIL").focus();
			return true;
		}
		else {
			var emailReg = new RegExp(/^([0-9a-zA-Z_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/);
			if (!emailReg.test($("#EMAIL").val())) {
				alert("이메일 주소가 잘못되었습니다.");
				$("#EMAIL").focus();
				return true;
			}
		}

		var newPassword = "";
		if ($("#PASSWORD").val() != "") {
			newPassword = hex_md5($("#PASSWORD").val());
		}

		if (!confirm("수정 하시겠습니까?"))
			return true;
		
		if ($("#index_loading_overlay").css("display") != 'none') {
			alert("수정 중 입니다");
			return false;
		}
		$("#index_loading_overlay").show();

		$.ajax ({
			type : "POST",
			url : "./ajax/admin_proc.asp",
			dataType : "json",
			data : ({
						"mode" : "edit",
						"AUTHORITY" : $("#AUTHORITY").val(),
						"UID" : objVal,
						"USER_NM" : $("#USER_NM").val(),
						"PASSWORD" : newPassword,
						"DEPT_NAME" : $("#DEPT_NAME").val(),
						"DEPT_LEVEL" : $("#DEPT_LEVEL").val(),
						"TELNUM" : $("#TELNUM").val(),
						"MOBILENUM" : $("#MOBILENUM").val(),
						"EMAIL" : $("#EMAIL").val(),
						"DESCRIPTION" : $("#DESCRIPTION").val(),
						"UPD_UID" : $("#hdn_uid").val()
					}),
			success : function (data) {
				$("#index_loading_overlay").hide();
			
				if (typeof data != 'object') {
					alert("Error :: Ajax Return Error");
				}
				else {
					var error_msg = data.error_msg;
					if (data.result == 0) {
						alert("수정되었습니다.");
						get_link = "admin_list.asp";//$("#list_link_hidden").val();
						location.href = get_link;
					}
					else {
						alert(error_msg);
					}
				}
			},
			error : function (data) {
				$("#index_loading_overlay").hide();
				alert("Error : Ajax Error : " + data);
			}
		});
	}
</script>
<div class="table_list">
	<table class="tbl_list" id="table_list" border="1" cellspacing="0" summary="관리자 계정수정">
		<caption>관리자 계정수정</caption>
		<colgroup>
			<col width="15%"><col width="35%"><col width="15%"><col width="35%">
		</colgroup>
		<tbody>
			<tr>
				<th>등급</th>
				<% If objRs("authority")&""="S" Then %>
					<td colspan="3"><%=strAuthority%><input type="hidden" id="AUTHORITY" name="AUTHORITY" value="<%=objRs("authority")%>"></td>
				<% Else %>
					<td colspan="3">
						<select id="AUTHORITY" name="AUTHORITY" class="form-control input-sm width_100">
							<option value="S" <%If objRs("authority")&""="S" Then%>selected<%End If%>>SuperAdmin</option>
							<option value="A" <%If objRs("authority")&""="A" Then%>selected<%End If%>>Admin</option>
						</select>
					</td>
				<% End If %>
			</tr>
			<tr>
				<th>아이디</th>
				<td><%=objRs("UID")%></td>
				<th>이름</th>
				<td><input id="USER_NM" name="USER_NM" type="text" class="width_150" value="<%=objRs("USER_NAME")%>" maxlength="50" /></td>
			</tr>
			<tr>
				<th>비밀번호</th>
				<td><input id="PASSWORD" name="PASSWORD" type="password" class="width_150" value="" maxlength="16" /><br><!--<span>※ 영문 / 숫자 조합 8~16자리</span>--></td>
				<th>비밀번호 확인</th>
				<td><input id="PASSWORD_CHK" name="PASSWORD_CHK" type="password" class="width_150" value="" maxlength="16" /><br><span>※ 비밀번호를 한번 더 입력해주세요.</span></td>
			</tr>
			<tr>
				<th>부서</th>
				<td><input id="DEPT_NAME" name="DEPT_NAME" type="text" class="width_150" value="<%=objRs("DEPT_NAME")%>" maxlength="50" /></td>
				<th>직책</th>
				<td><input id="DEPT_LEVEL" name="DEPT_LEVEL" type="text" class="width_150" value="<%=objRs("DEPT_LEVEL")%>" maxlength="20" /></td>
			</tr>
			<tr>
				<th>전화번호</th>
				<td><input id="TELNUM" name="TELNUM" type="text" class="width_150" value="<%=objRs("TELNUM")%>" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
				<th>휴대폰번호</th>
				<td><input id="MOBILENUM" name="MOBILENUM" type="text" class="width_150" value="<%=objRs("MOBILENUM")%>" maxlength="20" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" /></td>
			</tr>
			<tr>
				<th>이메일</th>
				<td><input id="EMAIL" name="EMAIL" type="text" class="i_text50" value="<%=objRs("EMAIL")%>" maxlength="100" style="ime-mode:disabled" /></td>
				<th>등록일</th>
				<td><%=FnFormatDateTime(objRs("REG_DT"))%></td>
			</tr>
			<tr>
				<th>메모</th>
				<td colspan="3">
					<textarea id="DESCRIPTION" name="DESCRIPTION" type="text" style="height:80px;" maxlength="1000"><%=objRs("DESCRIPTION")%></textarea>
				</td>
			</tr>
			<tr>
				<th>최종 접속일</th>
				<td><%=FnFormatDateTime(objRs("LAST_LOGIN_DT"))%></td>
				<th>최종 수정일</th>
				<td><%=FnFormatDateTime(objRs("UPD_DT"))%></td>
			</tr>
		</tbody>
	</table>
</div>
<div class="btm_btn_wrap">
	<ul>
		<li>
			<input type="button" class="btn btn-danger btn-sm" id="btn_del" name="btn_del" value="탈퇴" onClick="fnDel('<%=objRs("UID")%>');" />&nbsp;
		</li>
		<li class="btm_btn_right">
			<input type="button" class="btn btn-warning btn-sm" id="btn_list" name="btn_list" value="목록" onClick="fnGoBack();" />&nbsp;
			<input type="button" class="btn btn-primary btn-sm" id="btn_add" name="btn_add" value="수정" onClick="fnEdit('<%=objRs("UID")%>');" />
			<input type="hidden" id="list_link_hidden" value="admin_list.asp<%=list_link%>"/>
		</li>
	</ul>
</div>

<!--#include file = "index_footer.asp" -->