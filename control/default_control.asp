<!-- #include file = "../inc/_config.asp" -->
<!-- #include file = "../control/page_handling.asp" -->
<%
' remove cache
Response.Expires = -1
Response.ExpiresAbsolute = now() - 1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

' get current URL
Dim current_index_file, type_array, current_type, snb_name
current_index_file = Request.ServerVariables("SCRIPT_NAME")
type_array = Split(current_index_file, "/")
' get current file name
current_index_file = type_array(UBound(type_array))

' set default language type
If current_index_file = "index.asp" Then
	current_type = ""
Else
	If UBound(type_array) > 1 Then
		If type_array(UBound(type_array)-1) <> "" Then
			' get current language type, en - english, ko - korean
			current_type = type_array(UBound(type_array)-1)	
			If current_type <> "ko" and current_type <> "en" Then
				current_type = ""
			END If
		End If
	End If
End If

' set current type to session
Session("current_type") = current_type

strUserId = Session("F_UID")
strUserName = Session("F_USER_NAME")
strUserAuth = Session("F_AUTHORITY")

' get control name from current_index_file
Dim control_name_array, control_name
control_name_array = Split(current_index_file, ".")

' this is for more than one "." inside the file
for i = 0 to UBound(control_name_array) - 1
	control_name = control_name & control_name_array(i) & "."
Next
	control_name = Left(control_name, Len(control_name)-1)

' get a snb name of first two string
If control_name = "HF_01_00" Then
	snb_name = "lm"
ElseIf control_name = "HF_02_00" Then
	snb_name = "et"
ElseIf control_name = "HF_07_00" Then
	snb_name = "sm"	
Else
	snb_name = Left(control_name, 2)
	snb_name = LCase(snb_name)
End If
snb_name = path_prefix & "com/snb/" & snb_name & ".html"

' declare default include variable 
Dim css_include, js_include, js_file, css_file, fs, wrap_main
css_include = ""
js_include = ""

' for only main page
If control_name = "MN_00_00" Then
	wrap_main = "main"
End If

If control_name = "MN_00_00_test" Then
	wrap_main = "main"
End If

' set a default css file
If current_type = "ko" Then
	css_include = css_include & "<link rel=""stylesheet"" type=""text/css"" href=""/css/base_ko.css"" />"
Else
	css_include = css_include & "<link rel=""stylesheet"" type=""text/css"" href=""/css/base.css"" />"
End If

' get a js include
js_file = path_prefix & "js/" & control_name & "/script.js"
set fs=Server.CreateObject("Scripting.FileSystemObject")
if fs.FileExists(Server.MapPath(js_file)) then
  js_include = js_include & "<script type=""text/javascript"" src="""& js_file &"""></script>"
end if

' get a css include
css_file = path_prefix & "css/" & control_name & "/style.css"
if fs.FileExists(Server.MapPath(css_file)) then
  css_include = css_include & "<link rel=""stylesheet"" type=""text/css"" href="""& css_file &""" />"
end if

%>
<!--#include file = "../inc/xmlParser.asp" -->