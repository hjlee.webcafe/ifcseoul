<!--#include file = "./control/default_control.asp" -->
<%
    ' counting list
    Dim selectQuery, num , flag
    num = Request.QueryString("num")
    flag = Request.QueryString("flag")

    selectQuery = "select q_title, q_text from dbo.tblQrcode where q_num = '" & num & "' and q_flag =  '" & flag & "'"
    set objConn = OpenDBConnection()
    set objRs = SendQuery(objConn,selectQuery)

%>

<!DOCTYPE html>
<html xml:lang="ko" lang="ko">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <script type="text/javascript" src="/ckeditor5/build/ckeditor.js"></script>
    <title>QRCODE</title>
    <link rel="stylesheet" type="text/css" href="/overseer/css/base.css" />

    <!-- for bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-61219190-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-61219190-1');
    </script>

    <style>

        #containerWrap { max-width:1024px; margin: auto;}
        .q_text { border:0px; padding:30px; }
        .ck-editor__editable_inline { min-height: 500px; }
        .ck-editor__top {display:none; }
    </style>
</head>
<body>
 <div id="wrap">
    <div id="containerWrap">
        <!-- <h1><%=objRs("q_title")%></h1> -->
         <div id="container">
            <table class="table" id="table_list">
                <tr><td>
            <textarea type="text" id="editor" name="q_text" id="q_text" placeholder="내용을 입력 해 주세요">
                <%=objRs("q_text")%>
            </textarea>
        </td></tr>
            </table>
        </div>
    </div>
 </div>

<script>

    ClassicEditor.create( document.querySelector( '#editor' ), {
        image: {
            styles: ['alignLeft', 'alignCenter', 'alignRight']
        }
    } )
    .then( editor => {
        window.editor = editor;
        editor.isReadOnly = true;
    } )
    .catch( error => {
        console.error( error );
    } );

</script>
</body>
</html>
