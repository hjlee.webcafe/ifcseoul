<!-- #include file = "JSON_2.0.4.asp" -->
<%
 Session.CodePage = 65001
 Response.CharSet = "utf-8"
 Response.AddHeader "Pragma", "no-cache"
 Response.AddHeader "cache-control", "no-staff"
 Response.Expires = -1
 Response.ContentType="text/html"
%>

<%
Set xmlHttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
xmlHttp.Open "GET", "http://openapi.seoul.go.kr:8088/516c7a764c63656e383479704f554a/xml/AccInfo/1/5", False
xmlHttp.Send

Set xmlDOM = server.CreateObject("Microsoft.XMLDOM")
xmlDOM.async = False
xmlDOM.loadXML( xmlHttp.responseText )

Set nodeTotalCnt= xmlDOM.selectSingleNode("/AccInfo/list_total_count")
' Response.Write nodeTotalCnt.text

Set nodeRows = xmlDOM.selectNodes("/AccInfo/row")
'
Dim result
Dim temp, idx
'Set result = jsobject()
Set result = jsObject()
' Set dAccType = Server.CreateObject("Scripting.Dictionary")
' dAccType.Add "A01", "교통사고"
' dAccType.Add "A02", "차량고장"
' dAccType.Add "A03", "보행사고"
' dAccType.Add "A04", "공사"
' dAccType.Add "A05", "낙하물"
' dAccType.Add "A06", "버스사고"
' dAccType.Add "A07", "지하철사고"
' dAccType.Add "A08", "화재"
' dAccType.Add "A09", "기상/재난"
' dAccType.Add "A10", "집회및행사"
' dAccType.Add "A11", "기타"
' dAccType.Add "A12", "제보"
' dAccType.Add "A13", "단순정보"


idx = 0
For Each nodeRow In nodeRows
    Set result(idx) = jsObject()
   result(idx)("acc_id") = nodeRow.selectSingleNode("acc_id").text
   result(idx)("acc_datetime") = Left(nodeRow.selectSingleNode("occr_date").text, 4) + "-" + Mid(nodeRow.selectSingleNode("occr_date").text, 5,2) + "-" + Right(nodeRow.selectSingleNode("occr_date").text, 2) + " " + Left(nodeRow.selectSingleNode("occr_time").text,2) + ":" + Mid(nodeRow.selectSingleNode("occr_time").text,3,2) + ":" +Right(nodeRow.selectSingleNode("occr_time").text,2) +"~" + Left(nodeRow.selectSingleNode("exp_clr_date").text, 4) + "-" + Mid(nodeRow.selectSingleNode("exp_clr_date").text, 5,2) + "-" + Right(nodeRow.selectSingleNode("exp_clr_date").text, 2) + " " + Left(nodeRow.selectSingleNode("exp_clr_time").text,2) + ":" + Mid(nodeRow.selectSingleNode("exp_clr_time").text,3,2) + ":" +Right(nodeRow.selectSingleNode("exp_clr_time").text,2)
   ' result(idx)("acc_type") = dAccType.item(nodeRow.selectSingleNode("acc_type").text)
   result(idx)("acc_type") = getAccType(nodeRow.selectSingleNode("acc_type").text)
   result(idx)("acc_linkinfo") = getLinkInfo(nodeRow.selectSingleNode("link_id").text)
   result(idx)("acc_info") = nodeRow.selectSingleNode("acc_info").text
   idx = idx + 1
Next

result.Flush

Function getLinkInfo(link_id)
    Dim sUrl, sLinkInfo
    sUrl = "http://openapi.seoul.go.kr:8088/516c7a764c63656e383479704f554a/xml/LinkInfo/1/5/" + link_id
    //Response.Write sUrl

    Set xmlHttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
    xmlHttp.Open "GET", sUrl, False
    xmlHttp.Send

    Set xmlDOM = server.CreateObject("Microsoft.XMLDOM")
    xmlDOM.async = False
    xmlDOM.loadXML( xmlHttp.responseText )

    Set nodeRows1 = xmlDOM.selectNodes("/LinkInfo/row")
    For Each nodeRow1 In nodeRows1
         sLinkInfo = nodeRow1.selectSingleNode("road_name").text + "(" + nodeRow1.selectSingleNode("st_node_nm").text + "->" + nodeRow1.selectSingleNode("ed_node_nm").text + ")"
'sLinkInfo = nodeRow1.selectSingleNode("road_name").text
    Next
getLinkInfo = sLinkInfo

End Function

Function getAccType(code)
    Dim sUrl, sAccType


    Set xmlHttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
    xmlHttp.Open "GET", "http://openapi.seoul.go.kr:8088/516c7a764c63656e383479704f554a/xml/AccMainCode/1/15", False
    xmlHttp.Send

    Set xmlDOM = server.CreateObject("Microsoft.XMLDOM")
    xmlDOM.async = False
    xmlDOM.loadXML( xmlHttp.responseText )

    Set nodeRows2 = xmlDOM.selectNodes("/AccMainCode/row")
    For Each nodeRow2 In nodeRows2
        If nodeRow2.selectSingleNode("acc_type").text = code Then
               sAccType = nodeRow2.selectSingleNode("acc_type_nm").text
         End If
    Next
    getAccType = sAccType

End Function


%>



